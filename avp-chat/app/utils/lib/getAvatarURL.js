import { getURL } from './getURL';

export const getAvatarURL = ({ username, roomId, cache }) => {
	if (username) {
		// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
		let avatarUrl = '';
		try {
			let accout_url = process.env.ACCOUNT_URL || 'http://account.anvietenergy.com:8080';
			avatarUrl = accout_url + '/api/pri/user/' + username + '/avatar';
		} catch (ex) {
			avatarUrl = getURL(`/avatar/${ encodeURIComponent(username) }${ cache ? `?etag=${ cache }` : '' }`);
		}
		return avatarUrl; //getURL(`/avatar/${ encodeURIComponent(username) }${ cache ? `?etag=${ cache }` : '' }`);
	}
	if (roomId) {
		return getURL(`/avatar/room/${ encodeURIComponent(roomId) }${ cache ? `?etag=${ cache }` : '' }`);
	}
};
