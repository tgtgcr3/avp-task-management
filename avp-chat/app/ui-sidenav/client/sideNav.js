import {Meteor} from 'meteor/meteor';
import {Tracker} from 'meteor/tracker';
import {ReactiveVar} from 'meteor/reactive-var';
import {FlowRouter} from 'meteor/kadira:flow-router';
import {Template} from 'meteor/templating';

import {SideNav, menu, modal, popover} from '../../ui-utils';
import {settings} from '../../settings';
import {roomTypes, getUserPreference, t} from '../../utils';
import {Users} from '../../models';
import {hasAtLeastOnePermission, hasPermission} from "/app/authorization";

Template.sideNav.helpers({
	flexTemplate() {
		return SideNav.getFlex().template;
	},

	flexData() {
		return SideNav.getFlex().data;
	},

	footer() {
		return String(settings.get('Layout_Sidenav_Footer')).trim();
	},

	roomType() {
		let filter = '';
		if (Template.instance()._filterRoom) {
			filter = Template.instance()._filterRoom.get();
		}
		return roomTypes.getTypes().map((roomType) => ({
			template: roomType.customTemplate || 'roomList',
			data: {
				header: roomType.header,
				identifier: roomType.identifier,
				isCombined: roomType.isCombined,
				label: roomType.label,
				filter: filter
			},
		}));
	},

	loggedInUser() {
		return !!Meteor.userId();
	},

	sidebarViewMode() {
		const viewMode = getUserPreference(Meteor.userId(), 'sidebarViewMode');
		return viewMode || 'condensed';
	},

	sidebarHideAvatar() {
		return getUserPreference(Meteor.userId(), 'sidebarHideAvatar');
	},
	currentFilter() {
		if (Template.instance()._filterRoom)
			return Template.instance()._filterRoom.get();
		return '';
	}

});

Template.sideNav.events({
	'click .close-flex'() {
		return SideNav.closeFlex();
	},

	'click .arrow'() {
		return SideNav.toggleCurrent();
	},

	'scroll .rooms-list'() {
		return menu.updateUnreadBars();
	},

	'dropped .sidebar'(e) {
		return e.preventDefault();
	},
	'mouseenter .sidebar-item__link'(e) {
		const element = e.currentTarget;
		setTimeout(() => {
			const ellipsedElement = element.querySelector('.sidebar-item__ellipsis');
			const isTextEllipsed = ellipsedElement.offsetWidth < ellipsedElement.scrollWidth;

			if (isTextEllipsed) {
				element.setAttribute('title', element.getAttribute('aria-label'));
			} else {
				element.removeAttribute('title');
			}
		}, 0);
	},
	'click .toolbar-applist'() {
		jQuery('.parent-one-app-list').remove();
		jQuery('.one-people-list').remove();
		Template.sideNav.oneHelper.oneSelectToolbar('.toolbar-applist');

		let asideOffset = jQuery('aside').offset();
		    const windowHeight = window.innerHeight;
		    const windowWidth = window.innerWidth;
		    const { left, top } = asideOffset;
				let token = Cookies.get('rc_token');
				let userId = Cookies.get('rc_uid');
		let appList = new AppList(token, userId);
		appList.activate(left);
	},
	'click .toolbar-message'() {
		jQuery('.one-people-list').remove();
		jQuery('.parent-one-app-list').remove();
		Template.sideNav.oneHelper.oneSelectToolbar('.toolbar-message');
	},
	'click .toolbar-people'() {
		jQuery('.parent-one-app-list').remove();
		Template.sideNav.oneHelper.oneSelectToolbar('.toolbar-people');
		    let asideOffset = jQuery('aside').offset();
		    const windowHeight = window.innerHeight;
		    const windowWidth = window.innerWidth;
		    const { left, top } = asideOffset;
		let token = Cookies.get('rc_token');
		let userId = Cookies.get('rc_uid');
		let userList = new UserList(token, userId);
		userList.activate(left);
		userList.getActiveUserList();
		userList.getOfflineUserList();
	},
	'click .toolbar-create'() {
		const action = (title, content) => (e) => {
		//	e.preventDefault();
			modal.open({
				title: t(title),
				content,
				data: {
					onCreate() {
						modal.close();
					},
				},
				modifier: 'modal',
				showConfirmButton: false,
				showCancelButton: false,
				confirmOnEnter: false,
			});
		};

		const createChannel = action('Tạo Nhóm', 'createChannel');
		// const createDirectMessage = action('Direct_Messages', 'CreateDirectMessage');
		// const createDiscussion = action('Discussion_title', 'CreateDiscussion');


		const items = [
			hasAtLeastOnePermission(['create-c', 'create-p'])
			&& {
				icon: 'hashtag',
				name: t('Nhóm'),
				action: createChannel,
			},
			// hasPermission('create-d')
			// && {
			// 	icon: 'team',
			// 	name: t('Direct_Messages'),
			// 	action: createDirectMessage,
			// },
			// settings.get('Discussion_enabled') && hasAtLeastOnePermission(['start-discussion', 'start-discussion-other-user'])
			// && {
			// 	icon: 'discussion',
			// 	name: t('Discussion'),
			// 	action: createDiscussion,
			// },
		].filter(Boolean);

		// if (items.length === 1) {
		// 	return items[0].action(e);
		// }
		createChannel();
		return;

		const config = {
			columns: [
				{
					groups: [
						{
							items,
						},
					],
				},
			],
			currentTarget: $('.toolbar-create')[0],
			//offsetVertical: ($('.toolbar-create')[0].clientHeight + 10)
			offsetHorizontal: - $('.toolbar-create').width()
		};
		popover.open(config);
	}
});

Template.sideNav.oneHelper = {
	oneSelectToolbar(selector) {
		jQuery('.tool-bar-item').removeClass('activate');
		jQuery(selector).addClass('activate');
	}
}

const redirectToDefaultChannelIfNeeded = () => {
	const needToBeRedirect = () => ['/', '/home'].includes(FlowRouter.current().path);

	Tracker.autorun((c) => {
		const firstChannelAfterLogin = settings.get('First_Channel_After_Login');

		if (!needToBeRedirect()) {
			return c.stop();
		}

		if (!firstChannelAfterLogin) {
			return c.stop();
		}

		const room = roomTypes.findRoom('c', firstChannelAfterLogin, Meteor.userId());

		if (!room) {
			return;
		}

		c.stop();
		FlowRouter.go(`/channel/${firstChannelAfterLogin}`);
	});
};

Template.sideNav.onRendered(function () {
	SideNav.init();
	menu.init();
	redirectToDefaultChannelIfNeeded();

	return Meteor.defer(() => menu.updateUnreadBars());
});

Template.sideNav.onCreated(function () {
	this.groupedByType = new ReactiveVar(false);
	this._filterRoom = new ReactiveVar();
	//this._filterRoom.set('abc');
	let me = this;
	setTimeout(() => {
		jQuery('#abc').keyup(() => {
			console.log(jQuery('#abc').val());
			me._filterRoom.set(jQuery('#abc').val());
		});
		try {
		let token = Cookies.get('rc_token');
		let userId = Cookies.get('rc_uid');
		let userList = new UserList(token, userId);
		userList.sendLoginData(Meteor.user().username);
		} catch(ex) {
		}
	}, 2000);
	this.autorun(() => {
		const user = Users.findOne(Meteor.userId(), {
			fields: {
				'settings.preferences.sidebarGroupByType': 1
			},
		});
		const userPref = getUserPreference(user, 'sidebarGroupByType');
		this.groupedByType.set(userPref || settings.get('UI_Group_Channels_By_Type'));
	});
});
