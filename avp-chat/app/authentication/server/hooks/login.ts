import { Accounts } from 'meteor/accounts-base';

import { ILoginAttempt } from '../ILoginAttempt';
import { saveFailedLoginAttempts, saveSuccessfulLogin } from '../lib/restrictLoginAttempts';
import { logFailedLoginAttempts } from '../lib/logLoginAttempts';
import { callbacks } from '../../../callbacks/server';
import { settings } from '../../../settings/server';

import { HTTP } from 'meteor/http';

Accounts.onLoginFailure((login: ILoginAttempt) => {
	if (settings.get('Block_Multiple_Failed_Logins_Enabled')) {
		saveFailedLoginAttempts(login);
	}

	logFailedLoginAttempts(login);
});

Accounts.onLogout((opts) => {
	// aaaaaaaaaaaaaaaaaaaaa
	try {
		let accout_url = process.env.ACCOUNT_URL || 'http://account.anvietenergy.com:8080';
		let postUrl = accout_url + '/api/pri/keycloak/user/logout'
		if (opts && opts.user) {
			HTTP.post(postUrl, {
				headers:{
					'Content-Type': 'application/json'
				},
				data: {
					token: opts.user.username
				}
			}, (error, result) => {
				console.log(error);
				console.log(result);
			})
		}
	} catch (err) {

	}
	console.log('aaaaaaaaaaaaaaaa');

});

callbacks.add('afterValidateLogin', (login: ILoginAttempt) => {
	if (!settings.get('Block_Multiple_Failed_Logins_Enabled')) {
		return;
	}

	saveSuccessfulLogin(login);
});
