"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class MustExtendAppError {
    constructor() {
        this.name = 'MustExtendApp';
        this.message = 'App must extend the "App" abstract class.';
    }
}
exports.MustExtendAppError = MustExtendAppError;

//# sourceMappingURL=MustExtendAppError.js.map
