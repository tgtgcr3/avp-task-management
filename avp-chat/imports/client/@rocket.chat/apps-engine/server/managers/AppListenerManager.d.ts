import { IExternalComponent } from '../../definition/externalComponent';
import { ILivechatEventContext, ILivechatRoom } from '../../definition/livechat';
import { IMessage } from '../../definition/messages';
import { AppInterface } from '../../definition/metadata';
import { IRoom, IRoomUserJoinedContext } from '../../definition/rooms';
import { IUIKitIncomingInteraction, IUIKitResponse } from '../../definition/uikit';
import { IUser } from '../../definition/users';
import { AppManager } from '../AppManager';
import { ProxiedApp } from '../ProxiedApp';
export declare class AppListenerManager {
    private readonly manager;
    private am;
    private listeners;
    /**
     * Locked events are those who are listed in an app's
     * "essentials" list but the app is disabled.
     *
     * They will throw a EssentialAppDisabledException upon call
     */
    private lockedEvents;
    constructor(manager: AppManager);
    registerListeners(app: ProxiedApp): void;
    unregisterListeners(app: ProxiedApp): void;
    releaseEssentialEvents(app: ProxiedApp): void;
    lockEssentialEvents(app: ProxiedApp): void;
    getListeners(int: AppInterface): Array<ProxiedApp>;
    isEventBlocked(event: AppInterface): boolean;
    executeListener(int: AppInterface, data: IMessage | IRoom | IUser | ILivechatRoom | IUIKitIncomingInteraction | IExternalComponent | ILivechatEventContext | IRoomUserJoinedContext): Promise<void | boolean | IMessage | IRoom | IUser | IUIKitResponse | ILivechatRoom>;
    private executePreMessageSentPrevent;
    private executePreMessageSentExtend;
    private executePreMessageSentModify;
    private executePostMessageSent;
    private executePreMessageDeletePrevent;
    private executePostMessageDelete;
    private executePreMessageUpdatedPrevent;
    private executePreMessageUpdatedExtend;
    private executePreMessageUpdatedModify;
    private executePostMessageUpdated;
    private executePreRoomCreatePrevent;
    private executePreRoomCreateExtend;
    private executePreRoomCreateModify;
    private executePostRoomCreate;
    private executePreRoomDeletePrevent;
    private executePostRoomDeleted;
    private executePreRoomUserJoined;
    private executePostRoomUserJoined;
    private executePostExternalComponentOpened;
    private executePostExternalComponentClosed;
    private executeUIKitInteraction;
    private executePostLivechatRoomStarted;
    private executeLivechatRoomClosedHandler;
    private executePostLivechatRoomClosed;
    private executePostLivechatAgentAssigned;
    private executePostLivechatAgentUnassigned;
}
