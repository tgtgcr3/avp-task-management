"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ConfigurationExtend {
    constructor(https, sets, cmds, api, externalComponents) {
        this.http = https;
        this.settings = sets;
        this.slashCommands = cmds;
        this.api = api;
        this.externalComponents = externalComponents;
    }
}
exports.ConfigurationExtend = ConfigurationExtend;

//# sourceMappingURL=ConfigurationExtend.js.map
