"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ConfigurationModify {
    constructor(sets, cmds) {
        this.serverSettings = sets;
        this.slashCommands = cmds;
    }
}
exports.ConfigurationModify = ConfigurationModify;

//# sourceMappingURL=ConfigurationModify.js.map
