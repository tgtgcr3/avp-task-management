"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MarketplacePricingStrategy;
(function (MarketplacePricingStrategy) {
    MarketplacePricingStrategy["PricingStrategyOnce"] = "once";
    MarketplacePricingStrategy["PricingStrategyMonthly"] = "monthly";
    MarketplacePricingStrategy["PricingStrategyYearly"] = "yearly";
})(MarketplacePricingStrategy = exports.MarketplacePricingStrategy || (exports.MarketplacePricingStrategy = {}));

//# sourceMappingURL=MarketplacePricingStrategy.js.map
