"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MarketplaceSubscriptionType;
(function (MarketplaceSubscriptionType) {
    MarketplaceSubscriptionType["SubscriptionTypeApp"] = "app";
    MarketplaceSubscriptionType["SubscriptionTypeService"] = "service";
})(MarketplaceSubscriptionType = exports.MarketplaceSubscriptionType || (exports.MarketplaceSubscriptionType = {}));

//# sourceMappingURL=MarketplaceSubscriptionType.js.map
