"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MarketplacePurchaseType;
(function (MarketplacePurchaseType) {
    MarketplacePurchaseType["PurchaseTypeBuy"] = "buy";
    MarketplacePurchaseType["PurchaseTypeSubscription"] = "subscription";
})(MarketplacePurchaseType = exports.MarketplacePurchaseType || (exports.MarketplacePurchaseType = {}));

//# sourceMappingURL=MarketplacePurchaseType.js.map
