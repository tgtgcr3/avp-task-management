"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AppStorage {
    constructor(engine) {
        this.engine = engine;
    }
    getEngine() {
        return this.engine;
    }
}
exports.AppStorage = AppStorage;

//# sourceMappingURL=AppStorage.js.map
