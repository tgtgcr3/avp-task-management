"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AppLogStorage {
    constructor(engine) {
        this.engine = engine;
    }
    getEngine() {
        return this.engine;
    }
}
exports.AppLogStorage = AppLogStorage;

//# sourceMappingURL=AppLogStorage.js.map
