"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AppClientManager_1 = require("./AppClientManager");
exports.AppClientManager = AppClientManager_1.AppClientManager;
const AppServerCommunicator_1 = require("./AppServerCommunicator");
exports.AppServerCommunicator = AppServerCommunicator_1.AppServerCommunicator;

//# sourceMappingURL=index.js.map
