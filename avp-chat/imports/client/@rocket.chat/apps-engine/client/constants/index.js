"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * The id length of each action.
 */
exports.ACTION_ID_LENGTH = 80;
exports.MESSAGE_ID = 'rc-apps-engine-ui';

//# sourceMappingURL=index.js.map
