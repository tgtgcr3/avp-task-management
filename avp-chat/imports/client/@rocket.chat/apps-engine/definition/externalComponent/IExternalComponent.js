"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ExternalComponentLocation;
(function (ExternalComponentLocation) {
    ExternalComponentLocation["CONTEXTUAL_BAR"] = "CONTEXTUAL_BAR";
    ExternalComponentLocation["MODAL"] = "MODAL";
})(ExternalComponentLocation = exports.ExternalComponentLocation || (exports.ExternalComponentLocation = {}));

//# sourceMappingURL=IExternalComponent.js.map
