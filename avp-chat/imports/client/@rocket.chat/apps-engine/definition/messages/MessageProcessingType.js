"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MessageProcessingType;
(function (MessageProcessingType) {
    MessageProcessingType["SendMessage"] = "sendMessage";
    MessageProcessingType["RespondWithMessage"] = "respondWithMessage";
})(MessageProcessingType = exports.MessageProcessingType || (exports.MessageProcessingType = {}));

//# sourceMappingURL=MessageProcessingType.js.map
