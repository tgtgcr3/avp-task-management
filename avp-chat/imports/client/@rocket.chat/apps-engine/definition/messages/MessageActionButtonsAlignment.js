"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MessageActionButtonsAlignment;
(function (MessageActionButtonsAlignment) {
    MessageActionButtonsAlignment["VERTICAL"] = "vertical";
    MessageActionButtonsAlignment["HORIZONTAL"] = "horizontal";
})(MessageActionButtonsAlignment = exports.MessageActionButtonsAlignment || (exports.MessageActionButtonsAlignment = {}));

//# sourceMappingURL=MessageActionButtonsAlignment.js.map
