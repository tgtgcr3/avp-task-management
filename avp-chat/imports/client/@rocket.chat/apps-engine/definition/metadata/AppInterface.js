"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppInterface;
(function (AppInterface) {
    // Messages
    AppInterface["IPreMessageSentPrevent"] = "IPreMessageSentPrevent";
    AppInterface["IPreMessageSentExtend"] = "IPreMessageSentExtend";
    AppInterface["IPreMessageSentModify"] = "IPreMessageSentModify";
    AppInterface["IPostMessageSent"] = "IPostMessageSent";
    AppInterface["IPreMessageDeletePrevent"] = "IPreMessageDeletePrevent";
    AppInterface["IPostMessageDeleted"] = "IPostMessageDeleted";
    AppInterface["IPreMessageUpdatedPrevent"] = "IPreMessageUpdatedPrevent";
    AppInterface["IPreMessageUpdatedExtend"] = "IPreMessageUpdatedExtend";
    AppInterface["IPreMessageUpdatedModify"] = "IPreMessageUpdatedModify";
    AppInterface["IPostMessageUpdated"] = "IPostMessageUpdated";
    // Rooms
    AppInterface["IPreRoomCreatePrevent"] = "IPreRoomCreatePrevent";
    AppInterface["IPreRoomCreateExtend"] = "IPreRoomCreateExtend";
    AppInterface["IPreRoomCreateModify"] = "IPreRoomCreateModify";
    AppInterface["IPostRoomCreate"] = "IPostRoomCreate";
    AppInterface["IPreRoomDeletePrevent"] = "IPreRoomDeletePrevent";
    AppInterface["IPostRoomDeleted"] = "IPostRoomDeleted";
    AppInterface["IPreRoomUserJoined"] = "IPreRoomUserJoined";
    AppInterface["IPostRoomUserJoined"] = "IPostRoomUserJoined";
    // External Components
    AppInterface["IPostExternalComponentOpened"] = "IPostExternalComponentOpened";
    AppInterface["IPostExternalComponentClosed"] = "IPostExternalComponentClosed";
    // Blocks
    AppInterface["IUIKitInteractionHandler"] = "IUIKitInteractionHandler";
    // Livechat
    AppInterface["IPostLivechatRoomStarted"] = "IPostLivechatRoomStarted";
    AppInterface["IPostLivechatRoomClosed"] = "IPostLivechatRoomClosed";
    /**
     * @deprecated please use the AppMethod.EXECUTE_POST_LIVECHAT_ROOM_CLOSED method
     */
    AppInterface["ILivechatRoomClosedHandler"] = "ILivechatRoomClosedHandler";
    AppInterface["IPostLivechatAgentAssigned"] = "IPostLivechatAgentAssigned";
    AppInterface["IPostLivechatAgentUnassigned"] = "IPostLivechatAgentUnassigned";
})(AppInterface = exports.AppInterface || (exports.AppInterface = {}));

//# sourceMappingURL=AppInterface.js.map
