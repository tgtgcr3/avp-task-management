"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UIKitIncomingInteractionType;
(function (UIKitIncomingInteractionType) {
    UIKitIncomingInteractionType["BLOCK"] = "blockAction";
    UIKitIncomingInteractionType["VIEW_SUBMIT"] = "viewSubmit";
    UIKitIncomingInteractionType["VIEW_CLOSED"] = "viewClosed";
})(UIKitIncomingInteractionType = exports.UIKitIncomingInteractionType || (exports.UIKitIncomingInteractionType = {}));

//# sourceMappingURL=IUIKitIncomingInteraction.js.map
