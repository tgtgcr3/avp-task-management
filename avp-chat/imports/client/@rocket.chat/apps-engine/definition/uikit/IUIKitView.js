"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UIKitViewType;
(function (UIKitViewType) {
    UIKitViewType["MODAL"] = "modal";
    UIKitViewType["HOME"] = "home";
})(UIKitViewType = exports.UIKitViewType || (exports.UIKitViewType = {}));

//# sourceMappingURL=IUIKitView.js.map
