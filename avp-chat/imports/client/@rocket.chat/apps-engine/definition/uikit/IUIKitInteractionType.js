"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UIKitInteractionType;
(function (UIKitInteractionType) {
    UIKitInteractionType["MODAL_OPEN"] = "modal.open";
    UIKitInteractionType["MODAL_CLOSE"] = "modal.close";
    UIKitInteractionType["MODAL_UPDATE"] = "modal.update";
    UIKitInteractionType["ERRORS"] = "errors";
})(UIKitInteractionType = exports.UIKitInteractionType || (exports.UIKitInteractionType = {}));

//# sourceMappingURL=IUIKitInteractionType.js.map
