"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UIKitIncomingInteractionContainerType;
(function (UIKitIncomingInteractionContainerType) {
    UIKitIncomingInteractionContainerType["MESSAGE"] = "message";
    UIKitIncomingInteractionContainerType["VIEW"] = "view";
})(UIKitIncomingInteractionContainerType = exports.UIKitIncomingInteractionContainerType || (exports.UIKitIncomingInteractionContainerType = {}));

//# sourceMappingURL=UIKitIncomingInteractionContainer.js.map
