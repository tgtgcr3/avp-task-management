"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BlockType;
(function (BlockType) {
    BlockType["SECTION"] = "section";
    BlockType["DIVIDER"] = "divider";
    BlockType["IMAGE"] = "image";
    BlockType["ACTIONS"] = "actions";
    BlockType["CONTEXT"] = "context";
    BlockType["INPUT"] = "input";
})(BlockType = exports.BlockType || (exports.BlockType = {}));

//# sourceMappingURL=Blocks.js.map
