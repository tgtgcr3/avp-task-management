"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TextObjectType;
(function (TextObjectType) {
    TextObjectType["MARKDOWN"] = "mrkdwn";
    TextObjectType["PLAINTEXT"] = "plain_text";
})(TextObjectType = exports.TextObjectType || (exports.TextObjectType = {}));

//# sourceMappingURL=Objects.js.map
