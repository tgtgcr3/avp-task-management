"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LogMessageSeverity;
(function (LogMessageSeverity) {
    LogMessageSeverity["DEBUG"] = "debug";
    LogMessageSeverity["INFORMATION"] = "info";
    LogMessageSeverity["LOG"] = "log";
    LogMessageSeverity["WARNING"] = "warning";
    LogMessageSeverity["ERROR"] = "error";
    LogMessageSeverity["SUCCESS"] = "success";
})(LogMessageSeverity = exports.LogMessageSeverity || (exports.LogMessageSeverity = {}));

//# sourceMappingURL=ILogEntry.js.map
