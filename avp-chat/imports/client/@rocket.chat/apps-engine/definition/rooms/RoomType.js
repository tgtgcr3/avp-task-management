"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RoomType;
(function (RoomType) {
    RoomType["CHANNEL"] = "c";
    RoomType["PRIVATE_GROUP"] = "p";
    RoomType["DIRECT_MESSAGE"] = "d";
    RoomType["LIVE_CHAT"] = "l";
})(RoomType = exports.RoomType || (exports.RoomType = {}));

//# sourceMappingURL=RoomType.js.map
