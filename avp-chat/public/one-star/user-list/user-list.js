class UserList {
	constructor(apiKey, userId) {
		this.apiKey = apiKey;
		this.userId = userId;
		this.onlineUsers = [];
		this.offlineUSers = [];
		this.disabledUsernames = undefined;
	}

	sendLoginData(username) {
		var me = this;
		jQuery.ajax(
			{
				type: 'GET',
				beforeSend: (request) => {
					request.setRequestHeader('X-Auth-Token', this.apiKey);
					request.setRequestHeader('X-User-Id', this.userId);
				},
				url: '/api/v1/users.info?userId=' + me.userId,
				success: (msg) => {
					console.log(msg);
					if (msg.success == true && msg.user && msg.user.services && msg.user.services.keycloak && msg.user.services.keycloak.expiresAt) {
						let currentLogin = msg.user.services.keycloak.expiresAt;
						let lastLogin = window.localStorage.getItem('_my_last_login');
						if (currentLogin != lastLogin) {
							window.localStorage.setItem('_my_last_login', currentLogin);
							me.sendLoginDataToLogin(username);
						}
					}
				}
			}
		);
	}



	sendLoginDataToLogin(username) {

			jQuery.getJSON('https://api.ipify.org?format=json', (data) => {
				jQuery.ajax(
					{
						type: 'POST',
						beforeSend: (request) => {
							request.setRequestHeader('X-Auth-Token', this.apiKey);
							request.setRequestHeader('X-User-Id', this.userId);
						},
						url: '/api/v1/ntn/me/login_log?username=' + username + "&publicIp=" + data.ip,
						success: (msg) => {
							console.log(msg);
						}
					}
				);
}).fail(() => {
	jQuery.ajax(
		{
			type: 'POST',
			beforeSend: (request) => {
				request.setRequestHeader('X-Auth-Token', this.apiKey);
				request.setRequestHeader('X-User-Id', this.userId);
			},
			url: '/api/v1/ntn/me/login_log?username=' + username,
			success: (msg) => {
				console.log(msg);
			}
		}
	);
});

	}

	getActiveUserList() {
		var me = this;
		jQuery.ajax(
			{
				type: 'GET',
				beforeSend: (request) => {
					request.setRequestHeader('X-Auth-Token', this.apiKey);
					request.setRequestHeader('X-User-Id', this.userId);
				},
				url: '/api/v1/users.presence',
				success: (msg) => {
					console.log(msg);
					me.onlineUsers = msg.users;
					//me._populateUser(me, me.onlineUsers, 'one-online-userlist', '.one-online-userlist-result');
					me._populateOnlineUsers(me, me.onlineUsers);
				}
			}
		)
	}

	_populateUser(me, users, parentClass, selector) {
		let accountUrl = '';
		try {
			accountUrl = window.__one_settings.server_account_url;
		} catch (ex) {
		}

		if (me.disabledUsernames == undefined) {
			me._getDisabledUser(this._populateUser, me, users, parentClass, selector);
		} else {
			let $html = jQuery('<div />').addClass(parentClass);
			let numOfUser = 0;
			users.forEach(function (u) {
				if (me.disabledUsernames.indexOf(u.username) < 0) {
					let $user = me._createUserElement(u, accountUrl);
					$html.append($user);
					numOfUser++;
				}
			});
			jQuery(selector).find('.one-user-list-item').remove();
			jQuery(selector).append($html);
			//TODO: hacking
			if (selector.indexOf('.one-online-userlist-result') == 0) {
				jQuery('.online-number').text('( ' + numOfUser + ' )');
			}
		}
	}

	_getDisabledUser(callback, me, users, parentClass, selector) {
		jQuery.ajax(
			{
				type: 'GET',
				beforeSend: (request) => {
					request.setRequestHeader('X-Auth-Token', this.apiKey);
					request.setRequestHeader('X-User-Id', this.userId);
				},
				url: '/api/v1/ntn/me/users',
				success: (msg) => {
					me.disabledUsernames = msg.body.filter(u => u.enabled == false).map(u => u.username);
					callback(me, users, parentClass, selector);
				}
			}
		)
	}

	_populateOnlineUsers(me, users) {
		me._populateUser(me, users, 'one-online-userlist', '.one-online-userlist-result');
	}

	_populateOfflineUsers(me, users) {
		me._populateUser(me, users, 'one-offline-userlist', '.one-offline-userlist-result');
	}

	getOfflineUserList() {
		var me = this;
		jQuery.ajax(
			{
				type: 'GET',
				beforeSend: (request) => {
					request.setRequestHeader('X-Auth-Token', this.apiKey);
					request.setRequestHeader('X-User-Id', this.userId);
				},
				url: '/api/v1/users.list?query={"status":"offline"}&count=10240',
				success: (msg) => {
					console.log(msg);
					me.offlineUSers = msg.users;
					me._populateOfflineUsers(me, me.offlineUSers);
				}
			}
		)
	}



	_createUserElement(userInfo, accountUrl) {
		let $user = jQuery('<div class="one-user-list-item"/>').append(
			`<div class="one-user-list-item-avatar" onclick="_send_direct_message('${userInfo.username}')">` +
			`<figure aria-label="${userInfo.username}" class="rcx-box rcx-box--full rcx-avatar rcx-@4dp4l7">` +
			`<img src="${accountUrl}/api/pri/user/${userInfo.username}/avatar" class="rcx-box rcx-box--full rcx-avatar__element rcx-@4dp4l7">` +
			'</figure>' +
			'</div>' +
			'<div class="one-user-list-item-name">' +
			userInfo.name +
			'</div>'
		);
		return $user;
	}

	activate(left) {
		var me = this;
		let $peopleList = jQuery(
			'<div class="one-star one-people-list">' +
			'<div class="one-search-bar form-group has-search">' +
			'<span class="fa fa-search form-control-feedback"></span>' +
			'<input type="text" class="form-control mysearch" placeholder="Search" />' +
			'</div>' +
			'<div class="one-search-result">' +
			'<div class="one-online-userlist-result one-userlist">' +
			'<span class="list-title">Thành Viên Đang Đăng Nhập</span><span class="online-number"></span>' +
			'</div>' +
			'<div class="one-offline-userlist-result one-userlist">' +
			'<span class="list-title">Thành Viên Đang Ngoại Tuyến</span>' +
			'</div>' +
			'<div class="one-guest-userlist-result one-userlist">' +
			'<span class="list-title">Tài Khoảng Khách</span>' +
			'</div>' +
			'</div>' +
			'</div>'
		).css({ left: left })
		jQuery('#rocket-chat').append($peopleList).find('.mysearch').keyup(function (newText) {
			newText = jQuery(this).val().toLowerCase();
			let onlineUsers = me.onlineUsers.filter(function (u) {
				return transliterate(u.name.toLowerCase()).indexOf(transliterate(newText)) >= 0;
			})
			me._populateOnlineUsers(me, onlineUsers);

			let offlineUsers = me.offlineUSers.filter(function (u) {
				return transliterate(u.name.toLowerCase()).indexOf(transliterate(newText)) >= 0;
			})
			me._populateOfflineUsers(me, offlineUsers);
		});

	}
}
