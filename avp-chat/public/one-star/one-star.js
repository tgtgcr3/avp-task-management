
function Task() {
    this.taskFn = undefined;
    this.conditionFn = undefined;
}

Task.prototype.tobeExecuted = function() {
    return this.conditionFn();
}

Task.prototype.execute = function() {
    this.taskFn();
}

window.onestar = {
    toolbar_item: 'toolbarMessage',
    tasks: []

}

function onestar_onAppSelectionClick() {
    alert('abc');
}

function __onestar_waiting_heatbeat() {
    if (window.onestar.tasks.length > 0) {
        let isDoneIndex = [];
        for (let i = 0; i < window.onestar.tasks.length; i++) {
            try {
                if (window.onestar.tasks[i].tobeExecuted() == true) {
                    window.onestar.tasks[i].execute();
                    isDoneIndex.push(i);
                }
            } catch (ex) {
                isDoneIndex.push(i);
            }
        }
        if (isDoneIndex.length > 0) {
            for (let j = isDoneIndex.length - 1; j >= 0; j--) {
                window.onestar.tasks.splice(isDoneIndex[j], 1);
            }
        }
    }
    if (window.onestar.tasks.length > 0) {
        setTimeout(__onestar_waiting_heatbeat, 200);
    }
}

function _onestar_waiting(taskFn, conditionFn) {
    let task = new Task();
    task.taskFn = taskFn;
    task.conditionFn = conditionFn;
    window.onestar.tasks.push(task);
    if (window.onestar.tasks.length == 1) {
        setTimeout(__onestar_waiting_heatbeat, 200);
    }
}

_onestar_waiting(_onestar_startup_task, function() {
    return jQuery != undefined;
});


function _onestar_startup_task() {
    if (window.onestar && window.onestar.toolbar_item) {
        _onestar_waiting(function() {
            _onestar_selectToolbarItem(window.onestar.toolbar_item);
            _once_ready();
        }, function() {
            return jQuery('.tool-bar-item').length > 5;
        })

    }
}

function _once_ready() {
    // jQuery('#toolbarPeople').click(() => {
    //     _onestar_selectToolbarItem('toolbarPeople');
    // //     let asideOffset = jQuery('aside').offset();
    // //     const windowHeight = window.innerHeight;
    // //     const windowWidth = window.innerWidth;
    // //     const { left, top } = asideOffset;
	// //
    // //     // let $peoplelist = jQuery('<div class="one-star one-people-list"></div>').css({left: left});
    // //     // jQuery('#rocket-chat').append($peoplelist);
	// //
    // // let token = Cookies.get('rc_token');
    // // let userId = Cookies.get('rc_uid');
    // // let userList = new UserList(token, userId);
    // // userList.activate(left);
    // // userList.getActiveUserList();
    // // userList.getOfflineUserList();
    // });
    // jQuery('#toolbarMessage').click(() => {
    //     jQuery('.one-people-list').remove();
    //     _onestar_selectToolbarItem('toolbarMessage');
    // })
	// jQuery('#toolbarCreateNew').click(() => {
	// 	jQuery('#create-new-thread-btn').click();
	// });
    //'sidebar__toolbar-button rc-tooltip rc-tooltip--down js-button'
}

function _onestar_selectToolbarItem(id) {
    // jQuery('.tool-bar-item').removeClass('activate');
    // jQuery('#' + id).addClass('activate');
}

function _onestar_like_click() {
    // event = event || window.event;
    // let source = event.target || event.srcElement;
    let a = jQuery('span.one-like');
    let $textArea = a.parent().find('textarea');
    let currentVal = $textArea.val();
    $textArea.val(':like:');

        $textArea.focus();
        let $event = jQuery.Event('keydown', {which: 13});
        $textArea.trigger($event);
        $event = jQuery.Event('keydown', {which: 13});
        $textArea.trigger($event);


    $textArea.val(currentVal);
    $textArea.focus();
    if (currentVal != '') {
        $textArea.trigger("change");
    }

    // console.log(a);
    // //const cookies = new Cookies();
    // let token = Cookies.get('rc_token');
    // let userId = Cookies.get('rc_uid');
    // let userList = new UserList(token, userId);
    // console.log(userList.getUserList());
}

function _send_direct_message(username) {
    let $a = jQuery("<a/>").prop('href','/direct/' + username);//.click();
    jQuery('.one-people-list').append($a);
    $a[0].click();
    $a.remove();
    jQuery('.one-people-list').remove();
    _onestar_selectToolbarItem('toolbarMessage');
}
