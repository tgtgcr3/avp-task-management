///aaaaaaaaaaaaaaaaaaaaa
(function(){
	let __cfg = {};
	if (window.location.href.toLowerCase().indexOf('192.168.11') > 0) {
		//local
		__cfg = {
			account_url: 'http://192.168.11.51:4200',
			task_url: 'http://192.168.11.51:4300/avp',
			server_account_url: 'http://192.168.11.51:8080'
		};
	} else {
		__cfg = {
			account_url: 'http://account.anvietenergy.com:4200',
			task_url: 'http://task.anvietenergy.com:4300/avp',
			server_account_url: 'http://account.anvietenergy.com:8080'
		};
	}
	window.__one_settings = __cfg;
})();


class AppList {

	constructor(apiKey, userId) {
		this.apiKey = apiKey;
		this.userId = userId
	}

	activate(left, username, fullname) {
		let accountUrl = '';
        try {
            accountUrl = window.__one_settings.server_account_url;
        } catch (ex) {            
        }
		let $peopleList = jQuery(
			`<div class="parent-one-app-list"><div class="one-app-list">
			<div class="one-app-list-header">
				<div class="avatar">
					<img src="${accountUrl}/api/pri/user/${username}/avatar" />
				</div>
				<div class="user-info">
					<div class="fullname">

					</div>
				</div>
			</div>
			<div class="content">
				<div class="app-items">

					<div class="app-item item-wework">
						<div class="app-icon app-icon-wework">

						</div>
						<div class="app-name">
							AVP Task
						</div>
						<div class="app-description">
							Task Manager
						</div>
					</div>

					<div class="app-item item-account">
						<div class="app-icon app-icon-account">

						</div>
						<div class="app-name">
							AVP Account
						</div>
						<div class="app-description">
							Account AVP
						</div>
					</div>
				</div>
			</div>
		</div></div>
		`
	).css({left: left});



	jQuery('#rocket-chat').append($peopleList);
	jQuery('.parent-one-app-list .item-wework').click(function() {
		window.open(window.__one_settings.task_url, '_blank');
	});
	jQuery('.parent-one-app-list .item-account').click(function() {
		window.open(window.__one_settings.account_url, '_blank');
	});

		jQuery.ajax(
			{
					type: 'GET',
					beforeSend: (request) => {
							request.setRequestHeader('X-Auth-Token', this.apiKey);
							request.setRequestHeader('X-User-Id', this.userId);
					},
					url: '/api/v1/me',
					success: (msg) => {
							console.log(msg);
							jQuery('.parent-one-app-list .avatar img').prop('src', `${accountUrl}/api/pri/user/${msg.username}/avatar`);
							jQuery('.parent-one-app-list .fullname').text(msg.name);

					}
			}
	)

}

}
