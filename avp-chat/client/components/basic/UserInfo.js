import React, {useState, useEffect} from 'react';
import { Box, Margins, Tag, Button, Icon, Grid } from '@rocket.chat/fuselage';
import { css } from '@rocket.chat/css-in-js';

import { useTimeAgo } from '../../hooks/useTimeAgo';
import { useTranslation } from '../../contexts/TranslationContext';
import VerticalBar from './VerticalBar';
import { UTCClock } from './UTCClock';
import UserAvatar from './avatar/UserAvatar';
import UserCard from './UserCard';
import MarkdownText from './MarkdownText';
import { isNamedImports } from 'typescript';

const Label = (props) => <Box fontScale='p2' color='default' {...props} />;

const wordBreak = css`
	word-break: break-word;
`;

const Info = ({ className, ...props }) => <UserCard.Info className={[className, wordBreak]} flexShrink={0} {...props}/>;

// class NtnUserInfo extends React.Component {

// 	constructor(
// 		props
// 	) {
// 		super();
// 		this.userTitle = '';
// 	}

// 	componentDidMount() {
// 		jQuery.ajax(
// 	{
// 		type: 'GET',
// 		// beforeSend: (request) => {
// 		// 	// request.setRequestHeader('X-Auth-Token', this.apiKey);
// 		// 	// request.setRequestHeader('X-User-Id', this.userId);
// 		// },
// 		url: '/api/v1/ntn/me/users',
// 		success: (msg) => {
// 			// me.disabledUsernames = msg.body.filter(u => u.enabled == false).map(u => u.username);
// 			//me._populateUser(me, me.onlineUsers, 'one-online-userlist', '.one-online-userlist-result');
// 			//me._populateOnlineUsers(me, me.onlineUsers);
// 			// callback(me, users, parentClass, selector);
// 			this.userTitle = 'abc';
// 		}
// 	}
// )
// 	}

// 	render() {
// 		return <VerticalBar.ScrollableContent p='x24' {...this.props}>
// 		<Grid>
// 			<Grid.Item key="1">
// 				<UserAvatar size={'x60'} title={this.props.username} username={this.props.username}/>
// 			</Grid.Item>
// 			<Grid.Item key="3">
// 			{/* <UserCard.Username name={username} status={status} /> */}
// 				<Info>{ this.props.name }</Info>
// 				<Info>{ this.userTitle }</Info>
// 			</Grid.Item>

// 		</Grid>


// 		{this.props.actions}

// 		<Margins block='x4'>
// 			<UserCard.Username name={this.props.username} status={this.props.status} />
// 			<Info>{this.props.customStatus}</Info>

// 			{/* {email && <> <Label>{t('Email')}</Label>
// 				<Info display='flex' flexDirection='row' alignItems='center'>
// 					<Box is='a' withTruncatedText href={`mailto:${ email.address }`}>{email.address}</Box>
// 					<Margins inline='x4'>
// 						{email.verified && <Tag variant='primary'>{t('Verified')}</Tag>}
// 						{email.verified || <Tag disabled>{t('Not_verified')}</Tag>}
// 					</Margins>
// 				</Info>
// 			</>} */}


// 		</Margins>

// 	</VerticalBar.ScrollableContent>;
// 	}
// }

// export const UserInfo = React.memo(NtnUserInfo);

export const UserInfo = React.memo(function UserInfo({
	username,
	bio,
	email,
	status,
	phone,
	customStatus,
	roles = [],
	lastLogin,
	createdAt,
	utcOffset,
	customFields = [],
	name,
	data,
	// onChange,
	actions,
	...props
}) {
	const t = useTranslation();

	const timeAgo = useTimeAgo();
// aaaaaaaaaaaaaaaaaaaaaa
	const [userTitle, setUserTitle] = useState(null);
	const [phoneNumber, setPhoneNumber] = useState(null);
	const [myEmail, setMyEmail] = useState(null);
	useEffect(() => {
		function updateUserInfo(response) {
			console.log(response);
			let users = response.body;
			let user = users.find(u => u.username == username);
			if (user) {
				setUserTitle(user.userTitle);
				setMyEmail(user.email);
				setPhoneNumber(user.phoneNumber);
			}
		}

		jQuery.ajax(
			{
				type: 'GET',
				beforeSend: (request) => {
					request.setRequestHeader('X-Auth-Token', this.apiKey);
					request.setRequestHeader('X-User-Id', this.userId);
				},
				url: '/api/v1/ntn/me/userData',
				success: updateUserInfo
			}
		)
	}, []);


		return <VerticalBar.ScrollableContent p='x24' {...props}>
		<Grid>
			<Grid.Item xs="1">
				<UserAvatar size={'x60'} title={username} username={username} status="online"/>
			</Grid.Item>
			<Grid.Item xs="3">
			{/* <UserCard.Username name={username} status={status} /> */}
				<UserCard.Username name={name} status={status} />
				<Info>{ userTitle }</Info>
			</Grid.Item>

		</Grid>
		<div className="my-class"></div>
		<Margins block='x4'>
			<Label>Email</Label>
			<Info>{ myEmail }</Info>
			<Label>Số Điện Thoại</Label>
			<Info> { phoneNumber }</Info>
		</Margins>

	</VerticalBar.ScrollableContent>;

	// return <VerticalBar.ScrollableContent p='x24' {...props}>
	// 	<Grid>
	// 		<Grid.Item key="1">
	// 			<UserAvatar size={'x60'} title={username} username={username}/>
	// 		</Grid.Item>
	// 		<Grid.Item key="3">
	// 		{/* <UserCard.Username name={username} status={status} /> */}
	// 			<Info>{ name }</Info>
	// 		</Grid.Item>

	// 	</Grid>


	// 	{actions}

	// 	<Margins block='x4'>
	// 		<UserCard.Username name={username} status={status} />
	// 		<Info>{customStatus}</Info>

	// 		{!!roles && <>
	// 			<Label>{t('Roles')}</Label>
	// 			<UserCard.Roles>{roles}</UserCard.Roles>
	// 		</>}

	// 		{Number.isInteger(utcOffset) && <>
	// 			<Label>{t('Local Time')}</Label>
	// 			<Info><UTCClock utcOffset={utcOffset}/></Info>
	// 		</>}

	// 		<Label>{t('Last_login')}</Label>
	// 		<Info>{lastLogin ? timeAgo(lastLogin) : t('Never')}</Info>

	// 		{name && <>
	// 			<Label>{t('Full Name')}</Label>
	// 			<Info>{name}</Info>
	// 		</>}

	// 		{bio && <>
	// 			<Label>{t('Bio')}</Label>
	// 			<Info withTruncatedText={false}><MarkdownText content={bio}/></Info>
	// 		</>}

	// 		{phone && <> <Label>{t('Phone')}</Label>
	// 			<Info display='flex' flexDirection='row' alignItems='center'>
	// 				<Box is='a' withTruncatedText href={`tel:${ phone }`}>{phone}</Box>
	// 			</Info>
	// 		</>}

	// 		{email && <> <Label>{t('Email')}</Label>
	// 			<Info display='flex' flexDirection='row' alignItems='center'>
	// 				<Box is='a' withTruncatedText href={`mailto:${ email.address }`}>{email.address}</Box>
	// 				<Margins inline='x4'>
	// 					{email.verified && <Tag variant='primary'>{t('Verified')}</Tag>}
	// 					{email.verified || <Tag disabled>{t('Not_verified')}</Tag>}
	// 				</Margins>
	// 			</Info>
	// 		</>}

	// 		{ customFields && customFields.map((customField) => <React.Fragment key={customField.label}>
	// 			<Label>{t(customField.label)}</Label>
	// 			<Info>{customField.value}</Info>
	// 		</React.Fragment>) }

	// 		<Label>{t('Created_at')}</Label>
	// 		<Info>{timeAgo(createdAt)}</Info>

	// 	</Margins>

	// </VerticalBar.ScrollableContent>;
});

export const Action = ({ icon, label, ...props }) => (
	<Button title={label} {...props} mi='x4'>
		<Icon name={icon} size='x20' mie='x4' />
		{label}
	</Button>
);

UserInfo.Action = Action;

export default UserInfo;
