<html>
    <body>
        <p>Chào ${targetName},</p>

        <p>Đề xuất <a href="${requestUrl}" target="_blank">${requestName}</a> đã bị từ chối bởi ${approverName}.</p>

        <p>${moreContent}</p>
    </body>
</html>