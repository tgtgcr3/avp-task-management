<html>
    <body>
        <p>Chào ${targetName},</p>

        <p>Đề xuất <a href="${requestUrl}" target="_blank">${requestName}</a> đã được chấp nhận bởi ${approverName}.</p>

        <p>${moreContent}</p>
    </body>
</html>