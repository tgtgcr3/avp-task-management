<html>
    <body>
        <p>Chào ${targetName},</p>

        <p>${approverName} đã chuyển tiếp đề xuất <a href="${requestUrl}" target="_blank">${requestName}</a> cho bạn.</p>

        <p>${moreContent}</p>
    </body>
</html>