<html>
    <body>
        <p>Chào ${targetName},</p>

        <p>Đề xuất <a href="${requestUrl}" target="_blank">${requestName}</a> đã bị hủy bởi ${submitterName}.</p>

        <p>${moreContent}</p>
    </body>
</html>