package com.ntoannhan.onestar.onenotification.features.notification.data.model;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import reactor.core.publisher.FluxSink;

@Data
@Accessors(fluent = true)
public class OneFluxSink<T> {

    private FluxSink<T> fluxSink;

    private long createdTime;

    private String appName;

    private static final long MINUTES_6 = 6 * 60 * 1000;

    public OneFluxSink(FluxSink<T> fluxSink) {
        this.fluxSink = fluxSink;
        this.createdTime = System.currentTimeMillis() + MINUTES_6;
    }

    public boolean isTimeout() {
        return System.currentTimeMillis() > this.createdTime;
    }

    public FluxSink<T> getFluxSink() {
        return fluxSink;
    }

    public void setFluxSink(FluxSink<T> fluxSink) {
        this.fluxSink = fluxSink;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
