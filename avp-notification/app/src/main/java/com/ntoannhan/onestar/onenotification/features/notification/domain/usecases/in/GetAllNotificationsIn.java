package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in;

import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationStatus;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class GetAllNotificationsIn {

    private NotificationStatus notificationStatus;

    private int page;

    private int size;

}
