package com.ntoannhan.onestar.onenotification.features.notification.data.repositories.projection;

public interface BasicNotificationData {

    public Long getId();

    public String getTitle();

    public String getShortContent();

    public Long getCreatedDate();

    public int getNotificationStatus();
}
