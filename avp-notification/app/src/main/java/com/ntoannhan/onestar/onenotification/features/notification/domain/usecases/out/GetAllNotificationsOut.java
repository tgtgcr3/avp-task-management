package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.out;

import com.ntoannhan.onestar.one.core.data.entities.ResultGetAllData;;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.Notification;
import lombok.Builder;
import lombok.Data;

@Data
public class GetAllNotificationsOut extends ResultGetAllData<Notification> {


}
