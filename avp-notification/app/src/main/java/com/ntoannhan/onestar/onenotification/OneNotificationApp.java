package com.ntoannhan.onestar.onenotification;

import com.ntoannhan.onestar.one.core.resolver.TenantKeycloakConfigResolver;
import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableLoadTimeWeaving;
import org.springframework.context.annotation.Primary;

@SpringBootApplication
@EnableLoadTimeWeaving(aspectjWeaving = EnableLoadTimeWeaving.AspectJWeaving.ENABLED)
@ComponentScan({"com.ntoannhan.onestar.onenotification", "com.ntoannhan.onestar.one.core"})
public class OneNotificationApp {

    private final TenantKeycloakConfigResolver keycloakConfigResolver;

    public OneNotificationApp(TenantKeycloakConfigResolver keycloakConfigResolver) {
        this.keycloakConfigResolver = keycloakConfigResolver;
    }

    public static void main(String[] args) {
        SpringApplication.run(OneNotificationApp.class, args);
    }

    @Bean
    @Primary
    public KeycloakConfigResolver keycloakConfigResolver(KeycloakSpringBootProperties properties) {
        return keycloakConfigResolver;
    }

    @Bean
    @Primary
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
