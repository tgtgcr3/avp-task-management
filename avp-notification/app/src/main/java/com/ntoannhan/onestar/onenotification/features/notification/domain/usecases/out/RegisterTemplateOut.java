package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.out;

import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationTemplate;

public class RegisterTemplateOut extends NotificationTemplate {

    public static RegisterTemplateOut fromDomain(NotificationTemplate notificationTemplate) {
        RegisterTemplateOut out = new RegisterTemplateOut();
        out.id(notificationTemplate.id());
        out.appName(notificationTemplate.appName());
        out.category(notificationTemplate.category());
        out.notificationType(notificationTemplate.notificationType());
        out.template(notificationTemplate.template());
        return out;
    }

}
