package com.ntoannhan.onestar.onenotification.features.notification.domain.entities;

public enum NotificationType {

    Event(0), Email(1);

    private final int value;

    NotificationType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static NotificationType fromId(int id) {
        NotificationType[] values = values();
        for (NotificationType requestStatus : values) {
            if (requestStatus.value == id) {
                return requestStatus;
            }
        }
        return null;
    }

}
