package com.ntoannhan.onestar.onenotification.features.notification.presentation.rest.handler;

import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors.SendLogoutUserNotification;
import com.ntoannhan.onestar.onenotification.rest.api.InternalNotificationApiDelegate;
import com.ntoannhan.onestar.onenotification.rest.model.BooleanResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import com.ntoannhan.onestar.one.core.presentation.rest.ExceptionTranslator;

@Component
public class InternalNotificationApiDelegatorImpl implements InternalNotificationApiDelegate {

    @Autowired
    private SendLogoutUserNotification sendLogoutUserNotification;

    @Override
    public ResponseEntity<BooleanResponse> sendLogoutNotification(String username) {
        return sendLogoutUserNotification.call(username).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                }, result -> ResponseEntity.ok(new BooleanResponse().result(result))
        );
    }
}
