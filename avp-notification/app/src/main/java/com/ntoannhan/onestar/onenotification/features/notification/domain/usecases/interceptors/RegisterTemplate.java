package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors;

import com.ntoannhan.onestar.one.core.usecases.UseCase;
import com.ntoannhan.onestar.onenotification.features.notification.domain.services.NotificationService;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in.RegisterTemplateIn;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.out.RegisterTemplateOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class RegisterTemplate implements UseCase<RegisterTemplateOut, RegisterTemplateIn> {

    @Autowired
    @Qualifier("data-notification-service")
    private NotificationService notificationService;

    @Override
    public Either<Exception, RegisterTemplateOut> call(RegisterTemplateIn in) {
        return notificationService.createNotificationTemplate(in).fold(Either::left, notificationTemplate -> Either.right(RegisterTemplateOut.fromDomain(notificationTemplate)));
    }
}
