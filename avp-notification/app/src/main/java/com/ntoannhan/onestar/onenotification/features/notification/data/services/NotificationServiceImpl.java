package com.ntoannhan.onestar.onenotification.features.notification.data.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ntoannhan.onestar.one.core.context.TenantContext;
import com.ntoannhan.onestar.one.core.data.entities.ResultGetAllData;
import com.ntoannhan.onestar.one.core.exception.AppException;
import com.ntoannhan.onestar.one.core.exception.UserException;
import com.ntoannhan.onestar.one.core.mapper.OneMapper;
import com.ntoannhan.onestar.onenotification.features.notification.data.model.OneFluxSink;
import com.ntoannhan.onestar.onenotification.features.notification.data.repositories.entities.NotificationEntity;
import com.ntoannhan.onestar.onenotification.features.notification.data.repositories.entities.NotificationTemplateEntity;
import com.ntoannhan.onestar.onenotification.features.notification.data.repositories.jpa.NotificationJpa;
import com.ntoannhan.onestar.onenotification.features.notification.data.repositories.jpa.NotificationTemplateJpa;
import com.ntoannhan.onestar.onenotification.features.notification.data.repositories.projection.BasicNotificationData;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.Notification;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationStatus;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationTemplate;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationType;
import com.ntoannhan.onestar.onenotification.features.notification.domain.services.NotificationService;
import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import reactor.core.publisher.Flux;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Log4j2
@Component("data-notification-service")
public class NotificationServiceImpl implements NotificationService {

    private final NotificationTemplateJpa notificationTemplateJpa;

    private final ModelMapper modelMapper;

    private final NotificationJpa notificationJpa;

    private final JavaMailSender emailSender;

    private final Map<String, List<OneFluxSink<ServerSentEvent<String>>>> m_emitterMap = new ConcurrentHashMap<>();

    private static final Object m_syncObj = new Object();

    private final Queue<Notification> notificationQueues = new ArrayDeque<Notification>();

    private static final Object syncObj = new Object();

    private static boolean started = false;

    private FreeMarkerConfigurer m_freeMarkerConfig;

    public NotificationServiceImpl(NotificationTemplateJpa notificationTemplateJpa, ModelMapper modelMapper, NotificationJpa notificationJpa, JavaMailSender emailSender) {
        this.notificationTemplateJpa = notificationTemplateJpa;
        this.modelMapper = modelMapper;
        this.notificationJpa = notificationJpa;
        this.emailSender = emailSender;
    }

    @PostConstruct
    public void postConstruct() {
        modelMapper.addConverter(notificationTypeIntegerConverter);
        modelMapper.addConverter(intToNotificationTypeConverter);
        modelMapper.addConverter(notificationStatusIntegerConverter);
        modelMapper.addConverter(intToNotificationStatusConverter);

        modelMapper.addMappings(new EntityToDomainNotificationMapping());
        modelMapper.addMappings(new DomainToEntityNotificationMapping());

        this.m_freeMarkerConfig = createFreeMarkerConfig();
    }

    private FreeMarkerConfigurer createFreeMarkerConfig() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_29);
        TemplateLoader templateLoader = new ClassTemplateLoader(this.getClass(), "/templates");
        configuration.setTemplateLoader(templateLoader);

        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setConfiguration(configuration);
        return freeMarkerConfigurer;
    }

    @Override
    public Either<Exception, NotificationTemplate> createNotificationTemplate(NotificationTemplate notificationTemplate) {
        return createOrUpdateEntity(null, notificationTemplate).fold(Either::left, entity -> {
            notificationTemplate.id(entity.getId());
            return Either.right(notificationTemplate);
        });
    }

    @Override
    public Either<Exception, Flux<ServerSentEvent<String>>> createNotificationEmitter(String username) {
        log.debug("CreateNotificationEmitter for {}", username);
        List<OneFluxSink<ServerSentEvent<String>>> emitters = m_emitterMap.get(username);
        if (emitters == null) {
            synchronized (m_emitterMap) {
                emitters = m_emitterMap.computeIfAbsent(username, k -> new ArrayList<>());
            }
        }
        synchronized (m_syncObj) {
            final List<OneFluxSink<ServerSentEvent<String>>> obj = emitters;
            log.debug("number of emitters: {} -> {}", obj.size(), TenantContext.getExtraData("X-APP-ID"));
            Flux<ServerSentEvent<String>> sseEmitter = Flux.create(s -> obj.add(new OneFluxSink<>(s).appName(TenantContext.getExtraData("X-APP-ID"))));
//            sseEmitter.timeout(Duration.of(1, ChronoUnit.MINUTES));
            if (obj.size() > 1) {
                log.debug("Try to remove timeout emitter");
                removeTimeoutEmitter(obj);
            }
            return Either.right(sseEmitter);
        }

    }

    private List<OneFluxSink<ServerSentEvent<String>>> removeTimeoutEmitter(List<OneFluxSink<ServerSentEvent<String>>> fluxSinks) {
        try {
            List<OneFluxSink<ServerSentEvent<String>>> timeoutEvents = fluxSinks.stream().filter(OneFluxSink::isTimeout).collect(Collectors.toList());
            fluxSinks.removeAll(timeoutEvents);
            log.debug("number of timeout: " + timeoutEvents.size() + ", try to terminate it");
            try {
                timeoutEvents.forEach(s -> {
                    try {
                        s.getFluxSink().complete();
                    } catch (Exception ex) {
                    }
                });
            } catch (Exception ex) {
                log.error("cannot complete all timeoutEvent", ex);
            }
            return fluxSinks;
        } catch (Exception ex) {
            log.error("Error when remove timeout sinks", ex);
        }
        return fluxSinks;
    }

    @Override
    public Either<Exception, Boolean> sendLogoutNotification(String username) {
        ServerSentEvent<String> event = ServerSentEvent.<String>builder().id("action").event("logout").data(username).build();
        return sendSSEEvent(username, event, null);
    }

    private Either<Exception, Boolean> sendSSEEvent(String username, ServerSentEvent<String> event, String appName) {
        List<OneFluxSink<ServerSentEvent<String>>> fluxSinks = m_emitterMap.get(username);
        if (fluxSinks != null) {
            synchronized (m_syncObj) {
                if (fluxSinks.size() > 0) {
                    removeTimeoutEmitter(fluxSinks);
                    fluxSinks = fluxSinks.stream().collect(Collectors.toList());
                    log.debug("Number of event: " + fluxSinks.size());
                    log.debug("aaaaaaaaa: send SSE event to {}-{}", username, appName);
                    fluxSinks.forEach(s -> {
                                try {
                                    s.getFluxSink().next(event);
                                } catch (Exception ex) {
                                    log.error("Cannot send to event: ", ex);
                                }
                            }
                    );
                }
            }
            return Either.right(Boolean.TRUE);
        } else {
            return Either.left(new UserException("username_not_login"));
        }
    }

    @Override
    public Either<Exception, Boolean> sendNotification(List<Notification> notification) {
        if (notification.isEmpty()) {
            return Either.left(new UserException("Notifications is empty"));
        }
        notification.forEach(n -> n.setAppName(TenantContext.getExtraData("X-APP-ID")));
        notificationQueues.addAll(notification);
        startNotificationThread();
        return Either.right(true);
    }

    @Override
    public Either<Exception, ResultGetAllData<Notification>> getAllNotifications(NotificationStatus notificationStatus, int page, int size) {
        try {
            log.debug("Get all notification");
            Integer status = notificationStatus != null ? notificationStatus.getValue() : null;
            List<BasicNotificationData> basicDatas = notificationJpa.findAllByStatus(status, TenantContext.getUsername(),
                    PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id")));
            int total = notificationJpa.countAllByStatus(status);

            return Either.right(new ResultGetAllData<Notification>().total(total)
                    .items(basicDatas.stream().map(bs -> new Notification()
                            .id(bs.getId())
                            .title(bs.getTitle())
                            .notificationStatus(NotificationStatus.fromId(bs.getNotificationStatus()))
                            .shortContent(bs.getShortContent())
                            .date(bs.getCreatedDate())
                    ).collect(Collectors.toList()))
            );
        } catch (Exception ex) {
            log.error("Cannot retrive Notification", ex);
            return Either.left(new AppException("cannot_process_request"));
        }
    }

    @Override
    public Either<Exception, Boolean> updateNotificationStatus(List<Long> ids, NotificationStatus status) {
        try {
            int numOfStatus = notificationJpa.updateNotificationStatus(ids, status.getValue());
            return Either.right(Boolean.TRUE);
//            Optional<NotificationEntity> notificationOpt = notificationJpa.findById(id);
//            if (notificationOpt.isPresent()) {
//                NotificationEntity notification = notificationOpt.get();
//                notification.setNotificationStatus(status.getValue());
//                notificationJpa.save(notification);
//                return Either.right(Boolean.TRUE);
//            }
//            return Either.left(new UserException("notification_not_found"));
        } catch (Exception ex) {
            log.error("Cannot update notification status", ex);
            return Either.left(new AppException("cannot_process_request"));
        }
    }

    private void startNotificationThread() {
        if (!started) {
            log.info("Notification Thread not started");
            synchronized (syncObj) {
                if (!started) {
                    log.info("Notification Thread not started two");
                    started = true;
                    startThread();
                } else {
                    log.info("Notification thread already started");
                }

            }
        } else {
            log.info("Notification thread already started two");
        }
    }

    private void startThread() {
        log.info("Trying to start thread: aaaaaaaaaaaaaaaaaaaaaaaa");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Before going to while: {}", started);
                while (started) {
                    Notification notification = null;
                    try {
                        log.info("");
                        notification = notificationQueues.remove();
                        log.info("processing notification: " + notification.getTitle());
                        processNotification(notification);
                        log.info("processed notification: " + notification.getTitle());
                    } catch (Exception ex) {
                        log.error("Cannot process the notificaiton: {}", notification != null ? notification.getTitle() : "null", ex);
                    }

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (notificationQueues.isEmpty()) {
                        log.info("Queue is empty");
                        synchronized (syncObj) {
                            if (notificationQueues.isEmpty()) {
                                log.info("Queue is empty two");
                                started = false;
                                return;
                            } else {
                                log.info("query is not empty");
                            }
                        }
                    }
                }
            }
        });
        thread.start();
        log.info("Thread started: aaaaaaaaaaaaaaaaaaaaaaaa");
    }

    private void processNotification(Notification notification) {
        log.info("process notfication {}", notification.getId());
        NotificationEntity notificationEntity = OneMapper.mapTo(notification, NotificationEntity.class);
        notificationEntity = notificationJpa.save(notificationEntity);
        notification.setId(notificationEntity.getId());
        notification.setDate(notificationEntity.getCreatedDate());
        log.debug("Delivery request {} - {} - {}", notification.getTitle(), notification.getAppName(), notification.getNotificationType());
        deliverNotification(notification);
    }

    private void deliverNotification(Notification notification) {
        Notification cloneNotification = OneMapper.mapTo(notification, Notification.class);
        prepareData(cloneNotification);
        if (notification.getNotificationType() == NotificationType.Event) {

            sendSSENotificationEvent(cloneNotification);
        } else if (notification.getNotificationType() == NotificationType.Email) {
            sendEmailNotification(cloneNotification);
        }
    }

    private void sendSSENotificationEvent(Notification notification) {
        ObjectMapper objectMapper = new ObjectMapper();
        ServerSentEvent<String> event = null;
        try {
            event = ServerSentEvent.<String>builder().id("request").event("notification").data(
                    objectMapper.writeValueAsString(Map.of(
                            "id", notification.getId(),
                            "title", notification.getTitle(),
                            "shortContent", notification.getShortContent(),
                            "date", notification.getDate()
                    ))
            ).build();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        log.debug("bbbb: Send SSE Event request {} - {} - {} - {}", notification.getTitle(), notification.getAppName(), notification.getNotificationType(), event.id());
        sendSSEEvent(notification.getTarget(), event, notification.appName());
    }

    private void sendEmailNotification(Notification notification) {
        MimeMessage message = emailSender.createMimeMessage();
        try {
            log.debug("trying to send email {} to {}", notification.getTitle(), notification.getTargetEmail());
            message.setSubject(notification.getTitle());

            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(notification.getTargetEmail());
            helper.setFrom("no-reply@anvietenergy.com");
            helper.setText(notification.getLongContent(), true);
            emailSender.send(message);
        } catch (MessagingException e) {
            log.error("Cannot send email to {}", notification.getTargetEmail());
        }

//        SimpleMailMessage message = new SimpleMailMessage();

    }


    private void prepareData(Notification notification) {
        if (notification.getNotificationType() == NotificationType.Event) {
            notification.setAppName(null);
        } else {
            Template template = null;
            String templateFullname = notification.getAppName() + "_" + notification.getLongContent() + "_email.ftl";
            try {
                template = m_freeMarkerConfig.getConfiguration().getTemplate(templateFullname);
                notification.setLongContent(FreeMarkerTemplateUtils.processTemplateIntoString(template, notification.getData()));
            } catch (IOException | TemplateException e) {
                log.error("Cannot export using template: {} ", templateFullname, e);
            }
        }
    }

    private Either<Exception, NotificationTemplateEntity> createOrUpdateEntity(Long id, NotificationTemplate notificationTemplate) {
        NotificationTemplateEntity entity = null;
        if (id != null) {
            Optional<NotificationTemplateEntity> optional = notificationTemplateJpa.findById(id);
            if (optional.isEmpty()) {
                return Either.left(new UserException("notification_template_not_found"));
            }
            entity = optional.get();
        }
        if (entity != null) {
            updateEntityData(notificationTemplate, entity);
        } else {
            entity = convertNotificationTemplateToEntity(notificationTemplate);
        }

        try {
            notificationTemplateJpa.save(entity);
            return Either.right(entity);
        } catch (Exception ex) {
            log.error("Cannot SaveOrUpdate NotificationTemplate", ex);
            return Either.left(new AppException("cannot_create_notification_template"));
        }
    }

    private void updateEntityData(NotificationTemplate notificationTemplate, NotificationTemplateEntity entity) {
        entity.setAppName(notificationTemplate.appName());
        entity.setCategory(notificationTemplate.category());
        entity.setNotificationType(notificationTemplate.notificationType());
        entity.setTemplate(notificationTemplate.template());
    }

    private NotificationTemplateEntity convertNotificationTemplateToEntity(NotificationTemplate notificationTemplate) {
        NotificationTemplateEntity entity = new NotificationTemplateEntity();
        entity.setId(notificationTemplate.id());
        entity.setAppName(notificationTemplate.appName());
        entity.setCategory(notificationTemplate.category());
        entity.setNotificationType(notificationTemplate.notificationType());
        entity.setTemplate(notificationTemplate.template());
        return entity;
    }

    public static Converter<NotificationType, Integer> notificationTypeIntegerConverter = new Converter<NotificationType, Integer>() {
        @Override
        public Integer convert(MappingContext<NotificationType, Integer> mappingContext) {
            if (mappingContext.getSource() != null) {
                return mappingContext.getSource().getValue();
            }
            return NotificationType.Event.getValue();
        }
    };

    public static Converter<Integer, NotificationType> intToNotificationTypeConverter = new Converter<Integer, NotificationType>() {
        @Override
        public NotificationType convert(MappingContext<Integer, NotificationType> mappingContext) {
            return NotificationType.fromId(mappingContext.getSource());
        }
    };

    public static Converter<NotificationStatus, Integer> notificationStatusIntegerConverter = new Converter<NotificationStatus, Integer>() {
        @Override
        public Integer convert(MappingContext<NotificationStatus, Integer> mappingContext) {
            if (mappingContext.getSource() != null) {
                return mappingContext.getSource().getValue();
            }
            return NotificationStatus.Unread.getValue();
        }
    };

    public static Converter<Integer, NotificationStatus> intToNotificationStatusConverter = new Converter<Integer, NotificationStatus>() {
        @Override
        public NotificationStatus convert(MappingContext<Integer, NotificationStatus> mappingContext) {
            return NotificationStatus.fromId(mappingContext.getSource());
        }
    };

    static class EntityToDomainNotificationMapping extends PropertyMap<Notification, NotificationEntity> {

        @Override
        protected void configure() {

            NotificationEntity map = map();
            try {
                if (map != null) {
                    map.setNotificationStatus(this.<Integer>source("notificationStatus"));
                    map.setNotificationType(this.source("notificationType"));
                }
            } catch (Exception ex) {
            }

        }
    }

    static class DomainToEntityNotificationMapping extends PropertyMap<NotificationEntity, Notification> {

        @Override
        protected void configure() {
            Notification map = map();
            try {
                if (map != null) {
                    map.setNotificationType(this.source("notificationType"));
                    map.setNotificationStatus(this.source("notificationStatus"));
                }
            } catch (Exception ex) {

            }

        }
    }
}
