package com.ntoannhan.onestar.onenotification.features.notification.domain.entities;

public enum NotificationStatus {
    Unread(0), Read(1), Pending(0), Delivered(3), Deleted(4);

    private final int value;

    NotificationStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static NotificationStatus fromId(int id) {
        NotificationStatus[] values = values();
        for (NotificationStatus requestStatus : values) {
            if (requestStatus.value == id) {
                return requestStatus;
            }
        }
        return null;
    }
}
