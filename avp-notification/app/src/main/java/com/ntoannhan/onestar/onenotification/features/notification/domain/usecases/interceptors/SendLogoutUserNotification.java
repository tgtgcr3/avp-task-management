package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors;

import com.ntoannhan.onestar.one.core.usecases.UseCase;
import com.ntoannhan.onestar.onenotification.features.notification.domain.services.NotificationService;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SendLogoutUserNotification implements UseCase<Boolean, String> {

    @Autowired
    private NotificationService notificationService;

    @Override
    public Either<Exception, Boolean> call(String in) {
        return notificationService.sendLogoutNotification(in);
    }
}
