package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors;

import com.ntoannhan.onestar.one.core.usecases.UseCase;
import com.ntoannhan.onestar.onenotification.features.notification.domain.services.NotificationService;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in.UpdateNotificationStatusIn;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.out.UpdateNotificationStatusOut;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class UpdateNotificationStatus implements UseCase<UpdateNotificationStatusOut, UpdateNotificationStatusIn> {

    private final NotificationService notificationService;

    public UpdateNotificationStatus(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public Either<Exception, UpdateNotificationStatusOut> call(UpdateNotificationStatusIn in) {
        return notificationService.updateNotificationStatus(in.ids(), in.status()).fold(Either::left, out -> Either.right(new UpdateNotificationStatusOut().result(out)));
    }
}
