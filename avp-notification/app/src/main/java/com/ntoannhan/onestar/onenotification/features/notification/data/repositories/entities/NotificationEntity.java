package com.ntoannhan.onestar.onenotification.features.notification.data.repositories.entities;


import com.ntoannhan.onestar.one.core.data.repositories.entities.AbstractBaseEntity;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationStatus;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationType;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity(name = "Notification")
public class NotificationEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String title;

    private String shortContent;

    private String longContent;

    private String data;

    private int notificationType;

    private int notificationStatus;

    private long createdDate;

    private String appName;

    private String target;

    private String targetEmail;

    private String owner;

    private String ownerEmail;

}

