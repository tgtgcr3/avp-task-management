package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.out;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SendNotificationOut {

    private boolean result;

}
