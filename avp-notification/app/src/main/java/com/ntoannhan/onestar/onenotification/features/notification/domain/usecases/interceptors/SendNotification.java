package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors;

import com.ntoannhan.onestar.one.core.usecases.UseCase;
import com.ntoannhan.onestar.onenotification.features.notification.domain.services.NotificationService;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in.SendNotificationIn;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.out.SendNotificationOut;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;

@Component
public class SendNotification implements UseCase<SendNotificationOut, SendNotificationIn> {

    private final NotificationService notificationService;

    public SendNotification(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @RolesAllowed({"app", "admin"})
    @Override
    public Either<Exception, SendNotificationOut> call(SendNotificationIn in) {
        return notificationService.sendNotification(in.getNotifications()).fold(Either::left, result -> Either.right(SendNotificationOut.builder().result(result).build() ));
    }
}
