package com.ntoannhan.onestar.onenotification.features.notification.service;

import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in.RegisterTemplateIn;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors.RegisterTemplate;
import com.ntoannhan.onestar.onenotification.features.notification.presentation.data.NotificationType;
import com.ntoannhan.onestar.onenotification.features.notification.presentation.service.NotificationService;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private RegisterTemplate registerTemplate;

    @Override
    public Either<Exception, Long> registerTemplate(String appName, String category, NotificationType notificationType, String template) {
        return registerTemplate.call((RegisterTemplateIn) (new RegisterTemplateIn().appName(appName).category(category).notificationType(notificationType.getValue()).template(template)))
                .fold(Either::left, notificationTemplate -> Either.right(notificationTemplate.id()));
    }

    @Override
    public Either<Exception, Boolean> sendNotification(Long templateId, Map<String, Object> data) {
        return null;
    }
}
