package com.ntoannhan.onestar.onenotification.features.notification.data.model;

import com.ntoannhan.onestar.onenotification.features.notification.data.repositories.entities.NotificationTemplateEntity;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationTemplate;

public class NotificationTemplateModel extends NotificationTemplate {

    public static NotificationTemplateModel fromEntity(NotificationTemplateEntity entity) {
        NotificationTemplateModel notificationTemplateModel = new NotificationTemplateModel();
        notificationTemplateModel
                .id(entity.getId())
                .appName(entity.getAppName())
                .category(entity.getCategory())
                .notificationType(entity.getNotificationType())
                .template(entity.getTemplate());
        return notificationTemplateModel;
    }

}
