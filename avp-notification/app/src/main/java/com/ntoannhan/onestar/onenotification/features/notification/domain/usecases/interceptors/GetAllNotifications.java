package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors;

import com.ntoannhan.onestar.one.core.usecases.UseCase;
import com.ntoannhan.onestar.onenotification.features.notification.domain.services.NotificationService;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in.GetAllNotificationsIn;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.out.GetAllNotificationsOut;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class GetAllNotifications implements UseCase<GetAllNotificationsOut, GetAllNotificationsIn> {

    private final NotificationService notificationService;

    public GetAllNotifications(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public Either<Exception, GetAllNotificationsOut> call(GetAllNotificationsIn in) {
        return notificationService.getAllNotifications(in.notificationStatus(), in.page(), in.size())
                .fold(Either::left, out -> Either.right((GetAllNotificationsOut) new GetAllNotificationsOut().items(out.items()).total(out.total())));
    }
}
