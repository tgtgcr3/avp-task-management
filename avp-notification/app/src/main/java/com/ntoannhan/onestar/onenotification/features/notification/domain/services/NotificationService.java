package com.ntoannhan.onestar.onenotification.features.notification.domain.services;

import com.ntoannhan.onestar.one.core.data.entities.ResultGetAllData;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.Notification;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationStatus;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationTemplate;
import io.vavr.control.Either;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import reactor.core.publisher.Flux;

import java.util.List;

public interface NotificationService {

    Either<Exception, NotificationTemplate> createNotificationTemplate(NotificationTemplate notificationTemplate);

    Either<Exception, Flux<ServerSentEvent<String>>> createNotificationEmitter(String username);

    Either<Exception, Boolean> sendLogoutNotification(String username);

    Either<Exception, Boolean> sendNotification(List<Notification> notification);

    Either<Exception, ResultGetAllData<Notification>> getAllNotifications(NotificationStatus notificationStatus, int page, int size);

    Either<Exception, Boolean> updateNotificationStatus(List<Long> ids, NotificationStatus status);
}
