package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors;

import com.ntoannhan.onestar.one.core.usecases.UseCase;
import com.ntoannhan.onestar.onenotification.features.notification.domain.services.NotificationService;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;


@Component
public class CreateNotificationEmitter implements UseCase<Flux<ServerSentEvent<String>>, String> {

    @Autowired
    private NotificationService notificationService;

    @Override
    public Either<Exception, Flux<ServerSentEvent<String>>> call(String in) {
        return notificationService.createNotificationEmitter(in);
    }
}
