package com.ntoannhan.onestar.onenotification.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "oneapp")
@Component
public class OneAppProperties {

    private String requestToken;

}
