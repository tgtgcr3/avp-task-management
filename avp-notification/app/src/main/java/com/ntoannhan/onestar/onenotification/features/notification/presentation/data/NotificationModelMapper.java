package com.ntoannhan.onestar.onenotification.features.notification.presentation.data;

import com.ntoannhan.onestar.one.core.mapper.OneMapper;
import com.ntoannhan.onestar.onenotification.rest.model.Notification;
import com.ntoannhan.onestar.onenotification.rest.model.NotificationStatus;
import com.ntoannhan.onestar.onenotification.rest.model.NotificationType;

public class NotificationModelMapper {

    public static Notification toRestModel(com.ntoannhan.onestar.onenotification.features.notification.domain.entities.Notification entity) {
        Notification notification = OneMapper.mapTo(entity, Notification.class);
        notification.setNotificationStatus(toRestStatus(entity.getNotificationStatus()));
        return notification;
    }

    private static NotificationStatus toRestStatus(com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationStatus notificationStatus) {
        if (notificationStatus == null) {
            return null;
        }

        switch (notificationStatus) {
            case Deleted:
                return NotificationStatus.DELETED;
            case Read:
                return NotificationStatus.READ;
            case Unread:
                return NotificationStatus.UNREAD;
        }
        return null;
    }

    public static com.ntoannhan.onestar.onenotification.features.notification.domain.entities.Notification toDomainModel(Notification notification) {
        com.ntoannhan.onestar.onenotification.features.notification.domain.entities.Notification domain = OneMapper.mapTo(notification, com.ntoannhan.onestar.onenotification.features.notification.domain.entities.Notification.class);
        domain.setNotificationType(notification.getNotificationType() == NotificationType.EMAIL ? com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationType.Email : com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationType.Event);
        return domain;
    }

}
