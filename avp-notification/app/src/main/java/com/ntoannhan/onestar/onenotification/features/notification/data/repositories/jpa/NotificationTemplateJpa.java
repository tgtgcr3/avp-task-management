package com.ntoannhan.onestar.onenotification.features.notification.data.repositories.jpa;

import com.ntoannhan.onestar.onenotification.features.notification.data.repositories.entities.NotificationTemplateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface NotificationTemplateJpa extends JpaRepository<NotificationTemplateEntity, Long> {
}
