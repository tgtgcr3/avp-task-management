package com.ntoannhan.onestar.onenotification.features.notification.domain.entities;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class NotificationTemplate {

    private Long id;

    private String appName;

    private String category;

    private int notificationType;

    private String template;

}
