package com.ntoannhan.onestar.onenotification.features.notification.presentation.data;

public enum NotificationType {

    Email(1), SSE(2);

    private final int value;

    NotificationType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
