package com.ntoannhan.onestar.onenotification.features.notification.presentation.rest.handler;

import com.ntoannhan.onestar.one.core.presentation.rest.ExceptionTranslator;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationStatus;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in.GetAllNotificationsIn;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in.SendNotificationIn;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in.UpdateNotificationStatusIn;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors.GetAllNotifications;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors.SendNotification;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors.UpdateNotificationStatus;
import com.ntoannhan.onestar.onenotification.features.notification.presentation.data.NotificationModelMapper;
import com.ntoannhan.onestar.onenotification.features.notification.presentation.data.RestModelMapper;
import com.ntoannhan.onestar.onenotification.rest.api.NotificationApiDelegate;
import com.ntoannhan.onestar.onenotification.rest.model.BooleanResponse;
import com.ntoannhan.onestar.onenotification.rest.model.Notification;
import com.ntoannhan.onestar.onenotification.rest.model.NotificationQueryResult;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class NotificationApiDelegateImpl implements NotificationApiDelegate {

    private final SendNotification sendNotification;

    private final GetAllNotifications getAllNotifications;

    private final UpdateNotificationStatus updateNotificationStatus;

    public NotificationApiDelegateImpl(SendNotification sendNotification, GetAllNotifications getAllNotifications, UpdateNotificationStatus updateNotificationStatus) {
        this.sendNotification = sendNotification;
        this.getAllNotifications = getAllNotifications;
        this.updateNotificationStatus = updateNotificationStatus;
    }

    @Override
    public ResponseEntity<Notification> sendNotification(List<Notification> notification) {
        return sendNotification.call(SendNotificationIn.builder().notifications(notification.stream().map(n -> NotificationModelMapper.toDomainModel(n)).collect(Collectors.toList())).build()).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                }, out -> ResponseEntity.ok(notification.get(0))
        );
    }

    @Override
    public ResponseEntity<NotificationQueryResult> getAllNotifications(String status, Integer page, Integer size) {
        return getAllNotifications.call(
                new GetAllNotificationsIn().page(page).size(size)
                .notificationStatus(getDomainNotificationStatus(status))
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                }, out -> ResponseEntity.ok(new NotificationQueryResult()
                        .total(out.total())
                        .items(out.items().stream().map(n -> NotificationModelMapper.toRestModel(n)).collect(Collectors.toList()))
                )
        );
    }

    @Override
    public ResponseEntity<BooleanResponse> setNotificationStatus(Long id, String status) {
        return updateNotificationStatus.call(new UpdateNotificationStatusIn().status(getDomainNotificationStatus(status)).ids(Arrays.asList(id)))
                .fold(
                        ex -> {
                            throw ExceptionTranslator.getFailureException(ex);
                        }, out -> ResponseEntity.ok(new BooleanResponse().result(out.result()))

                );
    }

    @Override
    public ResponseEntity<BooleanResponse> setAllNotificationStatus(List<Long> nId, String status) {
        return updateNotificationStatus.call(new UpdateNotificationStatusIn().status(getDomainNotificationStatus(status)).ids(nId))
                .fold(
                        ex -> {
                            throw ExceptionTranslator.getFailureException(ex);
                        }, out -> ResponseEntity.ok(new BooleanResponse().result(out.result()))

                );
    }

    private NotificationStatus getDomainNotificationStatus(com.ntoannhan.onestar.onenotification.rest.model.NotificationStatus status) {
        if (status == null) {
            return null;
        }
        switch (status) {
            case READ:
                return NotificationStatus.Read;
            case UNREAD:
                return NotificationStatus.Unread;
            case DELETED:
                return NotificationStatus.Deleted;
        }
        return null;
    }

    private NotificationStatus getDomainNotificationStatus(String status) {
        if (status == null) {
            return null;
        }
        if (status.equalsIgnoreCase("read")) {
            return NotificationStatus.Read;
        }
        if (status.equalsIgnoreCase("unread")) {
            return NotificationStatus.Unread;
        }
        if (status.equalsIgnoreCase("deleted")) {
            return NotificationStatus.Deleted;
        }
        return null;
    }

}
