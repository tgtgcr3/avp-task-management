package com.ntoannhan.onestar.onenotification.features.notification.data.repositories.jpa;

import com.ntoannhan.onestar.onenotification.features.notification.data.repositories.entities.NotificationEntity;
import com.ntoannhan.onestar.onenotification.features.notification.data.repositories.projection.BasicNotificationData;
import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.Notification;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Component
public interface NotificationJpa extends JpaRepository<NotificationEntity, Long> {

    @Override
    @Query("SELECT n FROM Notification n WHERE n.id = :id")
    Optional<NotificationEntity> findById(Long id);

    @Query("SELECT n.id as id, n.createdDate as createdDate, n.title as title, n.shortContent as shortContent, n.notificationStatus as notificationStatus " +
            "FROM Notification n WHERE n.notificationType = 0 AND ((:status IS NULL AND n.notificationStatus <> 4) OR n.notificationStatus = :status) AND n.target = :currentUser")
    List<BasicNotificationData> findAllByStatus(Integer status, String currentUser, Pageable pageable);

    @Query("SELECT COUNT(n.id) " +
            "FROM Notification n WHERE n.notificationType = 0 AND ((:status IS NULL AND n.notificationStatus <> 4) OR n.notificationStatus = :status)")
    int countAllByStatus(Integer status);

    @Transactional
    @Modifying
    @Query("UPDATE Notification n SET n.notificationStatus = :status WHERE n.id in :ids")
    int updateNotificationStatus(List<Long> ids, int status);
}
