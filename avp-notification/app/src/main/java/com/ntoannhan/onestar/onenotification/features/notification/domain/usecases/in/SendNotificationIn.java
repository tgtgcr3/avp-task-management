package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in;

import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.Notification;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SendNotificationIn {

    private List<Notification> notifications;

}
