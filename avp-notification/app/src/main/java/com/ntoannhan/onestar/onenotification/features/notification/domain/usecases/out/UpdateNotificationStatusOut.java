package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.out;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class UpdateNotificationStatusOut {

    private boolean result;

}
