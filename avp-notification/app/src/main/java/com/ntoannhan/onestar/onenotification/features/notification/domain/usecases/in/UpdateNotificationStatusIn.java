package com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.in;

import com.ntoannhan.onestar.onenotification.features.notification.domain.entities.NotificationStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class UpdateNotificationStatusIn {

    private List<Long> ids;

    private NotificationStatus status;

}
