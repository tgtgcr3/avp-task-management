package com.ntoannhan.onestar.onenotification.core.config;

import com.ntoannhan.onestar.one.core.interceptors.OneContextRequestInterceptor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class OneConfig {

    private final OneContextRequestInterceptor oneContextRequestInterceptor;

    public OneConfig(OneContextRequestInterceptor oneContextRequestInterceptor) {
        this.oneContextRequestInterceptor = oneContextRequestInterceptor;

    }

    @PostConstruct
    public void postConstruct() {
        oneContextRequestInterceptor.addCustomHeader("X-APP-ID");
    }
}
