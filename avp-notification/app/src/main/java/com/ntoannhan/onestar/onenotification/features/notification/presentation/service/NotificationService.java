package com.ntoannhan.onestar.onenotification.features.notification.presentation.service;

import com.ntoannhan.onestar.onenotification.features.notification.presentation.data.NotificationType;
import io.vavr.control.Either;

import java.util.Map;

public interface NotificationService {

    Either<Exception, Long> registerTemplate(String appName, String category, NotificationType notificationType, String template);

    Either<Exception, Boolean> sendNotification(Long templateId, Map<String, Object> data);

}
