package com.ntoannhan.onestar.onenotification.core.config;

import com.ntoannhan.onestar.onenotification.core.config.OneAppProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

@Component
public class HeaderAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private OneAppProperties oneAppProperties;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String oneHeader = request.getHeader("X-ONE-HEADER");
        String appName = request.getHeader("X-APP-ID");

        if (oneHeader != null && oneHeader.trim().length() > 0 && appName != null && !appName.trim().isEmpty()) {
            String token = null;
            if ("request".equals(appName)) {
                token = oneAppProperties.getRequestToken();
            }
            if (oneHeader.equals(token)) {
                User user = findByToken(oneHeader);
                final UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(request, response);
    }

    private User findByToken(String token) {
        return new User(
                "internal",
                "password",
                true,
                true,
                true,
                true,
                Arrays.asList(new SimpleGrantedAuthority("ROLE_app"), new SimpleGrantedAuthority("ROLE_user")));


    }
}
