package com.ntoannhan.onestar.onenotification.features.notification.presentation.rest.handler;

import com.ntoannhan.onestar.onenotification.rest.api.FileApiDelegate;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Log4j2
@Component
public class FileApiDelegateImpl implements FileApiDelegate {

    @Autowired
    private HttpServletRequest m_request;


    @Override
    public ResponseEntity<Resource> download(String fileId) {
        if ("api.yml".equals(fileId)) {
            return downloadApi(fileId);
        }

        return null;

    }




    private ResponseEntity<Resource> downloadApi(String fileId) {
        Resource resource = new ClassPathResource("/" + fileId);
        if (!resource.exists()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "not found");
        }
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = m_request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

}
