package com.ntoannhan.onestar.onenotification.features.notification.presentation.rest.handler;

import com.ntoannhan.onestar.one.core.presentation.rest.ExceptionTranslator;
import com.ntoannhan.onestar.onenotification.features.notification.domain.usecases.interceptors.CreateNotificationEmitter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.LocalTime;

@Controller("/api/pri")
public class NotificationSSEHandler {

    @Autowired
    private CreateNotificationEmitter createNotificationEmitter;

    @GetMapping("/api/pri/logout-sse")
    public Flux<ServerSentEvent<String>> streamEvents(String username) {
        return createNotificationEmitter.call(username)
                .fold(ex -> {throw ExceptionTranslator.getFailureException(ex);},
        emitter -> emitter);
    }

}
