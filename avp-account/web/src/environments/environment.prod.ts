export const environment = {
  production: true,
  idUrl: 'http://sso.anvietenergy.com:9999',
  baseUrl: 'http://account.anvietenergy.com:4200',
  oneMessage: 'http://message.anvietenergy.com:4400',
  oneWework: 'http://task.anvietenergy.com/avp',
  clientId: 'avp-account',
  clientSecret: '1d637943-21c0-4c01-ace4-19840dc2f0da',
  oneMessageWs: 'ws://message.anvietenergy.com:4400',
  defaultAvatar: 'http://account.anvietenergy.com:4200/api/pri/file/download/default_avatar.png'
};
