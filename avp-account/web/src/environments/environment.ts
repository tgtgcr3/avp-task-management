// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// export const environment = {
//   production: false,
//   idUrl: 'http://sso.anvietenergy.com:9999',
//   baseUrl: 'http://account.anvietenergy.com:4200',
//   oneMessage: 'http://message.anvietenergy.com:4400',
//   oneWework: 'http://task.anvietenergy.com/avp',
//   clientId: 'avp-account',
//   clientSecret: '1d637943-21c0-4c01-ace4-19840dc2f0da',
//   oneMessageWs: 'ws://message.anvietenergy.com:4400',
//   defaultAvatar: 'http://account.anvietenergy.com:4200/api/pri/file/download/default_avatar.png'
// };

// export const environment = {
//   production: false,
//   idUrl: 'https://sso.ntoannhan.xyz',
//   baseUrl: 'https://account.ntoannhan.xyz',
//   oneMessage: 'http://192.168.12.51:3000',
//   oneWework: 'http://task.anvietenergy.com/avp',
//   clientId: 'avp-account',
//   clientSecret: '1d637943-21c0-4c01-ace4-19840dc2f0da',
//   oneMessageWs: 'ws://message.anvietenergy.com:4400',
//   defaultAvatar: 'https://account.ntoannhan.xyz/api/pri/file/download/default_avatar.png'
// };
//
export const environment = {
  production: false,
  idUrl: 'https://sso.ntoannhan.xyz',
  baseUrl: 'http://192.168.12.2:4200',
  oneMessage: 'http://192.168.12.51:3000',
  oneWework: 'http://task.anvietenergy.com/avp',
  clientId: 'avp-account',
  clientSecret: '1d637943-21c0-4c01-ace4-19840dc2f0da',
  oneMessageWs: 'ws://message.anvietenergy.com:4400',
  defaultAvatar: 'https://account.ntoannhan.xyz/api/pri/file/download/default_avatar.png'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
