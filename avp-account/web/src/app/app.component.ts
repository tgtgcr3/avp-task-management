import { Subscription } from 'rxjs';
import { SseService } from './core/service/sse-service';
import { KeyCloakService } from './core/service/keycloak.service';
import { Component, OnInit } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import { SecurityService } from './core/service/security.service';
import {ActionType, ActivityService, ActivitySource} from "../generated/web-api";
import { HttpClient } from '@angular/common/http';

declare var RocketChat: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'web';

  isLogedIn: boolean = false;
  userInfo: any = {};

  userId = '';


  //chatAdapter: ChatAdapter;

  constructor(
    private translate: TranslateService,
    private securityService: SecurityService,
    // public fileAdapter: MiniChatFileUploadAdapter,
    private keycloakService: KeyCloakService,
    private sseService: SseService,
    private activityLogService: ActivityService,
    private http: HttpClient
  ) {
    translate.setDefaultLang('vi-VN');
  }
  ngOnInit(): void {

    this.securityService.prepare().then(() => {
      this.isLogedIn = this.securityService.isLogedIn();
      if (this.isLogedIn == true) {
        let userInfo = this.securityService.getUserInfo();
        let username = this.keycloakService.getUsername(userInfo);
        this.keycloakService.findUserByUsername(username).toPromise().then(users => {
          this.userInfo = users.find(u => this.keycloakService.getUsername(u) == username);
        });  //this.securityService.getUserInfo();
        //console.log(this.userInfo);
        this.userId = username;
        if (username != null) {
          this.listenForLogout(username);
        }
        let lastAuthTime = localStorage.getItem('lastAuthTime');
        let currentAuthTime = userInfo.auth_time;
        if (lastAuthTime == null || lastAuthTime != currentAuthTime) {
          this.http.get('https://api.ipify.org?format=json').toPromise().then((data: any) => {
            this.sendActivityLogs(username, data.ip, currentAuthTime);
          }).catch((err:any) => {
            this.sendActivityLogs(username, null, currentAuthTime);
          });
        }


      } else {
        this.securityService.login();
      }
    });
  }

  sendActivityLogs(username, publicIp, currentAuthTime) {
    this.activityLogService.createActivity({
      user: username,
      action: ActionType.Login,
      source: ActivitySource.Account,
      localId: null,
      publicIp: publicIp
    }).toPromise().then(() => {
      localStorage.setItem('lastAuthTime', currentAuthTime);
    });
  }

  sseObservable: Subscription;

  private listenForLogout(username: string): void {
    this.sseObservable = this.sseService.getServerSentEvent("/sse/api/pri/logout-sse?username=" + username).subscribe(event => {
      console.log(event);
      if (event && event.data == username) {
        try {
          this.sseObservable.unsubscribe();
        } catch(ex) {
        }
        alert('Tài khoản bạn đã được thoát. Hệ thống sẽ chuyển đến trang đăng nhập để đăng nhập lại');
        setTimeout(() => {
          this.securityService.logout();
        });
      }
    }, (error: any) => {
      try {
        this.sseObservable.unsubscribe();
      } catch(ex) {
      }
      setTimeout(() => {
        this.listenForLogout(username);
      }, 1000);
    });
  }
}
