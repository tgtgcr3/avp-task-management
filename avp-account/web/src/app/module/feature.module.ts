import { OneMemberListComponent } from './member/component/memberList/memberList.component';
import { OneMiniChatComponent } from './messaging/mini-chat/mini-chat.component';
import { MessagingModule } from './messaging/messaging.module';
import {NgModule} from "@angular/core";
import {FeatureRoute} from "./feature.route";
import {MatButtonModule} from "@angular/material/button";
import {MemberModule} from "./member/member.module";
import {PersonalModule} from "./personal/personal.module";
import {LandingPageComponent} from "./landingPage/landingPage.component";
import {ManagementModule} from "./management/management.module";
import { GuestMemberModule } from "./guestMember/guestMember.module";
import {ActivityLogComponent} from "./activity-log/activity-log.component";
import {ActivityLogModule} from "./activity-log/activity-log.module";

@NgModule({
  imports: [
    FeatureRoute,
    MemberModule,
    PersonalModule,
    GuestMemberModule,
    ManagementModule,
    MatButtonModule,
    MessagingModule,
    ActivityLogModule
  ],
  declarations: [
    LandingPageComponent
  ],
  exports: [
    OneMiniChatComponent
  ]
})

export class FeatureModule {}
