import {ActivityLogComponent} from "./activity-log.component";
import {MatButtonModule} from "@angular/material/button";
import {NgModule} from "@angular/core";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatTableModule} from "@angular/material/table";
import {CommonModule} from "@angular/common";
import {ComponentModule} from "../../core/component/component.module";
import {NgxMatSelectSearchModule} from "ngx-mat-select-search";
import {MatSelectModule} from "@angular/material/select";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatTabsModule} from "@angular/material/tabs";


@NgModule({
  imports: [
    MatButtonModule,
    MatProgressSpinnerModule,
    MatTableModule,
    CommonModule,
    ComponentModule,
    NgxMatSelectSearchModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatIconModule,
    MatPaginatorModule,
    MatInputModule,
    MatDatepickerModule,
    MatTabsModule
  ],
  declarations: [
    ActivityLogComponent
  ],
  exports: [
  ]
})

export class ActivityLogModule {}
