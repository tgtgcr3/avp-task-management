import {Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from "@angular/core";
import {
  ActionType,
  ActivityLog,
  ActivityLogGetAllResult,
  ActivityService,
  ActivitySource,
  KeyCloakUserService, Project, ProjectService
} from "../../../generated/web-api";
import {KeyCloakService} from "../../core/service/keycloak.service";
import {forkJoin, ReplaySubject, Subject} from "rxjs";
import {formatDate} from "@angular/common";
import {FormControl} from "@angular/forms";
import {takeUntil} from "rxjs/operators";
import * as transliterate from "@sindresorhus/transliterate";

import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {environment} from "../../../environments/environment";
import {MatPaginator} from "@angular/material/paginator";

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'activity-log',
  templateUrl: './activity-log.component.html',
  styleUrls: ['./activity-log.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
  encapsulation: ViewEncapsulation.None
})

export class ActivityLogComponent implements OnInit, OnDestroy {

  activityLogs: MyActivityLog[] = [];

  isLoading = false;

  displayedColumns: string[] = ['userInfo', 'appInfo', 'actionInfo', 'descriptionInfo'];

  users: UserInfoData[] = [];

  appIcons: any = {};

  actionName: any = {};

  protected _onDestroy = new Subject<void>();

  filteredUsers:  ReplaySubject<UserInfoData[]> = new ReplaySubject<UserInfoData[]>(1);

  userFilterCtr: FormControl = new FormControl();
  userSelectFormCtr: FormControl = new FormControl();

  filterData = {
    username: null,
    application: null,
    action: null,
    fromDate: null,
    toDate: null,
    filterName: null,
    projectId: null
  }

  paginationData: any = {
    resultLength: 0,
    pageSize: 25,
    pageIndex: 0
  }

  pageSizeOptions = [5, 25, 50, 100];

  @ViewChild('paginator')
  paginator: MatPaginator;

  appDatas = [
    {
      type: ActivitySource.Account,
      icon: '/assets/images/account.png'
    },
    {
      type: ActivitySource.Task,
      icon: '/assets/images/wework.png'
    },
    {
      type: ActivitySource.Message,
      icon: '/assets/images/message.png'
    },
    {
      type: ActivitySource.Request,
      icon: '/assets/images/request.png'
    }
  ];

  appSelectFormCtr: FormControl = new FormControl();

  actionDatas = [
    {
      type: ActionType.Login,
      name: ActionType.Login
    },
    {
      type: ActionType.CreateTask,
      name: ActionType.CreateTask.replace('_', ' ')
    },
    {
      type: ActionType.UpdateTask,
      name: ActionType.UpdateTask.replace('_', ' ')
    },
    {
      type: ActionType.DeleteTask,
      name: ActionType.DeleteTask.replace('_', ' ')
    },
    {
      type: ActionType.CreateProject,
      name: ActionType.CreateProject.replace('_', ' ')
    },
    {
      type: ActionType.UpdateProject,
      name: ActionType.UpdateProject.replace('_', ' ')
    },
    {
      type: ActionType.DeleteProject,
      name: ActionType.DeleteProject.replace('_', ' ')
    }
  ];

  actionSelectFormCtr: FormControl = new FormControl();

  startDateFormControl: FormControl = new FormControl();
  toDateFormControl: FormControl = new FormControl();

  projects: ProjectData[] = [];
  filteredProjects: ReplaySubject<ProjectData[]> = new ReplaySubject<ProjectData[]>(1);
  projectSelectFormCtr: FormControl = new FormControl();
  projectFilterFormCtr: FormControl = new FormControl();

  constructor(
    private keycloakService: KeyCloakUserService,
    private activityLogService: ActivityService,
    private iKeycloakService: KeyCloakService,
    private projectService: ProjectService
  ) {
    this.appIcons[ActivitySource.Account] = '/assets/images/account.png';
    this.appIcons[ActivitySource.Task] = '/assets/images/wework.png';
    this.appIcons[ActivitySource.Message] = '/assets/images/message.png';
    this.appIcons[ActivitySource.Request] = '/assets/images/request.png';
  }


  ngOnInit(): void {

    this.isLoading = true;
    forkJoin([this.activityLogService.getActivityLogs('General'), this.keycloakService.getUsers()]).toPromise().then((res: [ActivityLogGetAllResult, any]) => {
      this.users = res[1].items.map(u => {
        let fullname = this.iKeycloakService.getFullName(u);
        return {
          username: this.iKeycloakService.getUsername(u),
          fullName: fullname,
          avatar: this.iKeycloakService.getAvatar(u),
          title: this.iKeycloakService.getTitle(u),
          fullNameSearch: transliterate(fullname.toLowerCase()),
          firstName: u.firstName,
          lastName: u.lastName
        }
      }).sort((a: UserInfoData, b: UserInfoData) => {
        let result = a.firstName.localeCompare(b.firstName);
        if (result == 0) {
          return a.lastName.localeCompare(b.lastName);
        }
        return result;
      });
      this.filteredUsers.next(this.users.slice())
      this.activityLogs = res[0].items.map(a => {
        return {
          ...a,
          userData: this.users.find(u => u.username == a.user)?? {
            username: a.user,
            fullName: a.user,
            avatar: environment.defaultAvatar,
            title: a.user,
            fullNameSearch: a.user,
            firstName: a.user,
            lastName: a.user
          },
          dateString: formatDate(a.date, 'HH:mm dd-MM-yyyy', 'en-US'),
          appIconUrl: this.appIcons[a.source],
          actionName: a.action.replace('_', ' ')
        }
      });
      this.paginationData.resultLength = res[0].total;
    }).finally(() => {
      this.isLoading = false;
    })

    this.userFilterCtr.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
      this.filterUsers();
    });
    this.userSelectFormCtr.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe((value:UserInfoData) => {
      this.setFilterValue('username', value?.username);
    });

    this.appSelectFormCtr.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe((value: any) => {
      this.setFilterValue('application', value?.type.toUpperCase());
    });

    this.actionSelectFormCtr.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe((value: any) => {
      this.setFilterValue('action', value?.type.toUpperCase());
    });

    this.startDateFormControl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe((value: any) => {
      this.setFilterValue('fromDate', value?.valueOf(), (value != null && this.filterData.toDate != null) || (value == null && this.filterData.toDate))
    });

    this.toDateFormControl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe((value: any) => {
      this.setFilterValue('toDate', value?.valueOf(), (value != null && this.filterData.fromDate != null) || (value == null && this.filterData.fromDate != null))
    });

    this.projectFilterFormCtr.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe((name: string) => {
      this.filterProjects();
    })

    this.projectSelectFormCtr.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe((project: ProjectData) => {
      this.setFilterValue('projectId', project?.id);
    });

    if (this.iKeycloakService.isAdmin() == false) {
      window.location.href = environment.baseUrl;
    }
  }

  setFilterValue(attrName, attrValue, needToUpdate?: boolean) {
    this.filterData[attrName] = attrValue;
    if (needToUpdate == null || needToUpdate == true) {
      if (this.paginationData.pageIndex > 0) {
        this.paginator.firstPage();
      } else {
        this.applyFilter();
      }

    }
  }

  applyFilter() {
    this.isLoading = true;
    let startDate: number = null;
    let toDate: number = null;
    if (this.filterData.fromDate != null && this.filterData.toDate != null) {
      startDate = this.filterData.fromDate;
      toDate = this.filterData.toDate;
    }
    this.activityLogService.getActivityLogs(this.filterData.filterName ?? 'General',this.filterData.username, this.paginationData.pageIndex, this.paginationData.pageSize,
      this.filterData.action, this.filterData.application,
      startDate, toDate,
      this.filterData.projectId
    ).toPromise().then((result: ActivityLogGetAllResult) => {
      this.activityLogs = result.items.map(a => {
        return {
          ...a,
          userData: this.users.find(u => u.username == a.user) ?? {
            username: a.user,
            fullName: a.user,
            avatar: environment.defaultAvatar,
            title: a.user,
            fullNameSearch: a.user,
            firstName: a.user,
            lastName: a.user
          },
          dateString: formatDate(a.date, 'HH:mm dd-MM-yyyy', 'en-US'),
          appIconUrl: this.appIcons[a.source],
          actionName: a.action.replace('_', ' ')
        }
      });
      this.paginationData.resultLength = result.total;
    }).finally(() => this.isLoading = false);
  }

  clearSelectedUser() {
    this.userSelectFormCtr.setValue(null);
  }

  clearSelectedApp() {
    this.appSelectFormCtr.setValue(null);
  }

  clearSelectedAction() {
    this.actionSelectFormCtr.setValue(null);
  }

  clearSelectedDate() {
    this.toDateFormControl.setValue(null);
    this.startDateFormControl.setValue(null);
    this.paginationData.pageIndex = 0;
  }

  selectTab(event) {
    let index = event.index;
    let appName = null;

    this.displayedColumns = ['userInfo', 'appInfo', 'actionInfo', 'descriptionInfo'];
    this.filterData.action = null;
    this.filterData.username = null;
    this.filterData.projectId = null;
    switch (index) {
      case 1:
        appName = ActivitySource.Account;
        this.displayedColumns = ['userInfo', 'actionInfo', 'descriptionInfo'];
        break;
      case 2:
        this.displayedColumns = ['userInfo', 'projectInfo', 'actionInfo', 'descriptionInfo'];
        appName = ActivitySource.Task;
        break;
      case 3:
        appName = ActivitySource.Request;
        this.displayedColumns = ['userInfo', 'actionInfo', 'descriptionInfo'];
        break;
      case 4:
        appName = ActivitySource.Message;
        this.displayedColumns = ['userInfo', 'actionInfo', 'descriptionInfo'];
        break;
    }

    this.filterData.application = appName?.toUpperCase();
    if (index == 2) {
      this.projectService.getAllProjects('').toPromise().then((projects: Project[]) => {
        this.projects = projects.map(p => {
          return {
            ...p,
            nameSearch: transliterate(p.name?.toLowerCase())
          }
        });
        this.filteredProjects.next(this.projects.slice());
      });
      this.setFilterValue('filterName', 'Task')
    } else {
      this.setFilterValue('filterName', 'General')
    }
  }

  filterUsers() {
    if (!this.users) {
      return;
    }

    let search = this.userFilterCtr.value;
    if (!search) {
      this.filteredUsers.next(this.users.slice());
      return;
    } else {
      search = transliterate(search.toLowerCase());
    }

    this.filteredUsers.next(
      this.users.filter(u => u.fullNameSearch.includes(search))
    )
  }

  filterProjects() {
    if (!this.projects || this.projects.length == 0) {
      return;
    }

    let search = this.projectFilterFormCtr.value;
    if (!search) {
      this.filteredProjects.next(this.projects.slice());
      return;
    } else {
      search = transliterate(search.toLowerCase());
    }

    this.filteredProjects.next(
      this.projects.filter(p => p.nameSearch.includes(search))
    )
  }

  clearSelectedProject() {
    this.projectSelectFormCtr.setValue(null);
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  onPageChanged(pageData) {
    this.paginationData.pageSize = pageData.pageSize;
    this.paginationData.pageIndex = pageData.pageIndex;
    this.applyFilter();
  }
}

interface MyActivityLog extends ActivityLog {
  userData: UserInfoData,
  dateString: string,
  appIconUrl: string,
  actionName: string
}

interface UserInfoData {
  username: string,
  fullName: string,
  avatar: string,
  title: string,
  fullNameSearch: string,
  lastName: string,
  firstName: string
}

interface ProjectData extends Project {
  nameSearch: string
}

