import {Component, OnInit} from "@angular/core";
import {SecurityService} from "../../core/service/security.service";

@Component({
  selector: 'landing-page-component',
  templateUrl: './landingPage.component.html',
  styleUrls: ['./landingPage.component.scss']
})

export class LandingPageComponent implements OnInit {

  isLogin = false;

  constructor(private securityService: SecurityService) {

  }




  ngOnInit(): void {
    this.isLogin = this.securityService.isLogedIn();
 //   this.securityService.login();
  }

  login() {
    this.securityService.login();
  }

}
