import {Component, OnInit} from "@angular/core";
import {Subject} from "rxjs";

@Component({
  selector: 'one-member-age',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})

export class MemberPageComponent implements OnInit {

  searchSubject: Subject<string> = new Subject<string>();

  eventSubject: Subject<string> = new Subject<string>();

  actionObject: Subject<string> = new Subject<string>();

  ngOnInit(): void {
  }

  onSearchChanged(newText) {
    this.searchSubject.next(newText);
  }

  onReceiveEvent(eventData) {
    this.eventSubject.next(eventData);
  }

  onActionChange(eventData) {
    this.actionObject.next(eventData);
  }
}
