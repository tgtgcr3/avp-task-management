import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {MemberCreationComponent} from "../memberCreation/memberCreation.component";
import {KeyCloakService} from "../../../../core/service/keycloak.service";
import { Observable } from "rxjs";

@Component({
  selector: 'one-member-list-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class OneMemberListHeader implements OnInit {

  @Output()
  searchChanged: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  eventAction: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  actionEvents: Observable<string>;

  @ViewChild('searchInput')
  searchInput: ElementRef;

  currentFilter: string = 'all';

  isAdmin: boolean = false;

  allTotal: string = '';
  managerTotal: string = '';
  onlineTotal: string = '';
  disableTotal: string = '';

  constructor(
    private dialog: MatDialog,
    private keycloak: KeyCloakService
  ) {
  }

  ngOnInit(): void {
    this.isAdmin = this.keycloak.isAdmin();
    this.actionEvents?.subscribe((data) => {
      if (data.indexOf('all:') == 0) {
        this.allTotal = data.replace('all:', '').trim();
      }
      if (data.indexOf('manager:') == 0) {
        this.managerTotal = data.replace('manager:', '').trim();
      }
      if (data.indexOf('online:') == 0) {
        this.onlineTotal = data.replace('online:', '').trim();
      }
      if (data.indexOf('disabled:') == 0) {
        this.disableTotal = data.replace('disabled:', '').trim();
      }
      if (data.indexOf('action:') == 0) {
        if (data.indexOf('clearSearch') > 0) {
          this.searchInput.nativeElement.value = '';
        }
      }
    });
  }

  createUserClick() {
    const dialogRef = this.dialog.open(MemberCreationComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.eventAction.emit('reload');
    });
  }

  onSearchChanged(newText) {

    this.searchChanged.emit(newText);
  }

  filterMembers(type: string) {
    this.currentFilter = type;
    this.searchInput.nativeElement.value = '';
    this.eventAction.emit(`filter:${type}`);
  }
}

