import { UserTitle } from './../../../../../generated/web-api/model/userTitle';
import { environment } from './../../../../../environments/environment';
import { MyErrorStateMatcher } from './../../../../core/service/default.error-matcher';
import { TranslateService } from '@ngx-translate/core';
import { KeyCloakUserService } from './../../../../../generated/web-api/api/keyCloakUser.service';
import { UserData } from './../../../../../generated/web-api/model/userData';
import {Component, ElementRef, OnInit, ViewChild, Inject} from "@angular/core";
import {KeyCloakService} from "../../../../core/service/keycloak.service";
import {Group, UserData as kUserData} from "../../../../../generated/keycloak-api";
import {UserData as sUserData} from '../../../../../generated/web-api';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MustMatch} from "../../../../core/helper/formHelper";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FileService, UserService, UserTitleData, UserTitleService} from "../../../../../generated/web-api";
import {MatAutocompleteSelectedEvent} from "@angular/material/autocomplete";
import {MatChipInputEvent} from "@angular/material/chips";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {forkJoin, Observable, ReplaySubject, Subject} from "rxjs";
import {map, startWith, takeUntil} from "rxjs/operators";
import * as transliterate from '@sindresorhus/transliterate';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'member-creation-one-component',
  templateUrl: './memberCreation.component.html',
  styleUrls: [
    './memberCreation.component.scss'
  ]
})

export class MemberCreationComponent implements OnInit {

  userData: kUserData = {};
  currentUsers: kUserData[] = [];
  userTitles: UserTitleData[] = [];
  submitted: boolean = false;
  registerForm: FormGroup;
  directMgmtCtrl: FormControl = new FormControl();

  currentFile: any;
  avatarUrl: any;

  groups: Group[] = [];

  directMgmts: any[] = [];
  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredDirectMgmts: Observable<any[]>;
  @ViewChild('fileUpload', {static: false}) fileUpload: ElementRef;
  @ViewChild('directMgmtInput') directMgmtInput: ElementRef<HTMLInputElement>;

  currentUserInfo: kUserData;
  currentGroups: Group[];
  currentUserData: sUserData;

  editMode: boolean = false;

  isAdmin: boolean = false;

  isOwner: boolean = false;

  isEmployee: boolean = true;

  isExternal: boolean = false;

  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();

  avatarUpdated: boolean = false;

  isSubmitting: boolean = false;

  public userTitleFilterFctl: FormControl = new FormControl();

  protected _onDestroy = new Subject<void>();

  public filteredUserTitles: ReplaySubject<UserTitle[]> = new ReplaySubject<UserTitle[]>(1)

  constructor(
    private keycloak: KeyCloakService,
    private userTitleService: UserTitleService,
    private keycloakUser: KeyCloakUserService,
    private userService: UserService,
    private fileService: FileService,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<MemberCreationComponent>,
    private translationService: TranslateService,
    private datePipe: DatePipe,
    @Inject(MAT_DIALOG_DATA) private dialogData: any
  ) {

    if (dialogData) {
      this.currentUserInfo = dialogData.userInfo;
      this.currentGroups = dialogData.groups;
      this.currentUserData = dialogData.userData || {
        username: this.getUsername()
      };
    }
    if (this.currentUserInfo != null && this.currentUserInfo.username != null) {
      this.editMode = true;
    }
    this.filteredDirectMgmts = this.directMgmtCtrl.valueChanges.pipe(
      startWith(null),
      map((user: UserData | null) => user ? this._filter(user) : this._getCurrentUsers())
    );
  }

  ngOnInit(): void {
    this.keycloak.getAvailableGroups().then((r) => {
      this.groups = r.filter(g => g.name.toLowerCase() != 'guest');
    }).catch((err) => console.log(err));

    this.userTitleService.getAllUserTitles().toPromise().then(res => {
      this.userTitles = res.items;
      this.filteredUserTitles.next(this.userTitles.slice());
    });

    this.userTitleFilterFctl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
      this.filterUserTitles();
    })

    const userInfo = this.currentUserInfo || {};
    const userData = this.currentUserData || {
      birthday: null,
      directMgmts: null
    };

    if (userInfo.attributes != null) {
      this.isExternal = userInfo.attributes['LDAP_ID'] != null;
    }

    this.avatarUrl = (userInfo.attributes != null && userInfo.attributes['avp_avatar']) ? (userInfo.attributes['avp_avatar'][0] || '') : environment.defaultAvatar;
    this.avatarUrl = this.avatarUrl || environment.defaultAvatar;
    const phoneNumber = (userInfo.attributes != null && userInfo.attributes['avp_number'] != null) ? userInfo.attributes['avp_number'] : '';
    let username = this.getUsername();
    let today = new Date();
    this.registerForm = this.formBuilder.group({
      username: [username || '', Validators.required],
      firstName: [userInfo.firstName || this.datePipe.transform(today, 'yyyy_MM_dd_HH_mm_ss'), Validators.required],
      lastName: [userInfo.lastName || 'Người Dùng', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      userTitle: [this.getUserTitle(userInfo) || ''],
      email: [userInfo.email || '', [Validators.required, Validators.email]],
      group: [this.getUserGroup(), []],
      birthday: [userData.birthday ? new Date(userData.birthday) : '', []],
      number: [phoneNumber, []]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

    this.isAdmin = this.keycloak.isAdmin();
    this.isOwner = this.keycloak.isOwner();
    if (this.editMode == true) {
      this.keycloak.getAvailableGroups().then(groups => {
        let guest = groups.find(g => g.name.toLowerCase() == 'guest');
        this.keycloak.getUsersOfGroup(guest.id).toPromise().then(users => {
          this.isEmployee = users.find(u => this.keycloak.getUsername(u) == username) == null;
        });
      });
    }
    if (this.isAdmin || this.isOwner) {
      this.keycloak.getAllUsers(null, false, null, 0, 10240).toPromise().then(users => {
        this.currentUsers = users.map(u => {
          let clone:MyKUserData = { ...u,
          avatarUrl: this.getAvatarUrlFromKUser(u),
          fullName: this.getFullName(u)
        };
          return clone;
        });
        if (userData.directMgmts != null) {
          this.directMgmts = userData.directMgmts.split(',').filter(a => a.length > 0).map(username => users.find(u => u.username == username));
        }
      });
    }
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getUserTitle(userdata): string {
    if (userdata.attributes && userdata.attributes['avp_title']) {
      return userdata.attributes['avp_title'][0];
    }
    return null;
  }

  filterUserTitles() {
    if (!this.userTitles) {
      return;
    }
    let search = this.userTitleFilterFctl.value;
    if (!search) {
      this.filteredUserTitles.next(this.userTitles.slice());
      return;
    }  else {
      search = transliterate(search).toLowerCase();
    }
    this.filteredUserTitles.next(this.userTitles.filter(ut => transliterate(ut.name).toLowerCase().indexOf(search) >= 0));
  }

  private getUserGroup() {
    if (this.currentGroups != null && this.currentGroups.length > 0) {
      return this.keycloak.getGroupOfUser(this.currentGroups).id;
    }
    this.keycloak.getAvailableGroups().then(us => {
      this.registerForm.controls.group.setValue(us.find(g => g.name.toLowerCase() == "user").id);
      //return us.find(g => g.name.toLowerCase() == "user").id;
    });
    return '';
  }

  get f() { return this.registerForm.controls; }

  private markControlDirty() {
    this.registerForm.controls.username.markAsTouched();
    this.registerForm.controls.firstName.markAsTouched();
    this.registerForm.controls.lastName.markAsTouched();
    this.registerForm.controls.password.markAsTouched();
    this.registerForm.controls.confirmPassword.markAsDirty();
    this.registerForm.controls.email.markAsDirty();
  }

  submitForm() {
    this.markControlDirty();


    this.submitted = true;


    if (this.isEmployee == false) {
      if (this.editMode == true) {
        this.updateGuest();
      }
    } else {
      this.saveOrUpdateEmployee();
    }

  }

  private updateGuest() {
    if (this.registerForm.controls.email.invalid ||
      this.registerForm.controls.firstName.invalid ||
      this.registerForm.controls.lastName.invalid
      ) {
      return;
      }
      this.isSubmitting = true;
      let userData = {
        email: this.registerForm.value.email,
        firstName: this.registerForm.value.firstName,
        lastName: this.registerForm.value.lastName,
        username: this.registerForm.value.username,
        attributes: {
          avp_number: this.registerForm.value.number != null && this.registerForm.value.number.length == 1 ? this.registerForm.value.number[0] : this.registerForm.value.number
        },
        emailVerified: true,
        enabled: true,

      };
      const userId = this.currentUserInfo.id;
      if (this.keycloak.isAdmin()) {
      forkJoin(
        this.keycloak.updateUser(userId, userData)
      ).toPromise().then((r) => {
        this.isSubmitting = false;
        this.dialogRef.close(true);

      }, (err: any) => {
        this.isSubmitting = false;
        if (this.editMode) {
          alert('Có lỗi trong quá trình cập nhập thông tin. Vui lòng thử lại sau');
        } else {
          alert('Có lỗi trong quá trình tạo người dùng. Vui lòng thử lại sau');
        }

      });
    }

  }

  private saveOrUpdateEmployee() {
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      if (this.editMode == true) {
        if (this.registerForm.controls.email.invalid ||
          this.registerForm.controls.firstName.invalid ||
          this.registerForm.controls.lastName.invalid ||
          this.registerForm.controls.userTitle.invalid ||
          this.registerForm.controls.birthday.invalid
          )
          return;
      } else {
      return;
      }
    }
    this.isSubmitting = true;
    let userData = {
      email: this.registerForm.value.email,
      firstName: this.registerForm.value.firstName,
      lastName: this.registerForm.value.lastName,
      username: this.registerForm.value.username,
      attributes: {
        avp_avatar: '',
        avp_title: this.registerForm.value.userTitle,
        avp_number: this.registerForm.value.number != null && this.registerForm.value.number.length == 1 ? this.registerForm.value.number[0] : this.registerForm.value.number
      },
      emailVerified: true,
      enabled: true

    };

    if (this.croppedImage != null && this.croppedImage != '') {
      this.fileService.upload(this.croppedImage).toPromise().then((r) => {
        userData.attributes.avp_avatar = r.url;
        if (this.editMode == true) {
          if (this.currentUserData != null) {
            this.currentUserData.avatarUrl = r.url;
            this.currentUserData.email = userData.email;
          }
          this.updateUser(userData);
        } else {
        this.createUser(userData);
        }
      }).catch(err => {
        this.isSubmitting = false;
        alert('Có lỗi trong quá trình tải hình ảnh lên. Vui lòng thử lại sau');
      });
    } else {
      if (this.avatarUrl != '' && this.avatarUrl != null) {
        userData.attributes.avp_avatar = this.avatarUrl;
        if (this.currentUserData != null) {
        this.currentUserData.avatarUrl = this.avatarUrl;
        this.currentUserData.email = userData.email;
        }
      }

      if (this.editMode == true) {
        this.updateUser(userData);
      } else {
      this.createUser(userData);
      }
    }
  }

  private updateUser(userData: any) {
    const userId = this.currentUserInfo.id;
    this.currentUserData.birthday = (this.registerForm.value.birthday != null && this.registerForm.value.birthday != '') ? this.registerForm.value.birthday.getTime() : null;
    if (this.isAdmin || this.isOwner) {
      this.currentUserData.directMgmts = this.directMgmts.map(userData => userData.username).join(',')
    }
    if (this.keycloak.isAdmin()) {
    forkJoin(
      this.keycloak.updateUser(userId, userData),
      this.userService.createUser(this.currentUserData),
      this.keycloak.assignGroup(userId, this.registerForm.value.group),
      this.keycloakUser.updateKeyCloakUser(userId, {
        id: userId,
    firstName: userData.firstName,
    lastName: userData.lastName,
    phoneNumber: userData.attributes.avp_number,
    avatar: userData.attributes.avp_avatar,
    email: userData.email
      })
    ).toPromise().then((r) => {
      this.isSubmitting = false;
      this.dialogRef.close(true);
    }, (err: any) => {
      this.isSubmitting = false;
      alert('Có lỗi trong quá trình cập nhập thông tin. Vui lòng thử lại sau');
    });
  } else {
    forkJoin(
      this.keycloakUser.updateKeyCloakUser(userId, {
        id: userId,
    firstName: userData.firstName,
    lastName: userData.lastName,
    phoneNumber: userData.attributes.avp_number,
    avatar: userData.attributes.avp_avatar
      }),
      this.userService.createUser(this.currentUserData)
    ).toPromise().then((r) => {
      this.isSubmitting = false;
      this.dialogRef.close(true);
    }).catch((err) => {
      this.isSubmitting = false;
      alert('Có lỗi trong quá trình cập nhật thông tin. Vui lòng thử lại sau');
    });
  }

  }

  private createUser(userData: any) {
    this.keycloak.createUser(userData).toPromise().then((r) => {

      this.keycloak.findUserByUsername(userData.username).toPromise().then((users) => {
        let user = users.find(u => u.username == userData.username);
        this.keycloak.resetPassword(user.id, this.registerForm.value.password).toPromise().then((r) => {
          this.keycloak.assignGroup(user.id, this.registerForm.value.group).then((r) => {
            console.log('assign group');
            console.log(r);
            let birthday = null;
            try {
              birthday = this.registerForm.value.birthday.getTime();
            } catch(ex) {

            }
            this.userService.createUser({
              username: user.username,
              birthday: birthday,
              avatarUrl: userData.attributes.avp_avatar,
              email: userData.email,
              directMgmts: this.directMgmts.map(userData => userData.username).join(','),
              kcId: user.id,
              firstName: user.firstName,
              lastName: user.lastName
            }).toPromise().then((r) => {
              this.isSubmitting = false;
              this.dialogRef.close(true);
            }).catch(err => {
              this.isSubmitting = false;
              alert('Có lỗi trong quá trình lưu dữ liệu. Vui lòng thử lại sau');
            });

          });
        })

      });
    }).catch(err => {
      if (err.error != null) {
        err = err.error;
      }
      this.translationService.get('keycloak.error.' + err.errorMessage.split(' ').join('_')).subscribe((res: string) => {
        this.isSubmitting = false;
        alert(res);
      });
    });
  }

  selectAvatar() {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.click();
  }

  selectedDirectMgmt(event: MatAutocompleteSelectedEvent) {
    this.directMgmts.push(event.option.value);
    this.directMgmtInput.nativeElement.value = '';
    this.directMgmtCtrl.setValue(null);
  }

  trySelectedDirectMgmt(name: MatChipInputEvent) {
  }

  removeDirectManager(directMgmt: any) {
    const index = this.directMgmts.indexOf(directMgmt);
    if (index >= 0) {
      this.directMgmts.splice(index, 1);
    }
  }

  private _filter(user: any): kUserData[] {
    const filterValue = transliterate((user.firstName != null) ? this.getFullName(user).toLowerCase() : user.toString().toLowerCase());

    return this._getCurrentUsers().filter(currentUser => transliterate(this.getFullName(currentUser).toLowerCase()).indexOf(filterValue) >= 0);
  }

  getFullName(user: kUserData): string {
    return `${user.lastName} ${user.firstName}`.trim();
  }

  getAvatarUrlFromKUser(user: kUserData): string {
    return user.attributes != null && user.attributes['avp_avatar'] != null && user.attributes['avp_avatar'].length > 0 ? (user.attributes['avp_avatar'][0] || environment.defaultAvatar) : environment.defaultAvatar ;
  }

  private _getCurrentUsers(): kUserData[] {
    return this.currentUsers.filter((currentUser) => currentUser.enabled && this.directMgmts.find(d => d.id == currentUser.id) == null);
  }

  getUsername() {
    return this.keycloak.getUsername(this.currentUserInfo || {});
  }

  imageCropped(event:any) {
    this.croppedImage = event.base64;
  }

  imageLoaded() {
  }

  cropperReady() {
  }

  loadImageFailed() {
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.avatarUpdated = true;
}

  imageChangedEvent: any = '';
    croppedImage: any = '';
}

interface MyKUserData extends kUserData {
  avatarUrl: string,
  fullName: string
}
