import { environment } from './../../../../../environments/environment';
import { KeyCloakUserService } from './../../../../../generated/web-api/api/keyCloakUser.service';
import { MiniChatAdapter } from './../../../messaging/services/mini-chat.adapter';
import { Group } from 'src/generated/keycloak-api';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from "@angular/core";
import { KeyCloakService } from "../../../../core/service/keycloak.service";
import { forkJoin, Observable, Subscription } from "rxjs";
import { AvatarService, UserService } from "../../../../../generated/web-api";

import { ActivatedRoute, Router } from '@angular/router';
import * as transliterate from '@sindresorhus/transliterate';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'one-member-list',
  templateUrl: './memberList.component.html',
  styleUrls: ['./memberList.component.scss']
})


export class OneMemberListComponent implements OnInit {

  @Input()
  filterMembers: string[]

  @Input()
  searchEvents: Observable<string>;

  @Input()
  actionEvents: Observable<string>;

  @Output()
  actionChange: EventEmitter<string> = new EventEmitter<string>();

  private actionSubscription: Subscription;

  private currentSearch: string = '';

  private eventsSubscription: Subscription;

  isAdmin: boolean = false;

  filteredUSers: any[] = [];
  users: any[] = [];

  displayedColumns: string[] = ['userInfo', 'contact', 'directManager', 'actions'];

  adminGroup: Group;
  ownerGroup: Group;
  guestGroup: Group;

  isLoading = true;

  listLength = 0;
  pageSize = 20;
  pageIndex = 0;

  globalSearch = '';
  filterManager? = false;
  filterOnline? = false;
  filterDisable? = false;

  defaultAvatar: string = '';

  pageSizeOptions: number[] = [5, 10, 25, 100, 500];

  userAvatars: any[] = [];

  constructor(
    private keycloakService: KeyCloakService,
    private userService: UserService,
    private router: Router,
    private chatAdapter: MiniChatAdapter,
    private keyCloakUserService: KeyCloakUserService,
    private avatarsService: AvatarService,
    private changeDetectionRef: ChangeDetectorRef
  ) {
    this.defaultAvatar = environment.defaultAvatar;
  }

  ngOnInit(): void {
    this.keycloakService.getAvailableGroups().then(gs => {
      return gs.forEach(g => {
        if (g.name == 'Admin') {
          this.adminGroup = g;
        }
        if (g.name == 'Owner') {
          this.ownerGroup = g;
        }
        if (g.name.toLowerCase() == 'guest') {
          this.guestGroup = g;
        }
      });
    }).then(r => {
      this._reload();
    });


    this.eventsSubscription = this.searchEvents?.subscribe((newFilter) => {
      this.applySearch(newFilter, this.filterManager, this.filterDisable, this.filterOnline);
    });

    this.actionSubscription = this.actionEvents?.subscribe(action => {
      if (action.indexOf('reload') == 0) {
        this._reload();
      }
      if (action.indexOf('filter') == 0) {
        const filter = action.replace('filter:', '');
        this.currentSearch = '';
        if (filter == 'all') {
          this.applySearch(null, null, null, null);
        } else if (filter == 'manager') {
          this.applySearch(null, true);
        } else if (filter == 'deactivated') {
          this.applySearch(null, null, true);
        } else if (filter == 'online') {
          this.applySearch(null, null, false, true);
        }
      }
    });

    this.isAdmin = this.keycloakService.isAdmin();
    this.chatAdapter.updateFunc = this._countData;
    this.chatAdapter.updateTarget = this;
    this.changeDetectionRef.detach();
  }

  ngOnDestroy(): void {
    try {
      this.chatAdapter.updateFunc = null;
      this.chatAdapter.updateTarget = null;
    } catch (ex) {
    }

  }

  // ngOnChanges(changes: SimpleChanges) {
  //   console.log('aaaaaaaaaa');
  //   this.isAdmin = this.keycloakService.isAdmin();

  // }

  private _reload() {
    this.isLoading = true;

    forkJoin(
      // TODO: Fix later
      this.keyCloakUserService.getUsers(null, null, null, null, null, 0,
        1024),
          this.userService.getAllUsers(),
          this.keycloakService.getUsersOfGroup(this.guestGroup.id),
          this.avatarsService.findAvatars()
        ).toPromise().then(responses => {
          let firstResponse = responses[0] as any;
          let userData = firstResponse.items as any[];
          this.listLength = firstResponse.total;
          let users = responses[1].items;
          let guestUsers = responses[2];
          this.userAvatars = responses[3] as any;
          userData.forEach(u => {
            let user = users.find(us => us.username == u.username);
            u.directManager = [];
            if (user != null) {
              u.directManager = user.directMgmts != null && user.directMgmts.trim() != '' ? user.directMgmts.split(',') : [];
            }
            u.avatar = responses[3][u.username];
          });
          userData = userData.filter(u => guestUsers.find(gu => gu.id == u.id) == null);
          if (this.filterMembers != null) {
            this.users = userData.filter(u => this.filterMembers.indexOf(this.getUsername(u)) >= 0);
          } else {
            this.users = userData;
          }

          this._countData();
          this.applySearch(this.currentSearch, this.filterManager, this.filterDisable, this.filterOnline);
          this.isLoading = false;
        });
  }

  numberOfManagers = -1;
  numberOfDisabled = -1;
  totalUser = 0;
  numberOfOnlineUsers = -1;
  totalEnabledUsers = -1;

  _countData() {
    let needToUpdate:boolean = false;
    const currOnlineUsers = this.chatAdapter.onlineUsernames || [];
    let onlineUsers = currOnlineUsers.length > 0 ? (currOnlineUsers.length) : 0;

    let users = this.users;
    let totalChanged = false;
    let total = users.length;
    if (total != this.totalUser) {
      totalChanged = true;
      this.totalUser = total;
      needToUpdate = true;
    }

    if (this.numberOfOnlineUsers != onlineUsers || totalChanged) {
      this.numberOfOnlineUsers = onlineUsers;
      this.actionChange.emit(`online:(${onlineUsers})`);
      needToUpdate = true;
    }

    forkJoin(
      this.keycloakService.getUsersOfGroup(this.adminGroup.id),
      this.keycloakService.getUsersOfGroup(this.ownerGroup.id),

    ).toPromise().then(arr => {
      const usersInGroup = arr[0].concat(arr[1]);
      let managers = 0;
      let disableds = 0;
      users.forEach((u) => {
        if (usersInGroup.find(gu => this.keycloakService.getUsername(gu) == this.keycloakService.getUsername(u) && gu.enabled == true) != null) {
          managers++;
        }
        if (u.enabled == false) {
          disableds++;
        }
      });
      if (managers != this.numberOfManagers || totalChanged) {
        this.numberOfManagers = managers;
        this.actionChange.emit(`manager:(${managers})`);
        needToUpdate = true;
      }
      if (disableds != this.numberOfDisabled || totalChanged) {
        this.numberOfDisabled = disableds;
        this.actionChange.emit(`disabled:(${disableds})`);
        needToUpdate = true;
      }
      let enabledUser = total - disableds;
      if (enabledUser != this.totalEnabledUsers || totalChanged) {
        this.actionChange.emit(`all:(${enabledUser}/${total})`);
        needToUpdate = true;
      }
      if (needToUpdate) {
        this.changeDetectionRef.detectChanges();
      }
    });
  }

  onPageChanged(pageEvent) {
    this.pageSize = pageEvent.pageSize;
    this.pageIndex = pageEvent.pageIndex;
    this._reload();
  }

  innerSearch(event) {
    let textStr = transliterate(event.target.value.trim().toLowerCase());
    this.filteredUSers = this.users.filter(u => transliterate(this.getFullname(u)).toLowerCase().indexOf(textStr) >= 0);
  }

  private applySearch(fullname?: string, manager?: boolean, userEnabled?: boolean, online?: boolean) {
    this.currentSearch = fullname;
    this.filterManager = manager
    this.filterDisable = userEnabled;
    this.filterOnline = online;

    let filtered = this.users || [];
    if (this.currentSearch == null || this.currentSearch.trim() == '') {
      filtered = this.users;
    } else {
      let text = this.currentSearch.toLowerCase().trim();
      text = transliterate(text);
      filtered = this.users.filter((u) => {
        return transliterate(this.getFullname(u)).toLowerCase().indexOf(text) >= 0;
      });
    }
    if ((this.filterDisable == false || this.filterDisable == null) &&
      (this.filterManager == null || this.filterManager == false) &&
      (this.filterOnline == null || this.filterOnline == false)
    ) {
      filtered = filtered.filter(u => u.enabled == true);
    }
    if (userEnabled == true) {
      filtered = filtered.filter(u => u.enabled == false);
    }
    if (manager != null && manager == true) {
      return forkJoin(
        this.keycloakService.getUsersOfGroup(this.adminGroup.id),
        this.keycloakService.getUsersOfGroup(this.ownerGroup.id),
      ).toPromise().then(arr => {
        const users = arr[0].concat(arr[1]);
        return this.filteredUSers = filtered.filter(u => {
          return users.find(gu => this.keycloakService.getUsername(gu) == this.keycloakService.getUsername(u) && gu.enabled == true) != null;
        });
      });
    }

    if (online == true) {
      const onlineUsers = this.chatAdapter.onlineUsernames || [];
      filtered = filtered.filter(u => onlineUsers.indexOf((u.username || u.preferred_username)) >= 0 && u.enabled == true);
    }
    // if (filtered.length > 0) {
    //   this.filteredUSers = filtered.filter(u => filtered.indexOf(this.keycloakService.getUsername(u)) >= 0);
    // }
    this.filteredUSers = this.keycloakService.sortUser(filtered);
    setTimeout(() => {
      this.changeDetectionRef.detectChanges();
    });

  }

  getAvatarUrlOfUser(username: string): string {
    const user = this.users.find(u => u.username == username);
    return user != null ? user.attributes.avp_avatar : null;
  }

  editUser(user) {
    this.router.navigate(['/userDetails', user.username || user.preferred_username]);
  }

  deactivateUser(user, tobeEnabled) {
    const message  = tobeEnabled ? 'Kích hoạt tài khoản ' + (user.username || user.preferred_username) + '?'
          : `Hủy kích hoạt tài khoản ${user.username || user.preferred_username}?`;
    if (confirm(message)) {
      user.enabled = tobeEnabled;
      let clone = { ...user };
      delete clone.directManager;
      this.keyCloakUserService.enableUser(user.id, tobeEnabled).toPromise().then(r => {
        this._reload();
        this.chatAdapter.loadFriends();
      });

      // this.keycloakService.updateUser(user.id, clone).toPromise().then(r => {
      //   this._reload();
      //   this.chatAdapter.loadFriends();
      // });

    }
  }

  getFullname(user): string {
    return this.keycloakService.getFullName(user);
  }

  getFullNameFromUsername(username) {
    const user = this.users.find(u => u.username == username);
    return user != null ? this.getFullname(user) : null;
  }

  getUsername(userInfo) {
    return this.keycloakService.getUsername(userInfo);
  }

  getRole(userInfo) {
    if (userInfo.groups) {
      let groups = userInfo.groups as string[];
      if (groups.indexOf('Owner') >= 0 || groups.indexOf('owner') >= 0) {
        return 'Owner';
      }
      if (groups.indexOf('Admin') >= 0 || groups.indexOf('admin') >= 0) {
        return 'Admin'
      }
    }
    return '';
  }

  // goFirst() {
  //   this.first = 0;
  //   this.currentPage = 0;
  //   this._reload();
  // }

  // goNext() {
  //   this.currentPage ++;
  //   this.first = this.currentPage * this.max;
  //   this._reload();
  // }

  // goPrevious() {
  //   this.currentPage --;
  //   this.first = this.currentPage *  this.max;
  //   this._reload();
  // }

}
