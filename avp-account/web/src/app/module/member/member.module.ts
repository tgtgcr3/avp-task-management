import { ImageCropperModule } from 'ngx-image-cropper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {NgModule} from "@angular/core";
import {MemberPageComponent} from "./member.component";
import {OneMemberListHeader} from "./component/header/header.component";
import {TranslateModule} from "@ngx-translate/core";
import {MatButtonModule} from "@angular/material/button";
import {MemberCreationComponent} from "./component/memberCreation/memberCreation.component";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {BrowserModule} from "@angular/platform-browser";
import {OneMemberListComponent} from "./component/memberList/memberList.component";
import {MatTableModule} from "@angular/material/table";
import {ComponentModule} from "../../core/component/component.module";
import {ErrorStateMatcher, MatNativeDateModule, ShowOnDirtyErrorStateMatcher} from "@angular/material/core";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIcon, MatIconModule} from "@angular/material/icon";
import {MatCardModule} from "@angular/material/card";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatChipsModule} from "@angular/material/chips";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import { MatTooltipModule } from "@angular/material/tooltip";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {DatePipe} from '@angular/common';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
  imports: [
    TranslateModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    BrowserModule,
    MatTableModule,
    MatIconModule,
    ReactiveFormsModule,
    ComponentModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatTooltipModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    ImageCropperModule,
    MatProgressBarModule,
    NgxMatSelectSearchModule
  ],
  declarations:[
    MemberPageComponent,
    OneMemberListHeader,
    MemberCreationComponent,
    OneMemberListComponent
  ],
  exports:[
    MemberPageComponent,
    OneMemberListComponent,
    MemberCreationComponent
  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher},
    DatePipe
  ]
})

export class MemberModule {}
