
import { Component, OnInit } from '@angular/core';

import { KeyCloakService } from "../../../../core/service/keycloak.service";
import { MatDialogRef } from "@angular/material/dialog";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Group } from 'src/generated/keycloak-api';
import { TranslateService } from '@ngx-translate/core';
@Component({
    selector: 'guest-member-creation',
    templateUrl: './guestMemberCreation.component.html',
    styleUrls: ['./guestMemberCreation.component.scss']
})
export class GuestMemberCreationComponent implements OnInit {

    registerForm: FormGroup;
    submitted: boolean = false;

    guestGroup: Group;

    constructor(
        private keycloak: KeyCloakService,
        private formBuilder: FormBuilder,
        private translationService: TranslateService,
        private dialogRef: MatDialogRef<GuestMemberCreationComponent>


    ) {

    }

    ngOnInit(): void {
        this.keycloak.getAvailableGroups().then((r) => {
            console.log(r);
            this.guestGroup = r.find(g => g.name.toLowerCase() == 'guest');
          }).catch((err) => console.log(err));

        this.registerForm = this.formBuilder.group({
            username: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            phoneNumber: ['']
        });
    }

    get f() { return this.registerForm.controls; }

    submitForm() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        let userData = {
            email: this.registerForm.value.email,
            firstName: this.registerForm.value.firstName,
            lastName: this.registerForm.value.lastName,
            username: this.registerForm.value.username,
            attributes: {
                avp_number: this.registerForm.value.phoneNumber
            },
            emailVerified: true,
            enabled: true,

        };

        this.createUser(userData);


    }

    private createUser(userData: any) {
        this.keycloak.createUser(userData).toPromise().then((r) => {

          this.keycloak.findUserByUsername(userData.username).toPromise().then((users) => {
            let user = users.find(u => u.username == userData.username);
            this.keycloak.assignGroup(user.id, this.guestGroup.id).then((r) => {
                console.log('assign group');
                console.log(r);

                this.dialogRef.close(true);
              });

          });
        }).catch(err => {
          if (err.error != null) {
            err = err.error;
          }
          this.translationService.get('keycloak.error.' + err.errorMessage.split(' ').join('_')).subscribe((res: string) => {
            alert(res);
          });
        });
      }
}
