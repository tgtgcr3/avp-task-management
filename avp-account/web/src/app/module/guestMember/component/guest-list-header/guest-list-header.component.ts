import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { KeyCloakService } from "../../../../core/service/keycloak.service";
import { MatDialog } from "@angular/material/dialog";
import { GuestMemberCreationComponent } from "../guestMemberCreation/guestMemberCreation.component";

@Component({
  selector: 'guest-list-header',
  templateUrl: './guest-list-header.component.html',
  styleUrls: ['./guest-list-header.component.scss']
})
export class GuestListHeaderComponent implements OnInit {

  isAdmin = false;

  @Output()
  searchChanged: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  eventAction: EventEmitter<string> = new EventEmitter<string>();

  currentFilter = 'activate';

  constructor(
    private dialog: MatDialog,
    private keycloak: KeyCloakService
  ) {

  }

  ngOnInit(): void {
    this.isAdmin = this.keycloak.isAdmin();
  }

  createGuestMember() {
    const dialogRef = this.dialog.open(GuestMemberCreationComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.eventAction.emit('refresh');
    });
  }

  filterMembers(type: string) {
    this.currentFilter = type;
    this.eventAction.emit(`filter:${type}`);
  }

  onSearchChanged(newText) {
    this.searchChanged.emit(newText);
  }
}
