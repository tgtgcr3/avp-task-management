import { ReactiveFormsModule } from '@angular/forms';
import { AdminPasswordModalComponent } from './component/password-modal/password-modal.component';
import {NgModule} from "@angular/core";

import {TranslateModule} from "@ngx-translate/core";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {BrowserModule} from "@angular/platform-browser";
import {PersonalHeader} from "./component/header/header.component";
import {PersonalInfoComponent} from "./component/personalInfo/personalInfo.component";
import {PersonalComponent} from "./personal.component";
import {ComponentModule} from "../../core/component/component.module";
import {MatCardModule} from "@angular/material/card";
import {KeyValueEditableTableComponent} from "./component/keyValueEditableTable/keyValueEditableTable.component";
import {MatTableModule} from "@angular/material/table";

@NgModule({
  imports: [
    TranslateModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    BrowserModule,
    ComponentModule,
    MatCardModule,
    MatTableModule,
    ReactiveFormsModule
  ],
  declarations:[
    PersonalHeader,
    PersonalInfoComponent,
    PersonalComponent,
    KeyValueEditableTableComponent,
    AdminPasswordModalComponent
  ],
  exports:[
    PersonalComponent
  ]
})

export class PersonalModule {}
