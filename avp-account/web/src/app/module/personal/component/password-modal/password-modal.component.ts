import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { MustMatch } from 'src/app/core/helper/formHelper';

@Component({
  selector: 'admin-password-modal-component',
  templateUrl: './password-modal.component.html',
  styleUrls: ['./password-modal.component.scss']
})

export class AdminPasswordModalComponent implements OnInit {

  registerForm: FormGroup;

  submitted = false;

  currentUser = '';

  constructor(
    private formBuilder: FormBuilder,
    private keycloakService: KeyCloakService,
    private dialogRef: MatDialogRef<AdminPasswordModalComponent>,
    @Inject(MAT_DIALOG_DATA) private dialogData: any
  ) {

  }

  get f() { return this.registerForm.controls; }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

  }

  submitForm() {
    this.submitted = true;
    if (this.dialogData != null && this.dialogData.userId != null) {
      // stop here if form is invalid
      if (this.registerForm.invalid) {
        return;
      }

      if (this.keycloakService.isAdmin()) {
        let userId = this.dialogData.userId;
        this.keycloakService.resetPassword(userId, this.registerForm.value.password).toPromise().then(r => {
          this.dialogRef.close(true);
        }).catch(e => {
          alert('Không thể đổi mật khẩu');
        })
      } else {
        alert('You are not an admin');
      }
    } else {
      alert('Missing data info');
    }

  }


}
