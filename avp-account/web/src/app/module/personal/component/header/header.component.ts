import { forkJoin } from 'rxjs';
import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import { UserData } from 'src/generated/keycloak-api';
import {KeyCloakService} from "../../../../core/service/keycloak.service";
import { ActivatedRoute, Router } from '@angular/router';
import {MemberCreationComponent} from "../../../member/component/memberCreation/memberCreation.component";
import { UserService } from 'src/generated/web-api';

@Component({
  selector: 'personal-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class PersonalHeader implements OnInit {

  @Input()
  userInfo: UserData = {
    attributes: {}
  };

  isAdmin: boolean = false;

  isMine: boolean = false;

  currentUsername: string = '';

  @Output()
  eventAction: EventEmitter<string> = new EventEmitter<string>();

  private sub: any;

  constructor(
    private dialog: MatDialog,
    private keycloakService: KeyCloakService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
  ) {

    this.sub = this.route.params.subscribe(params => {
      this.isMine = false;
      const me = this.keycloakService.getUserInfo() || {};
      this.currentUsername = params['id'];
      if (params['id'] === 'me') {
        this.isMine = true;
        this.currentUsername = me['username'] || me['preferred_username'];
      } else {
        if (me != null && (me['username'] != null || me['preferred_username'] != null)) {
          this.isMine = params['id'] == (me['username'] || me['preferred_username'])
        }
      }

   });

  }

  ngOnInit(): void {
  //  this.userInfo = this.keycloakService.getUserInfo() || {};
    this.isAdmin = this.keycloakService.isAdmin();
  }

  ngOnChanges(changes: SimpleChanges) {
      this.isAdmin = this.keycloakService.isAdmin();
}


  createUserClick() {
    forkJoin(this.keycloakService.getAllUsers(this.currentUsername, false),
    this.userService.getAllUsers(this.currentUsername)
    ).toPromise().then(data => {
      const allUsers = data[0];
      const allUserDatas = data[1].items;
      const userInfo = allUsers.find(user => (user.username || user.preferred_username) == this.currentUsername);
      const userData = allUserDatas.find(user => user.username == this.currentUsername);
      if (userInfo != null) {
        this.keycloakService.getGroupsByUserId(userInfo.id).toPromise().then(groups => {
          const dialogRef = this.dialog.open(MemberCreationComponent, {
            data: {
              userInfo,
              groups,
              userData
            }

          });

          dialogRef.afterClosed().subscribe(result => {
           if (result == true) {
             if (this.isMine == true) {
               window.location.reload();
             } else {
              this.eventAction.emit('reload');
             }

           }
           this.router.navigate([], {
            skipLocationChange: true,
            queryParamsHandling: 'merge' //== if you need to keep queryParams
          })
          });
        });
      }
    });
  }
}

