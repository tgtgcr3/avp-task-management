import { environment } from './../../../../../environments/environment';
import { UserGroup } from './../../../../../generated/web-api/model/userGroup';
import { UserGroupService } from './../../../../../generated/web-api/api/userGroup.service';
import { GroupService } from './../../../../../generated/keycloak-api/api/group.service';

import { AdminPasswordModalComponent } from './../password-modal/password-modal.component';

import { Component, Input, OnInit, SimpleChange, SimpleChanges } from "@angular/core";
import { KeyCloakService } from "../../../../core/service/keycloak.service";
import { MatDialog } from "@angular/material/dialog";
import { UserTitleCreationComponent } from "../../../management/component/userTitle/userTitleCreation/userTitleCreation.component";
import { UserData as WebUserData, UserExtraData, UserService } from "../../../../../generated/web-api";
import { UserData } from '../../../../../generated/keycloak-api';
import { KeyValueEditableTableComponent } from "../keyValueEditableTable/keyValueEditableTable.component";
import { UserInfo } from "angular-oauth2-oidc";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from "rxjs";

@Component({
  selector: 'one-personal-info',
  templateUrl: './personalInfo.component.html',
  styleUrls: ['./personalInfo.component.scss']
})

export class PersonalInfoComponent implements OnInit {

  @Input()
  userInfo: UserData = {
    username: ''
  };

  userData: WebUserData = {
    username: ''
  };

  allowEdit = false;

  private sub: any;

  directMemberInfo: MatTableDataSource<any> = new MatTableDataSource<any>();

  displayedColumns: string[] = ['name', 'description'];
  displayedColumnsThree: string[] = ['name', 'description', 'additional'];
  isEmployee = true;

  isAdmin = false;

  userGroups: UserGroup[] = [];

  myAvatar: string = environment.defaultAvatar;

  // @Input()
  // actionEvents: Observable<string>;
  // private actionSubscription: Subscription;

  constructor(private keycloakService: KeyCloakService,
    private userService: UserService,
    private groupService: UserGroupService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog) {
    this.directMemberInfo.data = [];
  }


  ngOnInit(): void {
    if (this.userInfo != null && (this.userInfo.username != null || this.userInfo.preferred_username != null)) {
      this.doLoadUserData();
    }
    this.isAdmin = this.keycloakService.isAdmin();

  }

  ngOnChanges(changes: SimpleChanges) {
    this.allowEdit = false;

    if (this.userInfo != null && (this.userInfo.username != null || this.userInfo.preferred_username != null)) {
      const username = this.keycloakService.getUsername(this.userInfo);
      this.doLoadUserData();


      this.keycloakService.getAvailableGroups().then(groups => {
        let guest = groups.find(g => g.name.toLowerCase() == 'guest');
        this.keycloakService.getUsersOfGroup(guest.id).toPromise().then(users => {
          this.isEmployee = users.find(u => this.keycloakService.getUsername(u) == username) == null;
        });
      });
      this.groupService.findGroupsByUsingName(username).toPromise().then((data: any) => {
        this.userGroups = data.items as UserGroup[];
      });

      const currentUser = this.keycloakService.getUserInfo();
      const currentUserName = currentUser.username || currentUser.preferred_username;
      const mine = this.userInfo.username || this.userInfo.preferred_username;
      this.allowEdit = currentUserName === mine || this.keycloakService.isAdmin();
      this.myAvatar = this.userInfo.attributes && this.userInfo.attributes['avp_avatar'] != null && this.userInfo.attributes['avp_avatar'].length > 0 ? this.userInfo.attributes['avp_avatar'][0] : environment.defaultAvatar;
      console.log(this.myAvatar);
    }
  }

  doLoadUserData() {
    this.userInfo.username = this.userInfo.username || this.userInfo.preferred_username;
    if (this.userInfo.username != null) {
      this.userService.getAllUsers().toPromise().then((users) => {
        this.userData = users.items.find((u) => this.keycloakService.getUsername(u) == this.userInfo.username) || {
          username: this.userInfo.username,
          contacts: []
        };

        if (this.userData.directMembers != null && this.userData.directMembers.trim() != '') {
          this.keycloakService.getAllUsers().toPromise().then(allUserInfo => {
            const memberNames = this.userData.directMembers.trim().split(',');
            const newData = [];
            memberNames.forEach(member => {
              const user = allUserInfo.find(u => (u.username || u.preferred_username) == member);
              if (user != null) {
                newData.push(user);
              }
            });
            this.directMemberInfo.data = newData;
          });
        } else {
          this.directMemberInfo.data = [];
        }

      });
    } else {
      this.userData = {
        username: this.userInfo.username,
        contacts: []
      };

    }
  }

  editContactInfo() {
    let contacts = this.userData.contacts.slice();
    const dialogRef = this.dialog.open(KeyValueEditableTableComponent, {
      width: '900px',
      maxWidth: '100vw',
      disableClose: true,
      data: {
        items: contacts,
        headers: {
          name: 'Tên',
          description: 'Thông Tin'
        },
        title: 'Thay Đổi Thông Tin Liên Lạc'
      }
    });



    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.userData.contacts = contacts;
        console.log(this.userData);
        this.userService.createUser(this.userData).toPromise().catch((err) => alert(err));
      }
    });
  }

  editStudyLevels() {
    let studyLevel = this.userData.studyLevels ? this.userData.studyLevels.slice() : [] as any[];
    studyLevel = studyLevel.map(s => {
      let name = s.name;
      let description = s.description;
      let additional = '';
      try {
        let jsonData = JSON.parse(description);
        if (jsonData) {
          description = jsonData.description;
          additional = jsonData.additional;
        }
      } catch (ex) {
      }
      return {name, description, additional}
    });
    const dialogRef = this.dialog.open(KeyValueEditableTableComponent, {
      width: '900px',
      maxWidth: '100vw',
      disableClose: true,
      data: {
        items: studyLevel,
        headers: {
          name: 'Tên Trường',
          description: 'Chuyên Ngành',
          additional: 'Thời Gian (bắt đầu - kết thúc)'
        },
        title: 'Thay Đổi Thông Tin Học Vấn'
      }
    });



    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.userData.studyLevels = studyLevel.map(s => {
          return {
            name: s.name,
            description: JSON.stringify({
              description: s.description,
              additional: s.additional
            })
          }
        });
        console.log(this.userData);
        this.userService.createUser(this.userData).toPromise().catch((err) => alert(err));
      }
    });
  }

  editRewards() {
    let rewards = this.userData.rewards ? this.userData.rewards.slice() : [] as any[];
    rewards = rewards.map(r => {
      let name = r.name;
      let description = r.description;
      let extraData = '';
      try {
        let jsonData = JSON.parse(description);
        if (jsonData) {
          description = jsonData.description;
          extraData = jsonData.extraData
        }
      } catch (ex) {
      }
      return {
        name, description, extraData
      }
    });

    const dialogRef = this.dialog.open(KeyValueEditableTableComponent, {
      width: '900px',
      maxWidth: '100vw',
      disableClose: true,
      data: {
        items: rewards,
        headers: {
          name: 'Tên thành tựu / giải thưởng',
          description: 'Thời gian',
          extraData: 'Thông tin thêm'
        },

        title: 'Thay Đổi Thông Tin Thành Tựu & Giải Thưởng'
      }
    });



    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.userData.rewards = rewards.map(r => {
          return {
            name: r.name,
            description: JSON.stringify({
              description: r.description,
              extraData: r.extraData
            })
          }
        });
        console.log(this.userData);
        this.userService.createUser(this.userData).toPromise().catch((err) => alert(err));
      }
    });
  }

  editExperiences() {

    let expericens = this.userData.experiences ? this.userData.experiences.slice() : [] as any[];
    expericens = expericens.map(r => {
      let name = r.name;
      let description = r.description;
      let extraData = '';
      let additional = '';
      try {
        let jsonData = JSON.parse(description);
        if (jsonData) {
          description = jsonData.description;
          extraData = jsonData.extraData;
          additional = jsonData.additional
        }
      } catch (ex) {
      }
      return {
        name, description, extraData, additional
      }
    })
    const dialogRef = this.dialog.open(KeyValueEditableTableComponent, {
      width: '900px',
      maxWidth: '100vw',
      disableClose: true,
      data: {
        items: expericens,
        headers: {
          name: 'Kinh Nghiệm',
          description: 'Vị Trí',
          additional: 'Thời Gian (bắt đầu - kết thúc)',
          extraData: 'Thông tin thêm'
        },
        title: 'Thay Đổi Thông Tin Công Việc & Nghề  Nghiệp'
      }
    });



    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.userData.experiences = expericens.map(r => {
          return {
            name: r.name,
            description: JSON.stringify({
              description: r.description,
              extraData: r.extraData,
              additional: r.additional
            })
          }
        });
        console.log(this.userData);
        this.userService.createUser(this.userData).toPromise().catch((err) => alert(err));
      }
    });
  }

  getUsername() {
    return this.keycloakService.getUsername(this.userInfo);
  }

  changePassword() {
    const dialogRef = this.dialog.open(AdminPasswordModalComponent, {
      data: {
        userId: this.userInfo.id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        alert('Đổi mật khẩu thành công');
      }
    });
  }

  getNumOfMembers(group) {
    const members = group.members;
    return members ? (members.split(',').length - 1) / 2 + " thành viên" : "Không có thành viên";
  }

  gotoGroup(group: UserGroup) {
    this.router.navigate(['/management/groups', group.id]);
  }

  getFullname(user): string {
    return this.keycloakService.getFullName(user);
  }

  gotoUser(user: any) {
    this.router.navigate(['/userDetails', this.keycloakService.getUsername(user)]);
  }

  getUsernameOfMember(user) {
    return this.keycloakService.getUsername(user);
  }

  getDescription(data, index) {
    let description = data.description;
      let additional = '';
      let extraData = '';
      try {
        let jsonData = JSON.parse(description);
        if (jsonData) {
          description = jsonData.description;
          additional = jsonData.additional;
          extraData = jsonData.extraData;
        }
      } catch (ex) {
      }
      if (index == 0) {
        return description;
      }
      if (index == 1) {
        return additional;
      }
      return extraData;
  }
}
