import {ChangeDetectorRef, Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UserExtraData, UserTitleData} from "../../../../../generated/web-api";
import { ThrowStmt } from '@angular/compiler';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'key-value-editable-table',
  templateUrl: './keyValueEditableTable.component.html',
  styleUrls: ['./keyValueEditableTable.component.scss']
})

export class KeyValueEditableTableComponent implements OnInit {

  displayedColumns: string[] = ['name', 'description', 'additional', 'extraData','action']
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  headers: any = {};
  dialogTitle: string = '';
  constructor(
    @Inject(MAT_DIALOG_DATA) data: any,
    private changeDetectorRefs: ChangeDetectorRef,
    private dialogRef: MatDialogRef<KeyValueEditableTableComponent>
  ) {
    this.dataSource.data = data.items || [];
    this.headers = data.headers || {};
    this.dialogTitle = data.title || '';
  }

  ngOnInit(): void {
  }

  submitForm() {
    this.dialogRef.close(true);
  }

  onNameChanged(data, newVal) {
    data.name = newVal;
  }

  onDescriptionChanged(data, newVal) {
    data.description = newVal;
  }
  onAdditionalChanged(data, newVal) {
    data.additional = newVal;
  }

  onExtraDataChanged(data, newVal) {
    data.extraData = newVal;
  }


  addNew() {
    let data = this.dataSource.data;
    data.push({
      name: '',
      description: '',
      additional: '',
      extraData: ''
    });

    this.dataSource.data = data;
  }

  remove(element) {
    let index = this.dataSource.data.indexOf(element);
    if (index >= 0) {
      let data = this.dataSource.data;
      data.splice(index, 1);
      this.dataSource.data = data;
    }
  }

  getDisplayedColumns() {
    if (this.headers.extraData == null) {
      let index = this.displayedColumns.indexOf('extraData');
      if (index >= 0)
        this.displayedColumns.splice(index, 1);
    }
    if (this.headers.additional == null) {
      let index = this.displayedColumns.indexOf('additional');
      if (index >= 0)
        this.displayedColumns.splice(index, 1);
    }
    return this.displayedColumns;
  }
}
