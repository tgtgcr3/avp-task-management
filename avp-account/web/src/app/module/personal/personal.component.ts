import {Component, Input, OnInit} from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, Subscription } from "rxjs";
import { UserData } from 'src/generated/keycloak-api';

import {KeyCloakService} from "../../core/service/keycloak.service";

@Component({
  selector: 'one-personal-component',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})

export class PersonalComponent implements OnInit {


  private sub: any;

  userInfo: UserData;


  eventSubject: Subject<string> = new Subject<string>();

  currentId: any;

  constructor(
    private keycloakService: KeyCloakService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.sub = this.route.params.subscribe(params => {
      this.currentId = params['id'];
      this._reload(this.currentId);

   });
  }

  _reload(id) {
    if (id == 'me') {
      let me = this.keycloakService.getUserInfo() || {};
      if (me != null && (me['username'] != null || me['preferred_username'] != null)) {
        this.loadUserInfo(me['username'] || me['preferred_username']);
      } else {
        this.router.navigate(['/']);
      }

    } else {
      this.loadUserInfo(id);
    }
  }

  ngOnInit(): void {
  }

  loadUserInfo(username: string) {
    this.keycloakService.findUserByUsername(username).toPromise().then(users => {
      if (users.length > 0) {
        this.userInfo = users.find(u => u.username == username);
      } else {
        this.router.navigate(['/']);
      }
    }).catch(err => {
      console.log(err);
      this.router.navigate(['/']);
    })
  }

  onReceiveEvent(eventData) {
    if (eventData.indexOf('reload') == 0) {
      this._reload(this.currentId);
    }
  }

}
