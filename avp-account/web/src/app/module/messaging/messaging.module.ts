import { DirectMessageContactComponent } from './direct-message-contact/direct-message-contact.component';
import { MiniChatContactComponent } from './mini-chat-contact/mini-chat-contact.component';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { OneMiniChatComponent } from './mini-chat/mini-chat.component';
import { NgModule } from '@angular/core';
import { NgChatModule } from 'ng-chat';
@NgModule({
  declarations: [
    OneMiniChatComponent,
    MiniChatContactComponent,
    DirectMessageContactComponent
  ],
  exports: [
    OneMiniChatComponent
  ],
  imports: [
    MatCardModule,
    CommonModule,
    NgChatModule,
  ]
})

export class MessagingModule {}
