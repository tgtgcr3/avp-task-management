import { environment } from './../../../../environments/environment';
import { Component, Input, SimpleChanges } from '@angular/core';
@Component({
  selector: 'direct-message-contact',
  templateUrl: './direct-message-contact.component.html',
  styleUrls: ['./direct-message-contact.component.scss']
})

export class DirectMessageContactComponent {

  @Input()
  directMessage: any;

  avatarUrl = '';
  directMessageName = '';

  constructor() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    this.avatarUrl = '';
    this.directMessageName = '';

  }

  getAvatarUrl() {
    if (this.directMessage && this.directMessage.usernames) {
      for (let i = 0; i < this.directMessage.usernames.length; i++) {
        if (this.directMessage.usernames[i] != 'toannhanb7') {
          return `${environment.baseUrl}/api/pri/user/${this.directMessage.usernames[i]}/avatar`;
        }
      }
    }
    return null;
  //  return `${environment.baseUrl}/api/pri/user/${this.user.username}/avatar`;
  }


}
