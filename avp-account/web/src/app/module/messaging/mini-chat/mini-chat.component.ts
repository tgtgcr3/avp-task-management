import { ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AvatarService } from './../../../../generated/web-api/api/avatar.service';
import { forkJoin } from 'rxjs';
import { RocketChatService } from './../../../../generated/rocket-api/api/rocketChat.service';
import { SecurityService } from './../../../core/service/security.service';
import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { Component, Input, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { RealTimeAPI } from "rocket.chat.realtime.api.rxjs";
import { environment } from 'src/environments/environment';
import { MiniChatAdapter } from '../services/mini-chat.adapter';
// import { driver } from '@rocket.chat/sdk';
// import { IAsteroid } from '@rocket.chat/sdk/dist/config/asteroidInterfaces';


@Component({
  selector: 'one-mini-chat',
  templateUrl: './mini-chat.component.html',
  styleUrls: ['./mini-chat.component.scss']
})

export class OneMiniChatComponent implements OnInit {

  @Input()
  public userId: string;

  private numOfTries: number = 0;

  constructor(
    public chatAdapter: MiniChatAdapter,
    private keycloakService: KeyCloakService,
    private changeDetectorRef: ChangeDetectorRef
  ) {

  }

  ngOnInit() {
    this.init();
  }

  private init(): void {
    if (this.keycloakService.getUserInfo() != null) {
      this.chatAdapter.init();
    } else {
      if (this.numOfTries++ < 10) {
        setTimeout(() => this.init(), 500);
      }
    }
  }


}
