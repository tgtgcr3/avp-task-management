import { environment } from './../../../../environments/environment';
import { Component, Input } from '@angular/core';
@Component({
  selector: 'mini-chat-contact',
  templateUrl: './mini-chat-contact.component.html',
  styleUrls: ['./mini-chat-contact.component.scss']
})

export class MiniChatContactComponent {

  @Input()
  user: any;

  constructor() {

  }

  getAvatarUrl() {
    return `${environment.baseUrl}/api/pri/user/${this.user.username}/avatar`;
  }


}
