import { KeyCloakService } from './../../../../core/service/keycloak.service';
import {Component, OnInit} from "@angular/core";
import {UserTitleData, UserTitleService} from "../../../../../generated/web-api";
import {MemberCreationComponent} from "../../../member/component/memberCreation/memberCreation.component";
import {MatDialog} from "@angular/material/dialog";
import {UserTitleCreationComponent} from "./userTitleCreation/userTitleCreation.component";

@Component({
  selector: 'one-user-title-component',
  templateUrl: './userTitle.component.html',
  styleUrls: ['./userTitle.component.scss']
})

export class UserTitleComponent implements OnInit {

  displayedColumns: string[] = ['name', 'actions'];
  userTitles: UserTitleData[] = [];
  constructor(
    private userTitleService: UserTitleService,
    private dialog: MatDialog,
    private keycloakService: KeyCloakService
  ) {
    this.userTitleService.configuration.basePath = this.userTitleService.configuration.basePath.replace('localhost/', 'localhost:4200/');
  }


  ngOnInit(): void {

    this.userTitleService.getAllUserTitles().toPromise().then(result => {
      this.userTitles = result.items;
    })

  }

  createUserTitle() {
    const dialogRef = this.dialog.open(UserTitleCreationComponent, {
      data: {
        animal: 'panda'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.userTitleService.getAllUserTitles().toPromise().then(result => {
          this.userTitles = result.items;
        })
      }
      console.log(`Dialog result: ${result}`);
    });
  }

  editUserTitle(userTitle) {
    const dialogRef = this.dialog.open(UserTitleCreationComponent, {
      data: userTitle
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.userTitleService.getAllUserTitles().toPromise().then(result => {
          this.userTitles = result.items;
        })
      }
      console.log(`Dialog result: ${result}`);
    });
  }

  removeUserTitle(userTitle) {
    const message = 'Bạn có chắc muốn xóa Chức Vụ này?';
    if (confirm(message)) {
      this.keycloakService.getAllUsers().toPromise().then(users => {
        let user = users.find(u => u.attributes != null && u.attributes['avp_title'] != null && u.attributes['avp_title'].length > 0 && u.attributes['avp_title'][0] == userTitle.name);
        if (user != null) {
          alert('Chức Vụ đang được sử dụng.')
        } else {
          this.userTitleService.removeUserTitle(userTitle.id).toPromise().then((r) => {
            this.userTitleService.getAllUserTitles().toPromise().then(result => {
              this.userTitles = result.items;
            })
          });
        }
      });
  }
  }
}
