import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MustMatch} from "../../../../../core/helper/formHelper";
import {UserTitleService, UserTitleData} from "../../../../../../generated/web-api";
import { KeyCloakService } from 'src/app/core/service/keycloak.service';

@Component({
  selector: 'user-title-creation-component',
  templateUrl: './userTitleCreation.component.html',
  styleUrls: ['./userTitleCreation.component.scss']
})

export class UserTitleCreationComponent implements OnInit {

  registerForm: FormGroup;

  submitted: boolean = false;

  isUpdate: boolean = false;

  userTitle: UserTitleData = {
    name: ''
  };

  constructor(@Inject(MAT_DIALOG_DATA) private data: UserTitleData,
              private formBuilder: FormBuilder,
              private userTitleService: UserTitleService,
              private keycloakService: KeyCloakService,
              private dialogRef: MatDialogRef<UserTitleCreationComponent>
              ) {
    this.userTitleService.configuration.basePath = '/api/pri';
    this.isUpdate = this.data != null && this.data.id != null;
  }


  ngOnInit(): void {

    this.registerForm = this.formBuilder.group({
      name: [this.data?.name, Validators.required]
    }, {
    });
  }

  get f() { return this.registerForm.controls; }

  submitForm() {
    this.submitted = true;
// stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    let userData = {
      name: this.registerForm.value.name.trim(),
      id: this.data?.id
    };

    if (this.data != null && this.data.id != null) {
      this.userTitleService.updateUserTitle(this.data.id, userData).toPromise().then((newData) => {

        this.keycloakService.getAllUsers().toPromise().then(users => {
          users.forEach(u => {
            if (u.attributes != null && u.attributes['avp_title'].length > 0 && u.attributes['avp_title'][0] == this.data.name) {
              u.attributes['avp_title'][0] = userData.name;
              this.keycloakService.updateUser(u.id, u).toPromise();
            }
          });
        });

        this.dialogRef.close(true);
      }).catch((err) => {
        console.log(err);
      })
    } else {
      this.userTitleService.createUserTitle(userData).toPromise().then((newData) => {
        this.dialogRef.close(true);
      }).catch((err) => {
        console.log(err);
      })
    }



    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }





}
