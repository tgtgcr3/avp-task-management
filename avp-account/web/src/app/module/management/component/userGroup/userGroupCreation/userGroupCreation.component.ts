import { UserData } from './../../../../../../generated/keycloak-api/model/userData';
import { map } from 'rxjs/operators';
import { startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA } from '@angular/cdk/keycodes';
import { ENTER } from '@angular/cdk/keycodes';
import { UserGroupService } from './../../../../../../generated/web-api/api/userGroup.service';
import { UserGroup } from './../../../../../../generated/web-api/model/userGroup';
import { Component, Inject, OnInit, ViewChild, ElementRef } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MustMatch } from "../../../../../core/helper/formHelper";
import { UserTitleService, UserTitleData } from "../../../../../../generated/web-api";
import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { updateYield } from 'typescript';

@Component({
  selector: 'user-group-creation-component',
  templateUrl: './userGroupCreation.component.html',
  styleUrls: ['./userGroupCreation.component.scss']
})

export class UserGroupCreationComponent implements OnInit {

  registerForm: FormGroup;

  submitted: boolean = false;

  isUpdate: boolean = false;

  userGroup: UserGroup = {
    id: '',
    name: ''
  };

  currentUsers: UserData[] = [];

  separatorKeysCodes: number[] = [ENTER, COMMA];

  groupOwnerFrmCtrl: FormControl = new FormControl();
  @ViewChild('groupOwnerInput') groupOwnerInput: ElementRef<HTMLInputElement>;

  @ViewChild('groupMemberInput') groupMemberInput: ElementRef<HTMLInputElement>;

  @ViewChild('chipList') chipList;

  @ViewChild('chipListGroupMember') chipListGroupMember;

  groupMemberFrmCtrl: FormControl = new FormControl();

  filteredUserMembers: Observable<any[]>;
  filteredUsers: Observable<any[]>;

  constructor(@Inject(MAT_DIALOG_DATA) private data: UserGroup,
    private formBuilder: FormBuilder,
    private userGroupService: UserGroupService,
    private keycloakService: KeyCloakService,
    private dialogRef: MatDialogRef<UserGroupCreationComponent>
  ) {
    this.userGroupService.configuration.basePath = '/api/pri';
    this.isUpdate = this.data != null && this.data.id != null;

    this.filteredUsers = this.groupOwnerFrmCtrl.valueChanges.pipe(
      startWith(null),
      map((user: UserGroup | null) => user ? this._filter(user) : this.currentUsers)
    );

    this.filteredUserMembers = this.groupMemberFrmCtrl.valueChanges.pipe(
      startWith(null),
      map((user: UserGroup | null) => user ? this._filterMember(user) : this.currentUsers)
    );
  }



  ngOnInit(): void {
    this.keycloakService.getAllUsers(null, false, null, 0, 1024).toPromise().then(users => {
      this.currentUsers = users;
    });

    this.registerForm = this.formBuilder.group({
      name: [this.data?.name, Validators.required],
      directory: [this.data?.id, Validators.required],
      groupOwner: [this.data?.owner, Validators.required],
      groupMembers: [this.data?.members, Validators.required],
      description: [this.data?.description]
    }, {
    });
    if (this.isUpdate == true) {
      this.registerForm.controls['directory'].disable();
    }
  }

  get groupOwner() {
    const name = this._groupOwnerName;
    if (name != null) {
      return this.currentUsers.find(u => this.keycloakService.getUsername(u) == name);
    }
    return null;
  }

  get _groupOwnerName() {
    return this.registerForm.get('groupOwner').value;
  }

  get groupMembers() {
    const members = this.registerForm.get('groupMembers').value;
    if (members != null && members.length > 2) {
      return this.currentUsers.filter(u => members.indexOf(`,${this.keycloakService.getUsername(u)},`) >= 0);
    }
    return [];
  }

  get f() { return this.registerForm.controls; }

  submitForm() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    if (this.isUpdate === false) {
      const userData = {
        name: this.registerForm.value.name.trim(),
        id: this.registerForm.value.directory.trim(),
        owner: this.registerForm.value.groupOwner.trim(),
        members: this.registerForm.value.groupMembers.trim(),
        description: this.registerForm.value.description?.trim()
      };

      this.userGroupService.createUserGroup(userData).toPromise().then(data => {
        this.dialogRef.close('ok');
      });
    } else {
      this.data.name = this.registerForm.value.name.trim();
      this.data.description = this.registerForm.value.description?.trim();
      this.userGroupService.updateUserGroup(this.data.id, this.data).toPromise().then(data => {
        this.dialogRef.close('Ok');
      }).catch(err => {
        alert('Không thể tạo nhóm. Đường dẫn đã tồn tại');
      });
    }
  }

  removeGroupOwner(groupOwner) {
    this.registerForm.get('groupOwner').setValue(null);
    this.registerForm.get('groupOwner').updateValueAndValidity();
    this.chipList.errorState = this.submitted;
  }

  getFullName(user): string {
    return user != null ? `${user.lastName.trim()} ${user.firstName.trim()}` : '';
  }

  selectedGroupOwner(event: MatAutocompleteSelectedEvent) {
    this.registerForm.get('groupOwner').setValue(this.keycloakService.getUsername(event.option.value));
    this.groupOwnerInput.nativeElement.value = '';
    this.groupOwnerFrmCtrl.setValue(null);
    this.registerForm.get('groupOwner').updateValueAndValidity();
    this.chipList.errorState = false;

  }

  trySelectedGroupOwner(name: MatChipInputEvent) {

  }

  private _filter(user: any): UserData[] {
    const filterValue = (user.firstName != null) ? this.getFullName(user).toLowerCase() : user.toString().toLowerCase();

    return this.currentUsers.filter(currentUser => this.getFullName(currentUser).toLowerCase().indexOf(filterValue) >= 0);
  }

  removeGroupMember(member) {
    this.registerForm.get('groupMembers').setValue(this.registerForm.value.groupMembers.replace(`,${this.keycloakService.getUsername(member)},`));
  }

  trySelectedGroupMember(name: MatChipInputEvent) {

  }

  selectedGroupMember(event: MatAutocompleteSelectedEvent) {
    this.registerForm.get('groupMembers').setValue((this.registerForm.value.groupMembers || '') + `,${this.keycloakService.getUsername(event.option.value)},`);
    // this.groupMembers.push(event.option.value);
    this.groupMemberInput.nativeElement.value = '';
    this.groupMemberFrmCtrl.setValue(null);
    this.registerForm.get('groupMembers').updateValueAndValidity();
  }

  private _filterMember(user: any): UserData[] {
    const filterValue = (user.firstName != null) ? this.getFullName(user).toLowerCase() : user.toString().toLowerCase();
    const groupUsernames = this.registerForm.value.groupMembers || '';
    const users = this.currentUsers.filter(currentUser => (this.getFullName(currentUser).toLowerCase().indexOf(filterValue) >= 0 &&
      groupUsernames.indexOf(`,${this.keycloakService.getUsername(currentUser)},`) < 0
    ));
    return users;
  }

}
