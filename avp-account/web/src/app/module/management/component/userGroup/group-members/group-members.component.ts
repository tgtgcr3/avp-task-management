import { UserGroupCreationComponent } from './../userGroupCreation/userGroupCreation.component';
import { Subject } from 'rxjs';
import { UserAddGroupComponent } from './component/user-add/user-add-group.component';
import { MatDialog } from '@angular/material/dialog';
import { UserGroup } from './../../../../../../generated/web-api/model/userGroup';
import { ActivatedRoute } from '@angular/router';
import { UserGroupService } from './../../../../../../generated/web-api/api/userGroup.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'group-member-component',
  templateUrl: './group-members.component.html',
  styleUrls: ['./group-members.component.scss']
})
export class GroupMemberComponent implements OnInit {

  groupId: string;

  groupInfo: UserGroup;

  members: string[] = ['~~~~~'];

  eventSubject: Subject<string> = new Subject<string>();

  constructor(
    private groupService: UserGroupService,
    private route: ActivatedRoute,
    private dialog: MatDialog
  ) {
    this.groupService.configuration.basePath = '/api/pri';
  }

  ngOnInit(): void {
    this.groupId = this.route.snapshot.paramMap.get('id');
    this.loadGroupInfo();
  }

  loadGroupInfo() {
    return this.groupService.getUserGroup(this.groupId).toPromise().then(groupInfo => {
      this.groupInfo = groupInfo;
      if (this.groupInfo.members != null && this.groupInfo.members.length > 2) {
        this.members = this.groupInfo.members.split(',').filter(m => m.length > 0);
      } else {
        this.members = ['~~~~~'];
      }
    });
  }

  updateUser() {
    const dialogRef = this.dialog.open(UserAddGroupComponent, {
      data: this.groupInfo
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.loadGroupInfo().then((d) => {
        this.eventSubject.next('reload');
      });
    });
  }

  updateGroup() {
    const dialogRef = this.dialog.open(UserGroupCreationComponent, {
      data: this.groupInfo
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.loadGroupInfo().then((d) => {
        this.eventSubject.next('reload');
      });
    });
  }
}
