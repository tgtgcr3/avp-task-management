import { KeyCloakService } from './../../../../../core/service/keycloak.service';
import { Input } from '@angular/core';
import { Observable } from 'rxjs';
import { UserGroup } from './../../../../../../generated/web-api/model/userGroup';
import { UserGroupService } from './../../../../../../generated/web-api/api/userGroup.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'user-group-list-component',
  templateUrl: './user-group-list.component.html',
  styleUrls: ['./user-group-list.component.scss']
})
export class UserGroupListComponent implements OnInit {

  userGroups: UserGroup[];

  filteredUserGroups: UserGroup[];

  displayedColumns: string[] = ['rowIndex', 'groupInfo', 'actions'];

  @Input()
  actionEvents: Observable<string>;

  @Input()
  filterEvents: Observable<string>;

  currentFilter = 'allGroup';

  currentUserName = '';
  isAdmin = false;

  constructor(
    private userGroupService: UserGroupService,
    private keycloakService: KeyCloakService
  ) {
    this.userGroupService.configuration.basePath = '/api/pri';
    this.userGroups = [];
  }

  ngOnInit(): void {
    this._reload();
    this.currentUserName = this.keycloakService.getUsername(this.keycloakService.getUserInfo());

    this.isAdmin = this.keycloakService.isAdmin();

    this.actionEvents?.subscribe(action => {
      if (action.indexOf('reload') == 0) {
        this._reload();
      }
    });

    this.filterEvents?.subscribe(filter => {
      this.currentFilter = filter;
      this._filterGroups();
    });
  }

  private _reload() {
    this.userGroupService.getAll().toPromise().then((obj: any) => {
      this.userGroups = obj.items;
      this._filterGroups();
    });
  }

  private _filterGroups() {
    if (this.currentFilter == 'allGroup') {
      this.filteredUserGroups =  this.userGroups;
    } else {
      const username = `,${this.currentUserName},`;
      this.filteredUserGroups = this.userGroups.filter(u => u.members.indexOf(username) >= 0 || u.owner == username);
    }
  }

  getNumOfMembers(group) {
    const members = group.members;
    return members ? (members.split(',').length - 1) / 2 + " thành viên": "Không có thành viên";
  }

  removeUserGroup(group) {
    const message  = `Bạn có chắc muốn xóa nhóm "${group.name}"`;
    if (confirm(message)) {
      this.userGroupService.deleteUserGroup(group.id).toPromise().then(data => this._reload());

    }
  }
}
