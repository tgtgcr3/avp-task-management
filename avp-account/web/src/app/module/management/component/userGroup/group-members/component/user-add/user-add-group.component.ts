import { MatDialogRef } from '@angular/material/dialog';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { UserData } from 'src/generated/keycloak-api';
import { KeyCloakService } from './../../../../../../../core/service/keycloak.service';
import { startWith, map } from 'rxjs/operators';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UserGroup } from './../../../../../../../../generated/web-api/model/userGroup';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UserGroupService } from './../../../../../../../../generated/web-api/api/userGroup.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'user-add-group-component',
  templateUrl: './user-add-group.component.html',
  styleUrls: ['./user-add-group.component.scss']
})
export class UserAddGroupComponent implements OnInit {

  registerForm: FormGroup;

  currentUsers: UserData[] = [];

  @ViewChild('groupMemberInput') groupMemberInput: ElementRef<HTMLInputElement>;

  groupMemberFrmCtrl: FormControl = new FormControl();

  filteredUserMembers: Observable<any[]>;
  filteredUsers: Observable<any[]>;

  separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor(
    private groupService: UserGroupService,
    @Inject(MAT_DIALOG_DATA) private data: UserGroup,
    private formBuilder: FormBuilder,
    private keycloakService: KeyCloakService,
    private dialogRef: MatDialogRef<UserAddGroupComponent>
  ) {
    this.groupService.configuration.basePath = '/api/pri';

    this.filteredUserMembers = this.groupMemberFrmCtrl.valueChanges.pipe(
      startWith(null),
      map((user: UserGroup | null) => user ? this._filterMember(user) : this.currentUsers)
    );

  }

  ngOnInit(): void {
    this.keycloakService.getAllUsers(null, false).toPromise().then(users => {
      this.currentUsers = users;
    });

    this.registerForm = this.formBuilder.group({
      groupMembers: [this.data?.members]
    }, {
    });
   }

   get groupMembers() {
    const members = this.registerForm.get('groupMembers').value;
    if (members != null && members.length > 2) {
      return this.currentUsers.filter(u => members.indexOf(`,${this.keycloakService.getUsername(u)},`) >= 0);
    }
    return [];
  }

   removeGroupMember(member) {
    this.registerForm.get('groupMembers').setValue(this.registerForm.value.groupMembers.replace(`,${this.keycloakService.getUsername(member)},`));
  }

  trySelectedGroupMember(name: MatChipInputEvent) {

  }

  selectedGroupMember(event: MatAutocompleteSelectedEvent) {
    this.registerForm.get('groupMembers').setValue((this.registerForm.value.groupMembers || '') + `,${this.keycloakService.getUsername(event.option.value)},`);
    // this.groupMembers.push(event.option.value);
    this.groupMemberInput.nativeElement.value = '';
    this.groupMemberFrmCtrl.setValue(null);
    this.registerForm.get('groupMembers').updateValueAndValidity();
  }

  private _filterMember(user: any): UserData[] {
    const filterValue = (user.firstName != null) ? this.getFullName(user).toLowerCase() : user.toString().toLowerCase();
    const groupUsernames = this.registerForm.value.groupMembers || '';
    const users = this.currentUsers.filter(currentUser => (this.getFullName(currentUser).toLowerCase().indexOf(filterValue) >= 0 &&
    groupUsernames.indexOf(`,${this.keycloakService.getUsername(currentUser)},`) < 0
  ));
    return users;
  }

  getFullName(user):string {
    return user != null ? `${user.lastName.trim()} ${user.firstName.trim()}` : '';
  }

  submitForm() {
    if (this.registerForm.invalid) {
      return;
    }

    this.data.members = this.registerForm.value.groupMembers.trim();

    this.groupService.updateUserGroup(this.data.id, this.data).toPromise().then(re => {
      this.dialogRef.close('Ok');
    });
  }
}
