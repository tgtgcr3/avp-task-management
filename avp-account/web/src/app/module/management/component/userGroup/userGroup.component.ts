import { Subject } from 'rxjs';
import { UserGroupCreationComponent } from './userGroupCreation/userGroupCreation.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from "@angular/core";

@Component({
  selector: 'user-group-component',
  templateUrl: './userGroup.component.html',
  styleUrls: ['./userGroup.component.scss']

})

export class UserGroupComponent implements OnInit {

  eventSubject: Subject<string> = new Subject<string>();

  filterSubject: Subject<string> = new Subject<string>();

  isShowAll = true;

  constructor(
    private dialog: MatDialog
  ) {

  }

  ngOnInit(): void {
  }


  createUserGroup() {
    const dialogRef = this.dialog.open(UserGroupCreationComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.eventSubject.next('reload');
    });
  }

  updateFilter(name) {
    this.filterSubject.next(name);
    this.isShowAll = name == 'allGroup';
  }

}
