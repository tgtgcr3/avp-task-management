import { UserAddGroupComponent } from './component/userGroup/group-members/component/user-add/user-add-group.component';
import { MemberModule } from './../member/member.module';
import { GroupMemberComponent } from './component/userGroup/group-members/group-members.component';
import { UserGroupListComponent } from './component/userGroup/userGroupList/user-group-list.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { UserGroupCreationComponent } from './component/userGroup/userGroupCreation/userGroupCreation.component';
import {NgModule} from "@angular/core";
import {UserTitleComponent} from "./component/userTitle/userTitle.component";
import {UserGroupComponent} from "./component/userGroup/userGroup.component";
import {MatTableModule} from "@angular/material/table";
import {ManagementComponent} from "./management.component";
import {TranslateModule} from "@ngx-translate/core";
import {UserTitleCreationComponent} from "./component/userTitle/userTitleCreation/userTitleCreation.component";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {BrowserModule} from "@angular/platform-browser";
import {MatIconModule} from "@angular/material/icon";
import {ReactiveFormsModule} from "@angular/forms";
import {CoreModule} from "../../core/core.module";
import {ManagementRoutingModule} from "./management.routing";
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    imports: [
        MatButtonModule,
        MatDialogModule,
        MatInputModule,
        MatSelectModule,
        BrowserModule,
        MatTableModule,
        MatIconModule,
        ReactiveFormsModule,
        TranslateModule,
        CoreModule,
        ManagementRoutingModule,
        MatChipsModule,
        MatAutocompleteModule,
        MemberModule,
        MatTooltipModule,
        BrowserAnimationsModule
    ],
  declarations: [
    UserTitleCreationComponent,
    UserTitleComponent,
    UserGroupComponent,
    ManagementComponent,
    UserGroupCreationComponent,
    UserGroupListComponent,
    GroupMemberComponent,
    UserAddGroupComponent
  ],
  exports: [
    ManagementComponent
  ]
})

export class ManagementModule{}
