import { GroupMemberComponent } from './management/component/userGroup/group-members/group-members.component';
import {RouterModule, Routes} from "@angular/router";
import {MemberPageComponent} from "./member/member.component";
import {NgModule} from "@angular/core";
import {PersonalComponent} from "./personal/personal.component";
import {LandingPageComponent} from "./landingPage/landingPage.component";
import {ManagementComponent} from "./management/management.component";
import {ManagementModule} from "./management/management.module";
import {UserTitleComponent} from "./management/component/userTitle/userTitle.component";
import { UserGroupComponent } from './management/component/userGroup/userGroup.component';
import { GuestMemberComponent } from './guestMember/guestMember.component';
import {ActivityLogComponent} from "./activity-log/activity-log.component";

const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent
  },
  {
    path: 'members',
    component: MemberPageComponent
  },
  {
    path: 'userDetails/:id',
    component: PersonalComponent
  },
  {
    path: 'management',
    component: ManagementComponent,
    children: [
      {
        path: '',
        redirectTo: '/management/userTitles',
        pathMatch: 'full'
      },
      {
        path: 'userTitles',
        component: UserTitleComponent
      },
      {
        path: 'userGroups',
        component: UserGroupComponent
      },
      {
        path: 'groups/:id',
        component: GroupMemberComponent
      }
    ]
  },
  {
    path: 'guests',
    component: GuestMemberComponent
  },
  {
    path: 'activity-log',
    component: ActivityLogComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class FeatureRoute {}
