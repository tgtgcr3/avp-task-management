import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CoreModule} from "./core/core.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import { OAuthModule } from 'angular-oauth2-oidc';
import {SecurityService} from "./core/service/security.service";
import {FeatureModule} from "./module/feature.module";
import {MatInputModule} from "@angular/material/input";
import {ErrorStateMatcher, ShowOnDirtyErrorStateMatcher} from "@angular/material/core";
import { BASE_PATH as webBasePath} from 'src/generated/web-api';
import { BASE_PATH as keycloakBasePath } from 'src/generated/keycloak-api';

import { environment } from 'src/environments/environment';

import { AvatarModule } from 'ngx-avatar';
import { CookieService } from 'ngx-cookie-service';
import { ImageCropperModule } from 'ngx-image-cropper';


function urlValidate(url: string): boolean {
  if (url.indexOf("/auth") >= 0) {
    return true;
  }
  if (url.indexOf('/api/pri') >= 0) {
    return true;
  }
  return false;
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    MatInputModule,
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    BrowserAnimationsModule,
    // translation
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    OAuthModule.forRoot({
      resourceServer: {
        //allowedUrls: ['http://localhost:4200/auth'],
        customUrlValidation: urlValidate,
        sendAccessToken: true
      }
    }),


    FeatureModule,
    AvatarModule,
    ImageCropperModule

  ],
  providers: [
    {
      provide: keycloakBasePath, useValue: environment.baseUrl
    },
    {
      provide: webBasePath, useValue: environment.baseUrl + '/api/pri'
    },
    {
      provide: Window, useValue: window
    },
    CookieService
   // {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private securityService: SecurityService) {
    //securityService.login();
  }

}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
