import { Group } from './../../../generated/keycloak-api/model/group';
import { TokenService } from './../../../generated/keycloak-api/api/token.service';
import { FormChangePassword } from './../../../generated/keycloak-api/model/formChangePassword';
import {Injectable, OnInit} from "@angular/core";
import {SecurityService} from "./security.service";
import {AccountService, GroupService, RoleService, UserResetPasswordData, UserService} from "../../../generated/keycloak-api";
import {} from "../../../generated/keycloak-api";
import {environment} from "../../../environments/environment";
import { timestamp } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { L } from '@angular/cdk/keycodes';

@Injectable({
  providedIn: 'root'
})

export class KeyCloakService implements OnInit {

  targetServer: string

  groups: Group[];

  adminGroup: Group;

  ownerGroup: Group;

  userGroup: Group;

  guestGroup: Group;

  constructor(private securityService: SecurityService, private roleService : RoleService,
              private userService: UserService,
              private groupService: GroupService,
              private accountService: AccountService,
              private tokenService: TokenService
  ) {
    this.targetServer = '/auth/admin/realms/An-Viet-Phat'; //this.securityService.getIssuer();
    this.roleService.configuration.basePath =
      this.groupService.configuration.basePath =

      this.userService.configuration.basePath = this.targetServer; //'/auth/admin/realms/An-Viet-Phat';

    this.accountService.configuration.basePath =
        this.tokenService.configuration.basePath = '/auth/realms/An-Viet-Phat';


  }

  ngOnInit(): void {
  }

  getAvailableRoles() {
    return this.roleService.getRoles();
  }

  getGroupOfUser(userGroups: Group[]) {
    let groupNames = ['owner', 'admin', 'user', 'guest'];
    for (let i = 0; i < groupNames.length; i++) {
      let group = this.findGroupByName(groupNames[i], userGroups);
      if (group != null) {
        return group;
      }
    }
    return null;
  }

  getAvailableGroups(force = false): Promise<Group[]> {
    return new Promise((res, rej) => {
      if (this.groups != null && force == false) {
        res(this.groups);
      } else {
        this.groupService.getAllGroups().toPromise().then(groups => {
          this.groups = groups;
          this.ownerGroup = this.findGroupByName('owner', groups);
          this.adminGroup = this.findGroupByName('admin', groups);
          this.userGroup = this.findGroupByName('user', groups);
          this.guestGroup = this.findGroupByName('guest', groups);
          res(groups);
        }).catch(ee => {
          rej(ee);
        });
      }
    });

  }

  findGroupByName(groupName: string, groups: Group[]) {
    return groups.find(g => g.name.toLowerCase() == groupName.toLowerCase());
  }


  getUserInfo() {
    return this.securityService.getUserInfo();
  }

  getAllUsers(username?: string, fullData?: boolean, search?: string, first?: number, max?: number) {
    return this.userService.getUsers(username, fullData, first, max, search);
  }

  updateUser(id: string, userData: object) {
    return this.userService.updateUser(id, userData);
  }

  createUser(userData: object) {
    return this.userService.createUser(userData);
  }

  getUsersOfGroup(groupId: string) {
    return this.groupService.getUsersOfGroup(groupId);
  }

  getGroupsByUserId(userId: string) {
    return this.groupService.getGroupByUserId(userId);
  }

  resetPassword(userId: string, password: string) {
    return this.userService.resetPassword(userId, {
      type: UserResetPasswordData.TypeEnum.Password,
      temporary: false,
      value: password
    });
  }

  findUserByUsername(userName: string) {
    return this.userService.getUsers(userName);
  }

  findUsersByUserTitle(userTitle: string) {

  }

  assignGroup(userId: string, groupId: string) {
    return new Promise((res, rej) => {
      this.findGroup(groupId).then(group => {
        if (group.name.toLowerCase() == 'owner') {
          forkJoin(
            this.userService.setGroup(userId, this.userGroup.id),
            this.userService.setGroup(userId, this.adminGroup.id),
            this.userService.setGroup(userId, this.ownerGroup.id)
          ).toPromise().then(r => {
            res(group);
          }).catch(err => {
            rej(err);
          });
        } else if (group.name.toLowerCase() == 'admin') {
          forkJoin(
            this.userService.setGroup(userId, this.userGroup.id),
            this.userService.setGroup(userId, this.adminGroup.id),
            this.userService.removeGroup(userId, this.ownerGroup.id)
          ).toPromise().then(r => {
            res(group);
          }).catch(err => {
            rej(err);
          });
        } else if (group.name.toLowerCase() == 'user') {
          forkJoin(
            this.userService.setGroup(userId, this.userGroup.id),
            this.userService.removeGroup(userId, this.adminGroup.id),
            this.userService.removeGroup(userId, this.ownerGroup.id)
          ).toPromise().then(r => {
            res(group);
          }).catch(err => {
            rej(err);
          });
        } else {
          forkJoin(
            this.userService.setGroup(userId, this.guestGroup.id)
          ).toPromise().then(r => {
            res(group);
          }).catch(err => {
            rej(err);
          });
        }
      });
    });

    //return this.userService.setGroup(userId, groupId);
  }



  private findGroup(groupId: string) {
    return this.getAvailableGroups().then(groups => {
      return groups.find(g => g.id == groupId);
    });
  }



  isAdmin() {
    let userInfo = this.securityService.getUserInfo();
    if (userInfo != null && userInfo['group'] != null) {
      return userInfo['group'].indexOf('/Admin') >= 0 || userInfo['group'].indexOf('/Owner') >= 0;
    }
    return false;
  }

  isOwner() {
    let userInfo = this.securityService.getUserInfo();
    if (userInfo != null && userInfo['group'] != null) {
      return userInfo['group'].indexOf('/Owner') >= 0;
    }
    return false;
  }

  updateUserPassword(username: string, currentPassword: string, newPassword: string) {
    return this.accountService.selfUpdatePassword({
      currentPassword,
    newPassword,
    confirmation: newPassword
    });


  }

  exchangeToken(targetClient: string) {
    return this.tokenService.exchangeToken(
      environment.clientId,
      environment.clientSecret,
      'urn:ietf:params:oauth:grant-type:token-exchange',
      this.securityService.getAccessToken(),
      'urn:ietf:params:oauth:token-type:refresh_token',
      targetClient
    );
  }

  getUsername(user: any) {
    return user.username || user.preferred_username;
  }

  isFromExternal(user: any): boolean {
    if (user.attributes != null) {
      return user.attributes['LDAP_ID'] != null;
    }
    return false;
  }

  getFullName(user: any) {
    if (user.lastName != null) {
      return `${(user.lastName || '').trim()} ${(user.firstName || '').trim()}`;
    }
    return `${(user.family_name || '').trim()} ${(user.given_name || '').trim()}`;

  }

  getAvatar(user: any): string {
    return this.getAttribute(user, 'avp_avatar', environment.defaultAvatar);
  }

  getTitle(user: any): string {
    return this.getAttribute(user, 'avp_title', 'Không xác định');
  }

  getAttribute(user: any, attrName: string, defaultValue: string): string {
    return user.attributes != null && user.attributes[attrName] != null && user.attributes[attrName].length > 0 ? (user.attributes[attrName][0] || defaultValue) : defaultValue ;
  }

  sortUser(users: any[]): any[] {
    return users.sort((a:any, b:any) => {
      let fullnameA = this.getFullName(a);
      let fullnameB = this.getFullName(b);
      let nameALIndex = fullnameA.lastIndexOf(' ');
      if (fullnameA[nameALIndex + 1] == '(') {
        nameALIndex = fullnameA.lastIndexOf(' ', nameALIndex - 1);
      }

      let nameBLIndex = fullnameB.lastIndexOf(' ');
      if (fullnameB[nameBLIndex + 1] == '(') {
        nameBLIndex = fullnameB.lastIndexOf(' ', nameBLIndex - 1);
      }


      let nameA = fullnameA.substr(nameALIndex + 1);
      let nameB = fullnameB.substr(nameBLIndex + 1);
      let compareResult = nameA.localeCompare(nameB);
      if (compareResult == 0) {
        return fullnameA.localeCompare(fullnameB);
      }
      return compareResult;
    })
  }
}
