import { SecurityService } from './security.service';
import { HttpResponse } from '@angular/common/http';
import {OAuthResourceServerErrorHandler} from 'angular-oauth2-oidc';
import { Observable, throwError } from 'rxjs';

export class CustomOauthErrorHandler implements OAuthResourceServerErrorHandler {

  constructor(
    private securityService: SecurityService
  ) {

  }

  handleError(err: HttpResponse<any>): Observable<any> {
    console.log(err);
    if (err.status == 400) {
      this.securityService.prepare();
    }
      return throwError(err);
  }

}
