import { Observable, Observer } from 'rxjs';
import { Injectable, NgZone, OnInit } from '@angular/core';

import {SSE} from 'sse.js';

@Injectable(
  {
    providedIn: 'root'
  }
)

export class SseService implements OnInit{

  eventSource: SSE;


  constructor(private _zone: NgZone) {

  }
  ngOnInit(): void {
    //throw new Error('Method not implemented.');
  }

  getServerSentEvent(url: string): Observable<any> {
    return new Observable((observer: Observer<any>) => {
      const eventSource = this.getEventSource(url);
      eventSource.addEventListener("logout", (e: any) => {
        this._zone.run(() => {
          observer.next(e);
        });
      });
      eventSource.stream();
      eventSource.onmessage = (event:any) => {
      }

      eventSource.onerror = (error: any) => {
        this._zone.run(() => {
          observer.error(error);
        });
      }
      eventSource.onopen = (ev: any) => {
        console.log(ev);
      }
    });
  }

  private getEventSource(url: string): SSE {
    return new SSE(url, {
      method: 'GET'
    });
  }

}
