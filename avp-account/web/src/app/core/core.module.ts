import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import {NgModule} from "@angular/core";

import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import {LeftPanelComponent} from "./layout/leftPanel/leftPanel.component";
import {ComponentModule} from "./component/component.module";
import {PanelItemComponent} from "./layout/leftPanel/comp/panelItem/panelItem.component";
import {TranslateModule} from "@ngx-translate/core";
import {RightPanelComponent} from "./layout/rightPanel/rightPanel.component";
import {CenterPanelComponent} from "./layout/centerPanel/centerPanel.component";
import {
  ApiModule as KeyCloakApiModule
} from "../../generated/keycloak-api";

import {
  ApiModule as WebApiModule
} from "../../generated/web-api";
import {AppListComponent} from "./layout/leftPanel/comp/appList/appList.component";
import {CenterHeaderComponent} from "./layout/centerHeader/centerHeader.component";
import {RouterModule} from "@angular/router";
import { PasswordModalComponent } from './layout/rightPanel/password-modal/password-modal.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    PanelItemComponent,
    LeftPanelComponent,
    RightPanelComponent,
    CenterPanelComponent,
    AppListComponent,
    CenterHeaderComponent,
    PasswordModalComponent
  ],
  imports: [
    ComponentModule,
    TranslateModule,
    WebApiModule,
    KeyCloakApiModule,
    CommonModule,
    BrowserModule,
    RouterModule,
    MatInputModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatButtonModule
    //SecurityService
  ],
  exports: [
    LeftPanelComponent,
    RightPanelComponent,
    CenterPanelComponent,
    CenterHeaderComponent
  ]
})

export class CoreModule {}
