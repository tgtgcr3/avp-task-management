import { AvatarModule } from 'ngx-avatar';
import {NgModule} from "@angular/core";
import {AvatarComponent} from "./avatar/avatar.component";
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  imports:[
    CommonModule,
    BrowserModule,
    AvatarModule
  ],
  declarations: [
    AvatarComponent
  ],
  exports: [
    AvatarComponent
  ]
})

export class ComponentModule {}
