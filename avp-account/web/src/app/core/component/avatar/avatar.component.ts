import { environment } from 'src/environments/environment';
import {Component, Input, OnInit, SimpleChanges} from "@angular/core";

@Component({
  selector: 'one-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: [
    './avatar.component.scss'
  ]
})

export class AvatarComponent{

  @Input()
  username: string;

  @Input()
  size: string;

  @Input()
  imageUrl: string;

  avatarUrl: string;

  style: string;
  constructor() {
    this.initUseravatar();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initUseravatar();
  }

  initUseravatar() {
    if (this.username != null) {
      this.avatarUrl = `/api/pri/user/${this.username}/avatar`;
    } else if (this.imageUrl != null) {
      this.avatarUrl = this.imageUrl;
    } else {
      this.avatarUrl = environment.defaultAvatar;
    }
  }



}
