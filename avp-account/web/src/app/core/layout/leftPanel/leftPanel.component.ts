import { KeyCloakUserService } from './../../../../generated/web-api/api/keyCloakUser.service';
import {Component, Input, OnChanges, OnInit, SimpleChanges} from "@angular/core";
import {SecurityService} from "../../service/security.service";
import {Router} from "@angular/router";
import {KeyCloakService} from "../../service/keycloak.service";
import {MatDialog} from "@angular/material/dialog";
import {AppListComponent} from "./comp/appList/appList.component";

@Component(
  {
    selector: 'one-left-panel',
    templateUrl: './leftPanel.component.html',
    styleUrls: [
      './leftPanel.component.scss'
    ]
  }
)

export class LeftPanelComponent implements OnInit, OnChanges {

  @Input()
  userInfo: any = {}

  currentRoute = '';

  isAdmin = false;

  constructor(
    private securityService: SecurityService,
    private keycloakService: KeyCloakService,
    private userKCService: KeyCloakUserService,
    private router: Router,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
   // this.userInfo = this.keycloakService.getUserInfo() || {};
    // console.log('userinfo');
    // console.log(this.userInfo);
    this.isAdmin = this.keycloakService.isAdmin();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['userInfo'] != null) {
      this.isAdmin = this.keycloakService.isAdmin();
    }
  }

  login() {
    this.securityService.login();
  }

  logout() {
    this.userKCService.logout({
      token: this.keycloakService.getUsername(this.keycloakService.getUserInfo())
    }).toPromise().then((data) => {
      this.securityService.logout();
      this.router.navigate(['/']);
    });

  }

  navigateToMemberList() {
    this.router.navigate(['/members']);
    this.currentRoute = 'members';
  }

  navigateToPersonalInfo() {
    this.router.navigate(['/userDetails', 'me']);
    this.currentRoute = 'personal';
  }

  navigateToActivityLogs() {
    this.router.navigate(['/activity-log']);
    this.currentRoute = 'activity-log';
  }

  showAppList() {
    const dialogRef = this.dialog.open(AppListComponent, {panelClass: 'one-app-dialog'});
    dialogRef.updatePosition({top: '10px', left: '106px'});
    dialogRef.updateSize('770px', 'calc(100% - 20px)');
  }

  navigateToGroups() {
    this.router.navigate(['/management/userGroups']);
    this.currentRoute = 'groups';
  }

  navigateToGuests() {
    this.router.navigate(['/guests']);
    this.currentRoute = 'guests';
  }

  getUsername() {
    return this.keycloakService.getUsername(this.userInfo);
  }


}
