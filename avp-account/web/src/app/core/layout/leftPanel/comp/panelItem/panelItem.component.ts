import {Component, Input} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'one-panel-item',
  templateUrl: './panelItem.component.html',
  styleUrls: [
    './panelItem.component.scss'
  ]
})

export class PanelItemComponent {
  @Input()
  label = '';

  @Input()
  iconClass = '';

  @Input()
  activated = false;

  @Input()
  routerLink: string;
  constructor(
    private translateService: TranslateService
  ) {
  }
}
