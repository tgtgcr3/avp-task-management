import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import {Component, OnInit} from "@angular/core";
import {DOCUMENT} from "@angular/common";
import {SecurityService} from "../../../../service/security.service";
import {environment} from "../../../../../../environments/environment";



@Component({
  selector: 'app-list-one-star',
  templateUrl: './appList.component.html',
  styleUrls: ['./appList.component.scss']
})

export class AppListComponent implements OnInit {

  userInfo: any = {};
  baseWeworkUrl: string = '';

  constructor(
    private keycloakService: KeyCloakService
  ) {
    this.baseWeworkUrl = environment.oneWework;
  }


  ngOnInit(): void {
    this.userInfo = this.keycloakService.getUserInfo() || {};
  }

  goToWeWork() {
    document.location.href = this.baseWeworkUrl;
  }

  goToMessage() {
    document.location.href = environment.oneMessage;
  }

  getFriendlyName() {
    return this.keycloakService.getFullName(this.userInfo);
  }
}
