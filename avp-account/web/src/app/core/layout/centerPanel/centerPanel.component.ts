import { Component, OnInit } from "@angular/core";
import {SecurityService} from "../../service/security.service";

@Component(
  {
    selector: 'one-center-panel',
    templateUrl: './centerPanel.component.html',
    styleUrls: [
      './centerPanel.component.scss'
    ]
  }
)

export class CenterPanelComponent implements OnInit {
  constructor(
    private securityService: SecurityService
  ) {
  }

  ngOnInit() {
  }

}
