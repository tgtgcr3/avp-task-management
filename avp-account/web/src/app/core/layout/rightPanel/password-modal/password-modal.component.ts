import { KeyCloakUserService } from './../../../../../generated/web-api/api/keyCloakUser.service';
import { KeyCloakService } from './../../../service/keycloak.service';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {MustMatch} from '../../../helper/formHelper';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import { textChangeRangeIsUnchanged } from 'typescript';

@Component({
  selector: 'password-modal-component',
  templateUrl: './password-modal.component.html',
  styleUrls: ['./password-modal.component.scss']
})

export class PasswordModalComponent implements OnInit {

  registerForm: FormGroup;

  submitted = false;

  currentUser = '';

  constructor(
    private formBuilder: FormBuilder,
    private keycloakService: KeyCloakService,
    private keycloakUserService: KeyCloakUserService,
    private dialogRef: MatDialogRef<PasswordModalComponent>,
  ) {

  }

  get f() { return this.registerForm.controls; }

  ngOnInit()  {
    this.registerForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],

    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

  }

  submitForm() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    let userinfo = this.keycloakService.getUserInfo();
    if (userinfo != null) {
      let username = userinfo.username || userinfo.preferred_username;
      this.keycloakUserService.updateUserPassword(username, {
        password: this.registerForm.value.currentPassword,
    newPassword:this.registerForm.value.password,
    confirmPassword:this.registerForm.value.password,
      }).toPromise().then(data => {
        this.dialogRef.close(true);
      }).catch(err => {
        alert('Mật Khẩu Không Đúng');
      })
      // this.keycloakService.updateUserPassword(username, this.registerForm.value.currentPassword, this.registerForm.value.password).toPromise()
      //   .then(data => {

      //   }).catch(err => {

      //   });
    }

  }


}
