import { UserService } from './../../../../generated/web-api';
import { PasswordModalComponent } from './password-modal/password-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { KeyCloakService } from "../../service/keycloak.service";
import { UserData } from 'src/generated/web-api';
import { forkJoin } from 'rxjs';
import { MemberCreationComponent } from 'src/app/module/member/component/memberCreation/memberCreation.component';

@Component({
  selector: 'one-right-panel',
  templateUrl: './rightPanel.component.html',
  styleUrls: [
    './rightPanel.component.scss'
  ]
})

export class RightPanelComponent implements OnInit {
  @Input()
  userInfo: any = {};

  isAdmin: boolean = false;

  isExternal: boolean = true;

  fullName: string = '';

  constructor(private keycloak: KeyCloakService,
    private dialog: MatDialog,
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
    // this.userInfo = this.keycloak.getUserInfo() || {};
    this.isAdmin = this.keycloak.isAdmin();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.isAdmin = this.keycloak.isAdmin();
    if (this.userInfo != null) {
      this.fullName = this.keycloak.getFullName(this.userInfo);
      let username = this.keycloak.getUsername(this.userInfo);
      if (username != null && username.length > 0) {
        this.keycloak.getAllUsers(username, true).toPromise().then((users: UserData[]) => {
          if (users.length > 0) {
            this.isExternal = this.keycloak.isFromExternal(users[0]);
          }
        });

      }
    }
  }

  changeUserPassword() {
    this.keycloak.getAllUsers(this.keycloak.getUsername(this.userInfo), false).toPromise().then((users) => {
      if (users != null && users.length > 0) {
        if (this.keycloak.isFromExternal(users[0])) {
          alert('We do not support reset password for your account');
        } else {
          const dialogRef = this.dialog.open(PasswordModalComponent);

          dialogRef.afterClosed().subscribe(result => {
            if (result == true) {
              alert('Đổi mật khẩu thành công');
            }
            console.log(`Dialog result: ${result}`);
          });
        }
      }
    })
  }

  getUsername(userInfo) {
    return this.keycloak.getUsername(userInfo);
  }

  updateCurrentUser() {
    let currentUsername = this.getUsername(this.userInfo);
    forkJoin(this.keycloak.getAllUsers(currentUsername, false),
    this.userService.getAllUsers(currentUsername)
    ).toPromise().then(data => {
      const allUsers = data[0];
      const allUserDatas = data[1].items;
      const userInfo = allUsers.find(user => (user.username || user.preferred_username) == currentUsername);
      const userData = allUserDatas.find(user => user.username == currentUsername);
      if (userInfo != null) {
        this.keycloak.getGroupsByUserId(userInfo.id).toPromise().then(groups => {
          const dialogRef = this.dialog.open(MemberCreationComponent, {
            data: {
              userInfo,
              groups,
              userData
            }

          });

          dialogRef.afterClosed().subscribe(result => {
            if (result == true) {
          //    if (this.isMine == true) {
                window.location.reload();
          //    } else {
          //     this.eventAction.emit('reload');
             }

          //  }
          //  this.router.navigate([], {
          //   skipLocationChange: true,
          //   queryParamsHandling: 'merge' //== if you need to keep queryParams
          // })
          });
        });
      }
    });

  }

}
