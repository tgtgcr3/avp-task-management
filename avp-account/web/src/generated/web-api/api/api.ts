export * from './activity.service';
import { ActivityService } from './activity.service';
export * from './avatar.service';
import { AvatarService } from './avatar.service';
export * from './export.service';
import { ExportService } from './export.service';
export * from './file.service';
import { FileService } from './file.service';
export * from './keyCloakUser.service';
import { KeyCloakUserService } from './keyCloakUser.service';
export * from './notification.service';
import { NotificationService } from './notification.service';
export * from './project.service';
import { ProjectService } from './project.service';
export * from './user.service';
import { UserService } from './user.service';
export * from './userGroup.service';
import { UserGroupService } from './userGroup.service';
export * from './userTitle.service';
import { UserTitleService } from './userTitle.service';
export const APIS = [ActivityService, AvatarService, ExportService, FileService, KeyCloakUserService, NotificationService, ProjectService, UserService, UserGroupService, UserTitleService];
