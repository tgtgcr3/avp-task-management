openapi: 3.0.0
info:
  version: 1.0.0
  title: ONE Account UI API
  contact:
    name: Nhan Nguyen
    email: toannhanb7@gmail.com

  description: >
    Rest API definition for ONE Account UI.

servers:
  - url: /

paths:
  /roles:
    get:
      description: get all roles
      operationId: getRoles
      tags:
        - Role

      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Role'

  /groups:
    get:
      description: get all groups
      operationId: getAllGroups
      tags:
        - Group
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Group'

  /groups/{id}/members:
    parameters:
      - in: path
        name: id
        required: true
        schema:
          type: string
    get:
      description: Get all users of group
      operationId: getUsersOfGroup
      tags:
        - Group
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserSearchData'

  /users:
    get:
      description: Get all users
      operationId: getUsers
      tags:
        - User

      parameters:
        - in: query
          name: username
          required: false
          schema:
            type: string
        - in: query
          name: briefRepresentation
          required: false
          schema:
            type: boolean
        - in: query
          name: first
          required: false
          schema:
            type: integer
        - in: query
          name: max
          required: false
          schema:
            type: integer
        - in: query
          name: search
          required: false
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserSearchData'
    post:
      description: Create User
      tags:
        - User
      operationId: createUser
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserData'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object

  /users/{id}:
    parameters:
      - in: path
        name: id
        required: true
        schema:
          type: string
    put:
        description: update user
        operationId: updateUser
        tags:
          - User 
        requestBody:
          required: true 
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserData'
        responses:
          '200':
            description: OK
            content:
              application/json:
                schema:
                  type: object
          

  /users/{id}/reset-password:
    parameters:
      - in: path
        name: id
        required: true
        schema:
          type: string
    put:
      description: reset-password
      operationId: resetPassword
      tags:
        - User
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserResetPasswordData'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
  /users/{userId}/groups/{groupId}:
    parameters:
      - in: path
        required: true
        name: userId
        schema:
          type: string
      - in: path
        required: true
        name: groupId
        schema:
          type: string
    put:
      description: Assign User to Group
      operationId: setGroup
      tags:
        - User
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
    delete:
      description: Remove User from Group
      operationId: removeGroup
      tags:
        - User
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object

  /users/{userId}/groups:
    parameters:
      - in: path
        required: true
        name: userId
        schema:
          type: string
    get:
      description: Get all available groups
      operationId: getGroupByUserId
      tags:
        - Group
      responses:
        '200':
          description: 'Ok'
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object

  /account/credentials/password:
    post:
      description: update user info
      operationId: selfUpdatePassword
      tags:
        - Account
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/FormChangePassword'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                type: object
  
  /protocol/openid-connect/token:
    post:
      description: exchange token
      operationId: exchangeToken
      tags:
        - Token
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              $ref: '#/components/schemas/ExchangeToken'
      responses:
        '200':
          description: ok
          content:
            application/json:
              schema:
                type: object


#    get:
#    post:
#    delete:






components:
  schemas:
    ExchangeToken:
      type: object
      properties:
        client_id:
          type: string
        client_secret:
          type: string
        grant_type:
          type: string
        subject_token:
          type: string
        requested_token_type:
          type: string
        audience:
          type: string

    FormChangePassword:
      type: object
      properties:
        currentPassword:
          type: string
        newPassword:
          type: string
        confirmation:
          type: string

    Role:
      type: object
      properties:
        attributes:
          type: object
        clientRole:
          type: boolean
        composite:
          type: boolean
        id:
          type: string
        name:
          type: string

    UserSearchData:
      type: array
      items:
        $ref: '#/components/schemas/UserData'

    UserData:
      type: object
      properties:
        id:
          type: string
        username:
          type: string
        lastName:
          type: string
        firstName:
          type: string
        email:
          type: string
        password:
          type: string
        preferred_username:
          type: string
        attributes:
          type: object
        access:
          type: object
        clientConsents:
          type: array
          items:
            type: object
        clientRoles:
          type: object 
        createdTimestamp:
          type: integer
          format: int64
        credentials:
          type: object
        disableableCredentialTypes:
          type: array
          items:
            type: string
        emailVerified:
          type: boolean
        enabled:
          type: boolean
        federatedIdentities:
          type: array
          items:
            type: object 
        federationLink:
          type: string
        groups:
          type: array
          items:
            type: string
        notBefore:
          type: integer
          format: int32
        origin:
          type: string
        realmRoles:
          type: array
          items:
            type: string
        requiredActions:
          type: array
          items:
            type: string
        self:
          type: string
        serviceAccountClientId:
          type: string 
          
          
  


    UserResetPasswordData:
      type: object
      properties:
        type:
          type: string
          enum:
            - password
        temporary:
          type: boolean
        value:
          type: string

    Group:
      type: object
      properties:
        id:
          type: string
        name:
          type: string


#    OAuth2:
#      type: oauth2
#      flows:
#        authorizationCode:
#          authorizationUrl: http://localhost:9999/oauth/authorize
#          tokenUrl: http://localhost:9999/oauth/token
#          scopes:
#            read: Grants
#            write: Grants
#            admin: Grants