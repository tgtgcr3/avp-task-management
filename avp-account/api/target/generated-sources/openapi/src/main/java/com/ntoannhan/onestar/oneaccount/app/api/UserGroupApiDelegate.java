package com.ntoannhan.onestar.oneaccount.app.api;

import com.ntoannhan.onestar.oneaccount.app.model.UserGroup;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link UserGroupApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

public interface UserGroupApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /userGroups
     * Create a new UserGroup
     *
     * @param userGroup  (optional)
     * @return Ok (status code 200)
     * @see UserGroupApi#createUserGroup
     */
    default ResponseEntity<UserGroup> createUserGroup(UserGroup userGroup) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"owner\" : \"owner\", \"members\" : \"members\", \"name\" : \"name\", \"description\" : \"description\", \"id\" : \"id\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * DELETE /userGroup/{id}
     * Delete UserGroup
     *
     * @param id  (required)
     * @return Ok (status code 200)
     * @see UserGroupApi#deleteUserGroup
     */
    default ResponseEntity<Object> deleteUserGroup(String id) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /userGroups/{username}
     * Find UserGroup by Username
     *
     * @param username  (required)
     * @return All User Group (status code 200)
     * @see UserGroupApi#findGroupsByUsingName
     */
    default ResponseEntity<Object> findGroupsByUsingName(String username) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /userGroups
     * Get All UserGroup
     *
     * @return All User Group (status code 200)
     * @see UserGroupApi#getAll
     */
    default ResponseEntity<Object> getAll() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /userGroup/{id}
     * Get UserGroup
     *
     * @param id  (required)
     * @return OK (status code 200)
     * @see UserGroupApi#getUserGroup
     */
    default ResponseEntity<UserGroup> getUserGroup(String id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"owner\" : \"owner\", \"members\" : \"members\", \"name\" : \"name\", \"description\" : \"description\", \"id\" : \"id\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * POST /userGroup/{id}
     * Update UserGroup
     *
     * @param id  (required)
     * @param userGroup  (optional)
     * @return Ok (status code 200)
     * @see UserGroupApi#updateUserGroup
     */
    default ResponseEntity<UserGroup> updateUserGroup(String id,
        UserGroup userGroup) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"owner\" : \"owner\", \"members\" : \"members\", \"name\" : \"name\", \"description\" : \"description\", \"id\" : \"id\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
