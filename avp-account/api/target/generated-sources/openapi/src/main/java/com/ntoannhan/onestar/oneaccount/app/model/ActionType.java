package com.ntoannhan.onestar.oneaccount.app.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets ActionType
 */
public enum ActionType {
  
  LOGIN("Login"),
  
  LOGOUT("Logout"),
  
  UPDATE_PROFILE("Update_Profile"),
  
  CREATE_TASK("Create_Task"),
  
  UPDATE_TASK("Update_Task"),
  
  DELETE_TASK("Delete_Task"),
  
  ASSIGN_TASK("Assign_Task"),
  
  CREATE_PROJECT("Create_Project"),
  
  UPDATE_PROJECT("Update_Project"),
  
  DELETE_PROJECT("Delete_Project");

  private String value;

  ActionType(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static ActionType fromValue(String value) {
    for (ActionType b : ActionType.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

