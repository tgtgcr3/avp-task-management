package com.ntoannhan.onestar.oneaccount.app.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

@Controller
@RequestMapping("${openapi.oNEAccountUI.base-path:/api/pri}")
public class UserGroupApiController implements UserGroupApi {

    private final UserGroupApiDelegate delegate;

    public UserGroupApiController(@org.springframework.beans.factory.annotation.Autowired(required = false) UserGroupApiDelegate delegate) {
        this.delegate = Optional.ofNullable(delegate).orElse(new UserGroupApiDelegate() {});
    }

    @Override
    public UserGroupApiDelegate getDelegate() {
        return delegate;
    }

}
