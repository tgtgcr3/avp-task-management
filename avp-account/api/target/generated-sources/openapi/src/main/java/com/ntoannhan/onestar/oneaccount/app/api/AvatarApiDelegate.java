package com.ntoannhan.onestar.oneaccount.app.api;

import org.springframework.core.io.Resource;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link AvatarApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

public interface AvatarApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * GET /avatars
     *
     * @param usernames  (optional)
     * @return Avatar found (status code 200)
     * @see AvatarApi#findAvatars
     */
    default ResponseEntity<Object> findAvatars(String usernames) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /avatar
     * Avatar Download
     *
     * @param email  (optional)
     * @return File Found (status code 200)
     * @see AvatarApi#getAvatar
     */
    default ResponseEntity<Resource> getAvatar(String email) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
