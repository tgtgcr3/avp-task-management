package com.ntoannhan.onestar.oneaccount.app.api;

import com.ntoannhan.onestar.oneaccount.app.model.Notification;
import com.ntoannhan.onestar.oneaccount.app.model.NotificationQueryResult;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link NotificationApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

public interface NotificationApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /notifications
     * create new notification
     *
     * @param email  (optional)
     * @param notification  (optional)
     * @return Ok (status code 200)
     * @see NotificationApi#createNotification
     */
    default ResponseEntity<Object> createNotification(String email,
        Notification notification) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /notifications
     * Get notification of current user
     *
     * @param page  (optional, default to 0)
     * @param size  (optional, default to 20)
     * @param email  (optional)
     * @return Ok (status code 200)
     * @see NotificationApi#notificationsGet
     */
    default ResponseEntity<NotificationQueryResult> notificationsGet(Integer page,
        Integer size,
        String email) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"total\" : 5, \"items\" : [ { \"owner\" : \"owner\", \"datetime\" : 6, \"targetEmail\" : \"targetEmail\", \"description\" : \"description\", \"id\" : 0, \"title\" : \"title\", \"ownerEmail\" : \"ownerEmail\", \"target\" : \"target\", \"status\" : 1 }, { \"owner\" : \"owner\", \"datetime\" : 6, \"targetEmail\" : \"targetEmail\", \"description\" : \"description\", \"id\" : 0, \"title\" : \"title\", \"ownerEmail\" : \"ownerEmail\", \"target\" : \"target\", \"status\" : 1 } ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * POST /notification/{id}
     * update notification status 0 &#x3D; unread, 1 &#x3D; read, 2 &#x3D; deleted
     *
     * @param id  (required)
     * @param status  (optional)
     * @param email  (optional)
     * @return Ok (status code 200)
     * @see NotificationApi#setNotificationStatus
     */
    default ResponseEntity<Object> setNotificationStatus(Long id,
        Integer status,
        String email) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
