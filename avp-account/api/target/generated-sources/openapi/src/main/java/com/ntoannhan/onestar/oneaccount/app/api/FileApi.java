/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech) (4.3.1).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
package com.ntoannhan.onestar.oneaccount.app.api;

import com.ntoannhan.onestar.oneaccount.app.model.FileInfo;
import org.springframework.core.io.Resource;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

@Validated
@Api(value = "File", description = "the File API")
public interface FileApi {

    default FileApiDelegate getDelegate() {
        return new FileApiDelegate() {};
    }

    /**
     * GET /file/download/{fileId}
     * File Download
     *
     * @param fileId  (required)
     * @return File Found (status code 200)
     */
    @ApiOperation(value = "", nickname = "download", notes = "File Download", response = Resource.class, tags={ "File", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "File Found", response = Resource.class) })
    @RequestMapping(value = "/file/download/{fileId}",
        produces = { "image/png" }, 
        method = RequestMethod.GET)
    default ResponseEntity<Resource> download(@ApiParam(value = "",required=true) @PathVariable("fileId") String fileId) {
        return getDelegate().download(fileId);
    }


    /**
     * POST /file/upload
     * File Upload
     *
     * @param body  (optional)
     * @return OK (status code 200)
     */
    @ApiOperation(value = "", nickname = "upload", notes = "File Upload", response = FileInfo.class, tags={ "File", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = FileInfo.class) })
    @RequestMapping(value = "/file/upload",
        produces = { "application/json" }, 
        consumes = { "image/_*" },
        method = RequestMethod.POST)
    default ResponseEntity<FileInfo> upload(@ApiParam(value = ""  )  @Valid @RequestBody(required = false) String body) {
        return getDelegate().upload(body);
    }

}
