package com.ntoannhan.onestar.oneaccount.app.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

@Controller
@RequestMapping("${openapi.oNEAccountUI.base-path:/api/pri}")
public class ProjectApiController implements ProjectApi {

    private final ProjectApiDelegate delegate;

    public ProjectApiController(@org.springframework.beans.factory.annotation.Autowired(required = false) ProjectApiDelegate delegate) {
        this.delegate = Optional.ofNullable(delegate).orElse(new ProjectApiDelegate() {});
    }

    @Override
    public ProjectApiDelegate getDelegate() {
        return delegate;
    }

}
