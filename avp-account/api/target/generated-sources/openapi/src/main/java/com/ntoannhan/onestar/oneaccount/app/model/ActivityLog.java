package com.ntoannhan.onestar.oneaccount.app.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ntoannhan.onestar.oneaccount.app.model.ActionType;
import com.ntoannhan.onestar.oneaccount.app.model.ActivitySource;
import com.ntoannhan.onestar.oneaccount.app.model.ClientInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ActivityLog
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

public class ActivityLog   {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("action")
  private ActionType action;

  @JsonProperty("source")
  private ActivitySource source;

  @JsonProperty("description")
  private String description;

  @JsonProperty("user")
  private String user;

  @JsonProperty("localId")
  private String localId;

  @JsonProperty("publicIp")
  private String publicIp;

  @JsonProperty("clientInfo")
  private ClientInfo clientInfo;

  @JsonProperty("date")
  private Long date;

  @JsonProperty("projectId")
  private Long projectId;

  @JsonProperty("projectName")
  private String projectName;

  public ActivityLog id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  @ApiModelProperty(value = "")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public ActivityLog action(ActionType action) {
    this.action = action;
    return this;
  }

  /**
   * Get action
   * @return action
  */
  @ApiModelProperty(value = "")

  @Valid

  public ActionType getAction() {
    return action;
  }

  public void setAction(ActionType action) {
    this.action = action;
  }

  public ActivityLog source(ActivitySource source) {
    this.source = source;
    return this;
  }

  /**
   * Get source
   * @return source
  */
  @ApiModelProperty(value = "")

  @Valid

  public ActivitySource getSource() {
    return source;
  }

  public void setSource(ActivitySource source) {
    this.source = source;
  }

  public ActivityLog description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  */
  @ApiModelProperty(value = "")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ActivityLog user(String user) {
    this.user = user;
    return this;
  }

  /**
   * Get user
   * @return user
  */
  @ApiModelProperty(value = "")


  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public ActivityLog localId(String localId) {
    this.localId = localId;
    return this;
  }

  /**
   * Get localId
   * @return localId
  */
  @ApiModelProperty(value = "")


  public String getLocalId() {
    return localId;
  }

  public void setLocalId(String localId) {
    this.localId = localId;
  }

  public ActivityLog publicIp(String publicIp) {
    this.publicIp = publicIp;
    return this;
  }

  /**
   * Get publicIp
   * @return publicIp
  */
  @ApiModelProperty(value = "")


  public String getPublicIp() {
    return publicIp;
  }

  public void setPublicIp(String publicIp) {
    this.publicIp = publicIp;
  }

  public ActivityLog clientInfo(ClientInfo clientInfo) {
    this.clientInfo = clientInfo;
    return this;
  }

  /**
   * Get clientInfo
   * @return clientInfo
  */
  @ApiModelProperty(value = "")

  @Valid

  public ClientInfo getClientInfo() {
    return clientInfo;
  }

  public void setClientInfo(ClientInfo clientInfo) {
    this.clientInfo = clientInfo;
  }

  public ActivityLog date(Long date) {
    this.date = date;
    return this;
  }

  /**
   * Get date
   * @return date
  */
  @ApiModelProperty(readOnly = true, value = "")


  public Long getDate() {
    return date;
  }

  public void setDate(Long date) {
    this.date = date;
  }

  public ActivityLog projectId(Long projectId) {
    this.projectId = projectId;
    return this;
  }

  /**
   * Get projectId
   * @return projectId
  */
  @ApiModelProperty(value = "")


  public Long getProjectId() {
    return projectId;
  }

  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }

  public ActivityLog projectName(String projectName) {
    this.projectName = projectName;
    return this;
  }

  /**
   * Get projectName
   * @return projectName
  */
  @ApiModelProperty(readOnly = true, value = "")


  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ActivityLog activityLog = (ActivityLog) o;
    return Objects.equals(this.id, activityLog.id) &&
        Objects.equals(this.action, activityLog.action) &&
        Objects.equals(this.source, activityLog.source) &&
        Objects.equals(this.description, activityLog.description) &&
        Objects.equals(this.user, activityLog.user) &&
        Objects.equals(this.localId, activityLog.localId) &&
        Objects.equals(this.publicIp, activityLog.publicIp) &&
        Objects.equals(this.clientInfo, activityLog.clientInfo) &&
        Objects.equals(this.date, activityLog.date) &&
        Objects.equals(this.projectId, activityLog.projectId) &&
        Objects.equals(this.projectName, activityLog.projectName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, action, source, description, user, localId, publicIp, clientInfo, date, projectId, projectName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ActivityLog {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    action: ").append(toIndentedString(action)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("    localId: ").append(toIndentedString(localId)).append("\n");
    sb.append("    publicIp: ").append(toIndentedString(publicIp)).append("\n");
    sb.append("    clientInfo: ").append(toIndentedString(clientInfo)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    projectId: ").append(toIndentedString(projectId)).append("\n");
    sb.append("    projectName: ").append(toIndentedString(projectName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

