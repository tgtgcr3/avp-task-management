package com.ntoannhan.onestar.oneaccount.app.api;

import org.springframework.core.io.Resource;
import com.ntoannhan.onestar.oneaccount.app.model.SearchUserData;
import com.ntoannhan.onestar.oneaccount.app.model.UserData;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link UserApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

public interface UserApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /users
     * Create User Data
     *
     * @param userData  (optional)
     * @return OK (status code 200)
     * @see UserApi#createUser
     */
    default ResponseEntity<UserData> createUser(UserData userData) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"birthday\" : 6, \"kcId\" : \"kcId\", \"lastName\" : \"lastName\", \"directMgmts\" : \"directMgmts\", \"avatarUrl\" : \"avatarUrl\", \"experiences\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"studyLevels\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"firstName\" : \"firstName\", \"directMembers\" : \"directMembers\", \"avatarBinary\" : \"avatarBinary\", \"email\" : \"email\", \"contacts\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"rewards\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"username\" : \"username\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /user/{userId}/avatar
     * Avatar Download
     *
     * @param userId  (required)
     * @return File Found (status code 200)
     * @see UserApi#downloadAvatar
     */
    default ResponseEntity<Resource> downloadAvatar(String userId) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /users
     * Get all UserData
     *
     * @param username  (optional)
     * @return OK (status code 200)
     * @see UserApi#getAllUsers
     */
    default ResponseEntity<SearchUserData> getAllUsers(String username) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"total\" : 0, \"items\" : [ { \"birthday\" : 6, \"kcId\" : \"kcId\", \"lastName\" : \"lastName\", \"directMgmts\" : \"directMgmts\", \"avatarUrl\" : \"avatarUrl\", \"experiences\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"studyLevels\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"firstName\" : \"firstName\", \"directMembers\" : \"directMembers\", \"avatarBinary\" : \"avatarBinary\", \"email\" : \"email\", \"contacts\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"rewards\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"username\" : \"username\" }, { \"birthday\" : 6, \"kcId\" : \"kcId\", \"lastName\" : \"lastName\", \"directMgmts\" : \"directMgmts\", \"avatarUrl\" : \"avatarUrl\", \"experiences\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"studyLevels\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"firstName\" : \"firstName\", \"directMembers\" : \"directMembers\", \"avatarBinary\" : \"avatarBinary\", \"email\" : \"email\", \"contacts\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"rewards\" : [ { \"name\" : \"name\", \"description\" : \"description\" }, { \"name\" : \"name\", \"description\" : \"description\" } ], \"username\" : \"username\" } ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
