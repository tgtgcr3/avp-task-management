package com.ntoannhan.onestar.oneaccount.app.api;

import com.ntoannhan.onestar.oneaccount.app.model.InlineObject;
import com.ntoannhan.onestar.oneaccount.app.model.KeyCloakUser;
import com.ntoannhan.onestar.oneaccount.app.model.UserPassword;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link KeyCloakUserApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

public interface KeyCloakUserApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /keycloak/user/{id}/activate
     * activated users
     *
     * @param id  (required)
     * @param activated  (required)
     * @return Ok (status code 200)
     * @see KeyCloakUserApi#enableUser
     */
    default ResponseEntity<Object> enableUser(String id,
        Boolean activated) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /keycloak/exportUser
     * export all user
     *
     * @return Ok (status code 200)
     * @see KeyCloakUserApi#exportUsers
     */
    default ResponseEntity<Object> exportUsers() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /internal/userinfo
     * update keycloak user
     *
     * @param username  (optional)
     * @param email  (optional)
     * @return Ok (status code 200)
     * @see KeyCloakUserApi#getKeyCloakUser
     */
    default ResponseEntity<Object> getKeyCloakUser(String username,
        String email) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /keycloakUser/count
     * get total user
     *
     * @return Ok (status code 200)
     * @see KeyCloakUserApi#getTotalUsers
     */
    default ResponseEntity<Object> getTotalUsers() {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /keycloak/users
     * find users
     *
     * @param fullName  (optional)
     * @param manager  (optional)
     * @param disabled  (optional)
     * @param guest  (optional)
     * @param userArray  (optional)
     * @param offset  (optional, default to 0)
     * @param size  (optional, default to 20)
     * @return Ok (status code 200)
     * @see KeyCloakUserApi#getUsers
     */
    default ResponseEntity<Object> getUsers(String fullName,
        Boolean manager,
        Boolean disabled,
        Boolean guest,
        String userArray,
        Integer offset,
        Integer size) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * POST /keycloak/user/logout
     * Logout user
     *
     * @param inlineObject  (optional)
     * @return Ok (status code 200)
     * @see KeyCloakUserApi#logout
     */
    default ResponseEntity<Object> logout(InlineObject inlineObject) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * POST /keycloakUser/{id}
     * update keycloak user
     *
     * @param id  (required)
     * @param keyCloakUser  (optional)
     * @return Ok (status code 200)
     * @see KeyCloakUserApi#updateKeyCloakUser
     */
    default ResponseEntity<Object> updateKeyCloakUser(String id,
        KeyCloakUser keyCloakUser) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * POST /keycloak/user/{username}/password
     * Update userpassword
     *
     * @param username  (required)
     * @param userPassword  (optional)
     * @return Ok (status code 200)
     * @see KeyCloakUserApi#updateUserPassword
     */
    default ResponseEntity<Object> updateUserPassword(String username,
        UserPassword userPassword) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
