package com.ntoannhan.onestar.oneaccount.app.api;

import com.ntoannhan.onestar.oneaccount.app.model.BooleanResponse;
import com.ntoannhan.onestar.oneaccount.app.model.RestError;
import com.ntoannhan.onestar.oneaccount.app.model.SearchUserTitleData;
import com.ntoannhan.onestar.oneaccount.app.model.UserTitleData;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link UserTitleApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

public interface UserTitleApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /userTitles
     * Create new user title
     *
     * @param userTitleData UserTitle details (required)
     * @return Ok (status code 200)
     *         or Duplicated (status code 400)
     * @see UserTitleApi#createUserTitle
     */
    default ResponseEntity<UserTitleData> createUserTitle(UserTitleData userTitleData) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"name\" : \"name\", \"id\" : 6 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /userTitles
     * Get all user title
     *
     * @return All User Title (status code 200)
     * @see UserTitleApi#getAllUserTitles
     */
    default ResponseEntity<SearchUserTitleData> getAllUserTitles() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"total\" : 0, \"items\" : [ { \"name\" : \"name\", \"id\" : 6 }, { \"name\" : \"name\", \"id\" : 6 } ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * DELETE /userTitles/{id}
     * Delete a user title
     *
     * @param id  (required)
     * @return OK (status code 200)
     *         or User title in-use (status code 400)
     * @see UserTitleApi#removeUserTitle
     */
    default ResponseEntity<BooleanResponse> removeUserTitle(Long id) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"result\" : true, \"description\" : \"description\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * POST /userTitles/{id}
     * Update user titile
     *
     * @param id  (required)
     * @param userTitleData  (optional)
     * @return Ok (status code 200)
     *         or Duplicated (status code 400)
     * @see UserTitleApi#updateUserTitle
     */
    default ResponseEntity<UserTitleData> updateUserTitle(Long id,
        UserTitleData userTitleData) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"name\" : \"name\", \"id\" : 6 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
