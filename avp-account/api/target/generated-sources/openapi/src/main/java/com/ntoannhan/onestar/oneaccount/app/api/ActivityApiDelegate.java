package com.ntoannhan.onestar.oneaccount.app.api;

import com.ntoannhan.onestar.oneaccount.app.model.ActionType;
import com.ntoannhan.onestar.oneaccount.app.model.ActivityLog;
import com.ntoannhan.onestar.oneaccount.app.model.ActivityLogGetAllResult;
import com.ntoannhan.onestar.oneaccount.app.model.ActivitySource;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A delegate to be called by the {@link ActivityApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

public interface ActivityApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /internal/activitylogs : Add an Activity Log
     *
     * @param activityLog  (optional)
     * @return Ok (status code 200)
     * @see ActivityApi#createActivity
     */
    default ResponseEntity<ActivityLog> createActivity(ActivityLog activityLog) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"date\" : 6, \"description\" : \"description\", \"clientInfo\" : { \"hostname\" : \"hostname\", \"browser\" : \"browser\", \"ipAddress\" : \"ipAddress\" }, \"publicIp\" : \"publicIp\", \"id\" : 0, \"projectName\" : \"projectName\", \"user\" : \"user\", \"localId\" : \"localId\", \"projectId\" : 1 }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /activitylogs/{type} : Get all activity logs
     *
     * @param type  (required)
     * @param user  (optional)
     * @param page  (optional, default to 0)
     * @param size  (optional, default to 30)
     * @param action  (optional)
     * @param source  (optional)
     * @param from  (optional)
     * @param to  (optional)
     * @param projectId  (optional)
     * @return ok (status code 200)
     * @see ActivityApi#getActivityLogs
     */
    default ResponseEntity<ActivityLogGetAllResult> getActivityLogs(String type,
        String user,
        Integer page,
        Integer size,
        ActionType action,
        ActivitySource source,
        Long from,
        Long to,
        Long projectId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"total\" : 0, \"items\" : [ { \"date\" : 6, \"description\" : \"description\", \"clientInfo\" : { \"hostname\" : \"hostname\", \"browser\" : \"browser\", \"ipAddress\" : \"ipAddress\" }, \"publicIp\" : \"publicIp\", \"id\" : 0, \"projectName\" : \"projectName\", \"user\" : \"user\", \"localId\" : \"localId\", \"projectId\" : 1 }, { \"date\" : 6, \"description\" : \"description\", \"clientInfo\" : { \"hostname\" : \"hostname\", \"browser\" : \"browser\", \"ipAddress\" : \"ipAddress\" }, \"publicIp\" : \"publicIp\", \"id\" : 0, \"projectName\" : \"projectName\", \"user\" : \"user\", \"localId\" : \"localId\", \"projectId\" : 1 } ] }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
