package com.ntoannhan.onestar.oneaccount.app.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ntoannhan.onestar.oneaccount.app.model.UserExtraData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * UserData
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-08-05T09:56:47.773402100+07:00[Asia/Bangkok]")

public class UserData   {
  @JsonProperty("username")
  private String username;

  @JsonProperty("email")
  private String email;

  @JsonProperty("avatarBinary")
  private String avatarBinary;

  @JsonProperty("birthday")
  private Long birthday;

  @JsonProperty("avatarUrl")
  private String avatarUrl;

  @JsonProperty("contacts")
  @Valid
  private List<UserExtraData> contacts = null;

  @JsonProperty("studyLevels")
  @Valid
  private List<UserExtraData> studyLevels = null;

  @JsonProperty("rewards")
  @Valid
  private List<UserExtraData> rewards = null;

  @JsonProperty("experiences")
  @Valid
  private List<UserExtraData> experiences = null;

  @JsonProperty("directMgmts")
  private String directMgmts;

  @JsonProperty("directMembers")
  private String directMembers;

  @JsonProperty("kcId")
  private String kcId;

  @JsonProperty("firstName")
  private String firstName;

  @JsonProperty("lastName")
  private String lastName;

  public UserData username(String username) {
    this.username = username;
    return this;
  }

  /**
   * Get username
   * @return username
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public UserData email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  */
  @ApiModelProperty(value = "")


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public UserData avatarBinary(String avatarBinary) {
    this.avatarBinary = avatarBinary;
    return this;
  }

  /**
   * Get avatarBinary
   * @return avatarBinary
  */
  @ApiModelProperty(value = "")


  public String getAvatarBinary() {
    return avatarBinary;
  }

  public void setAvatarBinary(String avatarBinary) {
    this.avatarBinary = avatarBinary;
  }

  public UserData birthday(Long birthday) {
    this.birthday = birthday;
    return this;
  }

  /**
   * Get birthday
   * @return birthday
  */
  @ApiModelProperty(value = "")


  public Long getBirthday() {
    return birthday;
  }

  public void setBirthday(Long birthday) {
    this.birthday = birthday;
  }

  public UserData avatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
    return this;
  }

  /**
   * Get avatarUrl
   * @return avatarUrl
  */
  @ApiModelProperty(value = "")


  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public UserData contacts(List<UserExtraData> contacts) {
    this.contacts = contacts;
    return this;
  }

  public UserData addContactsItem(UserExtraData contactsItem) {
    if (this.contacts == null) {
      this.contacts = new ArrayList<>();
    }
    this.contacts.add(contactsItem);
    return this;
  }

  /**
   * Get contacts
   * @return contacts
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<UserExtraData> getContacts() {
    return contacts;
  }

  public void setContacts(List<UserExtraData> contacts) {
    this.contacts = contacts;
  }

  public UserData studyLevels(List<UserExtraData> studyLevels) {
    this.studyLevels = studyLevels;
    return this;
  }

  public UserData addStudyLevelsItem(UserExtraData studyLevelsItem) {
    if (this.studyLevels == null) {
      this.studyLevels = new ArrayList<>();
    }
    this.studyLevels.add(studyLevelsItem);
    return this;
  }

  /**
   * Get studyLevels
   * @return studyLevels
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<UserExtraData> getStudyLevels() {
    return studyLevels;
  }

  public void setStudyLevels(List<UserExtraData> studyLevels) {
    this.studyLevels = studyLevels;
  }

  public UserData rewards(List<UserExtraData> rewards) {
    this.rewards = rewards;
    return this;
  }

  public UserData addRewardsItem(UserExtraData rewardsItem) {
    if (this.rewards == null) {
      this.rewards = new ArrayList<>();
    }
    this.rewards.add(rewardsItem);
    return this;
  }

  /**
   * Get rewards
   * @return rewards
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<UserExtraData> getRewards() {
    return rewards;
  }

  public void setRewards(List<UserExtraData> rewards) {
    this.rewards = rewards;
  }

  public UserData experiences(List<UserExtraData> experiences) {
    this.experiences = experiences;
    return this;
  }

  public UserData addExperiencesItem(UserExtraData experiencesItem) {
    if (this.experiences == null) {
      this.experiences = new ArrayList<>();
    }
    this.experiences.add(experiencesItem);
    return this;
  }

  /**
   * Get experiences
   * @return experiences
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<UserExtraData> getExperiences() {
    return experiences;
  }

  public void setExperiences(List<UserExtraData> experiences) {
    this.experiences = experiences;
  }

  public UserData directMgmts(String directMgmts) {
    this.directMgmts = directMgmts;
    return this;
  }

  /**
   * Get directMgmts
   * @return directMgmts
  */
  @ApiModelProperty(value = "")


  public String getDirectMgmts() {
    return directMgmts;
  }

  public void setDirectMgmts(String directMgmts) {
    this.directMgmts = directMgmts;
  }

  public UserData directMembers(String directMembers) {
    this.directMembers = directMembers;
    return this;
  }

  /**
   * Get directMembers
   * @return directMembers
  */
  @ApiModelProperty(value = "")


  public String getDirectMembers() {
    return directMembers;
  }

  public void setDirectMembers(String directMembers) {
    this.directMembers = directMembers;
  }

  public UserData kcId(String kcId) {
    this.kcId = kcId;
    return this;
  }

  /**
   * Get kcId
   * @return kcId
  */
  @ApiModelProperty(value = "")


  public String getKcId() {
    return kcId;
  }

  public void setKcId(String kcId) {
    this.kcId = kcId;
  }

  public UserData firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  /**
   * Get firstName
   * @return firstName
  */
  @ApiModelProperty(value = "")


  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public UserData lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  /**
   * Get lastName
   * @return lastName
  */
  @ApiModelProperty(value = "")


  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserData userData = (UserData) o;
    return Objects.equals(this.username, userData.username) &&
        Objects.equals(this.email, userData.email) &&
        Objects.equals(this.avatarBinary, userData.avatarBinary) &&
        Objects.equals(this.birthday, userData.birthday) &&
        Objects.equals(this.avatarUrl, userData.avatarUrl) &&
        Objects.equals(this.contacts, userData.contacts) &&
        Objects.equals(this.studyLevels, userData.studyLevels) &&
        Objects.equals(this.rewards, userData.rewards) &&
        Objects.equals(this.experiences, userData.experiences) &&
        Objects.equals(this.directMgmts, userData.directMgmts) &&
        Objects.equals(this.directMembers, userData.directMembers) &&
        Objects.equals(this.kcId, userData.kcId) &&
        Objects.equals(this.firstName, userData.firstName) &&
        Objects.equals(this.lastName, userData.lastName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(username, email, avatarBinary, birthday, avatarUrl, contacts, studyLevels, rewards, experiences, directMgmts, directMembers, kcId, firstName, lastName);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserData {\n");
    
    sb.append("    username: ").append(toIndentedString(username)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    avatarBinary: ").append(toIndentedString(avatarBinary)).append("\n");
    sb.append("    birthday: ").append(toIndentedString(birthday)).append("\n");
    sb.append("    avatarUrl: ").append(toIndentedString(avatarUrl)).append("\n");
    sb.append("    contacts: ").append(toIndentedString(contacts)).append("\n");
    sb.append("    studyLevels: ").append(toIndentedString(studyLevels)).append("\n");
    sb.append("    rewards: ").append(toIndentedString(rewards)).append("\n");
    sb.append("    experiences: ").append(toIndentedString(experiences)).append("\n");
    sb.append("    directMgmts: ").append(toIndentedString(directMgmts)).append("\n");
    sb.append("    directMembers: ").append(toIndentedString(directMembers)).append("\n");
    sb.append("    kcId: ").append(toIndentedString(kcId)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

