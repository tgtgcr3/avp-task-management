<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author></Author>
  <LastAuthor></LastAuthor>
  <Created>2017-11-05T03:48:32Z</Created>
  <LastSaved>2021-04-10T08:06:08Z</LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>12570</WindowHeight>
  <WindowWidth>23250</WindowWidth>
  <WindowTopX>32767</WindowTopX>
  <WindowTopY>32767</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16" ss:Name="Good">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#006100"/>
   <Interior ss:Color="#C6EFCE" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s17" ss:Name="Neutral">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#9C5700"/>
   <Interior ss:Color="#FFEB9C" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s18">
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s19">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Times New Roman" x:Family="Roman" ss:Size="11"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s20">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Times New Roman" x:Family="Roman" ss:Size="12"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s21" ss:Parent="s16">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#006100"
    ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s22" ss:Parent="s17">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#9C5700"
    ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s23" ss:Parent="s17">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#9C5700"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s24" ss:Parent="s16">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#006100"
    ss:Bold="1"/>
   <Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s25" ss:Parent="s16">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#006100"
    ss:Bold="1"/>
   <Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s26" ss:Parent="s16">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#006100"
    ss:Bold="1"/>
   <Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s27">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s28">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s29">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s30" ss:Parent="s17">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#9C5700"
    ss:Bold="1"/>
   <Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s31" ss:Parent="s17">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#9C5700"
    ss:Bold="1"/>
   <Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s32" ss:Parent="s17">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#9C5700"
    ss:Bold="1"/>
   <Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s33">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s36" ss:Parent="s16">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#006100"
    ss:Bold="1"/>
   <Interior ss:Color="#FFC000" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s37" ss:Parent="s16">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#006100"
    ss:Bold="1"/>
   <Interior ss:Color="#FFC000" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="@"/>
  </Style>
  <Style ss:ID="s38" ss:Parent="s16">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#006100"
    ss:Bold="1"/>
   <Interior ss:Color="#FFC000" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s39" ss:Parent="s16">
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#006100"
    ss:Bold="1"/>
   <Interior ss:Color="#FFC000" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s40">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Font ss:FontName="Times New Roman" x:Family="Roman" ss:Size="14"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s41">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Times New Roman" x:Family="Roman" ss:Size="16"
    ss:Color="#00B050" ss:Bold="1"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Ví du">
  <Names>
   <NamedRange ss:Name="Print_Area" ss:RefersTo="='Ví du'!R1C1:R48C10"/>
  </Names>
  <Table ss:ExpandedColumnCount="10" ss:ExpandedRowCount="48" x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="46.5" ss:DefaultRowHeight="15">
   <Column ss:AutoFitWidth="0" ss:Width="241.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="95.25" ss:Span="1"/>
   <Column ss:Index="4" ss:AutoFitWidth="0" ss:Width="56.25"/>
   <Column ss:StyleID="s18" ss:AutoFitWidth="0" ss:Width="72"/>
   <Column ss:StyleID="s18" ss:AutoFitWidth="0" ss:Width="74.25"/>
   <Column ss:StyleID="s18" ss:Width="103.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="111"/>
   <Column ss:Width="117.75"/>
   <Column ss:Width="162.75"/>
   <Row ss:AutoFitHeight="0" ss:Height="77.4375" ss:StyleID="s19">
    <Cell ss:MergeAcross="9" ss:StyleID="s40"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><B><Font html:Size="16"
        html:Color="#000000">TẬP ĐOÀN AN VIỆT PHÁT</Font></B><Font
       html:Color="#000000">&#10;                                </Font><Font
       html:Size="12" html:Color="#002060">Địa chỉ: Số 62-70, Đường B4, Khu Đô Thị Sala, Phường An Lợi Đông, Quận 2, TP. HCM, Việt Nam&#10;                                Điện thoại: 028 3636 4828  -  Fax: 028 3636 4881 - Website: www.anvietenergy.com</Font></ss:Data><NamedCell
      ss:Name="Print_Area"/></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="39" ss:StyleID="s19">
    <Cell ss:MergeAcross="9" ss:StyleID="s41"><Data ss:Type="String">${reportTitle}</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.125" ss:StyleID="s20">
    <Cell ss:StyleID="s23"><Data ss:Type="String">Bộ phận: ${department}</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">Người lập: ${reporter}</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s23"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s23"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">Chức vụ: ${title}</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s23"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s23"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s23"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s23"><NamedCell ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s23"><NamedCell ss:Name="Print_Area"/></Cell>
   </Row>
   <Row ss:Height="15.75" ss:StyleID="s21">
    <Cell ss:StyleID="s24"><Data ss:Type="String">Tên công việc</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Người thực hiện</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Người theo dõi</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Ưu tiên</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s25"><Data ss:Type="String">Ngày bắt đầu</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s25"><Data ss:Type="String">Deadline</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s25"><Data ss:Type="String">Hoàn thành thực tế</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Mô tả công việc</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Trạng thái</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Kết quả công việc</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
   </Row>
   <Row ss:Height="15.75" ss:StyleID="s39">
    <Cell ss:StyleID="s36"><Data ss:Type="String">Tên công việc</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s36"><Data ss:Type="String">Tên người tao ra CV</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s36"><Data ss:Type="String">Tên người theo dõi</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s36"><Data ss:Type="String">Loại Khẩn cấp hay quan trọng</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s37"><Data ss:Type="String">Ngày bd CV</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s37"><Data ss:Type="String">Ngày KT CV</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s37"><Data ss:Type="String">Ngày click vào ht</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s36"><Data ss:Type="String">Nội dung CV</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s36"><Data ss:Type="String">Trạng thái CV hiện tại</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
    <Cell ss:StyleID="s38"><Data ss:Type="String">KQCV kế chỗ tài liệu đính kèm</Data><NamedCell
      ss:Name="Print_Area"/></Cell>
   </Row>
   <#list data as project>
	<Row ss:Height="15.75" ss:StyleID="s39">
		<Cell ss:StyleID="s36"><Data ss:Type="String">${project.project_name}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String"></Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String"></Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String"></Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s37"><Data ss:Type="String"></Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s37"><Data ss:Type="String"></Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s37"><Data ss:Type="String"></Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String"></Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String"></Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s38"><Data ss:Type="String"></Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
   </Row>
   <#list project.work_packages as task>
	<Row ss:Height="15.75" ss:StyleID="s39">
		<Cell ss:StyleID="s36"><Data ss:Type="String">${task.subject}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String">${task.assignee}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String">${task.author}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String">${task.watchers}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s37"><Data ss:Type="String">${task.is_urgent?string("yes", "no")}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s37"><Data ss:Type="String">${task.start_date}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s37"><Data ss:Type="String">${task.due_date}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String">${task.finish_date}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s36"><Data ss:Type="String">${task.description}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
		<Cell ss:StyleID="s38"><Data ss:Type="String">${task.status}</Data><NamedCell
		  ss:Name="Print_Area"/></Cell>
   </Row>
   </#list>
   </#list>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <Scale>46</Scale>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>600</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>18</ActiveRow>
     <ActiveCol>7</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
  <DataValidation xmlns="urn:schemas-microsoft-com:office:excel">
   <Range>R32C3:R39C3,R6C4:R1048576C4</Range>
   <Type>List</Type>
   <CellRangeList/>
   <Value>&quot;Yes, No&quot;</Value>
  </DataValidation>
  <DataValidation xmlns="urn:schemas-microsoft-com:office:excel">
   <Range>R6C9:R1048576C9</Range>
   <Type>List</Type>
   <CellRangeList/>
   <Value>&quot;Done, Doing&quot;</Value>
  </DataValidation>
 </Worksheet>
</Workbook>
