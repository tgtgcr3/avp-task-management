package com.ntoannhan.onestar.oneaccount.app.repository;

import com.ntoannhan.onestar.oneaccount.app.persistence.OneUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OneUserRepository extends JpaRepository<OneUser, String> {

    @Query("Select ou from OneUser ou Where ou.directManagers LIKE CONCAT('%',:username,'%')")
    List<OneUser> findDirectMember(@Param("username") String username);

    @Query("Select ou from OneUser ou Where ou.email = :email")
    OneUser findUserByEmail(@Param("email") String email);

    // @Query("Select ou from OneUser ou Where ou.id in :usernames")
    // List<OneUser> findUsersByIds(@Param("usernames") String[] usernames);

}
