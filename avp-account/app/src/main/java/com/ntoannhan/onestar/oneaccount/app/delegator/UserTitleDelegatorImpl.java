package com.ntoannhan.onestar.oneaccount.app.delegator;

import com.ntoannhan.onestar.oneaccount.app.api.UserTitleApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.model.BooleanResponse;
import com.ntoannhan.onestar.oneaccount.app.model.RestError;
import com.ntoannhan.onestar.oneaccount.app.model.SearchUserTitleData;
import com.ntoannhan.onestar.oneaccount.app.model.UserTitleData;
import com.ntoannhan.onestar.oneaccount.app.persistence.OneUserTitle;
import com.ntoannhan.onestar.oneaccount.app.repository.OneUserTitleRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserTitleDelegatorImpl implements UserTitleApiDelegate {

    @Autowired
    private OneUserTitleRepository m_repo;

    private static final Logger m_logger = LogManager.getLogger(UserTitleDelegatorImpl.class);

    @Override
    public ResponseEntity<UserTitleData> createUserTitle(UserTitleData body) {

        OneUserTitle userTitle = m_repo.findByName(body.getName());
        m_logger.debug("Create userTitle " + body.getName());
        if (userTitle != null) {
            m_logger.error("UserTitle exists");
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST
            );
        }
        userTitle = new OneUserTitle();
        userTitle.setName(body.getName());
        m_repo.saveAndFlush(userTitle);
        body.setId(userTitle.getId());
        return ResponseEntity.ok(body);
    }

    @Override
    public ResponseEntity<BooleanResponse> removeUserTitle(Long id) {
        Optional<OneUserTitle> userTitleOpt = m_repo.findById(id);
        if (userTitleOpt.isPresent()) {
            if (userTitleOpt.get().getUsers().isEmpty()) {
                m_repo.deleteById(id);
                return ResponseEntity.ok(new BooleanResponse().result(true));
            }
        }
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST
        );
    }

    @Override
    public ResponseEntity<UserTitleData> updateUserTitle(Long id, UserTitleData userTitleData) {
        Optional<OneUserTitle> userTitleOpt = m_repo.findById(id);
        if (userTitleOpt.isPresent()) {
            OneUserTitle userTitle = userTitleOpt.get();;
            userTitle.setName(userTitleData.getName());
            m_repo.saveAndFlush(userTitle);
            userTitleData.setId(id);
            return ResponseEntity.ok(userTitleData);
        }
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST
        );
    }

    @Override
    public ResponseEntity<SearchUserTitleData> getAllUserTitles() {
        List<OneUserTitle> userTitles = m_repo.findAll();
        return ResponseEntity.ok(new SearchUserTitleData().items(userTitles.stream().map(ut -> new UserTitleData().id(ut.getId()).name(ut.getName())).collect(Collectors.toList())));
    }
}
