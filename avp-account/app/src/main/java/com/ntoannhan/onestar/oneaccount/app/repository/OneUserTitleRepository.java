package com.ntoannhan.onestar.oneaccount.app.repository;

import com.ntoannhan.onestar.oneaccount.app.persistence.OneUserTitle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OneUserTitleRepository extends JpaRepository<OneUserTitle, Long> {

    @Query("select ut from OneUserTitle ut where ut.name = ?1")
    OneUserTitle findByName(String name);

}
