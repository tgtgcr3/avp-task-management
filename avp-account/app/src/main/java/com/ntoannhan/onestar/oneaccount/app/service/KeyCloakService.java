package com.ntoannhan.onestar.oneaccount.app.service;

import com.ntoannhan.onestar.oneaccount.app.service.filestorage.KeycloakProperties;

import com.ntoannhan.onestar.oneaccount.app.service.props.ServerProp;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class KeyCloakService {
  
  
  private KeycloakProperties m_prop;

  private ServerProp m_serverProp;

  private Keycloak m_keycloak;

  private final RestTemplate m_restTemplate;

  @Autowired
  public KeyCloakService(KeycloakProperties prop, RestTemplateBuilder templateBuilder, ServerProp serverProp) {
    m_prop = prop;
    m_keycloak = KeycloakBuilder.builder().serverUrl(prop.getAuthServerUrl()).realm("an-viet-phat")
    .grantType(OAuth2Constants.CLIENT_CREDENTIALS) 
    .clientId("avp-account")
    .clientSecret("1d637943-21c0-4c01-ace4-19840dc2f0da")        
    .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();
    m_restTemplate = templateBuilder.build();
    m_serverProp = serverProp;
  }

  public Keycloak getKeycloak() {
    return m_keycloak;
  }

  public RestTemplate getRestTemplate() {
    return m_restTemplate;
  }

  public String getMessageUrl() {
    return m_serverProp.getMessageUrl();
  }

  public String getTaskUrl() {
    return m_serverProp.getTaskUrl();
  }

  public String getNotificationUrl() {
    return m_serverProp.getNotificationUrl();
  }

  public String getTaskToken() {
    return m_serverProp.getTaskToken();
  }

  public int getDefaultProjectId() {
    return m_serverProp.getTaskProjectDefaultId();
  }

  public int getDefaultRoleId() {
    return m_serverProp.getTaskDefaultRoleId();
  }

  public String getRequestUrl() {
    return m_serverProp.getRequestUrl();
  }

  public String getRequestToken() {
    return m_serverProp.getRequestToken();
  }
}
