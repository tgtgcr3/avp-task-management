package com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.persistence;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "rocketchat_subscription")
public class RocketchatSubscription {

    @Id
    private String _id;

    private String name;

    private String fname;

    private String t;

    public RocketchatSubscription() {
    }

    public RocketchatSubscription(String _id, String name, String fname, String t) {
        this._id = _id;
        this.name = name;
        this.fname = fname;
        this.t = t;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }
}
