package com.ntoannhan.onestar.oneaccount.app.service.importexport.strategy;

import com.ntoannhan.onestar.oneaccount.app.core.failure.Failure;
import io.vavr.control.Either;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class WeworkReportExportStrategy implements ExportStrategy{

    private static final List<String> headers = Arrays.asList(
            "subject", "assignee", "author", "watchers", "is_urgent", "start_date", "due_date", "finish_date", "description", "status", "result"
    );

    @Override
    public byte[] export(String templateName, Map<String, Object> data) {
        Resource resource = new ClassPathResource("templates/wework_report.xlsx");
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(resource.getInputStream());
            XSSFSheet sheet = workbook.getSheetAt(0);

            XSSFRow row = sheet.getRow(1);
            XSSFCell cell = row.getCell(0);
            cell.setCellValue((String) data.get("reportTitle"));

            row = sheet.getRow(2);
            cell = row.getCell(0);
            String cellValue = cell.getStringCellValue();
            cell.setCellValue(cellValue + data.get("department"));

            cell = row.getCell(1);
            cellValue = cell.getStringCellValue();
            cell.setCellValue(cellValue + data.get("reporter"));

            cell = row.getCell(5);
            cellValue = cell.getStringCellValue();
            cell.setCellValue(cellValue + data.get("title"));

            List<Map<String, Object>> projects = (List<Map<String, Object>>) data.get("data");

            writeProjectsData(sheet,  projects);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void writeProjectsData(XSSFSheet sheet, List<Map<String, Object>> projects) {
        int rowIndex = 4;
        XSSFRow row;
        XSSFCell cell;
        row = sheet.getRow(3);
        if (row == null) {
            row = sheet.createRow(3);
        }
        cell = row.getCell(0);
        if (cell == null) {
            cell = row.createCell(0);
        }
        XSSFCellStyle cellStyle = cell.getCellStyle();
        XSSFCellStyle projectStyle = sheet.getWorkbook().createCellStyle();
        projectStyle.cloneStyleFrom(cellStyle);
        XSSFFont font = sheet.getWorkbook().createFont();

        font.setColor(new XSSFColor(new byte[]{(byte) 0, (byte) 0, (byte) 0}, null));
        font.setFontHeight(12);
        font.setBold(true);
        projectStyle.setFont(font);


        for (Map<String, Object> project : projects) {
            row = sheet.getRow(rowIndex++);
            if (row == null) {
                row = sheet.createRow(rowIndex - 1);
            }
            cell = row.getCell(0);
            if (cell == null) {
                cell = row.createCell(0);
            }
            cell.setCellValue((String) project.get("project_name"));
            int colIndex = 0;
            for (int i = 0; i < headers.size(); i++) {
                cell = row.getCell(colIndex++);
                if (cell == null) {
                    cell = row.createCell(colIndex - 1);
                }
                cell.setCellStyle(projectStyle);
            }


            List<Map<String, Object>> tasks = (List<Map<String, Object>>) project.get("work_packages");
            for (Map<String, Object> task : tasks) {
                colIndex = 0;
                String header;
                row = sheet.getRow(rowIndex++);
                if (row == null) {
                    row = sheet.createRow(rowIndex - 1);
                }
                for (int i = 0; i < headers.size(); i++) {
                    header = headers.get(colIndex);
                    cell = row.getCell(colIndex++);
                    if (cell == null) {
                        cell = row.createCell(colIndex - 1);
                    }
                    cell.setCellValue(
                            convertValue(header, task.get(header))
                    );
                }

            }

        }

    }

    @Override
    public String convertValue(String header, Object value) {
        if ("is_urgent".equals(header)) {
            return ((boolean) value) ? "Yes" : "No";
        }
        return String.valueOf(value);
    }
}
