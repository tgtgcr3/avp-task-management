package com.ntoannhan.onestar.oneaccount.app.persistence;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private int action;

    private int source;

    private String description;

    private String submitter;

    private long date;

    private String localIp;

    private String browserName;

    private String hostname;

    private String publicIp;

    private Long projectId;

}
