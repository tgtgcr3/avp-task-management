package com.ntoannhan.onestar.oneaccount.app.domain.model;

import com.google.common.base.Objects;

public enum ActivityType {

    General("general"), Task("task");


    private final String value;

    ActivityType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ActivityType fromString(String id) {
        ActivityType[] values = values();
        for (ActivityType enumValue : values) {
            if (Objects.equal(enumValue.value, id)) {
                return enumValue;
            }
        }
        return null;
    }

}
