package com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.persistence;

public class Address {

    private String address;

    private boolean verified;

    public Address() {
    }

    public Address(String address, boolean verified) {
        this.address = address;
        this.verified = verified;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
