package com.ntoannhan.onestar.oneaccount.app.persistence;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.apache.http.impl.client.IdleConnectionEvictor;

@Entity
public class OneUserGroup {

  @Id
  private String id;

  private String name;
  
  private String members;

  @OneToOne
  private OneUser owner;

  private String description;

  public OneUserGroup() {

  }

  public OneUserGroup(String id, String name, String members, OneUser owner, String description) {
    this.id = id;
    this.name = name;
    this.members = members;
    this.owner = owner;
    this.description = description;
  }

  /**
   * @return String return the id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return String return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return List<OneUser> return the members
   */
  public String getMembers() {
    return members;
  }

  /**
   * @param members the members to set
   */
  public void setMembers(String members) {
    this.members = members;
  }

  /**
   * @return OneUser return the owner
   */
  public OneUser getOwner() {
    return owner;
  }

  /**
   * @param owner the owner to set
   */
  public void setOwner(OneUser owner) {
    this.owner = owner;
  }

  /**
   * @return String return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

}