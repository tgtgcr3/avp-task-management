package com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.repository;

import java.util.Optional;

import com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.persistence.Users;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component("rocketChatUsersRepository")
public interface UsersRepository extends MongoRepository<Users, String> {

    Optional<Users> findByUsername(String username);

}
