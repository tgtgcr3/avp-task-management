package com.ntoannhan.onestar.oneaccount.app.service.filestorage;

import java.util.Objects;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {

    private String uploadDir;

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }

    private String domainName;

    public FileStorageProperties() {
    }

    public FileStorageProperties(String uploadDir, String domainName) {
        this.uploadDir = uploadDir;
        this.domainName = domainName;
    }

    public String getDomainName() {
        return this.domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public FileStorageProperties uploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
        return this;
    }

    public FileStorageProperties domainName(String domainName) {
        this.domainName = domainName;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof FileStorageProperties)) {
            return false;
        }
        FileStorageProperties fileStorageProperties = (FileStorageProperties) o;
        return Objects.equals(uploadDir, fileStorageProperties.uploadDir) && Objects.equals(domainName, fileStorageProperties.domainName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uploadDir, domainName);
    }

    @Override
    public String toString() {
        return "{" +
            " uploadDir='" + getUploadDir() + "'" +
            ", domainName='" + getDomainName() + "'" +
            "}";
    }



}
