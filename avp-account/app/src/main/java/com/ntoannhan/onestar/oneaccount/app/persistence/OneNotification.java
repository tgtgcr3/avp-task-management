package com.ntoannhan.onestar.oneaccount.app.persistence;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

@Entity
public class OneNotification {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToOne
    private OneUser owner;

    @OneToOne
    private OneUser target;

    private String title;

    @Lob
    private String description;

    private Long datetime;

    @Column(columnDefinition = "int default 0")
    private int status;


    public OneNotification() {
    }

    public OneNotification(Long id, OneUser owner, OneUser target, String title, String description, Long datetime, int status) {
        this.id = id;
        this.owner = owner;
        this.target = target;
        this.title = title;
        this.description = description;
        this.datetime = datetime;
        this.status = status;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OneUser getOwner() {
        return this.owner;
    }

    public void setOwner(OneUser owner) {
        this.owner = owner;
    }

    public OneUser getTarget() {
        return this.target;
    }

    public void setTarget(OneUser target) {
        this.target = target;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDatetime() {
        return this.datetime;
    }

    public void setDatetime(Long datetime) {
        this.datetime = datetime;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public OneNotification id(Long id) {
        this.id = id;
        return this;
    }

    public OneNotification owner(OneUser owner) {
        this.owner = owner;
        return this;
    }

    public OneNotification target(OneUser target) {
        this.target = target;
        return this;
    }

    public OneNotification title(String title) {
        this.title = title;
        return this;
    }

    public OneNotification description(String description) {
        this.description = description;
        return this;
    }

    public OneNotification datetime(Long datetime) {
        this.datetime = datetime;
        return this;
    }

    public OneNotification status(int status) {
        this.status = status;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof OneNotification)) {
            return false;
        }
        OneNotification notification = (OneNotification) o;
        return Objects.equals(id, notification.id) && Objects.equals(owner, notification.owner) && Objects.equals(target, notification.target) && Objects.equals(title, notification.title) && Objects.equals(description, notification.description) && Objects.equals(datetime, notification.datetime) && status == notification.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, owner, target, title, description, datetime, status);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", owner='" + getOwner() + "'" +
            ", target='" + getTarget() + "'" +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", datetime='" + getDatetime() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }


}