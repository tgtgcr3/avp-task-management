package com.ntoannhan.onestar.oneaccount.app.delegator;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import com.ntoannhan.onestar.oneaccount.app.api.UserApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.model.SearchUserData;
import com.ntoannhan.onestar.oneaccount.app.model.UserData;
import com.ntoannhan.onestar.oneaccount.app.model.UserExtraData;
import com.ntoannhan.onestar.oneaccount.app.persistence.ExtraUserData;
import com.ntoannhan.onestar.oneaccount.app.persistence.OneUser;
import com.ntoannhan.onestar.oneaccount.app.repository.ExtraUserDataRepository;
import com.ntoannhan.onestar.oneaccount.app.repository.OneUserRepository;
import com.ntoannhan.onestar.oneaccount.app.service.CachingService;
import com.ntoannhan.onestar.oneaccount.app.service.KeyCloakService;
import com.ntoannhan.onestar.oneaccount.app.service.filestorage.FileStorageService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;

import static com.ntoannhan.onestar.oneaccount.app.delegator.KeycloakUserApiDelegateImpl.ALL_USER_CACHE_KEY;

@Service
public class UserDelegatorImpl implements UserApiDelegate{

    @Autowired
    private OneUserRepository m_repo;

    @Autowired
    private ExtraUserDataRepository m_extraRepo;

    @Autowired
    private FileStorageService m_service;

    @Autowired
    private HttpServletRequest m_request;

    private static final Logger m_logger = LogManager.getLogger(UserDelegatorImpl.class);

    @Autowired
    private CachingService m_cachingService;

    @Autowired
    private KeyCloakService m_keycloakService;

    public static final String ALL_PRIVATE_USER = "ALL_PRIVATE_USER";

    public static final String AVATAR_NAME_KEY = "AVATAR_NAME_KEY_";

    @Override
    public ResponseEntity<Resource> downloadAvatar(String userId) {
        Object cached = m_cachingService.getCache(AVATAR_NAME_KEY + userId);
        if (cached != null) {
            return download((String) cached);
        }
        Optional<OneUser> oneUser = m_repo.findById(userId);
        if (oneUser.isPresent()) {
            String userAvatar = oneUser.get().getAvatarUrl();
            if (userAvatar != null) {
                m_logger.info("Found userAvatar: " + userAvatar);
                int index = userAvatar.lastIndexOf("/");
                if (index > 0) {
                    String fileId = userAvatar.substring(index + 1);
                    return download(fileId);
                }
            } else {
                m_logger.error("Cannot find the avatarurl for user");
                return download("default");
            }
        }
        return download("default");     
    }

    public ResponseEntity<Resource> download(String fileId) {
        Resource resource = null;
        try {
            resource = m_service.loadFileAsResource(fileId);
            // Try to determine file's content type
            String contentType = null;
            try {
                contentType = m_request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            } catch (IOException ex) {
                m_logger.info("Could not determine file type.");
            }

            // Fallback to the default content type if type could not be determined
            if (contentType == null) {
                contentType = "application/octet-stream";
            }

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.status(400).build();

    }

    @Override
    public ResponseEntity<SearchUserData> getAllUsers(String username) {
        SearchUserData s = new SearchUserData();
        List<OneUser> users;
        if (username != null && username.length() > 0) {
            username = username.trim();
            Optional<OneUser> userOpts = m_repo.findById(username);
            if (userOpts.isPresent()) {
                users = Arrays.asList(userOpts.get());
            } else {
                users = new ArrayList<>();
            }
        } else {
            Object cache = m_cachingService.getCache(ALL_PRIVATE_USER);
            if (cache != null) {
                m_logger.debug("Get PrivateUse from cache");
                return ResponseEntity.ok((SearchUserData)cache);
            }
            users = m_repo.findAll();
        }
        List<UserData> userDataList = new ArrayList<>();
        users.forEach(ou ->
                {
                    UserData userdata =new
                            UserData()
                            .username(ou.getUsername())
                            .birthday(ou.getBirthday())
                            .avatarBinary(ou.getAvatarData())
                            .directMgmts(ou.getDirectManagers())
                            .studyLevels(ou.getStudies().stream().map(eu -> new UserExtraData().name(eu.getName()).description(eu.getDescription())).collect(Collectors.toList()))
                            .rewards(ou.getRewards().stream().map(eu -> new UserExtraData().name(eu.getName()).description(eu.getDescription())).collect(Collectors.toList()))
                            .experiences(ou.getExperiences().stream().map(eu -> new UserExtraData().name(eu.getName()).description(eu.getDescription())).collect(Collectors.toList()))
                            .email(ou.getEmail())
                            .avatarUrl(ou.getAvatarUrl())
                            .contacts(ou.getContacts().stream().map(eu -> new UserExtraData().name(eu.getName()).description(eu.getDescription())).collect(Collectors.toList()));


                    List<OneUser> directMembers = m_repo.findDirectMember(ou.getUsername());
                    userdata.setDirectMembers(directMembers.stream().map(OneUser::getUsername).collect(Collectors.joining(",")));
                    userDataList.add(
userdata
                    );


                });
        s.setItems(userDataList);
        s.setTotal((long) userDataList.size());
        if (s.getTotal()> 1) {
            m_cachingService.setCache(ALL_PRIVATE_USER, s);
        }
        return ResponseEntity.ok(s);
    }

    @Override
    public ResponseEntity<UserData> createUser(UserData userData) {
        m_cachingService.clearCache(ALL_USER_CACHE_KEY);
        m_cachingService.clearCache(ALL_PRIVATE_USER);
        m_cachingService.clearCache(AVATAR_NAME_KEY + userData.getUsername());
        clearLinkedCache();
        Optional<OneUser> oneUserOpt = m_repo.findById(userData.getUsername());
        boolean needToCreateNewUser = false;
        OneUser oneUser;
        if (oneUserOpt.isPresent()) {
            oneUser = oneUserOpt.get();
            m_logger.debug("Found existing user");
        } else {
            oneUser = new OneUser();
            oneUser.setUsername(userData.getUsername());            
            m_logger.debug("create new user");
            needToCreateNewUser = true;
        }
        
       // oneUser.setAvatarData(userData.getAvatarBinary());
        oneUser.setBirthday(userData.getBirthday() != null ? userData.getBirthday() : 0);
        oneUser.setAvatarUrl(userData.getAvatarUrl());
        oneUser.setEmail(userData.getEmail());
        List<Long> ids = replaceExtraData(oneUser, userData);
        m_logger.debug("Contact:");
        try {
            m_repo.save(oneUser);
            m_extraRepo.deleteUsersWithIds(ids);
        } catch (Exception ex) {
            m_logger.error("Cannot save or update user", ex);
            throw ex;
        }

        if (needToCreateNewUser) {
            createLinkedAppAccount(userData);
        }

        return ResponseEntity.ok(userData);
    }

    private void clearLinkedCache() {
        try {
            String messageLogout = m_keycloakService.getRequestUrl() + "/api/pri/internal/cache/user";
            HttpHeaders headers = new HttpHeaders();
            headers.set("X-APP-ID", "request");
            headers.set("X-TENANT-ID", "an-viet-phat");
            headers.set("X-ONE-HEADER", m_keycloakService.getRequestToken());
            HttpEntity<Object> entity = new HttpEntity<>(headers);
            m_logger.info("aaaaaaaaaaaaaaaaaaaa - cache - aaaaaaaaaaaaaaaaaaaaa");
            ResponseEntity<Object> result = m_keycloakService.getRestTemplate().exchange(messageLogout, HttpMethod.POST, entity, Object.class);
            m_logger.info("bbbbbbbbbbbbbbbbbbbb - cache - bbbbbbbbbbbbbbbbbbbbb: {}", result.getStatusCodeValue());
        }catch (Exception ex) {
            m_logger.error("Cannot clear cache for Request", ex);
        }
    }

    private void createLinkedAppAccount(UserData oneUser) {
        try {
            createTaskAccount(oneUser);
        } catch (Exception ex) {
            m_logger.error("Cannot create task account", ex);
        }
    }

    private void createTaskAccount(UserData oneUser) {
        String taskToken = m_keycloakService.getTaskToken();
        String taskUrl = m_keycloakService.getTaskUrl();
        Map<String, Object> taskUser = new HashMap<>();
        taskUser.put("login", oneUser.getEmail());
        taskUser.put("firstName", oneUser.getFirstName());
        taskUser.put("lastName", oneUser.getLastName());
        taskUser.put("identity_url", "openid:" + oneUser.getKcId());
        taskUser.put("email", oneUser.getEmail());
        taskUser.put("status", "active");
        taskUser.put("password", UUID.randomUUID().toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("apikey", taskToken);

        HttpEntity<Map> entity = new HttpEntity<>(taskUser, headers);
        ResponseEntity<Map> entityRes = m_keycloakService.getRestTemplate().exchange(taskUrl + "/api/v3/users", HttpMethod.POST, entity, Map.class);
         //= m_keycloakService.getRestTemplate().postForEntity(taskUrl + "/api/v3/users", taskUser, Map.class);
        if (entityRes.getStatusCode() == HttpStatus.OK) {
            Map body = entityRes.getBody();
            int userId = Integer.parseInt(body.get("id").toString());
            m_logger.debug("Create user successful with id: " + userId);
            Map<String, Object> membershipData = createMembershipData(userId);
            entity = new HttpEntity<>(membershipData, headers);
            entityRes = m_keycloakService.getRestTemplate().exchange(taskUrl + "/api/v3/memberships", HttpMethod.POST, entity, Map.class);
            if (entityRes.getStatusCode() == HttpStatus.OK) {
                m_logger.info("Assign user to default proejct done");
            } else {
                m_logger.error("Cannot assign user to default project");
            }
        } else {
            m_logger.error("Cannot create user");
        }
    }

    private Map<String, Object> createMembershipData(int userId) {
        int projectId = m_keycloakService.getDefaultProjectId();
        int roleId = m_keycloakService.getDefaultRoleId();

        return Map.of(
                "_links", Map.of(
                        "project", Map.of("href", "/api/v3/projects/" +projectId),
                        "principal", Map.of("href", "/api/v3/users/" +userId),
                        "roles", Arrays.asList(
                                Map.of("href", "/api/v3/roles/" + roleId)
                        )
                )
        );
    }

    private List<Long> replaceExtraData(OneUser oneUser, UserData userData) {
        List<Long> tobeDeleted = new ArrayList<>();
        if (oneUser.getContacts() != null && oneUser.getContacts().size() > 0) {
            m_logger.info("Clear all User Contacts");
            tobeDeleted.addAll(oneUser.getContacts().stream().map(ExtraUserData::getId).collect(Collectors.toList()));
        }
        if (oneUser.getContacts() != null) {
            oneUser.getContacts().clear();
        } else {
            oneUser.setContacts(new ArrayList<>());
        }
        if (userData.getContacts() != null) {
            oneUser.getContacts().addAll(userData.getContacts().stream().map(s -> {
                ExtraUserData eu = new ExtraUserData();
                eu.setName(s.getName());
                eu.setDescription(s.getDescription());
                eu.setUser(oneUser);
                return eu;
            }).collect(Collectors.toList()));
            m_logger.info("number of contact: " + oneUser.getContacts().size());
        }
        oneUser.setDirectManagers(userData.getDirectMgmts());


        if (oneUser.getRewards() != null && oneUser.getRewards().size() > 0) {
            m_logger.info("Clear all User Contacts");
            tobeDeleted.addAll(oneUser.getRewards().stream().map(ExtraUserData::getId).collect(Collectors.toList()));
        }
        if (oneUser.getRewards() != null) {
            oneUser.getRewards().clear();
        } else {
            oneUser.setRewards(new ArrayList<>());
        }

        if (userData.getRewards() != null) {
            oneUser.getRewards().addAll(userData.getRewards().stream().map(s -> {
                ExtraUserData eu = new ExtraUserData();
                eu.setName(s.getName());
                eu.setDescription(s.getDescription());
                eu.setUser(oneUser);
                return eu;
            }).collect(Collectors.toList()));
        }

        if (oneUser.getStudies() != null && oneUser.getStudies().size() > 0) {
            m_logger.info("Clear all User Contacts");
            tobeDeleted.addAll(oneUser.getStudies().stream().map(ExtraUserData::getId).collect(Collectors.toList()));
        }
        if (oneUser.getStudies() != null) {
            oneUser.getStudies().clear();
        } else {
            oneUser.setStudies(new ArrayList<>());
        }
        if (userData.getStudyLevels() != null) {
            oneUser.getStudies().addAll(userData.getStudyLevels().stream().map(s -> {
                ExtraUserData eu = new ExtraUserData();
                eu.setName(s.getName());
                eu.setDescription(s.getDescription());
                eu.setUser(oneUser);
                return eu;
            }).collect(Collectors.toList()));
        }

        if (oneUser.getExperiences() != null) {
            oneUser.getExperiences().clear();
        } else {
            oneUser.setExperiences(new ArrayList<>());
        }

        if (userData.getExperiences() != null) {
            oneUser.getExperiences().addAll(userData.getExperiences().stream().map(s -> {
                ExtraUserData eu = new ExtraUserData();
                eu.setName(s.getName());
                eu.setDescription(s.getDescription());
                eu.setUser(oneUser);
                return eu;
            }).collect(Collectors.toList()));
        }

        return tobeDeleted;
    }
}
