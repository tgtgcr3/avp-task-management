package com.ntoannhan.onestar.oneaccount.app.core.domain.usecase;

import com.ntoannhan.onestar.oneaccount.app.core.failure.Failure;
import io.vavr.control.Either;

public interface UseCase <T,V> {

    Either<Failure, T> execute(V v);

}
