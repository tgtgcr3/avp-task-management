package com.ntoannhan.onestar.oneaccount.app.service;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CachingService {

    private static final Map<String, Object> m_cached = new HashMap<>();

    private static final Object m_syncObj = new Object();

    public Object getCache(String key) {
        synchronized (m_syncObj) {
            return m_cached.get(key);
        }
    }

    public void setCache(String key, Object data) {
        synchronized (m_syncObj) {
            m_cached.put(key, data);
        }
    }

    public void clearCache(String key) {
        synchronized (m_syncObj) {
            m_cached.put(key, null);
        }
    }

}
