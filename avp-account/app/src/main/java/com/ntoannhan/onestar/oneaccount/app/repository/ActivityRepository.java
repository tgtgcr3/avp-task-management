package com.ntoannhan.onestar.oneaccount.app.repository;

import com.ntoannhan.onestar.oneaccount.app.persistence.Activity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ActivityRepository extends JpaRepository<Activity, Long> {

    @Query("SELECT COUNT(a) FROM Activity a WHERE " +
            "(:username IS NULL OR a.submitter = :username) AND " +
            "(:action IS NULL OR a.action = :action) AND " +
            "(:source IS NULL OR a.source = :source) AND " +
            "(:startDate IS NULL OR (:startDate <= a.date AND a.date <= :toDate))")
    long countByCondition(String username, Integer action, Integer source, Long startDate, Long toDate);

    @Query("SELECT a FROM Activity a WHERE " +
            "(:username IS NULL OR a.submitter = :username) AND " +
            "(:action IS NULL OR a.action = :action) AND " +
            "(:source IS NULL OR a.source = :source) AND " +
            "(:startDate IS NULL OR (:startDate <= a.date AND a.date <= :toDate))")
    List<Activity> findByCondition(String username, Integer action, Integer source, Long startDate, Long toDate, Pageable pageable);

    @Query("SELECT COUNT(a) FROM Activity a WHERE " +
            "(:username IS NULL OR a.submitter = :username) AND " +
            "(:action IS NULL OR a.action = :action) AND " +
            "(:source IS NULL OR a.source = :source) AND " +
            "(:projectId IS NULL OR a.projectId = :projectId) AND " +
            "(:startDate IS NULL OR (:startDate <= a.date AND a.date <= :toDate))")
    long countByConditionWithProjectId(String username, Integer action, Integer source, Long projectId, Long startDate, Long toDate);

    @Query("SELECT a FROM Activity a WHERE " +
            "(:username IS NULL OR a.submitter = :username) AND " +
            "(:action IS NULL OR a.action = :action) AND " +
            "(:source IS NULL OR a.source = :source) AND " +
            "(:projectId IS NULL OR a.projectId = :projectId) AND " +
            "(:startDate IS NULL OR (:startDate <= a.date AND a.date <= :toDate))")
    List<Activity> findByConditionWithProjectId(String username, Integer action, Integer source, Long projectId, Long startDate, Long toDate, Pageable pageable);

}
