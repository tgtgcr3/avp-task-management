package com.ntoannhan.onestar.oneaccount.app.core.failure;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class Failure {

    private Exception exception;

    private String reason;



}
