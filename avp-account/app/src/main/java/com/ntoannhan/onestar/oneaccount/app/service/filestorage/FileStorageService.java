package com.ntoannhan.onestar.oneaccount.app.service.filestorage;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Base64;
import java.util.Optional;
import java.util.UUID;

@Service
public class FileStorageService {

    private FileStorageProperties m_properties;

    public static final String DEFAULT_AVATAR = "default_avatar.png";

    private static final Resource m_default_avatar_resource = new ClassPathResource("images/" + DEFAULT_AVATAR);

    private final Path fileStorageLocation;

    private static final Logger m_logger = LogManager.getLogger(FileStorageService.class);

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) throws Exception {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        m_properties = fileStorageProperties;
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new Exception("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public String storeFileInBase64(String fileData) throws Exception {
        
            String[] fileDataParts = fileData.split(",");
            String ext = "";
            switch (fileDataParts[0]) {
                case "data:image/jpeg;base64":
                ext = ".jpeg";
                break;            
            default://should write cases for more images types
            ext = ".png";
                break;
            }
            byte[] decodedBytes = Base64.getDecoder().decode(fileDataParts[1]);
            String fileName = UUID.randomUUID().toString() + ext;
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            FileUtils.writeByteArrayToFile(targetLocation.toFile(), decodedBytes);
            return fileName;
        
        
    }

    public String storeFile(MultipartFile file) throws Exception {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw new Exception("Sorry! Filename contains invalid path sequence " + fileName);
            }

            String fileExt = fileName.substring(fileName.length() - 4);
            fileName = UUID.randomUUID().toString();

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName + fileExt);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName + fileExt;
        } catch (IOException ex) {
            throw new Exception("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(final String fileId) throws IOException {
        try {
            Optional<Path> fileOpt = Files.find(fileStorageLocation, 1, (path, attr) -> {
                return path.toString().contains(fileId);
            }).findFirst();
            if (fileOpt.isPresent()) {
                String fileName = fileOpt.get().toFile().getName();
                Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
                Resource resource = new UrlResource(filePath.toUri());
                if (resource.exists()) {
                    return resource;
                } else {
                    m_logger.warn("Avatar file is missing , using default");
                    return m_default_avatar_resource;
                }
            } else {              
                m_logger.warn("Avatar not found, using default");
              return m_default_avatar_resource;
            }
        } catch (MalformedURLException ex) {
            return m_default_avatar_resource;
         //   throw new FileNotFoundException("File not found " + fileId + "," + ex.getMessage());
        }
       // throw new FileNotFoundException("File not found " + fileId);
    }
}
