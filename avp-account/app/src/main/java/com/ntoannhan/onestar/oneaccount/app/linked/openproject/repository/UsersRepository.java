package com.ntoannhan.onestar.oneaccount.app.linked.openproject.repository;

import com.ntoannhan.onestar.oneaccount.app.linked.openproject.config.DatasourceConfig;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@AllArgsConstructor
@Component("openprojectUsersRepository")
public class UsersRepository {


    private final DatasourceConfig dataSourceConfig;

    public void updateEmail(String keycloakId, String email) {

    }

    public boolean updateUserInfo(String keycloakId, String email, String firstname, String lastname) {
        DataSource dataSource = dataSourceConfig.getDataSource();
        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement statement = conn.prepareStatement("UPDATE users SET mail = ? , firstname = ?, lastname = ? WHERE identity_url like CONCAT('%', ?)");
            statement.setString(1, email);
            statement.setString(2, firstname);
            statement.setString(3, lastname);
            statement.setString(4, keycloakId);
            int numOfCount = statement.executeUpdate();

            return numOfCount > 0;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    public boolean enableUser(String keycloakId, boolean enabled) {
        DataSource dataSource = dataSourceConfig.getDataSource();
        try (Connection conn = dataSource.getConnection()) {

            PreparedStatement statement = conn.prepareStatement("UPDATE users SET status = ? WHERE identity_url like CONCAT('%', ?)");
            statement.setInt(1, enabled ? 1 : 3);
            statement.setString(2, keycloakId);
            int numOfCount = statement.executeUpdate();

            return numOfCount > 0;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

}
