package com.ntoannhan.onestar.oneaccount.app.service.importexport;

import com.ntoannhan.onestar.oneaccount.app.service.importexport.strategy.ExportStrategy;
import com.ntoannhan.onestar.oneaccount.app.service.importexport.strategy.WeworkReportExportStrategy;
import com.ntoannhan.onestar.oneaccount.app.service.props.ImportExportProp;
import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.Map;

@Service
public class ImportExportService {

    @Autowired
    private ImportExportProp m_props;

    private static final Logger m_logger = LogManager.getLogger(ImportExportService.class);

    private static final Map<String, ExportStrategy> m_exportStrategies = Map.of(
            "wework_report", new WeworkReportExportStrategy()
    );


    private FreeMarkerConfigurer m_freeMarkerConfig;

    @PostConstruct
    private void postConstruct() {
        m_freeMarkerConfig = createFreeMarkerConfig();
    }


    private FreeMarkerConfigurer createFreeMarkerConfig() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_29);

        if (m_props.getTemplatesPath() != null && m_props.getTemplatesPath().trim().length() > 0) {
            try {
                m_logger.info("Trying to set template folder to: " + m_props.getTemplatesPath());
                configuration.setDirectoryForTemplateLoading(new File(m_props.getTemplatesPath()));
            } catch (IOException e) {
                m_logger.error("Cannot set template folder", e);
            }
        } else {
            TemplateLoader templateLoader = new ClassTemplateLoader(this.getClass(), "/templates");
            configuration.setTemplateLoader(templateLoader);
        }

        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setConfiguration(configuration);
        return freeMarkerConfigurer;
    }

    public byte[] export(String appName, String templateName, Map<String, Object> data) {
        String templateFullname = appName + "_" + templateName;
        m_logger.debug("Export data to string using template: " + templateFullname);
        ExportStrategy exportStrategy = m_exportStrategies.get(templateFullname);
        if (exportStrategy != null) {
            m_logger.debug("Found strategy for: " + templateFullname);
            return exportStrategy.export(templateName, data);
        }
//        Template template = null;
//        try {
//            template = m_freeMarkerConfig.getConfiguration().getTemplate(templateFullname);
//            return FreeMarkerTemplateUtils.processTemplateIntoString(template, data);
//        } catch (IOException | TemplateException e) {
//            m_logger.error("Cannot export using template: " + templateFullname, e);
//        }
        return null;
    }

}
