package com.ntoannhan.onestar.oneaccount.app.delegator;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.ntoannhan.onestar.oneaccount.app.api.NotificationApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.model.Notification;
import com.ntoannhan.onestar.oneaccount.app.model.NotificationQueryResult;
import com.ntoannhan.onestar.oneaccount.app.persistence.OneNotification;
import com.ntoannhan.onestar.oneaccount.app.persistence.OneUser;
import com.ntoannhan.onestar.oneaccount.app.repository.NotificationRepository;
import com.ntoannhan.onestar.oneaccount.app.repository.OneUserRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class NotificationApiDelegateImpt implements NotificationApiDelegate {

    @Autowired
    private NotificationRepository m_repo;

    @Autowired
    private OneUserRepository m_userRepo;

    private static final Logger m_logger = LogManager.getLogger(NotificationApiDelegateImpt.class);

    @Override
    public ResponseEntity<Object> createNotification(String email, Notification notification) {
        notification.datetime(System.currentTimeMillis());
        String username = getCurrentUsername();
        if (username == null) {
            username = getUsernameFromEmail(email);
        }
        if (notification.getOwner() == null) {
            if (notification.getOwnerEmail() != null) {
                notification.setOwner(getUsernameFromEmail(notification.getOwnerEmail()));
            }
        }
        if (notification.getTarget() == null) {
            if (notification.getTargetEmail() != null) {
                notification.setTarget(getUsernameFromEmail(notification.getTargetEmail()));
            }
        }
        if ((notification.getOwner() == null || !notification.getOwner().trim().equals(username))) {
            m_logger.warn("Owner is null or Owner is not the current user");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        notification.status(0);
        OneNotification oneNotification = toOneNotification(notification);
        if (oneNotification != null) {
            m_repo.saveAndFlush(oneNotification);
            return ResponseEntity.ok(toNotification(oneNotification));
        }
        m_logger.warn("Cannot convert Notification");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

    }

    @Override
    public ResponseEntity<NotificationQueryResult> notificationsGet(Integer skip, Integer size, String email) {
        if (skip == null) {
            skip = 0;
        }
        if (size == null) {
            size = 20;
        }

        String username = getCurrentUsername();
        if (username == null) {
            username = getUsernameFromEmail(email);
        }
        
        List<OneNotification> items = m_repo.getByTarget(username, PageRequest.of(skip, size, Sort.by(Sort.Direction.DESC, "datetime")));
        long count = m_repo.getTotalByTarget(username);
        return ResponseEntity.ok(new NotificationQueryResult().total((int) count)
                .items(items.stream().map(n -> toNotification(n)).collect(Collectors.toList())));

    }

    @Override
    public ResponseEntity<Object> setNotificationStatus(Long id, Integer status, String email) {
        Optional<OneNotification> notificationOpt = m_repo.findById(id);
        if (notificationOpt.isPresent()) {
            OneNotification notification = notificationOpt.get();
            String username = getCurrentUsername();
            if (username == null) {
                username = getUsernameFromEmail(email);
            }
            if (notification.getTarget().getUsername().equals(username)) {
                if (status != null && status < 3) {
                    notification.setStatus(status);
                    m_repo.save(notification);
                    return ResponseEntity.ok(toNotification(notification));
                }
            }
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

    }

    private OneNotification toOneNotification(Notification notification) {
        String ownerUsername = notification.getOwner();
        String targetUsername = notification.getTarget();
        if (ownerUsername == null || ownerUsername.trim().length() == 0 || targetUsername == null
                || targetUsername.trim().length() == 0) {
            return null;
        }
        Optional<OneUser> ownerOp = m_userRepo.findById(ownerUsername);
        Optional<OneUser> targetOp = m_userRepo.findById(targetUsername);
        if (ownerOp.isEmpty() || targetOp.isEmpty()) {
            return null;
        }

        return new OneNotification().datetime(notification.getDatetime()).owner(ownerOp.get()).target(targetOp.get())
                .title(notification.getTitle()).id(notification.getId()).status(notification.getStatus())
                .description(notification.getDescription());
    }

    private Notification toNotification(OneNotification oneNotification) {
        return new Notification().datetime(oneNotification.getDatetime()).id(oneNotification.getId())
                .description(oneNotification.getDescription()).owner(oneNotification.getOwner().getUsername())
                .ownerEmail(oneNotification.getOwner().getEmail())
                .target(oneNotification.getTarget().getUsername()).title(oneNotification.getTitle())
                .targetEmail(oneNotification.getTarget().getEmail())
                .status(oneNotification.getStatus());
    }

    private String getCurrentUsername() {
        //return "toannhanb7";
         String username = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else if (username != null ) {
            username = principal.toString();
        }
        m_logger.debug("aaaaaaaaaaaaaaaaaaaa: " + String.valueOf(username));
        return username;
    }

    private String getUsernameFromEmail(String email) {
        OneUser oneuser = m_userRepo.findUserByEmail(email);
        if (oneuser != null) {
            return oneuser.getUsername();
        }
        return null;
    }

}
