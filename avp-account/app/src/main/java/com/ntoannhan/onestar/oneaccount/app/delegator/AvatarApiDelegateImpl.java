package com.ntoannhan.onestar.oneaccount.app.delegator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ntoannhan.onestar.oneaccount.app.api.AvatarApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.persistence.OneUser;
import com.ntoannhan.onestar.oneaccount.app.repository.OneUserRepository;
import com.ntoannhan.onestar.oneaccount.app.service.filestorage.FileStorageProperties;
import com.ntoannhan.onestar.oneaccount.app.service.filestorage.FileStorageService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AvatarApiDelegateImpl implements AvatarApiDelegate {

  @Autowired
  private OneUserRepository m_repo;

  @Autowired
  private FileStorageService m_service;

  @Autowired
  private HttpServletRequest m_request;

  @Autowired
  private FileStorageProperties m_prop;

  private static final Logger m_logger = LogManager.getLogger(AvatarApiDelegateImpl.class);

  @Autowired
  private HttpServletResponse m_response;

  @Override
  public ResponseEntity<Resource> getAvatar(String email) {
    OneUser oneUser = m_repo.findUserByEmail(email);
    if (oneUser != null) {
      String userAvatar = oneUser.getAvatarUrl();
      if (userAvatar != null) {
        m_logger.info("Found userAvatar: " + userAvatar);
        int index = userAvatar.lastIndexOf("/");
        if (index > 0) {
          String fileId = userAvatar.substring(index + 1);
          return download(fileId, false);
        }
      } else {
        m_logger.error("Cannot find the avatarurl for user");
        return download("default_avatar.png", false);
      }
    }
    return ResponseEntity.status(400).build();
  }

  public ResponseEntity<Resource> download(String fileId, boolean needToCache) {
    Resource resource = null;
    try {      
      resource = m_service.loadFileAsResource(fileId);
      // Try to determine file's content type
      String contentType = null;
      try {
        contentType = m_request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
      } catch (IOException ex) {
        m_logger.info("Could not determine file type.");
      }



      // Fallback to the default content type if type could not be determined
      if (contentType == null) {
        contentType = "application/octet-stream";
      }
      if (needToCache) {
        m_response.setHeader("Cache-Control", "no-transform, public, max-age=3600");
      }
      return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
          .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
          .body(resource);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return ResponseEntity.status(400).build();

  }

  @Override
  public ResponseEntity<Object> findAvatars(String usernames) {
    List<OneUser> users = new ArrayList<>();
    if (usernames == null) {
      users = m_repo.findAll();
    } else {
      users = m_repo.findAllById(Arrays.asList(usernames.split(",")));
    }

    String defaultUrl =  m_prop.getDomainName() + "/api/pri/file/download/" + FileStorageService.DEFAULT_AVATAR;
        // .path("/api/pri/file/download/")
        // .path(FileStorageService.DEFAULT_AVATAR)
        // .toUriString();

    Map<String, String> avatarUrls = new HashMap<>();
    for (OneUser user : users) {
      String avatarUrl = user.getAvatarUrl();
      if (avatarUrl == null ) {
        avatarUrl = defaultUrl;
      }
      avatarUrls.put(user.getUsername(), avatarUrl);
    }
    
    return ResponseEntity.ok(avatarUrls);

  }

}
