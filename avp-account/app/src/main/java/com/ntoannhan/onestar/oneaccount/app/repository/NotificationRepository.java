package com.ntoannhan.onestar.oneaccount.app.repository;

import java.util.List;

import com.ntoannhan.onestar.oneaccount.app.persistence.OneNotification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NotificationRepository extends JpaRepository<OneNotification, Long>{
    
    @Query(value = "SELECT n FROM OneNotification n WHERE n.target.username = :target")
    List<OneNotification> getByTarget(@Param("target") String target, org.springframework.data.domain.Pageable pageable);

    @Query("SELECT COUNT(n) FROM OneNotification n WHERE n.target.username = :target")
    long getTotalByTarget(@Param("target") String target);
}
