package com.ntoannhan.onestar.oneaccount.app.linked.openproject.persistence;

import lombok.Data;

@Data
public class Project {

    private Long id;

    private String name;

}
