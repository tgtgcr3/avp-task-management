package com.ntoannhan.onestar.oneaccount.app.delegator;

import com.google.common.base.Objects;
import com.ntoannhan.onestar.oneaccount.app.api.ActivityApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.domain.model.ActivityAction;
import com.ntoannhan.onestar.oneaccount.app.domain.model.ActivityGetAll;
import com.ntoannhan.onestar.oneaccount.app.domain.model.ActivityType;
import com.ntoannhan.onestar.oneaccount.app.linked.openproject.persistence.Project;
import com.ntoannhan.onestar.oneaccount.app.linked.openproject.repository.ProjectRepository;
import com.ntoannhan.onestar.oneaccount.app.model.*;
import com.ntoannhan.onestar.oneaccount.app.persistence.Activity;
import com.ntoannhan.onestar.oneaccount.app.service.activitylog.ActivityService;
import lombok.AllArgsConstructor;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua_parser.Client;
import ua_parser.Parser;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
public class ActivityApiDelegateImpl implements ActivityApiDelegate {

    private final ActivityService activityService;

    private final KeycloakUserApiDelegateImpl keycloakUserApiDelegate;

    private final HttpServletRequest httpRequest;

    private final ProjectRepository projectRepository;

    @Override
    public ResponseEntity<ActivityLog> createActivity(ActivityLog activityLog) {
        Activity activity = convertToAppData(activityLog);
        if (activity == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        activity = activityService.createActivity(activity);
        if (activity != null) {
            return ResponseEntity.ok(convertToRestData(activity, Collections.emptyMap()));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


    @Override
    public ResponseEntity<ActivityLogGetAllResult> getActivityLogs(String type,
                                                                   String user, Integer page, Integer size, ActionType action, ActivitySource source, Long from, Long to, Long projectId) {
        int actionId = toAppAction(action);
        int sourceId = toAppSource(source);
        ActivityType activityType = ActivityType.fromString(type.toLowerCase(Locale.ROOT));
        if (activityType == null) {
            return ResponseEntity.ok(new ActivityLogGetAllResult()
                    .total(0L)
                    .items(Collections.emptyList()));
        }
        ActivityGetAll activityGetAll = activityService.findActivity(activityType, user, actionId > 0 ? ActivityAction.fromId(actionId) : null, sourceId > 0 ? com.ntoannhan.onestar.oneaccount.app.domain.model.ActivitySource.fromId(sourceId) : null, from, to, page, size, projectId);

        List<Long> projectIds = activityGetAll.items().stream().map(Activity::getProjectId).filter(java.util.Objects::nonNull).distinct().collect(Collectors.toList());
        Map<Long, Project> projects = projectRepository.findProject(projectIds);
        return ResponseEntity.ok(new ActivityLogGetAllResult()
                .total(activityGetAll.total())
                .items(activityGetAll.items().stream().map(a -> this.convertToRestData(a, projects)).collect(Collectors.toList()))
        );
    }

    private ActivityLog convertToRestData(Activity activity, Map<Long, Project> projects) {
        if (activity == null)
            return null;

        ActivityLog activityLog = new ActivityLog();
        return activityLog.id(activity.getId()).action(toRestAction(activity.getAction()))
                .user(activity.getSubmitter())
                .clientInfo(new ClientInfo()
                        .browser(activity.getBrowserName())
                        .hostname(activity.getHostname())
                        .ipAddress(activity.getPublicIp())
                )
                .projectId(activity.getProjectId())
                .projectName(activity.getProjectId() != null && projects.containsKey(activity.getProjectId()) ? projects.get(activity.getProjectId()).getName() : null)
//                .projectId()
                .localId(activity.getLocalIp())
                .publicIp(activity.getPublicIp())
                .source(toRestSource(activity.getSource())).description(activity.getDescription()).date(activity.getDate());
    }

    private ActivitySource toRestSource(int source) {
        if (source == 0) {
            return null;
        }

        com.ntoannhan.onestar.oneaccount.app.domain.model.ActivitySource activitySource = com.ntoannhan.onestar.oneaccount.app.domain.model.ActivitySource.fromId(source);
        switch (activitySource) {
            case Task:
                return ActivitySource.TASK;
            case Account:
                return ActivitySource.ACCOUNT;
            case Message:
                return ActivitySource.MESSAGE;
            case Request:
                return ActivitySource.REQUEST;
            default:
                return null;
        }
    }

    private ActionType toRestAction(int action) {
        if (action == 0) {
            return null;
        }
        ActivityAction activityAction = ActivityAction.fromId(action);
        switch (activityAction) {
            case Login:
                return ActionType.LOGIN;
            case Logout:
                return ActionType.LOGOUT;
            case Assign_Task:
                return ActionType.ASSIGN_TASK;
            case Create_Task:
                return ActionType.CREATE_TASK;
            case Delete_Task:
                return ActionType.DELETE_TASK;
            case Update_Task:
                return ActionType.UPDATE_TASK;
            case Update_Profile:
                return ActionType.UPDATE_PROFILE;
            case Create_Project:
                return ActionType.CREATE_PROJECT;
            case Update_Project:
                return ActionType.UPDATE_PROJECT;
            case Delete_Project:
                return ActionType.DELETE_PROJECT;
        }
        return null;
    }

    private Activity convertToAppData(ActivityLog activityLog) {
        Activity activity = new Activity();

        activity.setAction(toAppAction(activityLog.getAction()));
        activity.setSource(toAppSource(activityLog.getSource()));
        activity.setDescription(activityLog.getDescription());
        activity.setProjectId(activityLog.getProjectId());

        activity.setSubmitter(activityLog.getUser());
        if (activity.getSource() == com.ntoannhan.onestar.oneaccount.app.domain.model.ActivitySource.Task.getValue()) {
            String username = findUsernameFromEmail(activity.getSubmitter());
            activity.setSubmitter(username);
        }
        if (activity.getSubmitter() == null) {
            return null;
        }

        String remoteAddr = null;
        if (activityLog.getPublicIp() != null && activityLog.getPublicIp().trim().length() > 0) {
         remoteAddr = activityLog.getPublicIp();
        } else {

            remoteAddr = httpRequest.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null) {
                remoteAddr = httpRequest.getRemoteAddr();
            }
        }

        String remoteHost = httpRequest.getRemoteHost();
        activity.setPublicIp(remoteAddr);
        activity.setHostname(remoteHost);
        activity.setLocalIp(activityLog.getLocalId());
        String browserDetails = httpRequest.getHeader("User-Agent");
        if (browserDetails != null) {
            try {
                Parser uaParser = new Parser();
                Client client = uaParser.parse(browserDetails);
                activity.setBrowserName(client.userAgent.family);
            } catch (Exception ex) {
                activity.setBrowserName("Unknow");
            }
        }

        return activity;
    }

    private String findUsernameFromEmail(String submitter) {
        ResponseEntity<Object> users = this.keycloakUserApiDelegate.getUsers(null, null, null, null, null, 0, 10240);
        Map<String, Object> userMaps =  (Map<String, Object>)users.getBody();
        List<UserRepresentation> userList = (List<UserRepresentation>) userMaps.get("items");
        return userList.stream().filter(u -> Objects.equal(submitter, u.getEmail())).findAny().map(UserRepresentation::getUsername).orElse(null);
    }

    private int toAppSource(ActivitySource source) {
        if (source == null) {
            return 0;
        }
        switch (source) {
            case TASK:
                return com.ntoannhan.onestar.oneaccount.app.domain.model.ActivitySource.Task.getValue();
            case ACCOUNT:
                return com.ntoannhan.onestar.oneaccount.app.domain.model.ActivitySource.Account.getValue();
            case REQUEST:
                return com.ntoannhan.onestar.oneaccount.app.domain.model.ActivitySource.Request.getValue();
            case MESSAGE:
                return com.ntoannhan.onestar.oneaccount.app.domain.model.ActivitySource.Message.getValue();
            default:
                return 0;
        }
    }

    private int toAppAction(ActionType action) {
        if (action == null) {
            return 0;
        }

        switch (action) {
            case LOGIN:
                return ActivityAction.Login.getValue();
            case LOGOUT:
                return ActivityAction.Logout.getValue();
            case ASSIGN_TASK:
                return ActivityAction.Assign_Task.getValue();
            case UPDATE_PROFILE:
                return ActivityAction.Update_Profile.getValue();
            case CREATE_TASK:
                return ActivityAction.Create_Task.getValue();
            case UPDATE_TASK:
                return ActivityAction.Update_Task.getValue();
            case DELETE_TASK:
                return ActivityAction.Delete_Task.getValue();
            case CREATE_PROJECT:
                return ActivityAction.Create_Project.getValue();
            case UPDATE_PROJECT:
                return ActivityAction.Update_Project.getValue();
            case DELETE_PROJECT:
                return ActivityAction.Delete_Project.getValue();
            default:
                return 0;
        }
    }
}
