package com.ntoannhan.onestar.oneaccount.app.delegator;

import com.ntoannhan.onestar.oneaccount.app.api.ExportApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.domain.model.ExportInput;
import com.ntoannhan.onestar.oneaccount.app.domain.usecase.ExportData;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class ExportApiDelegateImpl implements ExportApiDelegate {

    @Autowired
    private ExportData m_exportData;

    @Override
    public ResponseEntity<Resource> exportData(String app, String schema, Map<String, Object> requestBody) {
        return m_exportData.execute(new ExportInput().appName(app).reportType(schema).data(requestBody))
                .fold(left -> ResponseEntity.status(500).build(),
                        right -> ResponseEntity.ok()
                        .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                        .body(new ByteArrayResource(right))
        );
    }
}
