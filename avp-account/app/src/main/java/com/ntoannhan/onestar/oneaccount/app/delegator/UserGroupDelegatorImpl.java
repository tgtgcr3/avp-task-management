package com.ntoannhan.onestar.oneaccount.app.delegator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.ntoannhan.onestar.oneaccount.app.api.UserGroupApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.model.UserGroup;
import com.ntoannhan.onestar.oneaccount.app.persistence.OneUserGroup;
import com.ntoannhan.onestar.oneaccount.app.repository.OneUserGroupRepository;
import com.ntoannhan.onestar.oneaccount.app.repository.OneUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserGroupDelegatorImpl implements UserGroupApiDelegate {
  
  @Autowired
  private OneUserGroupRepository m_repo;

  @Autowired
  private OneUserRepository m_userRepo;

  @Override
  public ResponseEntity<UserGroup> getUserGroup(String id) {
    Optional<OneUserGroup> oneUserGroupOpt = m_repo.findById(id);
    if (oneUserGroupOpt.isPresent()) {
      UserGroup userGroup = UserGroupDelegatorImpl.toJsonObject(oneUserGroupOpt.get());
      return ResponseEntity.ok(userGroup);
    }
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();    
  }

  @Override
  public ResponseEntity<UserGroup> updateUserGroup(String id, UserGroup userGroup) {

    String error = UserGroupDelegatorImpl.validateData(userGroup);
    if (error == null || error.length() == 0) {
      Optional<OneUserGroup> oneUserGroupOpt = m_repo.findById(id);
      if (oneUserGroupOpt.isPresent()) {
        OneUserGroup tobeUpdated = UserGroupDelegatorImpl.convertToEntity(userGroup, m_userRepo);
        tobeUpdated.setId(id);
        m_repo.saveAndFlush(tobeUpdated);
        return ResponseEntity.ok(userGroup);
      }
    }
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(userGroup);    
  }

  @Override
  public ResponseEntity<Object> deleteUserGroup(String id) {
    Optional<OneUserGroup> oneUserGroupOpt = m_repo.findById(id);
    if (oneUserGroupOpt.isPresent()) {
      m_repo.deleteById(id);
      return ResponseEntity.ok(Map.of("status", "OK"));
    }
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(id);
  }

  @Override
  public ResponseEntity<Object> getAll() {
    List<OneUserGroup> oneUserGroups = m_repo.findAll();  

    List<UserGroup> userGroups = oneUserGroups.stream().map(oug -> toJsonObject(oug)).collect(Collectors.toList());

    Map<String, Object> data = new HashMap<>();
    data.put("total", userGroups.size());
    data.put("items", userGroups);

    return ResponseEntity.ok(data);
  }

  @Override
  public ResponseEntity<UserGroup> createUserGroup(UserGroup userGroup) {
    if (userGroup != null) {
      String error = validateData(userGroup);
      if (error == null || error.length() == 0) {
        OneUserGroup tobeAdded = convertToEntity(userGroup, m_userRepo);
        if (tobeAdded.getOwner() != null) {
          Optional<OneUserGroup> oneUserGroupOpt = m_repo.findById(tobeAdded.getId());
          if (oneUserGroupOpt.isEmpty()) {
            OneUserGroup added = m_repo.saveAndFlush(tobeAdded);
            return ResponseEntity.ok(toJsonObject(added));
          }
        }
      }
    }

    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(userGroup);
    
  }

  @Override
  public ResponseEntity<Object> findGroupsByUsingName(String username) {
    List<OneUserGroup> myGroups = m_repo.getMyGroups(username);
    myGroups.forEach(g -> g.setOwner(null));
    return ResponseEntity.ok(Map.of(
            "items", myGroups,
            "total", myGroups.size()
    ));
  }

  public static UserGroup toJsonObject(OneUserGroup oneUserGroup) {
    return new UserGroup()
                .id(oneUserGroup.getId())
                .name(oneUserGroup.getName())
                .description(oneUserGroup.getDescription())
                .owner(oneUserGroup.getOwner().getUsername())
                .members(oneUserGroup.getMembers());
  }

  public static OneUserGroup convertToEntity(UserGroup userGroup, OneUserRepository userRepo) {
    OneUserGroup oneUserGroup = new OneUserGroup();
    oneUserGroup.setId(userGroup.getId());
    oneUserGroup.setName(userGroup.getName());
    oneUserGroup.setDescription(userGroup.getDescription());
    oneUserGroup.setMembers(userGroup.getMembers());
    oneUserGroup.setOwner(userRepo.findById(userGroup.getOwner()).get());
    return oneUserGroup;
  }

  public static String validateData(UserGroup userGroup) {
    if (userGroup.getId() == null || userGroup.getId().length() == 0) {
      return "Id is empty";
    }
    if (userGroup.getOwner() == null || userGroup.getOwner().length() == 0) {
      return "Owner is empty";
    }
    if (userGroup.getName() == null || userGroup.getName().length() == 0) {
      return "Name is empty";
    }
    return null;
  }

}
