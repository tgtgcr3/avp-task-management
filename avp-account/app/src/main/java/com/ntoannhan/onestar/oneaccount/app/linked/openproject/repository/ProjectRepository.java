package com.ntoannhan.onestar.oneaccount.app.linked.openproject.repository;

import com.ntoannhan.onestar.oneaccount.app.linked.openproject.config.DatasourceConfig;
import com.ntoannhan.onestar.oneaccount.app.linked.openproject.persistence.Project;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Component
@AllArgsConstructor
public class ProjectRepository {

    private final DatasourceConfig dataSourceConfig;

    @RolesAllowed({"admin"})
    public List<Project> findProject(String name) {
        if (name == null) {
            name = "";
        }
        DataSource dataSource = dataSourceConfig.getDataSource();
        try (Connection conn = dataSource.getConnection()) {
            PreparedStatement statement =
                    conn.prepareStatement("SELECT id, name " +
                            "FROM projects " +
                            "WHERE name like CONCAT('%', ?, '%') AND id > 6 AND " +
                            "id NOT IN (SELECT c.customized_id FROM custom_values c WHERE (c.custom_field_id = 19 AND c.value <= 1)) order by id desc");
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            List<Project> projects = new LinkedList<>();
            while (resultSet.next()) {
                Project project = new Project();
                 project.setId(resultSet.getLong("id"));
                 project.setName(resultSet.getString("name"));
                 projects.add(project);
            }

            return projects;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Collections.emptyList();
    }

    public Map<Long, Project> findProject(List<Long> projectIds) {
        if (projectIds == null || projectIds.size() == 0) {
            return Collections.emptyMap();
        }
        DataSource dataSource = dataSourceConfig.getDataSource();
        try (Connection conn = dataSource.getConnection()) {
            StringBuilder sql = new StringBuilder("SELECT id, name " +
                    "FROM projects " +
                    "WHERE id IN (");
            sql.append("?,".repeat(projectIds.size()));
            sql.replace(sql.length() - 1, sql.length(), ")");

            PreparedStatement statement =
                    conn.prepareStatement(sql.toString());

            for (int i = 0; i < projectIds.size(); i++) {
                statement.setLong(i + 1, projectIds.get(i));
            }
//            statement.setArray(1, conn.createArrayOf("INTEGER", projectIds.toArray(new Long[0])));
            ResultSet resultSet = statement.executeQuery();
            Map<Long, Project> projects = new HashMap<>();
            while (resultSet.next()) {
                Project project = new Project();
                project.setId(resultSet.getLong("id"));
                project.setName(resultSet.getString("name"));
                projects.put(project.getId(), project);
            }

            return projects;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return Collections.emptyMap();
    }

}
