package com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.service;

import com.mongodb.client.result.UpdateResult;
import com.ntoannhan.onestar.oneaccount.app.delegator.KeycloakUserApiDelegateImpl;
import com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.persistence.Address;
import com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.persistence.RocketchatSubscription;
import com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.persistence.Users;
import com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.repository.UsersRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class RCUsersService {

    @Autowired
    MongoTemplate mongoTemplate;;

    @Autowired
    @Qualifier("rocketChatUsersRepository")
    private UsersRepository m_rcRepo;

    private static final Logger m_logger = LogManager.getLogger(KeycloakUserApiDelegateImpl.class);

    public boolean updateUserInfo(String username, String email, String firstname, String lastname) {
        try {
            Query query = Query.query(Criteria.where("username").is(username));
            Update update = new Update();
            update.set("emails", new Address[]{
                    new Address(email, true)
            });
            update.set("name", getFullname(firstname, lastname));
            UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Users.class);


            if (updateResult.getMatchedCount() > 0 && updateResult.getModifiedCount() > 0) {
                return updateChatSubscription(username, firstname, lastname);
            }

        } catch (Exception ex) {
            m_logger.error("Cannot update UserInfo", ex);
        }
        return false;
    }

    public boolean enableUser(String username, boolean enabled) {
        try {
            Query query = Query.query(Criteria.where("username").is(username));
            Update update = new Update();

            update.set("active", enabled);
            UpdateResult updateResult = mongoTemplate.updateFirst(query, update, Users.class);


            return updateResult.getMatchedCount() > 0 && updateResult.getModifiedCount() > 0;

        } catch (Exception ex) {
            m_logger.error("Cannot update UserInfo", ex);
        }
        return false;
    }

    private String getFullname(String firstname, String lastname) {
        return lastname.trim() + " " + firstname.trim();
    }

    private boolean updateChatSubscription(String username, String firstname, String lastname) {
        try {
            Query query = Query.query(Criteria.where("t").is("d").and("name").is(username));

            Update update = new Update();
            update.set("fname", getFullname(firstname, lastname));
            UpdateResult updateResult = mongoTemplate.updateMulti(query, update, RocketchatSubscription.class);
            boolean success = updateResult.getMatchedCount() > 0 && updateResult.getModifiedCount() > 0;
            return success;
        } catch (Exception ex) {
            m_logger.error("Cannot update UserInfo", ex);
        }
        return false;
    }
}
