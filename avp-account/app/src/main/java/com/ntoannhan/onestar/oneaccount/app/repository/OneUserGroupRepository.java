package com.ntoannhan.onestar.oneaccount.app.repository;

import java.util.List;

import com.ntoannhan.onestar.oneaccount.app.persistence.OneUserGroup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OneUserGroupRepository extends JpaRepository<OneUserGroup, String> {
  
  @Query("SELECT ug FROM OneUserGroup ug WHERE ug.owner.username = :username OR ug.members LIKE CONCAT('%',:username,'%')")
  List<OneUserGroup> getMyGroups(@Param("username") String username);

}
