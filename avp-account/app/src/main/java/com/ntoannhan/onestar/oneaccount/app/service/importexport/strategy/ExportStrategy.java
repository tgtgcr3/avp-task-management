package com.ntoannhan.onestar.oneaccount.app.service.importexport.strategy;

import com.ntoannhan.onestar.oneaccount.app.core.failure.Failure;
import io.vavr.control.Either;

import java.util.Map;

public interface ExportStrategy {

    byte[] export(String templateName, Map<String, Object> data);

    default String convertValue(String header, Object value) {
        return String.valueOf(value);
    }
}
