package com.ntoannhan.onestar.oneaccount.app.domain.model;

public enum ActivitySource {
    Account(1), Task(2), Message(3), Request(4);

    private final int value;

    ActivitySource(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ActivitySource fromId(int id) {
        ActivitySource[] values = values();
        for (ActivitySource requestStatus : values) {
            if (requestStatus.value == id) {
                return requestStatus;
            }
        }
        return null;
    }

}