package com.ntoannhan.onestar.oneaccount.app.repository;

import com.ntoannhan.onestar.oneaccount.app.persistence.ExtraUserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ExtraUserDataRepository extends JpaRepository<ExtraUserData, Long> {

    @Modifying
    @Transactional
    @Query("delete from ExtraUserData u where u.id in ?1")
    void deleteUsersWithIds(List<Long> ids);

}
