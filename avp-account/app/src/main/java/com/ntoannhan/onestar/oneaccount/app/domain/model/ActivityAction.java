package com.ntoannhan.onestar.oneaccount.app.domain.model;

public enum ActivityAction {
    Login(1), Logout(2), Update_Profile(3),
    Create_Task(4), Assign_Task(5), Update_Task(6), Delete_Task(7),
    Create_Project(8), Update_Project(9), Delete_Project(10);


    private final int value;

    ActivityAction(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ActivityAction fromId(int id) {
        ActivityAction[] values = values();
        for (ActivityAction requestStatus : values) {
            if (requestStatus.value == id) {
                return requestStatus;
            }
        }
        return null;
    }

}
