package com.ntoannhan.onestar.oneaccount.app.delegator;

import com.ntoannhan.onestar.oneaccount.app.api.FileApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.model.FileInfo;
import com.ntoannhan.onestar.oneaccount.app.service.filestorage.FileStorageProperties;
import com.ntoannhan.onestar.oneaccount.app.service.filestorage.FileStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class FileApiDelegatorImpl implements FileApiDelegate {

    @Autowired
    private FileStorageService m_service;

    @Autowired
    private HttpServletRequest m_request;

    @Autowired
    private HttpServletResponse m_response;

    @Autowired
    private FileStorageProperties m_prop;

    private static final Logger m_logger = LogManager.getLogger(FileApiDelegatorImpl.class);

    @Override
    public ResponseEntity<FileInfo> upload(String fileData) {
        
        String fileName = null;
        try {
            fileName = m_service.storeFileInBase64(fileData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String fileDownloadUri = m_prop.getDomainName() + 
                "/api/pri/file/download/"  + fileName;
                //.toUriString();

        return ResponseEntity.ok(new FileInfo().fileId(fileName).url(fileDownloadUri));
    }

    @Override
    public ResponseEntity<Resource> download(String fileId) {
        Resource resource = null;
        try {
            resource = m_service.loadFileAsResource(fileId);
            // Try to determine file's content type
            String contentType = null;
            try {
                contentType = m_request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            } catch (IOException ex) {
                m_logger.info("Could not determine file type.");
            }

            // Fallback to the default content type if type could not be determined
            if (contentType == null) {
                contentType = "application/octet-stream";
            }
            m_response.setHeader("Cache-Control", "no-transform, public, max-age=86400");
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.status(400).build();

    }
}
