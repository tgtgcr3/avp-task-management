package com.ntoannhan.onestar.oneaccount.app.service.filestorage;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "keycloak")
public class KeycloakProperties {
  
  private String authServerUrl;

  private String realm;


  public void setAuthServerUrl(String authServerUrl) {
    this.authServerUrl = authServerUrl;
  }

  public String getAuthServerUrl() {
    return this.authServerUrl;
  }


  public String getRealm() {
    return this.realm;
  }

  public void setRealm(String realm) {
    this.realm = realm;
  }


}
