package com.ntoannhan.onestar.oneaccount.app.domain.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

@Accessors(fluent = true)
@Data
public class ExportInput {

    private String appName;

    private String reportType;

    private Map<String, Object> data;

}
