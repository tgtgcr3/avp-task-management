package com.ntoannhan.onestar.oneaccount.app.persistence;

import javax.persistence.*;
import java.util.List;

@Entity
public class OneUserTitle {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "userTitle")
    private List<OneUser> users;

    public OneUserTitle() {
    }

    public OneUserTitle(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OneUser> getUsers() {
        return users;
    }

    public void setUsers(List<OneUser> users) {
        this.users = users;
    }
}
