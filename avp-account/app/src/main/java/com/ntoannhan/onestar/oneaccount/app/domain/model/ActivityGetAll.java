package com.ntoannhan.onestar.oneaccount.app.domain.model;

import com.ntoannhan.onestar.oneaccount.app.persistence.Activity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class ActivityGetAll {

    private long total;

    private List<Activity> items;


}
