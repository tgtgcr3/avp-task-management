package com.ntoannhan.onestar.oneaccount.app.service.activitylog;

import com.ntoannhan.onestar.oneaccount.app.domain.model.ActivityAction;
import com.ntoannhan.onestar.oneaccount.app.domain.model.ActivityGetAll;
import com.ntoannhan.onestar.oneaccount.app.domain.model.ActivitySource;
import com.ntoannhan.onestar.oneaccount.app.domain.model.ActivityType;
import com.ntoannhan.onestar.oneaccount.app.persistence.Activity;
import com.ntoannhan.onestar.oneaccount.app.repository.ActivityRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;
import java.util.Collections;
import java.util.List;

@Component
public class ActivityService {

    private final ActivityRepository activityRepository;

    private static final Logger logger = LogManager.getLogger();

    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public Activity createActivity(Activity activity) {
        activity.setDate(System.currentTimeMillis());
        try {
            Activity saved = activityRepository.save(activity);
            return saved;
        } catch (Exception ex) {
            logger.error("Cannot store activity", ex);
        }
        return null;
    }

    @RolesAllowed({"admin"})
    public ActivityGetAll findActivity(ActivityType type, String username, ActivityAction action, ActivitySource source, Long startDate, Long toDate, int page, int size, Long projectId) {
        try {
            if (type == ActivityType.General) {
                long count = activityRepository.countByCondition(username, action != null ? action.getValue() : null, source != null ? source.getValue() : null, startDate, toDate);

                List<Activity> activities = activityRepository.findByCondition(username, action != null ? action.getValue() : null, source != null ? source.getValue() : null, startDate, toDate, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id")));

                return new ActivityGetAll().items(activities).total(count);
            } else if (type == ActivityType.Task) {
                long count = activityRepository.countByConditionWithProjectId(username, action != null ? action.getValue() : null, ActivitySource.Task.getValue(), projectId, startDate, toDate);

                List<Activity> activities = activityRepository.findByConditionWithProjectId(username, action != null ? action.getValue() : null, ActivitySource.Task.getValue(), projectId, startDate, toDate, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id")));

                return new ActivityGetAll().items(activities).total(count);
            }
        } catch (Exception ex) {
            logger.error("Cannot find all activity", ex);
        }

        return new ActivityGetAll().total(0).items(Collections.emptyList());
    }

}
