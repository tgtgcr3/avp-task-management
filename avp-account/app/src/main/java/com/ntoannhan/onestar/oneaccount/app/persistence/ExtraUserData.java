package com.ntoannhan.onestar.oneaccount.app.persistence;

import javax.persistence.*;

@Entity
public class ExtraUserData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    private OneUser user;

    private String name;

    private String description;

    public ExtraUserData() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OneUser getUser() {
        return user;
    }

    public void setUser(OneUser user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
