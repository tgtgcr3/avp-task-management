package com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.persistence;

import org.springframework.data.annotation.Id;

import java.util.List;

public class Users {

    @Id
    private String _id;

    private String username;

    private String name;

    private List<Address> emails;

    public Users() {
    }

    public Users(String id, String username, String name, List<Address> emails) {
        this._id = id;
        this.username = username;
        this.name = name;
        this.emails = emails;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Address> getEmails() {
        return emails;
    }

    public void setEmails(List<Address> emails) {
        this.emails = emails;
    }
}
