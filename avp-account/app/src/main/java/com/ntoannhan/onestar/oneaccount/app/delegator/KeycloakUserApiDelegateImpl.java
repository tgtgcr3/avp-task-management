package com.ntoannhan.onestar.oneaccount.app.delegator;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ntoannhan.onestar.oneaccount.app.api.KeyCloakUserApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.linked.openproject.repository.UsersRepository;
import com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.service.RCUsersService;
import com.ntoannhan.onestar.oneaccount.app.model.InlineObject;
import com.ntoannhan.onestar.oneaccount.app.model.KeyCloakUser;
import com.ntoannhan.onestar.oneaccount.app.model.UserPassword;
import com.ntoannhan.onestar.oneaccount.app.service.CachingService;
import com.ntoannhan.onestar.oneaccount.app.service.KeyCloakService;
import com.ntoannhan.onestar.oneaccount.app.service.filestorage.KeycloakProperties;
import com.ntoannhan.onestar.oneaccount.app.utils.SecurityUtils;
import net.gcardone.junidecode.Junidecode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.representations.idm.UserSessionRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class KeycloakUserApiDelegateImpl implements KeyCloakUserApiDelegate {

    private static final Logger m_logger = LogManager.getLogger(KeycloakUserApiDelegateImpl.class);

    @Autowired
    private KeyCloakService m_keycloakService;

    @Autowired
    private KeycloakProperties m_prop;

    @Autowired
    @Qualifier("openprojectUsersRepository")
    private UsersRepository m_ooRepo;

    @Autowired
    private RCUsersService m_rcUserService;

    private static final String GROUP_OWNER_NAME = "Owner";

    private static final String GROUP_Admin_NAME = "Admin";


    @Autowired
    private HttpServletRequest m_request;

    @Autowired
    private CachingService m_cachingService;

    public static final String ALL_USER_CACHE_KEY = "ALL_USER_CACHE_KEY";

//  @Autowired
//  @Qualifier("rocketChatUsersRepository")
//  private com.ntoannhan.onestar.oneaccount.app.linked.rocketchat.repository.UsersRepository m_rcRepo;


    @Override
    public ResponseEntity<Object> updateKeyCloakUser(String id, KeyCloakUser keyCloakUser) {

        String username = SecurityUtils.getCurrentUserName();
        m_logger.error("aaaaaaaaaaaaaaa");

        UsersResource users = m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users();
        UserResource userResource = users.get(id);
        if (userResource != null) {            
            UserRepresentation representation = userResource.toRepresentation();
            if (representation.getUsername().equals(username) || SecurityUtils.hasRole("admin") || SecurityUtils.hasRole("owner")) {
                m_logger.error("aaaaaaaaaaaaaaa 2");
                representation.setFirstName(keyCloakUser.getFirstName());
                representation.setLastName(keyCloakUser.getLastName());
                Map<String, List<String>> data = representation.getAttributes();
                data.put("avp_avatar", Arrays.asList(keyCloakUser.getAvatar()));
                data.put("avp_number", Arrays.asList(new String[]{keyCloakUser.getPhoneNumber()}));               
                representation.setAttributes(data);
                userResource.update(representation);       
                try {
                    m_ooRepo.updateUserInfo(representation.getId(), keyCloakUser.getEmail(), keyCloakUser.getFirstName(), keyCloakUser.getLastName());                
                } catch (Exception ex) {                    
                    m_logger.error("Cannot update user info for OO", ex);
                }
                try {
                    m_rcUserService.updateUserInfo(representation.getUsername(), keyCloakUser.getEmail(), keyCloakUser.getFirstName(), keyCloakUser.getLastName());             
                } catch (Exception ex) {                    
                    m_logger.error("Cannot update user info for RC", ex);
                }
                m_cachingService.clearCache(ALL_USER_CACHE_KEY);
                m_logger.error("bbbbbbbbbbbbbbbbbbbbb");
                clearLinkedCache();
                return ResponseEntity.ok(keyCloakUser);
            } else {
                return ResponseEntity.badRequest().body(keyCloakUser);
            }
        }
        return ResponseEntity.badRequest().body(keyCloakUser);
    }

    private void clearLinkedCache() {
        try {
            String messageLogout = m_keycloakService.getRequestUrl() + "/api/pri/internal/cache/user";
            HttpHeaders headers = new HttpHeaders();
            headers.set("X-APP-ID", "request");
            headers.set("X-TENANT-ID", "an-viet-phat");
            headers.set("X-ONE-HEADER", m_keycloakService.getRequestToken());
            HttpEntity<Object> entity = new HttpEntity<>(headers);
            m_logger.error("aaaaaaaaaaaaaaaaaaaa - cache - aaaaaaaaaaaaaaaaaaaaa");
            ResponseEntity<Object> result = m_keycloakService.getRestTemplate().exchange(messageLogout, HttpMethod.POST, entity, Object.class);
            m_logger.error("bbbbbbbbbbbbbbbbbbbb - cache - bbbbbbbbbbbbbbbbbbbbb: {}", result.getStatusCodeValue());
        }catch (Exception ex) {
            m_logger.error("Cannot clear cache for Request", ex);
        }
    }

    @Override
    public ResponseEntity<Object> getTotalUsers() {
        UsersResource users = m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users();
        int count = users.count();
        return ResponseEntity.ok(Map.of("total", count));
    }


    @Override
    public ResponseEntity<Object> getUsers(String fullName, Boolean manager, Boolean disabled, Boolean guest,
                                           String userArray, Integer offset, Integer size) {
        Object data = m_cachingService.getCache(ALL_USER_CACHE_KEY);
        if (data != null) {
            m_logger.error("aaaaaaaaaaaaaa: get from cache" );
            return ResponseEntity.ok(data);
        }
//        boolean isKCPagination = (fullName == null || fullName.trim().length() == 0) &&
//                (manager == null || manager == false) &&
//                (userArray == null || userArray.trim().length() == 0) && disabled == null && guest == null;
        Object result = getUsersUsingKeycloak(offset, 10240);
        m_cachingService.setCache(ALL_USER_CACHE_KEY, result);
        return ResponseEntity.ok(result);
    }

    @Override
    public ResponseEntity<Object> exportUsers() {
        UsersResource userResource = m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users();
        Integer count = userResource.count();
        List<UserRepresentation> users = userResource.list(0, count);
        List<String> collect = users.stream().map(user -> String.format("%s, %s, %s, %s, %s",
                user.getUsername(), user.getLastName(), user.getFirstName(), user.getEmail(), user.getFederationLink())).collect(Collectors.toList());
        return ResponseEntity.ok(Map.of("items", collect));
    }

    private Object getUsersUsingOneAccount(String fullName, Boolean manager, String userArray, Boolean disabled, Boolean guest, Integer offset,
                                           Integer size) {
        GroupsResource groupResource = m_keycloakService.getKeycloak().realm(m_prop.getRealm()).groups();
        List<GroupRepresentation> groups = groupResource.groups();

        UsersResource userResource = m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users();
        List<UserRepresentation> users = userResource.list();
        String userFullName = Junidecode.unidecode(fullName == null ? "" : fullName.trim().toLowerCase());
        List<String> usernames = new ArrayList<>();
        if (userArray != null && userArray.trim().length() > 0) {
            usernames.addAll(Arrays.asList(userArray.trim().split(",")));
            usernames.remove("");
        }


        List<UserRepresentation> collect = users.stream().filter(user -> {
            if (!Junidecode.unidecode(getFullname(user).toLowerCase()).contains(userFullName)) {
                return false;
            }
            if (manager != null && !inManagerGroup(user, manager == true)) {
                m_logger.info("fail in managerGroup");
                return false;
            }
            if (usernames.size() > 0 && usernames.indexOf(user.getUsername()) < 0) {
                m_logger.info("fail in usernames");
                return false;
            }
            if (disabled != null && !inDisabledList(user, disabled == true)) {
                m_logger.info("fail in disabled");
                return false;
            }
            if (guest != null && !inGuestList(user, guest == true)) {
                m_logger.info("fail in guest");
                return false;
            }
            return true;

        }).collect(Collectors.toList());

        offset = offset != null ? offset : 0;
        size = size != null ? size : 20;
        List<UserRepresentation> subList = collect.subList(offset, offset + size < collect.size() ? offset + size : collect.size());

        return Map.of("items", subList, "total", subList.size());
    }

    private boolean inGuestList(UserRepresentation user, boolean b) {
        List<String> groups = user.getGroups();
        if (groups == null) {
            return false;
        }
        boolean belongToManager = false;
        for (String groupName : groups) {
            if (groupName.toLowerCase().equals("guest")) {
                belongToManager = true;
                break;
            }
        }
        return b ? belongToManager : !belongToManager;
    }

    private boolean inDisabledList(UserRepresentation user, boolean b) {
        return user.isEnabled() != b;
    }

    private boolean inManagerGroup(UserRepresentation user, boolean isManager) {

        List<String> groups = user.getGroups();
        if (groups == null) {
            return false;
        }

        boolean belongToManager = false;
        for (String groupName : groups) {

            if (groupName.toLowerCase().equals("admin") || groupName.toLowerCase().equals("owner")) {

                belongToManager = true;
                break;
            }
        }
        return isManager == belongToManager;
    }

    private String getFullname(UserRepresentation user) {
        String firstName = user.getFirstName() == null ? "" : user.getFirstName();
        String lastName = user.getLastName() == null ? "" : user.getLastName();
        String fullName = lastName.trim() + " " + firstName.trim();
        return fullName.trim();
    }

    private Object getUsersUsingKeycloak(Integer offset, Integer size) {
        RealmResource realmResource = m_keycloakService.getKeycloak().realm(m_prop.getRealm());
        UsersResource userResource = realmResource.users();
        List<UserRepresentation> users = userResource.list(offset == null ? 0 : offset, size);
        if (SecurityUtils.hasRole("admin") || SecurityUtils.hasRole("owner")) {
            GroupsResource groupsResource = realmResource.groups();
            List<GroupRepresentation> allGroup = groupsResource.groups();
            allGroup.forEach(g -> {
                List<UserRepresentation> members = groupsResource.group(g.getId()).members();
                members.forEach(gu -> {
                    for (UserRepresentation u : users) {
                        if (u.getId().equals(gu.getId())) {
                            List<String> groups = u.getGroups();
                            if (groups == null) {
                                groups = new ArrayList<String>();
                                u.setGroups(groups);
                            }
                            groups.add(g.getName());
                            break;
                        }
                    }
                });
            });
        }

        return Map.of("items", users, "total", userResource.count());
    }

    private String getAttribute(Map<String, List<String>> attributes, String attrName) {
        List<String> attrNames = attributes.get(attrName);
        return attrNames != null && attrNames.size() > 0 ? attrNames.get(0) : "";
    }

    @Override
    public ResponseEntity<Object> getKeyCloakUser(String username,
                                                  String email) {
        UsersResource users = m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users();
        List<UserRepresentation> userPreps = null;
        if (username != null && username.trim().length() > 0) {
            userPreps = users.search(username.trim(), true);
        } else if (email != null && email.trim().length() > 0) {
            userPreps = users.search(email.trim(), 0, 10);
            userPreps = userPreps.stream().filter(u -> email.equals(u.getEmail())).collect(Collectors.toList());
        }
        if (userPreps != null && userPreps.size() > 0) {
            UserRepresentation representation = userPreps.get(0);
            Map<String, List<String>> attributes = representation.getAttributes();
            if (attributes == null) {
                attributes = new HashMap<>();
            }
            String avatar = getAttribute(attributes, "avp_avatar");
            String phoneNumber = getAttribute(attributes, "avp_number");
            String title = getAttribute(attributes, "avp_title");

            KeyCloakUser user = new KeyCloakUser()
                    .firstName(representation.getFirstName())
                    .lastName(representation.getLastName())
                    .avatar(avatar)
                    .phoneNumber(phoneNumber)
                    .title(title)
                    .username(representation.getUsername());

            return ResponseEntity.ok(user);

        }
        return ResponseEntity.badRequest().body(new KeyCloakUser());
    }

    @Override
    public ResponseEntity<Object> updateUserPassword(String username, UserPassword userPassword) {

        String userAuth = getCurrentUsername();
        if (userAuth == null || !userAuth.trim().equals(username)) {
            return ResponseEntity.badRequest().build();
        }

        if (userPassword.getNewPassword() == null || !userPassword.getNewPassword().equals(userPassword.getConfirmPassword())) {
            return ResponseEntity.badRequest().body(Map.of("status", "new_password_not_match"));
        }

        UsersResource userResource = m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users();        
        boolean found = false;
        List<UserRepresentation> users = userResource.search(username.trim(), true);
        if (users.size() == 1) {
            UserRepresentation userRepresentation = users.get(0);
            CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
            credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
            credentialRepresentation.setTemporary(false);
            credentialRepresentation.setValue(userPassword.getNewPassword());
            userResource.get(userRepresentation.getId()).resetPassword(credentialRepresentation);
            found = true;
        } else {
            m_logger.info("not found");
        }

        if (found) {
            return ResponseEntity.ok(Map.of("status", "ok"));
        }
        return ResponseEntity.badRequest().body(Map.of("status", "password_not_correct"));
    }

    private String getCurrentUsername() {
        //return "toannhanb7";
        String username = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            username = authentication.getName();

        }

        m_logger.debug("aaaaaaaaaaaaaaaaaaaa: " + String.valueOf(username));
        return username;
    }

  @Override
  public ResponseEntity<Object> logout(InlineObject inlineObject1) {
      String token = inlineObject1.getToken();
      if (token != null) {
          RealmResource realmResource = m_keycloakService.getKeycloak().realm(m_prop.getRealm());
          UserResource kcUser = null;
          if (token.indexOf("@", 2) > 0) {
              kcUser = findKeycloakUserUsingEmail(token);
          } else if (token.length() < 15) {
              kcUser = findKeycloakUserUsingUsername(token);
          }
          if (kcUser != null) {
              List<UserSessionRepresentation> sessions = kcUser.getUserSessions();
              for(UserSessionRepresentation session : sessions) {
                  realmResource.deleteSession(session.getId());
              }
              logoutFromLinkedServices(kcUser);
              callClientLogout(kcUser);
              return ResponseEntity.ok().body(Map.of("status", "ok"));
          }
          DecodedJWT jwt = JWT.decode(token);
          String sessionId = jwt.getClaim("session_state").asString();
          realmResource.deleteSession(sessionId);
          return ResponseEntity.ok().body(Map.of("status", "ok"));
      }
    return null;
  }

    private void callClientLogout(UserResource kcUser) {
        try {
            String username = kcUser.toRepresentation().getUsername();
            String messageLogout = m_keycloakService.getNotificationUrl() + "/api/pri/internal/notifications/all/logout/" + username;
            m_keycloakService.getRestTemplate().postForEntity(messageLogout, null, Map.class);
        } catch (Exception ex) {
            m_logger.error("Cannot call client logout", ex);
        }
    }

    private void logoutFromLinkedServices(UserResource kcUser) {
      UserRepresentation userRepresentation = kcUser.toRepresentation();
      try {
          logoutFromMessage(userRepresentation);
      } catch (Exception ex) {
          m_logger.error("Cannot logout from Message", ex);
      }

      try {
          logoutFromTask(userRepresentation);
      } catch (Exception ex) {
          m_logger.error("Cannot logout from Task", ex);
      }
  }

  private void logoutFromTask(UserRepresentation kcUser) {
    String messageLogout = m_keycloakService.getTaskUrl() + "/avp/avp_logout?email=" + kcUser.getEmail();
    m_keycloakService.getRestTemplate().getForEntity(messageLogout, Object.class);
  }

  private void logoutFromMessage(UserRepresentation kcUser) {
    String messageLogout = m_keycloakService.getMessageUrl() + "/api/v1/ntn/me/logout?username=" + kcUser.getUsername();
    m_keycloakService.getRestTemplate().getForEntity(messageLogout, Map.class);
  }

  @Override
  public ResponseEntity<Object> enableUser(String id, Boolean activated) {
        if (activated == null || id == null) {
            return ResponseEntity.status(400).body(Map.of("error", "missing param"));
        }
      RealmResource realmResource = m_keycloakService.getKeycloak().realm(m_prop.getRealm());
      UserResource userResource = realmResource.users().get(id);
      if (userResource == null) {
          return ResponseEntity.status(400).body(Map.of("error", "user_not_found"));
      }
      try {
          UserRepresentation user = userResource.toRepresentation();
          user.setEnabled(activated);
          userResource.update(user);
          try {
              m_rcUserService.enableUser(user.getUsername(), activated);
          } catch (Exception ex) {
              m_logger.error("Cannot update user status for rc", ex);
          }
          try {
              m_ooRepo.enableUser(id, activated);
          } catch (Exception ex) {
              m_logger.error("Cannot update user status for op", ex);
          }
          m_cachingService.clearCache(ALL_USER_CACHE_KEY);
          return ResponseEntity.ok(Map.of("msg", "ok"));
      } catch (Exception ex) {
          m_logger.error("Cannot update user status", ex);
      }
      return ResponseEntity.status(500).body(Map.of("error", "internal"));
  }

  private UserResource findKeycloakUserUsingEmail(String email) {
      UsersResource usersResource = m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users();
      List<UserRepresentation> list = usersResource.search(null, null, null, email, 0, 10);
      
      for (UserRepresentation ur : list) {
          if (email.equals(ur.getEmail())) {
              return m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users().get(ur.getId());
          }
      }
      return null;
  }

    private UserResource findKeycloakUserUsingUsername(String username) {
        UsersResource usersResource = m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users();
        List<UserRepresentation> list = usersResource.search(username, true);
        for (UserRepresentation ur : list) {
            if (username.equals(ur.getUsername())) {
                return m_keycloakService.getKeycloak().realm(m_prop.getRealm()).users().get(ur.getId());
            }
        }
        return null;
    }
}
