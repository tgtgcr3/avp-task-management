package com.ntoannhan.onestar.oneaccount.app.persistence;

import javax.persistence.*;
import java.util.List;

@Entity
public class OneUser {

    @Id
    private String username;

    @Lob
    private String avatarData;

    private String avatarUrl;

    private String email;

    @OneToOne
    private OneUserTitle userTitle;

    private String directManagers;

    private long birthday;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ExtraUserData> studies;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ExtraUserData> contacts;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ExtraUserData> rewards;

    public List<ExtraUserData> getExperiences() {
        return experiences;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public long getBirthday() {
        return birthday;
    }

    public void setBirthday(long birthday) {
        this.birthday = birthday;
    }

    public void setExperiences(List<ExtraUserData> experiences) {
        this.experiences = experiences;
    }

    @OneToMany(cascade = CascadeType.ALL)
    private List<ExtraUserData> experiences;

    public OneUser() {
    }

    public OneUser(String username, String avatarUrl) {
        this.username = username;
        this.avatarData = avatarUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatarData() {
        return avatarData;
    }

    public void setAvatarData(String avatarData) {
        this.avatarData = avatarData;
    }

    public OneUserTitle getUserTitle() {
        return userTitle;
    }

    public void setUserTitle(OneUserTitle userTitle) {
        this.userTitle = userTitle;
    }

    public String getDirectManagers() {
        return directManagers;
    }

    public void setDirectManagers(String directManagers) {
        this.directManagers = directManagers;
    }

    public List<ExtraUserData> getStudies() {
        return studies;
    }

    public void setStudies(List<ExtraUserData> studies) {
        this.studies = studies;
    }

    public List<ExtraUserData> getContacts() {
        return contacts;
    }

    public void setContacts(List<ExtraUserData> contacts) {
        this.contacts = contacts;
    }

    public List<ExtraUserData> getRewards() {
        return rewards;
    }

    public void setRewards(List<ExtraUserData> rewards) {
        this.rewards = rewards;
    }


}
