package com.ntoannhan.onestar.oneaccount.app.utils;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class SecurityUtils {

    public static boolean hasRole(String roleName) {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        roleName = "_" + roleName.toLowerCase();
        for (GrantedAuthority author : authorities) {
            if (author.getAuthority().toLowerCase().endsWith(roleName)) {
                return true;
            }
        }
        return false;
    }

    public static String getCurrentUserName() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = null;
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }

}
