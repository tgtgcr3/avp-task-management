package com.ntoannhan.onestar.oneaccount.app.domain.usecase;

import com.ntoannhan.onestar.oneaccount.app.core.domain.usecase.UseCase;
import com.ntoannhan.onestar.oneaccount.app.core.failure.Failure;
import com.ntoannhan.onestar.oneaccount.app.domain.model.ExportInput;
import com.ntoannhan.onestar.oneaccount.app.service.importexport.ImportExportService;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExportData implements UseCase<byte[], ExportInput> {

    @Autowired
    ImportExportService m_service;

    @Override
    public Either<Failure, byte[]> execute(ExportInput input) {
        byte[] exportedData = m_service.export(input.appName(), input.reportType(), input.data());
        if (exportedData == null) {
            return Either.left(new Failure().reason("Cannot export data"));
        }
        return Either.right(exportedData);
    }
}
