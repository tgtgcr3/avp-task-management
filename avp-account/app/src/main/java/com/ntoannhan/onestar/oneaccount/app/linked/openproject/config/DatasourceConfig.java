package com.ntoannhan.onestar.oneaccount.app.linked.openproject.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class DatasourceConfig {

    private DataSource m_dataSource;

    @Autowired
    OpenProjectProperties m_props;

    public DataSource getDataSource() {
        if (m_dataSource == null) {
            DataSourceBuilder builder = DataSourceBuilder.create();
            builder.driverClassName(m_props.getDriverClassName());
            builder.url(m_props.getJdbcUrl());
            builder.username(m_props.getUsername());
            builder.password(m_props.getPassword());
            m_dataSource = builder.build();
        }
        return m_dataSource;
    }

}
