package com.ntoannhan.onestar.oneaccount.app.config;

import com.ntoannhan.onestar.oneaccount.app.service.props.ServerProp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

@Component
public class HeaderAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private ServerProp serverProp;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String oneHeader = request.getHeader("X-ONE-Header");
        if (oneHeader == null || oneHeader.trim().length() == 0) {
            oneHeader = request.getHeader("X-ONE-HEADER");
        }

        if (oneHeader != null && oneHeader.trim().length() > 0) {
            if (serverProp != null && oneHeader.equals(serverProp.getInternalToken())) {
                User user = findByToken(oneHeader);
                final UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(request, response);
    }

    private User findByToken(String token) {
        return new User(
                "internal",
                "password",
                true,
                true,
                true,
                true,
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_user")));

    }
}
