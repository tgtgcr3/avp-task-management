package com.ntoannhan.onestar.oneaccount.app.service.props;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "myserver")
public class ServerProp {

    private String messageUrl;

    private String taskUrl;

    private String notificationUrl;

    private String taskToken;

    private int taskProjectDefaultId;

    private int taskDefaultRoleId;

    private String internalToken;

    private String requestUrl;

    private String requestToken;

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getTaskUrl() {
        return taskUrl;
    }

    public void setTaskUrl(String taskUrl) {
        this.taskUrl = taskUrl;
    }

    public String getNotificationUrl() {
        return notificationUrl;
    }

    public void setNotificationUrl(String notificationUrl) {
        this.notificationUrl = notificationUrl;
    }

    public String getTaskToken() {
        return taskToken;
    }

    public void setTaskToken(String taskToken) {
        this.taskToken = taskToken;
    }

    public int getTaskProjectDefaultId() {
        return taskProjectDefaultId;
    }

    public void setTaskProjectDefaultId(int taskProjectDefaultId) {
        this.taskProjectDefaultId = taskProjectDefaultId;
    }

    public int getTaskDefaultRoleId() {
        return taskDefaultRoleId;
    }

    public void setTaskDefaultRoleId(int taskDefaultRoleId) {
        this.taskDefaultRoleId = taskDefaultRoleId;
    }

    public String getInternalToken() {
        return internalToken;
    }

    public void setInternalToken(String internalToken) {
        this.internalToken = internalToken;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }
}
