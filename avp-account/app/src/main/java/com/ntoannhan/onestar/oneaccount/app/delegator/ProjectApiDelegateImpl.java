package com.ntoannhan.onestar.oneaccount.app.delegator;

import com.ntoannhan.onestar.oneaccount.app.api.ProjectApiDelegate;
import com.ntoannhan.onestar.oneaccount.app.linked.openproject.repository.ProjectRepository;
import com.ntoannhan.onestar.oneaccount.app.model.Project;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Component
public class ProjectApiDelegateImpl implements ProjectApiDelegate {

    private final ProjectRepository projectRepository;


    @Override
    public ResponseEntity<List<Project>> getAllProjects(String name) {
        return ResponseEntity.ok(
                projectRepository.findProject(name).stream().map(p -> new Project()
                        .id(p.getId())
                        .name(p.getName())
                ).collect(Collectors.toList())
        );
    }
}
