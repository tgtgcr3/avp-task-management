package com.ntoannhan.onestar.oneaccount.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
public class OneAccountApp {

    public static void main(String[] args) {
        SpringApplication.run(OneAccountApp.class, args);
    }

}


