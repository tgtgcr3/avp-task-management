package com.ntoannhan.onestar.onerequest.core.data.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class ResultGetAllData<T> {

    private int total;

    private List<T> items;

}
