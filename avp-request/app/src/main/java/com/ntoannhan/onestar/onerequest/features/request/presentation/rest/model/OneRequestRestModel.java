package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model;

import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Request;
import com.ntoannhan.onestar.onerequest.rest.model.OneRequest;
import org.modelmapper.ModelMapper;

public class OneRequestRestModel extends AbstractBaseRestModel<Request, OneRequest> {

    @Override
    public OneRequest toRestModel(Request request) {
        return OneMapper.mapTo(request, OneRequest.class);
    }

    @Override
    public Request toDomainModel(OneRequest oneRequest) {
        return OneMapper.mapTo(oneRequest, Request.class);
    }

}
