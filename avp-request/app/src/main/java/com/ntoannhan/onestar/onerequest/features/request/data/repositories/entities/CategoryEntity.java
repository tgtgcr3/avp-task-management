package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.AbstractBaseEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class CategoryEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String name;

}
