package com.ntoannhan.onestar.onerequest.features.discussion.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.discussion.data.repositories.entities.DiscussionEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DiscussionJpa extends JpaRepository<DiscussionEntity, Long> {

    @Query("SELECT d FROM Discussion d WHERE d.id = :id")
    @Override
    Optional<DiscussionEntity> findById(Long id);

    @Query("SELECT d FROM Discussion d WHERE d.id = :id AND d.targetId = :targetId AND d.targetType = :targetType")
    Optional<DiscussionEntity> findById(long targetType, long targetId, long id);

    @Query("SELECT COUNT(d) FROM Discussion d WHERE d.targetType = :targetType AND d.targetId = :targetId AND d.status <> 2")
    int countAllWithTarget(long targetType, long targetId);

    @Query("SELECT d FROM Discussion d WHERE d.targetType = :targetType AND d.targetId = :targetId AND d.status <> 2")
    List<DiscussionEntity> getAllWithTarget(long targetType, long targetId, Pageable pageable);
}
