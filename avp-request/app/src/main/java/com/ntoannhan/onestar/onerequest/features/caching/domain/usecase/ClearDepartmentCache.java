package com.ntoannhan.onestar.onerequest.features.caching.domain.usecase;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import io.vavr.control.Either;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

@Component
public class ClearDepartmentCache implements UseCase<Boolean, Boolean> {

    @CacheEvict(value = "department-all-department", allEntries = true)
    @Override
    public Either<Exception, Boolean> call(Boolean in) {
        return Either.right(Boolean.TRUE);
    }
}
