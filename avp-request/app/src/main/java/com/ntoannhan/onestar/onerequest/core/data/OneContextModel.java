package com.ntoannhan.onestar.onerequest.core.data;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class OneContextModel {

    private String tenantId;

    private String username;

    private String description;

}
