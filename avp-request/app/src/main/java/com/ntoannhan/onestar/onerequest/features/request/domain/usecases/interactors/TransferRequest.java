package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupForwardService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.TransferRequestIn;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class TransferRequest implements UseCase<Boolean, TransferRequestIn> {

    private final RequestGroupForwardService requestGroupForwardService;

    public TransferRequest(RequestGroupForwardService requestGroupForwardService) {
        this.requestGroupForwardService = requestGroupForwardService;
    }

    @Override
    public Either<Exception, Boolean> call(TransferRequestIn in) {
        return requestGroupForwardService.transfer(in.requestId(), in.targetGroupId());
    }
}
