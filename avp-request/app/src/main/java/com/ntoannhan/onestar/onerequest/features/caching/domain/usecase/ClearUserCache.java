package com.ntoannhan.onestar.onerequest.features.caching.domain.usecase;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.services.UserService;
import io.vavr.control.Either;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

@Component
public class ClearUserCache implements UseCase<Boolean, Boolean> {

    @CacheEvict(value = {"keycloak-alluser", UserService.CACHE_KEY_ONE_ALL_USER}, allEntries = true)
    @Override
    public Either<Exception, Boolean> call(Boolean in) {
        return Either.right(Boolean.TRUE);
    }
}
