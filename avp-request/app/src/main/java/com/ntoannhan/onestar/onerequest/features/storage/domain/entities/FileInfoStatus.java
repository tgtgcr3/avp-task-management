package com.ntoannhan.onestar.onerequest.features.storage.domain.entities;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;

public enum FileInfoStatus {
    CREATED(0), DELETED(1);

    private final int value;

    FileInfoStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static FileInfoStatus fromId(int id) {
        FileInfoStatus[] values = values();
        for (FileInfoStatus fileInfoStatus : values) {
            if (fileInfoStatus.value == id) {
                return fileInfoStatus;
            }
        }
        return null;
    }
}
