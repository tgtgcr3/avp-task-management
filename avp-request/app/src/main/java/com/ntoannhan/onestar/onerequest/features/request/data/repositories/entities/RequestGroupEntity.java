package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.AbstractBaseEntity;
import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.TenantAware;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "RequestGroup")
@Audited
public class RequestGroupEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String name;

    @Column(columnDefinition = "int default 0")
    private int mgmtNotification;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @OneToOne
    private CategoryEntity category;

    private Long targetEntityType;

    private Long targetEntityId;

    @NotAudited
    @OneToMany(mappedBy = "requestGroup")
    @ToString.Exclude
    private List<RequestGroupForwardEntity> forwards;


    @Column(columnDefinition = "TEXT")
    private String description;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY)
    private List<RequestGroupVersionEntity> requestGroupVersions;

    private String creator;

}
