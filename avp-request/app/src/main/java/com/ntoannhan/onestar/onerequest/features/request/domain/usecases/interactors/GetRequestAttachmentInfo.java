package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetRequestAttachmentInfosIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetRequestAttachmentInfosOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetRequestAttachmentInfo implements UseCase<GetRequestAttachmentInfosOut, GetRequestAttachmentInfosIn> {

    private final RequestService requestService;

    @Autowired
    public GetRequestAttachmentInfo(RequestService requestService) {
        this.requestService = requestService;
    }

    @Override
    public Either<Exception, GetRequestAttachmentInfosOut> call(GetRequestAttachmentInfosIn in) {
        return requestService.getAttachmentFileInfos(in.id(), in.fileIds()).fold(Either::left, out -> Either.right(new GetRequestAttachmentInfosOut().fileInfos(out)));
    }
}
