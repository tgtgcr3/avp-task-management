package com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.interceptor;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.services.DiscussionService;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in.DeleteDiscussionIn;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DeleteDiscussion implements UseCase<Boolean, DeleteDiscussionIn> {

    private final DiscussionService discussionService;

    public DeleteDiscussion(@Qualifier("InternalDiscussionService") DiscussionService discussionService) {
        this.discussionService = discussionService;
    }

    @Override
    public Either<Exception, Boolean> call(DeleteDiscussionIn in) {
        return discussionService.deleteDiscussion(in.targetType(), in.targetId(), in.id());
    }
}
