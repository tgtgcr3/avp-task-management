package com.ntoannhan.onestar.onerequest.features.request.data.services;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.OneRevisionEntity;
import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestGroupVersionEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestApprovalJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestGroupVersionJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections.GeneralReportDataProjection;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections.RequestGroupReport;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.ActionReportData;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.GeneralReportData;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.ReportService;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.RevisionType;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Component
public class ReportServiceImpl implements ReportService {

    private final RequestJpa requestJpa;

    private final RequestApprovalJpa requestApprovalJpa;

    private final RequestGroupVersionJpa requestGroupVersionJpa;

    private final EntityManagerFactory factory;

    public ReportServiceImpl(RequestJpa requestJpa, RequestApprovalJpa requestApprovalJpa, RequestGroupVersionJpa requestGroupVersionJpa, EntityManagerFactory factory) {
        this.requestJpa = requestJpa;
        this.requestApprovalJpa = requestApprovalJpa;
        this.requestGroupVersionJpa = requestGroupVersionJpa;
        this.factory = factory;
    }

    @Override
    public Either<Exception, GeneralReportData> generateGeneralReport(Long groupId, Long fromDate, Long toDate) {
        GeneralReportData reportData = new GeneralReportData();
        if (groupId != null) {
            Optional<RequestGroupVersionEntity> groupOps = requestGroupVersionJpa.findById(groupId);
            if (groupOps.isEmpty()) {
                return Either.right(reportData);
            }
            groupId = groupOps.get().getRequestGroup().getId();
        }
        Map<Integer, Integer> resultMap = requestJpa.countRequestByStatus(groupId, fromDate, toDate).stream().collect(Collectors.toMap(GeneralReportDataProjection::getStatus, GeneralReportDataProjection::getTotal));

        reportData.approved(resultMap.getOrDefault(RequestStatus.APPROVED.getValue(), 0));
        reportData.rejected(resultMap.getOrDefault(RequestStatus.REJECTED.getValue(), 0));
        reportData.total(resultMap.values().stream().mapToInt(t -> t).sum());
        reportData.timeoutNum(resultMap.getOrDefault(RequestStatus.TIMEOUT.getValue(), 0));
        if (
                reportData.total() > 0
        ) {
            reportData.timeout((double) Math.round((double) (resultMap.getOrDefault(RequestStatus.TIMEOUT.getValue(), 0) * 100 / reportData.total()) * 100) / 100);
        }
        reportData.highActive(requestApprovalJpa.findHighActiveApprover());
        return Either.right(reportData);
    }

    @Override
    public Either<Exception, Map<Long, ActionReportData>> generateActionReport(Long groupId, Long fromDate, Long toDate) {
        if (fromDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            fromDate = calendar.getTimeInMillis();
            calendar.set(Calendar.DAY_OF_MONTH, 31);
            toDate = calendar.getTimeInMillis();
        }

        Map<Long, MyActionReportData> dataByWeek = new HashMap<>();
        if (groupId != null) {
            Optional<RequestGroupVersionEntity> groupOps = requestGroupVersionJpa.findById(groupId);
            if (groupOps.isEmpty()) {
                return Either.right(dataByWeek.values().stream().collect(Collectors.toMap(o -> o.toDate, o -> (ActionReportData) o)));
            }
            groupId = groupOps.get().getRequestGroup().getId();
        }
        EntityManager entityManager = null;
        try {
            entityManager = factory.createEntityManager();
            AuditReader reader = AuditReaderFactory.get(entityManager);
            AuditQuery query = reader.createQuery().forRevisionsOfEntity(RequestEntity.class, false, false)
                    .add(AuditEntity.revisionProperty("timestamp").ge(fromDate))
                    .add(AuditEntity.revisionProperty("timestamp").lt(toDate));
            List resultList = query.getResultList();

            long aWeek = 24 * 3600 * 1000 * 7;
            while (fromDate + aWeek < toDate) {
                fromDate += aWeek;
                dataByWeek.put(fromDate, new MyActionReportData().fromDate(fromDate - aWeek).toDate(fromDate));

            }
            if (fromDate < toDate) {
                dataByWeek.put(toDate, new MyActionReportData().fromDate(fromDate).toDate(toDate));
            }

            Long finalGroupId = groupId;
            for (Object data : resultList) {

                Object[] arrData = (Object[]) data;
                RequestEntity entity = (RequestEntity) arrData[0];
                OneRevisionEntity auditEntity = (OneRevisionEntity) arrData[1];
                RevisionType action = (RevisionType) arrData[2];
                try {
                    if (finalGroupId != null && entity.getRequestGroupVersionEntity().getRequestGroup().getId() != finalGroupId) {
                        continue;
                    }
                } catch (Exception ex) {
                    continue;
                }
                for (Map.Entry<Long, MyActionReportData> entry : dataByWeek.entrySet()) {
                    MyActionReportData reportData = entry.getValue();
                    if (reportData.fromDate <= auditEntity.getTimestamp() && auditEntity.getTimestamp() <= reportData.toDate) {

                        if (action == RevisionType.ADD) {
                            reportData.created(reportData.created() + 1);
                        }
                        if (action == RevisionType.MOD) {
                            if (entity.getStatus() == RequestStatus.APPROVED.getValue()) {
                                reportData.approved(reportData.approved() + 1);
                            } else if (entity.getStatus() == RequestStatus.REJECTED.getValue()) {
                                reportData.rejected(reportData.rejected() + 1);
                            }
                        }
                        break;
                    }
                }
            }


            return Either.right(dataByWeek.values().stream().collect(Collectors.toMap(o -> o.toDate, o -> (ActionReportData) o))
            );
        } finally {
            if (entityManager != null) {
                try {
                    entityManager.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    @Override
    public Either<Exception, GeneralReportData> getRequestGroupReport(long requestGroupId) {
        try {
            Optional<RequestGroupVersionEntity> requestVersionOpt = requestGroupVersionJpa.findById(requestGroupId);
            if (requestVersionOpt.isEmpty()) {
                return Either.left(new UserException("request_group_not_found"));
            }
            Long groupId = requestVersionOpt.get().getRequestGroup().getId();
            List<RequestGroupReport> requestGroupReport = requestGroupVersionJpa.getRequestGroupReport(groupId);
            int total = requestGroupReport.stream().mapToInt(RequestGroupReport::getTotal).sum();
            int processed = requestGroupReport.stream().filter(r -> r.getStatus() == RequestStatus.REJECTED.getValue() || r.getStatus() == RequestStatus.APPROVED.getValue()).mapToInt(RequestGroupReport::getTotal).sum();
            int approved = requestGroupReport.stream().filter(r -> r.getStatus() == RequestStatus.APPROVED.getValue()).mapToInt(RequestGroupReport::getTotal).sum();
            return Either.right(new GeneralReportData().total(total).processed(processed).approved(approved));
        } catch (Exception ex) {
            log.error("cannot process Request", ex);
            return Either.left(new AppException("cannot_process_request"));
        }
    }


    static class MyActionReportData extends ActionReportData {
        private long fromDate;

        private long toDate;

        public MyActionReportData fromDate(Long fromDate) {
            this.fromDate = fromDate;
            return this;
        }

        public MyActionReportData toDate(Long toDate) {
            this.toDate = toDate;
            return this;
        }
    }
}
