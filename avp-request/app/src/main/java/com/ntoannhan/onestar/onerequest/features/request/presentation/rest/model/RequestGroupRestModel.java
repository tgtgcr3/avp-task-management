package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model;


import com.ntoannhan.onestar.onerequest.rest.model.RequestGroup;
import org.modelmapper.ModelMapper;


public class RequestGroupRestModel{

    private ModelMapper modelMapper;

    public RequestGroupRestModel(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public RequestGroup convertToRestModel(com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup requestGroup) {
        RequestGroup restModel = new RequestGroup();

        modelMapper.map(requestGroup, restModel);

        return restModel;
    }

    public com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup convertToDomainModel(RequestGroup requestGroup) {
        com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup domainModel = new com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup();
        modelMapper.map(requestGroup, domainModel);
        return domainModel;
    }

}
