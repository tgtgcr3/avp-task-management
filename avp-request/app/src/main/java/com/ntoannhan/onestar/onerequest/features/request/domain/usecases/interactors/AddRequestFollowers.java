package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Request;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.AddRequestFollowersIn;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class AddRequestFollowers implements UseCase<Request, AddRequestFollowersIn> {

    private final RequestService requestService;

    public AddRequestFollowers(RequestService requestService) {
        this.requestService = requestService;
    }

    @Override
    public Either<Exception, Request> call(AddRequestFollowersIn in) {
        return requestService.updateFollowers(in.requestId(), in.followers(), true);
    }
}
