package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out;

import com.ntoannhan.onestar.onerequest.core.usecases.out.UseCaseDataOut;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup;
import lombok.*;
import lombok.experimental.Accessors;
import org.modelmapper.ModelMapper;

public class GetOneRequestGroupOut extends RequestGroup implements UseCaseDataOut<RequestGroup, GetOneRequestGroupOut> {

    @Override
    public GetOneRequestGroupOut fromDomainEntity(RequestGroup requestGroup) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(requestGroup, this);

        return this;
    }
}
