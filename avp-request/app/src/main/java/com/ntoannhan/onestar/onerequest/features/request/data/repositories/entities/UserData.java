package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserData {

    private String username;

    private String email;

    private String directMgmts;

}
