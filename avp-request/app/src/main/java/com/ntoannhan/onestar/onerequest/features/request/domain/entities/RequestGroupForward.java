package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

import lombok.Data;

@Data
public class RequestGroupForward {

    private Long id;

    private RequestStatus requestStatus;

    private Long targetId;

    private String targetName;

}
