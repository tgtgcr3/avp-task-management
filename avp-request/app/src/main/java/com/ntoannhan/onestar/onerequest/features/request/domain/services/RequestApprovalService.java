package com.ntoannhan.onestar.onerequest.features.request.domain.services;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import io.vavr.control.Either;

import java.util.List;

public interface RequestApprovalService {

    Either<Exception, Boolean > evaluateRequest(long requestId, RequestStatus status, List<String> targetUsers, String description);

    Either<Exception, Boolean> addStatus(long requestId, RequestStatus status);
}
