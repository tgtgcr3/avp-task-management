package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Request;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetRequestIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetRequestOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetRequest implements UseCase<GetRequestOut, GetRequestIn> {

    @Autowired
    private RequestService requestService;

    @Override
    public Either<Exception, GetRequestOut> call(GetRequestIn in) {
        return requestService.getRequest(in.requestId()).fold(Either::left,
                request -> Either.right(new GetRequestOut().request(request)));
    }
}
