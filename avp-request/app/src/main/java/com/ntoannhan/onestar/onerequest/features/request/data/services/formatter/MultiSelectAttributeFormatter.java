package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Objects;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import lombok.SneakyThrows;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MultiSelectAttributeFormatter implements AttributeFormatter{

    @Override
    public String formatData(Long requestId, Object value, Attribute attribute, Object extra) throws Exception {
        if (attribute.additionData() != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            SelectAdditionData additionData = objectMapper.readValue(attribute.additionData(), SelectAdditionData.class);
            if (value == null) {
                value = Arrays.asList(additionData.getDefaultValue());
            }
            if (value instanceof List) {
                List values = (List)value;
                List<String> data = (List<String>) values.stream().map(v -> {
                    String vStr = "";
                    for (SelectKeyValue keyValue : additionData.getValues()) {
                        if (Objects.equal(keyValue.getValue(), v)) {
                            vStr = keyValue.getName();
                            break;
                        }
                    }
                    return vStr;
                }).collect(Collectors.toList());
                return String.join(", ", data);
            }
        }
        return String.valueOf(value);
    }
}
