package com.ntoannhan.onestar.onerequest.features.request.data.repositories.services;

import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.GetAllUserData;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.UserData;
import com.ntoannhan.onestar.onerequest.interfaces.config.AppConfiguration;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Log4j2
@Component
public class UserService {

    private final RestTemplate restTemplate;

    private final AppConfiguration appConfiguration;

    public static final String CACHE_KEY_ONE_ALL_USER = "ONE_ALL_USER";

    public UserService(RestTemplateBuilder templateBuilder, AppConfiguration appConfiguration) {
        this.restTemplate = templateBuilder.build();
        this.appConfiguration = appConfiguration;
    }

    @Cacheable(CACHE_KEY_ONE_ALL_USER)
    public List<UserData> getAllUser() {
        try {
            String url = this.appConfiguration.getAccountUrl() + "/api/pri/users";

            HttpHeaders headers = new HttpHeaders();
            headers.set("X-APP-ID", "request");
            headers.set("X-TENANT-ID", "an-viet-phat");
            headers.set("X-ONE-HEADER", appConfiguration.getAccountToken());

            HttpEntity<Object> entity = new HttpEntity<>(headers);

            log.debug("Send request to account to get user data");

            ResponseEntity<GetAllUserData> exchange = restTemplate.exchange(url, HttpMethod.GET, entity, GetAllUserData.class);
            if (exchange.getStatusCode() == HttpStatus.OK) {
                log.debug("Request is OK. UserData is returned");
                return exchange.getBody().getItems();
            }

            log.error("cannot get user data from account: {}", exchange.getStatusCodeValue());

            return Collections.emptyList();
        } catch (Exception ex) {
            log.error("Cannot send notification", ex);
            return Collections.emptyList();
        }
    }

    @CacheEvict(value = CACHE_KEY_ONE_ALL_USER, allEntries = true)
    public void clearEverything() {
    }

}
