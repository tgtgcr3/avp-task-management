package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SelectKeyValue {

    private String name;

    private Integer value;
}
