package com.ntoannhan.onestar.onerequest.features.discussion.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.AbstractBaseEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.ArrayStringTypeWrapper;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name="Discussion")
@Audited
public class DiscussionEntity extends AbstractBaseEntity implements ArrayStringTypeWrapper {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String owner;

    @Column(columnDefinition = "MEDIUMTEXT")
    private String content;

    private String mentioned;

    private String fileIds;

    private Integer status;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private long createdDate;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private long lastUpdatedDate;

    private long targetType;

    private long targetId;

    public void oneSetMentioneds(List<String> value) {
        this.mentioned = fromStringArray(value);
    }

    public List<String> oneGetMemtioneds() {
        return toListString(this.mentioned);
    }

    public void oneSetFileIds(List<String> value) {
        this.fileIds = fromStringArray(value);
    }

    public List<String> oneGetFileIds() {
        return toListString(this.fileIds);
    }


}
