package com.ntoannhan.onestar.onerequest.core.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.listener.AuditListener;
import lombok.Data;
import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity
@RevisionEntity(AuditListener.class)
public class OneRevisionEntity extends DefaultRevisionEntity {

    private String username;

    @Column(columnDefinition = "TEXT")
    private String description;

}
