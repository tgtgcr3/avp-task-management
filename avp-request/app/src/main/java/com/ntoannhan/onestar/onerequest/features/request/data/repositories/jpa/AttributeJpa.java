package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.AttributeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface AttributeJpa extends JpaRepository<AttributeEntity, Long> {

}
