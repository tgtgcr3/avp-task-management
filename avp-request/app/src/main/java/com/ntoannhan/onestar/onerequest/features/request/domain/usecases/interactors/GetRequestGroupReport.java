package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.GeneralReportData;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.ReportService;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;

@Component
public class GetRequestGroupReport implements UseCase<GeneralReportData, Long> {

    private final ReportService reportService;

    public GetRequestGroupReport(ReportService reportService) {
        this.reportService = reportService;
    }

    @RolesAllowed({"admin"})
    @Override
    public Either<Exception, GeneralReportData> call(Long in) {
        return reportService.getRequestGroupReport(in);
    }
}
