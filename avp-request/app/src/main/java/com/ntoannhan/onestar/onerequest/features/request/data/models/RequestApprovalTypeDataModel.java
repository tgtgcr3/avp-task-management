package com.ntoannhan.onestar.onerequest.features.request.data.models;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestApprovalTypeEntity;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestApprovalType;

public class RequestApprovalTypeDataModel extends RequestApprovalType {

    public static RequestApprovalType fromEntity(RequestApprovalTypeEntity entity) {
        return new RequestApprovalType()
                .id(entity.getId())
                .name(entity.getName());
    }

}
