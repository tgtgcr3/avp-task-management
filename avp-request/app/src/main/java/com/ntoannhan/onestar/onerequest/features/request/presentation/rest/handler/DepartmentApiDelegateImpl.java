package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.handler;

import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors.GetAllDepartments;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model.AllDepartmentDataResult;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.rest.api.DepartmentApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.DepartmentGetAllResult;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class DepartmentApiDelegateImpl implements DepartmentApiDelegate {

    @Autowired
    private GetAllDepartments getAllDepartments;

    @Override
    public ResponseEntity<DepartmentGetAllResult> getAllDepartments(Boolean includeMembers) {
        return getAllDepartments.call(includeMembers).fold(ex -> {throw ExceptionTranslator.getFailureException(ex);},
                out -> ResponseEntity.ok(new AllDepartmentDataResult().fromUserCaseDataOut(out)));
    }
}
