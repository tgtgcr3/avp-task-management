package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetRequestGroupAttachmentFileInfoIns;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetRequestGroupAttachmentFileInfosOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetRequestGroupAttachmentFileInfos implements UseCase<GetRequestGroupAttachmentFileInfosOut, GetRequestGroupAttachmentFileInfoIns> {

    private final RequestGroupService requestGroupService;

    @Autowired
    public GetRequestGroupAttachmentFileInfos(RequestGroupService requestGroupService) {
        this.requestGroupService = requestGroupService;
    }

    @Override
    public Either<Exception, GetRequestGroupAttachmentFileInfosOut> call(GetRequestGroupAttachmentFileInfoIns in) {
        return requestGroupService.getAttachmentFileInfos(in.groupId(), in.fileIds()).fold(Either::left, out -> Either.right(new GetRequestGroupAttachmentFileInfosOut().fileInfos(out)));
    }
}
