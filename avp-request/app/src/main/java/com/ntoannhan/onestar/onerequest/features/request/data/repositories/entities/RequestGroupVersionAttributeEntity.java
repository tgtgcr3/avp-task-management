package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
//@Entity(name = "RequestGroupVersionAttribute")
public class RequestGroupVersionAttributeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @OneToOne
    private RequestGroupVersionEntity requestGroupVersion;

    @OneToOne
    private AttributeEntity attr;

}
