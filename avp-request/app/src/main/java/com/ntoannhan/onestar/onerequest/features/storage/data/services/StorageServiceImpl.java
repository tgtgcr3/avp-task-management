package com.ntoannhan.onestar.onerequest.features.storage.data.services;

import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.features.storage.data.model.FileModel;
import com.ntoannhan.onestar.onerequest.features.storage.data.repositories.entities.FileInfoEntity;
import com.ntoannhan.onestar.onerequest.features.storage.data.repositories.jpa.FileInfoJpa;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfoVisibility;
import com.ntoannhan.onestar.onerequest.features.storage.domain.service.StorageService;
import com.ntoannhan.onestar.onerequest.interfaces.config.FileStorageProperties;
import io.vavr.control.Either;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service("InternalStorageService")
public class StorageServiceImpl implements StorageService {

    private FileInfoJpa fileInfoJpa;

    private final FileStorageProperties fileStorageProperties;

    private final Path baseStorageLocation;

    @Autowired
    public StorageServiceImpl(FileInfoJpa fileInfoJpa, FileStorageProperties fileStorageProperties) {
        this.fileInfoJpa = fileInfoJpa;
        this.fileStorageProperties = fileStorageProperties;
        this.baseStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
    }

    @Override
    public Either<Exception, FileInfo> save(InputStream inputStream, String originalFileName, FileInfoVisibility visibility) {
        return storeFile(inputStream, originalFileName).fold(Either::left,
                fileDto -> {
                FileInfoEntity fileInfo = new FileInfoEntity();
                fileInfo.setCreatedDate(System.currentTimeMillis());
                fileInfo.setFileName(fileDto.fileName());
                fileInfo.setPath(fileDto.path());
                fileInfo.setSize(fileDto.size());
                fileInfo.setUploader(TenantContext.getUsername());
                fileInfo.setVisibility(visibility.getValue());
                try {
                    fileInfo = fileInfoJpa.save(fileInfo);
                    return Either.right(convertEntityToDomain(fileInfo));
                } catch (Exception ex) {
                    log.error("Cannot store fileInfo to database", ex);
                    return Either.left(new AppException("cannot_store_file_info_to_database"));
                }
            });
    }

    @Override
    public Either<Exception, FileModel> loadFile(long fileId, FileInfoVisibility visibility) {
        try {
            Optional<FileInfoEntity> fileInfoOpt = fileInfoJpa.findById(fileId, visibility.getValue());
            if (fileInfoOpt.isPresent()) {
                FileInfoEntity fileInfo = fileInfoOpt.get();
                Path pathToFile = baseStorageLocation.resolve(fileInfo.getPath());
                Resource resource = new UrlResource(pathToFile.toUri());
                if (resource.exists()) {
                    FileModel fileModel = new FileModel();
                    fileModel.setFileName(fileInfo.getFileName());
                    fileModel.setResource(resource);
                    return Either.right(fileModel);
                }
                log.error("file for " + fileId + " not exist");
                return Either.left(new AppException("file_not_exist"));
            }
            return Either.left(new UserException("file_not_found"));
        } catch (Exception ex) {
            log.error("Cannot retrieve file for " + fileId, ex);
            return Either.left(new AppException("cannot_retrieve_file"));
        }
    }

    @Override
    public Either<Exception, List<FileInfo>> getFileInfos(List<Long> fileIds) {
        try {
            List<FileInfoEntity> fileInfos = fileInfoJpa.findAllById(fileIds);
            return Either.right(fileInfos.stream().map(this::convertEntityToDomain).collect(Collectors.toList()));
        } catch (Exception ex) {
            log.error("Cannot retrieve file infos");
            return Either.left(new AppException("canno_retrive_file_info"));
        }
    }


    private FileInfo convertEntityToDomain(FileInfoEntity fileInfoEntity) {
        FileInfo fileInfo = OneMapper.mapTo(fileInfoEntity, FileInfo.class);
        fileInfo.setVisibility(FileInfoVisibility.fromId(fileInfoEntity.getVisibility()));
        return fileInfo;
    }


    private Either<Exception, ServiceFileDto> storeFile(InputStream inputStream, String originalFileName) {
        String fileName = StringUtils.cleanPath(originalFileName);

        try {
            if (fileName.contains("..")) {
                log.info("File name is not valid. It contains ..: " + fileName);
                return Either.left(new UserException("file_name_not_valid"));
            }

            String fileExt = FilenameUtils.getExtension(fileName);
            if (fileExt.equals(fileName)) {
                log.info("Cannot determine file ex of " + fileName);
                return Either.left(new UserException("cannot_determine_file_ext"));
            }

            String storageName = UUID.randomUUID().toString() + "." + fileExt;
            Date today = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String folder = dateFormat.format(today);

            String subPath = folder + File.separator + storageName;
            Path target = baseStorageLocation.resolve(subPath);
            if (!target.getParent().toFile().mkdirs()) {
                log.error("Cannot create target folder at " + target.getParent().toAbsolutePath().toString());
                //return Either.left(new AppException("cannot_create_target_folder"));
            }

            long fileSize = Files.copy(inputStream, target, StandardCopyOption.REPLACE_EXISTING);
            return Either.right(new ServiceFileDto()
                    .path(subPath)
                    .fileName(originalFileName)
                    .size(fileSize)
            );

        } catch (IOException ex) {
            log.error("Cannot store file " + fileName, ex);
        } catch (Exception ex) {
            log.error("error when store file", ex);
        }
        return Either.left(new AppException("cannot_storage_file"));
    }

    @Data
    @Accessors(fluent = true)
    static
    class ServiceFileDto {
        private String path;

        private String fileName;

        private long size;
    }
}
