package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model;

import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetAllAttributeTypesOut;
import com.ntoannhan.onestar.onerequest.rest.model.AttributeType;
import com.ntoannhan.onestar.onerequest.rest.model.AttributeTypeGetAllResult;

import java.util.stream.Collectors;

public class AllAttributeTypeDataResult extends AttributeTypeGetAllResult {

    public AllAttributeTypeDataResult fromUseCaseOut(GetAllAttributeTypesOut out) {
        this.total(out.total());
        this.items(out.items().stream().map(o -> new AttributeType().id(o.id()).name(o.name())).collect(Collectors.toList()));
        return this;
    }

}
