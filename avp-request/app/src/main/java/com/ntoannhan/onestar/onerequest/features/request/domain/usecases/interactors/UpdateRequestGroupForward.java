package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupForwardService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.UpdateRequestGroupForwardIn;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class UpdateRequestGroupForward implements UseCase<RequestGroupForward, UpdateRequestGroupForwardIn> {

    private final RequestGroupForwardService requestGroupForwardService;

    public UpdateRequestGroupForward(RequestGroupForwardService requestGroupForwardService) {
        this.requestGroupForwardService = requestGroupForwardService;
    }


    @Override
    public Either<Exception, RequestGroupForward> call(UpdateRequestGroupForwardIn in) {
        return requestGroupForwardService.updateGroupForward(in.requestId(), in.forwardId(), in.requestGroupForward());
    }
}
