package com.ntoannhan.onestar.onerequest.core.usecases.out;

import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;

public interface UseCaseDataOut <E, O>{

    O fromDomainEntity(E e);

}
