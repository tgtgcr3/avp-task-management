package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class RequestApprovalType {

    private Long id;

    private String name;

}
