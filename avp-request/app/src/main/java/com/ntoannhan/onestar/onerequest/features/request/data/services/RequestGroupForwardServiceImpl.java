package com.ntoannhan.onestar.onerequest.features.request.data.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.*;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestGroupForwardJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestGroupJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestGroupVersionJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.services.formatter.AttributeValueWrapper;
import com.ntoannhan.onestar.onerequest.features.request.data.services.formatter.SelectAdditionData;
import com.ntoannhan.onestar.onerequest.features.request.data.services.formatter.SelectKeyValue;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestApprovalService;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupForwardService;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import net.gcardone.junidecode.Junidecode;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Component
public class RequestGroupForwardServiceImpl implements RequestGroupForwardService {

    private final RequestGroupForwardJpa requestGroupForwardJpa;

    private final RequestGroupJpa requestGroupJpa;

    private final RequestJpa requestJpa;

    private final RequestGroupVersionJpa requestGroupVersionJpa;

    private final AttributeServiceImpl attributeService;

    private final RequestService requestService;

    private final RequestApprovalService requestApprovalService;

    public RequestGroupForwardServiceImpl(RequestGroupForwardJpa requestGroupForwardJpa, RequestGroupJpa requestGroupJpa, RequestJpa requestJpa, RequestGroupVersionJpa requestGroupVersionJpa, AttributeServiceImpl attributeService, RequestService requestService, RequestApprovalService requestApprovalService) {
        this.requestGroupForwardJpa = requestGroupForwardJpa;
        this.requestGroupJpa = requestGroupJpa;
        this.requestJpa = requestJpa;
        this.requestGroupVersionJpa = requestGroupVersionJpa;
        this.attributeService = attributeService;
        this.requestService = requestService;
        this.requestApprovalService = requestApprovalService;
    }

    @Override
    public Either<Exception, RequestGroupForward> createGroupForward(long requestId, RequestGroupForward requestGroupForward) {
        try {

            List<RequestGroupEntity> requestGroups = requestGroupJpa.findAllIds(Arrays.asList(requestId, requestGroupForward.getTargetId()));
            if (requestGroups.size() < 2) {
                return Either.left(new UserException("group_not_found"));
            }
            RequestGroupEntity sourceGroup = requestGroups.get(0).getId() == requestId ? requestGroups.get(0) : requestGroups.get(1);
            RequestGroupEntity targetGroup = requestGroups.get(0).getId() == requestId ? requestGroups.get(1) : requestGroups.get(0);
            RequestGroupForwardEntity entity = new RequestGroupForwardEntity();
            entity.setRequestGroup(sourceGroup);
            entity.setTargetGroup(targetGroup);
            entity.setStatus(requestGroupForward.getRequestStatus().getValue());
            RequestGroupForwardEntity savedEntity = requestGroupForwardJpa.save(entity);
            return Either.right(convertToDomain(savedEntity));

        } catch (Exception ex) {
            log.error("cannot createGroupForward", ex);
        }
        return Either.left(new AppException("cannot_save"));
    }

    @Override
    public Either<Exception, RequestGroupForward> updateGroupForward(long requestId, long id, RequestGroupForward requestGroupForward) {
        try {
            Optional<RequestGroupForwardEntity> forwardOpt = requestGroupForwardJpa.findById(requestId, id);
            if (forwardOpt.isPresent()) {
                RequestGroupForwardEntity entity = forwardOpt.get();
                Optional<RequestGroupEntity> targetGroupOpt = requestGroupJpa.findById(requestGroupForward.getTargetId());
                if (targetGroupOpt.isPresent()) {
                    entity.setTargetGroup(targetGroupOpt.get());
                    entity.setStatus(requestGroupForward.getRequestStatus().getValue());
                    RequestGroupForwardEntity saved = requestGroupForwardJpa.save(entity);
                    return Either.right(convertToDomain(saved));
                }
                return Either.left(new UserException("target_group_not_found"));
            }
            return Either.left(new UserException("request_group_forward_not_found"));
        } catch (Exception ex) {
            log.error("Cannot updateGroupForward", ex);
        }
        return Either.left(new AppException("cannot_update_group_forward"));
    }

    @Override
    public Either<Exception, Boolean> deleteGroupForward(long requestId, long id) {
        try {
            Optional<RequestGroupForwardEntity> forwardOpt = requestGroupForwardJpa.findById(requestId, id);
            if (forwardOpt.isPresent()) {
                requestGroupForwardJpa.deleteById(id);
                return Either.right(Boolean.TRUE);
            }
            return Either.left(new UserException("request_group_forward_not_found"));
        } catch (Exception ex) {
            log.error("Cannot deleteGroupForward", ex);
        }
        return Either.left(new AppException("cannot_delete_group_forward"));
    }

    @Override
    public Either<Exception, List<RequestGroupForward>> getAll(long requestId) {
        try {
            List<RequestGroupForwardEntity> forwards = requestGroupForwardJpa.findAllByRequest(requestId);
            return Either.right(forwards.stream().map(this::convertToDomain).collect(Collectors.toList()));
        } catch (Exception ex) {
            log.error("Cannot getAllGroupForward", ex);
        }
        return Either.left(new AppException("cannot_get_all_group_forward"));
    }

    @Override
    public Either<Exception, Boolean> transfer(long requestId, long targetId) {
        try {
            Optional<RequestEntity> sourceResourceOpt = requestJpa.findById(requestId);
            if (sourceResourceOpt.isPresent()) {
                Long sourceGroupId = sourceResourceOpt.get().getRequestGroupVersionEntity().getRequestGroup().getId();
                Optional<RequestGroupForwardEntity> groupForwardEntityOpt = requestGroupForwardJpa.findBySourceAndTarget(sourceGroupId, targetId);
                if (groupForwardEntityOpt.isPresent()) {
                    return transferRequestToGroup(sourceResourceOpt.get(), targetId);
                }
                return Either.left(new UserException("cannot_transfer_request"));
            }
            return Either.left(new UserException("request_not_found"));
        } catch (Exception ex) {
            log.error("Cannot transfer request from request {} to group {}", requestId, targetId, ex);
        }
        return Either.left(new AppException("cannot_transfer_request"));

    }

    private Either<Exception, Boolean> transferRequestToGroup(RequestEntity request, long targetId) {
        try {
            List<RequestGroupVersionEntity> requestGroupVersionEntities = requestGroupVersionJpa.getTopRequestGroupVersion(targetId, PageRequest.of(0, 1));
            if (requestGroupVersionEntities.size() > 0) {
                RequestGroupVersionEntity requestGroup = requestGroupVersionEntities.get(0);
                String value = request.getValue();
                ObjectMapper objectMapper = new ObjectMapper();

                TypeReference<HashMap<String, AttributeValueWrapper>> typeRef = new TypeReference<HashMap<String, AttributeValueWrapper>>() {
                };

                HashMap<String, AttributeValueWrapper> map = objectMapper.readValue(value, typeRef);
                List<AttributeEntity> attrs = requestGroup.getAttrs();
                List<AttributeEntity> collect = attrs.stream().filter(a -> a.getRequired() == 1).collect(Collectors.toList());
                List<String> missingRequired = new ArrayList<>();
                for (AttributeEntity attr : collect) {
                    if (!map.containsKey(attr.getFieldName())) {
                        missingRequired.add(attr.getName());
                    }
                }

                if (missingRequired.size() > 0) {
                    return Either.left(new UserException("missing_required_attributes: " + String.join(",", missingRequired)));
                }

                HashMap<String, AttributeValueWrapper> newValues = new HashMap<>();
                return attributeService.convertToDomain(attrs).fold(Either::left, out -> {
                    out.forEach(attr -> {
                        String fieldName = attr.getFieldName();
                        AttributeValueWrapper attrWrapper = map.get(fieldName);
                        if (attrWrapper != null) {
                            AttributeValueWrapper newWrapper = convertToTargetAttribute(attr, attrWrapper);
                            newValues.put(attr.fieldName(), newWrapper);
                        } else {
                            AttributeValueWrapper newWrapper = new AttributeValueWrapper();
                            newWrapper.attr(attr);
                            newWrapper.setValue("");
                            newValues.put(attr.fieldName(), newWrapper);
                        }
                    });
                    try {
                        request.setValue(objectMapper.writeValueAsString(newValues));
                    } catch (Exception ex) {
                        log.error("cannot write value as string", ex);
                        return Either.left(new AppException("cannot_convert_request_value"));
                    }
                    Long now = System.currentTimeMillis();
                    request.setSubmitTime(System.currentTimeMillis());
                    if (requestGroup.getSla() > 0) {
                        request.setTimeout(requestService.calculateRemainingTime(now, requestGroup.getSla()));
                    } else {
                        request.setTimeout(0L);
                    }
                    request.setOldRequestGroup(request.getRequestGroupVersionEntity().getRequestGroup());
                    request.setRequestGroupVersionEntity(requestGroup);
                    requestJpa.save(request);
                    return requestApprovalService.addStatus(request.getId(), RequestStatus.TRANSFER);
                });

                //return Either.left(new UserException("cannot_transfer"));

            }
        } catch (Exception ex) {
            log.error("Cannot transfer Request To New Group", ex);
        }
        return Either.left(new AppException("cannot_transfer_request"));
    }

    private AttributeValueWrapper convertToTargetAttribute(Attribute attr, AttributeValueWrapper attrWrapper) {
        AttributeValueWrapper newWrapper = new AttributeValueWrapper();
        newWrapper.setAttr(attr);
        Object currentValue = attrWrapper.value();
        Object newValue = null;
        try {
            switch ((int) attrWrapper.getAttr().attrType().longValue()) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 9:
                    newValue = currentValue;
                    break;
                case 6:
                    newValue = getSelectValue(currentValue, attrWrapper.getAttr(), attr);
                    break;
                case 7:
                    newValue = getMultiSelectValue(currentValue, attrWrapper.getAttr(), attr);
                    break;
                case 8:
                    newValue = getTableValue(currentValue, attrWrapper.getAttr(), attr);
                    break;
            }
        } catch (Exception ex) {
            log.error("cannot calculate value", ex);
        }
        newWrapper.setValue(newValue);

        return newWrapper;
    }

    private Object getTableValue(Object currentValue, Attribute currentAttr, Attribute newAttr) throws JsonProcessingException {
        if (currentValue instanceof List) {
            List<Map<String, String>> values = (List<Map<String, String>>) currentValue;
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                SelectAdditionData additionData = objectMapper.readValue(newAttr.additionData(), SelectAdditionData.class);
                List<SelectKeyValue> addDataValues = additionData.getValues();
                addDataValues = addDataValues.stream().map(v -> {
                    v.setName(Junidecode.unidecode(v.getName().replaceAll(" ", "_")));
                    return v;
                }).collect(Collectors.toList());
                List<SelectKeyValue> finalAddDataValues = addDataValues;
                return values.stream().map(v -> {
                    Map<String, String> newVal = new LinkedHashMap<>();
                    finalAddDataValues.forEach(kv -> {
                        String key = kv.getName();
                        String val = v.getOrDefault(key, "");
                        newVal.put(key, val);
                    });
                    return newVal;
                }).collect(Collectors.toList());

            } catch (JsonProcessingException ex) {
                log.error("cannot read additionVal of targetGroup", ex);
                throw ex;
            }
        }
        return null;
    }

    private Object getSelectValue(Object currentValue, Attribute currentAttr, Attribute newAttr) throws JsonProcessingException {
        try {
            String valueString = null;
            if (currentAttr.additionData() != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                SelectAdditionData additionData = objectMapper.readValue(currentAttr.additionData(), SelectAdditionData.class);
//                if (currentValue == null) {
//                    currentValue = additionData.getDefaultValue();
//                }
                if (currentValue != null) {
                    Integer values = (Integer) currentValue;
                    for (SelectKeyValue keyValue : additionData.getValues()) {
                        if (Objects.equals(keyValue.getValue(), values)) {
                            valueString = keyValue.getName();
                            break;
                        }
                    }
                }

                if (newAttr.additionData() != null) {
                    SelectAdditionData newAdditionalData = objectMapper.readValue(newAttr.additionData(), SelectAdditionData.class);
                    List<SelectKeyValue> newAddValues = newAdditionalData.getValues();
                    for (SelectKeyValue keyValue : newAddValues) {
                        if (Objects.equals(keyValue.getName(), valueString)) {
                            return keyValue.getValue();
                        }
                    }
                    return 0;
                }
            }
        } catch (Exception ex) {
            log.error("Cannont getMultiSelectValue for {} to {} from {}", currentAttr.getFieldName(), newAttr.getName(), currentValue, ex);
            throw ex;
        }
        return 0;

    }


    private Object getMultiSelectValue(Object currentValue, Attribute currentAttr, Attribute newAttr) throws JsonProcessingException {
        try {
            List<String> valueStrings = new LinkedList<>();
            if (currentAttr.additionData() != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                SelectAdditionData additionData = objectMapper.readValue(currentAttr.additionData(), SelectAdditionData.class);
                if (currentValue == null) {
                    return null;
                }
                if (currentValue instanceof List) {
                    List values = (List) currentValue;
                    valueStrings = (List<String>) values.stream().map(v -> {
                        for (SelectKeyValue keyValue : additionData.getValues()) {
                            if (Objects.equals(keyValue.getValue(), v)) {
                                return keyValue.getName();
                            }
                        }
                        return null;
                    }).filter(v -> v != null).collect(Collectors.toList());
                }
                List<Integer> newValues = new LinkedList<>();
                if (newAttr.additionData() != null) {
                    SelectAdditionData newAdditionalData = objectMapper.readValue(newAttr.additionData(), SelectAdditionData.class);
                    List<SelectKeyValue> newAddValues = newAdditionalData.getValues();
                    newValues = valueStrings.stream().map(str -> {
                        for (SelectKeyValue keyValue : newAddValues) {
                            if (Objects.equals(str, keyValue.getName())) {
                                return keyValue.getValue();
                            }
                        }
                        return null;
                    }).filter(Objects::nonNull).collect(Collectors.toList());
                    return newValues;
                }
            }
        } catch (Exception ex) {
            log.error("Cannont getMultiSelectValue for {} to {} from {}", currentAttr.getFieldName(), newAttr.getName(), currentValue, ex);
            throw ex;
        }
        return null;
    }

    private RequestGroupForward convertToDomain(RequestGroupForwardEntity entity) {
        RequestGroupForward groupForward = new RequestGroupForward();
        groupForward.setTargetId(entity.getTargetGroup().getId());
        groupForward.setTargetName(entity.getTargetGroup().getName());
        groupForward.setRequestStatus(RequestStatus.fromId(entity.getStatus()));
        groupForward.setId(entity.getId());
        return groupForward;
    }
}
