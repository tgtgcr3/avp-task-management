package com.ntoannhan.onestar.onerequest.features.request.domain.services;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupStatus;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import io.vavr.control.Either;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface RequestGroupService {

    Either<Exception, RequestGroup> createRequestGroup(RequestGroup requestGroup);

    Either<Exception, RequestGroup> updateRequestGroup(long id, RequestGroup requestGroup);

    Either<Exception, Boolean> deleteRequestGroup(long id);

    Either<Exception, RequestGroup> getRequestGroup(long id, boolean latest);

    Either<Exception, List<RequestGroup>> getAll(boolean fullData, RequestGroupStatus status);

    Either<Exception, Boolean> updateRequestGroupStatus(long id, RequestGroupStatus status);

    Either<Exception, Boolean> importFromFile(InputStream inputStream, String contentType);

    Either<Exception, List<FileInfo>> getAttachmentFileInfos(Long id, List<Long> fileIds);

    Either<Exception, Resource> downloadAttachment(long requestId, long fileId);
}
