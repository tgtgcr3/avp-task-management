package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class UpdateRequestGroupForwardIn {

    private long requestId;

    private long forwardId;

    private RequestGroupForward requestGroupForward;

}
