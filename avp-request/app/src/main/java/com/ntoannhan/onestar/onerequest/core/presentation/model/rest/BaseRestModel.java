package com.ntoannhan.onestar.onerequest.core.presentation.model.rest;

public interface BaseRestModel <Domain, Rest> {

    Rest toRestModel(Domain domain);

    Domain toDomainModel(Rest rest);

}
