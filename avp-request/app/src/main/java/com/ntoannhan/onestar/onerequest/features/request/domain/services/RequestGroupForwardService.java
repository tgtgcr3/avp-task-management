package com.ntoannhan.onestar.onerequest.features.request.domain.services;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward;
import io.vavr.control.Either;

import java.util.List;

public interface RequestGroupForwardService {

    Either<Exception, RequestGroupForward> createGroupForward(long requestId, RequestGroupForward requestGroupForward);

    Either<Exception, RequestGroupForward> updateGroupForward(long requestId, long id, RequestGroupForward requestGroupForward);

    Either<Exception, Boolean> deleteGroupForward(long requestId, long id);

    Either<Exception, List<RequestGroupForward>> getAll(long requestId);

    Either<Exception, Boolean> transfer(long requestId, long groupId);
}
