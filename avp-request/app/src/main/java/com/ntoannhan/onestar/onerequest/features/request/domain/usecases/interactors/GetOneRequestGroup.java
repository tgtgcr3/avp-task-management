package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetOneRequestGroupIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetOneRequestGroupOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetOneRequestGroup implements UseCase<GetOneRequestGroupOut, GetOneRequestGroupIn> {

    @Autowired
    private RequestGroupService requestGroupService;

    @Override
    public Either<Exception, GetOneRequestGroupOut> call(GetOneRequestGroupIn in) {
        if (in == null) {
            return Either.left(new UserException("request_group_id_empty"));
        }
        return requestGroupService.getRequestGroup(in.id(), in.latest()).fold(Either::left, requestGroup -> Either.right(new GetOneRequestGroupOut().fromDomainEntity(requestGroup)));
    }
}
