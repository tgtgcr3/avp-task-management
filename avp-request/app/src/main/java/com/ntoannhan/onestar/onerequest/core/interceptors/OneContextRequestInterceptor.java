package com.ntoannhan.onestar.onerequest.core.interceptors;

import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

import java.security.Principal;

@Log4j2
@Component
public class OneContextRequestInterceptor implements WebRequestInterceptor {

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void preHandle(WebRequest webRequest) throws Exception {
        String tenantId = webRequest.getHeader("X-TENANT-ID");
        log.debug("TenantID: " + tenantId);
        TenantContext.setTenantId(tenantId);
        Principal userPrincipal = webRequest.getUserPrincipal();
        TenantContext.setUsername(userPrincipal != null ? userPrincipal.getName() : null);

        OneMapper.setMapper(modelMapper);
    }

    @Override
    public void postHandle(WebRequest webRequest, ModelMap modelMap) throws Exception {
        TenantContext.clear();
    }

    @Override
    public void afterCompletion(WebRequest webRequest, Exception e) throws Exception {

    }
}
