package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public interface ArrayStringTypeWrapper {

    default String[] toStringArray(String value) {
        return value != null && value.length() > 1 ? value.substring(1, value.length() - 1).split(",") : new String[0];
    }

    default String fromStringArray(String[] value) {
        return value != null && value.length > 0 ? "," + String.join(",", value) + "," : "";
    }

    default String fromStringArray(List<String> value) {
        return value != null && value.size() > 0 ? "," + String.join(",", value) + "," : "";
    }

    default List<String> toListString(String value) {
        return value != null && value.length() > 1 ? Arrays.asList(value.substring(1, value.length() - 1).split(",")) : new LinkedList<>();
    }

}
