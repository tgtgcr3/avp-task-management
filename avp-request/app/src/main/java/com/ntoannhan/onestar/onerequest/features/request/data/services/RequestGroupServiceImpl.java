package com.ntoannhan.onestar.onerequest.features.request.data.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.core.utils.SecurityUtils;
import com.ntoannhan.onestar.onerequest.features.request.data.models.RequestGroupDataModel;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.*;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.*;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.*;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.CategoryService;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.DepartmentService;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupService;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfoVisibility;
import com.ntoannhan.onestar.onerequest.features.storage.presentation.service.impl.StorageService;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import net.gcardone.junidecode.Junidecode;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Component
public class RequestGroupServiceImpl implements RequestGroupService {

    private final CategoryService categoryService;

    private final CategoryJpa categoryJpa;

    private final RequestGroupJpa requestGroupJpa;

    private final RequestApprovalTypeJpa requestApprovalTypeJpa;

    private final RequestGroupVersionJpa requestGroupVersionJpa;

    private final AttributeJpa attributeJpa;

    private final AttributeTypeJpa attributeTypeJpa;

    private final DepartmentService departmentService;

    private final StorageService storageService;


    @Autowired
    public RequestGroupServiceImpl(
            CategoryService categoryService, CategoryJpa categoryJpa, RequestGroupJpa requestGroupJpa,
            RequestApprovalTypeJpa requestApprovalTypeJpa, RequestGroupVersionJpa requestGroupVersionJpa,
            AttributeJpa attributeJpa, AttributeTypeJpa attributeTypeJpa, DepartmentService departmentService, StorageService storageService) {

        this.categoryService = categoryService;
        this.categoryJpa = categoryJpa;
        this.requestGroupJpa = requestGroupJpa;
        this.requestApprovalTypeJpa = requestApprovalTypeJpa;
        this.requestGroupVersionJpa = requestGroupVersionJpa;
        this.attributeJpa = attributeJpa;
        this.attributeTypeJpa = attributeTypeJpa;
        this.departmentService = departmentService;
        this.storageService = storageService;
    }

    @Override
    public Either<Exception, RequestGroup> createRequestGroup(RequestGroup requestGroup) {
        return createOrUpdateRequestGroupEntity(requestGroup, null).fold(Either::left,
                requestGroupEntity -> createRequestGroupVersionEntity(requestGroup, requestGroupEntity).fold(Either::left,
                        requestGroupVersionEntity -> Either.right(new RequestGroupDataModel().fromEntity(requestGroupVersionEntity))));

    }

    private List<Attribute> convertAttributeEntityToAttribute(List<AttributeEntity> attrs) {
        return attrs.stream().map(ate ->
                new Attribute().id(ate.getId()).fieldName(ate.getFieldName()).name(ate.getName()).attrType(ate.getAttrType().getId())
                        .order(ate.getAttrOrder())
                        .description(ate.getDescription())
                        .required(ate.getRequired() > 0)
                        .additionData(ate.getAdditionData())).collect(Collectors.toList());
    }

    private Either<Exception, RequestGroupVersionEntity> createRequestGroupVersionEntity(RequestGroup requestGroup, RequestGroupEntity requestGroupEntity) {
        return getRequestApprovalEntity(requestGroup.approveType()).fold(Either::left, approvalTypeEntity ->
                getOrCreateAttrEntities(requestGroup.attrs()).fold(
                        Either::left,
                        attrEntities -> {
                            RequestGroupVersionEntity entity = new RequestGroupVersionEntity();
                            entity.oneSetFollowers(requestGroup.getFollowers());
                            entity.setRequestGroup(requestGroupEntity);
                            entity.setApprovalType(approvalTypeEntity);
                            entity.setSla(requestGroup.sla());
                            entity.setStatus(requestGroup.status().getValue());
                            entity.setRequestForm(requestGroup.requestForm());
                            entity.oneSetApprovers(requestGroup.getApprovers());
                            entity.oneSetRequestForms(requestGroup.requestForms());
                            entity.setAttrs(attrEntities);
                            entity.setApprovalOwner(requestGroup.getApprovalOwner());
                            try {
                                entity = requestGroupVersionJpa.save(entity);
                                requestGroup.attrs(convertAttributeEntityToAttribute(entity.getAttrs()));
                                return Either.right(entity);
                            } catch (Exception ex) {
                                log.error("Cannot create RequestGroupVersion", ex);
                                return Either.left(ex);
                            }
                        }
                )
        );
    }

    private Either<Exception, List<AttributeEntity>> getOrCreateAttrEntities(List<Attribute> attrs) {
        List<AttributeEntity> attrEntities = new ArrayList<>();
        if (attrs != null && !attrs.isEmpty()) {
            List<Attribute> tobeCreatedAttrs = attrs.stream().filter(attr -> attr.id() == null).collect(Collectors.toList());

            Either<Exception, List<AttributeEntity>> either = createAttributes(tobeCreatedAttrs);
            if (either.isLeft()) {
                return either;
            }
            attrEntities = either.get();

            try {
                attrEntities.addAll(attributeJpa.findAllById(attrs.stream().map(Attribute::id).filter(Objects::nonNull).collect(Collectors.toList())));
            }catch (Exception ex) {
                log.error("Cannot retrieve AttributeEntity", ex);
                return Either.left(new AppException(ex.getMessage()));
            }
        }
        return Either.right(attrEntities);
    }

    public Either<Exception, List<AttributeEntity>> createAttributes(List<Attribute> attrs) {
        return convertAttributeEntity(attrs).fold(Either::left,
                attrEntities -> {
                    try {
                        List<AttributeEntity> saved = attributeJpa.saveAll(attrEntities);
                        return Either.right(saved);
                    } catch (Exception ex) {
                        log.error("Cannot save data", ex);
                        return Either.left(new AppException(ex.getMessage()));
                    }
                }
        );
    }

    private Either<Exception, List<AttributeEntity>> convertAttributeEntity(List<Attribute> attrs) {
        List<Long> attrTypeIds = attrs.stream().map(Attribute::attrType).collect(Collectors.toList());
        log.debug("All attribute type " +
                attrTypeIds.stream().map(String::valueOf).collect(Collectors.joining(",")));
        List<AttributeTypeEntity> attrTypeEntities = attributeTypeJpa.findAllById(attrTypeIds);
        log.debug("Number of records: " + attrTypeEntities.size());
        return Either.right(attrs.stream().map(attr -> {
            AttributeEntity entity = new AttributeEntity();
            entity.setId(attr.id());
            entity.setAttrType(attrTypeEntities.stream().filter(at -> Objects.equals(at.getId(), attr.attrType())).findFirst().get());
            entity.setName(attr.name());
            entity.setAttrOrder(attr.order());
            entity.setFieldName(attr.fieldName());
            entity.setRequired(attr.isRequired() ? 1 : 0);
            entity.setAdditionData(attr.additionData());
            entity.setDescription(attr.description());
            if (entity.getFieldName() == null || entity.getFieldName().trim().length() == 0) {
                entity.setFieldName(Junidecode.unidecode(entity.getName().toLowerCase(Locale.ROOT).replaceAll(" ", "_")));
            }
            return entity;
        }).collect(Collectors.toList()));
    }

    private Either<Exception, RequestApprovalTypeEntity> getRequestApprovalEntity(Long id) {
        Optional<RequestApprovalTypeEntity> opt = requestApprovalTypeJpa.findById(id);
        return opt.<Either<Exception, RequestApprovalTypeEntity>>map(Either::right)
                .orElseGet(() -> Either.left(new Exception("Cannot find RequestApprovalType with id: " + id)));
    }

    private Either<Exception, RequestGroupEntity> createOrUpdateRequestGroupEntity(RequestGroup requestGroup, Long requestGroupId) {
        RequestGroupEntity requestGroupEntity = new RequestGroupEntity();
        if (requestGroupId != null) {
            Optional<RequestGroupEntity> opt = requestGroupJpa.findById(requestGroupId);
            if (opt.isEmpty()) {
                return Either.left(new UserException("request_group_not_exist"));
            }
            requestGroupEntity = opt.get();
        } else {
            requestGroupEntity.setCreator(TenantContext.getUsername());
        }
        String category = requestGroup.category();
        if (category != null) {
            Optional<CategoryEntity> categoryOpt = categoryJpa.findByName(category);
            if (categoryOpt.isPresent()) {
                log.debug("Found existing RequestGroup Category");
                requestGroupEntity.setCategory(categoryOpt.get());
            } else {
                log.debug("Category {} not found. Trying to create a new one", category);
                CategoryEntity categoryEntity = new CategoryEntity();
                categoryEntity.setName(category);
                try {
                    categoryEntity = categoryJpa.save(categoryEntity);
                } catch (Exception ex) {
                    log.error("Cannot create RequestGroup Category with name {}", category, ex);
                    return Either.left(ex);
                }
                requestGroupEntity.setCategory(categoryEntity);
            }
        }
        requestGroupEntity.setDescription(requestGroup.description());
        requestGroupEntity.setName(requestGroup.name());
        requestGroupEntity.setMgmtNotification(requestGroup.mgmtNotification());
        requestGroupEntity.setTargetEntityId(requestGroup.targetId());
        requestGroupEntity.setTargetEntityType(requestGroup.targetType());

        try {
            requestGroupEntity = requestGroupJpa.save(requestGroupEntity);
        } catch (Exception ex) {
            log.error("Cannot create RequestGroup", ex);
            return Either.left(ex);
        }
        return Either.right(requestGroupEntity);
    }

    @Transactional
    @Override
    public Either<Exception, RequestGroup> updateRequestGroup(long id, RequestGroup requestGroup) {
        Optional<RequestGroupVersionEntity> opt = requestGroupVersionJpa.findById(id);
        requestGroup.id(id);
        if (opt.isPresent()) {
            RequestGroupVersionEntity requestGroupVersionEntity = opt.get();
            if (isValidRequest(requestGroup, requestGroupVersionEntity)) {
                RequestGroupEntity requestGroupEntity = requestGroupVersionEntity.getRequestGroup();
                if (needToCreateANewVersion(requestGroup, requestGroupVersionEntity)) {
                    Either<Exception, RequestGroupVersionEntity> either = createRequestGroupVersionEntity(requestGroup, requestGroupEntity);
                    if (either.isLeft()) {
                        return Either.left(either.getLeft());
                    }
                    requestGroup.id(either.get().getId());
                }
                return createOrUpdateRequestGroupEntity(requestGroup, requestGroupEntity.getId()).fold(Either::left, r -> Either.right(requestGroup));
            }
            return Either.left(new UserException("request_group_version_less_than_current_version, current version: " + requestGroupVersionEntity.getId()));
        }

        return Either.left(new UserException("request_group_not_available"));
    }

    private boolean isValidRequest(RequestGroup requestGroup, RequestGroupVersionEntity requestGroupVersionEntity) {
        Long currentVersionId = requestGroupVersionJpa.getCurrentVersion(requestGroupVersionEntity.getRequestGroup().getId());
        log.debug("Current version {}, new version {}", currentVersionId, requestGroupVersionEntity.getId());
        return currentVersionId != null && currentVersionId.equals(requestGroupVersionEntity.getId());
    }

    private boolean needToCreateANewVersion(RequestGroup requestGroup, RequestGroupVersionEntity requestGroupEntity) {
        if (requestGroup.status().getValue() != requestGroupEntity.getStatus()) {
            return true;
        }
        if (requestGroupEntity.getSla() != null) {
            if (requestGroup.sla() == null || !requestGroupEntity.getSla().equals(requestGroup.sla())) {
                return true;
            }
        } else if (requestGroup.sla() != null) {
            return true;
        }

        if (requestGroupEntity.getApprovalType().getId().intValue() != requestGroup.approveType()) {
            return true;
        }

        if (requestGroupEntity.getApprovers() != null) {
            if (requestGroup.getApprovers() == null || requestGroup.getApprovers().size() != requestGroupEntity.oneGetApprovers().length) {
                return true;
            }
            if (Arrays.stream(requestGroupEntity.oneGetApprovers()).anyMatch(s -> requestGroup.getApprovers().contains(s))) {
                return true;
            }
        } else if (requestGroup.getApprovers() != null) {
            return true;
        }

        if (requestGroupEntity.getFollowers() != null) {
            if (requestGroup.followers() == null || requestGroup.followers().size() != requestGroupEntity.oneGetFollowers().length) {
                return true;
            }
            if (Arrays.stream(requestGroupEntity.oneGetFollowers()).anyMatch(s -> requestGroup.followers().contains(s))) {
                return true;
            }
        } else if (requestGroup.followers() != null) {
            return true;
        }

        if (requestGroupEntity.getApprovalOwner() != null) {
            if (!requestGroup.getApprovalOwner().equals(requestGroup.approvalOwner())) {
                return true;
            }
        } else if (requestGroup.getApprovalOwner() != null) {
            return true;
        }

        if (requestGroupEntity.getAttrs() != null) {
            if (requestGroup.attrs() == null) {
                return true;
            }
            if (requestGroupEntity.getAttrs().size() != requestGroup.attrs().size()) {
                return true;
            }
            List<AttributeEntity> currentAttrs = requestGroupEntity.getAttrs().stream().filter(at -> isAttrExstIn(at, requestGroup.attrs())).collect(Collectors.toList());
            return currentAttrs.size() != requestGroupEntity.getAttrs().size();
        } else return requestGroup.attrs() != null;
    }

    private boolean isAttrExstIn(AttributeEntity at, List<Attribute> attrs) {
        for (Attribute attr: attrs) {
            if (Objects.equals(attr.id(), at.getId()) &&
                    Objects.equals(attr.name(), at.getName()) &&
                    Objects.equals(attr.fieldName(), at.getFieldName()) &&
                    Objects.equals(attr.attrType(), at.getAttrType()) &&
                    (at.getRequired() > 0)  == attr.required() &&
                    attr.order() == at.getAttrOrder()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Either<Exception, Boolean> deleteRequestGroup(long id) {
        return null;
    }

    @Override
    public Either<Exception, RequestGroup> getRequestGroup(long id, boolean latest) {
        try {
            Optional<RequestGroupVersionEntity> requestGroupEntityOptional = requestGroupVersionJpa.findById(id);
            RequestGroupVersionEntity requestGroupVersionEntity = null;
            if (requestGroupEntityOptional.isPresent()) {
                requestGroupVersionEntity = requestGroupEntityOptional.get();
            }
            if (latest && requestGroupVersionEntity != null) {
                requestGroupVersionEntity = requestGroupVersionJpa.getTopRequestGroupVersion(requestGroupEntityOptional.get().getRequestGroup().getId(),
                        PageRequest.of(0, 1)).get(0);
            }
            if (requestGroupVersionEntity == null) {
                return Either.left(new UserException("request_group_not_found"));
            } else {
                return Either.right(new RequestGroupDataModel().fromEntity(requestGroupVersionEntity));
            }

        } catch (Exception ex) {
            log.error("Cannot retrieve RequestGroupVersion with id: " + id, ex);
            return Either.left(new AppException("cannot_retrieve_request_group_version"));
        }
    }

    @Override
    public Either<Exception, List<RequestGroup>> getAll(boolean fullData, RequestGroupStatus status) {
        try {
            if (SecurityUtils.hasRole("owner")) {
                List<RequestGroupVersionEntity> requestGroupVersionEntities = requestGroupVersionJpa.getAllCurrentVersion(null);
                return convertRequestGroupVersionEntitiesToRequestGroup(requestGroupVersionEntities);
            }
             return departmentService.getAll(true).fold(Either::left,
                     departments -> {
                        String currentUser = TenantContext.getUsername();
                         List<Long> departmentIds = departments.stream().filter(d -> d.getMembers().contains(currentUser)).map(Department::id).distinct().collect(Collectors.toList());
                         List<RequestGroupVersionEntity> requestGroupVersionEntities = requestGroupVersionJpa.getAllCurrentVersion(departmentIds);
                         return convertRequestGroupVersionEntitiesToRequestGroup(requestGroupVersionEntities);

                     });
        } catch (Exception ex) {
            log.error("Cannot getAll RequestGroupVersion", ex);
            return Either.left(new AppException("cannot_load_request_group"));
        }
    }

    @Override
    public Either<Exception, Boolean> updateRequestGroupStatus(long id, RequestGroupStatus status) {
        Optional<RequestGroupVersionEntity> requestGroupVersionEntityOpt = requestGroupVersionJpa.findById(id);
        if (requestGroupVersionEntityOpt.isEmpty()) {
            return Either.left(new UserException("request_group_not_found"));
        }

        RequestGroupVersionEntity requestGroupVersionEntity = requestGroupVersionEntityOpt.get();
        requestGroupVersionEntity.setStatus(status.getValue());
        try {
            requestGroupVersionJpa.save(requestGroupVersionEntity);
            return Either.right(true);
        } catch (Exception ex) {
            log.error("Cannot update RequestGroup status", ex);
            return Either.left(new AppException("cannot_update_request_group_status"));
        }

    }

    private Either<Exception, List<RequestGroup>> convertRequestGroupVersionEntitiesToRequestGroup(List<RequestGroupVersionEntity> entities) {
        try {
            return Either.right(entities.stream().map(r -> new RequestGroupDataModel().fromEntity(r)
            ).collect(Collectors.toList()));
        } catch (Exception ex) {
            log.error("Cannot convert Entities to Domain", ex);
            return Either.left(new AppException("cannot_process_entities_to_domain"));
        }
    }

    @Override
    public Either<Exception, Boolean> importFromFile(InputStream inputStream, String contentType) {
        return departmentService.getAll(false).fold(Either::left, departments -> {
            try {
                List<AttributeTypeEntity> attributeTypes = attributeTypeJpa.findAll();

                XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
                XSSFSheet sheet = workbook.getSheetAt(0);

                XSSFRow row = sheet.getRow(0);
                if (row == null) {
                    return Either.left(new UserException("template_header_missing"));
                }
                if (!isHeaderRowValid(row)) {
                    return Either.left(new UserException("template_header_invalid"));
                }

                int count = 1;
                XSSFCell cell;
                List<RequestGroup> requestGroups = new ArrayList<>();
                int totalRow = sheet.getLastRowNum();
                while (count <= totalRow){
                    row = sheet.getRow(count ++);
                    if (row == null) {
                        continue;
                    }
                    cell = row.getCell(0);
                    if (cell == null || cell.getStringCellValue() == null || cell.getStringCellValue().trim().length() ==0) {
                        continue;
                    }

                    Boolean result = extractRequestGroupInfo(row, departments, attributeTypes).fold(ex -> {
                                log.error("cannot extract cell", ex);
                                return Boolean.FALSE;
                            },
                            requestGroups::add);
                    if (result == Boolean.FALSE) {
                        log.error("Cannot extract RequestGroup");
                        return Either.left(new UserException("template_not_valid"));
                    }

                }
                requestGroups.forEach(requestGroup -> {
                    createRequestGroup(requestGroup).fold(ex -> {
                        log.error("cannot Import requestgroup: " + requestGroup.getName(), ex);
                        return null;
                    }, result -> {
                        log.info("Import RequestGroup: " + requestGroup.getName() + " success");
                        return null;
                    });
                });
                return Either.right(Boolean.TRUE);
            } catch (Exception ex) {
                log.error("Cannot extract data from template", ex);
                return Either.left(new UserException("template_not_valid"));
            }
        });

    }

    @Override
    public Either<Exception, List<FileInfo>> getAttachmentFileInfos(Long id, List<Long> fileIds) {
        log.debug("try to download file for {} with {}", id, fileIds);
        Optional<RequestGroupVersionEntity> requestGroupOpt = requestGroupVersionJpa.findById(id);
        if (requestGroupOpt.isPresent()) {
            log.debug("Found request group");
            String[] fileInfoIds = requestGroupOpt.get().oneGetRequestForms();
            List<Long> groupFileInfos = Arrays.stream(fileInfoIds).map(Long::parseLong).filter(fileIds::contains).collect(Collectors.toList());
            log.debug("Number of files {} of group {}", groupFileInfos.size(), id);
            if (groupFileInfos.size() > 0) {
                return storageService.getFileInfos(groupFileInfos);
            }
        }
        return Either.right(Collections.emptyList());
    }

    @Override
    public Either<Exception, Resource> downloadAttachment(long requestGroupId, long fileId) {
        Optional<RequestGroupVersionEntity> requestGroupOpt = requestGroupVersionJpa.findById(requestGroupId);
        if (requestGroupOpt.isPresent()) {
            RequestGroupVersionEntity requestEntity = requestGroupOpt.get();
            //permission???
            String fileIdStr = String.valueOf(fileId);
            List<String> attachments = new ArrayList<>(Arrays.asList(requestEntity.oneGetRequestForms()));
            if (attachments.contains(fileIdStr)) {
                return storageService.loadFile(fileId, FileInfoVisibility.PRIVATE);
            }
            return Either.left(new UserException("attachment_not_found"));
        }
        log.error("Request not found", new Exception());
        return Either.left(new UserException("attachment_not_found"));
    }

    private boolean isHeaderRowValid(XSSFRow row) {
        return true;
    }

    private Either<Exception, RequestGroup> extractRequestGroupInfo(XSSFRow row, List<Department> departments, List<AttributeTypeEntity> attributeTypes) {
        try {
            RequestGroup requestGroup = new RequestGroup();

            int count = 0;
            XSSFCell cell = row.getCell(count++);
            requestGroup.name(cell.getStringCellValue());
            cell = row.getCell(count++);
            requestGroup.approvers(Arrays.stream(cell.getStringCellValue().split(",")).map(String::trim).filter(s -> !s.isEmpty()).collect(Collectors.toList()));
            cell = row.getCell(count++);
            if (cell != null) {
                requestGroup.followers(Arrays.stream(cell.getStringCellValue().split(",")).map(String::trim).filter(String::isEmpty).collect(Collectors.toList()));
            }

            cell = row.getCell(count++);
            if (cell != null) {
                requestGroup.setMgmtNotification(cell.getStringCellValue() != null && "yes".equalsIgnoreCase(cell.getStringCellValue().trim()) ? 1 : 0);
            }
            cell = row.getCell(count++);
            requestGroup.setApproveType((long) (cell.getStringCellValue() != null && "sequential".equalsIgnoreCase(cell.getStringCellValue().trim()) ? 2 : 1));
            cell = row.getCell(count++);
            if (cell != null) {
                requestGroup.category(cell.getStringCellValue());
            }

            cell = row.getCell(count++);
            if (cell != null) {
                requestGroup.description(cell.getStringCellValue());
            }

            //TODO: team
            cell = row.getCell(count++);
            if (cell != null) {
                String departmentName = cell.getStringCellValue();
                if (departmentName != null && departmentName.trim().length() > 0) {
                    departmentName = departmentName.trim();
                    String finalDepartmentName = departmentName;
                    requestGroup.setTargetId(departments.stream().filter(department -> department.name().equalsIgnoreCase(finalDepartmentName)).findFirst().orElseThrow().getId());
                }
            }

            //TODO: sla
            cell = row.getCell(count++);
            if (cell != null) {
                DataFormatter df = new DataFormatter();
                String sla = df.formatCellValue(cell);
                if (sla != null && sla.trim().length() > 0) {
                    int hour = Integer.parseInt(sla);
                    requestGroup.sla(hour * 3600000L);
                } else {
                    requestGroup.sla(0L);
                }
            }
            requestGroup.setStatus(RequestGroupStatus.ENABLED);

            extractAttributes(requestGroup, row, count, attributeTypes);

            return Either.right(requestGroup);
        } catch (Exception ex) {
            log.error("cannot extract request group info", ex);
            return Either.left(new UserException("file_not_valid"));
        }

    }

    private void extractAttributes(RequestGroup requestGroup, XSSFRow row, int count, List<AttributeTypeEntity> attributeTypes) {
        XSSFCell cell = null;
        String cellValue = null;
        List<Attribute> attrs = requestGroup.getAttrs();
        if (attrs == null) {
            attrs = new ArrayList<>();
            requestGroup.setAttrs(attrs);
        }
        int order = 0;
        while ((cell = row.getCell(count++)) != null) {
            cellValue = cell.getStringCellValue();
            if (cellValue != null && cellValue.trim().length() > 0) {
                cellValue = cellValue.trim();
                Attribute attribute = new Attribute();
                attribute.required(cellValue.endsWith("*"));
                int startIndex = cellValue.lastIndexOf('[');
                String attributeTypeName = "text";
                if (startIndex > 0) {
                    int endIndex = cellValue.lastIndexOf(']');
                    if (endIndex > 0) {
                        attributeTypeName = cellValue.substring(startIndex + 1, endIndex).trim();
                    }
                }
                String additionalData = null;
                if (attributeTypeName.contains(":")) {
                    String[] parts = attributeTypeName.split(":");
                    attributeTypeName = parts[0].trim();
                    additionalData = parseAdditionalData(parts[1].trim());
                }
                String finalAttributeTypeName = attributeTypeName;
                attribute.attrType(attributeTypes.stream().filter(type -> type.getName().equalsIgnoreCase(finalAttributeTypeName)).findAny().orElseThrow().getId());
                attribute.setName(cellValue.substring(0, startIndex > 0 ? startIndex : cellValue.length() - 1).trim());
                attribute.order(order ++);
                attribute.setAdditionData(additionalData);
                attrs.add(attribute);
            }
        }

    }

    private String parseAdditionalData(String addData) {
        String[] valueParts = addData.split(",");
        List<Map<String, Object>> valueList = new ArrayList<>();
        for (int i = 0; i < valueParts.length; i++) {
            valueList.add(Map.of(
                    "name", valueParts[i], "value", i + 1
            ));
        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(Map.of(
               "values", valueList, "default", 1
            ));
        } catch (JsonProcessingException e) {
            log.error("cannot parse addition data", e);
        }
        return null;
    }
}
