package com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.out;

import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.core.usecases.out.UseCaseDataOut;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import lombok.Data;
import org.modelmapper.ModelMapper;

@Data
public class UploadFileOut extends FileInfo implements UseCaseDataOut<FileInfo, UploadFileOut> {

    @Override
    public UploadFileOut fromDomainEntity(FileInfo fileInfo) {
        return OneMapper.mapTo(fileInfo, UploadFileOut.class);
    }
}
