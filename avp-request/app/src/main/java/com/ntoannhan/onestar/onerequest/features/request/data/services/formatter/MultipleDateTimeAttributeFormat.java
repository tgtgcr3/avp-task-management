package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import com.ntoannhan.onestar.onerequest.interfaces.config.AppConfiguration;
import com.ntoannhan.onestar.onerequest.interfaces.config.RequestConfiguration;
import lombok.AllArgsConstructor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.security.core.parameters.P;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@AllArgsConstructor
public class MultipleDateTimeAttributeFormat implements AttributeFormatter {

    private final AppConfiguration appConfiguration;

    @Override
    public void format(XWPFDocument document, XWPFParagraph paragraph, XWPFRun xwpfRun, Long requestId, Object value, Attribute attribute, Object extra) {
        String data = String.valueOf(value);
        String[] values = data.split("-");
        if (values.length == 2) {
            long startDate = Long.parseLong(values[0]);
            long endDate = Long.parseLong(values[1]);

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(startDate);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);

            long today = calendar.getTimeInMillis();

            RequestConfiguration requestConfig = appConfiguration.getRequest();
            long startTime = requestConfig.getMorningTime();
            long lunchTime = requestConfig.getLunchTime();
            long noonTime = requestConfig.getNoonTime();
            long endTime = requestConfig.getEndTime();


            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            List<Long> segments = new LinkedList<>();

            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                segments.add(startDate);
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                today = calendar.getTimeInMillis();
                startDate = today + startTime;
                if (endDate <= startDate) {
                    segments.add(endDate);
                } else {
                    segments.add(startDate);
                   // segments.add(startDate);
                }
            } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY && today + lunchTime <= startDate) {
                segments.add(startDate);
                calendar.add(Calendar.DAY_OF_MONTH, 2);
                today = calendar.getTimeInMillis();
                startDate = today + startTime;
                if (endDate <= startDate) {
                    segments.add(endDate);
                } else {
                    segments.add(startDate);
               //     segments.add(startDate);
                }
            }

            if (startDate >= today + endTime) {
                segments.add(startDate);
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                today = calendar.getTimeInMillis();
                startDate = today + startTime;
                if (endDate <= startDate) {
                    segments.add(endDate);
                } else {
                    segments.add(startDate);
                    segments.add(startDate);
                }
            } else if (startDate < today + startTime) {
                segments.add(startDate);
                startDate = today + startTime;
                if (endDate <= startDate) {
                    segments.add(endDate);
                } else {
                    segments.add(startDate);
                    segments.add(startDate);
                }
            } else if (today + lunchTime <= startDate && startDate <= today + noonTime) {
                segments.add(startDate);
                startDate = today + noonTime;
                if (endDate <= startDate) {
                    segments.add(endDate);
                } else {
                    segments.add(startDate);
                    segments.add(startDate);
                }
            } else {
                segments.add(startDate);
            }

            while (startDate < endDate) {
                if (startDate < today + lunchTime) {

                    if (endDate <= today + lunchTime) {
                        segments.add(endDate);
                        break;
                    }
                    segments.add(today + lunchTime);
                    startDate = today + noonTime;
                    if (endDate <= startDate) {
                        segments.add(today + lunchTime);
                        segments.add(endDate);
                        break;
                    }

                    if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                        calendar.add(Calendar.DAY_OF_MONTH, 2);
                        today = calendar.getTimeInMillis();
                        startDate = today + startTime;
                        if (endDate <= startDate) {
                            segments.add(segments.get(segments.size() - 1));
                            segments.add(endDate);
                            break;
                        }
                    }
                    segments.add(startDate);
                }

                if (startDate <= today + endTime) {
                    if (endDate <= today + endTime) {
                        segments.add(endDate);
                        break;
                    }
                    segments.add(today + endTime);
                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                    today = calendar.getTimeInMillis();
                    startDate = today + startTime;
                    if (endDate <= startDate) {
                        segments.add(segments.get(segments.size() - 1));
                        segments.add(endDate);
                        break;
                    }
                    segments.add(startDate);
                }

//                if (startDate > today + endDate) {
//                    calendar.add(Calendar.DAY_OF_MONTH, 1);
//                    today = calendar.getTimeInMillis();
//                    String timeStr = sdf.format(new Date(startDate)) + "-" + sdf.format(new Date(today + startTime));
//                    xwpfRun.setText(timeStr);
//                    xwpfRun.addBreak();
//                    startDate = today + startTime;
//                }
//
//                if (startDate == today + endDate) {
//                    calendar.add(Calendar.DAY_OF_MONTH, 1);
//                    today = calendar.getTimeInMillis();
//                    startDate = today + startTime;
//                }



//                xwpfRun.setText(arr[0], 0);
//                for (int i = 1; i < arr.length; i++) {
//                    xwpfRun.addBreak();
//                    xwpfRun.setText(arr[i]);
//                }
            }
            String timeStr = "\t" + sdf.format(new Date(segments.get(0))) + "\t-\t" + sdf.format(new Date(segments.get(1)));
            xwpfRun.setText(timeStr, 0);



            for (int i = 2; i < segments.size(); i = i + 2) {
                timeStr = "\t" + sdf.format(new Date(segments.get(i))) + "\t-\t" + sdf.format(new Date(segments.get(i+1)));
                xwpfRun.addBreak();
                xwpfRun.setText(timeStr);
            }
        }
    }

    @Override
    public String formatData(Long requestId, Object value, Attribute attribute, Object extra) throws Exception {
        String data = String.valueOf(value);
        String[] values = data.split("-");
        if (values.length == 2) {
            long startDate = Long.parseLong(values[0]);
            long endDate = Long.parseLong(values[1]);

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            String timeStr = sdf.format(new Date(startDate)) + " - " + sdf.format(new Date(endDate));
            return timeStr;
        }
        return "";
    }
}
