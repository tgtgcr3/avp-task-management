package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class UserDataId implements Serializable {

    private String username;

    private String target;

    private int dataType;

}
