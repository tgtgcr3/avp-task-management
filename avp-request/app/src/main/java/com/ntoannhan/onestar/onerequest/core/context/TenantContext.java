package com.ntoannhan.onestar.onerequest.core.context;

import com.ntoannhan.onestar.onerequest.core.data.OneContextModel;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class TenantContext {

    private static ThreadLocal<OneContextModel> currentData = new InheritableThreadLocal<>();

    public static String getTenantId() {
        return getContextModel().tenantId();
    }

    public static void setTenantId(String tenant) {
        getContextModel().tenantId(tenant);
    }

    public static void setUsername(String username) {
        getContextModel().username(username);
    }

    public static String getUsername() {
        return getContextModel().username();
    }

    public static String getDescription() {
        return getContextModel().description();
    }

    public static void setDescription(String description) {
        getContextModel().description(description);
    }

    private static OneContextModel getContextModel() {
        OneContextModel oneContextModel = currentData.get();
        if (oneContextModel == null) {
            oneContextModel = new OneContextModel();
            currentData.set(oneContextModel);
        }
        return oneContextModel;
    }



    public static void clear() {
        currentData.set(null);
    }



}
