package com.ntoannhan.onestar.onerequest.features.storage.presentation.service.impl;

import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfoVisibility;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in.DownloadFileIn;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in.GetFileInfosIn;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.interceptor.DownloadFile;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.interceptor.GetFileInfos;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StorageService {

    private final DownloadFile downloadFile;

    private final GetFileInfos getFileInfos;
    @Autowired
    public StorageService(DownloadFile downloadFile, GetFileInfos getFileInfos) {
        this.downloadFile = downloadFile;
        this.getFileInfos = getFileInfos;
    }

    public Either<Exception, Resource> loadFile(long fileId, FileInfoVisibility visibility) {
        return downloadFile.call(new DownloadFileIn().fileId(fileId).visibility(visibility)).fold(Either::left,
                out -> Either.right(out.resource()));
    }

    public Either<Exception, List<FileInfo>> getFileInfos(List<Long> fileIds) {
        return getFileInfos.call(new GetFileInfosIn().fileIds(fileIds)).fold(Either::left, out -> Either.right(out.fileInfos()));
    }



}
