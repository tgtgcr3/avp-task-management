package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.ExportRequestDataToRequestFormIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.ExportRequestDataToRequestFormOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExportRequestDataToRequestForm implements UseCase<ExportRequestDataToRequestFormOut, ExportRequestDataToRequestFormIn> {

    private final RequestService requestService;

    public ExportRequestDataToRequestForm(RequestService requestService) {
        this.requestService = requestService;
    }

    @Override
    public Either<Exception, ExportRequestDataToRequestFormOut> call(ExportRequestDataToRequestFormIn in) {
        return requestService.exportRequestForm(in.requestId()).fold(Either::left, resource -> Either.right(new ExportRequestDataToRequestFormOut().resource(resource)));
    }
}
