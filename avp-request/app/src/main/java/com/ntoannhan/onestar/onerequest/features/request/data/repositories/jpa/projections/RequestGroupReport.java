package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections;

public interface RequestGroupReport {

    int getStatus();

    int getTotal();

}
