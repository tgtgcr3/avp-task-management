package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.AbstractBaseEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name="RequestForward")
@Audited
public class RequestForwardEntity extends AbstractBaseEntity implements ArrayStringTypeWrapper {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String owner;

    private Integer forwardType;

    private String targetUsers;

    private Long submitTime;

    @OneToOne
    private RequestEntity request;

    public List<String> oneGetTargetUsers() {
        return toListString(targetUsers);
    }

    public void onSetTargetUsers(List<String> users) {
        this.targetUsers = fromStringArray(users);
    }

    @Override
    public String toString() {
        return "RequestForwardEntity{" +
                "id=" + id +
                ", owner='" + owner + '\'' +
                ", forwardType=" + forwardType +
                ", targetUsers='" + targetUsers + '\'' +
                ", submitTime=" + submitTime +
                '}';
    }
}