package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Entity(name = "UserRequestData")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Audited
public class UserRequestDataEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String username;

    private Long requestId;

    @Column(columnDefinition = "int default 0")
    private Integer visitStatus;

}
