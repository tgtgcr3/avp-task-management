package com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.out;

import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class GetFileInfosOut {

    private List<FileInfo> fileInfos;

}
