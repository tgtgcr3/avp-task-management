package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestGroupVersionEntity;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections.RequestGroupReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface RequestGroupVersionJpa extends JpaRepository<RequestGroupVersionEntity, Long> {

    @Query("SELECT MAX(r.id) FROM RequestGroupVersion r WHERE r.requestGroup.id = :requestGroupId")
    Long getCurrentVersion(@Param("requestGroupId") Long id);

    @Query("SELECT r FROM RequestGroupVersion r WHERE r.requestGroup.id = :requestGroupId ORDER BY r.id DESC")
    List<RequestGroupVersionEntity> getTopRequestGroupVersion(@Param("requestGroupId") Long id, Pageable pageable);

    @Query("SELECT r FROM RequestGroupVersion r WHERE (r.requestGroup.targetEntityId = 0 OR (COALESCE(:departmentIds, CAST(NULL AS int)) IS NULL OR r.requestGroup.targetEntityId IN :departmentIds)) AND r.id IN (SELECT MAX(v.id) FROM RequestGroupVersion v GROUP BY v.requestGroup.id)")
    List<RequestGroupVersionEntity> getAllCurrentVersion(List<Long> departmentIds);

    @Query("SELECT r FROM RequestGroupVersion r WHERE r.id = :id")
    @Override
    Optional<RequestGroupVersionEntity> findById(Long id);

    @Query("SELECT r.status as status, COUNT(r) as total FROM Request r WHERE r.requestGroupVersionEntity.requestGroup.id = :requestGroupId GROUP BY r.status")
    List<RequestGroupReport> getRequestGroupReport(long requestGroupId);
}
