package com.ntoannhan.onestar.onerequest.features.discussion.domain.entities;

import lombok.Data;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.Column;
import java.util.List;

@Data
public class Discussion {

    private Long id;

    private String owner;

    private String content;

    private List<String> mentioned;

    private List<String> fileIds;

    private DiscussionStatus status;

    private long createdDate;

    private long lastUpdatedDate;

}
