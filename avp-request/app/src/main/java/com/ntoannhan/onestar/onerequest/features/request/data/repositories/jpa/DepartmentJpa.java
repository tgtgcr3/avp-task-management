package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.DepartmentEntity;
import com.ntoannhan.onestar.onerequest.interfaces.config.AppConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DepartmentJpa {

    private final RestTemplate restTemplate;

    private final AppConfiguration appConfiguration;

    public DepartmentJpa(RestTemplateBuilder restTemplateBuilder, AppConfiguration appConfiguration) {
        this.restTemplate = restTemplateBuilder.build();
        this.appConfiguration = appConfiguration;
    }

    @Cacheable("department-all-department")
    public DepartmentEntity[] getAllDepartments() {
        return restTemplate.getForEntity(appConfiguration.getTaskUrl() + "/avp/api/v3/departments", DepartmentEntity[].class).getBody();
    }

}
