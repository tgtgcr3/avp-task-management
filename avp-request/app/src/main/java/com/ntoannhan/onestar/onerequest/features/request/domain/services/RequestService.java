package com.ntoannhan.onestar.onerequest.features.request.domain.services;

import com.ntoannhan.onestar.onerequest.core.data.entities.ResultGetAllData;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestEntity;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Request;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestActivity;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestApproval;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import io.vavr.control.Either;
import org.springframework.core.io.Resource;

import java.util.List;
import java.util.Map;

public interface RequestService {

//    Either<Exception>
    Either<Exception, Request> createRequest(Request request);

    Either<Exception, Request> getRequest(long id);

    Either<Exception, Request> updateRequest(long id, Request request);

    Either<Exception, ResultGetAllData<Request>> getAll(String search, int skip, int size, boolean fullData, Long requestGroupId,
                                                        Boolean toMe, Boolean fromMe, Boolean followed, RequestStatus requestStatus, Boolean favorite,
                                                        List<String> creators, Long createdDate, Long end
                                                        );

    Either<Exception, Resource> downloadAttachment(long requestId, long fileId);

    Either<Exception, Resource> exportRequestForm(long requestId);

    Either<Exception, List<RequestActivity>> getRequestActivities(long id);

    Either<Exception, List<FileInfo>> getAttachmentFileInfos(long id, List<Long> fileIds);

    Either<Exception, Map<Long, RequestApproval>> getRequestApprovalInfos(List<Long> ids);

    Either<Exception, Boolean> setFavorite(long id, boolean added);

    Either<Exception, Request> updateFollowers(long id, List<String> followers, boolean tobeAdded);

    Either<Exception, Boolean> removeRequest(long id);

    List<String> getAllApprovers(RequestEntity entity);

    Either<Exception, Boolean> visitRequest(long id);

    long calculateRemainingTime(Long now, Long timeout);

    Either<Exception, Resource> exportAllRequest(String search, int i, int i1, Boolean fullData, Long requestGroupId, Boolean toMe, Boolean fromMe, Boolean followed, RequestStatus requestStatus, Boolean favorite, List<String> creators, Long createdDate, Long end);
}
