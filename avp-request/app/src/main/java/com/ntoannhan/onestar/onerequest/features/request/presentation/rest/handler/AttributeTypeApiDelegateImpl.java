package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.handler;

import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetAllAttributeTypesIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors.GetAllAttributeTypes;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model.AllAttributeTypeDataResult;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.rest.api.AttributeTypeApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.AttributeTypeGetAllResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class AttributeTypeApiDelegateImpl implements AttributeTypeApiDelegate {

    @Autowired
    private GetAllAttributeTypes getAllAttributeTypes;

    @Override
    public ResponseEntity<AttributeTypeGetAllResult> getAllAttributeTypes() {
        return getAllAttributeTypes.call(new GetAllAttributeTypesIn()).fold(
                ex -> {throw ExceptionTranslator.getFailureException(ex);},
                out -> ResponseEntity.ok(new AllAttributeTypeDataResult().fromUseCaseOut(out))
        );
    }
}
