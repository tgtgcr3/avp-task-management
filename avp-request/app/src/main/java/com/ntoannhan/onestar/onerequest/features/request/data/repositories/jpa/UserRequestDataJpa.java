package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.UserRequestDataEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserRequestDataJpa extends JpaRepository<UserRequestDataEntity, Long> {

    @Query("SELECT ur.requestId FROM UserRequestData ur WHERE ur.username = :username AND ur.requestId IN :requestIds")
    List<Long> getAllVisitedRequests(String username, List<Long> requestIds);

}
