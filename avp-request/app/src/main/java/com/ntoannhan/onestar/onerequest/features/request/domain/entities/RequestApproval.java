package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class RequestApproval {

    private Long requestId;

    private List<String> approveds;

    private List<String> rejecteds;

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public List<String> getApproveds() {
        return approveds;
    }

    public void setApproveds(List<String> approveds) {
        this.approveds = approveds;
    }

    public List<String> getRejecteds() {
        return rejecteds;
    }

    public void setRejecteds(List<String> rejecteds) {
        this.rejecteds = rejecteds;
    }
}
