package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;

@Component
public class RemoveRequest implements UseCase<Boolean, Long> {

    private final RequestService requestService;

    public RemoveRequest(RequestService requestService) {
        this.requestService = requestService;
    }

    @RolesAllowed({"owner"})
    @Override
    public Either<Exception, Boolean> call(Long in) {
        return requestService.removeRequest(in);
    }
}
