package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class UpdateRequestStatusIn {

    private long requestId;

    private String description;

    private RequestStatus requestStatus;

}
