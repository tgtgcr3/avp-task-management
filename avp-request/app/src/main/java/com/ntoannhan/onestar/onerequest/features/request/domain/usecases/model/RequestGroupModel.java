package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.model;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupStatus;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class RequestGroupModel {

    public static com.ntoannhan.onestar.onerequest.rest.model.RequestGroup toRequestGroup(RequestGroup out) {
        com.ntoannhan.onestar.onerequest.rest.model.RequestGroup requestGroup = new com.ntoannhan.onestar.onerequest.rest.model.RequestGroup();
        return requestGroup.id(out.id())
                .name(out.name())
                .sla(out.sla())
                .followers(out.followers())
                .approveType(out.approveType().intValue())
                .category(out.category())
                .description(out.description())
                .status(toRequestStatusEnum(out.status()))
                .requestForm(out.requestForm())
                .targetId(out.targetId())
                .targetType(out.targetType())
                .approvers(out.getApprovers())
                .attrs(out.attrs() != null ? out.attrs().stream().map(AttributeModel::toAttributeRest).collect(Collectors.toList()) : new ArrayList<>())
                .creator(out.creator())
                .oId(out.oId())
                .mgmtNotification(out.mgmtNotification());
    }

    private static com.ntoannhan.onestar.onerequest.rest.model.RequestGroup.StatusEnum toRequestStatusEnum(RequestGroupStatus status) {
        switch (status) {
            case DELETED:
                return com.ntoannhan.onestar.onerequest.rest.model.RequestGroup.StatusEnum.DELETED;
            case DISABLED:
                return com.ntoannhan.onestar.onerequest.rest.model.RequestGroup.StatusEnum.DISABLED;
            default:
                return com.ntoannhan.onestar.onerequest.rest.model.RequestGroup.StatusEnum.ENABLED;
        }
    }

}
