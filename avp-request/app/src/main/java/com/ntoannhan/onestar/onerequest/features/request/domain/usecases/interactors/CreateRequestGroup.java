package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupService;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;

@Component
public class CreateRequestGroup implements UseCase<RequestGroup, RequestGroup> {

    @Autowired
    private RequestGroupService requestGroupService;

    @RolesAllowed({"admin"})
    @Override
    public Either<Exception, RequestGroup> call(RequestGroup in) {
        return validate(in).fold(Either::left, v -> requestGroupService.createRequestGroup(in));
    }

    private Either<Exception, Void> validate(RequestGroup in) {
        if (in.name() == null || in.name().trim().length() == 0) {
            return Either.left(new UserException("name_is_empty"));
        }
        return Either.right(null);
    }
}
