package com.ntoannhan.onestar.onerequest.features.request.data.models;

import com.ntoannhan.onestar.onerequest.core.data.DataModel;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.AttributeTypeEntity;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.AttributeType;
import org.modelmapper.ModelMapper;

public class AttributeTypeDataModel extends AttributeType implements DataModel<AttributeTypeEntity, AttributeTypeDataModel> {
    @Override
    public AttributeTypeDataModel fromEntity(AttributeTypeEntity attributeTypeEntity) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(attributeTypeEntity, this);
        return this;
    }
}
