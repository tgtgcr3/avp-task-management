package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out;

import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@Data
@Accessors(fluent = true)
public class GetRequestGroupAttachmentFileInfosOut {

    private List<FileInfo> fileInfos;

}
