package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class MultipleLineStringAttributeFormat implements AttributeFormatter {

    @Override
    public void format(XWPFDocument document, XWPFParagraph paragraph, XWPFRun xwpfRun, Long requestId, Object value, Attribute attribute, Object extra) {
        String data = String.valueOf(value);
        String[] arr = data.split("\n");
        xwpfRun.setText(arr[0], 0);
        for (int i = 1; i < arr.length; i++) {
            xwpfRun.addBreak();
            xwpfRun.setText(arr[i]);
        }
    }

    @Override
    public boolean selfFormat() {
        return true;
    }

}
