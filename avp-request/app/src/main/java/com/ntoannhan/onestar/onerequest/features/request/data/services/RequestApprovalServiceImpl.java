package com.ntoannhan.onestar.onerequest.features.request.data.services;

import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.Discussion;
import com.ntoannhan.onestar.onerequest.features.discussion.presentation.services.DiscussionService;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestApprovalRecordEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestApprovalTypeEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestForwardEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestApprovalJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestForwardJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestJpa;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestApprovalService;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Component
public class RequestApprovalServiceImpl implements RequestApprovalService {

    private final RequestJpa requestJpa;

    private final RequestApprovalJpa requestApprovalJpa;

    private final RequestServiceImpl requestService;

    private final RequestForwardJpa requestForwardJpa;

    private static final long Parallel = 1;

    private static final long Sequence = 2;

    private static final long OneApprover = 3;

    private final DiscussionService discussionService;

    private static final String REJECT_MESSAGE_WITH_DESCRIPTION =
            "<div class=\"rj\"><div class=\"ra-msg\">Đã <span class=\"at\">Từ Chối</span> đề xuất với phản hồi</div><div class=\"ra-des\">%s</div></div>";

    private static final String REJECT_MESSAGE_WITHOUT =
            "<div class=\"rj\"><div class=\"ra-msg\">Đã <span class=\"at\">Từ Chối</span> đề xuất</div></div>";

    private static final String APPROVED_MESSAGE_WITH_DESCRIPTION =
            "<div class=\"ap\"><div class=\"ra-msg\">Đã <span class=\"at\">Chấp Nhận</span> đề xuất với phản hồi</div><div class=\"ra-des\">%s</div></div>";

    private static final String APPROVED_MESSAGE_WITHOUT =
            "<div class=\"ap\"><div class=\"ra-msg\">Đã <span class=\"at\">Chấp Nhận</span> đề xuất</div></div>";

    private static final String APPROVED_FULLY_MESSAGE_WITH_DESCRIPTION =
            "<div class=\"ap\">" +
                    "<div class=\"ra-msg\">Đã <span class=\"at\">Chấp Nhận</span> đề xuất với phản hồi</div><div class=\"ra-des\">%s</div>" +
                    "<div class=\"ra-fully-msg\">Đề xuất đã hoàn thành</div>" +
            "</div>";

    private static final String APPROVED_FULLY_MESSAGE_WITHOUT =
            "<div class=\"ap\">" +
                    "<div class=\"ra-msg\">Đã <span class=\"at\">Chấp Nhận</span> đề xuất</div>" +
                    "<div class=\"ra-fully-msg\">Đề xuất đã hoàn thành</div>" +
                    "</div>";

    private static final String CANCEL_MESSAGE_WITH_DESCRIPTION =
            "<div class=\"cl\"><div class=\"ra-msg\">Đã <span class=\"at\">Hủy</span> đề xuất với phản hồi</div><div class=\"ra-des\">%s</div></div>";

    private static final String CANCEL_MESSAGE_WITHOUT =
            "<div class=\"cl\"><div class=\"ra-msg\">Đã <span class=\"at\">Hủy</span> đề xuất</div></div>";

    private static final String FWD_MESSAGE_WITH_DESCRIPTION =
            "<div class=\"fwd\">" +
                "<div class=\"ra-msg\">Đã <span class=\"at\">Chuyển</span> đề xuất tới:</div>" +
                "<div class=\"target\">" +
                    "<ul>" +
                    "%s" +
                    "</ul>" +
                "</div>" +
                "<div class=\"ra-msg2\"> với phản hồi: </div>" +
                "<div class=\"ra-des\">%s</div>" +
            "</div>";

    private static final String FWD_MESSAGE_WITHOUT =
            "<div class=\"fwd\">" +
                    "<div class=\"ra-msg\">Đã <span class=\"at\">Chuyển</span> đề xuất tới:</div>" +
                    "<div class=\"target\">" +
                    "<ul>" +
                    "%s" +
                    "</ul>" +
                    "</div>" +
                    "</div>";

    private static final String FWD_APPROVED_MESSAGE_WITH_DESCRIPTION =
            "<div class=\"fwd-ap\">" +
                    "<div class=\"ra-msg\">Đã <span class=\"at\">Đồng Ý và Chuyển</span> đề xuất tới:</div>" +
                    "<div class=\"target\">" +
                    "<ul>" +
                    "%s" +
                    "</ul>" +
                    "</div>" +
                    "<div class=\"ra-msg2\"> với phản hồi: </div>" +
                    "<div class=\"ra-des\">%s</div>" +
                    "</div>";

    private static final String FWD_APPROVED_MESSAGE_WITHOUT =
            "<div class=\"fwd-ap\">" +
                    "<div class=\"ra-msg\">Đã <span class=\"at\">Đồng Ý và Chuyển</span> đề xuất tới:</div>" +
                    "<div class=\"target\">" +
                    "<ul>" +
                    "%s" +
                    "</ul>" +
                    "</div>" +
                    "</div>";


    @Autowired
    public RequestApprovalServiceImpl(RequestJpa requestJpa, RequestApprovalJpa requestApprovalJpa, RequestService requestService, RequestForwardJpa requestForwardJpa, DiscussionService discussionService) {
        this.requestJpa = requestJpa;
        this.requestApprovalJpa = requestApprovalJpa;
        this.requestService = (RequestServiceImpl) requestService;
        this.requestForwardJpa = requestForwardJpa;
        this.discussionService = discussionService;
    }

    private List<String> getAllApprovers(RequestEntity requestEntity) {
        return requestService.getAllApprovers(requestEntity);
    }

    @Override
    public Either<Exception, Boolean> evaluateRequest(long requestId, RequestStatus status, List<String> targetUsers, String description) {
        try {
            Optional<RequestEntity> requestEntityOptional = requestJpa.findById(requestId);
            if (requestEntityOptional.isPresent()) {
                RequestEntity requestEntity = requestEntityOptional.get();
                List<String> approvers = getAllApprovers(requestEntity);
                String currentUser = TenantContext.getUsername();
                if (status == RequestStatus.APPROVED || status == RequestStatus.MOVE || status == RequestStatus.REQUEST_UPDATE ||
                        status == RequestStatus.REJECTED || status == RequestStatus.CANCELLED || status == RequestStatus.AGREED_FORWARD || status == RequestStatus.FORWARD) {
                    if (currentUser.equals(requestEntity.getRequestGroupVersionEntity().getApprovalOwner())) {
                        return processRequest(status, targetUsers, description, requestEntity);
                    }
                    for (String approver : approvers) {
                        if (currentUser.equals(approver)) {
                            if (requestEntity.getRequestGroupVersionEntity().getApprovalType().getId() == Sequence) {
                                String nextApprover = getNextApprover(requestEntity);
                                if (!currentUser.equals(nextApprover) && !(currentUser.equals(requestEntity.getSubmitter()) && status == RequestStatus.CANCELLED)) {
                                    return Either.left(new UserException("must_wait_for: " + nextApprover));
                                }
                            }
                            return processRequest(status, targetUsers, description, requestEntity);
                        }
                    }
                    if (status == RequestStatus.CANCELLED && currentUser.equals(requestEntity.getSubmitter())) {
                        return processRequest(status, targetUsers, description, requestEntity);
                    }
                }
                return Either.left(new UserException(HttpStatus.SC_UNAUTHORIZED, "unauthorized"));
            }
            log.debug("Request not found with id: " + requestId);
            return Either.left(new UserException("request_not_found"));
        } catch (Exception ex) {
            log.error("Cannot process the request with id " + requestId, ex);
            return Either.left(new AppException("cannot_process_request"));
        }

    }

    @Override
    public Either<Exception, Boolean> addStatus(long requestId, RequestStatus status) {
        Optional<RequestEntity> requestEntityOptional = requestJpa.findById(requestId);
        if (requestEntityOptional.isPresent()) {
            RequestApprovalRecordEntity entity = new RequestApprovalRecordEntity();
            entity.setRequest(requestEntityOptional.get());
            entity.setApprover(TenantContext.getUsername());
            entity.setDecision(status.getValue());
            entity.setComment("");
            entity.setSubmittedTime(System.currentTimeMillis());
            requestApprovalJpa.save(entity);
            return Either.right(Boolean.TRUE);
        }
        return Either.left(new UserException("request_not_found"));
    }

    private Either<Exception, Boolean> processRequest(RequestStatus status, List<String> targetUsers, String description, RequestEntity requestEntity) {
        return storeData(requestEntity, status, targetUsers, description).fold(Either::left, result -> {
            if (status == RequestStatus.APPROVED) {
                requestService.sendApprovedByApproverNotification(requestEntity);
            }
            if (status == RequestStatus.REJECTED) {
                requestService.sendRejectedByApproverNotification(requestEntity);
            }
            if (status == RequestStatus.CANCELLED) {
                if (TenantContext.getUsername().equals(requestEntity.getSubmitter())) {
                    requestService.sendCancelledBySubmitter(requestEntity);
                } else {
                    requestService.sendCancelledByApprover(requestEntity);
                }
                Discussion discussion = new Discussion();
                discussion.setContent(description != null && description.length() > 0 ? String.format(CANCEL_MESSAGE_WITH_DESCRIPTION, description) : CANCEL_MESSAGE_WITHOUT);
                discussionService.createSystemDiscussion(1, requestEntity.getId(), discussion);

            }
            return updateRequestStatus(requestEntity, status, targetUsers, description);
        });
    }

    private String getNextApprover(RequestEntity requestEntity) {
        List<RequestApprovalRecordEntity> records = requestApprovalJpa.findAllByApprovedRequestId(requestEntity.getId(),
                PageRequest.of(0, 1, Sort.Direction.DESC, "id"));

        List<String> approvers = getAllApprovers(requestEntity);

        if (records.size() > 0) {
            String currentUser = records.get(0).getApprover();
            int index = approvers.indexOf(currentUser);
            if (index < approvers.size() - 1) {
                return approvers.get(index + 1);
            }

            log.error("aaaaaaaaaaaaaaaaaaaaaa - direct");
            //something wrong
            return approvers.get(0);
        }
        return approvers.size() > 0 ? approvers.get(0) : null;
    }

    private Either<Exception, Boolean> updateRequestStatus(RequestEntity requestEntity, RequestStatus status, List<String> targetUsers, String description) {
        if (status == RequestStatus.AGREED_FORWARD || status == RequestStatus.FORWARD) {
            RequestForwardEntity entity = new RequestForwardEntity();
            entity.setOwner(TenantContext.getUsername());
            entity.setSubmitTime(System.currentTimeMillis());
            entity.setForwardType(status.getValue());
            entity.onSetTargetUsers(targetUsers);
            entity.setRequest(requestEntity);
            requestForwardJpa.save(entity);

            StringBuilder sb = new StringBuilder();
            targetUsers.forEach(u -> sb.append("<li>[").append(u).append("]</li>"));
            if (status == RequestStatus.FORWARD) {
               // requestService.sendRejectedNotification(requestEntity);
                Discussion discussion = new Discussion();

                discussion.setContent(description != null && description.length() > 0 ? String.format(FWD_MESSAGE_WITH_DESCRIPTION, sb.toString(),description) : String.format(FWD_MESSAGE_WITHOUT, sb.toString()));
                discussionService.createSystemDiscussion(1, requestEntity.getId(), discussion);
            }
            if (status == RequestStatus.AGREED_FORWARD) {
                requestService.sendApprovedNotification(requestEntity);
                Discussion discussion = new Discussion();
                discussion.setContent(description != null && description.length() > 0 ? String.format(FWD_APPROVED_MESSAGE_WITH_DESCRIPTION, sb.toString(),description) : String.format(FWD_APPROVED_MESSAGE_WITHOUT, sb.toString()));
                discussionService.createSystemDiscussion(1, requestEntity.getId(), discussion);
            }

            requestService.sendFwdNotification(requestEntity, targetUsers, description);

            return Either.right(true);
        }
        RequestApprovalTypeEntity approvalType = requestEntity.getRequestGroupVersionEntity().getApprovalType();
        boolean shouldUpdateStatus = false;
        if (approvalType.getId() == OneApprover || status == RequestStatus.REJECTED || status == RequestStatus.CANCELLED
                || TenantContext.getUsername().equals(requestEntity.getRequestGroupVersionEntity().getApprovalOwner())
        ) {
            if (status == RequestStatus.APPROVED && approvalType.getId() == OneApprover) {
                List<String> approvers = new ArrayList<>(Arrays.asList(requestEntity.oneGetApprovers()));
                approvers.addAll(Arrays.asList(requestEntity.getRequestGroupVersionEntity().oneGetApprovers()));
                shouldUpdateStatus = approvers.contains(TenantContext.getUsername()) || TenantContext.getUsername().equals(requestEntity.getRequestGroupVersionEntity().getApprovalOwner());
            } else {
                shouldUpdateStatus = true;
            }
        } else {
            List<RequestApprovalRecordEntity> requestApproverRecords = requestApprovalJpa.findAllByRequestId(requestEntity.getId(),
                    PageRequest.of(0, 1024));
            List<String> approvedList = requestApproverRecords.stream().map(RequestApprovalRecordEntity::getApprover).collect(Collectors.toList());
            List<String> requestApprovers = new ArrayList<>();
            if (requestEntity.getRequestGroupVersionEntity().getId() == 0) {
                requestApprovers.addAll(Arrays.asList(requestEntity.oneGetApprovers()));
            } else {
                requestApprovers.addAll(Arrays.asList(requestEntity.getRequestGroupVersionEntity().oneGetApprovers()));
                requestApprovers.addAll(Arrays.asList(requestEntity.oneGetDirectMgmts()));
            }
            requestApprovers.removeAll(approvedList);
            shouldUpdateStatus = requestApprovers.size() == 0;
        }
        if (shouldUpdateStatus) {
            requestEntity.setStatus(status.getValue());
            try {
                log.info("Request " + requestEntity.getId() + " was " + status);
                requestJpa.save(requestEntity);
                long requestId = requestEntity.getId();
                if (status == RequestStatus.REJECTED) {
                    requestService.sendRejectedNotification(requestEntity);
                    Discussion discussion = new Discussion();
                    discussion.setContent(description != null && description.length() > 0 ? String.format(REJECT_MESSAGE_WITH_DESCRIPTION, description) : REJECT_MESSAGE_WITHOUT);
                    discussionService.createSystemDiscussion(1, requestId, discussion);
                }
                if (status == RequestStatus.APPROVED) {
                    requestService.sendApprovedNotification(requestEntity);
                    Discussion discussion = new Discussion();
                    discussion.setContent(description != null && description.length() > 0 ? String.format(APPROVED_FULLY_MESSAGE_WITH_DESCRIPTION, description) : APPROVED_FULLY_MESSAGE_WITHOUT);
                    discussionService.createSystemDiscussion(1, requestId, discussion);
                }

                return Either.right(Boolean.TRUE);
            } catch (Exception ex) {
                log.error("Cannot store request approval for " + requestEntity.getId(), ex);
                return Either.left(new AppException("cannot_store_request_approval"));
            }
        } else {
            if (status == RequestStatus.APPROVED) {
                Discussion discussion = new Discussion();
                discussion.setContent(description != null && description.length() > 0 ? String.format(APPROVED_MESSAGE_WITH_DESCRIPTION, description) : APPROVED_MESSAGE_WITHOUT);
                discussionService.createSystemDiscussion(1, requestEntity.getId(), discussion);
            }
        }

        log.info("Request Status is not changed: " + requestEntity.getId());
        return Either.right(Boolean.TRUE);
    }


    private Either<Exception, Boolean> storeData(RequestEntity requestEntity, RequestStatus status, List<String> targetUsers,String description) {
        try {
            if (status != RequestStatus.FORWARD) {
                RequestStatus decision = status == RequestStatus.AGREED_FORWARD ? RequestStatus.APPROVED : status;
                RequestApprovalRecordEntity entity = new RequestApprovalRecordEntity();
                entity.setRequest(requestEntity);
                entity.setApprover(TenantContext.getUsername());
                entity.setDecision(decision.getValue());
                entity.setComment(description);
                entity.setSubmittedTime(System.currentTimeMillis());

                requestApprovalJpa.save(entity);
            }
            if (status == RequestStatus.AGREED_FORWARD || status == RequestStatus.FORWARD) {
                RequestApprovalRecordEntity entity = new RequestApprovalRecordEntity();
                entity.setRequest(requestEntity);
                entity.setApprover(TenantContext.getUsername());
                entity.setDecision(RequestStatus.FORWARD.getValue());
                entity.setComment(description);
                entity.setSubmittedTime(System.currentTimeMillis());
                requestApprovalJpa.save(entity);
            }
            return Either.right(Boolean.TRUE);
        } catch (Exception ex) {
            log.error("Cannot save request status", ex);
            return Either.left(new AppException("cannot_process_request"));
        }
    }
}
