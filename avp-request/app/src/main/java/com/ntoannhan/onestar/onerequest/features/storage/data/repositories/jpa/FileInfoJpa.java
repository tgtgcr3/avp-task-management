package com.ntoannhan.onestar.onerequest.features.storage.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestEntity;
import com.ntoannhan.onestar.onerequest.features.storage.data.repositories.entities.FileInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface FileInfoJpa extends JpaRepository<FileInfoEntity, Long> {

    @Query("SELECT r FROM FileInfo r WHERE r.id = :id")
    @Override
    Optional<FileInfoEntity> findById(Long id);

    @Query("SELECT r FROM FileInfo r WHERE r.id = :id AND r.visibility = :visibility")
    Optional<FileInfoEntity> findById(Long id, int visibility);
}
