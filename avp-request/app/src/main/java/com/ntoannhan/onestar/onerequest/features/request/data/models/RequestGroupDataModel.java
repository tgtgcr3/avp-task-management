package com.ntoannhan.onestar.onerequest.features.request.data.models;

import com.ntoannhan.onestar.onerequest.core.data.DataModel;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestGroupVersionEntity;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupStatus;

import java.util.Arrays;
import java.util.stream.Collectors;

public class RequestGroupDataModel extends RequestGroup implements DataModel<RequestGroupVersionEntity, RequestGroupDataModel> {

    @Override
    public RequestGroupDataModel fromEntity(RequestGroupVersionEntity requestGroupVersionEntity) {

        this.id(requestGroupVersionEntity.getId());
        this.approvers(requestGroupVersionEntity.getApprovers() != null ? Arrays.asList(requestGroupVersionEntity.oneGetApprovers()) : null);
        this.approveType(requestGroupVersionEntity.getApprovalType().getId());
        this.followers(requestGroupVersionEntity.getFollowers() != null ? Arrays.asList(requestGroupVersionEntity.oneGetFollowers()) : null);
        this.requestForm(requestGroupVersionEntity.getRequestForm());
        this.sla(requestGroupVersionEntity.getSla());
        this.status(RequestGroupStatus.fromId(requestGroupVersionEntity.getStatus()));
        this.attrs(requestGroupVersionEntity.getAttrs() != null ?
                requestGroupVersionEntity.getAttrs().stream().map(ae -> new AttributeDataModel().fromEntity(ae)).collect(Collectors.toList()) : null);

        this.name(requestGroupVersionEntity.getRequestGroup().getName());
        this.targetType(requestGroupVersionEntity.getRequestGroup().getTargetEntityType());
        this.targetId(requestGroupVersionEntity.getRequestGroup().getTargetEntityId());
        this.mgmtNotification(requestGroupVersionEntity.getRequestGroup().getMgmtNotification());
        this.category(requestGroupVersionEntity.getRequestGroup().getCategory() != null ? requestGroupVersionEntity.getRequestGroup().getCategory().getName() : null);
        this.description(requestGroupVersionEntity.getRequestGroup().getDescription());
        this.creator(requestGroupVersionEntity.getRequestGroup().getCreator());
        this.requestForms(requestGroupVersionEntity.getRequestForms() != null ? Arrays.asList(requestGroupVersionEntity.oneGetRequestForms()) : null);
        this.approvalOwner(requestGroupVersionEntity.getApprovalOwner());
        this.oId(requestGroupVersionEntity.getRequestGroup().getId());
        return this;
    }

}
