package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class AddRequestFavorite implements UseCase<Boolean, Long> {

    private final RequestService requestService;

    public AddRequestFavorite(RequestService requestService) {
        this.requestService = requestService;
    }

    @Override
    public Either<Exception, Boolean> call(Long in) {
        return requestService.setFavorite(in, true);
    }
}
