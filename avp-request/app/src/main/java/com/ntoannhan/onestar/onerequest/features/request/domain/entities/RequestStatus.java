package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

public enum RequestStatus {
    PENDING(0), CANCELLED(1), TIMEOUT(2), REJECTED(3), REQUEST_UPDATE(4), APPROVED(5), CLOSED(6), MOVE(7),
    FORWARD(8), AGREED_FORWARD(9), TRANSFER(10), DELETED(11);

    private final int value;

    RequestStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static RequestStatus fromId(int id) {
        RequestStatus[] values = values();
        for (RequestStatus requestStatus : values) {
            if (requestStatus.value == id) {
                return requestStatus;
            }
        }
        return null;
    }

    public String getStringValue() {
        if (value == 1) {
            return "Đã Hủy";
        }
        if (value == 2) {
            return "Quá Hạn";
        }
        if (value == 3) {
            return "Từ Chối";
        }
        if (value == 5) {
            return "Đồng Ý";
        }
        return "Chờ Xét Duyệt";
    }
}
