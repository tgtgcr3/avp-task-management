package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class GetOneRequestGroupIn {

    private long id;

    private boolean latest;

}
