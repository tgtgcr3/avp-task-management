package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.model.RequestGroupModel;
import com.ntoannhan.onestar.onerequest.rest.model.RequestGroupGetResult;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.ntoannhan.onestar.onerequest.features.request.domain.usecases.model.AttributeModel.toAttributeRest;

@Data
@Accessors(fluent = true)
public class GetRequestGroupOut {

    private int total;

    private List<RequestGroup> items;

    public static RequestGroupGetResult toGetRequestGroupResult(GetRequestGroupOut out) {
        return new RequestGroupGetResult()
                .total(out.total)
                .items(out.items.stream().map(RequestGroupModel::toRequestGroup).collect(Collectors.toList()));
    }
}
