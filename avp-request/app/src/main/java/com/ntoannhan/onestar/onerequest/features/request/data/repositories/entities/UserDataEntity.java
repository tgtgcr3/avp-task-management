package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.util.List;

@Data
@Entity(name = "UserData")
@IdClass(UserDataId.class)
public class UserDataEntity implements ArrayStringTypeWrapper {

    @Id
    private String username;

    @Id
    private String target;

    @Id
    private int dataType;

    private String data;

    public String[] oneGetData() {
        return toStringArray(this.data);
    }

    public void oneSetData(String[] data) {
        this.data = fromStringArray(data);
    }

    public void oneSetData(List<String> data) {
        this.data = fromStringArray(data);
    }

    public void oneSetId(UserDataId userDataId) {
        username = userDataId.getUsername();
        target = userDataId.getTarget();
        dataType = userDataId.getDataType();
    }

}
