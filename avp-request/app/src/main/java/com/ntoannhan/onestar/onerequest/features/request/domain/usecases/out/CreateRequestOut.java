package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Request;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class CreateRequestOut {

    private Request request;

}
