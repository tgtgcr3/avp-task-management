package com.ntoannhan.onestar.onerequest.features.storage.domain.entities;

public enum FileInfoVisibility {
    PRIVATE(0), PUBLIC(1);

    private final int value;

    FileInfoVisibility(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static FileInfoVisibility fromId(int id) {
        FileInfoVisibility[] values = values();
        for (FileInfoVisibility fileInfoVisibility : values) {
            if (fileInfoVisibility.value == id) {
                return fileInfoVisibility;
            }
        }
        return null;
    }
}
