package com.ntoannhan.onestar.onerequest.interfaces.config;

import com.ntoannhan.onestar.onerequest.core.config.HeaderAuthenticationFilter;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

  @Autowired
  private HeaderAuthenticationFilter headerAuthenticationFilter;

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

    KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
    keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
    auth.authenticationProvider(keycloakAuthenticationProvider);
  }

  @Bean
  public KeycloakSpringBootConfigResolver KeycloakConfigResolver() {
    return new KeycloakSpringBootConfigResolver();
  }

  @Bean
  @Override
  protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
    return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());

  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    super.configure(http);
    http.csrf().disable();
//    http.authorizeRequests().antMatchers("/api/pri/**").permitAll()
    http.authorizeRequests().antMatchers("/api/pri/file/download/*").permitAll()
//        .permitAll().antMatchers("/api/pri/avatar").permitAll()
//        .antMatchers("/api/pri/avatars").permitAll()
//        .antMatchers("/api/pri/notifications").permitAll()
//        .antMatchers("/api/pri/notification/*").permitAll()
//        .antMatchers("/api/pri/user/*/avatar").permitAll()
//        .antMatchers("/api/pri/keycloak/users**").permitAll()
//            .antMatchers("/api/pri/request/**").permitAll()
            .antMatchers("/*").permitAll()
        .antMatchers("/api/pri/**").hasRole("user").anyRequest().permitAll()
    .and().addFilterBefore(headerAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)

    ;
  }
}