package com.ntoannhan.onestar.onerequest.core.data.repositories.listener;

import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.OneRevisionEntity;
import org.hibernate.envers.RevisionListener;

public class AuditListener implements RevisionListener {

    @Override
    public void newRevision(Object entity) {
        if (entity instanceof OneRevisionEntity) {
            final String username = TenantContext.getUsername();
            ((OneRevisionEntity)entity).setUsername(username);
            ((OneRevisionEntity)entity).setDescription(TenantContext.getDescription());
        }
    }
}
