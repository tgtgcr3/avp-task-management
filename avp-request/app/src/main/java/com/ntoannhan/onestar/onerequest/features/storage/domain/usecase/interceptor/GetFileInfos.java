package com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.interceptor;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.storage.domain.service.StorageService;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in.GetFileInfosIn;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.out.GetFileInfosOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class GetFileInfos implements UseCase<GetFileInfosOut, GetFileInfosIn> {

    private final StorageService storageService;

    @Autowired
    public GetFileInfos(@Qualifier("InternalStorageService") StorageService storageService) {
        this.storageService = storageService;
    }

    @Override
    public Either<Exception, GetFileInfosOut> call(GetFileInfosIn in) {
        return storageService.getFileInfos(in.fileIds()).fold(Either::left, out -> Either.right(new GetFileInfosOut().fileInfos(out)));
    }
}
