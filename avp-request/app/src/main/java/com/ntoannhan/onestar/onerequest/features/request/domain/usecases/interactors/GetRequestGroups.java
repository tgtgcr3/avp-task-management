package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupStatus;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetRequestGroupsIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetRequestGroupOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetRequestGroups implements UseCase<GetRequestGroupOut, GetRequestGroupsIn> {

    private final RequestGroupService requestGroupService;

    public GetRequestGroups(RequestGroupService requestGroupService) {
        this.requestGroupService = requestGroupService;
    }

    @Override
    public Either<Exception, GetRequestGroupOut> call(GetRequestGroupsIn in) {
        return requestGroupService.getAll(true, RequestGroupStatus.ENABLED).fold(Either::left, requestGroups ->
                Either.right(new GetRequestGroupOut().items(requestGroups).total(requestGroups.size()))
                );
    }
}
