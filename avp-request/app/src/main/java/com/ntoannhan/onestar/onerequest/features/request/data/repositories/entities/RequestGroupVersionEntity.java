package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.AbstractBaseEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "RequestGroupVersion")
@Audited
public class RequestGroupVersionEntity extends AbstractBaseEntity implements ArrayStringTypeWrapper{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @OneToOne
    private RequestGroupEntity requestGroup;

    @JoinTable(name = "Request_Group_Version_Attribute")
    @OneToMany
    private List<AttributeEntity> attrs;

    @Column(columnDefinition = "varchar(1024)")
    private String followers;

    @Column(columnDefinition = "varchar(1024)")
    private String approvers;

    @Column(columnDefinition = "int default 0")
    private int requestForm;

    private Long sla;

    @OneToOne
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private RequestApprovalTypeEntity approvalType;

    @Column(columnDefinition = "int default 0")
    private int status;

    @Column(columnDefinition = "varchar(1024)")
    private String requestForms;

    private String approvalOwner;

    public void oneSetFollowers(String[] value) {
        this.followers = fromStringArray(value);
    }

    public void oneSetFollowers(List<String> value) {
        this.followers = fromStringArray(value);
    }

    public String[] oneGetFollowers() {
        return toStringArray(followers);
    }

    public void oneSetApprovers(String[] value) {
        this.approvers = fromStringArray(value);
    }

    public void oneSetApprovers(List<String> value) {
        this.approvers = fromStringArray(value);
    }

    public String[] oneGetApprovers() {
        return toStringArray(this.approvers);
    }

    public void oneSetRequestForms(String[] value) {
        this.requestForms = fromStringArray(value);
    }

    public void oneSetRequestForms(List<String> value) {
        this.requestForms = fromStringArray(value);
    }

    public String[] oneGetRequestForms() {
        return toStringArray(this.requestForms);
    }

}
