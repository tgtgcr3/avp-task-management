package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class RequestActivity {

    private long date;

    private String description;

    private String owner;

    private RequestStatus action;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public RequestStatus getAction() {
        return action;
    }

    public void setAction(RequestStatus action) {
        this.action = action;
    }
}
