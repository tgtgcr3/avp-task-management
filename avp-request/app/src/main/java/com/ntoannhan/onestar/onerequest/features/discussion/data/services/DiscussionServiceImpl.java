package com.ntoannhan.onestar.onerequest.features.discussion.data.services;

import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import com.ntoannhan.onestar.onerequest.core.data.entities.ResultGetAllData;
import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.features.discussion.data.repositories.entities.DiscussionEntity;
import com.ntoannhan.onestar.onerequest.features.discussion.data.repositories.jpa.DiscussionJpa;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.Discussion;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.DiscussionStatus;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.services.DiscussionService;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.services.RequestServiceImpl;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Request;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service("InternalDiscussionService")
public class DiscussionServiceImpl implements DiscussionService {

    private final DiscussionJpa discussionJpa;

    private final ModelMapper modelMapper;

    public DiscussionServiceImpl(DiscussionJpa discussionJpa, ModelMapper modelMapper) {
        this.discussionJpa = discussionJpa;
        this.modelMapper = modelMapper;
    }

    @Override
    public Either<Exception, Discussion> createDiscussion(long targetType, long targetId, Discussion discussion) {
        return createDiscussionWithStatus(targetType, targetId, discussion, DiscussionStatus.NEW);
    }

    private Either<Exception, Discussion> createDiscussionWithStatus(long targetType, long targetId, Discussion discussion, DiscussionStatus status) {
        try {
            DiscussionEntity entity = convertToEntityModel(discussion);
            entity.setStatus(status.getValue());
            entity.setTargetType(targetType);
            entity.setTargetId(targetId);
            entity.setOwner(TenantContext.getUsername());
            entity.setCreatedDate(System.currentTimeMillis());
            entity.setLastUpdatedDate(System.currentTimeMillis());
            DiscussionEntity save = discussionJpa.save(entity);
            return Either.right(convertToDomainModel(save));
        } catch (Exception ex) {
            log.error("Cannot save discussion for {} - {}", targetType, targetId, ex);
        }
        return Either.left(new AppException("cannot_save_discussion"));
    }

    @Override
    public Either<Exception, Discussion> updateDiscussion(long targetType, long targetId, long id, Discussion discussion) {
        try {
            Optional<DiscussionEntity> discussionEntityOpt = discussionJpa.findById(targetType, targetId, id);
            if (discussionEntityOpt.isPresent()) {
                DiscussionEntity entity = discussionEntityOpt.get();
                if (entity.getStatus() != DiscussionStatus.DELETED.getValue()) {
                    entity.setStatus(DiscussionStatus.EDITTED.getValue());
                    entity.setLastUpdatedDate(System.currentTimeMillis());
                    entity.oneSetMentioneds(discussion.getMentioned());
                    entity.oneSetFileIds(discussion.getFileIds());
                    entity.setContent(discussion.getContent());
                    DiscussionEntity save = discussionJpa.save(entity);
                    return Either.right(convertToDomainModel(save));
                }
                return Either.left(new UserException("discussion_deleted"));
            }
            return Either.left(new UserException("discussion_not_found"));
        } catch (Exception ex) {
            log.error("Cannot update discussion for {} - {}", targetType, targetId, ex);
        }
        return Either.left(new AppException("cannot_update_discussion"));
    }

    @Override
    public Either<Exception, Boolean> deleteDiscussion(long targetType, long targetId, long id) {
        try {
            Optional<DiscussionEntity> discussionEntityOpt = discussionJpa.findById(targetType, targetId, id);
            if (discussionEntityOpt.isPresent()) {
                DiscussionEntity entity = discussionEntityOpt.get();
                if (entity.getStatus() != DiscussionStatus.DELETED.getValue()) {
                    entity.setLastUpdatedDate(System.currentTimeMillis());
                    entity.setStatus(DiscussionStatus.DELETED.getValue());
                    discussionJpa.save(entity);
                    return Either.right(Boolean.TRUE);
                }
                return Either.left(new UserException("discussion_deleted"));
            }
            return Either.left(new UserException("discussion_not_found"));
        } catch (Exception ex) {
            log.error("Cannot update discussion for {} - {}", targetType, targetId, ex);
        }
        return Either.left(new AppException("cannot_delete_discussion"));
    }

    @Override
    public Either<Exception, Discussion> getDiscussion(long targetType, long targetId, long id) {
        try {
            Optional<DiscussionEntity> discussionEntityOpt = discussionJpa.findById(targetType, targetId, id);
            if (discussionEntityOpt.isPresent()) {
                DiscussionEntity entity = discussionEntityOpt.get();
                return Either.right(convertToDomainModel(entity));
            }
            return Either.left(new UserException("discussion_not_found"));
        } catch (Exception ex) {
            log.error("Cannot update discussion for {} - {}", targetType, targetId, ex);
        }
        return Either.left(new AppException("cannot_update_discussion"));
    }

    @Override
    public Either<Exception, ResultGetAllData<Discussion>> getAll(long targetType, long targetId, int page, int size, boolean desc) {
        //permission?
        try {
            int count = discussionJpa.countAllWithTarget(targetType, targetId);
            List<DiscussionEntity> entities = discussionJpa.getAllWithTarget(targetType, targetId,
                    PageRequest.of(page, size, Sort.by(desc ? Sort.Direction.DESC : Sort.Direction.ASC, "id")));
            return Either.right(new ResultGetAllData<Discussion>()
                    .total(count)
                    .items(entities.stream().map(this::convertToDomainModel).collect(Collectors.toList()))
            );
        } catch (Exception ex) {
            log.error("Cannot get all Discussion for {} - {}", targetType, targetId,ex);
        }

        return Either.left(new AppException("cannot_getall_discussion"));
    }

    @Override
    public Either<Exception, Discussion> createReadOnlyDiscussion(long targetType, long targetId, Discussion discussion) {
        return createDiscussionWithStatus(targetType, targetId, discussion, DiscussionStatus.READ_ONLY);
    }

    private DiscussionEntity convertToEntityModel(Discussion discussion) {
        DiscussionEntity entity = OneMapper.mapTo(discussion, DiscussionEntity.class);
        entity.oneSetMentioneds(discussion.getMentioned());
        entity.oneSetFileIds(discussion.getFileIds());
        return entity;
    }

    private Discussion convertToDomainModel(DiscussionEntity entity) {
        Discussion discussion = OneMapper.mapTo(entity, Discussion.class);
        discussion.setFileIds(entity.oneGetFileIds());
        discussion.setMentioned(entity.oneGetMemtioneds());
        return discussion;
    }

    @PostConstruct
    private void postConstruct() {
        modelMapper.addMappings(new DiscussionServiceImpl.EntityToDomainRequestMapping());
        modelMapper.addMappings(new DiscussionServiceImpl.DomainToEntityRequestMapping());
    }

    public static Converter<DiscussionStatus, Integer> requestStatusConverter = new Converter<DiscussionStatus, Integer>() {
        @Override
        public Integer convert(MappingContext<DiscussionStatus, Integer> mappingContext) {
            if (mappingContext.getSource() != null) {
                return mappingContext.getSource().getValue();
            }
            return DiscussionStatus.NEW.getValue();
        }
    };

    public static Converter<Integer, DiscussionStatus> intToRequestStatusConverter = new Converter<Integer, DiscussionStatus>() {
        @Override
        public DiscussionStatus convert(MappingContext<Integer, DiscussionStatus> mappingContext) {
            return DiscussionStatus.fromId(mappingContext.getSource());
        }
    };

    static class EntityToDomainRequestMapping extends PropertyMap<Discussion, DiscussionEntity> {

        @Override
        protected void configure() {
            DiscussionEntity map = using(requestStatusConverter).map();
            try {
                if (map != null) {
                    map.setStatus(this.<Integer>source("status"));
                }
            } catch (Exception ex) {
            }
        }
    }

    static class DomainToEntityRequestMapping extends PropertyMap<DiscussionEntity, Discussion> {

        @Override
        protected void configure() {
            using(intToRequestStatusConverter).map().setStatus(this.<DiscussionStatus>source("status"));
        }
    }
}
