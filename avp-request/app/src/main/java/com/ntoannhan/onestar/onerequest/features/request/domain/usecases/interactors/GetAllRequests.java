package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetAllRequestsIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetAllRequestsOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetAllRequests implements UseCase<GetAllRequestsOut, GetAllRequestsIn> {

    @Autowired
    private RequestService requestService;

    @Override
    public Either<Exception, GetAllRequestsOut> call(GetAllRequestsIn in) {
        return requestService.getAll(in.search(), in.page() == null ? 0 : in.page(), in.size() == null ? 100 : in.size(), in.fullData(), in.requestGroupId(),
                in.toMe(), in.fromMe(), in.followed(), in.requestStatus(), in.favorite(),
                in.creators(), in.createdDate(), in.end()
                )
                .fold(Either::left, result -> Either.right(
                        new GetAllRequestsOut().total(result.total())
                                            .items(result.items())
                ));
    }
}
