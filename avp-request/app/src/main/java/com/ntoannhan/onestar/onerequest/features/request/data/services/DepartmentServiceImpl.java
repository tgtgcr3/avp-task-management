package com.ntoannhan.onestar.onerequest.features.request.data.services;

import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.features.caching.domain.usecase.ClearDepartmentCache;
import com.ntoannhan.onestar.onerequest.features.caching.domain.usecase.ClearUserCache;
import com.ntoannhan.onestar.onerequest.features.request.data.models.DepartmentDataModel;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.DepartmentEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.DepartmentJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.services.KeycloakService;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Department;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.DepartmentService;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Component
public class DepartmentServiceImpl implements DepartmentService {

    private final KeycloakService keycloakService;

    private final DepartmentJpa departmentJpa;

    private final ClearDepartmentCache clearDepartmentCache;

    private final ClearUserCache clearUserCache;

    @Autowired
    public DepartmentServiceImpl(KeycloakService keycloakService, DepartmentJpa departmentJpa, ClearDepartmentCache clearDepartmentCache, ClearUserCache clearUserCache) {
        this.keycloakService = keycloakService;
        this.departmentJpa = departmentJpa;
        this.clearDepartmentCache = clearDepartmentCache;
        this.clearUserCache = clearUserCache;
    }

    @Override
    public Either<Exception, List<Department>> getAll(boolean includeMembers) {
        try {
            DepartmentEntity[] entities = departmentJpa.getAllDepartments();
            if (entities == null) {
                clearDepartmentCache.call(Boolean.TRUE);
                entities = departmentJpa.getAllDepartments();
            }
            if (entities == null) {
                log.error("Cannot retrieve department from service");
                return Either.left(new AppException("cannot_retrieve_department"));
            }
            if (!includeMembers) {
                return Either.right(Arrays.stream(entities).map(e -> {
                    DepartmentDataModel departmentDataModel = new DepartmentDataModel().fromEntity(e);
                    departmentDataModel.setMembers(Collections.emptyList());
                    return departmentDataModel;
                }).collect(Collectors.toList()));
            }
            List<UserRepresentation> allUsers = keycloakService.getAllUsers();
            if (allUsers == null) {
                clearUserCache.call(Boolean.TRUE);
                allUsers = keycloakService.getAllUsers();
            }
            if (allUsers != null) {
                List<UserRepresentation> finalAllUsers = allUsers;
                return Either.right(Arrays.stream(entities)
                        .map(e -> {
                            List<String> username = e.getMembers().stream().map(email -> {
                                try {
                                    Optional<UserRepresentation> any = finalAllUsers.stream().filter(u -> email.equals(u.getEmail())).findAny();
                                    if (any.isPresent()) {
                                        return any.get().getUsername();
                                    }
                                } catch (Exception ex) {
                                    log.error("Cannot find username for email: " + email);
                                }
                                return null;
                            }).filter(Objects::nonNull).collect(Collectors.toList());

                            DepartmentDataModel dataModel = new DepartmentDataModel().fromEntity(e);
                            dataModel.setMembers(username);
                            return dataModel;
                        }).collect(Collectors.toList()));
            }
            log.error("Cannot retrieve user from userservice");
            return Either.left(new AppException("cannot_get_user"));
        } catch (Exception ex) {
            log.error("Cannot get department from TaskManager", ex);
            return Either.left(new AppException("cannot_get_departments"));
        }

    }


}
