package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class DeleteRequestGroupForwardIn {

    private long requestId;

    private long forwardId;

}
