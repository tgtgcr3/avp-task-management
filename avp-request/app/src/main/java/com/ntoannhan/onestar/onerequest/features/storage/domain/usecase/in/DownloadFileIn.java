package com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in;

import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfoVisibility;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class DownloadFileIn {

    private long fileId;

    private FileInfoVisibility visibility;
}
