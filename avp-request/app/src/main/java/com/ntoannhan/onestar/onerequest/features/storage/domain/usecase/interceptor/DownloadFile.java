package com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.interceptor;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.storage.domain.service.StorageService;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in.DownloadFileIn;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.out.DownloadFileOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DownloadFile implements UseCase<DownloadFileOut, DownloadFileIn> {

    private final StorageService storageService;

    @Autowired
    public DownloadFile(@Qualifier("InternalStorageService") StorageService storageService) {
        this.storageService = storageService;
    }

    @Override
    public Either<Exception, DownloadFileOut> call(DownloadFileIn in) {
        return storageService.loadFile(in.fileId(), in.visibility()).fold(Either::left, out -> Either.right(new DownloadFileOut().resource(out.getResource()).fileName(out.getFileName())));
    }
}
