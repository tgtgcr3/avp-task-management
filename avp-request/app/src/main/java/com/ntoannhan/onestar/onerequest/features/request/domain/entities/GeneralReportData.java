package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class GeneralReportData {

    private int total;

    private int approved;

    private int rejected;

    private double sla;

    private double timeout;

    private int processed;

    private String highActive;

    private int timeoutNum;

    public int getTimeoutNum() {
        return timeoutNum;
    }

    public void setTimeoutNum(int timeoutNum) {
        this.timeoutNum = timeoutNum;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public int getRejected() {
        return rejected;
    }

    public void setRejected(int rejected) {
        this.rejected = rejected;
    }

    public double getSla() {
        return sla;
    }

    public void setSla(double sla) {
        this.sla = sla;
    }

    public double getTimeout() {
        return timeout;
    }

    public void setTimeout(double timeout) {
        this.timeout = timeout;
    }

    public String getHighActive() {
        return highActive;
    }

    public void setHighActive(String highActive) {
        this.highActive = highActive;
    }

    public int getProcessed() {
        return processed;
    }

    public void setProcessed(int processed) {
        this.processed = processed;
    }
}
