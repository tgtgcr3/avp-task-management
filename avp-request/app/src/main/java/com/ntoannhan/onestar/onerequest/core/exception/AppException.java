package com.ntoannhan.onestar.onerequest.core.exception;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class AppException extends Exception{

    private int code;

    private Object extra;

    public AppException(String message) {
        super(message);
    }

    public AppException(int code, String message) {
        this(message);
        this.code = code;
    }

    public AppException(int code, String message, Object extra) {
        this(code, message);
        this.extra = extra;
    }

}
