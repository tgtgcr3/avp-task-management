package com.ntoannhan.onestar.onerequest.features.request.domain.services;


import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.AttributeEntity;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.AttributeType;
import io.vavr.control.Either;

import java.util.List;

public interface AttributeService {

    Either<Exception, List<AttributeEntity>> createAttributes(List<Attribute> attrs);

    Either<Exception, List<AttributeEntity>> getAttributes(List<Long> ids);

    Either<Exception, Integer> updateAttributes(List<Attribute> attrs);

    Either<Exception, List<AttributeType>> getAllAttributeTypes();
}
