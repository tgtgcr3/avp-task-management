package com.ntoannhan.onestar.onerequest.features.storage.presentation.rest.model;

import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.core.presentation.model.rest.BaseRestModel;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;

public class FileInfoModel implements BaseRestModel<FileInfo, com.ntoannhan.onestar.onerequest.rest.model.FileInfo> {
    @Override
    public com.ntoannhan.onestar.onerequest.rest.model.FileInfo toRestModel(FileInfo fileInfo) {
        return OneMapper.mapTo(fileInfo, com.ntoannhan.onestar.onerequest.rest.model.FileInfo.class);
    }

    @Override
    public FileInfo toDomainModel(com.ntoannhan.onestar.onerequest.rest.model.FileInfo fileInfo) {
        return OneMapper.mapTo(fileInfo, FileInfo.class);
    }
}
