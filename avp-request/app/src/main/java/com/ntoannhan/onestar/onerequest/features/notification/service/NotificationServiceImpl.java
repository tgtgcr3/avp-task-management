package com.ntoannhan.onestar.onerequest.features.notification.service;

import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.features.notification.domain.usecases.in.RegisterTemplateIn;
import com.ntoannhan.onestar.onerequest.features.notification.domain.usecases.interceptors.RegisterTemplate;
import com.ntoannhan.onestar.onerequest.features.notification.presentation.data.NotificationType;
import com.ntoannhan.onestar.onerequest.features.notification.presentation.service.NotificationService;
import com.ntoannhan.onestar.onerequest.interfaces.config.AppConfiguration;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@Component
public class NotificationServiceImpl implements NotificationService {

    private final RegisterTemplate registerTemplate;

    private final RestTemplate restTemplate;

    private final AppConfiguration appConfiguration;

    public NotificationServiceImpl(RegisterTemplate registerTemplate, RestTemplateBuilder templateBuilder, AppConfiguration appConfiguration) {
        this.registerTemplate = registerTemplate;
        this.restTemplate = templateBuilder.build();
        this.appConfiguration = appConfiguration;
    }

    @Override
    public Either<Exception, Long> registerTemplate(String appName, String category, NotificationType notificationType, String template) {
        return registerTemplate.call((RegisterTemplateIn) (new RegisterTemplateIn().appName(appName).category(category).notificationType(notificationType.getValue()).template(template)))
                .fold(Either::left, notificationTemplate -> Either.right(notificationTemplate.id()));
    }

    @Override
    public Either<Exception, Boolean> sendEmails(List<Map<String, Object>> emailDatas) {
        if (emailDatas.isEmpty()) {
            return Either.left(new UserException("List is empty"));
        }
        String url = this.appConfiguration.getNotificationUrl() + "/api/pri/notifications";

        HttpHeaders headers = new HttpHeaders();
        headers.set("X-APP-ID", "request");
        headers.set("X-TENANT-ID", "an-viet-phat");
        headers.set("X-ONE-HEADER", appConfiguration.getNotificationToken());
        emailDatas = emailDatas.stream().map(HashMap::new).collect(Collectors.toList());

        emailDatas.forEach(e -> {
            e.put("appName", "request");
            e.put("notificationType", "Email");
        });

        HttpEntity<Object> entity = new HttpEntity<>(emailDatas, headers);

        restTemplate.exchange(url, HttpMethod.POST, entity, Map.class);

        return Either.right(Boolean.TRUE);

    }

    @Override
    public Either<Exception, Boolean> sendNotification(Object data) {
        try {
            if (data instanceof List && ((List)data).isEmpty()) {
                return Either.left(new UserException("List is empty"));
            }
            String url = this.appConfiguration.getNotificationUrl() + "/api/pri/notifications";

            HttpHeaders headers = new HttpHeaders();
            headers.set("X-APP-ID", "request");
            headers.set("X-TENANT-ID", "an-viet-phat");
            headers.set("X-ONE-HEADER", appConfiguration.getNotificationToken());

            HttpEntity<Object> entity = new HttpEntity<>(data, headers);

            restTemplate.exchange(url, HttpMethod.POST, entity, Map.class);

            return Either.right(Boolean.TRUE);
        } catch (Exception ex) {
            log.error("Cannot send notification", ex);
            return Either.left(new AppException("cannot_send_notification"));
        }
    }
}
