package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.AttributeType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class GetAllAttributeTypesOut {

    private int total;

    private List<AttributeType> items;

}
