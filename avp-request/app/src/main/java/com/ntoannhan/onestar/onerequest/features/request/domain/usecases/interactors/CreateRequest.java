package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.CreateRequestIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.CreateRequestOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateRequest implements UseCase<CreateRequestOut, CreateRequestIn> {

    @Autowired
    private RequestService requestService;

    @Override
    public Either<Exception, CreateRequestOut> call(CreateRequestIn in) {
        return requestService.createRequest(in.request()).fold(Either::left, request -> Either.right(new CreateRequestOut().request(request)));

    }
}
