package com.ntoannhan.onestar.onerequest.features.notification.data.services;

import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.features.notification.data.repositories.entities.NotificationTemplateEntity;
import com.ntoannhan.onestar.onerequest.features.notification.data.repositories.jpa.NotificationTemplateJpa;
import com.ntoannhan.onestar.onerequest.features.notification.domain.entities.NotificationTemplate;
import com.ntoannhan.onestar.onerequest.features.notification.domain.services.NotificationService;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Log4j2
@Component("data-notification-service")
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private NotificationTemplateJpa notificationTemplateJpa;

    @Override
    public Either<Exception, NotificationTemplate> createNotificationTemplate(NotificationTemplate notificationTemplate) {
        return createOrUpdateEntity(null, notificationTemplate).fold(Either::left, entity -> {
            notificationTemplate.id(entity.getId());
            return Either.right(notificationTemplate);
        });
    }

    private Either<Exception, NotificationTemplateEntity> createOrUpdateEntity(Long id, NotificationTemplate notificationTemplate) {
        NotificationTemplateEntity entity = null;
        if (id != null) {
            Optional<NotificationTemplateEntity> optional = notificationTemplateJpa.findById(id);
            if (optional.isEmpty()) {
                return Either.left(new UserException("notification_template_not_found"));
            }
            entity = optional.get();
        }
        if (entity != null) {
            updateEntityData(notificationTemplate, entity);
        } else {
            entity = convertNotificationTemplateToEntity(notificationTemplate);
        }

        try {
            notificationTemplateJpa.save(entity);
            return Either.right(entity);
        } catch (Exception ex) {
            log.error("Cannot SaveOrUpdate NotificationTemplate", ex);
            return Either.left(new AppException("cannot_create_notification_template"));
        }
    }

    private void updateEntityData(NotificationTemplate notificationTemplate, NotificationTemplateEntity entity) {
        entity.setAppName(notificationTemplate.appName());
        entity.setCategory(notificationTemplate.category());
        entity.setNotificationType(notificationTemplate.notificationType());
        entity.setTemplate(notificationTemplate.template());
    }

    private NotificationTemplateEntity convertNotificationTemplateToEntity(NotificationTemplate notificationTemplate) {
        NotificationTemplateEntity entity = new NotificationTemplateEntity();
        entity.setId(notificationTemplate.id());
        entity.setAppName(notificationTemplate.appName());
        entity.setCategory(notificationTemplate.category());
        entity.setNotificationType(notificationTemplate.notificationType());
        entity.setTemplate(notificationTemplate.template());
        return entity;
    }
}
