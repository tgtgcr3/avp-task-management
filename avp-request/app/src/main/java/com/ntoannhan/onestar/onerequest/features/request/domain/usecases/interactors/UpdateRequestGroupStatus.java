package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupStatus;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.UpdateRequestGroupStatusIn;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;

@Component
public class UpdateRequestGroupStatus implements UseCase<Boolean, UpdateRequestGroupStatusIn> {

    @Autowired
    private RequestGroupService requestGroupService;

    @RolesAllowed({"admin"})
    @Override
    public Either<Exception, Boolean> call(UpdateRequestGroupStatusIn in) {
        if (in.requestGroupId() == null) {
            return Either.left(new UserException("id_missing"));
        }
        String status = in.status() != null ? in.status().toLowerCase() : "";
        if (!("disabled".equals(status) || "enabled".equals(status) || "deleted".equals(status))) {
            return Either.left(new UserException("status_not_valid"));
        }

        return requestGroupService.updateRequestGroupStatus(in.requestGroupId(),
                "enabled".equals(status) ? RequestGroupStatus.ENABLED :
                            "disabled".equals(status) ? RequestGroupStatus.DISABLED : RequestGroupStatus.DELETED)
                .fold(Either::left, Either::right);
    }
}
