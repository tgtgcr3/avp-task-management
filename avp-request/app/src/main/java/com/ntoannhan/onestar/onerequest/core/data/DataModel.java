package com.ntoannhan.onestar.onerequest.core.data;

public interface DataModel<E, M> {

    M fromEntity(E e);

}
