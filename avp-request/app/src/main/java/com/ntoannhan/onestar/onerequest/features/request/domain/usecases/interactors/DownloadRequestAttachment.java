package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.DownloadRequestAttachmentIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.DownloadRequestAttachmentOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DownloadRequestAttachment implements UseCase<DownloadRequestAttachmentOut, DownloadRequestAttachmentIn> {

    @Autowired
    private RequestService requestService;

    @Override
    public Either<Exception, DownloadRequestAttachmentOut> call(DownloadRequestAttachmentIn in) {
        return requestService.downloadAttachment(in.requestId(), in.fileId()).fold(Either::left,
                out -> Either.right(new DownloadRequestAttachmentOut().resource(out)));
    }
}
