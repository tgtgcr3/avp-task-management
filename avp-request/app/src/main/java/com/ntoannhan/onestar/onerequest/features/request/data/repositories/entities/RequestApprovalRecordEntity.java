package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.AbstractBaseEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Data
@Entity(name = "RequestApprovalRecord")
@Audited
public class RequestApprovalRecordEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String approver;

    @Column(columnDefinition = "int default 0")
    private int decision;

    @Column(columnDefinition = "TEXT")
    private String comment;

    @OneToOne
    private RequestEntity request;

    private long submittedTime;
}
