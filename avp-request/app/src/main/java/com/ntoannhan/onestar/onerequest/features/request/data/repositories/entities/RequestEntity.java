package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.AbstractBaseEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
@Entity(name="Request")
@Audited
public class RequestEntity extends AbstractBaseEntity implements ArrayStringTypeWrapper{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String name;

    @OneToOne
    private RequestGroupVersionEntity requestGroupVersionEntity;

    @Column(columnDefinition = "int default 0")
    private int status;

    @Column(columnDefinition = "MEDIUMTEXT")
    private String value;

    @Column(columnDefinition = "varchar(1024)")
    private String directMgmts;

    @Column(columnDefinition = "varchar(1024)")
    private String followers;

    private String documents;

    private String approvers;

    @OneToMany
    private List<RequestApprovalRecordEntity> approvalRecords;

    private String submitter;

    private long submitTime;

    private Long timeout;

    private long lastUpdated;

    private String attrDocuments;

    private String directContent;

    private String taskIds;

    @OneToOne
    private RequestGroupEntity oldRequestGroup;

    @OneToMany(mappedBy = "request")
    private List<RequestForwardEntity> forwards;

    public void oneSetTaskIds(List<Long> values) {
        if (values != null) {
            List<String> ids = values.stream().map(Object::toString).collect(Collectors.toList());
            this.taskIds = fromStringArray(ids);
        }
    }

    public List<Long> oneGetTaskIds() {
        return Arrays.stream(toStringArray(this.taskIds)).map(Long::parseLong).collect(Collectors.toList());
    }

    public void oneSetDirectMgmts(String[] value) {
        this.directMgmts = fromStringArray(value);
    }

    public String[] oneGetDirectMgmts() {
        return toStringArray(this.directMgmts);
    }

    public void oneSetFollowers(String[] value) {
        this.followers = fromStringArray(value);
    }

    public String[] oneGetFollowers() {
        return toStringArray(followers);
    }

    public void oneSetDocuments(String[] value) {
        this.documents = fromStringArray(value);
    }

    public String[] oneGetDocuments() {
        return toStringArray(this.documents);
    }

    public void oneSetAttrDocuments(String[] value) {
        this.attrDocuments = fromStringArray(value);
    }

    public String[] oneGetAttrDocuments() {
        return toStringArray(this.attrDocuments);
    }

    public void oneSetApprovers(String[] value) {
        this.approvers = fromStringArray(value);
    }

    public String[] oneGetApprovers() {
        return toStringArray(this.approvers);
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RequestEntity that = (RequestEntity) o;
        return status == that.status && submitTime == that.submitTime && lastUpdated == that.lastUpdated && Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(value, that.value) && Objects.equals(directMgmts, that.directMgmts) && Objects.equals(followers, that.followers) && Objects.equals(documents, that.documents) && Objects.equals(approvers, that.approvers) && Objects.equals(submitter, that.submitter) && Objects.equals(timeout, that.timeout) && Objects.equals(attrDocuments, that.attrDocuments) && Objects.equals(directContent, that.directContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name, status, value, directMgmts, followers, documents, approvers, submitter, submitTime, timeout, lastUpdated, attrDocuments, directContent);
    }

    @Override
    public String toString() {
        return "RequestEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", value='" + value + '\'' +
                ", directMgmts='" + directMgmts + '\'' +
                ", followers='" + followers + '\'' +
                ", documents='" + documents + '\'' +
                ", approvers='" + approvers + '\'' +
                ", submitter='" + submitter + '\'' +
                ", submitTime=" + submitTime +
                ", timeout=" + timeout +
                ", lastUpdated=" + lastUpdated +
                ", attrDocuments='" + attrDocuments + '\'' +
                ", directContent='" + directContent + '\'' +
                ", taskIds='" + taskIds + '\'' +
                '}';
    }
}
