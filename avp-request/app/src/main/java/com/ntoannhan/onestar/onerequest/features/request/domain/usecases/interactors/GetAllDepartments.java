package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.DepartmentService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetAllDepartmentDataOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetAllDepartments implements UseCase<GetAllDepartmentDataOut, Boolean> {

    @Autowired
    private DepartmentService departmentService;

    @Override
    public Either<Exception, GetAllDepartmentDataOut> call(Boolean in) {
        return departmentService.getAll(in != null && in.booleanValue() == true).fold(Either::left,
            departments -> Either.right(new GetAllDepartmentDataOut().total((long) departments.size()).items(departments))
                );
    }
}
