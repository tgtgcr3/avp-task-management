package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.xmlbeans.XmlCursor;

import java.util.List;

public class UnorderListAttributeFormatter implements AttributeFormatter {

    @Override
    public boolean selfFormat() {
        return true;
    }

    @Override
    public void format(XWPFDocument document, XWPFParagraph paragraph, XWPFRun xwpfRun, Long requestId, Object value, Attribute attribute, Object extra) throws Exception {
        List<String> valueList = (List<String>) value;
        valueList.forEach(s -> {
            xwpfRun.addCarriageReturn();
            xwpfRun.setText("- " + s);
        });
    }
}
