package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;

@Data
@Entity(name = "Attribute")
@Audited
public class AttributeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String name;

    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    @OneToOne
    private AttributeTypeEntity attrType;

    private String fieldName;

    private int attrOrder;

    @Column(columnDefinition = "int default 0")
    private int required;

    private String additionData;

    @Column(columnDefinition = "text")
    private String description;

}
