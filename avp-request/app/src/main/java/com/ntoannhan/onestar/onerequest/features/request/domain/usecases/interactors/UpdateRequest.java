package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.UpdateRequestIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.UpdateRequestOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateRequest implements UseCase<UpdateRequestOut, UpdateRequestIn> {

    @Autowired
    private RequestService requestService;

    @Override
    public Either<Exception, UpdateRequestOut> call(UpdateRequestIn in) {
        return requestService.updateRequest(in.id(), in.request()).fold(Either::left, request -> Either.right(new UpdateRequestOut().request(request)));
    }
}
