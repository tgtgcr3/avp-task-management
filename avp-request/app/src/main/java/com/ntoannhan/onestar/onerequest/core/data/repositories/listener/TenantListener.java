package com.ntoannhan.onestar.onerequest.core.data.repositories.listener;

import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.TenantAware;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

public class TenantListener {

    @PreUpdate
    @PreRemove
    @PrePersist
    public void setTenant(TenantAware entity) {
        final String tenantId = TenantContext.getTenantId();
        entity.setTenantId(tenantId);
    }

}
