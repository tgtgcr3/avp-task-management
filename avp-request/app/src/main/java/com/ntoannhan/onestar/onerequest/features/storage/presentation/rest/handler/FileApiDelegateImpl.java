package com.ntoannhan.onestar.onerequest.features.storage.presentation.rest.handler;

import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfoVisibility;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in.DownloadFileIn;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in.UploadFileIn;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.interceptor.DownloadFile;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.interceptor.UploadFile;
import com.ntoannhan.onestar.onerequest.features.storage.presentation.rest.model.FileInfoModel;
import com.ntoannhan.onestar.onerequest.rest.api.FileApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.FileInfo;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.Locale;

@Log4j2
@Component
public class FileApiDelegateImpl implements FileApiDelegate {

    private final HttpServletRequest m_request;

    private final UploadFile uploadFile;

    private final DownloadFile downloadFile;

    public FileApiDelegateImpl(HttpServletRequest m_request, UploadFile uploadFile, DownloadFile downloadFile) {
        this.m_request = m_request;
        this.uploadFile = uploadFile;
        this.downloadFile = downloadFile;
    }

    @Override
    public ResponseEntity<Resource> download(String fileId) {
        if ("api.yml".equals(fileId)) {
            return downloadApi(fileId);
        }

        return downloadFile.call(new DownloadFileIn().fileId(Long.parseLong(fileId)).visibility(FileInfoVisibility.PUBLIC)).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> {
                    try {
                        return ResponseEntity.ok()
                                .contentType(MediaType.parseMediaType(Files.probeContentType(new File(out.fileName()).toPath()))).body(out.resource());
                    } catch (IOException ex) {
                        log.error("cannot guess the mediatype, return content without it", ex);
                        return ResponseEntity.ok().body(out.resource());
                    }
                }
        );

    }




    private ResponseEntity<Resource> downloadApi(String fileId) {
        Resource resource = new ClassPathResource("/" + fileId);
        if (!resource.exists()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "not found");
        }
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = m_request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @Override
    public ResponseEntity<FileInfo> upload(MultipartFile file, String visibility) {
                try {
            return uploadFile.call(new UploadFileIn().inputStream(file.getInputStream())
                    .fileName(file.getOriginalFilename())
                    .contentType(file.getContentType())
                    .visibility(getFileInfoStatus(visibility)))
                    .fold(
                            ex -> {
                                throw ExceptionTranslator.getFailureException(ex);
                            },
                            fileInfo -> ResponseEntity.ok(new FileInfoModel().toRestModel(fileInfo))
                    );
        } catch (IOException e) {
            log.error("cannot read the upload data", e);
            throw ExceptionTranslator.getFailureException(new UserException("cannot_read_upload_data"));
        }
    }

    private FileInfoVisibility getFileInfoStatus(String visibility) {
        if (visibility != null && "public".equals(visibility.toLowerCase(Locale.ROOT))) {
            return FileInfoVisibility.PUBLIC;
        }
        return FileInfoVisibility.PRIVATE;
    }

    //
//    @Override
//    public ResponseEntity<FileInfo> upload(MultipartFile file) {
//        try {
//            return uploadFile.call(new UploadFileIn().inputStream(file.getInputStream()).fileName(file.getOriginalFilename()).contentType(file.getContentType()))
//                    .fold(
//                            ex -> {
//                                throw ExceptionTranslator.getFailureException(ex);
//                            },
//                            fileInfo -> ResponseEntity.ok(new FileInfoModel().toRestModel(fileInfo))
//                    );
//        } catch (IOException e) {
//            log.error("cannot read the upload data", e);
//            throw ExceptionTranslator.getFailureException(new UserException("cannot_read_upload_data"));
//        }
//    }
}
