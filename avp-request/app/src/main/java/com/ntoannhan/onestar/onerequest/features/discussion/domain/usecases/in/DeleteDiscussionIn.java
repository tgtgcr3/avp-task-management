package com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class DeleteDiscussionIn {

    private long targetType;

    private long targetId;

    private long id;

}
