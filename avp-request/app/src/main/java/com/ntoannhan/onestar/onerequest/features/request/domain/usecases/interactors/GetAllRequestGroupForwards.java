package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupForwardService;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetAllRequestGroupForwards implements UseCase<List<RequestGroupForward>, Long> {

    private final RequestGroupForwardService requestGroupForwardService;

    public GetAllRequestGroupForwards(RequestGroupForwardService requestGroupForwardService) {
        this.requestGroupForwardService = requestGroupForwardService;
    }

    @Override
    public Either<Exception, List<RequestGroupForward>> call(Long in) {
        return requestGroupForwardService.getAll(in);
    }
}
