package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentEntity implements Serializable {

    private Long id;

    private String name;

    private List<String> members;

}
