package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestApprovalTypeService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetRequestApprovalTypesIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.model.RequestApprovalTypeModel;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetRequestApprovalTypesOut;

import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class GetRequestApprovalTypes implements UseCase<GetRequestApprovalTypesOut, GetRequestApprovalTypesIn> {

    @Autowired
    private RequestApprovalTypeService requestApprovalTypeService;

    @Override
    public Either<Exception, GetRequestApprovalTypesOut> call(GetRequestApprovalTypesIn in) {
        return requestApprovalTypeService.getAll().fold(Either::left, models -> Either.right(new GetRequestApprovalTypesOut()
                .total(models.size())
                .items(models.stream().map(RequestApprovalTypeModel::fromEntity).collect(Collectors.toList())))
        );
    }
}
