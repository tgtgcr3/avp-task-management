package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Request;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.RemoveRequestFollowersIn;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class RemoveRequestFollowers implements UseCase<Request, RemoveRequestFollowersIn> {

    private final RequestService requestService;

    public RemoveRequestFollowers(RequestService requestService) {
        this.requestService = requestService;
    }

    @Override
    public Either<Exception, Request> call(RemoveRequestFollowersIn in) {
        return requestService.updateFollowers(in.requestId(), in.followers(), false);
    }
}
