package com.ntoannhan.onestar.onerequest.core.config;

import com.ntoannhan.onestar.onerequest.core.interceptors.OneContextRequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    private final OneContextRequestInterceptor tenantInterceptor;

    @Autowired
    public WebConfiguration(OneContextRequestInterceptor tenantInterceptor) {
        this.tenantInterceptor = tenantInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addWebRequestInterceptor(this.tenantInterceptor);
    }
}
