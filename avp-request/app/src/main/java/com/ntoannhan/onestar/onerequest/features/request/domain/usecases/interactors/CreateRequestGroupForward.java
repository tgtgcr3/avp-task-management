package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupForwardService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.CreateRequestGroupForwardIn;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class CreateRequestGroupForward implements UseCase<RequestGroupForward, CreateRequestGroupForwardIn> {

    private final RequestGroupForwardService requestGroupForwardService;

    public CreateRequestGroupForward(RequestGroupForwardService requestGroupForwardService) {
        this.requestGroupForwardService = requestGroupForwardService;
    }

    @Override
    public Either<Exception, RequestGroupForward> call(CreateRequestGroupForwardIn in) {
        return requestGroupForwardService.createGroupForward(in.requestId(), in.requestGroupForward());
    }
}
