package com.ntoannhan.onestar.onerequest.features.notification.presentation.service;

import com.ntoannhan.onestar.onerequest.features.notification.presentation.data.NotificationType;
import io.vavr.control.Either;

import java.util.List;
import java.util.Map;

public interface NotificationService {

    Either<Exception, Long> registerTemplate(String appName, String category, NotificationType notificationType, String template);

    Either<Exception, Boolean> sendEmails(List<Map<String, Object>> emailDatas);

    Either<Exception, Boolean> sendNotification(Object data);


}
