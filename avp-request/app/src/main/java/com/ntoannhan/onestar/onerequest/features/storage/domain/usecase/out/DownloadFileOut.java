package com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.out;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.core.io.Resource;

@Data
@Accessors(fluent = true)
public class DownloadFileOut {

    private Resource resource;

    private String fileName;

}
