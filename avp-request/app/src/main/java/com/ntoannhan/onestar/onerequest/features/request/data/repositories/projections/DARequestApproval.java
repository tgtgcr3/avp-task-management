package com.ntoannhan.onestar.onerequest.features.request.data.repositories.projections;

public interface DARequestApproval {

    long getRequestId();

    int getDecision();

    String getRequestGroup();

    String getApprover();

    long getSubmittedTime();

    String getComment();

}
