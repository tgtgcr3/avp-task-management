package com.ntoannhan.onestar.onerequest.features.request.domain.services;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Category;
import io.vavr.control.Either;

public interface CategoryService {

    Either<Exception, Category> createCategory(String name);

}
