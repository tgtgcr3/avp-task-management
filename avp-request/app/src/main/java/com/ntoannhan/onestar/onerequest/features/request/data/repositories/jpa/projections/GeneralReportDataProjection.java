package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections;

public interface GeneralReportDataProjection {

    int getStatus();

    int getTotal();

}
