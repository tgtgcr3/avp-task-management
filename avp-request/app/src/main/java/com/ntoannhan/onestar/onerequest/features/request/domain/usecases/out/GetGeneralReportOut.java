package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.ActionReportData;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.GeneralReportData;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Map;

@Data
@Accessors(fluent = true)
public class GetGeneralReportOut {

    private GeneralReportData generalReportData;

    private Map<Long, ActionReportData> actionReportData;

}
