package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestForwardEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestForwardJpa extends JpaRepository<RequestForwardEntity, Long> {
}
