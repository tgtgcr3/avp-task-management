package com.ntoannhan.onestar.onerequest.core.aspect;

import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.hibernate.Session;

@Log4j2
@Aspect
//@Component
public class TenantFilterAspect {

    @Pointcut("execution (* org.hibernate.internal.SessionFactoryImpl.SessionBuilderImpl.openSession(..))")
    public void openSession() {
       log.debug("Open connection", new Exception(""));
    }

    @AfterReturning(pointcut = "openSession()", returning = "session")
    public void afterOpenSession(Object session) {
        log.debug("AfterOpenConnection");
        if (session instanceof Session) {
            final String tenantId = TenantContext.getTenantId();
            log.debug("Tenant Id: " + tenantId);
            if (tenantId != null) {
                org.hibernate.Filter filter = ((Session) session).enableFilter("tenantFilter");
                filter.setParameter("tenantId", tenantId);
            }
        }
    }
}
