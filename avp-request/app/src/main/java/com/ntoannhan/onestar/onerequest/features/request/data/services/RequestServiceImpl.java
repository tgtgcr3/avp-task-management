package com.ntoannhan.onestar.onerequest.features.request.data.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import com.ntoannhan.onestar.onerequest.core.data.entities.ResultGetAllData;
import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.exception.UncheckedUserException;
import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.core.utils.SecurityUtils;
import com.ntoannhan.onestar.onerequest.features.notification.presentation.service.NotificationService;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.*;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.*;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections.BasicRequestData;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.projections.DARequestApproval;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.services.KeycloakService;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.services.UserService;
import com.ntoannhan.onestar.onerequest.features.request.data.services.formatter.*;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.*;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfoVisibility;
import com.ntoannhan.onestar.onerequest.features.storage.presentation.service.impl.StorageService;
import com.ntoannhan.onestar.onerequest.interfaces.config.AppConfiguration;
import com.ntoannhan.onestar.onerequest.interfaces.config.RequestConfiguration;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHeaderFooter;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.hibernate.Hibernate;
import org.keycloak.representations.idm.UserRepresentation;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Log4j2
@Component
public class RequestServiceImpl implements RequestService {

    private final RequestGroupVersionJpa requestGroupVersionJpa;

    private final RequestJpa requestJpa;

    private final StorageService storageService;

    private final ModelMapper modelMapper;

    private final Map<Long, AttributeFormatter> attrFormatters;

    private final EntityManagerFactory factory;

    private final NotificationService notificationService;

    private final RequestApprovalJpa requestApprovalJpa;

    private final UserDataJpa userDataJpa;

    private final AppConfiguration appConfiguration;

    private final KeycloakService keycloakService;

//    private final UserService userService;

    private final UserRequestDataJpa userRequestDataJpa;

    private static final String CREATED_APPROVER_CONTENT = "[%s] đã tạo một đề xuất do bạn xét duyệt";

    private static final String CREATED_FOLLOWER_CONTENT = "[%s] đã tạo một đề xuất có bạn theo dõi";

    private static final String UPDATED_CONTENT = "Đề xuất <span class=\"request-name\">%s</span> đã được chỉnh sửa";

    private static final String APPROVED_CONTENT = "Đề xuất <span class=\"request-name\">%s</span> đã được chấp thuận bởi [%s]";

    private static final String FWD_CONTENT = "[%s] đã chuyển tiếp dề xuất <span class=\"request-name\">%s</span> cho bạn";

    private static final String REQUEST_APPROVED_CONTENT = "Đề xuất <span class=\"request-name\">%s</span> đã được chấp thuận";

    private static final String REJECTED_CONTENT = "Đề xuất <span class=\"request-name\">%s</span> đã bị từ chối bởi [%s]";

    private static final String REQUEST_REJECTED_CONTENT = "Đề xuất <span class=\"request-name\">%s</span> đã bị từ chối";

    private static final String REQUEST_CANCELLED_CONTENT = "Đề xuất <span class=\"request-name\">%s</span> đã bị hủy bởi [%s] ";

    private static final String TIMEOUT_CONTENT = "Đề xuất <span class=\"request-name\">%s</span> đã quá hạn xử lý";

    private static final int USER_DATA_REQUEST_FAVORITE = 1;

    private static final long ONE_HOUR = 3600000;

    private static final long ONE_DAY = 24 * ONE_HOUR;

    private static final long EIGHT_HOUR = 8 * ONE_HOUR;

    @Autowired
    public RequestServiceImpl(RequestGroupVersionJpa requestGroupVersionJpa, RequestJpa requestJpa, StorageService storageService, ModelMapper modelMapper, EntityManagerFactory factory, NotificationService notificationService, RequestApprovalJpa requestApprovalJpa, UserDataJpa userDataJpa, AppConfiguration appConfiguration, KeycloakService keycloakService, UserService userService, UserRequestDataJpa userRequestDataJpa) {
        this.requestGroupVersionJpa = requestGroupVersionJpa;
        this.requestJpa = requestJpa;
        this.storageService = storageService;
        this.modelMapper = modelMapper;
        this.factory = factory;
        this.notificationService = notificationService;
        this.requestApprovalJpa = requestApprovalJpa;
        this.userDataJpa = userDataJpa;
        this.appConfiguration = appConfiguration;
        this.keycloakService = keycloakService;
        this.userRequestDataJpa = userRequestDataJpa;
//        this.userService = userService;

        attrFormatters = new ConcurrentHashMap<>();
        attrFormatters.put(0L, new StringAttributeFormatter());
        attrFormatters.put(1L, new StringAttributeFormatter());
        attrFormatters.put(2L, new MultipleLineStringAttributeFormat());
        attrFormatters.put(3L, new DateTimeAttributeFormatter("dd/MM/yyyy"));
        attrFormatters.put(4L, new DateTimeAttributeFormatter("HH:mm dd/MM/yyyy"));
        attrFormatters.put(5L, new FileAttributeFormatter(storageService));
        attrFormatters.put(6L, new SelectAttributeFormatter());
        attrFormatters.put(7L, new MultiSelectAttributeFormatter());
        attrFormatters.put(8L, new TableAttributeFormatter());
        attrFormatters.put(9L, new StringAttributeFormatter());
        attrFormatters.put(10L, new MultipleDateTimeAttributeFormat(appConfiguration));
        attrFormatters.put(100L, new DateTimeAttributeFormatter("HH:mm"));
        attrFormatters.put(99L, new UnorderListAttributeFormatter());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 6);
        calendar.set(Calendar.MONTH, Calendar.JULY);
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long now = calendar.getTimeInMillis();
//        long timeoutFromSubmitTime = getTimeoutFromSubmitTime(now, 4 * ONE_HOUR);
//        log.error("---------------: {}", timeoutFromSubmitTime);
//        calendar.set(Calendar.HOUR_OF_DAY, 13);
//        calendar.add(Calendar.MINUTE, 1);
//        long remaining = calculateRemainingTime(calendar.getTimeInMillis(), timeoutFromSubmitTime);
//        log.error("---------------: {}", remaining);

//        calendar.add();
    }


    @PostConstruct
    private void postConstruct() {
        modelMapper.addMappings(new EntityToDomainRequestMapping());
        modelMapper.addMappings(new DomainToEntityRequestMapping());
    }

    @Transactional
    @Override
    public Either<Exception, Request> createRequest(Request request) {
        log.info("----------------- Current user: {}", TenantContext.getUsername());
        return convertRequestDomainToEntity(request).fold(Either::left, entity -> {
            try {
                entity.setStatus(RequestStatus.PENDING.getValue());
//                String currentUser = TenantContext.getUsername();

                if (entity.getRequestGroupVersionEntity().getRequestGroup().getMgmtNotification() == 1) {
                    log.debug("Request {} required DirectMgmgt {}", request.getId(), request.getDirectMgmts());
                    if (request.getDirectMgmts() == null || request.getDirectMgmts().length == 0) {
                        log.error("Required DirectMgmgt to create Request, DirectMgmt: {}", request.getDirectMgmts());
                        return Either.left(new UserException("DirectMgmgt is missing"));
                    }
                    entity.oneSetDirectMgmts(request.getDirectMgmts());
//                    List<UserData> allUser = userService.getAllUser();
//                    if (allUser == null || allUser.size() == 0) {
//                        userService.clearEverything();;
//                        allUser = userService.getAllUser();
//                    }
//                    Optional<UserData> userDataOpt = allUser.stream().filter(u -> currentUser.equals(u.getUsername())).findAny();
//                    if (userDataOpt.isPresent()) {
//                        // Work-arround for account bug
//                        String directManagers = userDataOpt.get().getDirectMgmts();
//                        log.debug("Direct Mgmt for user {}: {}", currentUser, directManagers);
//
//                        if (directManagers != null && directManagers.trim().length() > 0) {
//                            List<UserRepresentation> allKeycloakUsers = keycloakService.getAllUsers();
//
//                            log.debug("Number of user in UserMgmt: {}", allKeycloakUsers.size());
//                            entity.oneSetDirectMgmts(Arrays.stream(directManagers.split(",")).filter(s -> {
//                                if (s.trim().isEmpty()) {
//                                    return false;
//                                }
//                                Optional<UserRepresentation> enabledUsers = allKeycloakUsers.stream().filter(u -> s.equals(u.getUsername()) && u.isEnabled()).findAny();
//                                return enabledUsers.isPresent();
//                            }).toArray(String[]::new));
//                        }
//                        log.debug("DirectMgmgt for new request: {}", entity.getDirectMgmts());
//                    } else {
//                        log.debug("Cannot find current user in UserMgmt ????");
//                    }
                } else {
                    entity.setDirectMgmts("");
                }
                if (entity.getRequestGroupVersionEntity().getId() != 0) {
                    entity.oneSetApprovers(new String[0]);
                } else {
                    entity.oneSetApprovers(request.getApprovers());
                }

                entity.oneSetFollowers(request.getFollowers());

                entity.setSubmitter(TenantContext.getUsername());
                entity.setSubmitTime(System.currentTimeMillis());
                entity.setLastUpdated(System.currentTimeMillis());
                entity.oneSetAttrDocuments(request.getAttrDocuments());
                entity.oneSetDocuments(request.getDocuments());
                requestJpa.save(entity);
                return sendCreatedNotification(request, entity).fold(ex -> {
                    log.error("cannot send notification", ex);
                    return Either.right(convertEntityToDomain(entity));
                }, right -> {
                    return Either.right(convertEntityToDomain(entity));
                });

            } catch (Exception ex) {
                log.error("Cannot create new request", ex);
                return Either.left(new AppException("cannot_save_data"));
            }
        });
    }

    @Override
    public Either<Exception, Request> getRequest(long id) {
        //TODO: verify following data
        // - Department
        // - User role
        try {
            Optional<RequestEntity> requestOpt = requestJpa.findById(id);
            if (requestOpt.isEmpty()) {
                return Either.left(new UserException("request_not_found"));
            }
            RequestEntity requestEntity = requestOpt.get();
            if (requestEntity.getStatus() == RequestStatus.DELETED.getValue()) {
                return Either.left(new UserException("request_deleted"));
            }

            List<Long> visistedStatus = userRequestDataJpa.getAllVisitedRequests(TenantContext.getUsername(), Arrays.asList(id));
            Request request = convertEntityToDomain(requestEntity);
            try {
                List<DARequestApproval> approvalRecords = requestApprovalJpa.getRequestApprovalRecords(Arrays.asList(id));
                List<String> approveds = new ArrayList<>();
                List<String> rejecteds = new ArrayList<>();
                List<String> cancelled = new ArrayList<>();
                approvalRecords.forEach(decision -> {
                    if (decision.getDecision() == RequestStatus.APPROVED.getValue() || decision.getDecision() == RequestStatus.AGREED_FORWARD.getValue()) {
                        approveds.add(decision.getApprover());
                    } else if (decision.getDecision() == RequestStatus.REJECTED.getValue()) {
                        rejecteds.add(decision.getApprover());
                    } else if (decision.getDecision() != RequestStatus.FORWARD.getValue() && decision.getDecision() != RequestStatus.TRANSFER.getValue()){
                        cancelled.add(decision.getApprover());
                    }
                });
                request.setRejecteds(rejecteds.toArray(new String[0]));
                request.setApproveds(approveds.toArray(new String[0]));
                request.setCancelleds(cancelled.toArray(new String[0]));
                request.setHasRequestForm(requestEntity.getRequestGroupVersionEntity().getRequestForm() == 1);
                long now = Calendar.getInstance().getTimeInMillis();
                log.debug("current time in mill: {} - {}", now, requestEntity.getTimeout());
                request.remainingTime(calculateRemainingTime(now, requestEntity.getTimeout()));

                RequestGroupEntity oldRequestGroup = requestEntity.getOldRequestGroup();
                if (oldRequestGroup != null) {
                    request.setOgId(oldRequestGroup.getId());
                    request.setOgName(oldRequestGroup.getName());
                }
                request.setVisited(!visistedStatus.isEmpty());
            } catch (Exception ex) {
                log.error("Cannot get request status", ex);
            }
            return Either.right(request);
        } catch (Exception ex) {
            log.error("Cannot retrieve data from database", ex);
            return Either.left(new AppException("cannot_retrieve_data"));
        }

    }



    public long calculateRemainingTime(Long now, Long timeout) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(now);

        if (timeout == 0 || now >= timeout) {
            return 0;
        }

        RequestConfiguration requestConfiguration = appConfiguration.getRequest();
        long endTime = requestConfiguration.getEndTime();
        long morningTime = requestConfiguration.getMorningTime();
        long lunchTime = requestConfiguration.getLunchTime();
        long noonTime = requestConfiguration.getNoonTime();
        long endTimeMargin = requestConfiguration.getEndTimeMargin();
        long workingDay = requestConfiguration.getWorkingDay();

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);


        Calendar checkTime = Calendar.getInstance();
        checkTime.set(Calendar.DAY_OF_MONTH, day);
        checkTime.set(Calendar.MONTH, month);
        checkTime.set(Calendar.YEAR, year);
        checkTime.set(Calendar.HOUR_OF_DAY, 0);
        checkTime.set(Calendar.MINUTE, 0);
        checkTime.set(Calendar.SECOND, 0);
        checkTime.set(Calendar.MILLISECOND, 0);

        long today = checkTime.getTimeInMillis();
        checkTime.add(Calendar.DAY_OF_MONTH, 1);
        long nextDay = checkTime.getTimeInMillis();
        if (today + endTime <= now) {
            now = nextDay + morningTime;
            today = nextDay;
            checkTime.add(Calendar.DAY_OF_MONTH, 1);
            nextDay = checkTime.getTimeInMillis();
        } else if (now <= today + morningTime) {
            now = today + morningTime;
        }

        if (today + lunchTime <= now && now < today + noonTime) {
            now = today + noonTime;
        }

        if (workingDay == 1) {
            if ((checkTime.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && now >= today + lunchTime)
            ) {
                checkTime.add(Calendar.DAY_OF_MONTH, 2);
                today = checkTime.getTimeInMillis();
                checkTime.add(Calendar.DAY_OF_MONTH, 1);
                nextDay = checkTime.getTimeInMillis();
                now = today + morningTime;
            } else if (checkTime.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                today = checkTime.getTimeInMillis();
                checkTime.add(Calendar.DAY_OF_WEEK, 1);
                nextDay = checkTime.getTimeInMillis();
                now = today + morningTime;
            }
        }

        long remainingTime = 0;
        do {
            now += ONE_HOUR;
            remainingTime += ONE_HOUR;

            if (today + lunchTime < now && now <= today + noonTime) {
                remainingTime -= (now - today - lunchTime);
                now = today + lunchTime;
                if (now < timeout) {
                    now = today + noonTime;
                }

                if (checkTime.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                    checkTime.add(Calendar.DAY_OF_MONTH, 1);
                    nextDay = checkTime.getTimeInMillis();
                    today = nextDay;
                    now = today + morningTime;
                    checkTime.add(Calendar.DAY_OF_MONTH, 1);
                    nextDay = checkTime.getTimeInMillis();
                }
            } else if (today + endTime <= now) {
                remainingTime -= (now - today - endTime);
                now = today + endTime;

                today = nextDay;
                checkTime.add(Calendar.DAY_OF_MONTH, 1);
                nextDay = checkTime.getTimeInMillis();
                if (now < timeout) {
                    now = today + morningTime;
                }
            }
        } while (now < timeout);


        if (now > timeout) {
            remainingTime -= (now - timeout);
        }

        return remainingTime;
    }

    @Override
    public Either<Exception, Request> updateRequest(long id, Request request) {
        try {
            Optional<RequestEntity> requestEntityOpt = requestJpa.findById(id);
            if (requestEntityOpt.isPresent()) {
                RequestEntity requestEntity = requestEntityOpt.get();
                request.setId(id);
                request.setSubmitTime(requestEntity.getSubmitTime());
                request.setLastUpdated(System.currentTimeMillis());
                String submitter = requestEntity.getSubmitter();
                String directMgmts = requestEntity.getDirectMgmts();
                String approvers = requestEntity.getApprovers();
                String followers = requestEntity.getFollowers();
                int status = requestEntity.getStatus();
                long timeout = requestEntity.getTimeout();
                OneMapper.mapTo(request, requestEntity);
                requestEntity.setFollowers(followers);
                requestEntity.setApprovers(approvers);
                requestEntity.setStatus(status);
                requestEntity.setSubmitter(submitter);
                requestEntity.setDirectMgmts(directMgmts);
                requestEntity.setTimeout(timeout);
                requestEntity.oneSetDirectMgmts(request.directMgmts());
                requestEntity.oneSetFollowers(request.followers());
                requestEntity.oneSetDocuments(request.getDocuments());
                requestEntity.oneSetAttrDocuments(request.getAttrDocuments());
                requestEntity.oneSetTaskIds(request.getTaskIds());
                requestJpa.save(requestEntity);
                return Either.right(convertEntityToDomain(requestEntity));
            }
            return Either.left(new UserException("request_not_found"));
        } catch (Exception ex) {
            log.error("Cannot update request id: " + id, ex);
            return Either.left(new AppException("cannot_update_request"));
        }
    }

    @Override
    public Either<Exception, ResultGetAllData<Request>> getAll(String search, int page, int size, boolean fullData, Long requestGroupId,
                                                               Boolean toMe, Boolean fromMe, Boolean followed, RequestStatus requestStatus, Boolean favorite,
                                                               List<String> creators, Long createdDate, Long end
                                                               ) {
        if (search == null) {
            search = "";
        }
        try {
            String aUsername = "," + TenantContext.getUsername() + ",";
//            if (toMe == Boolean.FALSE) {
//                toMe = null;
//            }
//            if (fromMe == Boolean.FALSE) {
//                fromMe = null;
//            }
//            if (followed == Boolean.FALSE) {
//                followed = null;
//            }
//            if (favorite == null) {
//                favorite = false;
//            }
            if (!SecurityUtils.hasRole("owner")) {
                if (toMe == null && fromMe == null && followed == null) {
                    toMe = true;
                    fromMe = true;
                    followed = true;
                }
            }

            if (createdDate != null && end == null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(createdDate);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                createdDate = calendar.getTimeInMillis();
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                end = calendar.getTimeInMillis();
            }
            List<Long> requestFavorites = getRequestFavorites();
            int total = requestJpa.countAllWithBasicData(search, requestGroupId, TenantContext.getUsername(),
                    aUsername, toMe, fromMe, followed, requestStatus != null ? requestStatus.getValue() : null, creators, createdDate, end);
            if (fullData) {
                List<RequestEntity> requestEntities = null;
                if (favorite == Boolean.TRUE) {
                    total = requestFavorites.size();
                    requestEntities = requestJpa.findAllById(requestFavorites);
                } else {
                    requestEntities = requestJpa.findAllWithFullData(search, requestGroupId, TenantContext.getUsername(), aUsername, toMe, fromMe, followed, requestStatus != null ? requestStatus.getValue() : null,
                            creators, createdDate, end,
                            PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "submitTime")));
                }
                List<Long> requestIds = requestEntities.stream().map(RequestEntity::getId).collect(Collectors.toList());
                List<DARequestApproval> requestApprovalRecords = requestApprovalJpa.getRequestApprovalRecords(requestIds);
                Map<Long, List<String>> approvedMaps = new HashMap<>();
                Map<Long, List<String>> rejectedMaps = new HashMap<>();
                requestApprovalRecords.forEach(a -> {
                    if (a.getDecision() == RequestStatus.APPROVED.getValue()) {
                        List<String> strings = approvedMaps.computeIfAbsent(a.getRequestId(), k -> new ArrayList<>());
                        strings.add(a.getApprover());
                    } else if (a.getDecision() == RequestStatus.REJECTED.getValue()) {
                        List<String> strings = rejectedMaps.computeIfAbsent(a.getRequestId(), k -> new ArrayList<>());
                        strings.add(a.getApprover());
                    }
                });
                List<Long> visistedRequestIds = userRequestDataJpa.getAllVisitedRequests(TenantContext.getUsername(), requestIds);
                return Either.right(new ResultGetAllData<Request>().total(total).items(requestEntities.stream()
                        .map(basicRequestData -> {
                            Request request = convertEntityToDomain(basicRequestData);
                            request.setApprovers(getAllApprovers(basicRequestData).toArray(new String[0]));
                            request.setApproveds(approvedMaps.getOrDefault(request.getId(), new ArrayList<>()).toArray(new String[0]));
                            request.setRejecteds(rejectedMaps.getOrDefault(request.getId(), new ArrayList<>()).toArray(new String[0]));
                            request.setVisited(visistedRequestIds.contains(request.getId()));
                            return request;
                        }).collect(Collectors.toList())));
            } else {
                if (favorite != null && favorite == true) {
                    total = requestFavorites.size();
                    List<RequestEntity> requestEntities = requestJpa.findAllById(requestFavorites);
                    return Either.right(new ResultGetAllData<Request>().total(total).items(requestEntities.stream()
                            .map(basicRequestData -> {
                                Request request = new Request();
                                request.id(basicRequestData.getId());
                                request.name(basicRequestData.getName());
                                request.status(RequestStatus.fromId(basicRequestData.getStatus()));
                                request.submitter(basicRequestData.getSubmitter());
                                request.setStatus(RequestStatus.fromId(basicRequestData.getStatus()));
                                request.setApprovers(basicRequestData.getRequestGroupVersionEntity().oneGetApprovers());
                                request.setFavorite(true);
                                return request;
                            }).collect(Collectors.toList())));
                } else {
                    List<BasicRequestData> requestEntities = requestJpa.findAllWithBasicData(search, requestGroupId, TenantContext.getUsername(), aUsername, toMe, fromMe, followed, requestStatus != null ? requestStatus.getValue() : null,
                            creators, createdDate, end,
                            PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "submitTime")));

                    List<Long> requestIds = requestEntities.stream().map(BasicRequestData::getId).collect(Collectors.toList());
                    List<DARequestApproval> requestApprovalRecords = requestApprovalJpa.getRequestApprovalRecords(requestIds);
                    Map<Long, List<String>> approvedMaps = new HashMap<>();
                    Map<Long, List<String>> rejectedMaps = new HashMap<>();
                    requestApprovalRecords.forEach(a -> {
                        if (a.getDecision() == RequestStatus.APPROVED.getValue()) {
                            List<String> strings = approvedMaps.computeIfAbsent(a.getRequestId(), k -> new ArrayList<>());
                            strings.add(a.getApprover());
                        } else if (a.getDecision() == RequestStatus.REJECTED.getValue()) {
                            List<String> strings = rejectedMaps.computeIfAbsent(a.getRequestId(), k -> new ArrayList<>());
                            strings.add(a.getApprover());
                        }
                    });
                    List<Long> visistedRequestIds = userRequestDataJpa.getAllVisitedRequests(TenantContext.getUsername(), requestIds);
                    List<Request> requests = requestEntities.stream()
                            .map(basicRequestData -> {
                                Request request = OneMapper.mapTo(basicRequestData, Request.class);
                                request.setStatus(RequestStatus.fromId(basicRequestData.getStatus()));
                                request.setApprovers(basicRequestData.oneGetApprovers());
                                request.setFavorite(requestFavorites.contains(request.getId()));
                                request.setApproveds(approvedMaps.getOrDefault(request.getId(), new ArrayList<>()).toArray(new String[0]));
                                request.setDirectMgmts(basicRequestData.oneGetDirectMgmts());
                                request.setRejecteds(rejectedMaps.getOrDefault(request.getId(), new ArrayList<>()).toArray(new String[0]));
                                request.setVisited(visistedRequestIds.contains(request.getId()));
                                return request;
                            }).collect(Collectors.toList());

                    return Either.right(new ResultGetAllData<Request>().total(total).items(requests));
                }

            }
        } catch (Exception ex) {
            log.error("Cannot retrieve data", ex);
            return Either.left(new AppException("cannot_retrieve_Data"));
        }
    }

    @Override
    public Either<Exception, Resource> downloadAttachment(long requestId, long fileId) {
        Optional<RequestEntity> requestOpt = requestJpa.findById(requestId);
        if (requestOpt.isPresent()) {
            RequestEntity requestEntity = requestOpt.get();
            //permission???
            String fileIdStr = String.valueOf(fileId);
            List<String> attachments = new ArrayList<>();
            attachments.addAll(Arrays.asList(requestEntity.oneGetDocuments()));
            attachments.addAll(Arrays.asList(requestEntity.oneGetAttrDocuments()));
            if (attachments.contains(fileIdStr)) {
                return storageService.loadFile(fileId, FileInfoVisibility.PRIVATE);
            }
            return Either.left(new UserException("attachment_not_found"));
        }
        log.error("Request not found", new Exception());
        return Either.left(new UserException("attachment_not_found"));
    }

    private Either<Exception, RequestEntity> convertRequestDomainToEntity(Request request) {
        try {
            Optional<RequestGroupVersionEntity> requestGroupOpt = requestGroupVersionJpa.findById(request.requestGroupId());
            if (requestGroupOpt.isEmpty()) {
                throw new UncheckedUserException("request_group_not_found");
            }
            RequestEntity requestEntity = OneMapper.mapTo(request, RequestEntity.class);
            requestEntity.setRequestGroupVersionEntity(requestGroupOpt.get());
            requestEntity.setStatus(request.status() != null ? request.status().getValue() : 0);
            requestEntity.oneSetDocuments(request.getDocuments());
            if (requestGroupOpt.get().getSla() != null) {
                long now = Calendar.getInstance().getTimeInMillis();
                requestEntity.setTimeout(getTimeoutFromSubmitTime(now, requestGroupOpt.get().getSla()));
            }
           // if (requestEntity.getRequestGroupVersionEntity().getId() == 0) {
                requestEntity.oneSetFollowers(request.getFollowers());
                requestEntity.oneSetApprovers(request.getApprovers());
          //  } else {
          //      requestEntity.setApprovers(null);
          //      requestEntity.setFollowers(null);
          //  }
            requestEntity.oneSetTaskIds(request.getTaskIds());
            return Either.right(requestEntity);
        }catch (Exception ex) {
            log.error("Cannot convert Request domain to Entity", ex);
            if (ex instanceof UncheckedUserException) {
                return Either.left(new UserException(ex.getMessage()));
            }
            return Either.left(new AppException("cannot_convert_data"));
        }
    }

    public long getTimeoutFromSubmitTime(long submitTime, long sla) {
        if (sla == 0) {
            return 0;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(submitTime);

        RequestConfiguration requestConfiguration = appConfiguration.getRequest();
        long endTime = requestConfiguration.getEndTime();
        long morningTime = requestConfiguration.getMorningTime();
        long lunchTime = requestConfiguration.getLunchTime();
        long noonTime = requestConfiguration.getNoonTime();
        long endTimeMargin = requestConfiguration.getEndTimeMargin();
        long workingDay = requestConfiguration.getWorkingDay();

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);


        Calendar checkTime = Calendar.getInstance();
        checkTime.set(Calendar.DAY_OF_MONTH, day);
        checkTime.set(Calendar.MONTH, month);
        checkTime.set(Calendar.YEAR, year);
        checkTime.set(Calendar.HOUR_OF_DAY, 0);
        checkTime.set(Calendar.MINUTE, 0);
        checkTime.set(Calendar.SECOND, 0);
        checkTime.set(Calendar.MILLISECOND, 0);
        long today = checkTime.getTimeInMillis();
        checkTime.add(Calendar.DAY_OF_MONTH, 1);
        long nextDay = checkTime.getTimeInMillis();

        if (today + endTime <= submitTime) {
            submitTime= nextDay + morningTime;
            today = nextDay;
            checkTime.add(Calendar.DAY_OF_MONTH, 1);
            nextDay = checkTime.getTimeInMillis();
        } else if (submitTime <= today + morningTime) {
            submitTime = today + morningTime;
        }

        if (today + lunchTime <= submitTime && submitTime < today + noonTime) {
            submitTime = today + noonTime;
        }

        if (workingDay == 1) {
            if ((checkTime.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && submitTime >= checkTime.getTimeInMillis() + lunchTime)
            ) {
                checkTime.add(Calendar.DAY_OF_MONTH, 2);
                today = checkTime.getTimeInMillis();
                checkTime.add(Calendar.DAY_OF_MONTH, 1);
                nextDay = checkTime.getTimeInMillis();
                submitTime = today + morningTime;
            } else if (checkTime.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                today = checkTime.getTimeInMillis();
                checkTime.add(Calendar.DAY_OF_WEEK, 1);
                nextDay = checkTime.getTimeInMillis();
                submitTime = today + morningTime;
            }
        }


        do {
            sla -= ONE_HOUR;
            submitTime += ONE_HOUR;

            if (today + lunchTime < submitTime && submitTime <= today + noonTime) {
                sla += (submitTime - today - lunchTime);
                submitTime = today + noonTime;

                if (checkTime.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                    checkTime.add(Calendar.DAY_OF_MONTH, 1);
                    nextDay = checkTime.getTimeInMillis();
                    today = nextDay;
                    submitTime = today + morningTime;
                    checkTime.add(Calendar.DAY_OF_MONTH, 1);
                    nextDay = checkTime.getTimeInMillis();
                }
            } else if (today + endTime <= submitTime) {
              //  timeout -= (now - today - endTime);
                sla += (submitTime - today - endTime);
                submitTime = today + endTime;
                today = nextDay;
                checkTime.add(Calendar.DAY_OF_MONTH, 1);
                nextDay = checkTime.getTimeInMillis();
                if (sla > 0) {
                    submitTime = today + morningTime;
                }
            }
        } while (sla > 0);

        submitTime += sla;

        return submitTime;
    }

    private Request convertEntityToDomain(RequestEntity entity) {
        Request requestResult = OneMapper.mapTo(entity, Request.class);
        requestResult.requestGroupId(entity.getRequestGroupVersionEntity().getId());
        requestResult.status(RequestStatus.fromId(entity.getStatus()));
        requestResult.requestGroupName(entity.getRequestGroupVersionEntity().getRequestGroup().getName());

//        if (entity.getRequestGroupVersionEntity().getId() > 0) {
//            requestResult.setApprovers(entity.getRequestGroupVersionEntity().oneGetApprovers());
//            requestResult.setFollowers(entity.getRequestGroupVersionEntity().oneGetFollowers());
//        } else {
//            requestResult.setApprovers(entity.oneGetApprovers());
//            requestResult.setFollowers(entity.oneGetFollowers());
//        }
        Set<String> followers = new LinkedHashSet<>();
        followers.addAll(Arrays.asList(entity.getRequestGroupVersionEntity().oneGetFollowers()));
        followers.addAll(Arrays.asList(entity.oneGetFollowers()));
        requestResult.setApprovers(getAllApprovers(entity).toArray(new String[0]));
        requestResult.setFollowers(followers.toArray(new String[0]));
        requestResult.setDirectMgmts(entity.oneGetDirectMgmts());
        requestResult.setDocuments(entity.oneGetDocuments());
        requestResult.setTaskIds(entity.oneGetTaskIds());
        return requestResult;
    }

    @Override
    public Either<Exception, Resource> exportRequestForm(long requestId) {
        try {
            Optional<RequestEntity> requestOpt = requestJpa.findById(requestId);
            if (requestOpt.isPresent()) {
                RequestEntity request = requestOpt.get();
                if (request.getRequestGroupVersionEntity().getRequestForm() == 1 && request.getRequestGroupVersionEntity().getRequestForms() != null) {
                    String[] requestForms = request.getRequestGroupVersionEntity().oneGetRequestForms();
                    if (requestForms.length > 0) {
                        String fileIdStr = requestForms[0];
                        return storageService.loadFile(Long.parseLong(fileIdStr), FileInfoVisibility.PRIVATE).fold(
                                Either::left,
                                fileResource -> exportDataToRequestForm(request, fileResource, keycloakService.getAllUsers())
                        );
                    }
                    return Either.left(new UserException("request_form_not_found"));
                }
            }
            return Either.left(new UserException("request_not_found"));
        } catch (Exception ex) {
            log.error("cannot export data", ex);
            return Either.left(new AppException("cannot_process_data"));
        }
    }

    @Override
    public Either<Exception, List<RequestActivity>> getRequestActivities(long id) {
        try {
            List<DARequestApproval> requestApprovalRecords = requestApprovalJpa.getRequestApprovalRecords(Arrays.asList(id));
            return Either.right(requestApprovalRecords.stream().map(a -> new RequestActivity()
                    .date(a.getSubmittedTime())
                    .description(a.getComment())
                    .action(RequestStatus.fromId(a.getDecision()))
                    .owner(a.getApprover())
            ).collect(Collectors.toList()));
        } catch (Exception ex) {
            log.error("Cannot g");
        }


        return Either.right(new ArrayList<>());

    }

    @Override
    public Either<Exception, List<FileInfo>> getAttachmentFileInfos(long id, List<Long> fileIds) {
        try {
            Optional<RequestEntity> requestOpt = requestJpa.findById(id);
            if (requestOpt.isPresent()) {
                List<String> attachments = new ArrayList<>();
                attachments.addAll(Arrays.asList(requestOpt.get().oneGetAttrDocuments()));
                attachments.addAll(Arrays.asList(requestOpt.get().oneGetDocuments()));
                List<Long> docIds = attachments.stream().map(Long::parseLong).filter(fileIds::contains).collect(Collectors.toList());
                return storageService.getFileInfos(docIds);
            }
            return Either.left(new UserException("request_not_found"));
        } catch (Exception ex) {
            log.error("cannot retrive attachment", ex);
            return Either.left(new AppException("cannot_process_request"));
        }
    }

    private List<Long> getRequestFavorites() {
        String currentUser = TenantContext.getUsername();
        UserDataId userDataId = new UserDataId(currentUser, "request", USER_DATA_REQUEST_FAVORITE);
        Optional<UserDataEntity> userDataEntityOpt = userDataJpa.findById(userDataId);
        return userDataEntityOpt.map(userDataEntity -> Arrays.stream(userDataEntity.oneGetData()).map(Long::parseLong).collect(Collectors.toList())).orElse(Collections.emptyList());
    }

    @Override
    public Either<Exception, Boolean> setFavorite(long id, boolean added) {
        try {
            String currentUser = TenantContext.getUsername();
            UserDataId userDataId = new UserDataId(currentUser, "request", USER_DATA_REQUEST_FAVORITE);
            Optional<UserDataEntity> userDataEntityOpt = userDataJpa.findById(userDataId);
            UserDataEntity userDataEntity = null;
            if (userDataEntityOpt.isPresent()) {
                userDataEntity = userDataEntityOpt.get();
            } else {
                userDataEntity = new UserDataEntity();
                userDataEntity.oneSetId(userDataId);
            }
            List<String> requestFavorites = new ArrayList<>(Arrays.asList(userDataEntity.oneGetData()));
            String idStr = String.valueOf(id);
            if (added && !requestFavorites.contains(idStr)) {
                requestFavorites.add(String.valueOf(id));
            } else if (!added) {
                requestFavorites.remove(idStr);
            }
            userDataEntity.oneSetData(requestFavorites);
            userDataJpa.save(userDataEntity);
            return Either.right(Boolean.TRUE);
        } catch (Exception ex) {
            log.error("Cannot store RequestFavorite", ex);
            return Either.left(new AppException("cannot_process_request"));
        }
    }

    @Transactional
    @Override
    public Either<Exception, Request> updateFollowers(long id, List<String> followers, boolean tobeAdded) {
        try {
            Optional<RequestEntity> requestOpt = requestJpa.findById(id);
            log.info("update Request Followers for {}", id);
            if (requestOpt.isPresent()) {
                RequestEntity requestEntity = requestOpt.get();
                Set<String> currentFollowers = new LinkedHashSet<>(Arrays.asList(requestEntity.oneGetFollowers()));
                if (tobeAdded) {
                    followers.removeAll(currentFollowers);
                    currentFollowers.addAll(followers);
                } else {
                    followers.forEach(currentFollowers::remove);
                }
                requestEntity.oneSetFollowers(currentFollowers.toArray(new String[0]));
                requestJpa.save(requestEntity);
                return Either.right(convertEntityToDomain(requestEntity));
            }
            return Either.left(new UserException("request_not_found"));
        } catch (Exception ex) {
            log.error("Cannot update Request followers for {}", id, ex);
        }

        return Either.left(new AppException("cannot_update_request_followers"));
    }

    @Transactional
    @Override
    public Either<Exception, Boolean> removeRequest(long id) {
        try {
            Optional<RequestEntity> requestOpt = requestJpa.findById(id);
            log.info("delete Request {}", id);
            if (requestOpt.isPresent()) {
                RequestEntity requestEntity = requestOpt.get();
                requestEntity.setStatus(RequestStatus.DELETED.getValue());
                requestJpa.save(requestEntity);
                return Either.right(Boolean.TRUE);
            }
            return Either.left(new UserException("request_not_found"));
        } catch (Exception ex) {
            log.error("Cannot delete Request {}", id, ex);
        }

        return Either.left(new AppException("cannot_delete_request_followers"));
    }

    @Override
    public List<String> getAllApprovers(RequestEntity requestEntity) {
        final List<String> approvers = new LinkedList<>();
        approvers.addAll(Arrays.asList(requestEntity.oneGetDirectMgmts()));
        List<String> rApprovers = new LinkedList<>(Arrays.asList(requestEntity.oneGetApprovers()));
        rApprovers.removeAll(approvers);
        approvers.addAll(rApprovers);

        approvers.addAll(Arrays.asList(requestEntity.getRequestGroupVersionEntity().oneGetApprovers()));

        Hibernate.initialize(requestEntity.getForwards());
        List<RequestForwardEntity> forwards = requestEntity.getForwards();
        if (forwards != null) {
            for (RequestForwardEntity fwd : forwards) {
                String owner = fwd.getOwner();

                Integer forwardType = fwd.getForwardType();
                int i = approvers.indexOf(owner);
                approvers.addAll(forwardType == RequestStatus.FORWARD.getValue() ? i : i + 1, fwd.oneGetTargetUsers());
            }
        }

        return approvers;
    }

    @Override
    public Either<Exception, Map<Long, RequestApproval>> getRequestApprovalInfos(List<Long> ids) {
        List<DARequestApproval> approvalRecords = requestApprovalJpa.getRequestApprovalRecords(ids);
        Map<Long, RequestApproval> result = new HashMap<>();
        approvalRecords.forEach(ra -> {
            Long requestId = ra.getRequestId();
            RequestApproval requestApproval = result.get(requestId);
            if (requestApproval == null) {
                requestApproval = new RequestApproval();
                result.put(requestId, requestApproval);
            }

            List<String> list = null;
            if (ra.getDecision() == RequestStatus.APPROVED.getValue()) {
                list = requestApproval.getApproveds();
                if (list == null) {
                    list = new ArrayList<>();
                    requestApproval.setApproveds(list);
                }
            } else {
                list = requestApproval.getRejecteds();
                if (list == null) {
                    list = new ArrayList<>();
                    requestApproval.setRejecteds(list);
                }
            }
            list.add(ra.getApprover());
        });

        return Either.right(result);
    }

    private Either<Exception, Resource> exportDataToRequestForm(RequestEntity request, Resource fileResource, List<UserRepresentation> allUsers) {
        XWPFDocument document = null;
        Long requestId = request.getId();
        try {
            document = new XWPFDocument(fileResource.getInputStream());

            List<XWPFParagraph> paragraphs = new ArrayList<>(document.getParagraphs());
            paragraphs.addAll(document.getHeaderList().stream().map(XWPFHeaderFooter::getParagraphs).flatMap(List::stream).collect(Collectors.toList()));
            paragraphs.addAll(document.getFooterList().stream().map(XWPFHeaderFooter::getParagraphs).flatMap(List::stream).collect(Collectors.toList()));
            paragraphs.addAll(
                    document.getTables().stream().map(table -> table.getRows()).flatMap(List::stream).collect(Collectors.toList())
                            .stream().map(row -> row.getTableCells()).flatMap(List::stream).collect(Collectors.toList())
                            .stream().map(cell -> cell.getParagraphs()).flatMap(List::stream).collect(Collectors.toList())
            );
            String requestValue = request.getValue();
            ObjectMapper objectMapper = new ObjectMapper();

            TypeReference<HashMap<String, AttributeValueWrapper>> typeRef = new TypeReference<HashMap<String, AttributeValueWrapper>>() {
            };

            HashMap<String, AttributeValueWrapper> map = objectMapper.readValue(requestValue, typeRef);
            map.put("id", new AttributeValueWrapper().value(request.getId().toString()));
            map.put("name", new AttributeValueWrapper().value(request.getName()));
            map.put("company", new AttributeValueWrapper().value(appConfiguration.getCompanyName()));
            // map.put("status", new AttributeValueWrapper().value(RequestStatus.fromId(request.getStatus()).toString()));
            map.put("username", new AttributeValueWrapper().value(request.getSubmitter()));
            map.put("user", new AttributeValueWrapper().value(keycloakService.getFullName(allUsers.stream().filter(u -> Objects.equals(u.getUsername(), request.getSubmitter())).findFirst().get())));

            map.put("date", new AttributeValueWrapper().value(String.valueOf(request.getSubmitTime())).attr(new Attribute().attrType(3L)));
            map.put("time", new AttributeValueWrapper().value(String.valueOf(request.getSubmitTime())).attr(new Attribute().attrType(4L)));
            map.put("group", new AttributeValueWrapper().value(request.getRequestGroupVersionEntity().getRequestGroup().getName()));
            map.put("group.id", new AttributeValueWrapper().value(String.valueOf(request.getRequestGroupVersionEntity().getRequestGroup().getId())));
            map.put("status", new AttributeValueWrapper().value(RequestStatus.fromId(request.getStatus()).getStringValue()));

            // custom
            addCustomAttrbiutes(map, request, allUsers);
            //
            for (Map.Entry<String, AttributeValueWrapper> entry : map.entrySet()) {
                String attrKey = entry.getKey();
                AttributeValueWrapper attributeValueWrapper = entry.getValue();
                Object attrValue = attributeValueWrapper.value();
                // I am so lazy
                if (attributeValueWrapper.attr() != null && attributeValueWrapper.attr().attrType() == 9L) {
                    attrValue = attributeValueWrapper.attr().getName();
                }

            //    if (attrValue != null) {
                    AttributeFormatter formatter = getFormatter(attrValue == null ? null : attributeValueWrapper.attr());

//                    String attrValueStr = formatter.formatData(requestId, attrValue, attributeValueWrapper.attr, null);
                    XWPFDocument finalDocument = document;
                    Object finalAttrValue = attrValue;
                    paragraphs.forEach(paragraph -> {
                        List<XWPFRun> xwpfRuns = new LinkedList<>(paragraph.getRuns());
                        for (int i = 0; i < xwpfRuns.size(); i++) {
                            XWPFRun xwpfRun = xwpfRuns.get(i);
                            if ("${".equals(xwpfRun.getText(0))) {
                                if (i + 1 < xwpfRuns.size()) {
                                    if (attrKey.equals(xwpfRuns.get(i + 1).getText(0))) {
                                        if (i + 2 < xwpfRuns.size()) {
                                            if ("}".equals(xwpfRuns.get(i + 2).getText(0))) {
                                                xwpfRun.setText("", 0);
                                                xwpfRuns.get(i + 1).setText("", 0);
                                                xwpfRuns.get(i + 2).setText("", 0);
                                                replaceData(finalDocument, paragraph, xwpfRun, formatter, requestId, finalAttrValue, attributeValueWrapper.attr());
                                            }
                                        }
                                    }
                                }
                            } else if (("${" + attrKey + "}").equals(xwpfRun.getText(0))) {
                                xwpfRun.setText("", 0);
                                replaceData(finalDocument, paragraph, xwpfRun, formatter, requestId, finalAttrValue, attributeValueWrapper.attr());
                            } else {
                                String text = xwpfRun.getText(0);
                                if (text != null && text.contains("${" + attrKey + "}")) {

                                    String str = String.valueOf(finalAttrValue);

                                    int st = text.indexOf("${" + attrKey + "}");
                                    int end = st + ("${" + attrKey + "}").length();
                                    str = text.substring(0, st);
                                    xwpfRun.setText(str, 0);
                                    XWPFRun xwpfRun1 = paragraph.insertNewRun(i + 1);
                                    XWPFRun xwpfRun2 = paragraph.insertNewRun(i + 2);
                                    xwpfRun2.setText(text.substring(end));
                                    replaceData(finalDocument, paragraph, xwpfRun1, formatter, requestId, finalAttrValue, attributeValueWrapper.attr());

                                    // str += text.substring(end);
//                                         xwpfRun.setText("", 0);
//                                         String attrValueStr = formatter.formatData(requestId, attrValue, attributeValueWrapper.attr, null);
//                                         text = text.replaceAll("\\$\\{" + attrKey + "\\}", "");
//                                         xwpfRun.
//                                         //   xwpfRun.
//                                         xwpfRun.setText(text, 0);
//                                     }
                                }
                            }
                        }
                    });
                }
           // }
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            document.write(outputStream);
            return Either.right(new ByteArrayResource(outputStream.toByteArray()));
        } catch (IOException ex) {
            log.error("Cannot export data", ex);
            return Either.left(new AppException("cannot_process_data"));
        } catch (Exception ex) {
            log.error("Cannot export data", ex);
            return Either.left(new AppException("cannot_process_data"));
        } finally {
            if (document != null) {
                try {
                    document.close();
                } catch (IOException ignore) {}
            }
        }
    }

    private void addCustomAttrbiutes(HashMap<String, AttributeValueWrapper> map, RequestEntity request, List<UserRepresentation> allUsers) {
        Set<String> approvers = getAppovers(request);
        List<DARequestApproval> requestApprovalRecords = requestApprovalJpa.getRequestApprovalRecords(Collections.singletonList(request.getId()));
        Calendar calendar = Calendar.getInstance();
        List<OneExportData> userApprovers = approvers.stream().map(username -> {
            Optional<UserRepresentation> userOpt = allUsers.stream().filter(u -> Objects.equals(username, u.getUsername())).findAny();
            OneExportData exportData = new OneExportData();
            exportData.username = username;
            if (userOpt.isPresent()) {
                exportData.fullName = keycloakService.getFullName(userOpt.get());
                Optional<DARequestApproval> raOpt = requestApprovalRecords.stream().filter(ra -> Objects.equals(username, ra.getApprover())).findFirst();
                if (raOpt.isPresent()) {
                    DARequestApproval daRequestApproval = raOpt.get();
                    if (daRequestApproval.getDecision() == RequestStatus.APPROVED.getValue()) {
                        calendar.setTimeInMillis(daRequestApproval.getSubmittedTime());
                        exportData.approvedDate = calendar.get(Calendar.DAY_OF_MONTH) + "/" + calendar.get(Calendar.MONTH) + "/" + calendar.get(Calendar.YEAR);
                        exportData.approvedTime = calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);
                    }
                }
            }
            return exportData;
        }).collect(Collectors.toList());

        map.put("owners", new AttributeValueWrapper().value(userApprovers.stream().map(u -> u.fullName).collect(Collectors.toList())).attr(new Attribute().attrType(99L)));
        map.put("owners.username", new AttributeValueWrapper().value(userApprovers.stream().map(u -> u.username).collect(Collectors.toList())).attr(new Attribute().attrType(99L)));
        map.put("owners.text", new AttributeValueWrapper().value(userApprovers.stream().map(u -> u.fullName + " (@" + u.username + ")").collect(Collectors.toList())).attr(new Attribute().attrType(99L)));
        map.put("approved.date", new AttributeValueWrapper().value(userApprovers.stream().filter(u -> u.approvedDate != null).map(u -> u.approvedDate).collect(Collectors.toList())).attr(new Attribute().attrType(99L)));
        map.put("approved.time", new AttributeValueWrapper().value(userApprovers.stream().filter(u -> u.approvedTime != null).map(u -> u.approvedTime).collect(Collectors.toList())).attr(new Attribute().attrType(99L)));
        map.put("approved.name", new AttributeValueWrapper().value(userApprovers.stream().filter(u -> u.approvedTime != null).map(u -> u.fullName).collect(Collectors.toList())).attr(new Attribute().attrType(99L)));
        map.put("approved.list", new AttributeValueWrapper().value(userApprovers.stream().filter(u -> u.approvedTime != null).map(u -> u.fullName + " (" + u.approvedTime + " " + u.approvedDate + ")").collect(Collectors.toList())).attr(new Attribute().attrType(99L)));
    }

    class OneExportData {
        String username;

        String fullName;

        String approvedTime;

        String approvedDate;
    }


    private Set<String> getAppovers(RequestEntity request) {
        Set<String> approvers = new LinkedHashSet<>();
        approvers.addAll(Arrays.asList(request.oneGetDirectMgmts()));
        approvers.addAll(Arrays.asList(request.oneGetApprovers()));
        approvers.addAll(Arrays.asList(request.getRequestGroupVersionEntity().oneGetApprovers()));
        return approvers;
    }

    private void replaceData(XWPFDocument finalDocument, XWPFParagraph paragraph, XWPFRun xwpfRun, AttributeFormatter formatter, Long requestId, Object attrValue, Attribute attr) {
        if (formatter.selfFormat()) {
            try {
                formatter.format(finalDocument, paragraph, xwpfRun, requestId, attrValue, attr, "");
            } catch (Exception ex) {
                log.error("Cannot format data", ex);
            }
        } else {
            try {
                String attrValueStr = formatter.formatData(requestId, attrValue, attr, null);
                xwpfRun.setText(attrValueStr, 0);
            } catch (Exception ex) {
                log.error("Cannot format data", ex);
            }
        }
    }

    private AttributeFormatter getFormatter(Attribute attr) {
        if (attr != null) {
            AttributeFormatter formatter = attrFormatters.get(attr.attrType());
            if (formatter != null) {
                return formatter;
            }
        }
        return attrFormatters.get(0L);
    }

    public static Converter<RequestStatus, Integer> requestStatusConverter = new Converter<RequestStatus, Integer>() {
        @Override
        public Integer convert(MappingContext<RequestStatus, Integer> mappingContext) {
            if (mappingContext.getSource() != null) {
                return mappingContext.getSource().getValue();
            }
            return RequestStatus.PENDING.getValue();
        }
    };

    public static Converter<Integer, RequestStatus> intToRequestStatusConverter = new Converter<Integer, RequestStatus>() {
        @Override
        public RequestStatus convert(MappingContext<Integer, RequestStatus> mappingContext) {
            return RequestStatus.fromId(mappingContext.getSource());
        }
    };

    static class EntityToDomainRequestMapping extends PropertyMap<Request, RequestEntity> {

        @Override
        protected void configure() {
            RequestEntity map = using(requestStatusConverter).map();
            try {
                if (map != null) {
                    map.setStatus(this.<Integer>source("status"));
                }
            } catch (Exception ex) {
            }
        }
    }

    static class DomainToEntityRequestMapping extends PropertyMap<RequestEntity, Request> {

        @Override
        protected void configure() {
            using(intToRequestStatusConverter).map().status(this.<RequestStatus>source("status"));
        }
    }

//    @Scheduled(cron = "0 0 * * * *")
    @Scheduled(cron = "0 0/1 * * * ?")
    @Transactional
    public void autoUpdateRequestStatus() {
        try {
            long currentMilli = System.currentTimeMillis();
            log.info("update Request timeout at: {}", currentMilli);
            List<RequestEntity> requestTimeout = requestJpa.getRequestTimeout(currentMilli);
            requestTimeout.forEach((r) -> {
                log.error("timeout: id: {}- status: {} - timeout: {}", r.getId(), r.getStatus(), r.getTimeout());
            });
            requestTimeout.forEach(r -> r.setStatus(RequestStatus.TIMEOUT.getValue()));
            List<RequestEntity> updated = requestJpa.saveAll(requestTimeout);
            updated.forEach(r -> {
                try {
                    sendTimeoutNotification(r);
                } catch (Exception ex) {
                    log.error("Cannot send Timeout Notification for {}", r.getName(), ex);
                }
            });
        } catch (Exception ex) {
            log.error("cannot update request status", ex);
        }
    }

    void sendApprovedByApproverNotification(RequestEntity r) {
        String approver = TenantContext.getUsername();

        String[] followers = r.getRequestGroupVersionEntity().oneGetFollowers();
        Set<String> allTargets = new HashSet<>(Arrays.asList(followers));
        allTargets.addAll(Arrays.asList(r.getRequestGroupVersionEntity().oneGetApprovers()));
        allTargets.add(r.getSubmitter());
        allTargets.addAll(Arrays.asList(r.oneGetFollowers()));
        allTargets.addAll(Arrays.asList(r.oneGetApprovers()));
        allTargets.addAll(Arrays.asList(r.oneGetDirectMgmts()));
        allTargets.remove(approver);

        try {
            sendEventApprovedByApproverNotification(r, approver, allTargets);
        }catch (Exception ex) {
            log.error("Cannot send EventApprovedByAppover", ex);
        }
        try {
            sendEmailApprovedByApproverNotification(r, approver, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EmailApprovedByApprover", ex);
        }
    }

    private Map<String, NotiUserInfo> getAllUserInfo() {
        return keycloakService.getAllUsers().stream().collect(Collectors.toMap(UserRepresentation::getUsername, user -> {
            NotiUserInfo userInfo = new NotiUserInfo();
            userInfo.username = user.getUsername();
            userInfo.email = user.getEmail();
            userInfo.fullName = keycloakService.getFullName(user);
            return userInfo;
        }));
    }

    private void sendEmailApprovedByApproverNotification(RequestEntity request, String approver, Set<String> allTargets) {
        Map<String, NotiUserInfo> userMap = getAllUserInfo();

        String url = appConfiguration.getRequestUrl() + "/requests/" + request.getId();

        List<Map<String, Object>> data = new ArrayList<>();
        String approverName = "";
        if (userMap.containsKey(approver)) {
          approverName = userMap.get(approver).fullName;
        }

        String finalApproverName = approverName;
        allTargets.forEach((username) -> {
            try {
                data.add(Map.of(
                        "targetEmail", userMap.get(username).email,
                        "title", "Đề Xuất Được Chấp Nhận",
                        "longContent", "approved",
                        "data", Map.of(
                                "targetName", userMap.get(username).fullName,
                                "requestName", request.getName(),
                                "requestUrl", url,
                                "moreContent", "",
                                "approverName", finalApproverName
                        )
                ));
            } catch (Exception ex) {
                log.error("cannot create email created_approver content for user {}", username, ex);
            }
        });
        notificationService.sendEmails(data);
    }

    private void sendEventApprovedByApproverNotification(RequestEntity r, String approver, Set<String> allTargets) {
        String url = "/requests/" + r.getId();
        String content = String.format(APPROVED_CONTENT, r.getName(), approver);

        String shortContent = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            shortContent = objectMapper.writeValueAsString(Map.of(
                    "url", url,
                    "content", content

            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        sendNotification("Request Notification", shortContent, allTargets);
    }

    public void sendFwdNotification(RequestEntity r, List<String> targetUsers, String description) {
        String approver = TenantContext.getUsername();

        Set<String> targetUserSet = new LinkedHashSet<>(targetUsers);

        try {
            sendEventFwdByApproverNotification(r, approver, targetUserSet);
        }catch (Exception ex) {
            log.error("Cannot send EventFwdByAppover", ex);
        }
        try {
            sendEmailFwdByApproverNotification(r, approver, targetUserSet);
        } catch (Exception ex) {
            log.error("Cannot send EmailFwdByApprover", ex);
        }
    }

    private void sendEmailFwdByApproverNotification(RequestEntity request, String approver, Set<String> allTargets) {
        Map<String, NotiUserInfo> userMap = getAllUserInfo();

        String url = appConfiguration.getRequestUrl() + "/requests/" + request.getId();

        List<Map<String, Object>> data = new ArrayList<>();
        String approverName = "";
        if (userMap.containsKey(approver)) {
            approverName = userMap.get(approver).fullName;
        }

        String finalApproverName = approverName;
        allTargets.forEach((username) -> {
            try {
                data.add(Map.of(
                        "targetEmail", userMap.get(username).email,
                        "title", "Đề Xuất Được Chuyển Tiếp",
                        "longContent", "fwd",
                        "data", Map.of(
                                "targetName", userMap.get(username).fullName,
                                "requestName", request.getName(),
                                "approverName", finalApproverName,
                                "requestUrl", url,
                                "moreContent", ""
                        )
                ));
            } catch (Exception ex) {
                log.error("cannot create email created_approver content for user {}", username, ex);
            }
        });
        notificationService.sendEmails(data);
    }

    private void sendEventFwdByApproverNotification(RequestEntity r, String approver, Set<String> allTargets) {
        String url = "/requests/" + r.getId();
        String content = String.format(FWD_CONTENT, r.getName(), approver);

        String shortContent = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            shortContent = objectMapper.writeValueAsString(Map.of(
                    "url", url,
                    "content", content

            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        sendNotification("Request Notification", shortContent, allTargets);
    }

    public void sendCancelledBySubmitter(RequestEntity r) {
        String[] followers = r.getRequestGroupVersionEntity().oneGetFollowers();
        String[] approvers = r.getRequestGroupVersionEntity().oneGetApprovers();
        Set<String> allTargets = new HashSet<>(Arrays.asList(followers));
        allTargets.addAll(Arrays.asList(approvers));
        allTargets.addAll(Arrays.asList(r.oneGetDirectMgmts()));
        allTargets.addAll(Arrays.asList(r.oneGetApprovers()));
        allTargets.addAll(Arrays.asList(r.oneGetFollowers()));

        try {
            sendEventCancelledBySubmitter(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EventCancelledBySubmitter", ex);
        }
        try {
            sendEmailCancelledBySubmitter(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EmailCancelledBySubmitter", ex);
        }
    }

    private void sendEmailCancelledBySubmitter(RequestEntity request, Set<String> allTargets) {
        Map<String, NotiUserInfo> userMap = getAllUserInfo();

        String url = appConfiguration.getRequestUrl() + "/requests/" + request.getId();

        List<Map<String, Object>> data = new ArrayList<>();
        String submitter = "";
        if (userMap.containsKey(TenantContext.getUsername())) {
            submitter = userMap.get(TenantContext.getUsername()).fullName;
        }
        String finalSubmitter = submitter;
        allTargets.forEach((username) -> {
            try {
                data.add(Map.of(
                        "targetEmail", userMap.get(username).email,
                        "title", "Đề Xuất Bị Hủy",
                        "longContent", "request_cancelled",
                        "data", Map.of(
                                "targetName", userMap.get(username).fullName,
                                "submitterName", finalSubmitter,
                                "requestName", request.getName(),
                                "requestUrl", url,
                                "moreContent", ""
                        )
                ));
            } catch (Exception ex) {
                log.error("cannot create email request_cancelled content for user {}", username, ex);
            }
        });
        notificationService.sendEmails(data);
    }

    private void sendEventCancelledBySubmitter(RequestEntity r, Set<String> allTargets) {
        String url = "/requests/" + r.getId();
        String content = String.format(REQUEST_CANCELLED_CONTENT, r.getName(), TenantContext.getUsername());

        String shortContent = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            shortContent = objectMapper.writeValueAsString(Map.of(
                    "url", url,
                    "content", content

            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        sendNotification("Request Notification", shortContent, allTargets);
    }

    public void sendCancelledByApprover(RequestEntity r) {
        this.sendCancelledBySubmitter(r);
    }

    void sendApprovedNotification(RequestEntity r) {
        String[] followers = r.getRequestGroupVersionEntity().oneGetFollowers();
        String submitter = r.getSubmitter();
        Set<String> allTargets = new HashSet<>(Arrays.asList(followers));
        allTargets.add(submitter);
        allTargets.addAll(Arrays.asList(r.oneGetDirectMgmts()));
        allTargets.addAll(Arrays.asList(r.oneGetFollowers()));
        allTargets.addAll(Arrays.asList(r.oneGetApprovers()));
        allTargets.addAll(Arrays.asList(r.getRequestGroupVersionEntity().oneGetApprovers()));

        try {
            sendEventApprovedNotification(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EventApprovedNotificaiton", ex);
        }

        try {
            sendEmailApprovedNotification(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EmailApprovedNotification", ex);
        }
    }

    private void sendEmailApprovedNotification(RequestEntity request, Set<String> allTargets) {
        Map<String, NotiUserInfo> userMap = getAllUserInfo();

        String url = appConfiguration.getRequestUrl() + "/requests/" + request.getId();
        List<Map<String, Object>> data = new ArrayList<>();
        allTargets.forEach((username) -> {
            try {
                data.add(Map.of(
                        "targetEmail", userMap.get(username).email,
                        "title", "Đề Xuất Đã Được Chấp Nhận",
                        "longContent", "request_approved",
                        "data", Map.of(
                                "targetName", userMap.get(username).fullName,
                                "requestName", request.getName(),
                                "requestUrl", url,
                                "moreContent", ""
                        )
                ));
            } catch (Exception ex) {
                log.error("cannot create email request_cancelled content for user {}", username, ex);
            }
        });
        notificationService.sendEmails(data);
    }

    private void sendEventApprovedNotification(RequestEntity r, Set<String> allTargets) {
        String url = "/requests/" + r.getId();
        String content = String.format(REQUEST_APPROVED_CONTENT, r.getName());
        String shortContent = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            shortContent = objectMapper.writeValueAsString(Map.of(
                    "url", url,
                    "content", content

            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        sendNotification("Request Notification", shortContent, allTargets);
    }

    void sendRejectedByApproverNotification(RequestEntity r) {

        String[] followers = r.getRequestGroupVersionEntity().oneGetFollowers();
        Set<String> allTargets = new HashSet<>(Arrays.asList(followers));
        String submitter = r.getSubmitter();
        allTargets.add(submitter);
        allTargets.addAll(Arrays.asList(r.getRequestGroupVersionEntity().oneGetApprovers()));
        allTargets.addAll(Arrays.asList(r.oneGetApprovers()));
        allTargets.addAll(Arrays.asList(r.oneGetDirectMgmts()));
        allTargets.addAll(Arrays.asList(r.oneGetFollowers()));
        allTargets.remove(TenantContext.getUsername());

        try {
            sendEventRejectedByApproverNotification(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EventRejectedByApproverNotification", ex);
        }

        try {
            sendEmailRejectedByApproverNotification(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EmailRejectedByApproverNotification", ex);
        }
    }

    private void sendEmailRejectedByApproverNotification(RequestEntity request, Set<String> allTargets) {
        Map<String, NotiUserInfo> userMap = getAllUserInfo();

        String url = appConfiguration.getRequestUrl() + "/requests/" + request.getId();
        List<Map<String, Object>> data = new ArrayList<>();
        String approverName = "";
        if (userMap.containsKey(TenantContext.getUsername())) {
            approverName = userMap.get(TenantContext.getUsername()).fullName;
        }

        String finalApproverName = approverName;
        allTargets.forEach((username) -> {
            try {
                data.add(Map.of(
                        "targetEmail", userMap.get(username).email,
                        "title", "Đề Xuất Bị Từ Chối",
                        "longContent", "rejected",
                        "data", Map.of(
                                "targetName", userMap.get(username).fullName,
                                "requestName", request.getName(),
                                "approverName", finalApproverName,
                                "requestUrl", url,
                                "moreContent", ""
                        )
                ));
            } catch (Exception ex) {
                log.error("cannot create email request_cancelled content for user {}", username, ex);
            }
        });
        notificationService.sendEmails(data);
    }

    private void sendEventRejectedByApproverNotification(RequestEntity r, Set<String> allTargets) {
        String url = "/requests/" + r.getId();
        String approver = TenantContext.getUsername();
        String content = String.format(REJECTED_CONTENT, r.getName(), approver);
        String shortContent = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            shortContent = objectMapper.writeValueAsString(Map.of(
                    "url", url,
                    "content", content

            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        sendNotification("Request Notification", shortContent, allTargets);
    }

    void sendRejectedNotification(RequestEntity r) {
        String[] followers = r.getRequestGroupVersionEntity().oneGetFollowers();
        String submitter = r.getSubmitter();
        Set<String> allTargets = new HashSet<>(Arrays.asList(followers));
        allTargets.add(submitter);
        allTargets.addAll(Arrays.asList(r.oneGetFollowers()));

        try {
            sendEventRejectedNotification(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EventRejectedNotification", ex);
        }

        try {
            sendEmailRejectedNotification(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EmailRejectedNotification", ex);
        }
    }

    private void sendEmailRejectedNotification(RequestEntity request, Set<String> allTargets) {
        Map<String, NotiUserInfo> userMap = getAllUserInfo();

        String url = appConfiguration.getRequestUrl() + "/requests/" + request.getId();
        List<Map<String, Object>> data = new ArrayList<>();
        allTargets.forEach((username) -> {
            try {
                data.add(Map.of(
                        "targetEmail", userMap.get(username).email,
                        "title", "Đề Xuất Bị Từ Chối",
                        "longContent", "request_rejected",
                        "data", Map.of(
                                "targetName", userMap.get(username).fullName,
                                "requestName", request.getName(),
                                "requestUrl", url,
                                "moreContent", ""
                        )
                ));
            } catch (Exception ex) {
                log.error("cannot create email request_cancelled content for user {}", username, ex);
            }
        });
        notificationService.sendEmails(data);
    }

    private void sendEventRejectedNotification(RequestEntity r, Set<String> allTargets) {
        String url = "/requests/" + r.getId();
        String content = String.format(REQUEST_REJECTED_CONTENT, r.getName());

        String shortContent = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            shortContent = objectMapper.writeValueAsString(Map.of(
                    "url", url,
                    "content", content

            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        sendNotification("Request Notification", shortContent, allTargets);
    }

    private void sendTimeoutNotification(RequestEntity r) {
        String[] approvers = r.getRequestGroupVersionEntity().oneGetApprovers();
        String[] followers = r.getRequestGroupVersionEntity().oneGetFollowers();
        Set<String> allTargets = new HashSet<>();
        allTargets.addAll(Arrays.asList(approvers));
        allTargets.addAll(Arrays.asList(followers));
        allTargets.add(r.getSubmitter());
        allTargets.addAll(Arrays.asList(r.oneGetFollowers()));
        allTargets.addAll(Arrays.asList(r.oneGetApprovers()));
        allTargets.addAll(Arrays.asList(r.oneGetDirectMgmts()));

        try {
            sendEventTimeoutNotification(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EventTimeoutNotification", ex);
        }

        try {
            sendEmailTimeooutNotification(r, allTargets);
        } catch (Exception ex) {
            log.error("Cannot send EmailTimeoutNotification", ex);
        }

    }

    private void sendEmailTimeooutNotification(RequestEntity request, Set<String> allTargets) {
        Map<String, NotiUserInfo> userMap = getAllUserInfo();

        String url = appConfiguration.getRequestUrl() + "/requests/" + request.getId();
        List<Map<String, Object>> data = new ArrayList<>();
        allTargets.forEach((username) -> {
            try {
                data.add(Map.of(
                        "targetEmail", userMap.get(username).email,
                        "title", "Đề Xuất Đã Quá Hạn Xử Lý",
                        "longContent", "timeout",
                        "data", Map.of(
                                "targetName", userMap.get(username).fullName,
                                "requestName", request.getName(),
                                "requestUrl", url,
                                "moreContent", ""
                        )
                ));
            } catch (Exception ex) {
                log.error("cannot create email request_cancelled content for user {}", username, ex);
            }
        });
        notificationService.sendEmails(data);
    }

    private void sendEventTimeoutNotification(RequestEntity r, Set<String> allTargets) {
        String url = "/requests/" + r.getId();
        String content = String.format(TIMEOUT_CONTENT, r.getName());
        ObjectMapper objectMapper = new ObjectMapper();
        String shortContent = null;
        try {
            shortContent = objectMapper.writeValueAsString(Map.of(
                    "url", url,
                    "content", content

            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        sendNotification("Request Notification", shortContent, allTargets);
    }

    private Either<Exception, Boolean> sendNotification(String title, String shortContent, Set<String> targets) {
        List<Object> notifications = new ArrayList<>();
        for (String target: targets) {
            notifications.add(
                    Map.of("target", target,
                            "shortContent", shortContent,
                            "title", title
                    )
            );
        }
        return notificationService.sendNotification(notifications);
    }



    private Either<Exception, Boolean> sendCreatedNotification(Request request, RequestEntity entity) {
        try {
            sendCreatedRequestEmailNotification(entity);
        } catch (Exception ex) {
            log.error("Cannot send EmailNotification for RequestCreation", ex);
        }
        try {
            return sendEventNotification(entity);
        } catch (Exception ex) {
            log.error("Cannot send EventNotification for RequestCreation", ex);
        }
        return Either.left(new AppException("cannot_send_notificaiton"));
    }

    class NotiUserInfo {
        String username;

        String email;

        String fullName;
    }

    private void sendCreatedRequestEmailNotification(RequestEntity entity) {
        Map<String, NotiUserInfo> userMap = getAllUserInfo();
        String[] approvers = entity.getRequestGroupVersionEntity().oneGetApprovers();
        String[] followers = entity.getRequestGroupVersionEntity().oneGetFollowers();
        String[] directManagers = entity.oneGetDirectMgmts();


        String url = appConfiguration.getRequestUrl() + "/requests/" + entity.getId();
        List<Map<String, Object>> data = new ArrayList<>();
        Set<String> allApprovers = new HashSet<>(Arrays.asList(approvers));
        allApprovers.addAll(Arrays.asList(directManagers));
        allApprovers.addAll(Arrays.asList(entity.oneGetApprovers()));
        allApprovers.forEach((username) -> {
            try {
                data.add(Map.of(
                        "targetEmail", userMap.get(username).email,
                        "title", "Đề Xuất Mới",
                        "longContent", "created_approver",
                        "data", Map.of(
                                "targetName", userMap.get(username).fullName,
                                "ownerName", userMap.get(entity.getSubmitter()).fullName,
                                "url", url,
                                "moreContent", ""
                        )
                ));
            } catch (Exception ex) {
                log.error("cannot create email created_approver content for user {}", username, ex);
            }
        });
        Set<String> allFollowers = new HashSet<>(Arrays.asList(followers));
        allFollowers.addAll(Arrays.asList(entity.oneGetFollowers()));
        for(String username : allFollowers) {
            try {
                data.add(Map.of(
                        "targetEmail", userMap.get(username).email,
                        "title", "Đề Xuất Mới",
                        "longContent", "created_follower",
                        "data", Map.of(
                                "targetName", userMap.get(username).fullName,
                                "ownerName", userMap.get(entity.getSubmitter()).fullName,
                                "url", url,
                                "moreContent", ""
                        )
                ));
            } catch (Exception ex) {
                log.error("cannot create email created_follower content for user {}", username, ex);
            }
        }
        notificationService.sendEmails(data);
    }

    private Either<Exception, Boolean> sendEventNotification(RequestEntity entity) {
        Set<String> approversList = new HashSet<>(Arrays.asList(entity.getRequestGroupVersionEntity().oneGetApprovers()));
        approversList.addAll(Arrays.asList(entity.oneGetApprovers()));
        String[] approvers = approversList.toArray(new String[0]);

        String[] followers = entity.getRequestGroupVersionEntity().oneGetFollowers();
        String[] directManagers = entity.oneGetDirectMgmts();

        String url = "/requests/" + entity.getId();
        String content = String.format(CREATED_APPROVER_CONTENT, TenantContext.getUsername());
        ObjectMapper objectMapper = new ObjectMapper();
        String shortContent = null;
        try {
            shortContent = objectMapper.writeValueAsString(Map.of(
                    "url", url,
                    "content", content

            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        sendNotification("Request Notificaiton", shortContent, new HashSet<>(Arrays.asList(approvers)));
        if (directManagers.length > 0) {
            sendNotification("Request Notificaiton", shortContent, new HashSet<>(Arrays.asList(directManagers)));
        }
        content = String.format(CREATED_FOLLOWER_CONTENT, TenantContext.getUsername());

        try {
            shortContent = objectMapper.writeValueAsString(Map.of(
                    "url", url,
                    "content", content

            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (followers.length > 0) {
            return sendNotification("Request Notification", shortContent, new HashSet<>(Arrays.asList(followers)));
        }
        return Either.right(Boolean.TRUE);

    }

    @Override
    public Either<Exception, Boolean> visitRequest(long id) {
        try {
            Optional<RequestEntity> requestOpt = requestJpa.findById(id);
            if (requestOpt.isPresent()) {
                UserRequestDataEntity entity = new UserRequestDataEntity();
                entity.setRequestId(id);
                entity.setUsername(TenantContext.getUsername());
                entity.setVisitStatus(1);
                userRequestDataJpa.save(entity);
                return Either.right(Boolean.TRUE);
            }
            return Either.left(new UserException("request_not_found"));
        } catch (Exception ex) {
            log.error("Cannot store user visit status", ex);
            return Either.left(new AppException("cannot_store_data"));
        }
    }

    @Override
    public Either<Exception, Resource> exportAllRequest(String search, int page, int size, Boolean fullData, Long requestGroupId, Boolean toMe, Boolean fromMe, Boolean followed, RequestStatus requestStatus, Boolean favorite, List<String> creators, Long createdDate, Long end) {
        if (search == null) {
            search = "";
        }
        try {
            String aUsername = "," + TenantContext.getUsername() + ",";
            if (!SecurityUtils.hasRole("owner")) {
                if (toMe == null && fromMe == null && followed == null) {
                    toMe = true;
                    fromMe = true;
                    followed = true;
                }
            }

            if (createdDate != null && end == null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(createdDate);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                createdDate = calendar.getTimeInMillis();
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                end = calendar.getTimeInMillis();
            }
            List<Long> requestFavorites = getRequestFavorites();
            int total = requestJpa.countAllWithBasicData(search, requestGroupId, TenantContext.getUsername(),
                    aUsername, toMe, fromMe, followed, requestStatus != null ? requestStatus.getValue() : null, creators, createdDate, end);
            if (total > 0) {
                List<RequestEntity> requestEntities = requestJpa.findAllWithFullData(search, requestGroupId, TenantContext.getUsername(), aUsername, toMe, fromMe, followed, requestStatus != null ? requestStatus.getValue() : null,
                        creators, createdDate, end,
                        PageRequest.of(0, total, Sort.by(Sort.Direction.DESC, "submitTime")));

                List<Long> requestIds = requestEntities.stream().map(RequestEntity::getId).collect(Collectors.toList());
                List<DARequestApproval> requestApprovalRecords = requestApprovalJpa.getRequestApprovalRecords(requestIds);
                Map<Long, List<String>> approvedMaps = new HashMap<>();
                Map<Long, List<String>> rejectedMaps = new HashMap<>();
                requestApprovalRecords.forEach(a -> {
                    if (a.getDecision() == RequestStatus.APPROVED.getValue()) {
                        List<String> strings = approvedMaps.computeIfAbsent(a.getRequestId(), k -> new ArrayList<>());
                        strings.add(a.getApprover());
                    } else if (a.getDecision() == RequestStatus.REJECTED.getValue()) {
                        List<String> strings = rejectedMaps.computeIfAbsent(a.getRequestId(), k -> new ArrayList<>());
                        strings.add(a.getApprover());
                    }
                });

                return exportToExcelFile(requestEntities);
            }
            return Either.left(new UserException("data_empty"));
        } catch (Exception ex) {
            log.error("Cannot retrieve data", ex);
            return Either.left(new AppException("cannot_retrieve_Data"));
        }
    }

    private Either<Exception, Resource> exportToExcelFile(List<RequestEntity> requestEntities) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        try {
            final Map<String, Map<String, Integer>> keyToColumnNumbers = new HashMap<>();
            final Map<String, Integer> groupToNextRowIndex = new HashMap<>();
            requestEntities.forEach(request -> {
                try {
                    String groupName = request.getRequestGroupVersionEntity().getRequestGroup().getName();


                    XSSFSheet sheet = workbook.getSheet(groupName);
                    if (sheet == null) {
                        sheet = workbook.createSheet(groupName);
                    }

                    XSSFRow row = sheet.getRow(9);
                    if (row == null) {
                        row = sheet.createRow(9);
                    }
                    XSSFCell cell = row.getCell(0);
                    if (cell == null) {
                        cell = row.createCell(0);
                    }
                    Map<String, AttributeValueWrapper> requestValues = extractRequestValue(request);
                    Map<String, Integer> fieldsToIndex = keyToColumnNumbers.computeIfAbsent(groupName, k -> new HashMap<>());

                    //   if (cell.getStringCellValue() == null || cell.getStringCellValue().length() == 0) {
                    createHeaderRow(row, requestValues, fieldsToIndex, request.getRequestGroupVersionEntity().getAttrs());
                    //  }
                    Integer rowIndex = groupToNextRowIndex.computeIfAbsent(groupName, k -> 10);
                    groupToNextRowIndex.put(groupName, rowIndex + 1);

                    row = sheet.getRow(rowIndex);
                    if (row == null) {
                        row = sheet.createRow(rowIndex);
                    }
                    try {
                        writeDataToRow(row, request, requestValues, fieldsToIndex);
                    } catch (Exception ex) {
                        log.error("Cannot write data for request {}", request.getName(), ex);
                    }

                }catch (Exception ex) {
                    log.error("Cannot write data for request {}", request.getName(), ex);
                }
            });
            groupToNextRowIndex.keySet().forEach(name -> {
                XSSFSheet sheet = workbook.getSheet(name);
                if (sheet != null) {
                    int columnCount = DEFAULT_EXPORT_INDEX_TO_COLUMN_NAME.size() + keyToColumnNumbers.get(name).size();
                    for (int i = 0; i < columnCount; i++) {
                        sheet.autoSizeColumn(i);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Either.right(new ByteArrayResource(outputStream.toByteArray()));
    }

    private void writeDataToRow(XSSFRow row, RequestEntity request, Map<String, AttributeValueWrapper> requestValues, Map<String, Integer> fieldsToIndex) {
        List<UserRepresentation> allUsers = keycloakService.getAllUsers();
        writeDefaultData(row, request, allUsers);

        for (Map.Entry<String, AttributeValueWrapper> entry : requestValues.entrySet()) {
            if (entry.getValue() != null) {
                try {
                    Integer colIndex = fieldsToIndex.get(entry.getKey());
                    XSSFCell cell = row.getCell(colIndex);
                    if (cell == null) {
                        cell = row.createCell(colIndex);
                    }
                    AttributeValueWrapper value = entry.getValue();
                    AttributeFormatter formatter = getFormatter(value.getValue() == null ? null : value.attr());
                    String dataValue = formatter.formatData(request.getId(), value.getValue(), value.attr(), null);
                    cell.setCellValue(dataValue);
                    if (formatter instanceof MultipleLineStringAttributeFormat || formatter instanceof TableAttributeFormatter) {
                        if (value.getValue() != null) {
                            int length = dataValue.split("\n").length;
                            XSSFCellStyle cellStyle = row.getSheet().getWorkbook().createCellStyle();
                            cellStyle.setWrapText(true);
                            short height = row.getHeight();
                            short v = (short) (length * row.getSheet().getDefaultRowHeightInPoints());
                            if (height < v) {
                                row.setHeight(v);
                            }
                            cell.setCellStyle(cellStyle);
                        }
                    }
                    if (value.getAttr().getAttrType() == 0L) {
                        dataValue = formatter.formatData(request.getId(), value.getValue(), value.attr(), null);
                        cell.setCellValue(Float.parseFloat(dataValue));
                    }
                } catch (Exception ex) {
                    log.error("Unable set cell value for {} - ", entry.getKey(), entry.getValue(), ex);
                }
            }
        }
    }

    private void writeDefaultData(XSSFRow row, RequestEntity request, List<UserRepresentation> allUsers) {
        int col = 0;
        XSSFCell cell = row.getCell(col);
        if (cell == null) {
            cell = row.createCell(col);
        }
        DateTimeAttributeFormatter formatter = new DateTimeAttributeFormatter("HH:mm dd/MM/yyyy");
        cell.setCellValue(formatter.formatData(request.getId(), request.getSubmitTime(), null, null));

        cell = row.getCell(++col);
        if (cell == null) {
            cell = row.createCell(col);
        }
        String submitter = request.getSubmitter();
        Optional<UserRepresentation> userOpt = allUsers.stream().filter(u -> submitter.equals(u.getUsername())).findAny();
        if (userOpt.isPresent()) {
            cell.setCellValue(keycloakService.getFullName(userOpt.get()) + " ( @" + submitter + " )");
        }


        cell = row.getCell(++col);
        if (cell == null) {
            cell = row.createCell(col);
        }
        cell.setCellValue(request.getName());

        cell = row.getCell(++col);
        if (cell == null) {
            cell = row.createCell(col);
        }
        cell.setCellValue(RequestStatus.fromId(request.getStatus()).getStringValue());
    }

    private static final Map<Integer, String> DEFAULT_EXPORT_INDEX_TO_COLUMN_NAME = Map.of(
            0, "Ngày Tạo",
            1, "Người Tạo",
            2, "Tên Đề Xuất",
            3, "Tình Trạng"
    );

    private Map<String, Integer> createHeaderRow(XSSFRow row, Map<String, AttributeValueWrapper> requestValue, Map<String, Integer> currentFieldToColumnIndex, List<AttributeEntity> attrs) {
        XSSFFont font = row.getSheet().getWorkbook().createFont();
        font.setBold(true);
        font.setColor(IndexedColors.WHITE.getIndex());
        XSSFCellStyle cellStyle = row.getSheet().getWorkbook().createCellStyle();
        cellStyle.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFont(font);
        if (currentFieldToColumnIndex.isEmpty()) {

            int index = 0;
            do {
                XSSFCell cell = row.getCell(index);
                if (cell == null) {
                    cell = row.createCell(index);
                }

                cell.setCellStyle(cellStyle);
                cell.setCellValue(DEFAULT_EXPORT_INDEX_TO_COLUMN_NAME.get(index));
                index ++;
            } while (index < DEFAULT_EXPORT_INDEX_TO_COLUMN_NAME.size());

            for (String field : requestValue.keySet()) {
                XSSFCell cell = row.getCell(index);
                if (cell == null) {
                    cell = row.createCell(index);
                }
                cell.setCellStyle(cellStyle);
                try {
                    cell.setCellValue(attrs.stream().filter(a -> field.equals(a.getFieldName())).findAny().get().getName());
                    currentFieldToColumnIndex.put(field, index);
                    index++;
                } catch (Exception ex) {
                    log.error("Cannot set Header value for {}", field, ex);
                }
            }
        } else {
            for (String field : requestValue.keySet()) {
                if (!currentFieldToColumnIndex.containsKey(field)) {
                    int index = currentFieldToColumnIndex.size() + DEFAULT_EXPORT_INDEX_TO_COLUMN_NAME.size();
                    XSSFCell cell = row.getCell(index);
                    if (cell == null) {
                        cell = row.createCell(index);
                    }
                    cell.setCellStyle(cellStyle);
                    try {
                        cell.setCellValue(attrs.stream().filter(a -> field.equals(a.getFieldName())).findAny().get().getName());
                        currentFieldToColumnIndex.put(field, index);
                    } catch (Exception ex) {
                        log.error("Cannot set Header value for {}", field, ex);
                    }
                }
            }
        }
        return currentFieldToColumnIndex;
    }

    private Map<String, AttributeValueWrapper> extractRequestValue(RequestEntity request) throws JsonProcessingException {
        TypeReference<HashMap<String, AttributeValueWrapper>> typeRef = new TypeReference<HashMap<String, AttributeValueWrapper>>() {
        };
        ObjectMapper objectMapper = new ObjectMapper();
        HashMap<String, AttributeValueWrapper> map = objectMapper.readValue(request.getValue(), typeRef);
        return map;
    }

}
