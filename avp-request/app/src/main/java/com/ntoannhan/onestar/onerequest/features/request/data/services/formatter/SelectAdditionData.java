package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SelectAdditionData {

    private List<SelectKeyValue> values;

    @JsonProperty("default")
    private Long defaultValue;

}
