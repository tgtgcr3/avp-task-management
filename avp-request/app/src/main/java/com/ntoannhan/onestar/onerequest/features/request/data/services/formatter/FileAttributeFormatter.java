package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import com.ntoannhan.onestar.onerequest.features.storage.presentation.service.impl.StorageService;
import io.vavr.control.Either;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FileAttributeFormatter implements AttributeFormatter{

    private final StorageService storageService;

    public FileAttributeFormatter(StorageService storageService) {
        this.storageService = storageService;
    }

    @Override
    public String formatData(Long requestId, Object value, Attribute attribute, Object extra) {
        if (value != null) {
            Either<Exception, List<FileInfo>> fileInfos = this.storageService.getFileInfos(Collections.singletonList(Long.parseLong(value.toString())));
            if (fileInfos.isRight()) {
                if (fileInfos.get().size() > 0) {
                    return fileInfos.get().get(0).getName();
                }
            }
        }
        return String.valueOf(value);
    }
}
