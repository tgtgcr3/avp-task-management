package com.ntoannhan.onestar.onerequest.core.exception;

public class UncheckedUserException extends UncheckedException {

    public UncheckedUserException() {
    }

    public UncheckedUserException(int code) {
        super(code);
    }

    public UncheckedUserException(String message) {
        super(message);
    }

    public UncheckedUserException(int code, String message) {
        super(code, message);
    }

}
