package com.ntoannhan.onestar.onerequest.features.discussion.presentation.rest;

import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.Discussion;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.DiscussionStatus;

public class RestModel {

    public static Discussion toDomainModel(com.ntoannhan.onestar.onerequest.rest.model.Discussion rest) {
        Discussion discussion = OneMapper.mapTo(rest, Discussion.class);
        discussion.setStatus(toDomainStatus(rest.getStatus()));
        return discussion;
    }

    public static com.ntoannhan.onestar.onerequest.rest.model.Discussion toRestModel(Discussion domain) {
        com.ntoannhan.onestar.onerequest.rest.model.Discussion rest = OneMapper.mapTo(domain, com.ntoannhan.onestar.onerequest.rest.model.Discussion.class);
        rest.setStatus(toRestStatus(domain.getStatus()));
        return rest;
    }

    private static com.ntoannhan.onestar.onerequest.rest.model.DiscussionStatus toRestStatus(DiscussionStatus status) {
        switch (status) {
            case EDITTED:
                return com.ntoannhan.onestar.onerequest.rest.model.DiscussionStatus.EDITTED;
            case DELETED:
                return com.ntoannhan.onestar.onerequest.rest.model.DiscussionStatus.DELETED;
            case READ_ONLY:
                return com.ntoannhan.onestar.onerequest.rest.model.DiscussionStatus.READ_ONLY;
            default:
                return com.ntoannhan.onestar.onerequest.rest.model.DiscussionStatus.NEW;
        }
    }

    private static DiscussionStatus toDomainStatus(com.ntoannhan.onestar.onerequest.rest.model.DiscussionStatus status) {
        if (status == null) {
            return DiscussionStatus.NEW;
        }
        switch (status) {
            case DELETED:
                return DiscussionStatus.DELETED;
            case EDITTED:
                return DiscussionStatus.EDITTED;
            default:
                return DiscussionStatus.NEW;
        }
    }

}
