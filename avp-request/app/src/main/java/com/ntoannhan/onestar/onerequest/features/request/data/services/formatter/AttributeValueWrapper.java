package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class AttributeValueWrapper {
    Object value;

    Attribute attr;

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Attribute getAttr() {
        return attr;
    }

    public void setAttr(Attribute attr) {
        this.attr = attr;
    }
}
