package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface RequestGroupJpa extends JpaRepository<RequestGroupEntity, Long> {

    @Override
    @Query("SELECT r FROM RequestGroup r WHERE r.id = :id")
    Optional<RequestGroupEntity> findById(Long id);

    @Query("SELECT r FROM RequestGroup r WHERE r.id IN :ids")
    List<RequestGroupEntity> findAllIds(List<Long> ids);
}
