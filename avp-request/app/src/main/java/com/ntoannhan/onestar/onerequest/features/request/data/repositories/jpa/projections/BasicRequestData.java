package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections;


import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.ArrayStringTypeWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Objects;


public interface BasicRequestData extends ArrayStringTypeWrapper {
    Long getId();

    String getName();

    Integer getStatus();

    String getSubmitter();

    String getApprovers();

    String getRequestApprovers();

    String getSubmitTime();

    String getRequestGroupName();

    Long getGroupId();

    String getDirectMgmts();

    default String[] oneGetApprovers() {
        return toStringArray(this.getGroupId() > 0 ?this.getApprovers() : this.getRequestApprovers());
    }

    default String[] oneGetDirectMgmts() {
        return toStringArray(this.getDirectMgmts());
    }

}
