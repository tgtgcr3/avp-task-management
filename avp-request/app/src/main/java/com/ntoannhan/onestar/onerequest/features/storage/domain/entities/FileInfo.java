package com.ntoannhan.onestar.onerequest.features.storage.domain.entities;

import lombok.Data;

import javax.persistence.Column;

@Data
public class FileInfo {

    private Long id;

    private String name;

    private String path;

    private long createdDate;

    private long size;

    private String uploader;

    private FileInfoStatus status;

    private FileInfoVisibility visibility;

}