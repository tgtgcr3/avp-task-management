package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.handler;

import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.*;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors.*;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model.RequestGroupRestModel;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.features.storage.presentation.rest.model.FileInfoModel;
import com.ntoannhan.onestar.onerequest.rest.api.RequestGroupApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.BooleanResponse;
import com.ntoannhan.onestar.onerequest.rest.model.FileInfo;
import com.ntoannhan.onestar.onerequest.rest.model.RequestGroup;
import com.ntoannhan.onestar.onerequest.rest.model.RequestGroupGetResult;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@AllArgsConstructor
@Component
public class RequestGroupApiDelegateImpl implements RequestGroupApiDelegate {

    private final CreateRequestGroup createRequestGroup;

    private final UpdateRequestGroup updateRequestGroup;

    private final GetRequestGroups getRequestGroups;

    private final GetOneRequestGroup getOneRequestGroup;

    private final UpdateRequestGroupStatus updateRequestGroupStatusUC;

    private final ModelMapper modelMapper;

    private final ImportRequestGroupFromFile importRequestGroupFromFile;

    private final GetRequestGroupAttachmentFileInfos getRequestGroupAttachmentFileInfos;

    private final DownloadRequestGroupAttachment downloadRequestGroupAttachment;

    @Override
    public ResponseEntity<RequestGroup> createRequestGroup(RequestGroup requestGroup) {
        RequestGroupRestModel requestGroupRestModel = new RequestGroupRestModel(modelMapper);
        return createRequestGroup.call(requestGroupRestModel.convertToDomainModel(requestGroup))
                .fold(ex -> {throw ExceptionTranslator.getFailureException(ex);},
                        requestGroupR -> ResponseEntity.ok(requestGroupRestModel.convertToRestModel(requestGroupR)));
    }

    @Override
    public ResponseEntity<RequestGroup> updateRequestGroup(Long id, RequestGroup requestGroup) {
        RequestGroupRestModel requestGroupRestModel = new RequestGroupRestModel(modelMapper);
        return updateRequestGroup.call(
                new UpdateRequestGroupIn().id(id).requestGroup(requestGroupRestModel.convertToDomainModel(requestGroup))        )
                .fold(ex -> {throw ExceptionTranslator.getFailureException(ex);},
                        requestGroupR -> ResponseEntity.ok(requestGroupRestModel.convertToRestModel(requestGroupR)));

    }

    @Override
    public ResponseEntity<RequestGroupGetResult> getAllRequestGroups(Boolean fullData, String status) {
        RequestGroupRestModel requestGroupRestModel = new RequestGroupRestModel(modelMapper);
        return getRequestGroups.call(null).fold(ex -> {throw ExceptionTranslator.getFailureException(ex);},
                out -> ResponseEntity.ok(new RequestGroupGetResult()
                        .total(out.total())
                        .items(out.items().stream().map(requestGroupRestModel::convertToRestModel).collect(Collectors.toList()))
                ));
    }

    @Override
    public ResponseEntity<BooleanResponse> updateRequestGroupStatus(Long id, String status) {
        return updateRequestGroupStatusUC.call(new UpdateRequestGroupStatusIn().status(status).requestGroupId(id))
                .fold(ex -> {throw ExceptionTranslator.getFailureException(ex);}, result -> ResponseEntity.ok(new BooleanResponse().result(result)));
    }

    @Override
    public ResponseEntity<RequestGroup> getRequestGroup(Long id) {
        RequestGroupRestModel requestGroupRestModel = new RequestGroupRestModel(modelMapper);
        return getOneRequestGroup.call(new GetOneRequestGroupIn().id(id).latest(false)).fold(ex -> {throw ExceptionTranslator.getFailureException(ex);},
                result -> ResponseEntity.ok(requestGroupRestModel.convertToRestModel(result)));
    }

    @Override
    public ResponseEntity<RequestGroup> getLatestRequestGroup(Long id) {
        RequestGroupRestModel requestGroupRestModel = new RequestGroupRestModel(modelMapper);
        return getOneRequestGroup.call(new GetOneRequestGroupIn().id(id).latest(true)).fold(ex -> {throw ExceptionTranslator.getFailureException(ex);},
                result -> ResponseEntity.ok(requestGroupRestModel.convertToRestModel(result)));
    }

    @Override
    public ResponseEntity<BooleanResponse> importRequestGroup(MultipartFile file) {
        try {
            return importRequestGroupFromFile.call(new ImportRequestGroupFromFileIn().inputStream(file.getInputStream()).contentType(file.getContentType()))
                    .fold(ex -> {throw ExceptionTranslator.getFailureException(ex);},
                            result -> ResponseEntity.ok(new BooleanResponse().result(result)));
        } catch (IOException e) {
            log.error("cannot read the upload data", e);
            throw ExceptionTranslator.getFailureException(new UserException("cannot_read_upload_data"));
        }
    }

    @Override
    public ResponseEntity<List<FileInfo>> getGroupAttachmentInfo(Long id, List<Long> fileId) {
        return getRequestGroupAttachmentFileInfos.call(new GetRequestGroupAttachmentFileInfoIns().groupId(id).fileIds(fileId)).fold(
                ex -> {throw ExceptionTranslator.getFailureException(ex);},
                out -> ResponseEntity.ok(out.fileInfos().stream().map(fileInfo -> new FileInfoModel().toRestModel(fileInfo)).collect(Collectors.toList()))
        );
    }

    @Override
    public ResponseEntity<Resource> getRequestGroupAttachment(Long id, Long fileId) {
        return downloadRequestGroupAttachment.call(new DownloadRequestGroupAttachmentIn().requestGroupId(id).fileId(fileId)).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> {
                    return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.wordprocessingml.document")).body(out.resource());
                });
    }
}
