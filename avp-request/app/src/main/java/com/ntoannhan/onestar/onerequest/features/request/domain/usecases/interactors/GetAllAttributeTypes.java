package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.AttributeService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetAllAttributeTypesIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetAllAttributeTypesOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetAllAttributeTypes implements UseCase<GetAllAttributeTypesOut, GetAllAttributeTypesIn> {

    @Autowired
    private AttributeService attributeService;

    @Override
    public Either<Exception, GetAllAttributeTypesOut> call(GetAllAttributeTypesIn in) {
        return attributeService.getAllAttributeTypes().fold(Either::left, r -> Either.right(new GetAllAttributeTypesOut().total(r.size()).items(r)));
    }
}
