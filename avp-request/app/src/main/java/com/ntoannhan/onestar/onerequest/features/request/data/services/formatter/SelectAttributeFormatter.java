package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Objects;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import lombok.SneakyThrows;

public class SelectAttributeFormatter implements AttributeFormatter{
    @SneakyThrows
    @Override
    public String formatData(Long requestId, Object value, Attribute attribute, Object extra) {
        if (attribute.additionData() != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            SelectAdditionData additionData = objectMapper.readValue(attribute.additionData(), SelectAdditionData.class);
            if (value == null) {
                value = additionData.getDefaultValue();
            }
            for (SelectKeyValue keyValue :additionData.getValues()) {
                if (Objects.equal(keyValue.getValue(), value)) {
                    return keyValue.getName();
                }
            }
        }
        return String.valueOf(value);
    }
}
