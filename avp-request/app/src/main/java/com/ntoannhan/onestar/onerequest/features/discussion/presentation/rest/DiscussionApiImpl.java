package com.ntoannhan.onestar.onerequest.features.discussion.presentation.rest;

import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in.CreateDiscussionIn;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in.DeleteDiscussionIn;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in.GetAllDiscussionIn;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in.UpdateDiscussionIn;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.interceptor.CreateDiscussion;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.interceptor.DeleteDiscussion;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.interceptor.GetAllDiscussion;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.interceptor.UpdateDiscussion;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.rest.api.DiscussionApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.BooleanResponse;
import com.ntoannhan.onestar.onerequest.rest.model.Discussion;
import com.ntoannhan.onestar.onerequest.rest.model.DiscussionGetAll;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class DiscussionApiImpl implements DiscussionApiDelegate {

    private final CreateDiscussion createDiscussion;

    private final UpdateDiscussion updateDiscussion;

    private final DeleteDiscussion deleteDiscussion;

    private final GetAllDiscussion getAllDiscussion;

    public DiscussionApiImpl(CreateDiscussion createDiscussion, UpdateDiscussion updateDiscussion, DeleteDiscussion deleteDiscussion, GetAllDiscussion getAllDiscussion) {
        this.createDiscussion = createDiscussion;
        this.updateDiscussion = updateDiscussion;
        this.deleteDiscussion = deleteDiscussion;
        this.getAllDiscussion = getAllDiscussion;
    }

    @Override
    public ResponseEntity<Discussion> postDiscussion(Long target, Long targetId, Discussion discussion) {
        return createDiscussion.call(new CreateDiscussionIn()
                .targetType(target)
                .targetId(targetId)
                .discussion(RestModel.toDomainModel(discussion))
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(RestModel.toRestModel(out))
        );
    }

    @Override
    public ResponseEntity<Discussion> updateDiscussion(Long target, Long targetId, Long id, Discussion discussion) {
        return updateDiscussion.call(new UpdateDiscussionIn()
                .targetType(target)
                .targetId(targetId)
                .id(id)
                .discussion(RestModel.toDomainModel(discussion))
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(RestModel.toRestModel(out))
        );
    }

    @Override
    public ResponseEntity<DiscussionGetAll> getAllDiscussions(Long target, Long targetId, Integer page, Integer size, Boolean desc) {
        return getAllDiscussion.call(new GetAllDiscussionIn()
                .targetType(target)
                .targetId(targetId)
                .page(page)
                .size(size)
                .desc(desc == true)
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(new DiscussionGetAll().total(out.total())
                        .items(out.items().stream().map(RestModel::toRestModel).collect(Collectors.toList()))
                )
        );
    }

    @Override
    public ResponseEntity<BooleanResponse> deleteDiscussion(Long target, Long targetId, Long id) {
        return deleteDiscussion.call(new DeleteDiscussionIn()
                .targetType(target)
                .targetId(targetId)
                .id(id)
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(new BooleanResponse().result(out))
        );
    }
}
