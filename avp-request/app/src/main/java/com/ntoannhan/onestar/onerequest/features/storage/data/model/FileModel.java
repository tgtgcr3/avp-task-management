package com.ntoannhan.onestar.onerequest.features.storage.data.model;

import lombok.Data;
import org.springframework.core.io.Resource;

@Data
public class FileModel {

    private Resource resource;

    private String fileName;

}
