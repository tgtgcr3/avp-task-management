package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.AbstractBaseEntity;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity(name="RequestGroupForward")
public class RequestGroupForwardEntity extends AbstractBaseEntity implements ArrayStringTypeWrapper {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private int status;

    @ToString.Exclude
    @OneToOne
    private RequestGroupEntity requestGroup;

    @ToString.Exclude
    @OneToOne
    private RequestGroupEntity targetGroup;



}
