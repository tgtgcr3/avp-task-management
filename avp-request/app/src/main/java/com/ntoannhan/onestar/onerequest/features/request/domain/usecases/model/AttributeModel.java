package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.model;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;

public class AttributeModel {

    public static com.ntoannhan.onestar.onerequest.rest.model.Attribute toAttributeRest(Attribute attribute) {
        com.ntoannhan.onestar.onerequest.rest.model.Attribute attr = new com.ntoannhan.onestar.onerequest.rest.model.Attribute();
        attr.id(attribute.id());
        attr.attrType(attribute.attrType());
        attr.name(attribute.name());
        attr.fieldName(attribute.fieldName());
        attr.order(attribute.order());
        attr.required(attribute.isRequired());
        attr.additionData(attribute.additionData());
        attr.description(attribute.description());
        return attr;
    }

}
