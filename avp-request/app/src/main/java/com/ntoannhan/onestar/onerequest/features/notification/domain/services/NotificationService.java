package com.ntoannhan.onestar.onerequest.features.notification.domain.services;

import com.ntoannhan.onestar.onerequest.features.notification.domain.entities.NotificationTemplate;
import io.vavr.control.Either;

public interface NotificationService {

    Either<Exception, NotificationTemplate> createNotificationTemplate(NotificationTemplate notificationTemplate);

}
