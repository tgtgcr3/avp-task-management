package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class ApproveRequestIn {

    private long requestId;

    private RequestStatus requestStatus;

    private String comment;

    private List<String> targetUsers;

}
