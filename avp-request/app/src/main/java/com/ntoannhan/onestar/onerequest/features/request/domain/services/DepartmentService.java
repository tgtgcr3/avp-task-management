package com.ntoannhan.onestar.onerequest.features.request.domain.services;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Department;
import io.vavr.control.Either;

import java.util.List;

public interface DepartmentService {

    Either<Exception, List<Department>> getAll(boolean includeMembers);

}
