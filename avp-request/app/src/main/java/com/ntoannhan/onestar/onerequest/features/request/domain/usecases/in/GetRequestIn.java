package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class GetRequestIn {

    private long requestId;

    private boolean fullData;

}
