package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class Request {

    private Long id;

    private String name;

    private Long requestGroupId;

    private RequestStatus status;

    private String value;

    private String[] directMgmts;

    private String[] followers;

    private String[] documents;

    private String[] approvalRecords;

    private String submitter;

    private String[] approvers;

    private Long submitTime;

    private String requestGroupName;

    private String[] approveds;

    private String[] rejecteds;

    private String[] cancelleds;

    private String[] attrDocuments;

    private boolean favorite;

    private long remainingTime;

    private long timeout;

    private boolean hasRequestForm;

    private long lastUpdated;

    private List<Long> taskIds;

    private Long ogId;

    private String ogName;

    private boolean visited;

    public String[] getAttrDocuments() {
        return attrDocuments;
    }

    public void setAttrDocuments(String[] attrDocuments) {
        this.attrDocuments = attrDocuments;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isHasRequestForm() {
        return hasRequestForm;
    }

    public void setHasRequestForm(boolean hasRequestForm) {
        this.hasRequestForm = hasRequestForm;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public long getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(long remainingTime) {
        this.remainingTime = remainingTime;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String[] getCancelleds() {
        return cancelleds;
    }

    public void setCancelleds(String[] cancelleds) {
        this.cancelleds = cancelleds;
    }

    public String[] getApproveds() {
        return approveds;
    }

    public void setApproveds(String[] approveds) {
        this.approveds = approveds;
    }

    public String[] getRejecteds() {
        return rejecteds;
    }

    public void setRejecteds(String[] rejecteds) {
        this.rejecteds = rejecteds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getRequestGroupId() {
        return requestGroupId;
    }

    public void setRequestGroupId(Long requestGroupId) {
        this.requestGroupId = requestGroupId;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String[] getDirectMgmts() {
        return directMgmts;
    }

    public void setDirectMgmts(String[] directMgmts) {
        this.directMgmts = directMgmts;
    }

    public String[] getFollowers() {
        return followers;
    }

    public void setFollowers(String[] followers) {
        this.followers = followers;
    }

    public String[] getDocuments() {
        return documents;
    }

    public void setDocuments(String[] documents) {
        this.documents = documents;
    }

    public String[] getApprovalRecords() {
        return approvalRecords;
    }

    public void setApprovalRecords(String[] approvalRecords) {
        this.approvalRecords = approvalRecords;
    }

    public String getSubmitter() {
        return submitter;
    }

    public void setSubmitter(String submitter) {
        this.submitter = submitter;
    }

    public String[] getApprovers() {
        return approvers;
    }

    public void setApprovers(String[] approvers) {
        this.approvers = approvers;
    }

    public Long getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Long submitTime) {
        this.submitTime = submitTime;
    }

    public String getRequestGroupName() {
        return requestGroupName;
    }

    public void setRequestGroupName(String requestGroupName) {
        this.requestGroupName = requestGroupName;
    }

    public List<Long> getTaskIds() {
        return taskIds;
    }

    public void setTaskIds(List<Long> taskIds) {
        this.taskIds = taskIds;
    }

    public Long getOgId() {
        return ogId;
    }

    public void setOgId(Long ogId) {
        this.ogId = ogId;
    }

    public String getOgName() {
        return ogName;
    }

    public void setOgName(String ogName) {
        this.ogName = ogName;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }
}
