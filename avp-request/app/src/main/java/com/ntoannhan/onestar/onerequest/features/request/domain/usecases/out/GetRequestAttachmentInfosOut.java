package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out;

import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class GetRequestAttachmentInfosOut {

    private List<FileInfo> fileInfos;

}
