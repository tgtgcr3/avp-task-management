package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.UpdateRequestGroupIn;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.RolesAllowed;

@Component
public class UpdateRequestGroup implements UseCase<RequestGroup, UpdateRequestGroupIn> {

    @Autowired
    private RequestGroupService requestGroupService;

    @RolesAllowed({"admin"})
    @Override
    public Either<Exception, RequestGroup> call(UpdateRequestGroupIn in) {
        return requestGroupService.updateRequestGroup(in.id(), in.requestGroup());
    }
}
