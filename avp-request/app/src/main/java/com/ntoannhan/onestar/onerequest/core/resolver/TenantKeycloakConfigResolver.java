package com.ntoannhan.onestar.onerequest.core.resolver;

import com.ntoannhan.onestar.onerequest.core.properties.KeycloakProperty;
import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.spi.HttpFacade;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.representations.adapters.config.AdapterConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class TenantKeycloakConfigResolver extends KeycloakSpringBootConfigResolver {

    @Autowired
    private KeycloakProperty keycloakProperty;

    private final Map<String, KeycloakDeployment> keycloakDeploymentCache = new ConcurrentHashMap<>();

    @Override
    public KeycloakDeployment resolve(HttpFacade.Request request) {
        String realmName = request.getHeader("X-TENANT-ID");
        if (realmName == null) {
            realmName = "nothing";
        }
        KeycloakDeployment keycloakDeployment = keycloakDeploymentCache.get(realmName);
        if (keycloakDeployment == null) {

            AdapterConfig adapterConfig = new AdapterConfig();
            adapterConfig.setAuthServerUrl(keycloakProperty.getAuthServerUrl());
            adapterConfig.setResource(keycloakProperty.getResource());
            adapterConfig.setRealm(realmName);
            adapterConfig.setUseResourceRoleMappings(keycloakProperty.isUseResourceRoleMappings());
            adapterConfig.setBearerOnly(keycloakProperty.isBearerOnly());
            adapterConfig.setPrincipalAttribute(keycloakProperty.getPrincipalAttribute());
            adapterConfig.setCredentials(Map.of("secret", keycloakProperty.getSecret()));
            keycloakDeployment = KeycloakDeploymentBuilder.build(adapterConfig);
            keycloakDeploymentCache.put(realmName, keycloakDeployment);
        }

        return keycloakDeployment;
    }

}
