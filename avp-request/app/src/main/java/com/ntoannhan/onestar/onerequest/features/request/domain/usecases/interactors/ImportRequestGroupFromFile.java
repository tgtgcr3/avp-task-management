package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.ImportRequestGroupFromFileIn;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImportRequestGroupFromFile implements UseCase<Boolean, ImportRequestGroupFromFileIn> {

    private final RequestGroupService requestGroupService;

    @Autowired
    public ImportRequestGroupFromFile(RequestGroupService requestGroupService) {
        this.requestGroupService = requestGroupService;
    }


    @Override
    public Either<Exception, Boolean> call(ImportRequestGroupFromFileIn in) {
        return requestGroupService.importFromFile(in.inputStream(), in.contentType());
    }
}
