package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(fluent = true)
public class RequestGroup {

    private Long id;

    private String name;

    private int mgmtNotification;

    private Long approveType;

    private String category;

    private Long sla;

    private List<String> followers;

    private List<String> approvers;

    private String description;

    private List<Attribute> attrs;

    private RequestGroupStatus status;

    private Long targetId;

    private Long targetType;

    private int requestForm;

    private String creator;

    private List<String> requestForms;

    private String approvalOwner;

    private Long oId;

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMgmtNotification() {
        return mgmtNotification;
    }

    public void setMgmtNotification(int mgmtNotification) {
        this.mgmtNotification = mgmtNotification;
    }

    public Long getApproveType() {
        return approveType;
    }

    public void setApproveType(Long approveType) {
        this.approveType = approveType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getSla() {
        return sla;
    }

    public void setSla(Long sla) {
        this.sla = sla;
    }

    public List<String> getFollowers() {
        return followers;
    }

    public void setFollowers(List<String> followers) {
        this.followers = followers;
    }

    public List<String> getApprovers() {
        return approvers;
    }

    public void setApprovers(List<String> approvers) {
        this.approvers = approvers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Attribute> getAttrs() {
        return attrs;
    }

    public void setAttrs(List<Attribute> attrs) {
        this.attrs = attrs;
    }

    public RequestGroupStatus getStatus() {
        return status;
    }

    public void setStatus(RequestGroupStatus status) {
        this.status = status;
    }

    public Long getTargetId() {
        return targetId;
    }

    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    public Long getTargetType() {
        return targetType;
    }

    public void setTargetType(Long targetType) {
        this.targetType = targetType;
    }

    public int getRequestForm() {
        return requestForm;
    }

    public void setRequestForm(int requestForm) {
        this.requestForm = requestForm;
    }

    public List<String> getRequestForms() {
        return requestForms;
    }

    public void setRequestForms(List<String> requestForms) {
        this.requestForms = requestForms;
    }

    public String getApprovalOwner() {
        return approvalOwner;
    }

    public void setApprovalOwner(String approvalOwner) {
        this.approvalOwner = approvalOwner;
    }

    public Long getoId() {
        return oId;
    }

    public void setoId(Long oId) {
        this.oId = oId;
    }
}
