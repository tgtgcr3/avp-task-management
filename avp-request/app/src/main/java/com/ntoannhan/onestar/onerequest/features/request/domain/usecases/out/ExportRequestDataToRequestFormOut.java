package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.core.io.Resource;

@Data
@Accessors(fluent = true)
public class ExportRequestDataToRequestFormOut {

    private Resource resource;

}
