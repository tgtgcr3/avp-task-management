package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model;

import com.ntoannhan.onestar.onerequest.core.presentation.model.rest.BaseRestModel;
import org.modelmapper.ModelMapper;

public abstract class AbstractBaseRestModel<Domain, Rest> implements BaseRestModel<Domain, Rest> {

}
