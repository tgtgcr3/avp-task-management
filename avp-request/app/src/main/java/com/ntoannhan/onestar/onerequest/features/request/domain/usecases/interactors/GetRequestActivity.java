package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestActivity;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetRequestActivity implements UseCase<List<RequestActivity>, Long> {

    private final RequestService requestService;

    @Autowired
    public GetRequestActivity(RequestService requestService) {
        this.requestService = requestService;
    }

    @Override
    public Either<Exception, List<RequestActivity>> call(Long in) {
        return requestService.getRequestActivities(in);
    }
}
