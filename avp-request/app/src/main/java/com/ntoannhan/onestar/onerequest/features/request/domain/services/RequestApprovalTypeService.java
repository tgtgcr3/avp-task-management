package com.ntoannhan.onestar.onerequest.features.request.domain.services;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestApprovalType;
import io.vavr.control.Either;

import java.util.List;

public interface RequestApprovalTypeService {

    Either<Exception, List<RequestApprovalType>> getAll();

}
