package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.model;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestApprovalType;

public class RequestApprovalTypeModel extends RequestApprovalType {

    public static RequestApprovalTypeModel fromEntity(RequestApprovalType domainType) {
        RequestApprovalTypeModel model = new RequestApprovalTypeModel();

        model.id(domainType.id());
        model.name(domainType.name());

        return model;
    }

}
