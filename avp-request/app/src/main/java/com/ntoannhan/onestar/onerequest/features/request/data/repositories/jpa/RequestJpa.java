package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections.BasicRequestData;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections.BasicTimeoutRequest;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.projections.GeneralReportDataProjection;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
public interface RequestJpa extends JpaRepository<RequestEntity, Long> {

    @Query("SELECT r FROM Request r WHERE r.id = :id")
    @Override
    Optional<RequestEntity> findById(Long id);

    @Query("SELECT r.id as id, r.name as name, r.status as status, r.approvers as requestApprovers, r.requestGroupVersionEntity.approvers as approvers, r.submitter as submitter, r.submitTime as submitTime, r.directMgmts as directMgmts, r.requestGroupVersionEntity.requestGroup.name as requestGroupName, r.requestGroupVersionEntity.id as groupId " +
            "FROM Request r WHERE " +
            "r.name LIKE %:name% AND (:groupId IS NULL OR r.requestGroupVersionEntity.requestGroup.id = :groupId) AND r.status <> 11 AND " +
            "(COALESCE(:creators, CAST(NULL AS int)) IS NULL OR r.submitter IN :creators) AND " +
            "(:start IS NULL OR (r.submitTime >= :start AND r.submitTime < :end)) AND " +
            "((:toMe IS NULL OR (:toMe = TRUE AND (r.requestGroupVersionEntity.followers LIKE %:aUsername% OR r.requestGroupVersionEntity.approvers LIKE %:aUsername% OR :username = r.requestGroupVersionEntity.approvalOwner OR r.directMgmts LIKE %:aUsername% OR r.approvers LIKE %:aUsername%))) OR " +
            "(:fromMe IS NULL OR (:fromMe = TRUE AND :username = r.submitter)) OR " +
            "(:followed IS NULL OR (:followed = TRUE AND (r.requestGroupVersionEntity.followers LIKE %:aUsername% OR r.followers LIKE %:aUsername%)))) AND " +
            "(:status IS NULL OR (r.status = :status))")
    List<BasicRequestData> findAllWithBasicData(String name, Long groupId, String username, String aUsername, Boolean toMe, Boolean fromMe, Boolean followed, Integer status, List<String> creators, Long start, Long end, Pageable pageable);

    @Query("SELECT COUNT(r) FROM Request r WHERE " +
            "r.name LIKE %:name% AND (:groupId IS NULL OR r.requestGroupVersionEntity.requestGroup.id = :groupId) AND r.status <> 11 AND " +
            "(COALESCE(:creators, CAST(NULL AS int)) IS NULL OR r.submitter IN :creators) AND " +
            "(:start IS NULL OR (r.submitTime >= :start AND r.submitTime < :end)) AND " +
            "((:toMe IS NULL OR (:toMe = TRUE AND (r.requestGroupVersionEntity.followers LIKE %:aUsername% OR r.requestGroupVersionEntity.approvers LIKE %:aUsername% OR :username = r.requestGroupVersionEntity.approvalOwner OR r.directMgmts LIKE %:aUsername% OR r.approvers LIKE %:aUsername% OR " +
            "(SELECT COUNT(f) FROM RequestForward f WHERE f.request.id = r.id AND f.targetUsers LIKE %:aUsername%) > 0))) OR " +
            "(:fromMe IS NULL OR (:fromMe = TRUE AND :username = r.submitter)) OR " +
            "(:followed IS NULL OR (:followed = TRUE AND (r.requestGroupVersionEntity.followers LIKE %:aUsername% OR r.followers LIKE %:aUsername%)))) AND " +
            "(:status IS NULL OR (r.status = :status))")
    int countAllWithBasicData(String name, Long groupId, String username, String aUsername, Boolean toMe, Boolean fromMe, Boolean followed, Integer status, List<String> creators, Long start, Long end);

    @Query("SELECT r FROM Request r WHERE " +
            "r.name LIKE %:name% AND (:groupId IS NULL OR r.requestGroupVersionEntity.requestGroup.id = :groupId) AND r.status <> 11 AND " +
            "(COALESCE(:creators, CAST(NULL AS int)) IS NULL OR r.submitter IN :creators) AND " +
            "(:start IS NULL OR (r.submitTime >= :start AND r.submitTime < :end)) AND " +
            "((:toMe IS NULL OR (:toMe = TRUE AND (r.requestGroupVersionEntity.followers LIKE %:aUsername% OR r.requestGroupVersionEntity.approvers LIKE %:aUsername% OR :username = r.requestGroupVersionEntity.approvalOwner OR r.directMgmts LIKE %:aUsername% OR r.approvers LIKE %:aUsername% OR " +
            "(SELECT COUNT(f) FROM RequestForward f WHERE f.request.id = r.id AND f.targetUsers LIKE %:aUsername%) > 0))) OR " +
            "(:fromMe IS NULL OR (:fromMe = TRUE AND :username = r.submitter)) OR " +
            "(:followed IS NULL OR (:followed = TRUE AND (r.requestGroupVersionEntity.followers LIKE %:aUsername% OR r.followers LIKE %:aUsername%)))) AND " +
            "(:status IS NULL OR (r.status = :status))")
    List<RequestEntity> findAllWithFullData(String name, Long groupId, String username, String aUsername, Boolean toMe, Boolean fromMe, Boolean followed, Integer status, List<String> creators, Long start, Long end, Pageable pageable);

    @Transactional
    @Modifying
    @Query("UPDATE Request r SET r.status = 2 WHERE (r.status = 0 OR r.status = 4) AND r.timeout > 0 AND r.timeout < :currentTime")
    int updateRequestTimeout(long currentTime);

    @Transactional
    @Query("SELECT r FROM Request r WHERE (r.status = 0 OR r.status = 4) AND r.timeout > 0 AND r.timeout < :currentTime")
    List<RequestEntity> getRequestTimeout(long currentTime);

    @Query("SELECT COUNT(r) as total, r.status as status FROM Request r WHERE ((:groupId IS NULL OR r.requestGroupVersionEntity.requestGroup.id = :groupId) AND (:startDate IS NULL OR (:startDate <= r.submitTime AND r.submitTime < :endDate))) GROUP BY r.status")
    List<GeneralReportDataProjection> countRequestByStatus(Long groupId, Long startDate, Long endDate);
}
