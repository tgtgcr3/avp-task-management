package com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class GetAllDiscussionIn {

    private long targetType;

    private long targetId;

    private int page;

    private int size;

    private boolean desc;

}
