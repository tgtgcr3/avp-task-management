package com.ntoannhan.onestar.onerequest.features.request.data.repositories.services;

import com.ntoannhan.onestar.onerequest.core.context.TenantContext;
import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.core.properties.KeycloakProperty;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
@Service
public class KeycloakService {

    private KeycloakProperty keycloakProperty;

    private final Map<String, Keycloak> keycloakCache = new ConcurrentHashMap<>();

    @Autowired
    public KeycloakService(KeycloakProperty keycloakProperty) {
        this.keycloakProperty = keycloakProperty;
    }

    @Cacheable("keycloak-alluser")
    public List<UserRepresentation> getAllUsers() {
        try {
            List<UserRepresentation> list = getKeycloak().realm(TenantContext.getTenantId()).users().list(0, 10240);
            log.error("Size of getAllUsers: {}", list.size());
            return list;
        } catch (Exception ex) {
            log.error("Cannot retrieve user from Keycloak", ex);
        }
        return null;
    }

    private Keycloak getKeycloak() {
        String realmName = TenantContext.getTenantId();
        Keycloak keycloak = keycloakCache.get(realmName);
        if (keycloak == null) {
                    keycloak = KeycloakBuilder.builder().serverUrl(keycloakProperty.getAuthServerUrl())
                .realm(realmName)
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientId(keycloakProperty.getResource())
                .clientSecret(keycloakProperty.getSecret())
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build()).build();
                    keycloakCache.put(realmName, keycloak);
        }
        return keycloak;
    }

    public String getFullName(UserRepresentation user) {
        return user.getLastName().trim() + " " + user.getFirstName().trim();
    }

}
