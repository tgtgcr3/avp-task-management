package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

public enum RequestActivityAction {
    Created(0), Updated(1), Rejected(2), Approved(3), Moved(4), Cancelled(5);

    private final int value;

    RequestActivityAction(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static RequestActivityAction fromId(int id) {
        RequestActivityAction[] values = values();
        for (RequestActivityAction requestStatus : values) {
            if (requestStatus.value == id) {
                return requestStatus;
            }
        }
        return null;
    }
}
