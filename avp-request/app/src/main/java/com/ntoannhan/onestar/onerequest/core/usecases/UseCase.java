package com.ntoannhan.onestar.onerequest.core.usecases;

import io.vavr.control.Either;

public interface UseCase <T, I>{

    Either<Exception, T> call(I in);

}
