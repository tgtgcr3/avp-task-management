package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestApproval;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class GetRequestApprovals implements UseCase<Map<Long, RequestApproval>, List<Long>> {

    private final RequestService requestService;

    public GetRequestApprovals(RequestService requestService) {
        this.requestService = requestService;
    }

    @Override
    public Either<Exception, Map<Long, RequestApproval>> call(List<Long> in) {
        return requestService.getRequestApprovalInfos(in);
    }
}
