package com.ntoannhan.onestar.onerequest.features.notification.data.repositories.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity(name = "NotificationTemplate")
public class NotificationTemplateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String appName;

    private String category;

    private int notificationType;

    @Column(columnDefinition = "TEXT")
    private String template;
}
