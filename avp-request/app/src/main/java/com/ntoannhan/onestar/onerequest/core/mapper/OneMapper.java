package com.ntoannhan.onestar.onerequest.core.mapper;

import org.modelmapper.ModelMapper;

public class OneMapper {

    private static ModelMapper modelMapper;

    public static void setMapper(ModelMapper modelMapper) {
        OneMapper.modelMapper = modelMapper;
    }

    public static <O, T> T mapTo(O o, Class<T> clazz) {
        return modelMapper.map(o, clazz);
    }

    public static <O, T> T mapTo(O o, T t) {
        modelMapper.map(o, t);
        return t;
    }

}
