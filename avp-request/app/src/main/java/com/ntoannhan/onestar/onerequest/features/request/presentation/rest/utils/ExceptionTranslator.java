package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils;

import com.ntoannhan.onestar.onerequest.core.exception.UserException;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ExceptionTranslator {

    public static ResponseStatusException getFailureException(Exception ex){
        if (ex instanceof UserException) {
            return new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
        return new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    }

}
