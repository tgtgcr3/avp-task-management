package com.ntoannhan.onestar.onerequest.features.discussion.presentation.services;

import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.Discussion;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in.CreateDiscussionIn;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.interceptor.CreateSystemDiscussion;
import io.vavr.control.Either;
import org.springframework.stereotype.Service;

@Service
public class DiscussionService {

    private final CreateSystemDiscussion createSystemDiscussion;

    public DiscussionService(CreateSystemDiscussion createSystemDiscussion) {
        this.createSystemDiscussion = createSystemDiscussion;
    }

    public Either<Exception, Discussion> createSystemDiscussion(long targetType, long targetId, Discussion discussion) {
        return createSystemDiscussion.call(new CreateDiscussionIn().targetType(targetType).targetId(targetId).discussion(discussion));
    }

}
