package com.ntoannhan.onestar.onerequest.features.request.data.services;

import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.features.request.data.models.RequestApprovalTypeDataModel;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestApprovalTypeEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.RequestApprovalTypeJpa;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestApprovalType;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestApprovalTypeService;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Component
public class RequestApprovalTypeServiceImpl implements RequestApprovalTypeService {

    @Autowired
    private RequestApprovalTypeJpa requestApprovalTypeJpa;

    @Override
    public Either<Exception, List<RequestApprovalType>> getAll() {
        try {
            List<RequestApprovalTypeEntity> entities = requestApprovalTypeJpa.findAll();
            return Either.right(entities.stream().map(RequestApprovalTypeDataModel::fromEntity).collect(Collectors.toList()));
        } catch (Exception ex) {
            log.error("Cannot get all RequestApprovalType", ex);
            return Either.left(new AppException("cannot_get_all_approval_type"));
        }
    }
}
