package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.handler;

import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.*;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors.*;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model.OneRequestRestModel;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model.RestModelMapper;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.features.storage.presentation.rest.model.FileInfoModel;
import com.ntoannhan.onestar.onerequest.rest.api.RequestApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.*;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class RequestApiDelegateImpl implements RequestApiDelegate {

    private final CreateRequest createRequest;

    private final GetRequest getRequest;

    private final GetAllRequests getAllRequests;

    private final DownloadRequestAttachment downloadRequestAttachment;

    private final UpdateRequest updateRequest;

    private final ExportRequestDataToRequestForm exportRequestDataToRequestForm;

    private final GetRequestActivity getRequestActivity;

    private final ApproveRequest approveRequest;

    private final GetRequestAttachmentInfo getRequestAttachmentInfo;

    private final GetRequestApprovals getRequestApprovals;

    private final AddRequestFollowers addRequestFollowers;

    private final RemoveRequestFollowers removeRequestFollowers;

    private final RemoveRequest removeRequest;

    private final VisitRequest visitRequest;

    private final ExportAllRequests exportAllRequest;

    public RequestApiDelegateImpl(CreateRequest createRequest, GetRequest getRequest, GetAllRequests getAllRequests,
                                  DownloadRequestAttachment downloadRequestAttachment, UpdateRequest updateRequest, ExportRequestDataToRequestForm exportRequestDataToRequestForm, GetRequestActivity getRequestActivity, ApproveRequest approveRequest, GetRequestAttachmentInfo getRequestAttachmentInfo, GetRequestApprovals getRequestApprovals, AddRequestFollowers addRequestFollowers, RemoveRequestFollowers removeRequestFollowers, RemoveRequest removeRequest, VisitRequest visitRequest, ExportAllRequests exportAllRequest, AddRequestFavorite addRequestFavorite, RemoveRequestFavorite removeRequestFavorite) {
        this.createRequest = createRequest;
        this.getRequest = getRequest;
        this.getAllRequests = getAllRequests;
        this.downloadRequestAttachment = downloadRequestAttachment;
        this.updateRequest = updateRequest;
        this.exportRequestDataToRequestForm = exportRequestDataToRequestForm;
        this.getRequestActivity = getRequestActivity;
        this.approveRequest = approveRequest;
        this.getRequestAttachmentInfo = getRequestAttachmentInfo;
        this.getRequestApprovals = getRequestApprovals;
        this.addRequestFollowers = addRequestFollowers;
        this.removeRequestFollowers = removeRequestFollowers;
        this.removeRequest = removeRequest;
        this.visitRequest = visitRequest;
        this.exportAllRequest = exportAllRequest;
        this.addRequestFavorite = addRequestFavorite;
        this.removeRequestFavorite = removeRequestFavorite;
    }

    @Override
    public ResponseEntity<OneRequest> createRequest(OneRequest oneRequest) {
        OneRequestRestModel oneRequestRestModel = new OneRequestRestModel();
        return createRequest.call(new CreateRequestIn().request(oneRequestRestModel.toDomainModel(oneRequest))).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(oneRequestRestModel.toRestModel(out.request()))

        );
    }

    @Override
    public ResponseEntity<BooleanResponse> updateRequestStatus(Long id, RequestStatusBody requestStatusBody) {
        return approveRequest.call(new ApproveRequestIn().requestId(id)
                .targetUsers(requestStatusBody.getTargetUsers())
                .requestStatus(RestModelMapper.toRequestStatus(requestStatusBody.getStatus()))
                .comment(requestStatusBody.getDescription())).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(new BooleanResponse().result(Boolean.TRUE))
        );

    }

    @Override
    public ResponseEntity<OneRequest> getRequest(Long id, Boolean fullData) {
        OneRequestRestModel oneRequestRestModel = new OneRequestRestModel();
        return getRequest.call(new GetRequestIn().requestId(id).fullData(fullData)).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(oneRequestRestModel.toRestModel(out.request()))
        );
    }

    @Override
    public ResponseEntity<RequestGetAllResult> getAllRequests(String search,
                                                              Integer page,
                                                              Integer size,
                                                              Boolean fullData,
                                                              Long requestGroup,
                                                              String status,
                                                              Boolean toMe,
                                                              Boolean fromMe,
                                                              Boolean followed,
                                                              Boolean favorite,
                                                              Long createdDate,
                                                              Long end,
                                                              List<String> creators) {
                OneRequestRestModel oneRequestRestModel = new OneRequestRestModel();
        return getAllRequests.call(new GetAllRequestsIn().search(search).page(page).size(size).fullData(fullData).requestGroupId(requestGroup)
                .requestStatus(convertRequestStatusForSearch(status))
                .toMe(toMe).fromMe(fromMe).followed(followed).favorite(favorite)
                .creators(creators)
                .createdDate(createdDate)
                .end(end)
        )
                .fold(ex -> {
                            throw ExceptionTranslator.getFailureException(ex);
                        },
                        out -> ResponseEntity.ok(new RequestGetAllResult()
                                .total(out.total())
                                .items(out.items().stream().map(oneRequestRestModel::toRestModel).collect(Collectors.toList()))
                        )
                );
    }

    @Override
    public ResponseEntity<List<FileInfo>> getAttachmentInfo(Long id, List<Long> fileId) {
        return getRequestAttachmentInfo.call(new GetRequestAttachmentInfosIn().id(id).fileIds(fileId)).fold(
                ex -> {throw ExceptionTranslator.getFailureException(ex);},
                out -> ResponseEntity.ok(out.fileInfos().stream().map(fileInfo -> new FileInfoModel().toRestModel(fileInfo)).collect(Collectors.toList()))
        );
    }

    private RequestStatus convertRequestStatusForSearch(String status) {
        if (status == null) {
             return null;
        }
        if ("Pending".equalsIgnoreCase(status)) {
            return RequestStatus.PENDING;
        }
        if ("Approved".equalsIgnoreCase(status)) {
            return RequestStatus.APPROVED;
        }
        if ("Rejected".equalsIgnoreCase(status)) {
            return RequestStatus.REJECTED;
        }
        if ("Timeout".equalsIgnoreCase(status)) {
            return RequestStatus.TIMEOUT;
        }
        if ("Cancelled".equalsIgnoreCase(status)) {
            return RequestStatus.CANCELLED;
        }
        return null;
    }

    @Override
    public ResponseEntity<Resource> getAttachment(Long id, Long fileId) {
        return downloadRequestAttachment.call(new DownloadRequestAttachmentIn().requestId(id).fileId(fileId))
                .fold(
                        ex -> {
                            throw ExceptionTranslator.getFailureException(ex);
                        },
                        out -> ResponseEntity.ok(out.resource())
                );
    }

    @Override
    public ResponseEntity<OneRequest> updateRequest(Long id, OneRequest oneRequest) {
        OneRequestRestModel oneRequestRestModel = new OneRequestRestModel();
        return updateRequest.call(new UpdateRequestIn().id(id).request(oneRequestRestModel.toDomainModel(oneRequest))).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(oneRequestRestModel.toRestModel(out.request()))
        );
    }

    @Override
    public ResponseEntity<Resource> exportRequestForm(Long id) {
        return exportRequestDataToRequestForm.call(new ExportRequestDataToRequestFormIn().requestId(id)).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> {
                    return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.wordprocessingml.document")).body(out.resource());
                });

    }

    @Override
    public ResponseEntity<List<RequestActivity>> getActivityLogs(Long id) {
        return getRequestActivity.call((id)).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> {
                    return ResponseEntity.ok(out.stream().map(o -> {
                        RequestActivity requestActivity = OneMapper.mapTo(o, RequestActivity.class);
                        requestActivity.setDate(o.getDate());
                        requestActivity.setAction(convertToAction(o.getAction()));
                        return requestActivity;
                    }).collect(Collectors.toList()));
                });
    }

    @Override
    public ResponseEntity<Map<String, RequestApproval>> getRequestAcitivyLogs(List<Long> id) {
        return getRequestApprovals.call(id).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(out.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().toString(), e -> OneMapper.mapTo(e.getValue(), RequestApproval.class))))
        );
    }

    private final AddRequestFavorite addRequestFavorite;

    @Override
    public ResponseEntity<BooleanResponse> addRequestFavorite(Long id) {
        return addRequestFavorite.call(id).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(new BooleanResponse().result(Boolean.TRUE))
        );
    }

    private final RemoveRequestFavorite removeRequestFavorite;

    @Override
    public ResponseEntity<BooleanResponse> removeRequestFavorite(Long id) {
        return removeRequestFavorite.call(id).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(new BooleanResponse().result(Boolean.TRUE))
        );
    }

    @Override
    public ResponseEntity<OneRequest> addRequestFollowers(Long id, List<String> requestBody) {
        OneRequestRestModel oneRequestRestModel = new OneRequestRestModel();
        return addRequestFollowers.call(new AddRequestFollowersIn()
                .requestId(id).followers(requestBody)
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(oneRequestRestModel.toRestModel(out))
        );
    }


    @Override
    public ResponseEntity<OneRequest> removeRequestFollowers(Long id, List<String> requestBody) {
        OneRequestRestModel oneRequestRestModel = new OneRequestRestModel();
        return removeRequestFollowers.call(new RemoveRequestFollowersIn()
                .requestId(id).followers(requestBody)
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(oneRequestRestModel.toRestModel(out))
        );
    }

    @Override
    public ResponseEntity<BooleanResponse> removeRequest(Long id) {
        return removeRequest.call(id).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(new BooleanResponse().result(out))
        );
    }

    private RequestActivity.ActionEnum convertToAction(RequestStatus action) {
        if (action == null) {
            return null;
        }
        switch (action) {
            case APPROVED:
                return RequestActivity.ActionEnum.APPROVED;
            case REJECTED:
                return RequestActivity.ActionEnum.REJECTED;
            case CANCELLED:
                return RequestActivity.ActionEnum.CANCELLED;
            case AGREED_FORWARD:
                return RequestActivity.ActionEnum.AGREED_FORWARD;
            case FORWARD:
                return RequestActivity.ActionEnum.FORWARD;
            case TRANSFER:
                return RequestActivity.ActionEnum.TRANSFERED;
        }
        return null;
    }

    @Override
    public ResponseEntity<BooleanResponse> visitRequest(Long id) {
        return visitRequest.call(id).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(new BooleanResponse().result(Boolean.TRUE))
        );
    }



    @Override
    public ResponseEntity<Resource> exportAllRequests(String search, Integer page, Integer size, Boolean fullData, Long requestGroup, String status, Boolean toMe, Boolean fromMe, Boolean followed, Boolean favorite, Long createdDate, Long end, List<String> creators) {
        return exportAllRequest.call(new ExportAllRequestsIn().search(search).page(page).size(size).fullData(fullData).requestGroupId(requestGroup)
                .requestStatus(convertRequestStatusForSearch(status))
                .toMe(toMe).fromMe(fromMe).followed(followed).favorite(favorite)
                .creators(creators)
                .createdDate(createdDate)
                .end(end)
        )
                .fold(ex -> {
                            throw ExceptionTranslator.getFailureException(ex);
                        },
                        out -> ResponseEntity.ok().contentType(MediaType.parseMediaType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")).body(out.resource())

                );
        }

}
