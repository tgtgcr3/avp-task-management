package com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class GetFileInfosIn {

    private List<Long> fileIds;

}
