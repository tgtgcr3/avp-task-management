package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestApprovalService;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.ApproveRequestIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.ApproveRequestOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApproveRequest implements UseCase<ApproveRequestOut, ApproveRequestIn> {

    private final RequestApprovalService requestApprovalService;

    @Autowired
    public ApproveRequest(RequestApprovalService requestApprovalService) {
        this.requestApprovalService = requestApprovalService;
    }

    @Override
    public Either<Exception, ApproveRequestOut> call(ApproveRequestIn in) {
        return requestApprovalService.evaluateRequest(in.requestId(), in.requestStatus(), in.targetUsers(),in.comment()).fold(Either::left,
                out -> Either.right(new ApproveRequestOut()));
    }
}
