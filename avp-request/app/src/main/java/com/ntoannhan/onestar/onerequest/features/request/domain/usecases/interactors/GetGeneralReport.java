package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.ReportService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetGeneralReportIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetGeneralReportOut;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class GetGeneralReport implements UseCase<GetGeneralReportOut, GetGeneralReportIn> {

    private final ReportService reportService;

    public GetGeneralReport(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    public Either<Exception, GetGeneralReportOut> call(GetGeneralReportIn in) {
        return reportService.generateGeneralReport(in.groupId(), in.fromDate(), in.toDate()).fold(Either::left, out -> Either.right(new GetGeneralReportOut().generalReportData(out)));
    }
}
