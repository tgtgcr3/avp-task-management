package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.ExportAllRequestsIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.ExportRequestDataToRequestFormOut;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetAllRequestsOut;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class ExportAllRequests implements UseCase<ExportRequestDataToRequestFormOut, ExportAllRequestsIn> {

    private final RequestService requestService;

    public ExportAllRequests(RequestService requestService) {
        this.requestService = requestService;
    }

    @Override
    public Either<Exception, ExportRequestDataToRequestFormOut> call(ExportAllRequestsIn in) {
        return requestService.exportAllRequest(in.search(), in.page() == null ? 0 : in.page(), in.size() == null ? 100 : in.size(), in.fullData(), in.requestGroupId(),
                in.toMe(), in.fromMe(), in.followed(), in.requestStatus(), in.favorite(),
                in.creators(), in.createdDate(), in.end()
        )
                .fold(Either::left, resource -> Either.right(new ExportRequestDataToRequestFormOut().resource(resource)));
    }

}
