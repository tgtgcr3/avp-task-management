package com.ntoannhan.onestar.onerequest.interfaces.config;

import lombok.Data;

@Data
public class RequestConfiguration {

    private int workingDay;

    private long morningTime;

    private long lunchTime;

    private long noonTime;

    private long endTime;

    private long endTimeMargin;
}
