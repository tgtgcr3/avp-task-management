package com.ntoannhan.onestar.onerequest.core.exception;

import lombok.Data;

@Data
public class UncheckedException extends RuntimeException{

    private int code;

    public UncheckedException() {
    }

    public UncheckedException(int code) {
        this.code = code;
    }

    public UncheckedException(String message) {
        super(message);
    }

    public UncheckedException(int code, String message) {
        super(message);
        this.code = code;
    }

}
