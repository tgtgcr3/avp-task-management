package com.ntoannhan.onestar.onerequest.features.discussion.domain.entities;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;

public enum DiscussionStatus {
    NEW(0), EDITTED(1), DELETED(2), READ_ONLY(3);

    private final int value;

    DiscussionStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static DiscussionStatus fromId(int id) {
        DiscussionStatus[] values = values();
        for (DiscussionStatus requestStatus : values) {
            if (requestStatus.value == id) {
                return requestStatus;
            }
        }
        return null;
    }



}
