package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.handler;

import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestApprovalTypeService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetRequestApprovalTypesIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors.GetRequestApprovalTypes;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model.RequestApprovalTypeModel;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.rest.api.RequestApprovalTypeApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.RequestApprovalTypeGetAllResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ApprovalTypeApiDelegateImpl implements RequestApprovalTypeApiDelegate {

    @Autowired
    private GetRequestApprovalTypes getRequestApprovalTypes;

    @Override
    public ResponseEntity<RequestApprovalTypeGetAllResult> getAllRequestApprovalTypes() {
        return getRequestApprovalTypes.call(new GetRequestApprovalTypesIn()).fold(ex -> {throw ExceptionTranslator.getFailureException(ex);},
                data -> ResponseEntity.ok(new RequestApprovalTypeGetAllResult()
                        .total(data.total())
                        .items(data.items().stream().map(RequestApprovalTypeModel::fromDomainModel).collect(Collectors.toList()))));
    }
}
