package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestApprovalRecordEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.projections.DARequestApproval;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface RequestApprovalJpa extends JpaRepository<RequestApprovalRecordEntity, Long> {

    @Override
    @Query("SELECT r FROM RequestApprovalRecord r WHERE r.id = :id")
    Optional<RequestApprovalRecordEntity> findById(Long id);

    @Query("SELECT r FROM RequestApprovalRecord r WHERE r.request.id = :requestId")
    List<RequestApprovalRecordEntity> findAllByRequestId(Long requestId, Pageable pageable);

    @Query("SELECT r FROM RequestApprovalRecord r WHERE r.request.id = :requestId AND r.decision = 5")
    List<RequestApprovalRecordEntity> findAllByApprovedRequestId(Long requestId, Pageable pageable);

    @Query("SELECT r.request.id as requestId, r.decision as decision, r.approver as approver, r.submittedTime as submittedTime, r.comment as comment FROM RequestApprovalRecord r WHERE r.request.id IN :requestIds")
    List<DARequestApproval> getRequestApprovalRecords(List<Long> requestIds);

    @Query(value = "SELECT approver FROM request_approval_record GROUP BY approver HAVING COUNT(approver) = (" +
            "SELECT MAX(t.approvercount) FROM " +
            "(SELECT approver, COUNT(approver) approvercount FROM request_approval_record GROUP BY approver) as t )", nativeQuery = true)
    String findHighActiveApprover();

}
