package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.stream.Collectors;

@Data
@Accessors(fluent = true)
public class UpdateRequestGroupIn {

    private long id;

    private RequestGroup requestGroup;
}
