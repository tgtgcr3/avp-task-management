package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Request;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupStatus;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import com.ntoannhan.onestar.onerequest.rest.model.OneRequest;
import com.ntoannhan.onestar.onerequest.rest.model.RequestGroup;
import com.ntoannhan.onestar.onerequest.rest.model.RequestStatusBody;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RestModelMapper {

    @Autowired
    public RestModelMapper(ModelMapper modelMapper) {
        addRequestGroupMapping(modelMapper);

        addRequestMapping(modelMapper);

    }

    private void addRequestGroupMapping(ModelMapper modelMapper) {
        modelMapper.addMappings(new RestToDomainRequestGroupMapping());
        modelMapper.addMappings(new DomainToRestRequestGroupMapping());
    }

    static class RestToDomainRequestGroupMapping extends PropertyMap<RequestGroup, com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup> {

        @Override
        protected void configure() {
            map().setStatus(toRequestGroupStatus(source.getStatus()));
        }
    }

    static class DomainToRestRequestGroupMapping extends PropertyMap<com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroup, RequestGroup> {

        @Override
        protected void configure() {
            map().status(toRequestGroupStatusEnum(source.status()));
        }
    }

    public static RequestGroupStatus toRequestGroupStatus(com.ntoannhan.onestar.onerequest.rest.model.RequestGroup.StatusEnum statusEnum) {
        if (statusEnum == null) {
            return null;
        }
        switch (statusEnum) {
            case DISABLED:
                return RequestGroupStatus.DISABLED;
            case DELETED:
                return RequestGroupStatus.DELETED;
            default:
                return RequestGroupStatus.ENABLED;
        }
    }

    public static RequestGroup.StatusEnum toRequestGroupStatusEnum(RequestGroupStatus status) {
        if (status == null) {
            return null;
        }
        switch (status) {
            case DISABLED:
                return RequestGroup.StatusEnum.DISABLED;
            case DELETED:
                return RequestGroup.StatusEnum.DELETED;
            default:
                return RequestGroup.StatusEnum.ENABLED;
        }
    }

    private void addRequestMapping(ModelMapper modelMapper) {
        modelMapper.addMappings(new RestRequestToDomainMapping());
        modelMapper.addMappings(new DomainRequestToRestMapping());
    }

    private static class RestRequestToDomainMapping extends PropertyMap<OneRequest, Request> {

        @Override
        protected void configure() {
            map().status(toRequestStatus(source.getStatus()));
        }
    }

    private static class DomainRequestToRestMapping extends PropertyMap<Request, OneRequest> {

        @Override
        protected void configure() {
            map().status(toRequestStatusEnum(source.status()));
        }
    }

    private static RequestStatus toRequestStatus(OneRequest.StatusEnum statusEnum) {
        if (statusEnum == null) {
            return null;
        }
        switch (statusEnum) {
            case APPROVED:
                return RequestStatus.APPROVED;
            case CANCELLED:
                return RequestStatus.CANCELLED;
            case CLOSED:
                return RequestStatus.CLOSED;
            case TIMEOUT:
                return RequestStatus.TIMEOUT;
            case REJECTED:
                return RequestStatus.REJECTED;
            case REQUEST_UPDATE:
                return RequestStatus.REQUEST_UPDATE;
            default:
                return RequestStatus.PENDING;
        }
    }

    public static RequestStatus toRequestStatus(RequestStatusBody.StatusEnum statusEnum) {
        if (statusEnum == null) {
            return null;
        }
        switch (statusEnum) {
            case APPROVED:
                return RequestStatus.APPROVED;
            case CANCELLED:
                return RequestStatus.CANCELLED;
            case CLOSED:
                return RequestStatus.CLOSED;
            case REJECTED:
                return RequestStatus.REJECTED;
            case REQUEST_UPDATE:
                return RequestStatus.REQUEST_UPDATE;
            case FORWARD:
                return RequestStatus.FORWARD;
            case AGREED_FORWARD:
                return RequestStatus.AGREED_FORWARD;
            default:
                return RequestStatus.PENDING;
        }
    }

    private static OneRequest.StatusEnum toRequestStatusEnum(RequestStatus status) {
        if (status == null) {
            return null;
        }
        switch (status) {
            case APPROVED:
                return OneRequest.StatusEnum.APPROVED;
            case CANCELLED:
                return OneRequest.StatusEnum.CANCELLED;
            case CLOSED:
                return OneRequest.StatusEnum.CLOSED;
            case TIMEOUT:
                return OneRequest.StatusEnum.TIMEOUT;
            case REJECTED:
                return OneRequest.StatusEnum.REJECTED;
            case REQUEST_UPDATE:
                return OneRequest.StatusEnum.REQUEST_UPDATE;
            default:
                return OneRequest.StatusEnum.PENDING;
        }
    }




}
