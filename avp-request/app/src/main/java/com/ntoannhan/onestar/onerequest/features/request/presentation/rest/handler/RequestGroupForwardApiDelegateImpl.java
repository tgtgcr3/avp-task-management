package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.handler;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.CreateRequestGroupForwardIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.DeleteRequestGroupForwardIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.TransferRequestIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.UpdateRequestGroupForwardIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors.*;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.rest.api.RequestGroupForwardApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.BooleanResponse;
import com.ntoannhan.onestar.onerequest.rest.model.RequestGroupForward;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RequestGroupForwardApiDelegateImpl implements RequestGroupForwardApiDelegate {

    private final CreateRequestGroupForward createRequestGroupForward;

    private final UpdateRequestGroupForward updateRequestGroupForward;

    private final DeleteRequestGroupForward deleteRequestGroupForward;

    private final GetAllRequestGroupForwards getAllRequestGroupForwards;

    private final TransferRequest transferRequest;

    public RequestGroupForwardApiDelegateImpl(CreateRequestGroupForward createRequestGroupForward, UpdateRequestGroupForward updateRequestGroupForward, DeleteRequestGroupForward deleteRequestGroupForward, GetAllRequestGroupForwards getAllRequestGroupForwards, TransferRequest transferRequest) {
        this.createRequestGroupForward = createRequestGroupForward;
        this.updateRequestGroupForward = updateRequestGroupForward;
        this.deleteRequestGroupForward = deleteRequestGroupForward;
        this.getAllRequestGroupForwards = getAllRequestGroupForwards;
        this.transferRequest = transferRequest;
    }

    @Override
    public ResponseEntity<RequestGroupForward> createForwardGroup(Long id, RequestGroupForward requestGroupForward) {
        return createRequestGroupForward.call(new CreateRequestGroupForwardIn()
                .requestId(id)
                .requestGroupForward(convertToDomain(requestGroupForward))
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(convertToRest(out))
        );
    }

    @Override
    public ResponseEntity<RequestGroupForward> updateForwardGroup(Long gId, Long id, RequestGroupForward requestGroupForward) {
        return updateRequestGroupForward.call(new UpdateRequestGroupForwardIn()
                .requestId(gId)
                .forwardId(id)
                .requestGroupForward(convertToDomain(requestGroupForward))
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(convertToRest(out))
        );
    }

    @Override
    public ResponseEntity<BooleanResponse> removeForwardGroup(Long gId, Long id) {
        return deleteRequestGroupForward.call(new DeleteRequestGroupForwardIn()
                .requestId(gId)
                .forwardId(id)
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(new BooleanResponse().result(out))
        );
    }

    @Override
    public ResponseEntity<List<RequestGroupForward>> getAllForwardGroups(Long id) {
        return getAllRequestGroupForwards.call(id
        ).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(out.stream().map(this::convertToRest).collect(Collectors.toList()))
        );
    }

    @Override
    public ResponseEntity<BooleanResponse> transferRequest(Long rId, Long gId) {
        return transferRequest.call(new TransferRequestIn().requestId(rId).targetGroupId(gId)).fold(
                ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> ResponseEntity.ok(new BooleanResponse().result(out))
        );
    }

    private RequestGroupForward convertToRest(com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward domain) {
        RequestGroupForward rest = new RequestGroupForward();
        rest.id(domain.getId());
        rest.setStatus(convertToStatusRest(domain.getRequestStatus()));
        rest.setTargetId(domain.getTargetId());
        rest.setTargetName(domain.getTargetName());
        return rest;
    }

    private RequestGroupForward.StatusEnum convertToStatusRest(RequestStatus requestStatus) {
        if (requestStatus == null) {
            return RequestGroupForward.StatusEnum.PENDING;
        }
        switch (requestStatus) {
            case REJECTED:
                return RequestGroupForward.StatusEnum.REJECTED;
            case APPROVED:
                return RequestGroupForward.StatusEnum.APPROVED;
            case CLOSED:
                return RequestGroupForward.StatusEnum.DONE;
            default:
                return RequestGroupForward.StatusEnum.PENDING;
        }
    }

    private com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward convertToDomain(RequestGroupForward requestGroupForward) {
        com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward domain = new com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward();
        domain.setId(requestGroupForward.getId());
        domain.setRequestStatus(convertToStatusDomain(requestGroupForward.getStatus()));
        domain.setTargetId(requestGroupForward.getTargetId());
        return domain;
    }

    private RequestStatus convertToStatusDomain(RequestGroupForward.StatusEnum status) {
        if (status == null) {
            return RequestStatus.PENDING;
        }
        switch (status) {
            case APPROVED:
                return RequestStatus.APPROVED;
            case REJECTED:
                return RequestStatus.REJECTED;
            case DONE:
                return RequestStatus.CLOSED;
            default:
                return RequestStatus.PENDING;
        }
    }
}
