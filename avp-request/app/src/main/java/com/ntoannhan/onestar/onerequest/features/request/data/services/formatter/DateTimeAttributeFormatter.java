package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeAttributeFormatter implements AttributeFormatter {

    String dateFormat;

    public DateTimeAttributeFormatter(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public String formatData(Long requestId, Object value, Attribute attribute, Object extra) {
        long milli = Long.parseLong(String.valueOf(value));
        Date date = new Date(milli);
        SimpleDateFormat sdf = new SimpleDateFormat(this.dateFormat);
        return sdf.format(date);
    }
}
