package com.ntoannhan.onestar.onerequest.interfaces.config;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "oneapp")
@Component
public class AppConfiguration {

    private String taskUrl;

    private String notificationUrl;

    private String notificationToken;

    private String accountUrl;

    private String accountToken;

    private RequestConfiguration request;

    private String companyName;

    private String requestUrl;

}
