package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.UserDataEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.UserDataId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDataJpa extends JpaRepository<UserDataEntity, UserDataId> {

//    @Query("SELECT u FROM UserData u WHERE u.username = :userDataId.username ")
//    @Override
//    Optional<UserDataEntity> findById(UserDataId userDataId);

}
