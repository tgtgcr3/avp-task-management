package com.ntoannhan.onestar.onerequest.features.storage.data.repositories.entities;

import com.ntoannhan.onestar.onerequest.core.data.repositories.entities.AbstractBaseEntity;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Data
@Entity(name = "FileInfo")
@Audited
public class FileInfoEntity extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    private String fileName;

    private String path;

    private long createdDate;

    private long size;

    private String uploader;

    @Column(columnDefinition = "int default 0")
    private int status;

    @Column(columnDefinition = "int default 0")
    private int visibility;
}
