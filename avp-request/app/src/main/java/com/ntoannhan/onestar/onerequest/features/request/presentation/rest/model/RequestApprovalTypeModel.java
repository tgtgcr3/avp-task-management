package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestApprovalType;

public class RequestApprovalTypeModel extends com.ntoannhan.onestar.onerequest.rest.model.RequestApprovalType {

    public static RequestApprovalTypeModel fromDomainModel(RequestApprovalType domainModel) {
        RequestApprovalTypeModel model = new RequestApprovalTypeModel();
        model.id(domainModel.id());
        model.name(domainModel.name());
        return model;
    }


}
