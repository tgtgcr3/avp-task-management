package com.ntoannhan.onestar.onerequest.features.discussion.domain.services;

import com.ntoannhan.onestar.onerequest.core.data.entities.ResultGetAllData;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.Discussion;
import io.vavr.control.Either;

public interface DiscussionService {

    Either<Exception, Discussion> createDiscussion(long targetType, long targetId, Discussion discussion);

    Either<Exception, Discussion> updateDiscussion(long targetType, long targetId, long id, Discussion discussion);

    Either<Exception, Boolean> deleteDiscussion(long targetType, long targetId, long id);

    Either<Exception, Discussion> getDiscussion(long targetType, long targetId, long id);

    Either<Exception, ResultGetAllData<Discussion>> getAll(long targetType, long targetId, int page, int size, boolean desc);

    Either<Exception, Discussion> createReadOnlyDiscussion(long targetType, long targetId, Discussion discussion);

}
