package com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.storage.domain.service.StorageService;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in.UploadFileIn;
import com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.out.UploadFileOut;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class UploadFile implements UseCase<UploadFileOut, UploadFileIn> {

    @Autowired
    @Qualifier("InternalStorageService")
    private StorageService storageService;

    @Override
    public Either<Exception, UploadFileOut> call(UploadFileIn in) {
        return storageService.save(in.inputStream(), in.fileName(), in.visibility()).fold(
                Either::left,
                (fileInfo) -> Either.right(new UploadFileOut().fromDomainEntity(fileInfo))
        );
    }
}
