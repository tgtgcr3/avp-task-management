package com.ntoannhan.onestar.onerequest.features.request.domain.entities;

public enum RequestGroupStatus {
    ENABLED(0), DISABLED(1), DELETED(2);

    private final int value;
    RequestGroupStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static RequestGroupStatus fromId(int id) {
        if (id == 0) {
            return ENABLED;
        }
        if (id == 1) {
            return DISABLED;
        }
        if (id == 2) {
            return DELETED;
        }
        return null;
    }
}
