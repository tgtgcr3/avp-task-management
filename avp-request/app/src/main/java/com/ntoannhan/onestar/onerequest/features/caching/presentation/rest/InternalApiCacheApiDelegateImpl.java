package com.ntoannhan.onestar.onerequest.features.caching.presentation.rest;

import com.ntoannhan.onestar.onerequest.features.caching.domain.usecase.ClearDepartmentCache;
import com.ntoannhan.onestar.onerequest.features.caching.domain.usecase.ClearUserCache;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.rest.api.InternalApiApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.BooleanResponse;
import io.vavr.control.Either;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class InternalApiCacheApiDelegateImpl implements InternalApiApiDelegate {

    private final ClearDepartmentCache clearDepartmentCache;

    private final ClearUserCache clearUserCache;

    public InternalApiCacheApiDelegateImpl(ClearDepartmentCache clearDepartmentCache, ClearUserCache clearUserCache) {
        this.clearDepartmentCache = clearDepartmentCache;
        this.clearUserCache = clearUserCache;
    }

    @Override
    public ResponseEntity<BooleanResponse> clearCache(String type) {
        Either<Exception, Boolean> result = null;
        if ("user".equals(type)) {
            result = clearUserCache.call(Boolean.TRUE);
        }
        if ("department".equals(type)) {
            result = clearDepartmentCache.call(Boolean.TRUE);
        }
        if (result != null) {
            return result.fold(
                    ex -> {throw ExceptionTranslator.getFailureException(ex);},
                    (out) -> ResponseEntity.ok(new BooleanResponse().result(out))
            );
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
