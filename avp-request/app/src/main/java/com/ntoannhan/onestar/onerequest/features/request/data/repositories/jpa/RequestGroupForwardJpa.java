package com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa;

import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.RequestGroupForwardEntity;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestGroupForward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface RequestGroupForwardJpa extends JpaRepository<RequestGroupForwardEntity, Long> {

    @Query("SELECT r FROM RequestGroupForward r WHERE r.id = :id AND r.requestGroup.id = :groupId")
    Optional<RequestGroupForwardEntity> findById(Long groupId, Long id);

    @Query("SELECT r FROM RequestGroupForward r WHERE r.requestGroup.id = :groupId")
    List<RequestGroupForwardEntity> findAllByRequest(Long groupId);

    @Query("SELECT r FROM RequestGroupForward  r WHERE r.requestGroup.id = :sourceId AND r.targetGroup.id = :targetId")
    Optional<RequestGroupForwardEntity> findBySourceAndTarget(Long sourceId, Long targetId);

}
