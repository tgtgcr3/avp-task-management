package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public interface AttributeFormatter {

    default String formatData(Long requestId, Object value, Attribute attribute, Object extra) throws Exception {
        return value == null ? "" : String.valueOf(value);
    }

    default void format(XWPFDocument document, XWPFParagraph paragraph, XWPFRun xwpfRun, Long requestId, Object value, Attribute attribute, Object extra) throws Exception {
    }

    default boolean selfFormat() {
        return false;
    }

}
