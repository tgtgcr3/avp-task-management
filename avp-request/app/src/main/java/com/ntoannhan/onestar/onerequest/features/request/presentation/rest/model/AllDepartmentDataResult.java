package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.model;

import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.GetAllDepartmentDataOut;
import com.ntoannhan.onestar.onerequest.rest.model.Department;
import com.ntoannhan.onestar.onerequest.rest.model.DepartmentGetAllResult;

import java.util.stream.Collectors;

public class AllDepartmentDataResult extends DepartmentGetAllResult {

    public AllDepartmentDataResult fromUserCaseDataOut(GetAllDepartmentDataOut out) {
        this.setTotal(out.total().intValue());
        this.setItems(out.items().stream().map(d -> new Department().id(d.id()).name(d.name()).members(d.members())).collect(Collectors.toList()));
        return this;
    }

}
