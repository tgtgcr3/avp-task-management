package com.ntoannhan.onestar.onerequest.core.data.repositories.entities;

public interface TenantAware {

    void setTenantId(String tenantId);

}
