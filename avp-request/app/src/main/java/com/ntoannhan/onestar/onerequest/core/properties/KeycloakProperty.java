package com.ntoannhan.onestar.onerequest.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties("server-config.keycloak")
@Component
public class KeycloakProperty {

    private String authServerUrl;

    private String resource;

    private String secret;

    private String principalAttribute;

    private boolean useResourceRoleMappings;

    private boolean bearerOnly;

}
