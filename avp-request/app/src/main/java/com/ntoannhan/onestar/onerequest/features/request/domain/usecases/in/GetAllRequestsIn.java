package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.RequestStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class GetAllRequestsIn {

    private String search;

    private Integer page;

    private Integer size;

    private Boolean fullData;

    private Long requestGroupId;

    private RequestStatus requestStatus;

    private Boolean toMe;

    private Boolean fromMe;

    private Boolean followed;

    private Boolean favorite;

    private List<String> creators;

    private Long createdDate;

    private Long end;

}
