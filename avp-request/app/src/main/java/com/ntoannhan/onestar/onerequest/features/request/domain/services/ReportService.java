package com.ntoannhan.onestar.onerequest.features.request.domain.services;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.ActionReportData;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.GeneralReportData;
import io.vavr.control.Either;

import java.util.Map;

public interface ReportService {

    Either<Exception, GeneralReportData> generateGeneralReport(Long groupId, Long fromDate, Long toDate);

    Either<Exception, Map<Long, ActionReportData>> generateActionReport(Long groupId, Long fromDate, Long toDate);

    Either<Exception, GeneralReportData> getRequestGroupReport(long requestGroupId);

}
