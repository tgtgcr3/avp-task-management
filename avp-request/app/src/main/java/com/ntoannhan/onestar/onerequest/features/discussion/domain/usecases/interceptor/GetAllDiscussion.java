package com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.interceptor;

import com.ntoannhan.onestar.onerequest.core.data.entities.ResultGetAllData;
import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.Discussion;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.services.DiscussionService;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in.GetAllDiscussionIn;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class GetAllDiscussion implements UseCase<ResultGetAllData<Discussion>, GetAllDiscussionIn> {

    private final DiscussionService discussionService;

    public GetAllDiscussion(@Qualifier("InternalDiscussionService") DiscussionService discussionService) {
        this.discussionService = discussionService;
    }

    @Override
    public Either<Exception, ResultGetAllData<Discussion>> call(GetAllDiscussionIn in) {
        return discussionService.getAll(in.targetType(), in.targetId(), in.page(), in.size(), in.desc());
    }
}
