package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Request;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class CreateRequestIn {

    private Request request;

}
