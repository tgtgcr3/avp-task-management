package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.InputStream;

@Data
@Accessors(fluent = true)
public class ImportRequestGroupFromFileIn {

    private InputStream inputStream;

    private String contentType;

}
