package com.ntoannhan.onestar.onerequest.features.request.data.models;

import com.ntoannhan.onestar.onerequest.core.data.DataModel;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.DepartmentEntity;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Department;
import org.modelmapper.ModelMapper;

public class DepartmentDataModel extends Department implements DataModel<DepartmentEntity, DepartmentDataModel> {
    @Override
    public DepartmentDataModel fromEntity(DepartmentEntity departmentEntity) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(departmentEntity, this);
        return this;
    }
}
