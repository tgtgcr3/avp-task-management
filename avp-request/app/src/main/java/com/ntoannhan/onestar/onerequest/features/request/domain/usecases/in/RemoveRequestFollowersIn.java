package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class RemoveRequestFollowersIn {

    private long requestId;

    private List<String> followers;

}
