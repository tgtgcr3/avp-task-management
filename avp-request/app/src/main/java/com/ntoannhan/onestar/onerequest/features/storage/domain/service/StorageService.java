package com.ntoannhan.onestar.onerequest.features.storage.domain.service;

import com.ntoannhan.onestar.onerequest.features.storage.data.model.FileModel;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfo;
import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfoVisibility;
import io.vavr.control.Either;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.util.List;

public interface StorageService {

    Either<Exception, FileInfo> save(InputStream inputStream, String originalFileName, FileInfoVisibility visibility);

    Either<Exception, FileModel> loadFile(long fileId, FileInfoVisibility visibility);

    Either<Exception, List<FileInfo>> getFileInfos(List<Long> fileIds);
}
