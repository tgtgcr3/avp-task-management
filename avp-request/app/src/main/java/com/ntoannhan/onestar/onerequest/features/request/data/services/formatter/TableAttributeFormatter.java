package com.ntoannhan.onestar.onerequest.features.request.data.services.formatter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import lombok.SneakyThrows;
import net.gcardone.junidecode.Junidecode;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.springframework.security.core.parameters.P;

import java.math.BigInteger;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class TableAttributeFormatter implements AttributeFormatter{

    @Override
    public void format(XWPFDocument document, XWPFParagraph paragraph, XWPFRun xwpfRun, Long requestId, Object value, Attribute attribute, Object extra) throws Exception{
        XmlCursor xmlCursor = paragraph.getCTP().newCursor();
        xmlCursor.toNextSibling();
        XWPFTable table = document.insertNewTbl(xmlCursor);

        String additionData = attribute.getAdditionData();
        if (additionData != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            SelectAdditionData additionData1 = objectMapper.readValue(additionData, SelectAdditionData.class);
            List<String> headers = additionData1.getValues().stream().map(h -> {
                String headerName = h.getName().trim();
                if (headerName.endsWith(")")) {
                    int indext = headerName.lastIndexOf("(");
                    headerName = headerName.substring(0, indext);
                }
                return headerName;
            }).collect(Collectors.toList());

            List<String> keys = headers.stream().map(s -> Junidecode.unidecode(s.replaceAll(" ", "_"))).collect(Collectors.toList());
//            XWPFTableRow headerRow = table.createRow();
//
//            headers.forEach(h -> {
//                XWPFTableCell cell = headerRow.createCell();
//                cell.setText(h);
//            });
            if (value instanceof List) {
                List<Map<String, String>> values = (List<Map<String, String>>) value;
                int numOfCols = keys.size();
                int numOfRow = values.size() + 1;
//                document.insertTable(xmlCursor.get);
//                XWPFTable table = document.createTable(numOfRow, numOfCols);
                XWPFTableRow row = table.getRow(0);
                for (int i = 0; i < numOfCols; i++) {
                    if (i > 0) {
                        row.createCell();
                    } else {
                        row.getCell(0).removeParagraph(0);
                    }
                    XWPFTableCell cell = row.getCell(i);
//                    cell.setText(headers.get(i));
                    cell.setColor("BFBFBF");
                    setHeader(cell, headers.get(i));
                }

                for (int i = 1; i < numOfRow; i++) {
                    table.createRow();
                    row = table.getRow(i);
                    Map<String, String> dataRow = values.get(i - 1);
                    for (int j = 0; j < numOfCols; j++) {
                        String key = keys.get(j);
                        row.getCell(j).setText(dataRow.get(key));
                    }
                }
                tableSetBorders(table, STBorder.SINGLE, 4, 0, "000000");


//                    values.forEach(vm -> {
//                            XWPFTableRow row = table.createRow();
//                            keys.forEach(k -> {
//                                    XWPFTableCell cell = row.createCell();
//                                    cell.setText(vm.get(k));
//
//                            });
//                    });

            }



        }
    }

    private void setHeader(XWPFTableCell cell, String text) {
        XWPFParagraph paragraph  = cell.addParagraph();
        XWPFRun run = paragraph.createRun();
        run.setBold(true);
        run.setText(text);

    }


    private static void tableSetBorders(
            XWPFTable table,
            STBorder.Enum borderType,
            int size,
            int space,
            String hexColor) {

        table.getCTTbl().getTblPr().getTblBorders().getBottom().setColor(hexColor);
        table.getCTTbl().getTblPr().getTblBorders().getTop().setColor(hexColor);
        table.getCTTbl().getTblPr().getTblBorders().getLeft().setColor(hexColor);
        table.getCTTbl().getTblPr().getTblBorders().getRight().setColor(hexColor);
        table.getCTTbl().getTblPr().getTblBorders().getInsideH().setColor(hexColor);
        table.getCTTbl().getTblPr().getTblBorders().getInsideV().setColor(hexColor);

        table.getCTTbl().getTblPr().getTblBorders().getRight().setSz(BigInteger.valueOf(size));
        table.getCTTbl().getTblPr().getTblBorders().getTop().setSz(BigInteger.valueOf(size));
        table.getCTTbl().getTblPr().getTblBorders().getLeft().setSz(BigInteger.valueOf(size));
        table.getCTTbl().getTblPr().getTblBorders().getBottom().setSz(BigInteger.valueOf(size));
        table.getCTTbl().getTblPr().getTblBorders().getInsideH().setSz(BigInteger.valueOf(size));
        table.getCTTbl().getTblPr().getTblBorders().getInsideV().setSz(BigInteger.valueOf(size));

        table.getCTTbl().getTblPr().getTblBorders().getBottom().setVal(borderType);
        table.getCTTbl().getTblPr().getTblBorders().getTop().setVal(borderType);
        table.getCTTbl().getTblPr().getTblBorders().getLeft().setVal(borderType);
        table.getCTTbl().getTblPr().getTblBorders().getRight().setVal(borderType);
        table.getCTTbl().getTblPr().getTblBorders().getInsideH().setVal(borderType);
        table.getCTTbl().getTblPr().getTblBorders().getInsideV().setVal(borderType);
    }

    @Override
    public boolean selfFormat() {
        return true;
    }

    @Override
    public String formatData(Long requestId, Object value, Attribute attribute, Object extra) throws Exception {
        String additionData = attribute.getAdditionData();
        StringBuilder sb = new StringBuilder();
        if (additionData != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            SelectAdditionData additionData1 = objectMapper.readValue(additionData, SelectAdditionData.class);
            List<String> headers = additionData1.getValues().stream().map(h -> {
                String headerName = h.getName().trim();
                if (headerName.endsWith(")")) {
                    int indext = headerName.lastIndexOf("(");
                    headerName = headerName.substring(0, indext);
                }
                return headerName;
            }).collect(Collectors.toList());

            List<String> keys = headers.stream().map(s -> Junidecode.unidecode(s.replaceAll(" ", "_"))).collect(Collectors.toList());
            if (value instanceof List) {
                List<Map<String, String>> values = (List<Map<String, String>>) value;
                int numOfCols = keys.size();
                int numOfRow = values.size() + 1;

                sb.append("| ").append(String.join(" | ", headers)).append(" |\n");

                for (int i = 1; i < numOfRow; i++) {
                    sb.append("| ");
                    Map<String, String> dataRow = values.get(i - 1);
                    for (int j = 0; j < numOfCols; j++) {
                        String key = keys.get(j);
                        sb.append(dataRow.get(key)).append(" |");
                    }
                    sb.append("\n");
                }
            }

        }
        return sb.toString();
    }
}
