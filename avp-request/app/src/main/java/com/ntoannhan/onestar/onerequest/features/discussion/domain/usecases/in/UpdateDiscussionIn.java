package com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in;

import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.Discussion;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class UpdateDiscussionIn {

    private Discussion discussion;

    private long targetType;

    private long targetId;

    private long id;

}
