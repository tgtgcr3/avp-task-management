package com.ntoannhan.onestar.onerequest.features.storage.domain.usecase.in;

import com.ntoannhan.onestar.onerequest.features.storage.domain.entities.FileInfoVisibility;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.InputStream;

@Data
@Accessors(fluent = true)
public class UploadFileIn {

    private InputStream inputStream;

    private String fileName;

    private String contentType;

    private FileInfoVisibility visibility;

}
