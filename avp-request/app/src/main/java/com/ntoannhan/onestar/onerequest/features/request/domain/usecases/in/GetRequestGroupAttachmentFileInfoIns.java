package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(fluent = true)
public class GetRequestGroupAttachmentFileInfoIns {

    private long groupId;

    private List<Long> fileIds;

}
