package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;


import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.DownloadRequestGroupAttachmentIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.out.DownloadRequestGroupAttachmentOut;
import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class DownloadRequestGroupAttachment implements UseCase<DownloadRequestGroupAttachmentOut, DownloadRequestGroupAttachmentIn> {

    private final RequestGroupService requestGroupService;

    @Override
    public Either<Exception, DownloadRequestGroupAttachmentOut> call(DownloadRequestGroupAttachmentIn in) {
        return requestGroupService.downloadAttachment(in.requestGroupId(), in.fileId()).fold(Either::left, out -> Either.right(new DownloadRequestGroupAttachmentOut().resource(out)));
    }
}
