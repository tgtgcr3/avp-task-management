package com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.interceptor;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.entities.Discussion;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.services.DiscussionService;
import com.ntoannhan.onestar.onerequest.features.discussion.domain.usecases.in.CreateDiscussionIn;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class CreateSystemDiscussion implements UseCase<Discussion, CreateDiscussionIn> {

    private final DiscussionService discussionService;

    public CreateSystemDiscussion(@Qualifier("InternalDiscussionService") DiscussionService discussionService) {
        this.discussionService = discussionService;
    }

    @Override
    public Either<Exception, Discussion> call(CreateDiscussionIn in) {
        return discussionService.createReadOnlyDiscussion(in.targetType(), in.targetId(), in.discussion());
    }
}
