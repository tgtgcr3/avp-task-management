package com.ntoannhan.onestar.onerequest.features.request.presentation.rest.handler;

import com.ntoannhan.onestar.onerequest.core.mapper.OneMapper;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.ActionReportIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.GetGeneralReportIn;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors.GetActionReport;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors.GetGeneralReport;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors.GetRequestGroupReport;
import com.ntoannhan.onestar.onerequest.features.request.presentation.rest.utils.ExceptionTranslator;
import com.ntoannhan.onestar.onerequest.rest.api.ReportApiDelegate;
import com.ntoannhan.onestar.onerequest.rest.model.ReportAction;
import com.ntoannhan.onestar.onerequest.rest.model.ReportData;
import com.ntoannhan.onestar.onerequest.rest.model.ReportGeneralData;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ReportApiDelegateImpl implements ReportApiDelegate {

    private final GetGeneralReport getGeneralReport;

    private final GetActionReport getActionReport;

    private final GetRequestGroupReport getRequestGroupReportUS;

    public ReportApiDelegateImpl(GetGeneralReport getGeneralReport, GetActionReport getActionReport, GetRequestGroupReport getRequestGroupReport) {
        this.getGeneralReport = getGeneralReport;
        this.getActionReport = getActionReport;
        this.getRequestGroupReportUS = getRequestGroupReport;
    }

    @Override
    public ResponseEntity<ReportData> getGeneralReport(String type, Long start, Long end, Integer group) {
        if (type == null || type.equals("general")) {
            return this.getGeneralReport.call(new GetGeneralReportIn().groupId(group != null ?Long.valueOf(group) : null).fromDate(start).toDate(end)).fold(
                    ex -> {
                        throw ExceptionTranslator.getFailureException(ex);
                    },
                    out -> {
                        ReportData reportData = new ReportData();
                        reportData.general(OneMapper.mapTo(out.generalReportData(), ReportGeneralData.class));
                        return ResponseEntity.ok(reportData);
                    }
            );
        } else {
            return getActionReport.call(new ActionReportIn().groupId(group != null ?Long.valueOf(group) : null).fromDate(start).toDate(end)).fold(
                    ex -> {
                        throw ExceptionTranslator.getFailureException(ex);
                    },
                    out -> {
                        ReportData reportData = new ReportData();
                        Map<String, ReportAction> actionData = new HashMap<>();
                        out.actionReportData().forEach((k, v) -> {
                            actionData.put(k.toString(), OneMapper.mapTo(v, ReportAction.class));
                        });
                        reportData.action(actionData);
                        return ResponseEntity.ok(reportData);
                    }
            );
        }
    }

    @Override
    public ResponseEntity<ReportGeneralData> getRequestGroupReport(Long id) {
        return getRequestGroupReportUS.call(id).fold( ex -> {
                    throw ExceptionTranslator.getFailureException(ex);
                },
                out -> {
                    ReportGeneralData reportData = OneMapper.mapTo(out, ReportGeneralData.class);
                    return ResponseEntity.ok(reportData);
                });
    }


}
