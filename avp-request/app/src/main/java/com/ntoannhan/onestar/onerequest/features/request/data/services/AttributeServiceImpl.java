package com.ntoannhan.onestar.onerequest.features.request.data.services;

import com.ntoannhan.onestar.onerequest.core.exception.AppException;
import com.ntoannhan.onestar.onerequest.features.request.data.models.AttributeTypeDataModel;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.AttributeEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.AttributeTypeEntity;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.AttributeJpa;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.jpa.AttributeTypeJpa;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.AttributeType;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.AttributeService;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import net.gcardone.junidecode.Junidecode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Log4j2
@Component
public class AttributeServiceImpl implements AttributeService {

    @Autowired
    private AttributeJpa attributeJpa;

    @Autowired
    private AttributeTypeJpa attributeTypeJpa;

    @Override
    public Either<Exception, List<AttributeEntity>> createAttributes(List<Attribute> attrs) {
        return convertAttributeEntity(attrs).fold(Either::left,
                    attrEntities -> {
                        try {
                            List<AttributeEntity> saved = attributeJpa.saveAll(attrEntities);
                            return Either.right(saved);
                        } catch (Exception ex) {
                            log.error("Cannot save data", ex);
                            return Either.left(new AppException(ex.getMessage()));
                        }
                    }
                );
    }

    public Either<Exception, List<AttributeEntity>> convertAttributeEntity(List<Attribute> attrs) {
        List<Long> attrTypeIds = attrs.stream().map(Attribute::attrType).collect(Collectors.toList());
        List<AttributeTypeEntity> attrTypeEntities = attributeTypeJpa.findAllById(attrTypeIds);

        return Either.right(attrs.stream().map(attr -> {
            AttributeEntity entity = new AttributeEntity();
            entity.setId(attr.id());
            entity.setAttrType(attrTypeEntities.stream().filter(at -> at.getId() == attr.id()).findFirst().get());
            entity.setName(attr.name());
            entity.setAttrOrder(attr.order());
            entity.setFieldName(attr.fieldName());
            entity.setAdditionData(attr.additionData());
            if (entity.getFieldName() == null || entity.getFieldName().trim().length() == 0) {
                entity.setFieldName(Junidecode.unidecode(entity.getName().toLowerCase(Locale.ROOT).replaceAll(" ", "_")));
            }
            return entity;
        }).collect(Collectors.toList()));
    }

    public Either<Exception, List<Attribute>> convertToDomain(List<AttributeEntity> attrs) {
        try {
            return Either.right(attrs.stream().map(attr -> new Attribute()
                    .id(attr.getId())
                    .attrType(attr.getAttrType().getId())
                    .required(attr.getRequired() == 1)
                    .additionData(attr.getAdditionData())
                    .order(attr.getAttrOrder())
                    .description(attr.getDescription())
                    .fieldName(attr.getFieldName())
            ).collect(Collectors.toList()));
        } catch (Exception ex) {
            log.error("Cannot convert Attr to Domain", ex);
        }
        return Either.left(new AppException("cannot_convert_attr_to_domain"));
    }

    @Override
    public Either<Exception, List<AttributeEntity>> getAttributes(List<Long> ids) {
        return null;
    }

    @Override
    public Either<Exception, Integer> updateAttributes(List<Attribute> attrs) {
        return null;
    }

    @Override
    public Either<Exception, List<AttributeType>> getAllAttributeTypes() {
        try {
            List<AttributeTypeEntity> attributeTypeEntities = attributeTypeJpa.findAll();
            return Either.right(attributeTypeEntities.stream().map(e -> new AttributeTypeDataModel().fromEntity(e)).collect(Collectors.toList()));
        } catch (Exception ex) {
            log.error("Cannot load attribute type", ex);
            return Either.left(new AppException("cannot_load_attribute_type"));
        }
    }


}
