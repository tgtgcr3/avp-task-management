package com.ntoannhan.onestar.onerequest.features.request.domain.usecases.interactors;

import com.ntoannhan.onestar.onerequest.core.usecases.UseCase;
import com.ntoannhan.onestar.onerequest.features.request.domain.services.RequestGroupForwardService;
import com.ntoannhan.onestar.onerequest.features.request.domain.usecases.in.DeleteRequestGroupForwardIn;
import io.vavr.control.Either;
import org.springframework.stereotype.Component;

@Component
public class DeleteRequestGroupForward implements UseCase<Boolean, DeleteRequestGroupForwardIn> {

    private final RequestGroupForwardService requestGroupForwardService;

    public DeleteRequestGroupForward(RequestGroupForwardService requestGroupForwardService) {
        this.requestGroupForwardService = requestGroupForwardService;
    }

    @Override
    public Either<Exception, Boolean> call(DeleteRequestGroupForwardIn in) {
        return requestGroupForwardService.deleteGroupForward(in.requestId(), in.forwardId());
    }
}
