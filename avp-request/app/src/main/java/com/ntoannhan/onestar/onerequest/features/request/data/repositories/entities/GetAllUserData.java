package com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetAllUserData {

    private List<UserData> items;

    private long total;

}
