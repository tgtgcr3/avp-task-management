package com.ntoannhan.onestar.onerequest.features.request.data.models;

import com.ntoannhan.onestar.onerequest.core.data.DataModel;
import com.ntoannhan.onestar.onerequest.features.request.data.repositories.entities.AttributeEntity;
import com.ntoannhan.onestar.onerequest.features.request.domain.entities.Attribute;

public class AttributeDataModel extends Attribute implements DataModel<AttributeEntity, AttributeDataModel> {

    @Override
    public AttributeDataModel fromEntity(AttributeEntity attributeEntity) {
        this.id(attributeEntity.getId())
                .attrType(attributeEntity.getAttrType().getId())
                .name(attributeEntity.getName())
                .order(attributeEntity.getAttrOrder())
                .required(attributeEntity.getRequired() > 0)
                .additionData(attributeEntity.getAdditionData())
                .description(attributeEntity.getDescription())
                .fieldName(attributeEntity.getFieldName());
        return this;
    }
}
