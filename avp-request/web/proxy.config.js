module.exports = {
  "/auth/*": {
    "target": "http://192.168.11.51:3000",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true
  },
  "/api/v1/*": {
    "target": "http://192.168.11.51:3000",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true
  },
  "/account/*": {
    "target": "http://192.168.11.51:3000",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true,
    "pathRewrite": {
      "^/account": ""
    }
  },
  "/api/*": {
    "target": "http://192.168.12.2:4300",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true
  },
  "/chatfile/*": {
    "target": "http://192.168.11.51:3000",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true,
    "pathRewrite": {
      "^/chatfile": ""
    }
  },
  "/packages/*": {
    "target": "http://192.168.11.51:3000",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true
  },
  "/sse/api/pri/logout-sse": {
    "target": "http://192.168.11.51:3000",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true,
    "pathRewrite": {
      "^/sse": ""
    },
    onProxyReq: (proxyRes, req, res) => {
      proxyRes.setHeader('X-APP-ID', 'request');
      proxyRes.setHeader('X-TENANT-ID', 'an-viet-phat');
    }

  },
  "/notification/api/pri/*": {
    "target": "http://192.168.11.51:3000",
    "secure": false,
    "logLevel": "debug",
    "changeOrigin": true,
    "pathRewrite": {
      "^/notification": ""
    },
    onProxyReq: (proxyRes, req, res) => {
      proxyRes.setHeader('X-APP-ID', 'request');
    }
  }
};
