export * from './avatar.service';
import { AvatarService } from './avatar.service';
export * from './keyCloakUser.service';
import { KeyCloakUserService } from './keyCloakUser.service';
export const APIS = [AvatarService, KeyCloakUserService];
