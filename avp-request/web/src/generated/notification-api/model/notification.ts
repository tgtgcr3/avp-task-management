/**
 * ONE Notification UI API
 * Rest API definition for ONE Notification UI. 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: toannhanb7@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { NotificationType } from './notificationType';
import { NotificationStatus } from './notificationStatus';


export interface Notification { 
    id?: number;
    owner?: string;
    ownerEmail?: string;
    target?: string;
    targetEmail?: string;
    title?: string;
    shortContent?: string;
    longContent?: string;
    data?: object;
    notificationType?: NotificationType;
    notificationStatus?: NotificationStatus;
    date?: number;
    appName?: string;
}

