/**
 * ONE Request UI API
 * Rest API definition for ONE Request UI. 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: toannhanb7@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { RequestGroup } from './requestGroup';


export interface RequestGroupGetResult { 
    total?: number;
    items?: Array<RequestGroup>;
}

