/**
 * ONE Request UI API
 * Rest API definition for ONE Request UI. 
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: toannhanb7@gmail.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Attribute } from './attribute';


export interface RequestGroup { 
    id?: number;
    name?: string;
    mgmtNotification?: number;
    approveType?: number;
    category?: string;
    sla?: number;
    followers?: Array<string>;
    description?: string;
    status?: RequestGroup.StatusEnum;
    approvers?: Array<string>;
    targetId?: number;
    targetType?: number;
    requestForm?: number;
    attrs?: Array<Attribute>;
    readonly creator?: string;
    oId?: number;
    requestForms?: Array<string>;
    approvalOwner?: string;
}
export namespace RequestGroup {
    export type StatusEnum = 'ENABLED' | 'DISABLED' | 'DELETED';
    export const StatusEnum = {
        ENABLED: 'ENABLED' as StatusEnum,
        DISABLED: 'DISABLED' as StatusEnum,
        DELETED: 'DELETED' as StatusEnum
    };
}


