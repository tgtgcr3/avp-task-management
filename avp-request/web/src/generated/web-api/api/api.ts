export * from './attributeType.service';
import { AttributeTypeService } from './attributeType.service';
export * from './department.service';
import { DepartmentService } from './department.service';
export * from './discussion.service';
import { DiscussionService } from './discussion.service';
export * from './file.service';
import { FileService } from './file.service';
export * from './internalApi.service';
import { InternalApiService } from './internalApi.service';
export * from './report.service';
import { ReportService } from './report.service';
export * from './request.service';
import { RequestService } from './request.service';
export * from './requestApprovalType.service';
import { RequestApprovalTypeService } from './requestApprovalType.service';
export * from './requestGroup.service';
import { RequestGroupService } from './requestGroup.service';
export * from './requestGroupForward.service';
import { RequestGroupForwardService } from './requestGroupForward.service';
export const APIS = [AttributeTypeService, DepartmentService, DiscussionService, FileService, InternalApiService, ReportService, RequestService, RequestApprovalTypeService, RequestGroupService, RequestGroupForwardService];
