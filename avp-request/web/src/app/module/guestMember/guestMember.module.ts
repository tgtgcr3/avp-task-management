import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CommonModule } from '@angular/common';
import { NgModule } from "@angular/core";
import { GuestListHeaderComponent } from "./component/guest-list-header/guest-list-header.component";
import { GuestMemberComponent } from "./guestMember.component";
import {GuestMemberCreationComponent} from "./component/guestMemberCreation/guestMemberCreation.component";
import {GuestMemberListComponent} from "./component/guestMemberList/guestMemberList.component";

import {ComponentModule} from "../../core/component/component.module";
import {TranslateModule} from "@ngx-translate/core";
import {CoreModule} from "../../core/core.module";
import { MatButtonModule } from '@angular/material/button';
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import {MatTableModule} from "@angular/material/table";
@NgModule(
    {
        imports: [
            ComponentModule,
            CoreModule,
            TranslateModule,
            MatButtonModule,
            MatDialogModule,
            MatInputModule,
            ReactiveFormsModule,
            MatTableModule,
            CommonModule,
            MatTooltipModule,
            BrowserAnimationsModule
        ],
        declarations: [
            GuestListHeaderComponent,
            GuestMemberComponent,
            GuestMemberCreationComponent,
            GuestMemberListComponent
        ]
    }
)

export class GuestMemberModule {}
