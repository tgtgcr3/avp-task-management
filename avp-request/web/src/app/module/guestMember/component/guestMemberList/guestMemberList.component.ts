import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { KeyCloakService } from "../../../../core/service/keycloak.service";
import { forkJoin, Observable, Subscription } from "rxjs";
import { ActivatedRoute, Router } from '@angular/router';

import { Group, UserData } from "../../../../../generated/keycloak-api";
import * as transliterate from "@sindresorhus/transliterate";
@Component({
  selector: 'guest-member-list',
  templateUrl: './guestMemberList.component.html',
  styleUrls: ['./guestMemberList.component.scss']
})
export class GuestMemberListComponent implements OnInit {

  @Input()
  searchEvents: Observable<string>;

  @Input()
  actionEvents: Observable<string>;

  private actionSubscription: Subscription;

  private currentSearch: string = '';

  private currentFilter: string = 'activate';

  private guestGroup: Group;

  filteredUSers: UserData[];

  allUsers: UserData[];

  displayedColumns: string[] = ['userInfo', 'contact', 'actions'];

  private eventsSubscription: Subscription;

  isAdmin: boolean = false;

  constructor(
    private keyCloakService: KeyCloakService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.keyCloakService.getAvailableGroups().then(groups => {
      this.guestGroup = groups.find(g => g.name.toLowerCase() == 'guest');
      this.getAllUsers();
    });
    this.isAdmin = this.keyCloakService.isAdmin();

    if (this.actionEvents != null) {
      this.actionEvents.subscribe(action => {
        if (action != null && action.indexOf('filter:') >= 0) {
          this.currentFilter = action.replace('filter:', '').trim();
          this.getAllUsers();
        } else if (action.indexOf('refresh') >= 0) {
          this.getAllUsers();
        }
      });
    }
    if (this.searchEvents != null) {
      this.searchEvents.subscribe(text => {
        this.currentSearch = (text || '').trim();
        this.filteredUSers = this._applyFilterAndSearch(this.allUsers);
      });
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.isAdmin = this.keyCloakService.isAdmin();
  }

  private getAllUsers() {
    this.keyCloakService.getUsersOfGroup(this.guestGroup.id).toPromise().then(users => {
      this.allUsers = users;
      this.filteredUSers = this._applyFilterAndSearch(users);
    });
  }

  private _applyFilterAndSearch(allUsers: UserData[]): UserData[] {
    let isActivated = this.currentFilter == 'activate';
    let currentSearch = (this.currentSearch || '').trim();
    currentSearch = transliterate(currentSearch).toLowerCase();
    return allUsers.filter(u => {
      return u.enabled == isActivated && transliterate(this.getFullname(u)).toLowerCase().indexOf(currentSearch) >= 0;
    });
  }

  editUser(user) {
    this.router.navigate(['/userDetails', user.username || user.preferred_username]);
  }

  deactivateUser(user, tobeEnabled) {
    const message = tobeEnabled ? 'Kích hoạt tài khoản ' + (user.username || user.preferred_username) + '?'
      : `Hủy kích hoạt tài khoản ${user.username || user.preferred_username}?`;
    if (confirm(message)) {
      user.enabled = tobeEnabled;
      let clone = { ...user };
      delete clone.directManager;
      this.keyCloakService.updateUser(user.id, clone).toPromise().then(r => {
        this.getAllUsers();
      });

    }
  }

  getFullname(user: UserData): string {
    return `${(user.lastName || '').trim()} ${(user.firstName || '').trim()}`;
  }
}
