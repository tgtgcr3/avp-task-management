import { Component, OnInit } from "@angular/core";
import { Subject } from 'rxjs';


@Component({
    selector: 'guest-member-component',
    templateUrl: './guestMember.component.html',
    styleUrls: ['./guestMember.component.scss']
})

export class GuestMemberComponent implements OnInit {

  searchSubject: Subject<string> = new Subject<string>();

  eventSubject: Subject<string> = new Subject<string>();

  ngOnInit(): void {
  }

  onSearchChanged(newText) {
    this.searchSubject.next(newText);
  }

  onReceiveEvent(eventData) {
    this.eventSubject.next(eventData);
  }

}
