import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';

import {NgModule} from "@angular/core";

import {MatTableModule} from "@angular/material/table";
import {ManagementComponent} from "./management.component";
import {TranslateModule} from "@ngx-translate/core";

import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {BrowserModule} from "@angular/platform-browser";
import {MatIconModule} from "@angular/material/icon";
import {ReactiveFormsModule} from "@angular/forms";
import {CoreModule} from "../../core/core.module";
import {ManagementRoutingModule} from "./management.routing";
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    imports: [
        MatButtonModule,
        MatDialogModule,
        MatInputModule,
        MatSelectModule,
        BrowserModule,
        MatTableModule,
        MatIconModule,
        ReactiveFormsModule,
        TranslateModule,
        CoreModule,
        ManagementRoutingModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatTooltipModule,
        BrowserAnimationsModule
    ],
  declarations: [
    ManagementComponent
  ],
  exports: [
    ManagementComponent
  ]
})

export class ManagementModule{}
