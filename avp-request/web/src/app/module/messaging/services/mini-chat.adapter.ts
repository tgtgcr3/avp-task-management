import { KeyCloakUserService } from './../../../../generated/account-api/api/keyCloakUser.service';
import { AvatarService } from './../../../../generated/account-api/api/avatar.service';
import { CookieService } from 'ngx-cookie-service';
// import { KeyCloakUserService } from './../../../../generated/web-api/api/keyCloakUser.service';
import { environment } from './../../../../environments/environment';
import { RocketChatService } from './../../../../generated/rocket-api/api/rocketChat.service';
import { RealTimeAPI } from 'rocket.chat.realtime.api.rxjs';
// import { AvatarService } from './../../../../generated/web-api/api/avatar.service';

import { SecurityService } from './../../../core/service/security.service';
import { KeyCloakService } from './../../../core/service/keycloak.service';
import { Injectable, OnInit } from '@angular/core';
import { ChatAdapter, IChatGroupAdapter, User, Group, Message, FileMessage, ChatParticipantStatus, ParticipantResponse, ParticipantMetadata, ChatParticipantType, IChatParticipant, PagedHistoryChatAdapter, MessageType } from 'ng-chat';
import { forkJoin, Observable, of } from 'rxjs';
import { IFileUploadAdapter } from 'ng-chat/ng-chat/core/file-upload-adapter';



@Injectable({
  providedIn: 'root'
})

// export class MiniChatFileUploadAdapter implements IFileUploadAdapter {
//   uploadFile(file: File, participantId: any): Observable<Message> {
//     throw new Error('Method not implemented.');
//   }

// }

export class MiniChatAdapter extends PagedHistoryChatAdapter implements IChatGroupAdapter, IFileUploadAdapter {
  regex: RegExp = /:[\s\w]+:/;

  rocketRealTime: RealTimeAPI;
  currentUser: any;
  currentUserName: string;

  friends: ParticipantResponse[] = [];
  userId: any;
  userAuth: any;
  directMessageRoomIds = {};
  directMessageSubscriptions = {};

  groupMessageIds = {};
  groupMessageSubscription = {};
  groupIdToRoomIds = {};

  roomIdToNgGroups = {};
  roomIdToNgGroupOpeneds = {};


  public onlineUsernames: string[] = [];

  public updateFunc: any = null;
  public updateTarget: any = null;

  initFrendMessage = false;
  private emoji: any = {};

  constructor(
    private keycloakService: KeyCloakService,
    private securityService: SecurityService,
    private rocketService: RocketChatService,
    private avatarsService: AvatarService,
    private keycloakUserService: KeyCloakUserService,
    private cookieService: CookieService
  ) {
    super();
    this.rocketRealTime = new RealTimeAPI(`${environment.oneMessageWs}/websocket`);

  }
  uploadFile(file: File, destinataryId: any): Observable<Message> {
    const rid = this.directMessageRoomIds[destinataryId] || this.groupIdToRoomIds[destinataryId];
    return new Observable(sub => {
      setTimeout(() => {
        this.rocketService.uploadFile(rid, this.userId, this.userAuth, file).toPromise().then((r: any) => {

          sub.next(this.convertMessage(r.message, destinataryId));
          sub.complete();
        })
      })
    });
  }

  init() {
    this.onlineUsernames = [];
    this.currentUser = this.keycloakService.getUserInfo();
    this.currentUserName = this.currentUser.username || this.currentUser.preferred_username;
    this.keycloakService.exchangeToken('avp-message').toPromise().then(s => {
      console.log('Rocket Chat Token: ');
      console.log(s);
      this.loginToRocket(s).then(r => {
        this.initWebsocket();
        setTimeout(() => {
          this.loadFriends();
        }, 1000);
      });

    }).catch(e => console.log(e));
    this.rocketService.getAllEmoji(null, null).toPromise().then((res: any) => {
      this.emoji = res.body;
    });
  }

  loginToRocket(tokenInfo: any) {
    let accessToken = tokenInfo.access_token;
    return this.rocketService.login({
      serviceName: 'keycloak',
      accessToken: accessToken,
      idToken: 'hash',
      scope: 'profile',
      expiresIn: 3600
    }).toPromise().then((data: any) => {
      console.log('Rocket Chat Data');
      console.log(data.data);
      this.userId = data.data.userId;
      this.userAuth = data.data.authToken;
      this.cookieService.set('rc_uid', this.userId);
      this.cookieService.set('rc_token', this.userAuth);

      return data.data;
    });
  }

  initWebsocket() {
    this.rocketRealTime.connectToServer().subscribe((data) => {
      console.log('connected');

      this.rocketRealTime.keepAlive().subscribe();

      console.log('try to login with realtime');
      this.rocketRealTime.loginWithAuthToken(this.userAuth).subscribe((data: any) => {
        console.log('realtime');
        console.log(data);
        if (data.msg === 'result') {
          this.rocketRealTime.subscribe((data) => {
            console.log('subcribe');
            console.log(data);
          });
          this.rocketRealTime.onMessage((value: any) => {
            console.log('onMessage');
            console.log(value);
          });
          this.listenToStreamNotifyLogged();
        }
      });
    });
  }

  listenToStreamNotifyLogged() {
    this.rocketRealTime.getSubscription('stream-notify-logged', 'user-status', false).subscribe((data: any) => {
      console.log('stream-notify-logged', data);
      this.loadFriends();
    });
    this.rocketRealTime.getSubscription('stream-notify-user', 'rooms-changed', false).subscribe((data: any) => {
      console.log('stream-notify-user', data);
    //  this.loadFriends();
    });
    this.rocketRealTime.getSubscription('stream-room-messages', '__my_messages__', false).subscribe((data: any) => {
      console.log('all my message', data);

      if (data.fields.args.length == 2) {
        const msgData = data.fields.args[0];
        const roomData = data.fields.args[1];

        if (roomData.roomType === 'd') {
          const rid = msgData.rid;
          for (const username in this.directMessageRoomIds) {
            if (this.directMessageRoomIds[username] === rid) {
              return;
            }
          }
          const username = msgData.u.username;
          const par = this.friends.find(u => u.participant.id === username);
          this.directMessageRoomIds[username] = msgData.rid;
          // this.onMessageReceived(par.participant, {
          //   fromId: username,
          //   message: msgData.msg,
          //   toId: this.currentUserName,
          //   dateSent: new Date(msgData.ts.$date)
          // });
          this.onMessageReceived(par.participant, this.convertMessage(msgData, par.participant.id));
          this.subscribeForRoomMessage(par.participant, rid);
        } else {
          if (roomData.roomType == 'p') {
            if (data.msg == 'changed' && msgData.t == 'r') {
              let group = this.roomIdToNgGroupOpeneds[msgData.rid];
              if (group != null) {
                group.displayName = msgData.msg;
              }
              group = this.roomIdToNgGroups[msgData.rid];
              if (group != null) {
                group.displayName = msgData.msg;
              }
              group = this.roomIdToNgGroupOpeneds[msgData.rid];
              if (group == null) {
                return;
              }
              group = this.roomIdToNgGroups[msgData.rid];
              // this.onMessageReceived(group, {
              //   fromId: msgData.u.username,
              //   message: msgData.msg,
              //   toId: group.id,
              //   dateSent: new Date(msgData.ts.$date)
              // });
              this.onMessageReceived(group, this.convertGroupMessage(msgData, group.id));
            }
            if (msgData.u.username == this.currentUserName) {
              return;
            }
            let group = this.roomIdToNgGroupOpeneds[msgData.rid];
            if (group != null) {
              return;
            }
            group = this.roomIdToNgGroups[msgData.rid];
            this.groupIdToRoomIds[group.id] = msgData.rid;
            this.roomIdToNgGroupOpeneds[msgData.rid] = group;
            // this.onMessageReceived(group, {
            //   fromId: msgData.u.username,
            //   message: msgData.msg,
            //   toId: group.id,
            //   dateSent: new Date(msgData.ts.$date)
            // });
            this.onMessageReceived(group, this.convertGroupMessage(msgData, group.id));
            this.subscribeForRoomMessage(group, msgData.rid);
          }
        }
      }
    });
  }



  public loadFriends() {
    this.onlineUsernames = [];
    return forkJoin(
      this.rocketService.getAllUsers(this.userId, this.userAuth, 1024),
      this.avatarsService.findAvatars(),
      this.keycloakUserService.getUsers(null, null, null, null, null, 0, 1024),
      this.rocketService.getRooms(this.userId, this.userAuth)
    ).toPromise().then((ds: any[]) => {
      const d = ds[0];
      const kcusers = (ds[2] as any).items as any[];
      const disabledUsers = kcusers.filter(u => u.enabled == false);
      const rooms = ds[3];
      this.friends = [];

      this.friends = d.users.filter((user: any) => {
        if (user.username !== this.currentUserName) {
          return disabledUsers.find(u => this.keycloakService.getUsername(u) == user.username) == null && kcusers.find(u => this.keycloakService.getUsername(u) == user.username) != null;
        }
        return false;
      })
        .sort((user1, user2) => {
          if (user1.status === user2.status) {
            return user1.name.localeCompare(user2.name);
          }
          return this.getUserStatus(user1.status) - this.getUserStatus(user2.status);
        }).map(user => {
          if (user.status == 'online') {
            this.onlineUsernames.push(user.username);
          }
          return {
            participant: {
              status: this.getUserStatus(user.status),
              participantType: ChatParticipantType.User,
              id: user.username,
              displayName: user.name,
              avatar: ds[1][user.username]

            }
          };
        });

      if (rooms.update != null && rooms.update.length > 1) {
        let groups = rooms.update.filter(r => r.t == 'p') as any[];
        // Todo issue
        return forkJoin(
          groups.map(g => this.rocketService.getRoomMember(g._id, this.userId, this.userAuth))
        ).toPromise().then((groupInfos: any[]) => {
          let groupFriends = [];
          for (let i = 0; i < groups.length; i++) {
            let r = groups[i];
            let groupInfo = {
              participantType: ChatParticipantType.Group,
              id: r._id,
              displayName: r.fname,
              status: ChatParticipantStatus.Online,
              avatar: '/assets/shared/group-icon.png',
              chattingTo: groupInfos[i].members.map(m => {
                return {
                  id: m.username,
                  displayName: m.name,
                  avatar: `/api/pri/user/${m.username}/avatar`
                }
              })//r.usernames != null ? r.usernames.map(username => d.users.find(u => u.username == username)) : []
              //avatar: ds[1][user.username]
            };
            groupFriends.push({
              participant: groupInfo
            });

            this.roomIdToNgGroups[groupInfo.id] = groupInfo;
          }
          this.friends = [].concat(groupFriends).concat(this.friends);
          this.onFriendsListChanged(this.friends);
          // this.friends = this.friends.concat(rooms.update.filter(r => r.name != null).map(r => {
          //   return
          // })
        });

      }


      setTimeout(() => {
        try {
          if (this.updateFunc != null) {
            this.updateFunc.apply(this.updateTarget, []);
          }
        } catch (ex) {
          console.log(ex);
        }

      });
    });
  }

  getUserStatus(status: string): ChatParticipantStatus {
    if (status === 'online') {
      return ChatParticipantStatus.Online;
    }
    if (status === 'away') {
      return ChatParticipantStatus.Away;
    }
    if (status === 'busy') {
      return ChatParticipantStatus.Busy;
    }
    return ChatParticipantStatus.Offline;
  }



  listFriends(): Observable<ParticipantResponse[]> {
    return of(this.friends);
  }

  getMessageHistoryByPage(destinataryId: any, size: number, page: number): Observable<Message[]> {
    console.log(destinataryId, size, page);
    return new Observable(subscriber => {
      setTimeout(() => {
        let isChannel = false;
        let rid = this.directMessageRoomIds[destinataryId];
        if (rid == null) {
          isChannel = true;
          rid = this.groupIdToRoomIds[destinataryId]
        }
        if (rid == null) {
          subscriber.next([]);
          subscriber.complete();
        } else {
          if (isChannel == false) {
            this._retrieveIMHistory(rid, (page - 1) * size, size, destinataryId, subscriber)
          } else {
            this._retrieveChannelHistory(rid, (page - 1) * size, size, destinataryId, subscriber);
          }

        }

      }, 1000);

    });

  }

  convertMessage(msg: any, destinataryId: string): Message {
    let message: Message;
    if (msg.file != null) {
      let attachments = msg.attachments || [];
      let attachment = attachments.find(at => at.title == msg.file.name);
      if (attachment != null) {

        if (msg.file.type.indexOf('image/') >= 0) {
          message = new Message();
          message.type = MessageType.Image;
          message.message = '/chatfile' + attachment.title_link;
        } else {
          let fileMsg = new FileMessage();
          fileMsg.downloadUrl = '/chatfile' + attachment.title_link;
          fileMsg.mimeType = msg.file.type;
          fileMsg.message = msg.file.name;
          message = fileMsg;
        }
        message.dateSent = new Date(msg.ts)
        message.fromId = msg.u.username;
        message.toId = msg.u.username === destinataryId ? this.currentUserName : destinataryId;

        return message;
      }
    } else {
      message = {
        type: msg.attachments != null && msg.attachments.length > 0 ? MessageType.File : MessageType.Text,
        fromId: msg.u.username,
        message: msg.msg,
        toId: msg.u.username === destinataryId ? this.currentUserName : destinataryId,
        dateSent: new Date(msg.ts)
      };
      let emojiMatches = this._getAllEmojiMatches(message.message);
        if (emojiMatches.length > 0) {
          message.message = this._replaceEmoji(message.message, emojiMatches);
        }
    }
    // if (isGroup == true) {
    //   message.chattingTo = this.friends.filter(x => x.participant.id == message.fromId).map(x => x.participant);
    // }
    return message;
  }

  convertGroupMessage(msg: any, destinataryId: string): Message {
    let message: Message;
    if (msg.file != null) {
      let attachments = msg.attachments || [];
      let attachment = attachments.find(at => at.title == msg.file.name);
      if (attachment != null) {
        if (msg.file.type.indexOf('image/') >= 0) {
          message = new Message();
          message.type = MessageType.Image;
          message.message = '/chatfile' + attachment.title_link;
        } else {
          let fileMsg = new FileMessage();
          fileMsg.downloadUrl = '/chatfile' + attachment.title_link;
          fileMsg.mimeType = msg.file.type;
          fileMsg.message = msg.file.name;
          message = fileMsg;
        }
        message.dateSent = new Date(msg.ts)
        message.fromId = msg.u.username;
        message.toId = msg.u.username === destinataryId ? this.currentUserName : destinataryId;

      }
    } else {
      message = {
        type: msg.attachments != null && msg.attachments.length > 0 ? MessageType.File : MessageType.Text,
        fromId: msg.u.username,// === destinataryId ? destinataryId : null,
        message: msg.msg,
        toId: null, // msg.u.username === destinataryId ? this.currentUserName : destinataryId,
        dateSent: new Date(msg.ts)
      }
      if (msg.t == 'r') {
        message.message = `<font color="green">tên nhóm đã được đổi thành '<b>${msg.msg}</b>'</font>`;
      } else if (msg.t == 'subscription-role-added') {
        message.message = `<font color="green">'<b>${msg.msg}</b>' đã là chủ nhóm</font>`;
      } else if (msg.t == 'subscription-role-removed') {
        message.message = `<font color="green">'<b>${msg.msg}</b>' đã không còn là chủ nhóm</font>`;
      } else {
        let emojiMatches = this._getAllEmojiMatches(message.message);
        if (emojiMatches.length > 0) {
          message.message = this._replaceEmoji(message.message, emojiMatches);
        }
      }
    }
    // message.chattingTo = this.friends.filter(x => x.participant.id == message.fromId).map(x => x.participant);
    return message;
  }

  getMessageHistory(destinataryId: any): Observable<Message[]> {
    return new Observable(subscriber => {
      setTimeout(() => {
        let isChannel = false;
        let rid = this.directMessageRoomIds[destinataryId];
        if (rid == null) {
          isChannel = true;
          rid = this.groupIdToRoomIds[destinataryId]
        }
        if (rid == null) {
          subscriber.next([]);
          subscriber.complete();
        } else {
          if (isChannel == false) {
            this._retrieveIMHistory(rid, 0, 100, destinataryId, subscriber)
          } else {
            this._retrieveChannelHistory(rid, 0, 100, destinataryId, subscriber);
          }

        }
      }, 1000);
    });
  }

  private _retrieveChannelHistory(rid, skip, count, destinataryId, subscriber) {
    this.rocketService.getChannelHistory(this.userId, this.userAuth, rid, skip, count).toPromise().then((data: any) => {
      const messages = data.messages.reverse().map(msg => this.convertGroupMessage(msg, destinataryId));
      subscriber.next(messages);
      subscriber.complete();
    });
  }

  private _retrieveIMHistory(rid, skip, count, destinataryId, subscriber) {
    this.rocketService.getIMHistory(this.userId, this.userAuth, rid, skip, count, true).toPromise().then((data: any) => {
      const messages = data.messages.reverse().map(msg => this.convertMessage(msg, destinataryId));
      subscriber.next(messages);
      subscriber.complete();
    });
  }

  sendMessage(message: Message): void {
    if (message instanceof FileMessage) {
      return;
    }
    console.log(message);
    const toId = message.toId;
    const rid = this.directMessageRoomIds[toId] != null ? this.directMessageRoomIds[toId] : this.groupIdToRoomIds[toId];
    const msg = message.message;
    this.rocketRealTime.callMethod('sendMessage',
      {
        rid, msg
      }
    ).subscribe(data => {
      console.log('Send Messsage data', data);
    });
    let emojiMatches = this._getAllEmojiMatches(message.message);
        if (emojiMatches.length > 0) {
          message.message = this._replaceEmoji(message.message, emojiMatches);
        }
  }
  groupCreated(group: Group): void {
    const userids = group.chattingTo.map(u => u.id);
    let groupName = group.chattingTo.map(u => u.displayName).join(',');
    if (groupName.length > 255) {
      groupName = groupName.substring(0, 254);
    }
    group.displayName = groupName;
    this.rocketRealTime.callMethod('createPrivateGroup',
      groupName,
      userids
    ).subscribe((groupData: any) => {
      const rid = groupData.result.rid;
      this.groupMessageIds[rid] = userids;
      this.groupIdToRoomIds[group.id] = rid;
      this.roomIdToNgGroups[rid] = group;
      this.subscribeForRoomMessage(group, rid);
    });
  }

  onParticipantClicked(participant: IChatParticipant) {
    if (participant.participantType == ChatParticipantType.Group) {
      this._createGroup(participant);
    } else {
      this._createDirectMessage(participant);
    }

  }

  private _createGroup(participant: IChatParticipant) {
    this.groupIdToRoomIds[participant.id] = participant.id;
    this.roomIdToNgGroupOpeneds[participant.id] = participant;
    this.subscribeForRoomMessage(participant, participant.id);
    // this.directMessageSubscriptions[participant.id] =
    // this.rocketRealTime.getSubscription('stream-room-messages', participant.id, false).subscribe((sData) => {
    //   const fields = sData.fields;
    //   if (fields.eventName === participant.id) {
    //     const args: any[] = fields.args;
    //     args.forEach(arg => {
    //       if (arg.u.username != this.currentUserName) {
    //         this.onMessageReceived(participant, {
    //           fromId: arg.u.username,
    //           message: arg.msg,
    //           toId: this.currentUserName,
    //           dateSent: new Date(arg.ts.$date)
    //         });
    //       }
    //     });
    //   }
    // });
  }

  private _createDirectMessage(participant: IChatParticipant) {
    const participantUsername = participant.id;
    this.rocketRealTime.callMethod('createDirectMessage', participantUsername).subscribe(data => {
      this.directMessageRoomIds[participantUsername] = data.result.rid;
      const rid = data.result.rid;
      this.subscribeForRoomMessage(participant, rid);
    });
  }

  subscribeForRoomMessage(participant: any, rid: string) {
    const participantUsername = participant.id;
    this.directMessageSubscriptions[participantUsername] =
      this.rocketRealTime.getSubscription('stream-room-messages', rid, false).subscribe((sData) => {
        const fields = sData.fields;
        if (fields.eventName === rid) {
          const args: any[] = fields.args;
          args.forEach(arg => {
            if (arg.u.username != this.currentUserName) {
              // this.onMessageReceived(participant, {
              //   fromId: arg.u.username,
              //   message: arg.msg,
              //   toId: this.currentUserName,
              //   dateSent: new Date(arg.ts.$date)
              // });
              this.onMessageReceived(participant, this.convertMessage(arg, participant.id));
            }
          });
        }
      });
  }

  onParticipantChatClosed(participant: IChatParticipant) {
    console.log('close', participant);
    const subscription = this.directMessageSubscriptions[participant.id];
    if (subscription != null) {
      subscription.unsubscribe();
      delete this.directMessageSubscriptions[participant.id];
    }
    delete this.directMessageRoomIds[participant.id];
    const rid = this.groupIdToRoomIds[participant.id];
    delete this.groupMessageIds[rid];
    delete this.groupIdToRoomIds[participant.id];
    delete this.roomIdToNgGroupOpeneds[rid];
  }

  private _getAllEmojiMatches(msgText: string): string[] {
    let result = msgText.match(this.regex);
    let allMatches: string[] = [];
    while (result != null) {

      allMatches.push(result[0]);
      msgText = msgText.replace(result[0], '');
      result = msgText.match(this.regex);
    }
    return allMatches;
  }

  private _replaceEmoji(msgText: string, allMatches: string[]): string {
    allMatches.forEach(s => {
      let emojiData = this.emoji[s];
      if (emojiData != null) {
        let emojiHtml = `<span class="emojione emojione-${emojiData.category} _${emojiData.uc_base} big"></span>`;
        msgText = msgText.replace(s, emojiHtml);
      } else if (s == ':like:') {
        let emojiHtml = `<span class="emojione emoji-like big"></span>`;
        msgText = msgText.replace(s, emojiHtml);
      }
    });
    return msgText;
  }

}
