import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { map } from 'rxjs/operators';
import { KeyCloakUserService } from 'src/generated/account-api/api/keyCloakUser.service';
import { forkJoin, Subscription } from 'rxjs';
import { RequestGroupGetResult } from './../../../generated/web-api/model/requestGroupGetResult';
import { environment } from './../../../environments/environment';
import { RequestGroupService } from './../../../generated/web-api/api/requestGroup.service';
import { Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from '@angular/material/dialog';
import { OneAlertService } from "src/app/core/service/one-alert.service";
import { OneRouteService } from "src/app/core/service/one-route.service";
import * as message from '../../core/common/one-message';
import * as cst from '../../core/common/one-const';
import * as util from '../../core/common/one-util';
import { GroupFormComponent } from "../group/component";
import { RequestFormComponent } from "../request/component";
import { RequestGroup } from 'src/generated/web-api';
import { U } from '@angular/cdk/keycodes';
import { RequestGroupWrapperService } from 'src/app/core/service/request-group-wrapper.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'landing-page-component',
  templateUrl: './landingPage.component.html',
  styleUrls: ['./landingPage.component.scss']
})

export class LandingPageComponent implements OnInit, OnDestroy {

  groupId?: number;
  toMe?: boolean;
  fromMe?: boolean;
  followed?: boolean;

  isLoading = false;

  public grouppedGroups: any[] = [];
  public isOwner: boolean;
  private groups: RequestGroup[] = [];
  private sub: Subscription;

  constructor(private oneRoute: OneRouteService,
    private oneAlert: OneAlertService,
    private route: ActivatedRoute,
    private requestGroupWrapperService: RequestGroupWrapperService,
    private keycloakUserService: KeyCloakUserService,
    private keycloakService: KeyCloakService,
    public dialog: MatDialog) {

      this.sub = this.route.queryParams.subscribe(params => {
        this.toMe = (params['toMe'] == "true");
        this.fromMe = (params['fromMe'] == "true");
        this.followed = (params['followed'] == "true");
      });
  }

  ngOnInit() {
    this.loadGroups();
    this.isOwner = this.keycloakService.isOwner();
  }

  ngOnDestroy() {
    if(this.sub) {
      this.sub.unsubscribe();
      this.sub = null;
    }
  }

  public addNewRequest() {
    this.oneAlert.onSuccess();
  }

  public openRequestCreateForm() {
    this.isLoading = true;
    this.keycloakUserService.getUsers().toPromise().then((result: any) => {
      const dialogRef = this.dialog.open(RequestFormComponent, {
        width: '1200px',
        position: {
          top: '65px'
        },
        hasBackdrop: true,
        disableClose: true,
        data: {
          requestGroups: this.groups,
          users: result.items.filter(user => {
            return this.keycloakService.isActive(user);
          })
          .map(user => {
            return {
              ...user,
              avatarUrl: this.keycloakService.getAvatar(user),
              fullName: this.keycloakService.getFullName(user),
              username: this.keycloakService.getUsername(user)
            }
          })
        }
      });

      dialogRef.afterClosed().subscribe((result: boolean) => {
        if(result) {
          this.oneRoute.navigateTo(['/requests'], this.oneRoute.queryParams).then((value) => {
            console.log(value);
            // leave it for now
            window.location.reload();
          });
        }
      })
    }).finally(() => {
      this.isLoading = false;
    })

  }

  public openGroupCreateForm() {
    this.isLoading = true;
    this.keycloakUserService.getUsers()
      .toPromise().then((result: any) => {
        const dialogRef = this.dialog.open(GroupFormComponent, {
          width: '800px',
          position: {
            top: '65px'
          },
          hasBackdrop: true,
          disableClose: true,
          data: {
            users: result.items.filter(user => {
              return this.keycloakService.isActive(user);
            }).map(user => {
              return {
                ...user,
                avatarUrl: this.keycloakService.getAvatar(user),
                fullName: this.keycloakService.getFullName(user),
                username: this.keycloakService.getUsername(user)
              }
            })
          }
        });

        dialogRef.afterClosed().subscribe(result => {
          if(result.status == cst.SAVE_STATUS.OK) {
            window.location.reload();
          }
        })
      }).finally(() => {
        this.isLoading = false;
      })
  }

  isInRouting(groupId: number) {
    if (this.isInRequest) {
      return groupId == this.oneRoute.queryParams['groupId'];
    }
    else {
      return groupId == this.oneRoute.params['id'];
    }
  }

  navToGroup(group: RequestGroup) {
    this.groupId = group.id;
    if (this.isInRequest) {
      this.updateFilter();
    }
    else {
      this.oneRoute.navigateTo(['/groups', group.id]);
    }
  }

  updateToMe(toMe: boolean) {
    this.toMe = toMe;
    this.updateFilter();
  }

  updateFromMe(fromMe: boolean) {
    this.fromMe = fromMe;
    this.updateFilter();
  }

  updateFollowed(followed: boolean) {
    this.followed = followed;
    this.updateFilter();
  }

  navToRequest() {
    this.oneRoute.navigateTo(['/requests']);
  }

  private loadGroups() {
    this.requestGroupWrapperService.getAll()
      .then((responses: RequestGroup[]) => {
        let activeGroups = responses.filter((requestGroup: RequestGroup) => {
          return requestGroup.status == RequestGroup.StatusEnum.ENABLED;
        })
        this.groups = activeGroups;
        this.grouppedGroups = this.groupByCategories(activeGroups);
      });
  }

  private groupByCategories(groups: RequestGroup[]) {
    let directGroup = groups.find((requestGroup: RequestGroup) => {
      return requestGroup.oId == 0;
    });

    let emptyCategoryGroups = groups.filter((group: RequestGroup) => {
      return group.oId != 0 && (group.category == null || group.category == "");
    })
    .sort((a, b) => (a.name > b.name) ? 1 : -1);

    let otherGroups = groups.filter((group: RequestGroup) => {
      return group.oId != 0 && group.category != null && group.category != "";
    });

    let grouppedResult = util.groupBy(otherGroups, 'category');
    let grouppedAsList = Object.keys(grouppedResult).map(function (categoryName: any) {
      let groups = grouppedResult[categoryName].sort((a, b) => (a.name > b.name) ? 1 : -1);
      return { category: categoryName, groups: groups, isExpanded: true };
    });

    let sortedGroupList = grouppedAsList.sort((a, b) => (a.category > b.category) ? 1 : -1);
    sortedGroupList.push({
      category: 'Khác',
      groups: emptyCategoryGroups,
      isExpanded: true
    });

    return [{
      category: 'QUAN TRỌNG',
      groups: [directGroup],
      isExpanded: true
    }].concat(sortedGroupList);
  }

  private updateFilter() {
    let parameter = {...this.oneRoute.queryParams};
    parameter['groupId'] = this.groupId;
    if (this.toMe == false && this.fromMe == false && this.followed == false) {
      parameter['toMe'] = null;
      parameter['fromMe'] = null;
      parameter['followed'] = null;
    } else {
      parameter['toMe'] = this.toMe;
      parameter['fromMe'] = this.fromMe;
      parameter['followed'] = this.followed;
    }


    this.oneRoute.navigateTo([this.oneRoute.pathWithoutOptionalParams], parameter);
  }

  public get isInDetail() {
    return !!this.oneRoute.params['id'] == true && (this.isInRequest || this.isInSystemRequest);
  }

  public get isInRequest() {
    let regex = /^\/requests/gi;
    return regex.test(this.oneRoute.current);
  }

  public get isInSystemRequest() {
    let regex = /^\/systemRequest/gi;
    return regex.test(this.oneRoute.current);
  }

  public get isInGroup() {
    let regex = /^\/(groups|systemRequest)/gi;
    return regex.test(this.oneRoute.current);
  }

  get isGroupRequests() {
    return this.oneRoute.queryParams['groupId'];
  }

  public get text() {
    return message;
  }

  public isOnPage(pageUrl: string) {
    return this.oneRoute.current == pageUrl;
  }
}

