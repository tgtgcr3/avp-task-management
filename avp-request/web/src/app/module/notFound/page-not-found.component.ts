import {Component} from '@angular/core';
import * as message from '../../core/common/one-message';

@Component({
    templateUrl: './page-not-found.component.html',
    styleUrls: ['./page-not-found.component.scss'],
    selector: 'page-not-found'
})
export class PageNotFoundComponent {

    public get text() {
        return message;
    }
}