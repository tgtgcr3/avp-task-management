import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';
import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import {OneRouteService} from 'src/app/core/service/one-route.service';
import { RequestGroupWrapperService } from 'src/app/core/service/request-group-wrapper.service';
import { KeyCloakUserService } from 'src/generated/account-api';
import { RequestGroup } from 'src/generated/web-api';
import { RequestListComponent } from './component/request-list/request-list.component';
import {SecurityService} from "../../core/service/security.service";

@Component({
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
  selector: 'request'
})
export class RequestComponent implements OnInit, OnDestroy {

  requestListFullView = true;

  filter: any = {
    requestGroup: null
  };
  loaded: boolean = false;
  users: any[];

  private sub: Subscription;

  constructor(private oneRoute: OneRouteService,
    private route: ActivatedRoute,
    private requestGroupWrapperService: RequestGroupWrapperService,
    private keycloakService: KeyCloakService,
    private securityService: SecurityService,
    private keycloakUserService: KeyCloakUserService) {
    this.sub = this.route.queryParams.subscribe(params => {
      if (this.filter.groupId != params['groupId'] && params['groupId']) {
        this.loadGroup(params);
      }
      else {
        this.loaded = false;

        if(!params['groupId']) {
          this.filter.requestGroup = null;
        }

        let filterParams = {...params, ...{requestGroup: this.filter.requestGroup}};
        this.filter = filterParams;
        this.loaded = true;
      }
    });
  }

  ngOnInit() {
    this.keycloakUserService.getUsers(null, null, null, null, null, 0,
      1024).toPromise().then(
      (result: any) => {
        this.users = result.items.map(user => {
          return {
            ...user,
            avatarUrl: this.keycloakService.getAvatar(user),
            fullName: this.keycloakService.getFullName(user),
            username: this.keycloakService.getUsername(user)
          }
        });
      }
    );
  }

  ngOnDestroy() {
    if(this.sub) {
      this.sub.unsubscribe();
      this.sub = null;
    }
  }

  private loadGroup(params: any) {
    this.loaded = false;
    this.requestGroupWrapperService.get(params['groupId']).then(
      (requestGroup: RequestGroup) => {
        let filterParams = {...params, ...{requestGroup: requestGroup}};
        this.filter = filterParams;
      }
    )
    .finally(() => this.loaded = true);
  }

  public get isInDetail() {
    return !!this.oneRoute.params['id'] == true && (this.isInRequest || this.isInSystemRequest) && this.securityService.getAccessToken() != null;
  }

  public get isInRequest() {
    let regex = /^\/requests/gi;
    return regex.test(this.oneRoute.current);
  }

  public get isInSystemRequest() {
    let regex = /^\/systemRequest/gi;
    return regex.test(this.oneRoute.current);
  }
}
