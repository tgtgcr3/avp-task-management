import { RequestListComponent } from './component/request-list/request-list.component';
import { BasicAttributeInputComponent } from './component/request-group-template/attribute-inputs/basic-attribute-input-type.component';
import { RequestGroupTemplateComponent } from './component/request-group-template/request-group-template.component';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { RequestComponent } from './request.component';
import { RequestDetailComponent, RequestFormComponent, RequestHeaderComponent, RequestLiComponent, RequestDetailSidebarComponent, StatusConfirmDialog, TaskDialog } from './component/index';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { ComponentModule } from '../../core/component/component.module';
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RequestGroupTemplateDirective } from './component/request-group-template/request-group.directive';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from "@angular/material/card";
import { MatListModule } from "@angular/material/list";
import {MatExpansionModule} from '@angular/material/expansion';
import {MatMenuModule} from '@angular/material/menu';
import {DateAttributeInputComponent} from "./component/request-group-template/attribute-inputs/date-attribute-input-type.component";
import {FileAttributeInputTypeComponent} from "./component/request-group-template/attribute-inputs/file-attribute-input-type.component";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MaterialFileInputModule} from "ngx-material-file-input";
import {DateTimeAttributeInputComponent} from "./component/request-group-template/attribute-inputs/datetime-attribute-input-type.component";
import {NgxMatDatetimePickerModule, NgxMatTimepickerModule} from "@angular-material-components/datetime-picker";
import {NgxMatMomentModule} from "@angular-material-components/moment-adapter";
import {SelectAttributeInputComponent} from "./component/request-group-template/attribute-inputs/select-attribute-input-type.component";
import {
  RequestAttributeBaseComponent,
  RequestAttributeSelectComponent,
  RequestAttributeFileComponent,
  RequestAttributeDateComponent,
  RequestAttributeTemplateComponent,
  RequestAttributeTemplateDirective, RequestAttributeTableComponent
} from "./component/request/detail/request-attribute-template";
import {TableAttributeInputComponent} from "./component/request-group-template/attribute-inputs/table-attribute-input-type-component";
import {TitleAttributeInputComponent} from "./component/request-group-template/attribute-inputs/title-attribute-input-type.component";
import {RequestAttributeTitleComponent} from "./component/request/detail/request-attribute-template/request-attribute/request-attribute-title.component";
import {DiscussionsComponent} from "./component/request/detail/discussion/discussions.component";
import {DiscussionComponent} from "./component/request/detail/discussion/discussion/discussion.component";
import {MultiDateTimeAttributeInputComponent} from "./component/request-group-template/attribute-inputs/multi-datetime-attribute-input-type.component";
import {RequestAttributeMultiDatetimeComponent} from "./component/request/detail/request-attribute-template/request-attribute/request-attribute-multi-datetime.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatChipsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatInputModule,
    ComponentModule,
    NgxMatSelectSearchModule,
    ReactiveFormsModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatListModule,
    MatMenuModule,
    MatDatepickerModule,
    MatProgressBarModule,
    MaterialFileInputModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatMomentModule,
    MatExpansionModule
  ],
  declarations: [
    RequestComponent,
    RequestHeaderComponent,
    RequestLiComponent,
    RequestDetailComponent,
    RequestFormComponent,
    BasicAttributeInputComponent,
    DateAttributeInputComponent,
    DateTimeAttributeInputComponent,
    FileAttributeInputTypeComponent,
    SelectAttributeInputComponent,
    TableAttributeInputComponent,
    TitleAttributeInputComponent,
    RequestGroupTemplateComponent,
    RequestGroupTemplateDirective,
    MultiDateTimeAttributeInputComponent,
    RequestListComponent,
    RequestDetailSidebarComponent,
    StatusConfirmDialog,
    RequestAttributeBaseComponent,
    RequestAttributeSelectComponent,
    RequestAttributeFileComponent,
    RequestAttributeDateComponent,
    RequestAttributeTemplateComponent,
    RequestAttributeTemplateDirective,
    RequestAttributeTableComponent,
    RequestAttributeMultiDatetimeComponent,
    TaskDialog,
    RequestAttributeTitleComponent,
    DiscussionsComponent,
    DiscussionComponent
  ],
  exports: [
    RequestFormComponent
  ]
})
export class RequestModule { }

