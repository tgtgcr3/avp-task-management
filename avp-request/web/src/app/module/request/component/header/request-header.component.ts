import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { OneRouteService } from 'src/app/core/service/one-route.service';
import {OneRequest, RequestGroup, RequestService} from 'src/generated/web-api';
import * as message from '../../../../core/common/one-message';
import * as constant from '../../../../core/common/one-const';
import * as util from '../../../../core/common/one-util';
import { ActivatedRoute } from '@angular/router';
import {environment} from "../../../../../environments/environment";
import {formatDate} from "@angular/common";
import * as FileSaver from 'file-saver';
import {OneAlertService} from "../../../../core/service/one-alert.service";

@Component({
    templateUrl: './request-header.component.html',
    styleUrls: ['./request-header.component.scss'],
    selector: 'request-header'
})
export class RequestHeaderComponent implements OnChanges {

    @Input()
    filter: any;

    @Input()
    users: any[] = [];

    opened: boolean = false;

    filterFormGroup: FormGroup;

    currentStatus?: string;

    statuses: any[] = [];

    refreshing: boolean = false;

    isInFilter = false;

    constructor(private oneRoute: OneRouteService,
        private route: ActivatedRoute,
        private requestService: RequestService,
                private oneAlert: OneAlertService,
        private formBuilder: FormBuilder) {

        this.statuses = constant.StatusFilters;

        this.filterFormGroup = this.formBuilder.group({
            name: [''],
            status: [null],
            submitters: [null],
            submitTime: this.formBuilder.group({
                start: [null],
                end: [null]
            })
        });
    }

    slideSearchBar(open: boolean) {
        this.opened = open;
    }

    updateFilter() {
        let params = this.oneRoute.queryParams;
        params['status'] = util.capitalizeFirstLetter(this.filterFormGroup.controls.status.value);
        params['query'] = this.filterFormGroup.controls.name.value;
        params['creators'] = this.filterFormGroup.controls.submitters.value;

        params['createdDate'] = this.filterFormGroup.controls.submitTime.value.start ?
                                        this.filterFormGroup.controls.submitTime.value.start.getTime() : null;

        params['end'] = this.filterFormGroup.controls.submitTime.value.end ?
                                        this.filterFormGroup.controls.submitTime.value.end.getTime() : null;
        this.isInFilter = params['createdDate'] != null;
        this.oneRoute.navigateTo([this.oneRoute.pathWithoutOptionalParams], params);
        this.slideSearchBar(false);
    }

    onFilter(status?: string) {
        this.filterFormGroup.controls.status.setValue(status?.toUpperCase());
        this.currentStatus = status;
        let params = this.oneRoute.queryParams;
        params['status'] = status;
        this.oneRoute.navigateTo([this.oneRoute.pathWithoutOptionalParams], params);
    }

    onBack() {
        if (this.oneRoute.current.includes('requests')) {
            this.oneRoute.navigateTo(['/requests'], this.oneRoute.queryParams).then(() => {
        //      window.location.reload();
            });
        }
        else if (this.oneRoute.current.includes('systemRequest')) {
            this.oneRoute.navigateTo(['/systemRequest'], this.oneRoute.queryParams);
        }
    }

    onEditGroup() {
        this.oneRoute.navigateTo(['/groups', this.filter.requestGroup.id]);
    }

    onRefresh() {
        this.refreshing = true;
        this.oneRoute.refreshComponent();
        setTimeout(() => {
            this.refreshing = false;
        }, 2000);
    }

    ngOnChanges(changes: SimpleChanges) {
        if(changes.filter) {
            this.currentStatus = this.filter.status;
            this.filter.status ?
                this.filterFormGroup.controls.status.setValue(this.filter.status.toUpperCase()) :
                this.filterFormGroup.controls.status.setValue(null);

            this.filterFormGroup.controls.name.setValue(this.filter.query);
            this.filterFormGroup.controls.submitters.setValue(this.filter.creators);

            if(this.filter.createdDate && this.filter.end) {
                let start = new Date(Number.parseInt(this.filter.createdDate));
                let end = new Date(Number.parseInt(this.filter.end));
                (this.filterFormGroup.controls.submitTime as FormGroup).controls.start.setValue(start);
                (this.filterFormGroup.controls.submitTime as FormGroup).controls.end.setValue(end);
            }
        }
    }

    get isInDetail() {
        return !!this.oneRoute.params['id'] == true;
    }

    get isFilterAll() {
        return this.currentStatus == null;
    }

    get isOverdue() {
        return this.currentStatus?.toUpperCase() == OneRequest.StatusEnum.TIMEOUT;
    }

    get isPending() {
        return this.currentStatus?.toUpperCase() == OneRequest.StatusEnum.PENDING;
    }

    get isApproved() {
        return this.currentStatus?.toUpperCase() == OneRequest.StatusEnum.APPROVED;
    }

    get isRejected() {
        return this.currentStatus?.toUpperCase() == OneRequest.StatusEnum.REJECTED;
    }

    get isCancelled() {
        return this.currentStatus?.toUpperCase() == OneRequest.StatusEnum.CANCELLED;
    }

    get text() {
        return message;
    }

    get title() {
        return this.filter.requestGroup ? this.filter.requestGroup.name : 'Danh sách đề xuất';
    }

  exportExcelFiles() {
      let status: any = null;
    status = this.filterFormGroup.controls.status.value != null ? util.capitalizeFirstLetter(this.filterFormGroup.controls.status.value) : null;
    let query = this.filterFormGroup.controls.name.value;
    let creators = this.filterFormGroup.controls.submitters.value;

    let createdDate = this.filterFormGroup.controls.submitTime.value.start ?
      this.filterFormGroup.controls.submitTime.value.start.getTime() : null;

    let end = this.filterFormGroup.controls.submitTime.value.end ?
      this.filterFormGroup.controls.submitTime.value.end.getTime() : null;
    this.requestService.exportAllRequests(environment.tenantId, query, 0, 20, true, null, status,
      null, null, null, null, createdDate, end, creators).toPromise().then((data) => {
        let name = 'request_export';
        if (createdDate) {
          name += '_' + formatDate(createdDate, 'dd-mm-yyyy', 'en-US');
        }
        if (end) {
          name += '_' + formatDate(end, 'dd-mm-yyyy', 'en-US');
        }
      FileSaver.saveAs(data, name + '.xlsx');
    }).catch(error => {
      this.oneAlert.onError('Không tồn tại đề xuất thõa điều kiện');
    });

  }
}
