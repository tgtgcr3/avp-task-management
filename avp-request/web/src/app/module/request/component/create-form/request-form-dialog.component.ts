import { environment } from './../../../../../environments/environment';
import { RequestService } from './../../../../../generated/web-api/api/request.service';
import { OneRequest } from './../../../../../generated/web-api/model/oneRequest';
import { FormControl, Validators } from '@angular/forms';
import { RequestGroup } from './../../../../../generated/web-api/model/requestGroup';
import { Component, Inject, OnInit } from '@angular/core';
import * as message from '../../../../core/common/one-message';
import * as cst from '../../../../core/common/one-const';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import * as transliterate from '@sindresorhus/transliterate';
import { OneRouteService } from 'src/app/core/service/one-route.service';
import { OneAlertService } from 'src/app/core/service/one-alert.service';
import { FileInfo, FileService } from 'src/generated/web-api';

@Component({
  templateUrl: './request-form-dialog.component.html',
  selector: 'request-form-dialog',
  styleUrls: ['./request-form-dialog.component.scss']
})
export class RequestFormComponent implements OnInit {

  requestFormGroup: FormGroup;

  allUsers: any[] = [];

  isEdit: boolean = false;
  isClone: boolean = false;

  requestGroups: RequestGroup[] = [];

  public requestGroupFilter: FormControl = new FormControl();

  protected _onDestroy = new Subject<void>();

  public filteredRequestGroups: ReplaySubject<RequestGroup[]> = new ReplaySubject<RequestGroup[]>(1)

  submitted: boolean = false;

  attrSubject: Subject<string> = new Subject<string>();

  attrObservable: Observable<string>;

  requestId = 0;

  taskIds = [];

  attachmentInfo: any = {
    oneControl: null,
    attr: {
      name: 'Tệp đính kèm',
      required: false
    }
  };
  directRequest = false;

  get title() {
    return this.isEdit ? 'Chỉnh sửa đề xuất' : 'Tạo đề xuất mới';
  }

  get saveText() {
    return this.isEdit ? 'Lưu' : 'Tạo đề xuất';
  }

  constructor(
    private formBuilder: FormBuilder,
    private requestService: RequestService,
    private dialogRef: MatDialogRef<RequestFormComponent>,
    private oneRoute: OneRouteService,
    private oneAlert: OneAlertService,
    private fileService: FileService,
    @Inject(MAT_DIALOG_DATA) private dialogData: any
  ) {
    this.requestGroups = dialogData.requestGroups.sort((a, b) => a.name.localeCompare(b.name));
    this.allUsers = dialogData.users;
    this.requestGroupFilter.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
      this.filterRequestGroups();
    });
    this.requestFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      requestGroup: [null, Validators.required],
      requestGroupData: [null, Validators.required],
      approvers: [null, Validators.required],
      followers: [null],
      fixedUsers: [null],
      directMgmts: [null]
    });
    this.attachmentInfo.oneControl = new FormControl();

    if (dialogData.isEdit || dialogData.isClone) {
      this.isEdit = dialogData.isEdit;
      this.isClone = dialogData.isClone;
      let attributeValue = JSON.parse(dialogData.request.value);
      this.requestFormGroup = this.formBuilder.group({
        name: [dialogData.request.name, Validators.required],
        requestGroup: [this.requestGroups[0], Validators.required],
        requestGroupData: [attributeValue, Validators.required],
        approvers: [this.requestGroups[0].approvers, Validators.required],
        followers: [this.requestGroups[0].followers],
        fixedUsers: [this.requestGroups[0].followers],
        directMgmts: [dialogData.request.directMgmts]
      });
      let attachment = dialogData.request.documents.length > 0 ? dialogData.request.documents[0] : null;
      this.attachmentInfo.oneControl.setValue(attachment);
      this.requestId = dialogData.request.id;
      this.taskIds = dialogData.request.taskIds;
    }
  }

  filterRequestGroups() {
    if (!this.requestGroups) {
      return;
    }
    let search = this.requestGroupFilter.value;
    if (!search) {
      this.filteredRequestGroups.next(this.requestGroups.slice());
      return;
    } else {
      search = transliterate(search).toLowerCase();
    }
    this.filteredRequestGroups.next(this.requestGroups.filter(ut => transliterate(ut.name).toLowerCase().indexOf(search) >= 0));
  }

  ngOnInit(): void {

    this.filterRequestGroups();
    this.requestFormGroup.controls.approvers.valueChanges.subscribe((data: any) => {
      console.log(data);
    });
    this.requestFormGroup.controls.followers.valueChanges.subscribe((data: any) => {
      console.log(data);
    });

    this.requestFormGroup.controls.requestGroup.valueChanges.subscribe((group: RequestGroup) => {
      this.requestFormGroup.controls.approvers.setValue(group.approvers || []);
      this.requestFormGroup.controls.followers.setValue(group.followers || []);
      this.requestFormGroup.controls.fixedUsers.setValue(group.followers || []);

      this.directRequest = group.oId == 0 && this.isEdit == false;
      if (this.isEdit == true || this.isClone == true) {
        if (group.oId == 0) {
          this.requestFormGroup.controls.approvers.setValue(this.dialogData.request.approvers);
          this.requestFormGroup.controls.followers.setValue(this.dialogData.request.followers);
          this.requestFormGroup.controls.fixedUsers.setValue(this.dialogData.request.followers);
        }
      }
    });


    if(this.oneRoute.queryParams['groupId']) {
      let targetGroup = this.requestGroups.find(group => {
        return group.id == this.oneRoute.queryParams['groupId'];
      });
      this.requestFormGroup.controls.requestGroup.setValue(targetGroup);
    }
    else {
      this.requestFormGroup.controls.requestGroup.setValue(this.requestGroups[0]);
    }
  }

  getCurrentRequestGroup() {
    return this.requestFormGroup.value.requestGroup;
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  get groupDescription() {
    return this.requestFormGroup.value.requestGroup.description;
  }

  public get text() {
    return message;
  }

  submitRequest() {
    this.submitted = true;

    if (this.requestFormGroup.invalid) {
      this.requestFormGroup.controls.name.markAsTouched();
      return;
    } else {
      this.attrSubject.next('validate-data');
    }

  }

  submitData(): void {
    let requestData: OneRequest = {
      name: this.requestFormGroup.value.name,
      requestGroupId: this.requestFormGroup.value.requestGroup.id,
      value: JSON.stringify(this.requestFormGroup.value.requestGroupData),
      approvers:[],
      followers: this.requestFormGroup.value.followers.filter((user: string) => {
        return !this.requestFormGroup.value.fixedUsers.includes(user);
      }),
      taskIds: this.taskIds,
      directMgmts: this.requestFormGroup.value.directMgmts
    }

    let documents = this.extractDocuments(this.requestFormGroup.value.requestGroupData);
    requestData.attrDocuments = documents;

    requestData.documents = this.attachmentInfo.oneControl.value ? [this.attachmentInfo.oneControl.value]: [];

    if (this.requestFormGroup.value.requestGroup.mgmtNotification == true && (requestData.directMgmts == null || requestData.directMgmts.length == 0)) {
      this.requestFormGroup.controls.directMgmts.setErrors({key: 'directmgmt_missing'});
      return;
    }

    if (this.isEdit) {
      this.dialogRef.close({
        status: cst.SAVE_STATUS.OK,
        payload: requestData
      });
    }
    else {
      if (!this.isClone && requestData.requestGroupId == 0) {
        requestData.approvers = this.requestFormGroup.value.approvers;
      }
      this.requestService.createRequest(environment.tenantId, requestData).toPromise().then((request: OneRequest) => {
        this.dialogRef.close(true);
      }).catch((error: any) => {
        this.oneAlert.onError(error.error);
      });
    }
  }

  onAttrEventReceived(event) {
    if (event.indexOf('validate-data') == 0) {
      if (event.indexOf(':valid') > 0) {
        this.submitData();
      }
    }
  }

  private extractDocuments(requestGroupData: object): string[] {
    let documents: string[] = [];
    for (let key in requestGroupData) {
      let value = requestGroupData[key];
      if (value != null) {
        let attr = value.attr;
        if (attr != null && attr.attrType == 5 && value.value != null && value.value != '') {
          documents.push(`${value.value}`);
        }
      }
    }
    return documents;
  }
}
