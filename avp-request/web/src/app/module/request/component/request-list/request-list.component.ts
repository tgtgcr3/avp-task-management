import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { OneRequest } from './../../../../../generated/web-api/model/oneRequest';
import { RequestGetAllResult } from './../../../../../generated/web-api/model/requestGetAllResult';
import { environment } from 'src/environments/environment';
import { Subject, Subscription} from 'rxjs';
import { RequestService } from 'src/generated/web-api';
import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { formatDate } from "@angular/common";
import { OneRouteService } from 'src/app/core/service/one-route.service';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.scss']
})

export class RequestListComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  fullView: boolean = true;

  @Input()
  filter: any = {};

  @Input()
  userInfos: any[] = [];

  isLoading: boolean = false;

  filteredRequests: ListOneRequest[] = [];
  dataSource: MatTableDataSource<ListOneRequest>;

  displayedColumns: string[] = ['idCheck', 'requestName', 'status', 'submitter', 'approvers', 'submitDate'];

  pageInfo: any = {
    hasPage: true,
    total: 0,
    pageSize: 25,
    options: [10, 25, 50, 100],
    pageIndex: 0
  };

  inSearching = false;

  public destroyed = new Subject<any>();

  get activeRequestId() {
    return this.oneRoute.params['id'];
  }

  private groupId?: number;

  private refreshSub: Subscription;

  constructor(
    private requestService: RequestService,
    private keycloakService: KeyCloakService,
    private oneRoute: OneRouteService,
    private translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.refreshSub = this.oneRoute.refreshComponentEvt.subscribe(() => this.refresh());
    this.updateVisibileHeaders();
  }

  ngOnDestroy() {
    if(this.refreshSub) {
      this.refreshSub.unsubscribe();
      this.refreshSub = null;
    }

    this.destroyed.next();
    this.destroyed.complete();
  }

  private loadRequests(): void {
    this.isLoading = true;
    this.requestService.getAllRequests(environment.tenantId, this.filter.query, this.pageInfo.pageIndex, this.pageInfo.pageSize, true,
      this.groupId, this.filter.status, this.filter.toMe, this.filter.fromMe, this.filter.followed,null,
      this.filter.createdDate, this.filter.end, this.filter.creators
    ).toPromise()
      .then((result: RequestGetAllResult) => {
        this.filteredRequests = result.items.map(request => {
          return {
            ...request,
            friendlyStatusName: this.translateService.instant(`approvalStatus.${request.status}`),
            submitterInfo: this.findUserInfos([request.submitter], [], [])[0],
            approverInfos: this.findUserInfos((request.directMgmts || []).concat(request.approvers.filter(u => request.directMgmts.indexOf(u) < 0)), request.approveds || [], request.rejecteds || []),
            submitDate: formatDate((request.submitTime > 0 ? new Date(request.submitTime) : Date.now()), 'dd-MM-yyyy', 'en-US')
          };
        });
        this.dataSource = new MatTableDataSource(this.filteredRequests);
        this.pageInfo.total = result.total;
      }).finally(() => this.isLoading = false);
  }

  onPageChanged(event) {
    this.pageInfo.pageIndex = event.pageIndex;
    this.pageInfo.pageSize = event.pageSize;
    this.loadRequests();
  }

  findUserInfos(usernames: string[], approveds: string[], rejecteds: string[]): any {
    if (usernames == null) {
      return [];
    }
    return usernames.map(username => {
      const userInfo = this.userInfos.find(user => this.keycloakService.getUsername(user) == username);
      if (userInfo == null) {
        return {
          submitterInfo: {},
          approverInfos: []
        };
      }

      return {
        ...userInfo,
        fullName: this.keycloakService.getFullName(userInfo),
        avatarUrl: this.keycloakService.getAvatar(userInfo),
        done: approveds.includes(username) ? 1 : rejecteds.includes(username) ? 2 : 0
      };
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.fullView) {
      this.updateVisibileHeaders();
    }

    if (changes.filter) {
      if (!this.filter.requestGroup) {
        this.groupId = null;
      }
      else {
        this.groupId = this.filter.requestGroup.oId;
      }
      this.loadRequests();
    }
  }

  refresh() {
    this.loadRequests();
  }

  private updateVisibileHeaders() {
    if (this.fullView) {
      this.displayedColumns = ['idCheck', 'requestName', 'status', 'submitter', 'approvers', 'submitDate'];
    } else {
      this.displayedColumns = ['idCheck', 'requestName'];
    }
  }

  gotoRequestDetails(row: any) {
    this.fullView = false;
    let id = row.id
    row.visited = true;
    if (this.oneRoute.current.includes('requests')) {
      this.oneRoute.navigateTo(['/requests', id], this.oneRoute.queryParams);
    }
    else if (this.oneRoute.current.includes('systemRequest')) {
      this.oneRoute.navigateTo(['/systemRequest', id], this.oneRoute.queryParams);
    }
  }
}

interface ListOneRequest extends OneRequest {
  friendlyStatusName: string;
  submitterInfo: any;
  approverInfos: any[];
}


