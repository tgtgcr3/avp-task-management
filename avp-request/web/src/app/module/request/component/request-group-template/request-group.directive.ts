import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
  selector: '[requestGroup]'
})

export class RequestGroupTemplateDirective {
  constructor(public viewContainerRef: ViewContainerRef) {

  }
}
