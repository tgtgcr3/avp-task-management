import { FormControl } from '@angular/forms';
import { Component, Input, OnInit } from "@angular/core";
import { Attribute, AttributeType } from "../../../../../../generated/web-api";
import { BaseAttributeInputComponent } from "./base-attribute-input.component";

@Component({
  templateUrl: './basic-attribute-input-type.component.html',
  styleUrls: ['./baisc-attribute-input-type.component.scss']
})

export class BasicAttributeInputComponent implements BaseAttributeInputComponent, OnInit {
  @Input()
  attribute: Attribute;

  @Input()
  oneControl: FormControl;

  inputType: string;

  min = 0;
  max = Number.MAX_VALUE;

  constructor() {

  }
  ngOnInit(): void {
    switch(this.attribute.attrType) {
      case 1:
        this.inputType = "string";
        break;
      case 2:
        this.inputType = "textarea";
        break;
      default:
        this.inputType = "number";
    }

    if (this.attribute.attrType == 0 && this.attribute.additionData) {
      try {
        let additionData = JSON.parse(this.attribute.additionData);
        if (additionData) {
          if (additionData.min) {
            this.min = additionData.min;
          }
          if (additionData.max) {
            this.max = additionData.max;
          }
        }
      } catch(ex) {
      }
    }
  }
}
