import { Attribute } from './../../../../../generated/web-api/model/attribute';
import {FormControl, Validators} from '@angular/forms';
import { RequestGroupTemplateDirective } from './request-group.directive';
import { BasicAttributeInputComponent } from './attribute-inputs/basic-attribute-input-type.component';
import { RequestGroup } from './../../../../../generated/web-api/model/requestGroup';
import {
  Component, ComponentFactory,
  ComponentFactoryResolver,
  Directive, EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { BaseAttributeInputComponent } from './attribute-inputs/base-attribute-input.component';
import {DateAttributeInputComponent} from "./attribute-inputs/date-attribute-input-type.component";
import {FileAttributeInputTypeComponent} from "./attribute-inputs/file-attribute-input-type.component";
import {DateTimeAttributeInputComponent} from "./attribute-inputs/datetime-attribute-input-type.component";
import {SelectAttributeInputComponent} from "./attribute-inputs/select-attribute-input-type.component";
import {TableAttributeInputComponent} from "./attribute-inputs/table-attribute-input-type-component";
import {Observable, Subject} from "rxjs";
import {TitleAttributeInputComponent} from "./attribute-inputs/title-attribute-input-type.component";
import {MultiDateTimeAttributeInputComponent} from "./attribute-inputs/multi-datetime-attribute-input-type.component";



@Component({
  selector: 'request-group-template',
  template: `<div><ng-template requestGroup></ng-template></div>
  `
})

export class RequestGroupTemplateComponent implements OnInit, OnDestroy {

  @Input()
  requestGroup: RequestGroup;

  @Input()
  oneControl: FormControl;

  @Input()
  requestId: number;

  @ViewChild(RequestGroupTemplateDirective, { static: true }) requestGroupElm!: RequestGroupTemplateDirective

  private inputFormControls: FormControl[] = [];

  private currentData: any = {};

  attrSubject: Subject<string> = new Subject<string>();

  @Input()
  attrObservable: Observable<string>;

  @Output()
  attrEmitter: EventEmitter<string> = new EventEmitter<string>();

  attrFactories: {[key:number]: ComponentFactory<any>};

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    this.attrFactories = {
      3: this.componentFactoryResolver.resolveComponentFactory(DateAttributeInputComponent),
      4: this.componentFactoryResolver.resolveComponentFactory(DateTimeAttributeInputComponent),
      5: this.componentFactoryResolver.resolveComponentFactory(FileAttributeInputTypeComponent),
      6: this.componentFactoryResolver.resolveComponentFactory(SelectAttributeInputComponent),
      7: this.componentFactoryResolver.resolveComponentFactory(SelectAttributeInputComponent),
      8: this.componentFactoryResolver.resolveComponentFactory(TableAttributeInputComponent),
      9: this.componentFactoryResolver.resolveComponentFactory(TitleAttributeInputComponent),
      10: this.componentFactoryResolver.resolveComponentFactory(MultiDateTimeAttributeInputComponent)
    };
  }

  ngOnDestroy(): void {

  }
  ngOnInit(): void {
    this.loadComponent();

    this.attrObservable?.subscribe((event: string) => {
      if (event == 'validate-data') {
        this.validateData();
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.requestGroup && this.requestGroup != null) {
      this.loadComponent();
    }

  }

  loadComponent() {
    if (this.requestGroup == null) {
      return;
    }
    let attrs = this.requestGroup.attrs.sort((a, b) => (a.order ?? Number.MIN_VALUE) - (b.order ?? Number.MIN_VALUE));
    const viewContainerRef = this.requestGroupElm.viewContainerRef;
    viewContainerRef.clear();
    this.inputFormControls = [];
    this.currentData = {};
    let controlValue = {};
    if (this.oneControl) {
      controlValue = this.oneControl.value || {};
    }

    attrs.forEach(attr => {
      let componentFactory = this.attrFactories[attr.attrType];
      if (componentFactory == null) {
        componentFactory = this.componentFactoryResolver.resolveComponentFactory(BasicAttributeInputComponent)
      }
      let inputControl = new FormControl;
      this.currentData[attr.fieldName] = this.oneControl.value ? this.oneControl.value[attr.fieldName] : null;
      inputControl.setValue(this.currentData[attr.fieldName] ? this.currentData[attr.fieldName].value : null);
      inputControl.valueChanges.subscribe((a) => this.handleDataChanged(attr.fieldName, attr, a));
      this.handleDataChanged(attr.fieldName, attr, null);
      if (attr.required) {
        inputControl.setValidators(Validators.required);
      }
      this.inputFormControls.push(inputControl);
      this.handleDataChanged(attr.fieldName, attr, null);
      const componentRef = viewContainerRef.createComponent<BaseAttributeInputComponent>(componentFactory)

      componentRef.instance.attribute = attr;
      componentRef.instance.oneControl = inputControl;
      componentRef.instance.parentId = this.requestId;
      if (controlValue[attr.fieldName] != null) {
        inputControl.setValue(controlValue[attr.fieldName].value);
      }
    });
    if (this.oneControl) {
      this.oneControl.setValue(this.currentData);
    }
  }

  handleDataChanged(attrName: string, attr: Attribute,value: any) {
    this.currentData[attrName] = {
      attr, value
    };
  //  console.log(this.currentData);
    if (this.oneControl) {
      this.oneControl.setValue(this.currentData);
    }
  }

  private validateData() {
    for (let i = 0; i < this.inputFormControls.length; i++) {
      if (this.inputFormControls[i].invalid) {
        this.inputFormControls[i].markAsTouched();
        this.attrEmitter.next("validate-data:invalid");
        return;
      }
    }
    this.attrEmitter.next("validate-data:valid");
  }
}
