import {Component, ElementRef, Input, OnInit, ViewChild} from "@angular/core";
import {BaseAttributeInputComponent} from "./base-attribute-input.component";
import {Attribute, FileInfo, FileService, RequestService} from "../../../../../../generated/web-api";
import {FormControl} from "@angular/forms";
import {Observer} from "rxjs";
import {environment} from "../../../../../../environments/environment";
import {FileInput} from "ngx-material-file-input";

@Component({
  templateUrl: './file-attribute-input-type.component.html',
  selector: 'one-file-control'
})

export class FileAttributeInputTypeComponent implements BaseAttributeInputComponent, OnInit {

  @Input()
  attribute: Attribute;

  @Input()
  oneControl: FormControl;

  @Input()
  parentId: number;

  formControl = new FormControl();

  fileName = '';

  uploading = false;

  @ViewChild('fileUpload', {static: false}) fileUpload: ElementRef;

  constructor(
    private fileService: FileService,
    private requestService: RequestService
  ) {
  }

  ngOnInit(): void {
    this.formControl.valueChanges.subscribe((fileInput: FileInput) => {
      this.fileSelected(fileInput);
    });
    if (this.oneControl != null && this.oneControl.value != null) {
      this.requestService.getAttachmentInfo(environment.tenantId, this.parentId, [this.oneControl.value]).toPromise().then((fileInfos: FileInfo[]) => {
        if (fileInfos.length > 0) {
          this.fileName = fileInfos[0].fileName;
        }
      });
    }
  }

  selectFile() {
    const fileInput = this.fileUpload.nativeElement;
    fileInput.click();
  }

  fileSelected(event): void {
    this.uploading = true;
    try {
      this.fileService.upload(environment.tenantId, event.files[0]).toPromise().then((fileInfo: FileInfo) => {
        this.fileName = fileInfo.fileName;
        this.oneControl.setValue(fileInfo.fileId);
      }).finally(() => {
        this.uploading = false;
      });
    } catch (ex) {
      this.uploading = false;
    }
  }




}
