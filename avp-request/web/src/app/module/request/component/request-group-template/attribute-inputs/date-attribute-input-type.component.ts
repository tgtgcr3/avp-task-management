import {BaseAttributeInputComponent} from "./base-attribute-input.component";
import {Component, Input, OnInit} from "@angular/core";
import {Attribute} from "../../../../../../generated/web-api";
import {FormControl, Validators} from "@angular/forms";

@Component({
  templateUrl: './date-attribute-input-type.component.html'
})

export class DateAttributeInputComponent implements BaseAttributeInputComponent, OnInit {

  @Input()
  attribute: Attribute;

  @Input()
  oneControl: FormControl;

  attrControl: FormControl = new FormControl();

  ngOnInit(): void {
    if (this.oneControl != null) {
      this.attrControl.valueChanges.subscribe((value: any) => {
        this.oneControl.setValue(value.getTime());
      });
      if (this.oneControl.value != null) {
        this.attrControl.setValue(new Date(this.oneControl.value));
      }
    }
    if (this.attribute != null) {
      if (this.attribute.required == true) {
        this.attrControl.setValidators(Validators.required);
      }
    }
  }

}
