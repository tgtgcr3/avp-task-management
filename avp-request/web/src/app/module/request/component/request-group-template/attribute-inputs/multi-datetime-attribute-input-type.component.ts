import {BaseAttributeInputComponent} from "./base-attribute-input.component";
import {Component, Input, OnInit} from "@angular/core";
import {Attribute} from "../../../../../../generated/web-api";
import {FormControl, Validators} from "@angular/forms";
import * as moment from 'moment';


@Component({
  templateUrl: './multi-datetime-attribute-input-type.component.html'
})

export class MultiDateTimeAttributeInputComponent implements BaseAttributeInputComponent, OnInit {

  @Input()
  attribute: Attribute;

  @Input()
  oneControl: FormControl;

  startDateControl: FormControl = new FormControl();

  endDateControl: FormControl = new FormControl();

  ngOnInit(): void {
    if (this.oneControl != null) {
      this.startDateControl.valueChanges.subscribe((value: any) => {
        if (value == null) {
          this.oneControl.setValue(null);
        } else if (this.endDateControl.value != null) {
          this.oneControl.setValue(`${value.set({second: 0, millisecond: 0})?.valueOf()}-${this.endDateControl.value.set({second: 0, millisecond: 0})?.valueOf()}`);
          if (value.valueOf() >= this.endDateControl.value.valueOf()) {
            this.oneControl.setErrors({key: 'abc'}, {emitEvent: true})
            this.startDateControl.setErrors({key: 'start_date_greater_than_end_date'});
            this.endDateControl.setErrors({key: 'end_date_less_than_start_date'});
          } else {
            this.oneControl.setErrors(null, {emitEvent: true});
          }
        }
      });
      this.endDateControl.valueChanges.subscribe((value: any) => {
        if (value == null) {
          this.oneControl.setValue(null);
        } else if (this.startDateControl.value != null) {
          this.oneControl.setValue(`${this.startDateControl.value.set({second: 0, millisecond: 0}).valueOf()}-${value?.set({second: 0, millisecond: 0})?.valueOf()}`);
          if (this.startDateControl.value.valueOf() >= value.valueOf()) {
            this.oneControl.setErrors({key: 'abc'}, {emitEvent: true});
            this.startDateControl.setErrors({key: 'start_date_greater_than_end_date'});
            this.endDateControl.setErrors({key: 'end_date_less_than_start_date'});
          } else {
            this.oneControl.setErrors(null, {emitEvent: true});
          }
        }
      });
      if (this.oneControl.value != null) {
        let values = this.oneControl.value.split('-');
        if (values.length > 0) {
          this.startDateControl.setValue(moment(parseInt(values[0])));
          if (values.length == 2) {
            this.endDateControl.setValue(moment(parseInt(values[1])));
          }
        }
        // this.attrControl.setValue(new Date(this.oneControl.value));
      }
    }
    if (this.attribute != null) {
      if (this.attribute.required == true) {
        this.startDateControl.setValidators(Validators.required);
        this.endDateControl.setValidators([Validators.required]);
      }
    }
  }
}
