import { FormControl } from '@angular/forms';
import { Component, Input, OnInit } from "@angular/core";
import { Attribute, AttributeType } from "../../../../../../generated/web-api";
import { BaseAttributeInputComponent } from "./base-attribute-input.component";

@Component({
  templateUrl: './title-attribute-input-type.component.html',
  styleUrls: ['./title-attribute-input-type.component.scss']
})

export class TitleAttributeInputComponent implements BaseAttributeInputComponent, OnInit {
  @Input()
  attribute: Attribute;

  @Input()
  oneControl: FormControl;

  constructor() {

  }
  ngOnInit(): void {
  }
}
