import { FormControl } from '@angular/forms';
import { Component, Input, OnInit } from "@angular/core";
import { Attribute, AttributeType } from "../../../../../../generated/web-api";
import { BaseAttributeInputComponent } from "./base-attribute-input.component";

@Component({
  templateUrl: './select-attribute-input-type.component.html'
})

export class SelectAttributeInputComponent implements BaseAttributeInputComponent, OnInit {
  @Input()
  attribute: Attribute;

  @Input()
  oneControl: FormControl;

  multiple = false;
  inputType: string;

  min = 0;
  max = Number.MAX_VALUE;

  ops: any[] = [];

  constructor() {

  }
  ngOnInit(): void {
    if (this.attribute.attrType == 7) {
      this.multiple = true;
    }

    if (this.attribute.additionData) {
      let config = JSON.parse(this.attribute.additionData);
      this.ops = config.values;
    }
  }
}
