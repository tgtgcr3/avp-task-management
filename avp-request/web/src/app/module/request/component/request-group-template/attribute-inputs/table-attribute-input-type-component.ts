import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ChangeDetectorRef, Component, Input, OnInit} from "@angular/core";
import { Attribute, AttributeType } from "../../../../../../generated/web-api";
import { BaseAttributeInputComponent } from "./base-attribute-input.component";
import * as transliterate from "@sindresorhus/transliterate";
import {Observable, Observer} from "rxjs";

@Component({
  templateUrl: './table-attribute-input-type.component.html',
  styleUrls: ['./table-attribute-input-type.component.scss']
})

export class TableAttributeInputComponent implements BaseAttributeInputComponent, OnInit {
  @Input()
  attribute: Attribute;

  @Input()
  oneControl: FormControl;

  columnOpts: ColumnOpt[] = [];

  columnNames: string[] = [];

  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private changeDetectionRef: ChangeDetectorRef
  ) {
    this.formGroup = this.formBuilder.group({
      tableData: this.formBuilder.array([])
    });

  }

  initData(): any[] {
    if (this.oneControl != null && this.oneControl.value) {
      let arr = this.formGroup.get('tableData') as FormArray;
      this.oneControl.value.forEach(oldData => arr.push(this.formBuilder.group(oldData)));
    }
    return [];
  }

  get tableData() {
    return this.formGroup.controls.tableData;
  }

  ngOnInit(): void {

    if (this.attribute.additionData) {
      this.columnNames.push('oneindex');
      let config = JSON.parse(this.attribute.additionData);
      this.columnOpts = config.values.map(v => {

        let colName = transliterate(v.name).replace(/\s/gi, '_');
        this.columnNames.push(colName);
        return {
          id: v.id,
          name: colName,
          friendlyName: v.name
        }
      });
      this.columnNames.push('btn');
    }


    this.formGroup.valueChanges.subscribe((data:any) => {
      this.oneControl?.setValue(this.formGroup.value.tableData);
    });
    this.initData();
  }

  ngAfterViewChecked():void {
    setTimeout(() => {
      this.changeDetectionRef.detach();
    });
  }
  addRow() {
    let newData:any = {};
    this.columnOpts.forEach((colOpt: ColumnOpt) => {
      newData[colOpt.name] = '';
    });
    let arr = this.formGroup.get('tableData') as FormArray;
    arr.push(this.formBuilder.group(newData));
    this.changeDetectionRef.detectChanges();
  }

  removeRow(index: number): void {
    let arr = this.formGroup.get('tableData') as FormArray;
    arr.removeAt(index);
    this.changeDetectionRef.detectChanges();
  }
}

interface ColumnOpt {
  id?: number,
  name?: string,
  friendlyName?: string
}
