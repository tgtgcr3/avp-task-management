import { FormControl } from '@angular/forms';
import { Attribute } from '../../../../../../generated/web-api';
import {Observable, Observer} from "rxjs";

export interface BaseAttributeInputComponent {
  attribute: Attribute;
  oneControl: FormControl;
  // attrEvent?: Observable<any>;
  parentId?: number;
}
