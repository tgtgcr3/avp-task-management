import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmDialog} from 'src/app/core/component/confirm-dialog/confirm-dialog.component';
import * as cst from "src/app/core/common/one-const";
import {Discussion, DiscussionStatus} from "../../../../../../../../generated/web-api";

@Component({
  templateUrl: './discussion.component.html',
  selector: 'discussion',
  styleUrls: ['./discussion.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DiscussionComponent implements OnInit {

  @Input() discussion: any;
  @Input() isAddNew: boolean = false;
  @Input() currentUser: any;
  @Input() users: any[];
  @Output() event: EventEmitter<any> = new EventEmitter<any>();

  isTexting: boolean = false;
  isEdit: boolean = false;
  discussionControl: FormControl;
  isOwner: boolean = false;

  readOnly = false;

  formattedContent = "";

  constructor(private dialog: MatDialog) {
  }

  ngOnInit() {
    this.discussionControl = new FormControl(this.discussion.content);
    this.readOnly = this.discussion.status == DiscussionStatus.ReadOnly
    this.isOwner = this.discussion.owner == this.currentUser.username && !this.readOnly;
    if (this.readOnly) {
      this.formattedContent = this.discussion.content;
      let usernames = this.formattedContent.match(/\[\w+\]/g);
      for (let i = 0; i < usernames.length; i++) {
        let original = usernames[i];
        let username = original.substr(1, original.length - 2);
        let user = this.users.find(u => u.username == username);
        if (user) {
          this.formattedContent = this.formattedContent.replace(original, `<span class="user-details">${user.fullName} @${user.username}</span> (${user.title})`);
        } else {
          this.formattedContent = this.formattedContent.replace(original, username);
        }
      }

    }
  }

  onAddDiscussion() {
    this.isTexting = true;
  }

  onCancelDiscussion() {
    this.isTexting = false;
    this.isEdit = false;
    this.discussionControl.setValue(this.discussion.content);
  }

  onEdit() {
    this.isEdit = true;
    this.isTexting = true;
  }

  onDelete() {
    const dialogRef = this.dialog.open(ConfirmDialog,
      {
        data: 'Bạn có chắc mình muốn xóa bình luận này?',
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.status == cst.SAVE_STATUS.OK) {
        let payload = {
          status: 'delete',
          id: this.discussion.id
        }
        this.event.emit(payload);
      }
    });
  }

  onPost() {
    if (!this.discussionControl.value) {
      return;
    }

    let status = this.isEdit ? 'update' : 'create';
    let payload = {
      status: status,
      discussion: {
        id: this.discussion.id,
        content: this.discussionControl.value
      }
    };
    this.onCancelDiscussion();

    this.event.emit(payload);
  }
}
