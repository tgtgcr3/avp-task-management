import { Component, Input, OnInit } from '@angular/core';
import { BaseRequestAttributeComponent } from './request-attribute.component';

@Component({
  template:
    `
    <mat-list-item>
      <div class="index m-2">[{{order}}]</div>
      <div class="fieldName m-2">{{attribute.attr.name}}:</div>
      <div class="value m-2">
        <li *ngFor="let item of items">{{item.name}}</li>
      </div>
    </mat-list-item>
    `,
    styles: ['.value {font-size: 15px; word-break: break-word;}', '.index {width: 25px;}', '.fieldName {min-width: 150px}']
})
export class RequestAttributeSelectComponent implements BaseRequestAttributeComponent, OnInit {

  @Input() requestId: number;

  @Input()
  order: number;

  @Input()
  attribute: any;

  items: any[];

  ngOnInit() {
    this.items = this.attribute.attr.attrType == 6 ? 
      this.extractSelectionValue([this.attribute.value], this.attribute.attr.additionData) : 
      this.extractSelectionValue(this.attribute.value, this.attribute.attr.additionData);
  }

  private extractSelectionValue(selected: number[], additionData: string) : any[] {
    let optionData = JSON.parse(additionData);
    let selectedItems = optionData.values.filter(item => {
      return selected.includes(item.value);
    });
    return selectedItems;
  }
}