import { Component, Input, OnInit } from '@angular/core';
import { BaseRequestAttributeComponent } from './request-attribute.component';

@Component({
  template:
    `
    <mat-list-item style="height: auto">
      <div class="index m-2">[{{order}}]</div>
      <div class="fieldName m-2">{{attribute.attr.name}}:</div>
      <div class="value m-2" *ngIf="isMultiline == false">{{text}}</div>
      <div class="value m-2" *ngIf="isMultiline == true" style="pointer-events: none;"><p [innerHTML]="text"></p></div>
    </mat-list-item>
    `,
  styles: ['.value {font-size: 15px; word-break: break-word;}', '.index {width: 25px;}', '.fieldName {min-width: 150px}']
})
export class RequestAttributeBaseComponent implements BaseRequestAttributeComponent, OnInit {

  @Input() requestId: number;

  @Input()
  order: number;

  @Input()
  attribute: any;

  text = '';

  isMultiline = false;

  ngOnInit() {
    if (this.attribute != null && this.attribute.attr != null) {
      if (this.attribute.attr.attrType == 2) {
        if (this.attribute.value != null) {
          this.text = this.attribute.value.replace(/\n/g, '<br/>');
        }
        this.isMultiline = true;
      }
      else {
        this.text = this.attribute.value;
      }
    } 
  }
}
