import { formatDate } from '@angular/common';
import {Component, Input, OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { OneAlertService } from 'src/app/core/service/one-alert.service';
import { environment } from 'src/environments/environment';
import { Discussion, DiscussionGetAll, DiscussionService, OneRequest } from 'src/generated/web-api';

@Component({
    templateUrl: './discussions.component.html',
    selector: 'discussions'
})
export class DiscussionsComponent implements OnInit {

    @Input() request: OneRequest;
    @Input() users: any[] = [];

    discussions: OneDiscussion[] = [];
    newDiscussion: OneDiscussion;
    currentUser: any;
    pageInfo: any = {
        total: 0,
        pageSize: 10,
        options: [10, 25, 50, 100],
        pageIndex: 0
    };
    
    constructor(private discussionService: DiscussionService,
        private keyClockService: KeyCloakService,
        private oneAlert: OneAlertService) { }

    ngOnInit() {
        this.onLoadDiscussions();
        this.keyClockService.getUserInfo().then(
            (user: any) => {
                this.currentUser = this.findUser(user.preferred_username);
                this.newDiscussion = {
                    content: "",
                    createUser: this.currentUser
                }
            }
        )
    }

    onEvent(event: any) {
        let request: Promise<any>;
        if(event.status == 'create') {
            let payload: Discussion = {
                content: event.discussion.content
            };

            request = this.discussionService.postDiscussion(environment.tenantId, environment.discussionTarget, this.request.id, payload).toPromise();
        }
        else if(event.status == 'update') {
            let payload: Discussion = {
                content: event.discussion.content
            };

            request = this.discussionService.updateDiscussion(environment.tenantId, environment.discussionTarget, this.request.id, event.discussion.id, payload).toPromise();
        }
        else if(event.status == 'delete') {
            request = this.discussionService.deleteDiscussion(environment.tenantId, environment.discussionTarget, this.request.id, event.id).toPromise();
        }
        
        request
        .then((response: Discussion) => {
            this.onLoadDiscussions();
        })
        .catch((error: any) => {
            this.oneAlert.onError(error);
        });
    }

    onPageChanged(event) {
        this.pageInfo.pageIndex = event.pageIndex;
        this.pageInfo.pageSize = event.pageSize;
        this.onLoadDiscussions();
    }

    private onLoadDiscussions() {
        this.discussionService.getAllDiscussions(
            environment.tenantId, environment.discussionTarget, this.request.id, this.pageInfo.pageIndex, this.pageInfo.pageSize).toPromise()
            .then((response: DiscussionGetAll) => {
                this.discussions = response.items.filter(discussion => {
                    return discussion.status != 'Deleted';
                }).map(discussion => {
                    return {
                        ...discussion,
                        createUser: this.findUser(discussion.owner),
                        postedDate: formatDate(new Date(discussion.lastUpdatedDate), 'dd/MM/yyyy', 'en-US')
                    }
                });
                let deletedDiscussions = response.items.filter(discussion => {
                    return discussion.status == 'Deleted';
                });
                this.pageInfo.total = response.total - deletedDiscussions.length;
            });
    }

    private findUser(username: string) {
        return this.users.find(user => {
            return user.username == username;
        });
    }
}

export interface OneDiscussion extends Discussion {
    createUser?: any;
    postedDate?: string;
}