import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
  selector: '[requestAttribute]'
})

export class RequestAttributeTemplateDirective {
  constructor(public viewContainerRef: ViewContainerRef) {

  }
}
