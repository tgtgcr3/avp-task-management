import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {RequestStatusBody} from 'src/generated/web-api';
import * as cst from 'src/app/core/common/one-const';
import {FormControl} from "@angular/forms";

@Component({
  templateUrl: './status-confirm-dialog.component.html',
  styleUrls: ['./status-confirm-dialog.component.scss'],
  selector: 'status-confirm-dialog'
})
export class StatusConfirmDialog {

  request: any;
  description: string;

  forward = false;

  targetUserFormControl: FormControl = new FormControl();
  users: any[];

  hasError = false;

  get title() {
    return this.statusText[this.status];
  }

  get color() {
    return this.status === RequestStatusBody.StatusEnum.APPROVED ? 'primary' : 'warn';
  }

  private status: RequestStatusBody.StatusEnum;

  private statusText: any = {
    APPROVED: 'Đồng ý',
    REJECTED: 'Từ chối',
    FORWARD: 'Chuyển tiếp'
  }

  constructor(
    private dialogRef: MatDialogRef<StatusConfirmDialog>,
    @Inject(MAT_DIALOG_DATA) private dialogData: any
  ) {
    this.status = dialogData.status;
    this.request = dialogData.request;
    this.forward = dialogData.forward;
    this.users = dialogData.users;
  }

  onSend() {
    this.dialogRef.close({
      status: cst.SAVE_STATUS.OK,
      data: {
        description: this.description
      }
    });
  }

  onAgreeFwd(agreed: boolean) {
    if (this.targetUserFormControl.value == null || this.targetUserFormControl.value.length == 0) {
      this.hasError = true;
      return;
    }
    this.dialogRef.close({
      status: cst.SAVE_STATUS.OK,
      data: {
        selectedUsers: this.targetUserFormControl.value,
        agreed: agreed,
        description: this.description
      }
    });
  }
}
