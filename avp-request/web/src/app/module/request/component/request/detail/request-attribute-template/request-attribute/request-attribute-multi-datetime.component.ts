import { formatDate } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { BaseRequestAttributeComponent } from './request-attribute.component';

@Component({
  template:
    `
    <mat-list-item>
      <div class="index m-2">[{{order}}]</div>
      <div class="fieldName m-2">{{attribute.attr.name}}:</div>
      <div class="value m-2">{{dateText}}</div>
    </mat-list-item>
    `,
  styles: ['.value {font-size: 15px; word-break: break-word;}', '.index {width: 25px;}', '.fieldName {min-width: 150px}']
})
export class RequestAttributeMultiDatetimeComponent implements BaseRequestAttributeComponent, OnInit {

  @Input() requestId: number;

  @Input()
  order: number;

  @Input()
  attribute: any;

  dateText: string;

  ngOnInit() {
    let format = 'HH:mm dd/MM/yyyy';

    if (this.attribute.value != null) {
      let values = this.attribute.value.split('-');
      let startDate: string = '';
      let endDate: string = '';
      if (values.length > 0) {
        startDate = formatDate(new Date(parseInt(values[0])), format, 'en-US');
        if (values.length == 2) {
          endDate = formatDate(new Date(parseInt(values[1])), format, 'en-US');
        }
      }
      try {
        this.dateText = `${startDate} - ${endDate}`;
      } catch (ex) {
      }

    }

  }
}
