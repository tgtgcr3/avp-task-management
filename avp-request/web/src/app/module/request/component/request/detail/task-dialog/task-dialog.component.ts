import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import { formatDate } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject, Subject } from 'rxjs';
import * as transliterate from '@sindresorhus/transliterate';

@Component({
    templateUrl: './task-dialog.component.html',
    styleUrls: ['./task-dialog.component.scss'],
    selector: 'task-dialog'
})
export class TaskDialog implements OnInit, OnDestroy {

    taskFormGroup: FormGroup;
    assigneeFilter: FormControl = new FormControl();
    protected _onDestroy = new Subject<void>();

    users: any[] = [];
    projects: any[] = [];
    categories: any[] = [];

    filteredUsers: ReplaySubject<any[]> = new ReplaySubject<any[]>(1)

    constructor(
        private formBuilder: FormBuilder,
        private dialogRef: MatDialogRef<TaskDialog>,
        @Inject(MAT_DIALOG_DATA) private dialogData: any) 
    {
        this.projects = dialogData.projects;
        this.users = dialogData.users.sort((a, b) => (a.firstName > b.firstName) ? 1 : -1 );
        
        this.taskFormGroup = this.formBuilder.group({
            name: ['', Validators.required],
            project: [this.projects[0], Validators.required],
            category: [null],
            assignee: [null],
            followers: [null],
            startDate: [null],
            dueDate: [null]
        });

        this.assigneeFilter.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
            this.filterAssignees();
          });
    }

    ngOnInit() {
        this.filterAssignees();
        this.taskFormGroup.controls.project.valueChanges.subscribe((project: any) => {
            this.categories = project.categories;
        });
    }

    ngOnDestroy(): void {
        this._onDestroy.next();
        this._onDestroy.complete();
      }

    onCreate() {
        if (!this.taskFormGroup.invalid) {
            let payload = {
                name: this.taskFormGroup.controls.name.value,
                project_id: this.taskFormGroup.controls.project.value.id,
                category_id: this.taskFormGroup.controls.category.value ? this.taskFormGroup.controls.category.value.id : null,
                startDate: this.formatDate(this.taskFormGroup.controls.startDate.value),
                dueDate: this.formatDate(this.taskFormGroup.controls.dueDate.value),
                followers: this.taskFormGroup.controls.followers.value ? this.taskFormGroup.controls.followers.value.map(username => {
                    return this.users.find(user => user.username == username).email
                }) : null,
                assignee_email: this.taskFormGroup.controls.assignee.value ? this.taskFormGroup.controls.assignee.value.email : null
            }

            this.dialogRef.close(payload);
        }
    }

    private filterAssignees() {
        if (!this.users) {
            return;
        }

        let search = this.assigneeFilter.value;
        if (!search) {
            this.filteredUsers.next(this.users.slice());
            return;
        } else {
            search = transliterate(search).toLowerCase();
        }
        this.filteredUsers.next(this.users.filter(ut => transliterate(ut.fullName).toLowerCase().indexOf(search) >= 0));
    }

    private formatDate(date: any) {
        if(date != null) {
            return formatDate(date, 'yyyy-MM-dd', 'en-US');
        }
        return null;
    }
}