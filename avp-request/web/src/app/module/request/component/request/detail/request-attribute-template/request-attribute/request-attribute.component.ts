export interface BaseRequestAttributeComponent {
  attribute: any;
  order: number;
  requestId: number;
}