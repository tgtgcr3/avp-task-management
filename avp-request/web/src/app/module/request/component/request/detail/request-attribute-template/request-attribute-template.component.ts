import {Component, ComponentFactoryResolver, Input, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { RequestAttributeTemplateDirective } from './request-attribute-template.directive';
import { RequestAttributeBaseComponent } from './request-attribute/request-attribute-base.component';
import { RequestAttributeDateComponent } from './request-attribute/request-attribute-date.component';
import { RequestAttributeFileComponent } from './request-attribute/request-attribute-file.component';
import { RequestAttributeSelectComponent } from './request-attribute/request-attribute-select.component';
import { BaseRequestAttributeComponent } from './request-attribute/request-attribute.component';
import {RequestAttributeTableComponent} from "./request-attribute/request-attribute-table.component";
import {RequestAttributeTitleComponent} from "./request-attribute/request-attribute-title.component";
import {RequestAttributeMultiDatetimeComponent} from "./request-attribute/request-attribute-multi-datetime.component";

@Component({
    template: '<div><ng-template requestAttribute></ng-template></div>',
    selector: 'attribute-template'
})
export class RequestAttributeTemplateComponent implements OnInit, OnDestroy {

    @ViewChild(RequestAttributeTemplateDirective, {static: true}) attributesEl!: RequestAttributeTemplateDirective;

    @Input()
    request: any;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    }

    ngOnInit() {

    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.request && this.request.attrs != null) {
          this.loadComponent();
        }

      }

    ngOnDestroy() {

    }

    loadComponent() {
        if (this.request.attrs == null) {
          return;
        }
        let attrs = this.request.attrs.sort((a, b) => (a.attr.order ?? Number.MIN_VALUE) - (b.attr.order ?? Number.MIN_VALUE));
        const viewContainerRef = this.attributesEl.viewContainerRef;
        viewContainerRef.clear();
        let count = 1;
        for (let index = 0; index < attrs.length; index ++){
          let item = attrs[index];
          let componentFactory = null;
          if (item.attr.attrType == 3 || item.attr.attrType == 4) {
            componentFactory = this.componentFactoryResolver.resolveComponentFactory(RequestAttributeDateComponent)
          }  else if (item.attr.attrType == 5) {
            componentFactory = this.componentFactoryResolver.resolveComponentFactory(RequestAttributeFileComponent);
          } else if (item.attr.attrType == 6 || item.attr.attrType == 7) {
            componentFactory = this.componentFactoryResolver.resolveComponentFactory(RequestAttributeSelectComponent)
          }else if (item.attr.attrType == 8) {
            componentFactory = this.componentFactoryResolver.resolveComponentFactory(RequestAttributeTableComponent);
          }else if (item.attr.attrType == 9) {
            componentFactory = this.componentFactoryResolver.resolveComponentFactory(RequestAttributeTitleComponent);
          } else if (item.attr.attrType == 10) {
            componentFactory = this.componentFactoryResolver.resolveComponentFactory(RequestAttributeMultiDatetimeComponent);
          } else {
            componentFactory = this.componentFactoryResolver.resolveComponentFactory(RequestAttributeBaseComponent)
          }
          const componentRef = viewContainerRef.createComponent<BaseRequestAttributeComponent>(componentFactory)

          componentRef.instance.requestId = this.request.id;
          componentRef.instance.attribute = item;
          if (item.attr.attrType != 9) {
            componentRef.instance.order = count++;
          }

        }
      }
}
