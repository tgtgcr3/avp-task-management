import {BaseRequestAttributeComponent} from "./request-attribute.component";
import {Component, Input, OnInit} from "@angular/core";
import * as transliterate from "@sindresorhus/transliterate";

@Component({
  templateUrl: './request-attribute-table.component.html',
  styleUrls: ['./request-attribute-table.component.scss']
})

export class RequestAttributeTableComponent implements BaseRequestAttributeComponent, OnInit {

  @Input()
  requestId: number;

  @Input()
  order: number;

  @Input()
  attribute: any;

  columnNames: string[] = [];

  columnOpts: ColumnOpt[] = [];

  attrTableSource: any[] = [];

  ngOnInit() {
    console.log(this.attribute);
    if (this.attribute && this.attribute.attr && this.attribute.attr.additionData) {
      let additionalData = JSON.parse(this.attribute.attr.additionData);
      this.columnNames.push('oneindex');
      this.columnOpts = additionalData.values.map(opts => {
        let colName = transliterate(opts.name).replace(/\s/gi, '_');
        this.columnNames.push(colName);
        return {
          name: colName,
          friendlyName: opts.name
        }
      });

      if (this.attribute.value && this.attribute.value.length > 0) {
        this.attrTableSource = this.attribute.value;
      }
    }
  }
}

interface ColumnOpt {
  name?: string,
  friendlyName?: string
}
