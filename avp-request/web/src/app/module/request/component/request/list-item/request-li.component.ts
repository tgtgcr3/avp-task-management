import {Component, Input} from '@angular/core';
import { OneRouteService } from 'src/app/core/service/one-route.service';

@Component({
    templateUrl: './request-li.component.html',
    selector: 'request-li'
})
export class RequestLiComponent {

    @Input() id: string;

    constructor(private oneRoute: OneRouteService) { }

    public get isInDetail() {
        return this.oneRoute.params['id'] == this.id;
    }
}