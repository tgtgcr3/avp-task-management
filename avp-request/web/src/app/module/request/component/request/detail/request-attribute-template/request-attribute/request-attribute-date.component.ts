import { formatDate } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { BaseRequestAttributeComponent } from './request-attribute.component';

@Component({
  template:
    `
    <mat-list-item>
      <div class="index m-2">[{{order}}]</div>
      <div class="fieldName m-2">{{attribute.attr.name}}:</div>
      <div class="value m-2">{{dateText}}</div>
    </mat-list-item>
    `,
  styles: ['.value {font-size: 15px; word-break: break-word;}', '.index {width: 25px;}', '.fieldName {min-width: 150px}']
})
export class RequestAttributeDateComponent implements BaseRequestAttributeComponent, OnInit {

  @Input() requestId: number;

  @Input()
  order: number;

  @Input()
  attribute: any;

  dateText: string;

  ngOnInit() {
    let format = 'dd/MM/yyyy';
    if (this.attribute.attr.attrType == 4) {
      format = 'HH:mm dd/MM/yyyy';
    }

    if (this.attribute.value != null) {
      try {
        this.dateText = formatDate(new Date(this.attribute.value), format, 'en-US');
      } catch (ex) {
      }

    }

  }
}
