import { formatDate } from '@angular/common';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserMenuComponent } from 'src/app/core/component/user-menu/user-menu.component';
import { environment } from 'src/environments/environment';
import { OneRequest, RequestActivity, RequestService, RequestStatusBody } from 'src/generated/web-api';

@Component({
    templateUrl: './request-detail-sidebar.component.html',
    styleUrls: ['./request-detail-sidebar.component.scss'],
    selector: 'request-detail-sidebar'
})
export class RequestDetailSidebarComponent implements OnInit {
    @Input() request: OneRequest = {};
    @Input() fixedFollowers: string[] = [];
    @Input() users: any[] = [];
    @Output() removeFollower: EventEmitter<string> = new EventEmitter();
    @Output() addFollower: EventEmitter<string> = new EventEmitter();

    approvers: any[] = [];
    followers: any[] = [];
    activities: OneRequestActivity[] = [];

    constructor(private requestService: RequestService,
        private dialog: MatDialog) { }

    ngOnInit() {
        this.loadUsers();
        let createActivity = {
            action: RequestActivity.ActionEnum.Created,
            createdDate: {
                text: formatDate(new Date(this.request.submitTime), 'dd/MM/yyyy HH:mm', 'en-US'),
                date: formatDate(new Date(this.request.submitTime), 'dd/MM/yyyy', 'en-US'),
                time: formatDate(new Date(this.request.submitTime), 'HH:mm', 'en-US')
            },
            createdUser: this.users.find(user => user.username == this.request.submitter),
            statusIcon: this.getStatusIcon(RequestActivity.ActionEnum.Created),
            actionText: this.getActionText(RequestActivity.ActionEnum.Created)
        };
        this.activities.push(createActivity);
        this.requestService.getActivityLogs(environment.tenantId, this.request.id).toPromise()
        .then((responses: RequestActivity[]) => {
            this.activities = this.activities.concat(responses.map(activity => {
                let date = activity.date > 0 ? new Date(activity.date) : Date.now();
                return {
                    ...activity,
                    createdUser: this.users.find(user => user.username == activity.owner),
                    createdDate: {
                        text: formatDate(date, 'dd/MM/yyyy HH:mm', 'en-US'),
                        date: formatDate(date, 'dd/MM/yyyy', 'en-US'),
                        time: formatDate(date, 'HH:mm', 'en-US')
                    },
                    statusIcon: this.getStatusIcon(activity.action),
                    actionText: this.getActionText(activity.action)
                }
            }).sort((a, b) => a.date - b.date));
        });
    }

    onRemoveFollower(user: any) {
        this.removeFollower.emit(user.username);
    }

    onAddFollower() {
        const dialogRef = this.dialog.open(UserMenuComponent,
            {
                data: {
                    users: this.users,
                    selectedUsers: this.followers,
                    title: 'Thêm người theo dõi'
                },
                width: '350px',
                position: {
                    top: '65px'
                },
                hasBackdrop: true,
                disableClose: true
            });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (result.status == 'added') {
                this.addFollower.emit(result.user.username);
            }
        })
    }

    private getDecisionStatus(username: string): RequestStatusBody.StatusEnum {
      if((this.request.approveds || []).includes(username)) {
        return RequestStatusBody.StatusEnum.APPROVED;
      }
      else if((this.request.rejecteds || []).includes(username)) {
        return RequestStatusBody.StatusEnum.REJECTED;
      }
      return null;
    }
    private loadUsers() {
        if(this.request.approvers) {
          let directMgmts = this.request.directMgmts || [];

          let approverList = directMgmts.concat((this.request.approvers || []).filter(u => directMgmts.indexOf(u) < 0));

          this.approvers = approverList.map(username => {
            let user = this.users.find(u => u.username == username);
            if (user) {
              return {
                ...user,
                approvalStatus: this.getDecisionStatus(username)
              };
            } else {
              return {
                avatarUrl: environment.defaultAvatar,
                fullName: "Không Xác Định"
              }
            }
          })       ;
        }

        if(this.request.followers) {
            this.followers = this.users.filter((user: any) => {
                return this.request.followers.includes(user.username);
            }).map((user: any) => {
                let removable = this.fixedFollowers != null && !this.fixedFollowers.includes(user.username);
                return {
                    ...user,
                    removable: removable
                }
            });
        }
    }

    private getStatusIcon(status: RequestActivity.ActionEnum) {
        switch(status) {
            case RequestActivity.ActionEnum.Created:
                return 'add_circle';
            case RequestActivity.ActionEnum.Approved:
                return 'check_circle';
            case RequestActivity.ActionEnum.Cancelled:
                return 'remove_circle';
            case RequestActivity.ActionEnum.Moved:
                return 'keyboard_tab';
            case RequestActivity.ActionEnum.Rejected:
                return 'cancel';
            case RequestActivity.ActionEnum.Updated:
                return 'layers';
          case RequestActivity.ActionEnum.Forward:
          case RequestActivity.ActionEnum.AgreedForward:
            return 'keyboard_tab';
        }
    }

    private getActionText(status: RequestActivity.ActionEnum) {
        switch(status) {
            case RequestActivity.ActionEnum.Created:
                return 'tạo đề xuất';
            case RequestActivity.ActionEnum.Approved:
                return 'chấp thuận đề xuất';
            case RequestActivity.ActionEnum.Cancelled:
                return 'hủy đề xuất';
            case RequestActivity.ActionEnum.Moved:
                return 'chuyển tiếp đề xuất';
            case RequestActivity.ActionEnum.Rejected:
                return 'từ chối đề xuất';
            case RequestActivity.ActionEnum.Updated:
                return 'cập nhật đề xuất';
          case RequestActivity.ActionEnum.Forward:
          case RequestActivity.ActionEnum.AgreedForward:
            return 'chuyển tiếp đề xuất'
        }
    }

}

interface OneRequestActivity extends RequestActivity {
    createdDate: any;
    createdUser: any;
    statusIcon: string;
    actionText: string;
}
