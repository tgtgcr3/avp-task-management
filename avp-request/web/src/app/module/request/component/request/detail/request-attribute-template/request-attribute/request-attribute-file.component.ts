import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FileInfo, RequestService } from 'src/generated/web-api';
import { BaseRequestAttributeComponent } from './request-attribute.component';
import * as FileSaver from 'file-saver';

@Component({
  template:
    `
    <mat-list-item>
      <div class="index m-2">[{{order}}]</div>
      <div class="fieldName m-2">{{attribute.attr.name}}:</div>
      <div class="value m-2" (click)="onDownload()" *ngIf="fileInfo.fileId">
        {{fileInfo.fileName}} (Tải về)
      </div>
    </mat-list-item>
    `,
  styles: ['.value {font-size: 15px; cursor: pointer;}', '.value:hover { color: #267cde }', '.index {width: 25px;}', '.fieldName {min-width: 150px}']
})
export class RequestAttributeFileComponent implements BaseRequestAttributeComponent, OnInit {

  @Input() requestId: number;

  @Input()
  order: number;

  @Input()
  attribute: any;

  fileInfo: FileInfo = {};

  constructor(private requestService: RequestService) { }

  ngOnInit() {
    this.loadFileInfo()
  }

  onDownload() {
    this.requestService.getAttachment(environment.tenantId, this.requestId, this.attribute.value).toPromise()
      .then((response: Blob) => {
        FileSaver.saveAs(response, this.fileInfo.fileName);
      })
  }

  private loadFileInfo() {
    this.requestService.getAttachmentInfo(environment.tenantId, this.requestId, [this.attribute.value]).toPromise()
      .then((responses: FileInfo[]) => {
        if(responses.length > 0) {
          this.fileInfo = responses[0];
        }
        else {
          this.fileInfo = {
            fileName: 'Unknown',
            fileId: null
          }
        }
      })
  }
}