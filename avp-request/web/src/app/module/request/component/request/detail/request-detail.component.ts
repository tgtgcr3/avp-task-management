import {Component, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { BooleanResponse, FileInfo, OneRequest, RequestApprovalType, RequestGroup, RequestGroupForward, RequestGroupForwardService, RequestGroupService, RequestService, RequestStatusBody } from "../../../../../../generated/web-api";
import { environment } from "../../../../../../environments/environment";
import { KeyCloakUserService } from "../../../../../../generated/account-api";
import { forkJoin, Subscription } from "rxjs";
import { KeyCloakService } from "../../../../../core/service/keycloak.service";
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialog } from 'src/app/core/component/confirm-dialog/confirm-dialog.component';
import * as cst from 'src/app/core/common/one-const';
import { RequestFormComponent } from '../../create-form/request-form-dialog.component';
import * as FileSaver from 'file-saver';
import { OneAlertService } from 'src/app/core/service/one-alert.service';
import { StatusConfirmDialog } from './status-confirm/status-confirm-dialog.component';
import {DatePipe, formatDate} from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import {HttpErrorResponse} from "@angular/common/http";
import {UserData} from "../../../../../../generated/keycloak-api";
import { RequestApprovalTypeWrapperService } from 'src/app/core/service/request-approval-type.service';
import { TaskDialog } from './task-dialog/task-dialog.component';
import { OneTaskService } from 'src/app/core/service/one-task.service';
import { GroupSelectDialog } from 'src/app/core/component/group-select/group-select-dialog.component';
import {SecurityService} from "../../../../../core/service/security.service";
import { OneRouteService } from 'src/app/core/service/one-route.service';

@Component({
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.scss'],
  selector: 'request-detail'
})

export class RequestDetailComponent implements OnInit, OnDestroy {

  private sub: any;
  private refreshSub: Subscription;

  isLoading: boolean = false;
  isOwner: boolean = false;

  currentId: number;

  request: _MyOneRequest;

  showDecisionActions: boolean = true;
  myApproval?: any;

  users: any[] = [];
  currentUser: any;

  dueDate: any;
  requestGroup: _MyOneRequestGroup;
  remainingTime: any;
  canEdit: boolean = false;

  attachments: FileInfo[] = [];
  workPackages: any[] = [];

  private approvalTypes: RequestApprovalType[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private oneAlert: OneAlertService,
    private requestService: RequestService,
    private keycloakUserService: KeyCloakUserService,
    private keycloakService: KeyCloakService,
    private requestGroupService: RequestGroupService,
    private dialog: MatDialog,
    private datePipe: DatePipe,
    private translateService: TranslateService,
    private requestApprovalTypeWrapperService: RequestApprovalTypeWrapperService,
    private taskAPIService: OneTaskService,
    private forwardRequestGroupService: RequestGroupForwardService,
    private oneRoute: OneRouteService

  ) {
  }


  ngOnInit(): void {
    //hacking

    this.refreshSub = this.oneRoute.refreshComponentEvt.subscribe(() => this.reloadData(this.currentId));

    this.sub = this.route.params.subscribe(params => {
      this.currentId = params['id'];
      this.reloadData(this.currentId);
    });

    this.keycloakService.getUserInfo().then((userInfo: any) => {
      this.currentUser = userInfo;
      this.checkAbleToSetStatus();
      this.isOwner = this.keycloakService.isOwner();
    });
  }

  ngOnDestroy () {
    if(this.refreshSub) {
      this.refreshSub.unsubscribe();
      this.refreshSub = null;
    }
  }

  onClone() {
    const dialogRef = this.dialog.open(RequestFormComponent, {
      width: '1200px',
      position: {
        top: '65px'
      },
      hasBackdrop: true,
      disableClose: true,
      data: {
        requestGroups: [this.requestGroup],
        users: [...this.users],
        isClone: true,
        request: { ...this.request }
      }
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if(result) {
        this.oneRoute.navigateTo(['/requests'], this.oneRoute.queryParams).then((value) => {
          window.location.reload();
        });
      }
    });
  }

  onForward() {
    this.forwardRequestGroupService.getAllForwardGroups(environment.tenantId, this.requestGroup.oId).toPromise()
    .then((responses: RequestGroupForward[]) => {
      let groups = responses.filter((group: RequestGroupForward) => {
        if (this.request.status == OneRequest.StatusEnum.PENDING) {
          return group.status == RequestGroupForward.StatusEnum.Pending;
        }
        else if (this.request.status == OneRequest.StatusEnum.APPROVED) {
          return group.status == RequestGroupForward.StatusEnum.Approved ||
            group.status == RequestGroupForward.StatusEnum.Done;
        }
        else if (this.request.status == OneRequest.StatusEnum.REJECTED) {
          return group.status == RequestGroupForward.StatusEnum.Rejected ||
            group.status == RequestGroupForward.StatusEnum.Done;
        }
      }).map(group => {
        return {
          ...group,
          name: group.targetName
        };
      });

      const dialogRef = this.dialog.open(GroupSelectDialog,
        {
          data: {
            groups: groups
          },
          width: '500px',
          position: {
            top: '65px'
          },
          hasBackdrop: true,
          disableClose: true
        });

      dialogRef.afterClosed().subscribe((result: RequestGroupForward) => {
        if(result) {
          this.forwardRequestGroupService.transferRequest(environment.tenantId, this.request.id, result.targetId).toPromise()
          .then((response: any) => {
            window.location.reload();
          })
          .catch(error => this.oneAlert.onError(error));
        }
      });
    })

  }

  onCancel() {
    const dialogRef = this.dialog.open(ConfirmDialog,
      {
        data: 'Bạn có chắc mình muốn hủy đề xuất này?',
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.status == cst.SAVE_STATUS.OK) {
        let statusBody: RequestStatusBody = {
          status: RequestStatusBody.StatusEnum.CANCELLED,
          description: ''
        }
        this.isLoading = true;

        this.requestService.updateRequestStatus(environment.tenantId, this.request.id, statusBody).toPromise()
          .then((response: BooleanResponse) => {
            console.log(response.description);
            // this.router.navigate(['/requests']);
            this.reloadData(this.request.id);
          })
          .catch((error: any) => {
            this.oneAlert.onError(error.error);
          })
          .finally(() => this.isLoading = false);
      }
    });
  }

  onEdit() {
    const dialogRef = this.dialog.open(RequestFormComponent, {
      width: '1200px',
      position: {
        top: '65px'
      },
      hasBackdrop: true,
      disableClose: true,
      data: {
        requestGroups: [this.requestGroup],
        users: [...this.users],
        isEdit: true,
        request: { ...this.request }
      }
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.status == cst.SAVE_STATUS.OK) {
        let payload = result.payload;
        this.save(payload);
      }
    });
  }

  onPrint() {
    if (this.request.hasRequestForm == false) {
      this.oneAlert.onError(`Đề xuất "${this.request.name}" không có mẫu in đề xuất`);
      return;
    }
    this.isLoading = true;
    this.requestService.exportRequestForm(environment.tenantId, this.request.id).toPromise()
      .then((response: Blob) => {
        FileSaver.saveAs(response, `${this.request.name}.docx`);
      })
      .catch((error: any) => {
        this.oneAlert.onError(error.error);
      })
      .finally(() => this.isLoading = false);
  }

  onDownload(file: FileInfo) {
    this.requestService.getAttachment(environment.tenantId, this.request.id, file.fileId).toPromise()
    .then((response: Blob) => {
      FileSaver.saveAs(response, file.fileName);
    }).catch((error: any) => {
      this.oneAlert.onError(error);
    });
  }

  onAddTask() {
    this.taskAPIService.getProjects(this.currentUser).toPromise()
    .then(
      (response: any) =>  {
        if(response.status == 'Failed') {
          this.oneAlert.onError({message: response.message});
        }
        else {
          if(response.items.length > 0) {
            const dialogRef = this.dialog.open(TaskDialog, {
              data: {
                projects: response.items,
                users: this.users.filter((user: any) => { return this.keycloakService.isActive(user); })
              },
              width: '500px',
              hasBackdrop: true,
              disableClose: true
            });

            dialogRef.afterClosed().subscribe((result: any) => {
              this.taskAPIService.createTask(this.currentUser.email, result.name, result.project_id,
                result.category_id, result.assignee_email, result.followers, result.startDate, result.dueDate).toPromise()
              .then(
                (response: any) => {
                  if(response.status == 'Failed') {
                    this.oneAlert.onError(response);
                  }
                  else {
                    let taskIds = this.request.taskIds;
                    taskIds.push(response.workPackage.id);
                    let payload = {
                      ...this.request
                    };
                    payload.taskIds = taskIds;

                    this.save(payload);
                  }
                }
              )
              .catch(error => this.oneAlert.onError(error));
            });
          }
        }
      }
    )
    .catch((error: any) => {
      this.oneAlert.onError(error);
    });
  }

  onAddFollower(user: string) {
    this.requestService.addRequestFollowers(environment.tenantId, this.request.id, [user]).toPromise()
    .then((response: any) => {
      this.reloadData(this.request.id);
    })
    .catch((error: any) => {
      this.oneAlert.onError(error);
    })
  }

  onRemoveFollower(user: string) {
    this.requestService.removeRequestFollowers(environment.tenantId, this.request.id, [user]).toPromise()
    .then((response: any) => {
      this.reloadData(this.request.id);
    })
    .catch((error:any) => {
      this.oneAlert.onError(error);
    })
  }

  updateStatus(status: RequestStatusBody.StatusEnum, forward: boolean) {
    let directMgmts = this.request.directMgmts || [];

    let approverList = directMgmts.concat(this.request.approvers || []);
    const dialogRef = this.dialog.open(StatusConfirmDialog, {
      data: {
        status: status,
        request: this.request,
        forward: forward,
        users: this.users.filter(u => approverList.indexOf(u.username) < 0)
      },
      width: '500px',
      hasBackdrop: true,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if(result.status == cst.SAVE_STATUS.OK) {
        let decisionStatus = status;
        let selectedUsers: string[] = [];
        if (forward == true) {
          decisionStatus = result.data.agreed == true ? RequestStatusBody.StatusEnum.AGREEDFORWARD : RequestStatusBody.StatusEnum.FORWARD;
          selectedUsers = result.data.selectedUsers;
        }
        let payload: RequestStatusBody = {
          status:  decisionStatus,
          targetUsers: selectedUsers,
          description: result.data.description
        };

        this.requestService.updateRequestStatus(environment.tenantId, this.request.id, payload).toPromise()
        .then((response: any) => {
          if(response.result == true) {
            this.oneAlert.onSuccess();
            this.oneRoute.refreshComponent();
          }
          else {
            this.oneAlert.onError(response.description);
          }
        })
        .catch((error: HttpErrorResponse) => {
          let oneError = error.error || {};
          let message = oneError.message || '';
          if (message.indexOf('must_wait_for:') >= 0) {
            let nextApprovers = message.replace('must_wait_for:', '').trim();
            this.keycloakService.getAllUsers().toPromise().then((allUser:UserData[]) => {
              let nextUser = allUser.find((u:UserData) => u.username == nextApprovers);
              if (nextUser != null) {
                this.oneAlert.onError(`${this.keycloakService.getFullName(nextUser)} phải xét duyệt trước`);
              } else {
                this.oneAlert.onError('Có lỗi khi xử lý Đề Xuất này');
              }
            });
          } else {
            this.oneAlert.onError(oneError);
          }

        });
      }
    })
  }

  deteleRequest() {
    const dialogRef = this.dialog.open(ConfirmDialog,
      {
        data: 'Bạn có chắc mình muốn xóa đề xuất này?',
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.status == cst.SAVE_STATUS.OK) {
        this.requestService.removeRequest(environment.tenantId, this.request.id).toPromise()
        .then((response: any) => {
          this.oneAlert.onSuccess('Xóa yêu cầu thành công');
          this.router.navigate(['/requests']).then(
            () => { window.location.reload(); }
          );
        })
        .catch((error: any) => {
          this.oneAlert.onError(error);
        })
      }
    })
  }

  private reloadData(id: number) {
    this.isLoading = true;
    forkJoin([
      this.requestService.getRequest(environment.tenantId, id, true),
      this.keycloakUserService.getUsers(null, null, null, null, null, 0, 1024),
      this.requestApprovalTypeWrapperService.getAll()
    ]).toPromise().then((result: [OneRequest, any, RequestApprovalType[]]) => {
      this.users = result[1].items.filter((user:any) => user.enabled).map(user => {
        return {
          ...user,
          avatarUrl: this.keycloakService.getAvatar(user),
          fullName: this.keycloakService.getFullName(user),
          username: this.keycloakService.getUsername(user),
          title: this.keycloakService.getTitle(user)
        }
      });
      this.initRequest(result[0]);
      this.checkAbleToSetStatus();
      this.approvalTypes = result[2];
      this.requestGroupService.getRequestGroup(this.request.requestGroupId, environment.tenantId).toPromise()
      .then((response: RequestGroup) => {
        let approvalType = this.approvalTypes.find(type => type.id == response.approveType);
        this.requestGroup = {
          ...response,
          approvalTypeName: this.translateService.instant(`approvalType.${approvalType.name}`)
        };
        this.initTime();
      });

      if (this.request.visited == false) {
        this.requestService.visitRequest(environment.tenantId, this.request.id).toPromise().then((status) => {
        });
      }
    })
    .catch((error: any) => {
      if(error.error.message == 'request_not_found' || error.error.message == 'request_deleted') {
        this.oneAlert.onError({message: 'Không tìm thấy đề xuất'});
      }
      else {
        this.oneAlert.onError(error.error);
      }
      this.router.navigate(['/requests']);
    })
    .finally(() => {
      this.isLoading = false;
    });
  }

  private initTime() {
    // let ddate = new Date(this.request.submitTime);
   // // console.log(this.request.timeout);
   // //this.request.
   //  ddate.setTime(ddate.getTime() + this.requestGroup.sla * 60 * 60 * 1000);
    //TODO: only work for UTC+7
    if (this.request.timeout == 0) {
      this.remainingTime = this.dueDate = 'Không xác định';
      return;
    } else {
      this.dueDate = this.datePipe.transform(this.request.timeout, 'HH:mm MMM d')
    }

    this.updateRemainingTime();
  }

  private loadTasks() {
    this.workPackages = [];
    this.taskAPIService.getTasks(this.request.taskIds).toPromise()
    .then((responses: any[]) => {
      this.workPackages = responses.map((wp: any) => {
        return {
          ...wp,
          href: environment.oneTask + `/avp/tasks?task=${wp.id}`,
          friendlyStatus: this.translateService.instant(`workPackage.Status.${wp.status.name}`),
          assignee: this.getUserInfo(wp.assigned_to)
        }
      });
    });
  }

  private getUserInfo(user?: any) {
    if(user) {
      return this.users.find(u => u.email == user.mail);
    }
    return null;
  }

  private updateRemainingTime() {
    let diff = this.request.remainingTime / 1000;
    if (diff > 0) {
      let hours = Math.floor(diff / 3600);
      let minutes = Math.floor((diff - (hours * 3600)) / 60);
      let seconds = Math.round(diff - (hours * 3600) - (minutes * 60));

      let hourString = hours.toString(),
        minuteString = minutes.toString(),
        secondString = seconds.toString();

      if (hours < 10) { hourString = "0" + hours; }
      if (minutes < 10) { minuteString = "0" + minutes; }
      if (seconds < 10) { secondString = "0" + seconds; }

      this.remainingTime = hourString + ':' + minuteString + ':' + secondString;
    }
    else {
      this.remainingTime = '00:00:00';
    }
  }

  private checkAbleToSetStatus() {
    if(this.request && this.currentUser) {
      let currentUsername = this.keycloakService.getUsername(this.currentUser);

      this.canEdit = this.request.submitter == currentUsername &&
                      (this.request.status == OneRequest.StatusEnum.PENDING || this.request.status == OneRequest.StatusEnum.REQUESTUPDATE);

      if(this.request.approveds?.includes(currentUsername)) {
        this.myApproval = {
          status: RequestStatusBody.StatusEnum.APPROVED,
          icon: 'check_circle',
          text: 'Bạn đã chấp thuận cho đề xuất này'
        }
      }
      else if(this.request.rejecteds?.includes(currentUsername)) {
        this.myApproval = {
          status: RequestStatusBody.StatusEnum.REJECTED,
          icon: 'cancel',
          text: 'Bạn đã từ chối đề xuất này'
        }
      } else if (this.request.cancelleds?.includes(currentUsername)) {
        this.myApproval = {
          status: RequestStatusBody.StatusEnum.CANCELLED,
          icon: 'free_cancellation',
          text: 'Bạn đã hủy đề xuất này'
        }
      }else {
        this.myApproval = null;
      }

      this.showDecisionActions = this.myApproval == null &&
        (this.request.approvers?.findIndex(username => username == currentUsername ) > -1 ||
            this.request.directMgmts?.findIndex(username => username == currentUsername) > -1
        ) && this.request.status == OneRequest.StatusEnum.PENDING;
    }
  }

  private initRequest(originalRequest: OneRequest) {
    let submitter = this.users.find(u => this.keycloakService.getUsername(u) == originalRequest.submitter);
    let submitterUser = submitter != null ? submitter : {};
    submitterUser = {
      ...submitterUser,
      title: this.keycloakService.getTitle(submitterUser),
      username: this.keycloakService.getUsername(submitterUser),
      fullname: this.keycloakService.getFullName(submitterUser)
    }
    this.request = {
      ...originalRequest,
      submitterUser,
      attrs: this.convertToAttrs(JSON.parse(originalRequest.value)),
      friendlyStatusName: this.translateService.instant(`approvalStatus.${originalRequest.status}`)
    };

    this.loadAttachments(originalRequest);
    this.loadTasks();
  }

  private loadAttachments(originalRequest: OneRequest) {
    this.attachments = [];
    if(originalRequest.documents.length != 0) {
      let documentIds = originalRequest.documents.map(x => Number.parseInt(x));
      this.requestService.getAttachmentInfo(environment.tenantId, originalRequest.id, documentIds).toPromise().then(
        (responses: FileInfo[]) => {
          this.attachments = responses;
        }
      )
      .catch(
        (error: any) => {
          this.oneAlert.onError(error);
        }
      )
    }
  }

  private convertToAttrs(valueObject: any): any[] {
    let attrs: any[] = [];
    for (let attrName in valueObject) {
      attrs.push(valueObject[attrName]);
    }
    return attrs.sort((a: any, b: any) => a.attr.order - b.attr.order);
  }

  private save(payload: OneRequest) {
    this.isLoading = true;
    this.requestService.updateRequest(environment.tenantId, this.request.id, payload).toPromise()
      .then((response: OneRequest) => {
        this.initRequest(response);
      })
      .catch((error: any) => {
        this.oneAlert.onError(error.error);
      })
      .finally(() => this.isLoading = false);
  }
}

interface _MyOneRequest extends OneRequest {
  submitterUser: any;
  attrs: any;
  friendlyStatusName: string;
}

interface _MyOneRequestGroup extends RequestGroup {
  approvalTypeName: string;
}
