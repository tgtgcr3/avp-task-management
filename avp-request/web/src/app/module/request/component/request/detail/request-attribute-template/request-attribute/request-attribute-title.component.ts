import {BaseRequestAttributeComponent} from "./request-attribute.component";
import {Component, Input, OnInit} from "@angular/core";
import * as transliterate from "@sindresorhus/transliterate";

@Component({
  templateUrl: './request-attribute-title.component.html',
  styleUrls: ['./request-attribute-title.component.scss']
})

export class RequestAttributeTitleComponent implements BaseRequestAttributeComponent, OnInit {

  @Input()
  requestId: number;

  @Input()
  order: number;

  @Input()
  attribute: any;

  ngOnInit() {

  }
}
