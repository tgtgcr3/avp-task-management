import { formatDate } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Subscription } from 'rxjs';
import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { environment } from 'src/environments/environment';
import { KeyCloakUserService } from 'src/generated/account-api';
import { OneRequest, RequestGetAllResult, RequestService } from 'src/generated/web-api';
import * as message from '../../core/common/one-message';

@Component({
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    selector: 'search-module'
})
export class SearchComponent implements OnInit, OnDestroy {

    isLoading: boolean = true;
    pageInfo: any = {
        hasPage: true,
        total: 0,
        pageSize: 25,
        options: [10, 25, 50, 100],
        pageIndex: 0
    };

    filteredRequests: ListOneRequest[] = [];
    users: any[] = [];

    public get text() {
        return message;
    }

    private filter: any = {};
    private sub: Subscription;

    constructor(private requestService: RequestService,
        private route: ActivatedRoute,
        private keyCloakService: KeyCloakService,
        private keyCloakUserService: KeyCloakUserService,
        private translateService: TranslateService) {
            this.sub = this.route.queryParams.subscribe(params => {
                this.filter = {
                    groupId: params['groupId'],
                    status: params['status'],
                    createdDate: params['createdDate'],
                    end: params['end'],
                    creators: params['creators']
                };

                this.loadRequests();
            });
         }

    ngOnInit() {
        this.loadRequests();
    }

    ngOnDestroy() {
        if(this.sub) {
            this.sub.unsubscribe();
            this.sub = null;
        }
    }

    private loadRequests(): void {
        this.isLoading = true;
        forkJoin([
            this.keyCloakUserService.getUsers(null, null, null, null, null, 0, 1024),
            this.requestService.getAllRequests(environment.tenantId, null, this.pageInfo.pageIndex, this.pageInfo.pageSize, true,
                this.filter.groupId, this.filter.status, this.filter.toMe, this.filter.fromMe, this.filter.followed,
              null, this.filter.createdDate, this.filter.end, this.filter.creators
              )
        ]).toPromise()
            .then((result: [any, RequestGetAllResult]) => {
                this.users = result[0].items.map(user => {
                    return {
                        ...user,
                        avatarUrl: this.keyCloakService.getAvatar(user),
                        fullName: this.keyCloakService.getFullName(user),
                        username: this.keyCloakService.getUsername(user)
                    }
                });

                this.filteredRequests = result[1].items.map(request => {
                    return {
                        ...request,
                        submitterInfo: this.findUserInfos([request.submitter])[0],
                        submitDate: formatDate((request.submitTime > 0 ? new Date(request.submitTime) : Date.now()), 'dd-MM-yyyy', 'en-US'),
                        friendlyStatus: this.translateService.instant(`approvalStatus.${request.status}`)
                    };
                });
                this.pageInfo.total = result[1].total;
            }).finally(() => this.isLoading = false);
    }

    onPageChanged(event: any) {
        this.pageInfo.pageIndex = event.pageIndex;
        this.pageInfo.pageSize = event.pageSize;
        this.loadRequests();
    }

    findUserInfos(usernames: string[]): any {
        if (usernames == null) {
            return [];
        }
        return usernames.map(username => {
            const userInfo = this.users.find(user => this.keyCloakService.getUsername(user) == username);
            if (userInfo == null) {
                return {
                    submitterInfo: {},
                    approverInfos: []
                };
            }

            return {
                ...userInfo,
                fullName: this.keyCloakService.getFullName(userInfo),
                avatarUrl: this.keyCloakService.getAvatar(userInfo)

            };
        });
    }
}

interface ListOneRequest extends OneRequest {
    submitterInfo: any;
    friendlyStatus: string;
}
