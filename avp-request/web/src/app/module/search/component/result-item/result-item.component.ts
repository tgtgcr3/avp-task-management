import {Component, Input} from '@angular/core';

@Component({
    templateUrl: './result-item.component.html',
    styleUrls: ['./result-item.component.scss'],
    selector: 'result-item'
})
export class ResultItemComponent {

    @Input() request: any;

    openRequest() {
        window.open(`/requests/${this.request.id}`);
    }
}