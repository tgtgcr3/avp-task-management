import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as transliterate from '@sindresorhus/transliterate';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { OneRouteService } from 'src/app/core/service/one-route.service';
import { RequestGroupWrapperService } from 'src/app/core/service/request-group-wrapper.service';
import { RequestGroup } from 'src/generated/web-api';
import * as message from '../../../../core/common/one-message';
import * as cst from '../../../../core/common/one-const';
import * as util from '../../../../core/common/one-util';

@Component({
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss'],
    selector: 'filter'
})
export class FilterComponent implements OnInit, OnDestroy {

    @Input() users: any[];

    searchFormGroup: FormGroup;

    selectedStatus: string = 'all';

    get text() {
        return message;
    }

    statuses: any[] = [];

    requestGroups: RequestGroup[] = [];
    filteredRequestGroups: ReplaySubject<RequestGroup[]> = new ReplaySubject<RequestGroup[]>(1);
    requestGroupFilter: FormControl = new FormControl();

    filteredUsers: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
    userFilter: FormControl = new FormControl();

    protected _onDestroy = new Subject<void>();

    constructor(private requestGroupWrapperService: RequestGroupWrapperService,
        private oneRoute: OneRouteService,
        private formBuilder: FormBuilder) {

        this.requestGroupFilter.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
            this.filterRequestGroups();
        });

        this.userFilter.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
            this.filterUsers();
        })

        this.statuses = cst.StatusFilters;

        this.searchFormGroup = this.formBuilder.group({
            status: [''],
            requestGroup: [''],
            submitters: [null],
            submitTime: this.formBuilder.group({
                start: [null],
                end: [null]
            })
        });
    }

    ngOnInit() {
        this.requestGroupWrapperService.getAll()
            .then((responses: RequestGroup[]) => {
                this.requestGroups = responses;
                this.filterRequestGroups();
            });
        this.filterUsers();
    }

    ngOnDestroy(): void {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    filterRequestGroups() {
        if (!this.requestGroups) {
            return;
        }
        let search = this.requestGroupFilter.value;
        if (!search) {
            this.filteredRequestGroups.next(this.requestGroups.slice());
            return;
        } else {
            search = transliterate(search).toLowerCase();
        }
        this.filteredRequestGroups.next(this.requestGroups.filter(ut => transliterate(ut.name).toLowerCase().indexOf(search) >= 0));
    }

    filterUsers() {
        if (!this.users) {
            return;
        }

        let search = this.userFilter.value;
        if (!search) {
            this.filteredUsers.next(this.users.slice());
            return;
        } else {
            search = transliterate(search).toLowerCase();
        }
        this.filteredUsers.next(this.users.filter(ut => transliterate(ut.fullName).toLowerCase().indexOf(search) >= 0));
    }

    onSearch() {
        let params = this.oneRoute.queryParams;
        params['status'] = util.capitalizeFirstLetter(this.searchFormGroup.controls.status.value);
        params['groupId'] = this.searchFormGroup.controls.requestGroup.value;
        params['creators'] = this.searchFormGroup.controls.submitters.value;

        params['createdDate'] = this.searchFormGroup.controls.submitTime.value.start ?
                                        this.searchFormGroup.controls.submitTime.value.start.getTime() : null;

        params['end'] = this.searchFormGroup.controls.submitTime.value.end ?
                                        this.searchFormGroup.controls.submitTime.value.end.getTime() : null;

        this.oneRoute.navigateTo(['/search'], params);
    }
}
