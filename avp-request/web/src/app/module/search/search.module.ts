import {NgModule} from "@angular/core";
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatDividerModule} from '@angular/material/divider';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {MatChipsModule} from '@angular/material/chips';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import {SearchComponent} from "./search.component";
import { FilterComponent } from "./component/filter/filter.component";
import { ResultItemComponent } from "./component/result-item/result-item.component";
import { ComponentModule } from "src/app/core/component/component.module";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MatToolbarModule,
        MatTabsModule,
        MatButtonModule,
        MatIconModule,
        MatSidenavModule,
        MatDatepickerModule,
        MatSelectModule,
        MatDividerModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        NgxMatSelectSearchModule,
        ComponentModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations:[
        SearchComponent,
        FilterComponent,
        ResultItemComponent
    ],
    exports: []
})
export class SearchModule {}

