import {Component, Input, OnInit} from '@angular/core';
import { ReportData, ReportGeneralData } from 'src/generated/web-api';

@Component({
    templateUrl: './general-report.component.html',
    styleUrls: ['./general-report.component.scss'],
    selector: 'general-report'
})
export class GeneralReportComponent implements OnInit {

    @Input() generalReportData: ReportGeneralData;
    @Input() users: any[] = [];

    highActivetUser: any;

    ngOnInit() {
        this.highActivetUser = this.users.find(user => {
            return user.username == this.generalReportData.highActive;
        });
        if (this.generalReportData != null) {
          if (
            this.generalReportData.total > 0
          )
          this.generalReportData.sla = Math.round((this.generalReportData.approved + this.generalReportData.rejected) / this.generalReportData.total * 10000) / 100;
        }
    }
}
