import { formatDate } from '@angular/common';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Label } from 'ng2-charts';
import { ReportData, ReportGeneralData } from 'src/generated/web-api';

@Component({
    templateUrl: './request-report.component.html',
    styleUrls: ['./request-report.component.scss'],
    selector: 'request-report'
})
export class RequestReportComponent implements OnInit, OnChanges {

    @Input() generalReportData: ReportGeneralData;
    @Input() actionReportData: any;

    public barChartRendering: boolean = true;
    public barChartOptions: any = {
        responsive: true,
        legend: {
            position: 'bottom'
        },
        scales: { xAxes: [{}], yAxes: [{}] },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
            }
        }
    };
    public barChartLabels: Label[] = [];
    public barChartType: any = 'bar';
    public barChartLegend = true;

    public barChartData: any[] = [
        { data: [], label: 'Đã tạo' },
        { data: [], label: 'Đã đồng ý' },
        { data: [], label: 'Đã từ chối' }
    ];
    public barChartColors = [
        { backgroundColor: 'rgba(0,0,255,0.3)' },
        { backgroundColor: 'rgba(0,255,0,0.3)' },
        { backgroundColor: 'rgba(255,0,0,0.3)' },
    ];


    public pieChartRendering: boolean = true;
    public pieChartOptions: any = {
        responsive: true,
        legend: {
            position: 'bottom',
        },
        plugins: {
            datalabels: {
                formatter: (value, ctx) => {
                    const label = ctx.chart.data.labels[ctx.dataIndex];
                    return label;
                },
            },
        }
    };
    public pieChartLabels: Label[] = ['Đang chờ duyệt', 'Đã đồng ý', 'Đã từ chối'];
    public pieChartData: number[] = [];
    public pieChartType: any = 'pie';
    public pieChartLegend = true;
    public pieChartColors = [
        {
            backgroundColor: ['rgba(0,0,255,0.3)', 'rgba(0,255,0,0.3)', 'rgba(255,0,0,0.3)'],
        },
    ];

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
        if(changes.actionReportData && changes.actionReportData.currentValue) {
            console.log('action changes');
            this.initActionReport();
        }

        if(changes.generalReportData && changes.generalReportData.currentValue) {
            console.log('general changes');
            this.initStatusReport();
        }
    }

    private initActionReport() {
        this.barChartRendering = true;
        let keys = Object.keys(this.actionReportData).sort();

        this.barChartLabels = [];
        this.barChartData[0].data = [];
        this.barChartData[1].data = [];
        this.barChartData[2].data = [];

        keys.forEach(time => {
            let date = formatDate(time, 'MMM dd', 'en-US');
            this.barChartLabels.push(date);
            this.barChartData[0].data.push(this.actionReportData[time]['created']);
            this.barChartData[1].data.push(this.actionReportData[time]['approved']);
            this.barChartData[2].data.push(this.actionReportData[time]['rejected']);
        });

        this.barChartRendering = false;
    }

    private initStatusReport() {
        this.pieChartRendering = true;
        this.pieChartData = [];
        this.pieChartData.push(this.generalReportData.total - (this.generalReportData.approved + this.generalReportData.rejected + this.generalReportData.timeoutNum));
        this.pieChartData.push(this.generalReportData.approved);
        this.pieChartData.push(this.generalReportData.rejected);
        this.pieChartRendering = false;
    }
}
