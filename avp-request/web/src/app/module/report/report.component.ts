import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';
import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { OneRouteService } from 'src/app/core/service/one-route.service';
import { RequestGroupWrapperService } from 'src/app/core/service/request-group-wrapper.service';
import { environment } from 'src/environments/environment';
import { KeyCloakUserService } from 'src/generated/account-api';
import { ReportAction, ReportData, ReportGeneralData, ReportService, RequestGroup } from 'src/generated/web-api';
import * as message from '../../core/common/one-message';

@Component({
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss'],
    selector: 'report'
})
export class ReportComponent implements OnInit, OnDestroy {

    rangeForm = new FormGroup({
        start: new FormControl(),
        end: new FormControl()
    });

    requestGroupForm = new FormControl(null);

    generalReportData: ReportGeneralData;
    actionReportData: any;
    users: any[] = [];
    requestGroups: RequestGroup[] = [];

    loading: boolean = true;

    get text() {
        return message;
    }

    get isGeneral() {
        return !this.oneRoute.queryParams['type'];
    }

    get isGroup() {
        return this.oneRoute.queryParams['type'] == 'group';
    }

    private _filter: any = {
        groupId: null,
        startTime: null,
        endTime: null
    };

    private sub: Subscription;

    constructor(private reportService: ReportService,
        private route: ActivatedRoute,
        private oneRoute: OneRouteService,
        private requestGroupWrapperService: RequestGroupWrapperService,
        private keycloakService: KeyCloakService,
        private keyCloakUserService: KeyCloakUserService) {

        this.sub = this.route.queryParams.subscribe(params => {
            if(params['starttime'] && params['endtime']) {
                let start = new Date(Number.parseInt(params['starttime']));
                let end = new Date(Number.parseInt(params['endtime']));
                this.rangeForm.controls.start.setValue(start);
                this.rangeForm.controls.end.setValue(end);
            }
            
            if(params['type'] == 'group') {
                if(params['groupId']) {
                    this.requestGroupForm.setValue(Number.parseInt(params['groupId']));
                    if(this.requestGroups.length == 0) {
                        this.loadRequestGroups();
                    }
                }
                else {
                    this.requestGroupForm.setValue(this.requestGroups[0].id);
                }
            }
            this._filter = {
                groupId: params['groupId'],
                startTime: params['starttime'],
                endTime: params['endtime']
            }
            this.loadReport();
        });

        this.rangeForm.controls.end.valueChanges.subscribe(date => {
            let start = this.rangeForm.controls.start.value;
            if(start && date) {
                let optionParams = this.oneRoute.queryParams;
                optionParams['starttime'] = start.getTime();
                optionParams['endtime'] = date.getTime();

                this.oneRoute.navigateTo([this.oneRoute.pathWithoutOptionalParams], optionParams);
            }
        });

        this.requestGroupForm.valueChanges.subscribe(group => {
            if(this.oneRoute.queryParams['type'] == 'group') {
                let optionParams = this.oneRoute.queryParams;
                optionParams['groupId'] = group;
    
                this.oneRoute.navigateTo([this.oneRoute.pathWithoutOptionalParams], optionParams);
            }
        });
    }

    ngOnInit() {
        this.loadRequestGroups();
    }

    ngOnDestroy() {
        if (this.sub) {
            this.sub.unsubscribe();
            this.sub = null;
        }
    }

    reportGeneral() {
        let optionParams = this.oneRoute.queryParams;
        optionParams['type'] = undefined;
        optionParams['groupId'] = undefined;
        this.oneRoute.navigateTo([this.oneRoute.pathWithoutOptionalParams], optionParams);
    }

    reportByGroup() {
        let optionParams = this.oneRoute.queryParams;
        optionParams['type'] = 'group';
        optionParams['groupId'] = this.requestGroups[0].id;
        this.oneRoute.navigateTo([this.oneRoute.pathWithoutOptionalParams], optionParams);
    }

    private loadReport() {
        this.loading = true;
        forkJoin([
            this.reportService.getGeneralReport(environment.tenantId, 'general', this._filter.startTime, this._filter.endTime, this._filter.groupId),
            this.keyCloakUserService.getUsers(null, null, null, null, null, 0, 1024)
        ]).toPromise().then(
            (result: [ReportData, any]) => {
                this.generalReportData = result[0].general;
                this.users = result[1].items.map(user => {
                    return {
                        ...user,
                        avatarUrl: this.keycloakService.getAvatar(user),
                        fullName: this.keycloakService.getFullName(user),
                        username: this.keycloakService.getUsername(user)
                    }
                });
                this.loadRequestReport();
            }
        ).finally(() => this.loading = false);
    }

    private loadRequestReport() {
        this.reportService.getGeneralReport(environment.tenantId, 'action', this._filter.startTime, this._filter.endTime, this._filter.groupId).toPromise()
        .then(
            (result: ReportData) => {
                this.actionReportData = result.action;
            }
        )
    }

    private loadRequestGroups() {
        this.requestGroupWrapperService.getAll().then(
            (responses: RequestGroup[]) => {
                this.requestGroups = responses;
                this.requestGroupForm.setValue(this.requestGroups[0].id);
            }
        );
    }
}