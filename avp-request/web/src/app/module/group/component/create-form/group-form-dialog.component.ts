import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Department, DepartmentService, RequestApprovalType, RequestGroup } from 'src/generated/web-api';
import { RequestGroupWrapperService } from '../../../../core/service/request-group-wrapper.service';
import * as cst from '../../../../core/common/one-const';
import * as message from '../../../../core/common/one-message';
import { RequestApprovalTypeWrapperService } from 'src/app/core/service/request-approval-type.service';
import { KeyCloakUserService } from 'src/generated/account-api/api/keyCloakUser.service';
import { environment } from 'src/environments/environment';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
    templateUrl: './group-form-dialog.component.html',
    styleUrls: ['./group-form-dialog.component.scss'],
    selector: 'group-create-form'
})
export class GroupFormComponent implements OnInit {
    requestGroupFormGroup: FormGroup;
    newGroupRequest: RequestGroup = {
        mgmtNotification: 0,
        requestForm: 0,
        status: RequestGroup.StatusEnum.ENABLED,
        targetType: 1,
        targetId: 0
    };
    users: any[] = [];
    departments: Department[] = [];

    approvalTypes: MyApprovalType[] = [];

    requiredDM: boolean = false;
    customForm: boolean = false;

    groupSla = 0;


    get text() {
        return message;
    }

    get isInvalidData() {
        return this.newGroupRequest.name == ''
        || !this.requestGroupFormGroup.controls.approvers.value
        || this.requestGroupFormGroup.controls.approvers.value.length == 0;
    }

    get saveText() {
        return this.isEdit ? message.EDIT_GROUP : message.CREATE_GROUP;
    }

    get title() {
        return this.isEdit ? message.EDIT_GROUP_TITLE : message.CREATE_GROUP_TITLE;
    }

    private get isEdit() {
        return this.data.requestGroup ? true : false;
    }

    constructor(
        private formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<GroupFormComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private requestGroupWrapperService: RequestGroupWrapperService,
        private departmentService: DepartmentService,
        private translationService: TranslateService,
        private requestApprovalTypeWrapperService: RequestApprovalTypeWrapperService) {
            this.requestGroupFormGroup = this.formBuilder.group({
                name: ['', Validators.required],
                SLA: [0, Validators.min(0)],
                approvers: [null, Validators.required],
                followers: [null]
              });

            if(this.data) {
                if(this.data.requestGroup) {
                    this.newGroupRequest = this.data.requestGroup;
                    this.groupSla = this.newGroupRequest.sla / 3600000;
                }
                this.users = this.data.users;
            }

            this.requiredDM = this.newGroupRequest.mgmtNotification == 1;
            this.customForm = this.newGroupRequest.requestForm == 1;
            this.requestGroupFormGroup.controls.approvers.setValue(this.newGroupRequest.approvers);
            this.requestGroupFormGroup.controls.followers.setValue(this.newGroupRequest.followers);
         }

    ngOnInit() {
        forkJoin([
            this.requestApprovalTypeWrapperService.getAll(),
            this.departmentService.getAllDepartments(environment.tenantId, false)
        ]).toPromise().then(
            (results: any[]) => {
                this.approvalTypes = results[0].map(type => {
                    return {
                        ...type,
                        friendlyName: this.translationService.instant(`approvalType.${type.name}`)
                    }
                });
                this.departments = results[1].items;
            }
        )
    }

    public onSave() {
        this.newGroupRequest.mgmtNotification = this.requiredDM === true ? 1 : 0;
        this.newGroupRequest.requestForm = this.customForm === true ? 1 : 0;
        this.newGroupRequest.approvers = this.requestGroupFormGroup.controls.approvers.value;
        this.newGroupRequest.followers = this.requestGroupFormGroup.controls.followers.value;
        this.newGroupRequest.sla = this.groupSla * 3600000;
        let promise:Promise<RequestGroup> = undefined;
        if(this.isEdit) {
            promise = this.requestGroupWrapperService.update(this.newGroupRequest.id, this.newGroupRequest);
        }
        else {
            promise = this.requestGroupWrapperService.create(this.newGroupRequest);
        }

        promise
        .then((response: RequestGroup) => {
            this.dialogRef.close({
                status: cst.SAVE_STATUS.OK,
                body: response
            });
        });
    }

    public getNameErrorMessage() {
        if (this.requestGroupFormGroup.controls.name.hasError('required')) {
            return `Tên ${message.ERR_NOT_NULL_VALUE}`;
        }
        return '';
    }

    public getSLAErrorMessage() {
        if (this.requestGroupFormGroup.controls.SLA.hasError('min')) {
            return `${message.ERR_OUT_OF_RANGE} Giá trị phải lớn hơn 0`;
        }

        if (this.requestGroupFormGroup.controls.SLA.hasError('pattern')) {
            return message.ERR_INCORRECT_FORMAT;
        }

        return '';
    }
}


interface MyApprovalType extends RequestApprovalType {
    friendlyName?: string;
}

