import {Component, ElementRef, EventEmitter, Output, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {KeyCloakService} from 'src/app/core/service/keycloak.service';
import {OneAlertService} from 'src/app/core/service/one-alert.service';
import {OneRouteService} from 'src/app/core/service/one-route.service';
import {RequestGroupWrapperService} from 'src/app/core/service/request-group-wrapper.service';
import {KeyCloakUserService} from 'src/generated/account-api/api/keyCloakUser.service';
import * as message from '../../../../core/common/one-message';
import * as cst from '../../../../core/common/one-const';
import {GroupFormComponent} from '../create-form/group-form-dialog.component';
import {FileService, RequestService, RequestStatusBody} from "../../../../../generated/web-api";
import {environment} from "../../../../../environments/environment";
import * as FileSaver from 'file-saver';
import {StatusConfirmDialog} from "../../../request/component";
import {HttpErrorResponse} from "@angular/common/http";
import {UserData} from "../../../../../generated/keycloak-api";
import {ExportRequestDialogComponent} from "../export-request-dialog/export-request-dialog.component";

@Component({
  templateUrl: './group-header.component.html',
  styleUrls: ['./group-header.component.scss'],
  selector: 'group-header'
})
export class GroupHeaderComponent {

  @Output()
  filter: EventEmitter<string> = new EventEmitter<string>();

  currentFilter: string = '';

  @ViewChild('uploader')
  uploader: ElementRef;

  get isFilterAll() {
    return this.currentFilter == '';
  }

  get isFilterActive() {
    return this.currentFilter == 'ENABLED';
  }

  get isFilterInactive() {
    return this.currentFilter == 'DISABLED';
  }

  constructor(private oneRoute: OneRouteService,
              private keycloakUserService: KeyCloakUserService,
              private keycloakService: KeyCloakService,
              private requestGroupWrapperService: RequestGroupWrapperService,
              private oneAlert: OneAlertService,
              private fileService: FileService,
              private requestService: RequestService,
              public dialog: MatDialog) {
  }

  public openCreateForm() {
    this.keycloakUserService.getUsers()
    .toPromise().then((result: any) => {
        const dialogRef = this.dialog.open(GroupFormComponent, {
            width: '800px',
            position: {
                top: '65px'
            },
            hasBackdrop: true,
            disableClose: true,
            data: {
            users: result.items.filter(user => {
              return this.keycloakService.isActive(user);
            }).map(user => {
                return {
                ...user,
                avatarUrl: this.keycloakService.getAvatar(user),
                fullName: this.keycloakService.getFullName(user),
                username: this.keycloakService.getUsername(user)
                }
            })
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if(result.status == cst.SAVE_STATUS.OK) {
                window.location.reload();
            }
        })
    });
  }

  uploadFile($event) {
    let file = $event.target.files[0] as File;
    let extension = file.name.split('.').pop();
    if (['xlsx', 'xls'].includes(extension)) {
      this.requestGroupWrapperService.import(file).then((data) => {
        setTimeout(() => {
          window.location.reload();
        });
      }).catch((error) => {
        this.oneAlert.onError("Nội dung của tệp không đúng. Vui lòng kiểm tra lại");
      }).finally(() => {
        this.uploader.nativeElement.value = '';
      });
    } else {
      this.uploader.nativeElement.value = '';
      this.oneAlert.onError({message: `Tệp không đúng định dạng - Extension: ${extension}`});
    }
  }

  onFilter(status: string) {
    this.currentFilter = status;
    // this.filter.emit(status);
    this.oneRoute.navigateTo([this.oneRoute.pathWithoutOptionalParams], {status: status});
  }

  public get isInDetail() {
    return !!this.oneRoute.params['id'] == true;
  }

  public get text() {
    return message;
  }

  downloadExampleRequest() {
    this.fileService.download(environment.tenantId, '0').toPromise().then((response: Blob) => {
      FileSaver.saveAs(response, `request-group-template.xlsx`);
    });
  }
}
