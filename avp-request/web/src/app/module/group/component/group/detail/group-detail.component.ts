import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {OneRouteService} from 'src/app/core/service/one-route.service';
import {RequestGroupWrapperService} from 'src/app/core/service/request-group-wrapper.service';
import {
  Attribute,
  AttributeType,
  FileInfo,
  FileService,
  RequestGroup,
  RequestGroupForward,
  RequestGroupForwardService,
  RequestGroupService, RequestService
} from 'src/generated/web-api';
import {AttributeFormDialog} from './attribute-form/attribute-form-dialog.component';
import * as cst from '../../../../../core/common/one-const';
import {GroupFormComponent} from '../../create-form/group-form-dialog.component';
import {UserMenuComponent} from 'src/app/core/component/user-menu/user-menu.component';
import {GroupSelectDialog} from 'src/app/core/component/group-select/group-select-dialog.component';
import {ConfirmDialog} from 'src/app/core/component/confirm-dialog/confirm-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import {forkJoin, Subscription} from 'rxjs';
import {KeyCloakUserService} from 'src/generated/account-api/api/keyCloakUser.service';
import {KeyCloakService} from 'src/app/core/service/keycloak.service';
import {OneLoaderService} from 'src/app/core/service/one-loader.service';
import {OneAlertService} from 'src/app/core/service/one-alert.service';
import * as FileSaver from 'file-saver';
import {RequestFormComponent} from 'src/app/module/request/component';
import {AttributeTypeWrapperService} from "../../../../../core/service/attribute-type.service";
import { ForwardGroupEditForm } from './forward-group-edit-form/forward-group-edit-form.component';
import {ExportRequestDialogComponent} from "../../export-request-dialog/export-request-dialog.component";
import {environment} from "../../../../../../environments/environment";
import {formatDate} from "@angular/common";

@Component({
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.scss'],
  selector: 'group-detail'
})
export class GroupDetailComponent implements OnInit, OnDestroy {

  public requestGroup: MyRequestGroup = {};
  public loaded: boolean = false;
  public approvers: any[] = [];
  public followers: any[] = [];
  public allUsers: any[] = [];
  public fileInfos: FileInfo[] = [];
  public forwardGroups: RequestGroupForward[] = [];

  private routerSub: Subscription;
  private attrTypes: AttributeType[] = [];

  constructor(public dialog: MatDialog,
              private route: ActivatedRoute,
              private oneRoute: OneRouteService,
              private oneLoader: OneLoaderService,
              private oneAlert: OneAlertService,
              private requestGroupWrapperService: RequestGroupWrapperService,
              private requestGroupService: RequestGroupService,
              private fileService: FileService,
              private keycloakService: KeyCloakService,
              private attributeTypeService: AttributeTypeWrapperService,
              private keycloakUserService: KeyCloakUserService,
              private requestService: RequestService,
              private forwardGroupRequestService: RequestGroupForwardService) {

  }

  ngOnInit() {
    this.routerSub = this.route.params.subscribe(params => {
      this.loadRequestGroup(params['id']);
    });
  }

  ngOnDestroy() {
    this.routerSub.unsubscribe();
  }

  onActionEvent(event: any) {
    if (event.status == 'update') {
      this.save(event.payload);
    }
  }

  openAttributeForm() {
    const dialogRef = this.dialog.open(AttributeFormDialog,
      {
        data: {
          group: {...this.requestGroup},
          isEdit: false,
          attrTypes: this.attrTypes
        },
        width: '500px',
        position: {
          top: '65px'
        },
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: any) => {
      this.onActionEvent(result);
    });
  }

  openEditForm() {
    const dialogRef = this.dialog.open(GroupFormComponent,
      {
        data: {
          requestGroup: {...this.requestGroup},
          users: this.allUsers
        },
        width: '800px',
        position: {
          top: '65px'
        },
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: cst.DialogResult) => {
      if (result.status === cst.SAVE_STATUS.OK) {
        this.loadRequestGroup(result.body.id);
      }
    });
  }

  openUserSelect(type: 'approver' | 'follower') {
    const dialogRef = this.dialog.open(UserMenuComponent,
      {
        data: {
          users: this.allUsers.filter(user => {
            return this.keycloakService.isActive(user);
          }),
          selectedUsers: type == 'approver' ? [...this.approvers] : [...this.followers],
          title: type == 'approver' ? 'Thêm người xét duyệt' : 'Thêm người theo dõi'
        },
        width: '350px',
        position: {
          top: '65px'
        },
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.status == 'added') {
        let payload = {...this.requestGroup};
        if (type == 'approver') {
          if (payload.approvers) {
            payload.approvers.push(result.user.username);
          } else {
            payload.approvers = [result.user.username];
          }
        } else if (type == 'follower') {
          if (payload.followers) {
            payload.followers.push(result.user.username);
          } else {
            payload.followers = [result.user.username];
          }
        }

        this.save(payload);
      }
    });
  }

  openGroupSelect() {
    this.requestGroupWrapperService.getAll().then((responses: RequestGroup[]) => {
      const dialogRef = this.dialog.open(GroupSelectDialog,
        {
          data: {
            groups: responses.filter((group => {
              return group.id != this.requestGroup.id && !this.forwardGroups.some(g => g.targetId == group.oId);
            }))
          },
          width: '500px',
          position: {
            top: '65px'
          },
          hasBackdrop: true,
          disableClose: true
        });

      dialogRef.afterClosed().subscribe((result: any) => {
        if(result) {
          let payload: RequestGroupForward = {
            id: 0,
            targetId: result.oId,
            status: RequestGroupForward.StatusEnum.Pending
          };

          this.forwardGroupRequestService.createForwardGroup(environment.tenantId, this.requestGroup.oId, payload).toPromise()
          .then((response: RequestGroupForward) => {
            this.forwardGroups.push(response);
          })
        }
      })
    });
  }

  onEditForwardGroup(group: RequestGroupForward) {
    const dialogRef = this.dialog.open(ForwardGroupEditForm,
      {
        data: {
          group: group
        },
        width: '500px',
        position: {
          top: '65px'
        },
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: RequestGroupForward) => {
      if(result) {
        this.forwardGroupRequestService.updateForwardGroup(environment.tenantId, this.requestGroup.oId, result.id, result).toPromise()
        .then((response: RequestGroupForward) => {
          this.loadForwardRequestGroups();
        });
      }
    })
  }

  onDeleteForwardGroup(group: RequestGroupForward) {
    const dialogRef = this.dialog.open(ConfirmDialog,
      {
        data: `Bạn có chắc mình muốn xóa nhóm chuyển tiếp <b>${group.targetName}</b>?`,
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.status == cst.SAVE_STATUS.OK) {
        this.forwardGroupRequestService.removeForwardGroup(environment.tenantId, this.requestGroup.oId, group.id).toPromise()
        .then((result: any) => {
          this.loadForwardRequestGroups();
        });
      }
    })
  }

  cloneGroup() {
    const dialogRef = this.dialog.open(ConfirmDialog,
      {
        data: 'Bạn có chắc mình muốn nhân bản nhóm đề xuất này?',
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.status == cst.SAVE_STATUS.OK) {
        let payload = {...this.requestGroup};
        payload.id = null;
        this.clone(payload);
      }
    })
  }

  deleteGroup() {
    const dialogRef = this.dialog.open(ConfirmDialog,
      {
        data: 'Bạn có chắc mình muốn xóa nhóm đề xuất này? Dữ liệu sẽ không thể khôi phục sau khi xóa. Vui lòng cẩn thận.',
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.status == cst.SAVE_STATUS.OK) {
        let payload = {...this.requestGroup};
        payload.status = RequestGroup.StatusEnum.DELETED;
        this.save(payload, true);
      }
    })
  }

  groupRequests() {
    this.oneRoute.navigateTo(['/requests'], {groupId: this.requestGroup.id});
  }

  createRequest() {
    const dialogRef = this.dialog.open(RequestFormComponent, {
      width: '1200px',
      position: {
        top: '65px'
      },
      hasBackdrop: true,
      disableClose: true,
      data: {
        requestGroups: [this.requestGroup],
        users: this.allUsers
      }
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.groupRequests();
      }
    })
  }

  onRemoveApprover(tobeRemoveUser: any) {
    let payload = {...this.requestGroup};
    payload.approvers = payload.approvers.filter((user: any) => {
      return user != tobeRemoveUser.username;
    });

    this.save(payload);
  }

  onRemoveFollower(tobeRemoveUser: any) {
    let payload = {...this.requestGroup};
    payload.followers = payload.followers.filter((user: any) => {
      return user != tobeRemoveUser.username;
    });

    this.save(payload);
  }

  uploadForm($event: any) {
    let file = $event.target.files[0] as File;
    let extension = file.name.split('.').pop();
    if (['docx', 'doc'].includes(extension)) {
      this.fileService.upload(environment.tenantId, file).toPromise()
        .then((response: FileInfo) => {
          let payload = {...this.requestGroup};
          payload.requestForms = [`${response.fileId}`];
          payload.requestForm = 1;
          this.save(payload);
        })
    } else {
      this.oneAlert.onError({message: `Tệp không đúng định dạng - Extension: ${extension}`});
    }
  }

  onDownloadFile(file: FileInfo) {
    this.requestGroupService.getRequestGroupAttachment(environment.tenantId, this.requestGroup.id, file.fileId).toPromise().then((response: Blob) => {
      FileSaver.saveAs(response, file.fileName);
    });

  }

  private loadRequestGroup(id: number) {
    this.loaded = false;
    this.oneLoader.show();

    forkJoin([
      this.requestGroupWrapperService.get(id),
      this.keycloakUserService.getUsers(),
      this.attributeTypeService.getAll()
    ]).toPromise().then(
      (results: [RequestGroup, any, AttributeType[]]) => {
        this.requestGroup = {
          ...results[0],
          orderedAttrs: results[0].attrs?.sort((a, b) => (a.order > b.order) ? 1 : -1),
          isDirect: results[0].oId == 0
        };
        this.allUsers = results[1].items.map(user => {
          return {
            ...user,
            avatarUrl: this.keycloakService.getAvatar(user),
            fullName: this.keycloakService.getFullName(user),
            username: this.keycloakService.getUsername(user)
          }
        });
        this.attrTypes = results[2];

        this.initUserInfo();
        this.loadTemplateFiles();
        this.loadForwardRequestGroups();

      }
    ).finally(() => {
      this.loaded = true;
      this.oneLoader.hide();
    });
  }

  public originalOrder(a: any, b: any):number {
    return 0;
  }

  private save(payload: any, isDelete: boolean = false) {
    this.loaded = false;
    this.requestGroupWrapperService.update(this.requestGroup.id, payload)
      .then((response: RequestGroup) => {
        if (isDelete) {
          this.oneRoute.navigateTo(['/groups']).then(() => {
            window.location.reload();
          });
        } else {
          this.requestGroup = {
            ...response,
            orderedAttrs: response.attrs?.sort((a, b) => (a.order > b.order) ? 1 : -1),
            isDirect: response.oId == 0
          };
          this.initUserInfo();
          this.loadTemplateFiles()
          this.loaded = true;
        }
      })
      .catch((error: any) => {
        this.oneAlert.onError(error.error);
      });
  }

  private clone(payload: any) {
    this.loaded = false;
    this.requestGroupWrapperService.create(payload)
      .then((response: RequestGroup) => {
        this.oneRoute.navigateTo(['/groups']).then(() => {
          window.location.reload();
        });
        this.loaded = true;
      });
  }

  private initUserInfo() {
    if (this.requestGroup.approvers) {
      this.approvers = this.requestGroup.approvers.map((username: string) => {
        let user = this.allUsers.find(u => u.username == username);
        if (user == null) {
          return {};
        }
        return user;
      });
    }

    if (this.requestGroup.followers) {
      this.followers = this.allUsers.filter((user: any) => {
        return this.requestGroup.followers.includes(user.username);
      });
    }
  }

  private loadTemplateFiles() {
    let fileIds = this.requestGroup.requestForms.map(id => Number.parseInt(id));
    if (fileIds.length == 0) {
      this.fileInfos = [];
    } else {
      this.requestGroupService.getGroupAttachmentInfo(environment.tenantId, this.requestGroup.id, fileIds).toPromise()
        .then((responses: FileInfo[]) => {
          this.fileInfos = responses;
        });
    }
  }

  private loadForwardRequestGroups() {
    this.forwardGroupRequestService.getAllForwardGroups(environment.tenantId, this.requestGroup.oId).toPromise()
    .then((responses: RequestGroupForward[]) => {
      this.forwardGroups = responses;
    })
  }

  exportRequests() {
    const dialogRef = this.dialog.open(ExportRequestDialogComponent, {
      data: {
      },
      width: '500px',
      hasBackdrop: true,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if(result.status == cst.SAVE_STATUS.OK) {
        this.requestService.exportAllRequests(environment.tenantId,null, 1, 20, true, this.requestGroup.oId,
          null, null, null, null, null, result.data.start.getTime(), result.data.end.getTime()).toPromise().then((data) => {
          FileSaver.saveAs(data, this.requestGroup.name + '_' + formatDate(result.data.start, 'dd-MM-yyyy', 'en-US') + '_' + formatDate(result.data.end, 'dd-MM-yyyy', 'en-US') + '.xlsx');
        }).catch(error => {
          this.oneAlert.onError('Không tồn tại đề xuất thõa điều kiện');
        });
      }
    })
  }
}

interface MyRequestGroup extends RequestGroup {
  orderedAttrs?: Attribute[];
  isDirect?: boolean;
}
