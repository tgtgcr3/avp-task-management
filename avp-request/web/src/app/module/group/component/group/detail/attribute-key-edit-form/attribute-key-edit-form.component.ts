import {Component, Inject} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Attribute } from 'src/generated/web-api';

@Component({
    templateUrl: './attribute-key-edit-form.component.html',
    styleUrls: ['./attribute-key-edit-form.component.scss'],
    selector: 'attribute-key-edit-form'
})
export class AttributeKeyEditForm {

    fieldNameControl = new FormControl('', Validators.required);

    constructor(
        public dialogRef: MatDialogRef<AttributeKeyEditForm>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
            this.fieldNameControl.setValue(this.data.attribute.fieldName);
         }

    onSaveAttribute() {
        let payload = {...this.data.group};

        let index = payload.attrs.findIndex((attribute: Attribute) => {
            return this.data.attribute.id == attribute.id;
        });

        if(index >= 0) {
            payload.attrs[index].fieldName = this.fieldNameControl.value;
        }

        this.dialogRef.close({
            status: 'update',
            payload: payload
        });
    }

}