import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {RequestGroupWrapperService} from 'src/app/core/service/request-group-wrapper.service';
import {RequestApprovalType, RequestGroup} from 'src/generated/web-api';
import * as message from '../../../../../core/common/one-message';
import {MatSlideToggleChange} from '@angular/material/slide-toggle';
import {RequestApprovalTypeWrapperService} from 'src/app/core/service/request-approval-type.service';
import {ReplaySubject, Subscription} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {UserMenuComponent} from 'src/app/core/component/user-menu/user-menu.component';
import {TranslateService} from '@ngx-translate/core';
import {OneRouteService} from "../../../../../core/service/one-route.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  templateUrl: './group-li.component.html',
  styleUrls: ['./group-li.component.scss'],
  selector: 'group-li'
})
export class GroupLiComponent implements OnInit, OnDestroy {
  @Input() group: RequestGroup = {};
  @Input() users: any[] = [];
  @Input() approvalTypes: RequestApprovalType[] = [];

  @Input()
  needRefresh = false;

  requireDM: boolean = false;
  status: boolean = true;
  approvalType: string = '';
  approvers: any[] = [];
  moreApprover: number = 0;

  friendlyApprovalName: string = '';

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(public dialog: MatDialog,
              private translateService: TranslateService,
              private oneRoute: OneRouteService,
              private route: ActivatedRoute,
              private router: Router,
              private requestGroupWrapperService: RequestGroupWrapperService) {
  }

  get description(): string {
    return this.group.description ? this.group.description : message.NO_DESCRIPTION;
  }

  get sla(): string {
    return this.group.sla ? `<b>${this.group.sla / 3600000}</b> (giờ)` : '--';
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  ngOnInit() {
    this.initUserInfo();
    this.refineData();
  }

  onStatusChange(event: MatSlideToggleChange) {
    console.log(event);
    let payload = {...this.group};
    payload.status = event.checked === true ? RequestGroup.StatusEnum.ENABLED : RequestGroup.StatusEnum.DISABLED;
    this.save(payload);
  }

  openApproversPopup() {
    const dialogRef = this.dialog.open(UserMenuComponent,
      {
        data: {
          users: this.users,
          selectedUsers: [...this.approvers],
          title: 'Thêm người xét duyệt'
        },
        width: '350px',
        position: {
          top: '65px'
        },
        hasBackdrop: true,
        disableClose: true
      });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.status == 'added') {
        let payload = {...this.group};
        if (payload.approvers) {
          payload.approvers.push(result.user.username);
        } else {
          payload.approvers = [result.user.username];
        }

        this.save(payload);
      }
    });
  }

  private save(payload: RequestGroup) {
    return this.requestGroupWrapperService.update(this.group.id, payload)
      .then((response: RequestGroup) => {
        this.group = response;
        if (this.needRefresh == true) {
          setTimeout(() => {
            this.oneRoute.reload();
          });
        }
      });
  }

  private refineData() {
    this.requireDM = this.group.mgmtNotification === 1 ? true : false;
    this.status = this.group.status === RequestGroup.StatusEnum.ENABLED ? true : false;
    this.approvalType = this.approvalTypes.find((a: RequestApprovalType) => a.id == this.group.approveType).name;
    this.friendlyApprovalName = this.translateService.instant(`approvalType.${this.approvalType}`);
  }

  private initUserInfo() {
    if (this.group.approvers) {
      this.approvers = this.users.filter((user: any) => {
        return this.group.approvers.includes(user.username);
      });

      if (this.approvers.length > 3) {
        this.moreApprover = this.approvers.length - 2;
        this.approvers = this.approvers.slice(0, 2);
      }
    }
  }
}
