import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DepartmentSelect } from 'src/app/core/component/department-select/department-select.component';
import { RequestApprovalTypeWrapperService } from 'src/app/core/service/request-approval-type.service';
import { environment } from 'src/environments/environment';
import {
  Department,
  DepartmentService,
  ReportGeneralData,
  ReportService,
  RequestApprovalType,
  RequestGroup
} from 'src/generated/web-api';
import * as cst from 'src/app/core/common/one-const';
import { UserMenuComponent } from 'src/app/core/component/user-menu/user-menu.component';
import { forkJoin } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
    templateUrl: './group-detail-sidebar.component.html',
    selector: 'group-detail-sidebar',
    styleUrls: ['group-detail-sidebar.component.scss']
})
export class GroupDetailSidebarComponent implements OnInit {

    @Input() model: any = {};
    @Input() users: any[] = [];
    @Output() actionEvent: EventEmitter<any> = new EventEmitter();

    approvalType: MyApprovalType = {};
    isDirectManager: boolean = false;

    approvalTypes: MyApprovalType[] = [];
    departments: Department[] = [];

    creator: any = {};
    target: Department = {};

    approvalOwner: any = {};
    approvers: any[] = [];

    reportData: ReportGeneralData = {
      total: 0,
      processed: 0,
      approved: 0
    };

    constructor(public dialog: MatDialog,
        private translationService: TranslateService,
        private departmentService: DepartmentService,
        private reportService: ReportService,
        private requestApprovalTypeWrapperService: RequestApprovalTypeWrapperService) { }

    ngOnInit() {
        forkJoin([
            this.requestApprovalTypeWrapperService.getAll(),
            this.departmentService.getAllDepartments(environment.tenantId)
        ]).toPromise().then(
            (results: any[]) => {
                this.approvalTypes = results[0].map((type: RequestApprovalType) => {
                    return {
                        ...type,
                        friendlyName: this.translationService.instant(`approvalType.${type.name}`)
                    }
                });
                this.departments = results[1].items;
                this.initData();
            }
        );

        this.reportService.getRequestGroupReport(environment.tenantId, this.model.id).toPromise().then((report: ReportGeneralData) => {
          this.reportData = report;
        });

    }

    ngOnDestroy() {
    }

    onApprovalTypeChange(type: MyApprovalType) {
        let payload = { ...this.model };
        payload.approveType = type.id;
        this.save(payload);
    }

    onDirectManagerChange(isDirectManager: boolean) {
        let payload = { ...this.model };
        payload.mgmtNotification = isDirectManager == true ? 1 : 0;
        this.save(payload);
    }

    onEmailNotificationChange(isEmailNotification: boolean) {

    }

    openDepartmentSelection() {
        const dialogRef = this.dialog.open(DepartmentSelect,
            {
                data: {
                    departments: this.departments
                },
                width: '400px',
                position: {
                    top: '65px'
                },
                hasBackdrop: true,
                disableClose: true
            });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (result.status == cst.SAVE_STATUS.OK) {
                let payload = { ...this.model };
                payload.targetId = result.department.id;

                this.save(payload);
            }
        });
    }

    openUserSelect() {
        const dialogRef = this.dialog.open(UserMenuComponent,
            {
                data: {
                    users: this.approvers,
                    selectedUsers: [this.approvalOwner],
                    title: 'Quyền tối cao'
                },
                width: '350px',
                position: {
                    top: '65px'
                },
                hasBackdrop: true,
                disableClose: true
            });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (result.status == 'added') {
                let payload = { ...this.model };
                payload.approvalOwner = result.user.username;
                this.save(payload);
            }
        });
    }

    initData() {
        this.approvalType = this.approvalTypes.find((item: MyApprovalType) => {
            return item.id == this.model.approveType;
        });
        this.isDirectManager = this.model.mgmtNotification == 1 ? true : false;
        this.creator = this.users.find((user: any) => {
            return this.model.creator == user.username;
        }) || {};

        this.target = this.model.targetId == 0 || this.model.targetId == null ?
            {
                id: 0,
                name: 'Toàn công ty'
            } :
            this.departments.find((dep: Department) => {
                return dep.id == this.model.targetId;
            });

        if (this.model.approvers) {
            this.approvers = this.users.filter((user: any) => {
                return this.model.approvers.includes(user.username);
            });
        }

        this.approvalOwner = this.users.find((user: any) => {
            return this.model.approvalOwner == user.username;
        });

        if (!this.approvalOwner) {
            this.approvalOwner = {
                username: null,
                fullName: 'Không xác lập'
            }
        }
    }

    private save(payload: RequestGroup) {
        this.actionEvent.emit({
            status: 'update',
            payload: payload
        });
    }
}

interface MyApprovalType extends RequestApprovalType {
    friendlyName?: string;
}
