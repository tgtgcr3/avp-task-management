import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {AttributeTypeWrapperService} from 'src/app/core/service/attribute-type.service';
import {Attribute, AttributeType, RequestGroup} from 'src/generated/web-api';
import * as util from 'src/app/core/common/one-util';
import {TranslateService} from "@ngx-translate/core";

@Component({
  templateUrl: './attribute-form-dialog.component.html',
  styleUrls: ['attribute-form-dialog.component.scss'],
  selector: 'attribute-form-dialog'
})
export class AttributeFormDialog implements OnInit{

  attributeType?: AttributeType;
  attributeTypes: MyAttrType[] = [];

  errorMessage?: string;

  otherAttributes: Attribute[] = [];
  followingBy?: number;
  order?: number;

  attributeFormGroup: FormGroup;

  hasConfig = false;

  additionDataLabel = '';

  additionalDataDes = '';

  formSubmitted = false;


  get title() {
    return this.data.isEdit == true ? 'Chỉnh sửa trường dữ liệu' : 'Thêm trường dữ liệu';
  }

  get saveText() {
    return this.data.isEdit == true ? 'Chỉnh sửa' : 'Thêm trường mới';
  }

  get form() {
    return this.attributeFormGroup.controls;
  }

  constructor(
    public dialogRef: MatDialogRef<AttributeFormDialog>,
    private translationService: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder
    ) {
    this.attributeTypes = this.data.attrTypes.map(attr => {
      return {
        ...attr,
        friendlyName: translationService.instant(`attrType.${attr.name}`) + ` (${attr.name})`
      }
    });
    let oldAttr:Attribute = this.data.attribute || {};
    this.hasConfig = this.hasAdditionalConfig(oldAttr.attrType);
    this.otherAttributes = this.data.group.attrs.filter(attr => {
      return attr.id != oldAttr.id
    });
    this.order = oldAttr.order;
    this.attributeFormGroup = this.formBuilder.group({
      attrType: [oldAttr.attrType, Validators.required],
      attrName: [oldAttr.name, Validators.required],
      description: [oldAttr.description],
      additionalData: [this.hasConfig ? this.convertDataToString(oldAttr.additionData) : null],
      attrRequired: [oldAttr.required, Validators.required],
      attrOrder: [this.getOrderAttrId(oldAttr.order)]
    });

  }

  ngOnInit(): void {
    this.attributeFormGroup.controls.attrType.valueChanges.subscribe((value) => {
      this.hasConfig = this.hasAdditionalConfig(value);
      if (this.hasConfig) {
        this.additionDataLabel = 'Các lựa chọn';
        this.additionalDataDes = 'Các lựa chọn cách nhau bởi dấu ","';
        if (value == 8) {
          this.additionDataLabel = 'Các tên cột';
          this.additionalDataDes = 'Các tên cột cách nhau bởi dấu ","';
        }
      }
    });
  }

  private hasAdditionalConfig(attrType: number): boolean {
    return attrType == 6 || attrType == 7 || attrType == 8;
  }

  closeForm() {
    this.formSubmitted = true;
    if (this.attributeFormGroup.invalid) {
      if (this.attributeFormGroup.controls.additionalData.invalid && this.hasConfig) {
        return;
      }
    }
    if (this.hasConfig) {
      if (!this.attributeFormGroup.controls.additionalData) {
        this.errorMessage = 'Yêu cầu thêm các tùy chọn';
      }
    }

    let payload = {...this.data.group};
    let orderTarget = this.otherAttributes.length;
    if (this.otherAttributes.length > 1 && this.attributeFormGroup.controls.attrOrder.value) {
      let orderId = this.attributeFormGroup.controls.attrOrder.value;
      orderTarget = this.otherAttributes.find(attr => {
        return orderId == attr.id
      }).order;
      payload.attrs = util.reorder(this.data.group.orderedAttrs, 'order', this.order, orderTarget);
    }

    if (this.data.isEdit) {
      let targetAttr = payload.attrs.find((attribute: Attribute) => {
        return this.data.attribute.id == attribute.id;
      });

      if (targetAttr) {
        targetAttr.attrType = this.attributeFormGroup.value.attrType;
        targetAttr.name = this.attributeFormGroup.value.attrName;
        targetAttr.description = this.attributeFormGroup.value.description;
        targetAttr.required = this.attributeFormGroup.value.attrRequired;

        if (this.hasConfig) {
          let data = this.convertStringToData(this.attributeFormGroup.value.additionalData);
          targetAttr.additionData = JSON.stringify(data);
        } else {
          targetAttr.additionData = null;
        }
      }
    } else {
      let attribute: Attribute = {
        attrType: this.attributeFormGroup.value.attrType,
        name: this.attributeFormGroup.value.attrName,
        description: this.attributeFormGroup.value.description,
        fieldName: '',
        required: this.attributeFormGroup.value.attrRequired,
        order: this.otherAttributes.length > 1 ? orderTarget + 1 : this.otherAttributes.length
      };
      if (this.hasConfig) {
        let data = this.convertStringToData(this.attributeFormGroup.value.additionalData);
        attribute.additionData = JSON.stringify(data);
      }
      payload.attrs.push(attribute);
    }

    payload.attrs = payload.attrs.sort((a, b) => a.order > b.order ? 1 : -1);
    payload.attrs.forEach((attr: Attribute, index: number) => {
      attr.order = index;
    });
    console.log(payload.attrs);
    this.dialogRef.close({
      status: 'update',
      payload: payload
    });
  }

  private getOrderAttrId(value: number): number {
    if (value == null) {
      return this.otherAttributes.length > 0 ?this.otherAttributes[this.otherAttributes.length - 1].id : null;
    }

    if (this.data.group.attrs && this.data.group.attrs.length > 0) {
      if (value) {
        return this.otherAttributes.find(attr => {
          return attr.order == value - 1;
        }).id;
      }
    } 

    return null;
  }

  private convertDataToString(jsonString: string) {
    if (jsonString != null) {
      let jsonObject = JSON.parse(jsonString);
      let values = jsonObject.values.map(opt => opt.name).join();
      return values;
    }
    return null;

  }

  private convertStringToData(dataText: string) {
    let optionValues = dataText.split(',');
    let values = [];
    optionValues.forEach((value, index) => {
      values.push({
        name: value.trim(),
        value: index + 1
      });
    });

    let options = {
      values: values,
      default: values[0].value
    };

    return options;
  }

  getNameErrorMessage(): string {
    return '';
  }
}

interface MyAttrType extends AttributeType {
  friendlyName: string
}
