import {Component, OnInit, Inject} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RequestGroupForward } from 'src/generated/web-api';

@Component({
    templateUrl: './forward-group-edit-form.component.html',
    styleUrls: ['./forward-group-edit-form.component.scss']
})
export class ForwardGroupEditForm {

    forwardWhens: any[] = []

    group: RequestGroupForward;

    statusForm: FormControl;

    constructor(
        public dialogRef: MatDialogRef<ForwardGroupEditForm>,
        @Inject(MAT_DIALOG_DATA) public data: any) 
        {
            this.group = data.group;
            this.forwardWhens = Object.values(RequestGroupForward.StatusEnum);
            this.statusForm = new FormControl(this.group.status);
        }
    
    onSave() {
        let payload = {...this.group};
        payload.status = this.statusForm.value;
        this.dialogRef.close(payload);
    }
}