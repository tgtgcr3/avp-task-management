import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl, FormGroup} from "@angular/forms";
import * as cst from 'src/app/core/common/one-const';

@Component({
  templateUrl: './export-request-dialog.component.html',
  styleUrls: ['./export-request-dialog.component.scss'],
  selector: 'status-confirm-dialog'
})
export class ExportRequestDialogComponent {

  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });

  private _oneMonth = 2678400000;

  constructor(
    private dialogRef: MatDialogRef<ExportRequestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private dialogData: any
  ) {

  }

  onSend() {
    if (!(this.range.value.start && this.range.value.end)) {
      this.range.controls.start.setErrors({matStartDateInvalid: true});
      this.range.controls.end.setErrors({matEndDateInvalid: true});
      return;
    }
    if (this.range.value.end - this.range.value.start > 3 * this._oneMonth) {
      this.range.controls.start.setErrors({outOfRange: true});
      this.range.controls.end.setErrors({});
      return;
    }
    this.dialogRef.close({
      status: cst.SAVE_STATUS.OK,
      data: {
        start: this.range.value.start,
        end: this.range.value.end
      }
    });
  }


}
