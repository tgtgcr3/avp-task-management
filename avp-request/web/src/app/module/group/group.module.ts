import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ComponentModule } from '../../core/component/component.module';
import { GroupComponent } from "./group.component";
import {
  GroupDetailComponent,
  GroupFormComponent,
  GroupHeaderComponent,
  GroupLiComponent,
  GroupDetailSidebarComponent,
  ForwardGroupEditForm
} from "./component";
import { AttributeFormDialog } from "./component/group/detail/attribute-form/attribute-form-dialog.component";
import { AttributeKeyEditForm } from "./component/group/detail/attribute-key-edit-form/attribute-key-edit-form.component";
import { TranslateModule } from "@ngx-translate/core";
import {ExportRequestDialogComponent} from "./component/export-request-dialog/export-request-dialog.component";
import {MatDatepickerModule} from "@angular/material/datepicker";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatChipsModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatInputModule,
    MatDividerModule,
    MatMenuModule,
    ComponentModule,
    MatProgressSpinnerModule,
    TranslateModule,
    MatDatepickerModule
  ],
  declarations: [
    GroupComponent,
    GroupLiComponent,
    GroupHeaderComponent,
    GroupDetailComponent,
    GroupFormComponent,
    GroupDetailSidebarComponent,
    AttributeFormDialog,
    AttributeKeyEditForm,
    ForwardGroupEditForm,
    ExportRequestDialogComponent
  ],
  exports: [
    GroupFormComponent
  ]
})
export class GroupModule { }

