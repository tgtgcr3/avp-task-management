import {Component, OnDestroy, OnInit} from '@angular/core';
import {forkJoin, Subscription} from 'rxjs';
import {KeyCloakService} from 'src/app/core/service/keycloak.service';
import {OneLoaderService} from 'src/app/core/service/one-loader.service';
import {OneRouteService} from 'src/app/core/service/one-route.service';
import {RequestApprovalTypeWrapperService} from 'src/app/core/service/request-approval-type.service';
import {RequestGroupWrapperService} from 'src/app/core/service/request-group-wrapper.service';
import {KeyCloakUserService} from 'src/generated/account-api';
import {RequestApprovalType, RequestGroup} from 'src/generated/web-api';
import * as util from '../../core/common/one-util';
import {ActivatedRoute} from "@angular/router";

@Component({
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss'],
  selector: 'group'
})
export class GroupComponent implements OnInit, OnDestroy {

  public groups: RequestGroup[] = [];
  public grouppedGroups: any[] = [];
  public allUsers: any[] = [];
  public approvalTypes: RequestApprovalType[] = [];
  public isLoading: boolean = true;

  private refreshSubscription: Subscription;

  private sub: Subscription;

  private status?: boolean = null;

  constructor(private requestGroupWrapperService: RequestGroupWrapperService,
              private requestApprovalTypeWrapperService: RequestApprovalTypeWrapperService,
              private oneRoute: OneRouteService,
              private keycloakService: KeyCloakService,
              private keycloakUserService: KeyCloakUserService,
              private route: ActivatedRoute,
              private oneLoader: OneLoaderService) {
    this.refreshSubscription = this.requestGroupWrapperService.refresh.subscribe(this.onRefresh.bind(this));

    this.sub = this.route.queryParams.subscribe(params => {
      let status = params['status'];
      this.initData(status == null || status == '' ? null : status == 'ENABLED');
     // this.status = status == null || status == '' ? null : status == 'ENABLED';
    });
  }

  get total() {
    return this.groups.length;
  }

  ngOnDestroy() {
    this.refreshSubscription.unsubscribe();
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  ngOnInit() {
     //this.initData();
  }

  initData(status?: boolean) {
    this.isLoading = true;
    this.oneLoader.show();
    this.status = status;
    forkJoin([
      this.keycloakUserService.getUsers(),
      this.requestGroupWrapperService.getAll(),
      this.requestApprovalTypeWrapperService.getAll()
    ]).toPromise().then(
      (results: any[]) => {
        this.allUsers = results[0].items.map(user => {
          return {
            ...user,
            avatarUrl: this.keycloakService.getAvatar(user),
            fullName: this.keycloakService.getFullName(user),
            username: this.keycloakService.getUsername(user)
          }
        });

        let noneDirectGroups = results[1].filter((group: RequestGroup) => {
          return group.oId != 0;
        });
        this.grouppedGroups = this.groupByCategories(noneDirectGroups);
        this.groups = noneDirectGroups;
        this.approvalTypes = results[2];
      }
    ).finally(() => {
      this.isLoading = false;
      this.oneLoader.hide();
    })
  }

  onRefresh() {
    // this.oneRoute.navigateTo(['/groups']).then(() => {
    //   window.location.reload();
    // });
    this.oneRoute.reload();
  }

  onFilter(status: string) {
    // let filteredGroups = status == '' ? [...this.groups] : this.groups.filter((group: RequestGroup) => {
    //   return group.status == status;
    // });
    // this.grouppedGroups = this.groupByCategories(filteredGroups);
  }

  private groupByCategories(groups: RequestGroup[]) {
    let status = this.status == null ? null : this.status == true ? RequestGroup.StatusEnum.ENABLED : RequestGroup.StatusEnum.DISABLED;
    groups = groups.filter(g => {
      return this.status == null || g.status == status
    });
    let emptyCategoryGroups = groups.filter((group: RequestGroup) => {
      return group.category == null || group.category == "";
    }).sort((a, b) => (a.name > b.name) ? 1 : -1);

    let otherGroups = groups.filter((group: RequestGroup) => {
      return group.category != null && group.category != "";
    })
    let grouppedResult = util.groupBy(otherGroups, 'category');
    let grouppedAsList = Object.keys(grouppedResult).map(function (categoryName: any) {
      let groups = grouppedResult[categoryName].sort((a, b) => (a.name > b.name) ? 1 : -1);
      return {category: categoryName, groups: groups, isExpanded: true};
    });

    let sortedGroupList = grouppedAsList.sort((a, b) => (a.category > b.category) ? 1 : -1);
    sortedGroupList.push({
      category: 'Khác',
      groups: emptyCategoryGroups,
      isExpanded: true
    });
    return sortedGroupList;
  }
}
