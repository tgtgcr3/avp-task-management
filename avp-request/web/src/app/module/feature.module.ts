import {NgModule} from "@angular/core";
import {CommonModule, DatePipe} from '@angular/common';
import {FeatureRoute, OnlyGrantedActivate, OnlyOwnerActivate} from "./feature.route";
import {MatButtonModule} from "@angular/material/button";
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
// import {PersonalModule} from "./personal/personal.module";
import {LandingPageComponent} from "./landingPage/landingPage.component";
import {PageNotFoundComponent} from "./notFound/page-not-found.component";
import {ManagementModule} from "./management/management.module";
// import { GuestMemberModule } from "./guestMember/guestMember.module";
import {RequestModule} from './request/request.module';
import {SearchModule} from './search/search.module';
import {ReportModule} from './report/report.module';
import {GroupModule} from './group/group.module';
import {OneRouteService} from '../core/service/one-route.service';
import {OneAlertService} from '../core/service/one-alert.service';
import {RequestGroupWrapperService} from '../core/service/request-group-wrapper.service';
import {RequestApprovalTypeWrapperService} from '../core/service/request-approval-type.service';
import {AttributeTypeWrapperService} from '../core/service/attribute-type.service';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {BrowserModule} from "@angular/platform-browser";
import {OneTaskService} from "../core/service/one-task.service";

@NgModule({
  imports: [
    CommonModule,
    FeatureRoute,
    // PersonalModule,
    // GuestMemberModule,
    ManagementModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatListModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatDialogModule,
    MatInputModule,
    MatSnackBarModule,
    RequestModule,
    SearchModule,
    ReportModule,
    GroupModule,
    MatProgressSpinnerModule
  ],
  providers: [
    OneRouteService,
    OneAlertService,
    RequestGroupWrapperService,
    RequestApprovalTypeWrapperService,
    AttributeTypeWrapperService,
    OnlyGrantedActivate,
    OnlyOwnerActivate,
    DatePipe,
    OneTaskService
  ],
  declarations: [
    LandingPageComponent,
    PageNotFoundComponent
  ],
  exports: [
  ]
})

export class FeatureModule {}
