import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, RouterModule, RouterStateSnapshot, Routes} from "@angular/router";
import {Injectable, NgModule} from "@angular/core";
import {LandingPageComponent} from "./landingPage/landingPage.component";
import {RequestComponent} from './request/request.component';
import {RequestDetailComponent} from './request/component';
import {SearchComponent} from './search/search.component';
import { ReportComponent } from './report/report.component';
import { GroupComponent } from './group/group.component';
import { GroupDetailComponent } from './group/component';
import { KeyCloakService } from "../core/service/keycloak.service";

@Injectable()
export class OnlyGrantedActivate implements CanActivate {
  constructor(private keyCloakService: KeyCloakService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Promise<boolean> {
    return new Promise((resolve) => {
      resolve(this.keyCloakService.isOwner() || this.keyCloakService.isAdmin());
    });
  }
}

@Injectable()
export class OnlyOwnerActivate implements CanActivate {
  constructor(private keyCloakService: KeyCloakService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Promise<boolean> {
    return new Promise((resolve) => {
      resolve(this.keyCloakService.isOwner());
    })
  }
}

const routes: Routes = [
  // {
  //   path: '', redirectTo: '/requests', pathMatch: 'full'
  // },
  {
    path: 'requests',
    component: LandingPageComponent,
    runGuardsAndResolvers: 'always',
    children: [
      {
        path: '',
        component: RequestComponent,
        children: [
          {
            path: ':id',
            component: RequestDetailComponent
          }
        ]
      }
    ]
  },
  {
    path: 'groups',
    component: LandingPageComponent,
    canActivate: [OnlyGrantedActivate],
    children: [
      {
        path: '',
        component: GroupComponent
      },
      {
        path: ':id',
        component: GroupDetailComponent
      }
    ]
  },
  {
    path: 'systemRequest',
    component: LandingPageComponent,
    canActivate: [OnlyOwnerActivate],
    children: [
      {
        path: '',
        component: RequestComponent,
        children: [
          {
            path: ':id',
            component: RequestDetailComponent
          }
        ]
      }
    ]
  },
  {
    path: 'report',
    component: ReportComponent,
    canActivate: [OnlyGrantedActivate]
  },
  {
    path: 'search',
    component: SearchComponent
  },
  // {
  //   path: '**',
  //   component: PageNotFoundComponent
  // }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})

export class FeatureRoute {}
