import { RocketChatService } from './../generated/rocket-api/api/rocketChat.service';
import { AvatarService } from './../generated/account-api/api/avatar.service';
import { KeyCloakUserService } from 'src/generated/account-api/api/keyCloakUser.service';
import { RequestGroupService } from './../generated/web-api/api/requestGroup.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CoreModule} from "./core/core.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClient, HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import { OAuthModule } from 'angular-oauth2-oidc';
import {SecurityService} from "./core/service/security.service";
import {FeatureModule} from "./module/feature.module";
import {MatInputModule} from "@angular/material/input";
import {ErrorStateMatcher, MAT_DATE_LOCALE, ShowOnDirtyErrorStateMatcher} from "@angular/material/core";
import {
  AttributeTypeService,
  BASE_PATH as webBasePath,
  DepartmentService,
  DiscussionService,
  FileService,
  ReportService,
  RequestApprovalTypeService,
  RequestGroupForwardService,
  RequestService
} from 'src/generated/web-api';
import { BASE_PATH as keycloakBasePath } from 'src/generated/keycloak-api';

import { environment } from 'src/environments/environment';
import { NgChatModule } from 'ng-chat';
import { AvatarModule } from 'ngx-avatar';
import { CookieService } from 'ngx-cookie-service';

import {HttpErrorInterceptor} from './interceptor/http-error.interceptor';
import {HttpPostInterceptor} from './interceptor/http-post.interceptor';
import {NotificationService} from "../generated/notification-api";
import {NGX_MAT_DATE_FORMATS, NgxMatDateFormats} from "@angular-material-components/datetime-picker";


function urlValidate(url: string): boolean {
  // if (url.indexOf('/notification/ap') == 0) {
  //   return false;
  // }
  if (url.indexOf("/auth") >= 0) {
    return true;
  }
  if (url.indexOf('/api/pri') >= 0) {
    return true;
  }
  return false;
}

const CUSTOM_DATE_FORMATS: NgxMatDateFormats = {
  parse: {
    dateInput: "l, LTS"
  },
  display: {
    dateInput: "DD/MM/YYYY HH:mm",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
  }
};



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    MatInputModule,
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    BrowserAnimationsModule,
    // translation
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    OAuthModule.forRoot({
      resourceServer: {
        //allowedUrls: ['http://localhost:4200/auth'],
        customUrlValidation: urlValidate,
        sendAccessToken: true
      }
    }),

    FeatureModule,
    NgChatModule,
    AvatarModule

  ],
  providers: [
    {
      provide: keycloakBasePath, useValue: environment.baseUrl
    },
    {
      provide: MAT_DATE_LOCALE, useValue: 'vi'
    },
    { provide: NGX_MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS },
    // {
    //   provide: webBasePath, useValue: '/api/pri'
    // },
    {
      provide: Window, useValue: window
    },
    CookieService,
    // { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
    // { provide: HTTP_INTERCEPTORS, useClass: HttpPostInterceptor, multi: true }
   // {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private securityService: SecurityService,
    private requestGroupService: RequestGroupService,
    private keycloakUserService: KeyCloakUserService,
    private avatarsService: AvatarService,
    private requestService: RequestService,
    private rocketService: RocketChatService,
    private requestApprovalTypeService: RequestApprovalTypeService,
    private departmentService: DepartmentService,
    private attributeTypeService: AttributeTypeService,
    private fileService: FileService,
    private notificationService: NotificationService,
    private reportService: ReportService,
    private discussionService: DiscussionService,
    private forwardRequestGroupService: RequestGroupForwardService
    ) {
    this.requestGroupService.configuration.basePath = this.requestGroupService.configuration.basePath.replace('http://localhost', '');
    this.keycloakUserService.configuration.basePath = this.keycloakUserService.configuration.basePath.replace('http://localhost', '');
    this.avatarsService.configuration.basePath = this.avatarsService.configuration.basePath.replace('http://localhost', '');
    this.rocketService.configuration.basePath = this.rocketService.configuration.basePath.replace('http://localhost', '');
    this.requestService.configuration.basePath = this.requestService.configuration.basePath.replace('http://localhost', '');
    this.requestApprovalTypeService.configuration.basePath = this.requestApprovalTypeService.configuration.basePath.replace('http://localhost', '');
    this.departmentService.configuration.basePath = this.departmentService.configuration.basePath.replace('http://localhost', '');
    this.attributeTypeService.configuration.basePath = this.attributeTypeService.configuration.basePath.replace('http://localhost', '');
    this.fileService.configuration.basePath = this.fileService.configuration.basePath.replace('http://localhost', '');
    this.notificationService.configuration.basePath = this.notificationService.configuration.basePath.replace('http://localhost', '');
    this.reportService.configuration.basePath = this.reportService.configuration.basePath.replace('http://localhost', '');
    this.discussionService.configuration.basePath = this.discussionService.configuration.basePath.replace('http://localhost', '');
    this.forwardRequestGroupService.configuration.basePath = this.forwardRequestGroupService.configuration.basePath.replace('http://localhost', '');
  }

}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
