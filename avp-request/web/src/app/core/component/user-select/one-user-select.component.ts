import {KeyCloakService} from 'src/app/core/service/keycloak.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import * as transliterate from '@sindresorhus/transliterate';
import {Observable, Subject, Subscription} from 'rxjs';
import {map, startWith, takeUntil} from 'rxjs/operators';

@Component({
  templateUrl: './one-user-select.component.html',
  styleUrls: ['./one-user-select.component.scss'],
  selector: 'one-user-select'
})
export class OneUserSelectComponent implements OnInit, OnDestroy {

  @Input()
  allUsers: any[] = [];

  @Input()
  label: string = ''

  @Input()
  oneControl: FormControl;

  @Input()
  fixedUserControl: FormControl;

  @Input()
  isDisabled = false;

  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  errorMessage: string = 'Người dùng không thể bỏ trống';
  userCtrl = new FormControl();
  filteredUsers: Observable<any[]>;
  selectedUsers: any[] = [];
  availableUsers: any[] = [];

  private sub: Subscription;
  private _onDestroy = new Subject<void>();

  @ViewChild('userInput') userInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(
    private keycloakService: KeyCloakService
  ) {
    this.filteredUsers = this.userCtrl.valueChanges.pipe(
      startWith(null),
      takeUntil(this._onDestroy),
      map((user: string | null) => user ? this._filter(user) : this.availableUsers.slice()));
  }

  ngOnInit(): void {
    if (this.oneControl) {
      this.sub = this.oneControl.valueChanges.subscribe((users: string[]) => {
        if (users) {
          this.selectedUsers = users.map(username => this.allUsers.find(user => user.username == username)).filter(user => { return user != undefined });
          this.availableUsers = this.allUsers.filter((user: any) => {
            return this.selectedUsers.find(u => u && u.username == user.username) == null;
          });
          this.availableUsers = this.availableUsers.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1 );
        }
      });

      if(this.oneControl.value) {
        let initUsers = this.oneControl.value.map((username: string) => {
          return this.allUsers.find(u => u.username == username);
        }).filter(u => u != null);

        this.selectedUsers = initUsers;
        this.availableUsers = this.allUsers.filter((user: any) => {
          return this.selectedUsers.find(u => u && u.username == user.username) == null;
        });
      }
      else {
        this.availableUsers = this.allUsers;
      }
    }

    this.availableUsers = this.availableUsers.sort((a, b) => (a.fullName > b.fullName) ? 1 : -1 );
  }

  ngOnDestroy() {
    if(this.sub){
      this.sub.unsubscribe();
      this.sub = null;
    }

    this._onDestroy.next();
    this._onDestroy.complete();
  }

  isFixed(user: any) {
    if(this.fixedUserControl && this.fixedUserControl.value) {
      return this.fixedUserControl.value.includes(user.username);
    }
    return false;
  }

  remove(user: any): void {
    const index = this.selectedUsers.indexOf(user);

    if (index >= 0) {
      this.selectedUsers.splice(index, 1);
      this.setFormControlValue();
      this.userCtrl.setValue(null);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.selectedUsers.push(event.option.value);
    this.userInput.nativeElement.value = '';
    this.setFormControlValue();
    this.userCtrl.setValue(null);
  }

  private setFormControlValue(): void {
    if (this.oneControl) {
      this.oneControl.setValue(this.selectedUsers.map(u => u.username));
    }
  }

  private _filter(value: string): string[] {
    if (value.toLowerCase) {
      const filterValue = transliterate(value.toLowerCase());
      return this.availableUsers.filter(user => transliterate(this.keycloakService.getFullName(user).toLowerCase()).indexOf(filterValue) >= 0);
    }
    return [];
  }
}
