import {Component} from '@angular/core';
import * as message from '../../common/one-message';

@Component({
    templateUrl: './one-loader.component.html',
    styles: ['.text { text-align: center; color: #fff; margin-top: 16px; }'],
    selector: 'one-loader'
})
export class OneLoaderComponent {
    get text() {
        return message;
    }
}