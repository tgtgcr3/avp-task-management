import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { RequestStatusBody } from 'src/generated/web-api';
import {KeyCloakService} from "../../service/keycloak.service";

@Component({
    templateUrl: './user-info-item.component.html',
    selector: 'user-info-item',
    styleUrls: ['user-info-item.component.scss']
})
export class UserInfoItemComponent implements OnInit {

    @Input() showOption: boolean = false;
    @Input() showApprovalStatus: boolean = false;
    @Input() user: any = {};
    @Output() clicked: EventEmitter<any> = new EventEmitter<any>();
    @Output() remove: EventEmitter<any> = new EventEmitter<any>();

    get avatar() {
        return this.user.attributes.avp_avatar ? this.user.attributes.avp_avatar[0] : '';
    }

    get title() {
        return this.keycloakService.getTitle(this.user);
    }

    get approvalStatusIcon() {
        return this.user.approvalStatus == RequestStatusBody.StatusEnum.APPROVED ? 'check_circle' : 'cancel';
    }

    constructor(
      private keycloakService: KeyCloakService
    ) { }

    ngOnInit() {

    }

    onClicked() {
        this.clicked.emit(this.user);
    }

    onRemove() {
        this.remove.emit(this.user);
    }
}
