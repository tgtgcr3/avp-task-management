import {Component, Inject} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Department } from 'src/generated/web-api';
import * as cst from '../../common/one-const';

@Component({
    templateUrl: './department-select.component.html',
    selector: 'department-select',
    styleUrls: ['./department-select.component.scss']
})
export class DepartmentSelect {

    filteredDepartments: Department[] = [];
    departmentCtrl = new FormControl('');
    company: any = {
        id: 0,
        name: 'Toàn công ty'
    };

    private departments: Department[] = [];

    constructor(public dialogRef: MatDialogRef<DepartmentSelect>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
            this.departments = this.data.departments;
            this.filteredDepartments = this.data.departments;
            this.departmentCtrl.valueChanges.subscribe(
                (response: any) => {
                  if(typeof response === "string") {
                    this.filteredDepartments = response ? this._filter(response) : this.departments.slice();
                  }
                }
              );
        }

    onSelect(department: Department) {
        this.dialogRef.close({
            status: cst.SAVE_STATUS.OK,
            department: department
        });
    }

    private _filter(value: any) {
        const filterValue = value.toLowerCase();
        return this.departments.filter(user => user.name.toLowerCase().indexOf(filterValue) >= 0 || user.name.toLowerCase().indexOf(filterValue) >= 0);
    }
}