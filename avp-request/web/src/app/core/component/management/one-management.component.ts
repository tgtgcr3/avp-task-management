import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { environment } from './../../../../environments/environment';
import { Component, Input, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OneManagementDialog } from './dialog/one-management-dialog.component';

@Component({
  templateUrl: './one-management.component.html',
  styleUrls: ['./one-management.component.scss'],
  selector: 'one-management'
})
export class OneManagementComponent {

  @Input()
  userInfo: any;

  userAvartarUrl: string;
  userFullname: string;
  userTitle: string;

  constructor(public dialog: MatDialog,
    private keycloakService: KeyCloakService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.userInfo && this.userInfo) {
      this.userAvartarUrl = this.userInfo.avatar || environment.defaultAvatar;
      this.userFullname = this.keycloakService.getFullName(this.userInfo);
      this.userTitle = this.userInfo.userTitle;
    }
  }

  openDialog() {
    const dialogRef = this.dialog.open(OneManagementDialog, {
      width: '635px',
      panelClass: 'one-notification-dialog',
      hasBackdrop: true,
      data: {
        userInfo: this.userInfo
      }
    });
  }
}
