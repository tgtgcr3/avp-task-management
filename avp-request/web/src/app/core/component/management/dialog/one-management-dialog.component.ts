import { SecurityService } from './../../../service/security.service';
import { KeyCloakUserService } from 'src/generated/account-api/api/keyCloakUser.service';
import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';

@Component({
  templateUrl: './one-management-dialog.component.html',
  styleUrls: ['./one-management-dialog.component.scss'],
  selector: 'one-management-dialog'
})
export class OneManagementDialog {

  userInfo: any;
  userAvartarUrl: string;
  userFullname: string;

  constructor(
    public dialogRef: MatDialogRef<OneManagementDialog>,
    private keycloakService: KeyCloakService,
    private securityService: SecurityService,
    private keycloakUserService: KeyCloakUserService,
    @Inject(MAT_DIALOG_DATA) private dialogData: any
  ) {
    this.userInfo = this.dialogData.userInfo;
    this.userAvartarUrl = this.userInfo.avatar || environment.defaultAvatar;
    this.userFullname = this.keycloakService.getFullName(this.userInfo);
  }

  onClose() {
    this.dialogRef.close();
  }

  logout() {
    try {
      this.keycloakUserService.logout({
        token: this.keycloakService.getUsername(this.userInfo)
      });
    } catch(ex) {

    }
    setTimeout(() => {
      this.securityService.logout();
    });
  }
}
