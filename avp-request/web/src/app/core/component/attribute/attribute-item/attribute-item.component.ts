import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { AttributeTypeWrapperService } from 'src/app/core/service/attribute-type.service';
import { AttributeType, RequestGroup } from 'src/generated/web-api';
import {Attribute} from '../../../../../generated/web-api/model/attribute';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AttributeFormDialog } from 'src/app/module/group/component/group/detail/attribute-form/attribute-form-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { AttributeKeyEditForm } from 'src/app/module/group/component/group/detail/attribute-key-edit-form/attribute-key-edit-form.component';
import { ConfirmDialog } from '../../confirm-dialog/confirm-dialog.component';
import * as cst from 'src/app/core/common/one-const';

@Component({
    templateUrl: './attribute-item.component.html',
    selector: 'attribute-item',
    styleUrls: ['./attribute-item.component.scss']
})
export class AttributeItemComponent implements OnInit {

    @Input() attribute: Attribute = {};
    @Input() group: RequestGroup = {};
    @Output() attributeChanged: EventEmitter<any> = new EventEmitter<any>();
    @Output() dataChanged: EventEmitter<any> = new EventEmitter<any>();

    private attributeTypes: AttributeType[] = []

    isRequired: boolean = false;
    type: string = '';
    hasRequired = true;

    get isRequiredText() {
        return this.isRequired ? '* required' : 'not required'
    }

    get attrLength() {
        return this.group.attrs?.length;
    }

    constructor(public dialog: MatDialog,
        private attributeTypeService: AttributeTypeWrapperService) { }

    ngOnInit() {
        this.loadAttributeTypes();
        this.hasRequired = this.attribute.attrType != 9;
    }

    onRequiredChange(event: MatSlideToggleChange) {
        let payload = {...this.group};
        this.attribute.required = event.checked;

        let index = payload.attrs.findIndex((attribute: Attribute) => {
            return this.attribute.id == attribute.id;
        });

        if(index >= 0) {
            payload.attrs[index] = this.attribute;
        }

        this.attributeChanged.emit({
            status: 'update',
            payload: payload
        });
    }

    openAttributeForm() {
      this.attributeTypeService.getAll().then((attrTypes: AttributeType[]) => {
        const dialogRef = this.dialog.open(AttributeFormDialog,
          {
            data: {
              group: {...this.group},
              isEdit: true,
              attrTypes: attrTypes,
              attribute: {...this.attribute}
            },
            width: '500px',
            position: {
              top: '65px'
            },
            hasBackdrop: true,
            disableClose: true
          });

        dialogRef.beforeClosed().subscribe((result: any) => {
          this.dataChanged.emit(result);
        });
      });

    }

    openAttributeKeyForm() {
        const dialogRef = this.dialog.open(AttributeKeyEditForm,
            {
                data: {
                    group: {...this.group},
                    isEdit: true,
                    attribute: {...this.attribute}
                },
                width: '500px',
                position: {
                    top: '65px'
                },
                hasBackdrop: true,
                disableClose: true
            });

        dialogRef.beforeClosed().subscribe(this.onChange.bind(this));
    }

    deleteConfirm() {
        const dialogRef = this.dialog.open(ConfirmDialog,
            {
                data: 'Bạn có chắc mình muốn xóa thuộc tính này?',
                hasBackdrop: true,
                disableClose: true
            });

        dialogRef.afterClosed().subscribe((result: any) => {
            if (result.status == cst.SAVE_STATUS.OK) {

                let payload = {...this.group};
                let index = payload.attrs.indexOf(this.attribute);
                payload.attrs.splice(index, 1);

                payload.attrs.forEach((attr, index) => {
                    attr.order = index;
                });

                let event = {
                    status: 'update',
                    payload: payload
                };

                this.onChange(event);
            }
        })
    }

    up() {
        let index = this.group.attrs.indexOf(this.attribute);

        let temp = this.group.attrs[index - 1].order;
        this.group.attrs[index - 1].order = this.group.attrs[index].order;
        this.group.attrs[index].order = temp;

        let event = {
            status: 'update',
            payload: {...this.group}
        }
        this.onChange(event);
    }

    down() {
        let index = this.group.attrs.indexOf(this.attribute);

        let temp = this.group.attrs[index + 1].order;
        this.group.attrs[index + 1].order = this.group.attrs[index].order;
        this.group.attrs[index].order = temp;

        let event = {
            status: 'update',
            payload: {...this.group}
        }
        this.onChange(event);
    }

    private loadAttributeTypes() {
        this.attributeTypeService.getAll().then(
            (response: AttributeType[]) => {
                this.attributeTypes = response;
                this.initData();
            }
        );
    }

    private initData() {
        this.isRequired = this.attribute.required ? true : false;
        let attributeType = this.attributeTypes.find((item: AttributeType) => {
            return this.attribute.attrType == item.id;
        });

        this.type = attributeType.name;
    }

    private onChange(result: any) {
        if(result.status){
            this.attributeChanged.emit(result);
        }
    }
}
