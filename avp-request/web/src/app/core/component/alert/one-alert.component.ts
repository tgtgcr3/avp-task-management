import { Inject, Component } from "@angular/core";
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
    template: `
        <div>
            <mat-icon class="icon" [ngClass]="icon == 'done' ? 'success' : 'error' ">{{icon}}</mat-icon>
            <span class="message">{{data.message}}</span>
        </div>
    `,
    styleUrls: ['./one-alert.component.scss'],
    selector: 'one-alert-content'
})
export class OneAlertComponent {
    constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {}

    public get icon() {
        if(this.data.type == 'success') {
            return 'done';
        }

        return 'info';
    }
}