import { AvatarModule } from 'ngx-avatar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from "@angular/core";
import {AvatarComponent} from "./avatar/avatar.component";
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import {MatChipsModule} from '@angular/material/chips';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCardModule} from '@angular/material/card';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatButtonModule} from '@angular/material/button';
import {NotificationComponent, NotificationDialog} from "./notification/index";
import {OneManagementComponent, OneManagementDialog} from "./management/index";
import {OneAlertComponent} from "./alert/one-alert.component";
import {OneUserSelectComponent} from "./user-select/one-user-select.component";
import {OneLoaderComponent} from "./loader/one-loader.component";
import {AttributeItemComponent} from "./attribute/attribute-item/attribute-item.component";
import {UserInfoItemComponent} from "./user-info/user-info-item.component";
import {UserMenuComponent} from "./user-menu/user-menu.component";
import {GroupSelectDialog} from "./group-select/group-select-dialog.component";
import {ConfirmDialog} from "./confirm-dialog/confirm-dialog.component";
import {DepartmentSelect} from "./department-select/department-select.component";

@NgModule({
  imports:[
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AvatarModule,
    MatIconModule,
    MatBadgeModule,
    MatTooltipModule,
    MatDialogModule,
    MatChipsModule,
    MatSelectModule,
    MatInputModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatSlideToggleModule,
    MatButtonModule
  ],
  declarations: [
    AvatarComponent,
    NotificationComponent,
    NotificationDialog,
    OneManagementComponent,
    OneManagementDialog,
    OneAlertComponent,
    OneUserSelectComponent,
    OneLoaderComponent,
    AttributeItemComponent,
    UserInfoItemComponent,
    UserMenuComponent, 
    GroupSelectDialog,
    ConfirmDialog,
    DepartmentSelect
  ],
  exports: [
    AvatarComponent,
    NotificationComponent,
    NotificationDialog,
    OneManagementComponent,
    OneManagementDialog,
    OneAlertComponent,
    OneUserSelectComponent,
    OneLoaderComponent,
    AttributeItemComponent,
    UserInfoItemComponent,
    UserMenuComponent,
    GroupSelectDialog,
    ConfirmDialog,
    DepartmentSelect
  ]
})

export class ComponentModule {}
