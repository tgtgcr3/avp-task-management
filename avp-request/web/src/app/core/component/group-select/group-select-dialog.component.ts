import {Component, Inject, OnInit} from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RequestGroup } from 'src/generated/web-api';

@Component({
    templateUrl: './group-select-dialog.component.html',
    styleUrls: ['./group-select-dialog.component.scss'],
    selector: 'group-select-dialog'
})
export class GroupSelectDialog {
    
    requestGroups: RequestGroup[] = [];
    displayingGroups: RequestGroup[] = [];

    constructor(public dialogRef: MatDialogRef<GroupSelectDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
            this.requestGroups = data.groups;
            this.displayingGroups = [...this.requestGroups]; 
         }
    

    onSearch(event: any) {
        this.displayingGroups = this.requestGroups.filter((group: RequestGroup) => {
            return group.name.toLowerCase().includes(event.target.value);
        })
    }
    
    onSelect(group: RequestGroup) {
        this.dialogRef.close(group);
    }
}