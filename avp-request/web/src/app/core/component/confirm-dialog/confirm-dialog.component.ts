import {Component, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as cst from "../../common/one-const";

@Component({
    templateUrl: './confirm-dialog.component.html',
    selector: 'confirm-dialog',
    styleUrls: ['confirm-dialog.component.scss']
})
export class ConfirmDialog {

    constructor(
        public dialogRef: MatDialogRef<ConfirmDialog>,
        @Inject(MAT_DIALOG_DATA) public message: string) { }

    onOk() {
        this.dialogRef.close({
            status: cst.SAVE_STATUS.OK
        });
    }
}