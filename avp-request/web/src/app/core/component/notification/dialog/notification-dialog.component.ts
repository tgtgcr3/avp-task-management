import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { environment } from 'src/environments/environment';
import { KeyCloakUserService } from 'src/generated/account-api';
import { Notification, NotificationService } from 'src/generated/notification-api';
import { NotificationQueryResult } from 'src/generated/web-api';

@Component({
    templateUrl: './notification-dialog.component.html',
    styleUrls: ['./notification-dialog.component.scss'],
    selector: 'nofitication-dialog'
})
export class NotificationDialog implements OnInit {

    public notificationItems: OneNotification[] = [];
    public loading: boolean = true;

    private pageInfo: any = {
        size: 15,
        pageIndex: 0
    }
    private users: any[] = [];
    
    constructor(
        public dialogRef: MatDialogRef<NotificationDialog>,
        private notificationService: NotificationService,
        private keycloakUserService: KeyCloakUserService,
        private keycloakService: KeyCloakService
    ) {}

    onClose() {
        this.dialogRef.close();
    }

    ngOnInit() {
        this.loading = true;
        forkJoin([
            this.keycloakUserService.getUsers(null, null, null, null, null, 0, 1024),
            this.notificationService.getAllNotifications(environment.tenantId, environment.appId, "All", this.pageInfo.pageIndex, this.pageInfo.size)
        ]).toPromise().then(
            (results: [any, NotificationQueryResult]) => {
                this.users = results[0].items.map(user => {
                    return {
                        ...user,
                        fullName: this.keycloakService.getFullName(user),
                        avatarUrl: this.keycloakService.getAvatar(user)
                    }
                });

                this.notificationItems = results[1].items.map(notify => {
                    return this.parseToOneNotify(notify);
                });
                
                this.fireReadNotification();
            }
        )
    }

    loadMore() {
        this.pageInfo.size += 10;
        this.loadNotifications();
    }

    navigateTo(notify: OneNotification) {
        window.location.href = notify.url;
    }

    private loadNotifications() {
        this.notificationService.getAllNotifications(environment.tenantId, environment.appId, "All", this.pageInfo.pageIndex, this.pageInfo.size).toPromise()
        .then((response: NotificationQueryResult) => {
            this.notificationItems = response.items.map(notify => {
                return this.parseToOneNotify(notify);
            });
            this.fireReadNotification();
        })
    }

    private fireReadNotification() {
        let unreadNotis = this.notificationItems.filter((noti: OneNotification) => {
            return noti.notificationStatus == 'Unread';
        });

        if(unreadNotis.length > 0) {
            let ids: number[] = unreadNotis.map(noti => noti.id);
    
            this.notificationService.setAllNotificationStatus(environment.tenantId, environment.appId, ids, 'Read').toPromise().then(
                (result: any) => {
                    
                }
            );
        }
    }

    private parseToOneNotify(notify: Notification) : OneNotification {
        let shortContent = notify.shortContent;
        let parsedContent:any = {};
        let author:any = {};
        try {
            parsedContent = JSON.parse(shortContent);
            author = this.extractUserViaContent(parsedContent);
        }
        catch(ex) {
            parsedContent = {
                url: '/requests',
                content: 'On Error'
            };
        }
        

        let oneNotification: OneNotification = {
            ...notify,
            url: parsedContent.url,
            author: author,
            content: parsedContent.content
        };
        
        return oneNotification;
    }

    private extractUserViaContent(contentObj: any) {
        let startIndex = contentObj.content.indexOf('[');
        let endIndex = contentObj.content.indexOf(']');
        if( startIndex > -1 && endIndex > -1 && endIndex > startIndex) {
            let username = contentObj.content.substr(startIndex + 1, endIndex - startIndex - 1);
            if(username) {
                let user = this.users.find(user => user.username == username);
                contentObj.content = contentObj.content.replace(`[${username}]`, `<b>${user.fullName}</b>`);
                return user;
            }
        }
        return {
            avatarUrl: '/assets/images/request.png'
        }
    }
}

interface OneNotification extends Notification {
    author: any;
    url: string;
    content: string;
}
  