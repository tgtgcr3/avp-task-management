import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { Notification, NotificationService } from 'src/generated/notification-api';
import { NotificationQueryResult } from 'src/generated/notification-api/model/notificationQueryResult';
import { SseService } from '../../service/sse-service';
import {NotificationDialog} from './dialog/notification-dialog.component';

@Component({
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss'],
    selector: 'one-notification'
})
export class NotificationComponent implements OnInit {

    public notificationResult: NotificationQueryResult = {};
    public unreadNoti: any;
    constructor(public dialog: MatDialog,
        private sseService: SseService,
        private notificationService: NotificationService) {
        }

    openDialog() {
        const dialogRef = this.dialog.open(NotificationDialog, {
            panelClass: 'one-notification-dialog',
            hasBackdrop: true
        });

        dialogRef.afterClosed().subscribe((result: any) => {
            this.loadUnreadNotifications();
        })
    }

    ngOnInit() {
        this.loadUnreadNotifications();
        this.sseService.addEventListener(SseService.Event_Notification).subscribe(
            (event: any) => {
                this.handleEvent(event);
            }
        )
    }

    private handleEvent(event: any) {
        let data = JSON.parse(event.data);
        let notification: Notification = {
            id: data.id,
            shortContent: data.shortContent,
            title: data.title,
            date: data.date,
            notificationStatus: 'Unread'
        };
        
        this.notificationResult.items.push(notification);
        
        let count = this.notificationResult.items.length > 20 ? '20+' : this.notificationResult.items.length;
        this.unreadNoti = count == 0 ? null : count;
    }

    private loadUnreadNotifications() {
        this.notificationService.getAllNotifications(environment.tenantId, environment.appId, "Unread", 0, 21).toPromise()
        .then((response: NotificationQueryResult) => {
            this.notificationResult = response;
            let count = this.notificationResult.items.length > 20 ? '20+' : this.notificationResult.items.length;
            this.unreadNoti = count == 0 ? null : count;
        });
    }
}