import {Component, Inject, OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as transliterate from '@sindresorhus/transliterate';

@Component({
    templateUrl: './user-menu.component.html',
    selector: 'user-menu',
    styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent {

    filteredUsers: any[] = [];
    availableUsers: any[] = [];
    userCtrl = new FormControl('');

    get title() {
        return this.data.title;
    }

    private users: any[] = [];

    constructor(public dialogRef: MatDialogRef<UserMenuComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
            this.users = this.data.users;
            this.availableUsers = this.data.users.filter((user: any) => {
                return !this.data.selectedUsers.find((selected: any) => selected.username == user.username);
            }).sort((a, b) => (a.firstName > b.firstName) ? 1 : -1 );
            this.filteredUsers = this.availableUsers;
            this.userCtrl.valueChanges.subscribe(
                (response: any) => {
                  if(typeof response === "string") {
                    this.filteredUsers = response ? this._filter(response) : this.availableUsers.slice();
                  }
                }
              );
        }

    onUserSelected(user: any) {
        this.dialogRef.close({
            status: 'added',
            user: user
        });
    }

    private _filter(value: any) {
        const filterValue = transliterate(value.toLowerCase());

        return this.availableUsers.filter(user => transliterate(user.fullName.toLowerCase()).indexOf(filterValue) >= 0);
    }
}
