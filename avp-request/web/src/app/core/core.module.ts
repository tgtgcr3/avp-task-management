import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {NgModule} from "@angular/core";

import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import {LeftPanelComponent} from "./layout/leftPanel/leftPanel.component";
import {ComponentModule} from "./component/component.module";
import {PanelItemComponent} from "./layout/leftPanel/comp/panelItem/panelItem.component";
import {TranslateModule} from "@ngx-translate/core";
import {RightPanelComponent} from "./layout/rightPanel/rightPanel.component";
import {CenterPanelComponent} from "./layout/centerPanel/centerPanel.component";
import {TopActionBarComponent} from "./layout/topActionBar/top-action-bar.component";
import {MainPanelComponent} from "./layout/mainPanel/main-panel.component";
import {OneRouteService} from "./service/one-route.service";
import {
  ApiModule as KeyCloakApiModule
} from "../../generated/keycloak-api";

import {
  ApiModule as WebApiModule
} from "../../generated/web-api";
import {AppListComponent} from "./layout/leftPanel/comp/appList/appList.component";
import {CenterHeaderComponent} from "./layout/centerHeader/centerHeader.component";
import {RouterModule} from "@angular/router";
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    PanelItemComponent,
    LeftPanelComponent,
    RightPanelComponent,
    CenterPanelComponent,
    AppListComponent,
    CenterHeaderComponent,
    TopActionBarComponent,
    MainPanelComponent
  ],
  providers: [
    OneRouteService
  ],
  imports: [
    ComponentModule,
    TranslateModule,
    WebApiModule,
    KeyCloakApiModule,
    CommonModule,
    BrowserModule,
    RouterModule,
    MatInputModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatListModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatSelectModule
    //SecurityService
  ],
  exports: [
    LeftPanelComponent,
    RightPanelComponent,
    CenterPanelComponent,
    CenterHeaderComponent,
    TopActionBarComponent,
    MainPanelComponent
  ]
})

export class CoreModule {}
