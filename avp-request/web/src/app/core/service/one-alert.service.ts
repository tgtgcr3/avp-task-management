import { Injectable } from "@angular/core";
import { MatSnackBar } from '@angular/material/snack-bar';
import * as cst from '../common/one-const';
import * as ms from '../common/one-message';
import { OneAlertComponent } from "../component/alert/one-alert.component";

@Injectable({
    providedIn: 'root'
})
export class OneAlertService {

    constructor(private _snackBar: MatSnackBar) { }

    public onSuccess(message: string = ms.SAVE_SUCCESSED) {
        this.onShow(message, 'success');
    }

    public onError(error: any) {
      let message = error;
      if (error.message) {
        message = `Error: ${error.message}`;
      }
      this.onShow(message, 'error');
    }

    private onShow(message: string, type: cst.AlertType) {
        this._snackBar.openFromComponent(OneAlertComponent, {
            duration: cst.ALERT_DURATION * 1000,
            data: { message: message, type: type },
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: type
        });
    }
}


