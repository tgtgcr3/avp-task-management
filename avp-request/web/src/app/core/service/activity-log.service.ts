import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ActivityLogService {

    constructor(protected httpClient: HttpClient) {
    }

    createActivity(payload: any) {
        let url = '/account/api/pri/internal/activitylogs';
        return this.httpClient.post(url, payload);
    }
}