import { Inject, Injectable, Optional } from "@angular/core";
import {
    HttpClient, HttpHeaders, HttpParams,
    HttpResponse, HttpEvent, HttpParameterCodec
} from '@angular/common/http';
import { BASE_PATH, COLLECTION_FORMATS } from '../../../generated/web-api/variables';
import { environment } from "../../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class OneTaskService {

    private basePath = '';
    constructor(protected httpClient: HttpClient) {
        this.basePath = environment.oneTaskAPI;
    }

    createTask(creator: string, name: string, project_id: number, category_id?: number, assignee_email?: string, followers?: string[], startDate?: string, dueDate?: string) : Observable<any> {
        let requestUrl = this.basePath + '/create_work_packages?';
        requestUrl += `creator_email=${creator}&name=${name}&project_id=${project_id}`;

        if(assignee_email) requestUrl += `&assignee_email=${assignee_email}`;
        if(followers) {
            followers.forEach((email: string) => {
                requestUrl += `&followers[]=${email}`;
            });
        }
        if(category_id) requestUrl += `&category_id=${category_id}`;
        if(startDate) requestUrl += `&startDate=${startDate}`;
        if(dueDate) requestUrl += `&dueDate=${dueDate}`;

        return this.httpClient.get(requestUrl);
    }

    getTasks(ids: number[]) : Observable<any> {
        let requestUrl = this.basePath + `/work_packages?`;
        if(ids.length > 0) {
            ids.forEach((id: number) => {
                requestUrl += `ids[]=${id}&`;
            });
        }
        return this.httpClient.get(requestUrl);
    }

    getProjects(user: any) : Observable<any> {
        let requestUrl = this.basePath + `/projects?user_email=${user.email}`;
        return this.httpClient.get(requestUrl);
    }
}