import {Injectable, OnInit} from "@angular/core";
import {OAuthService} from "angular-oauth2-oidc";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class SecurityService  implements OnInit {

  constructor(
    private oauthService: OAuthService
  ) {
    this.oauthService.configure({
      redirectUri: environment.baseUrl,
      clientId: environment.clientId,
      scope: 'profile email offline_access',
      issuer: environment.idUrl + '/auth/realms/An-Viet-Phat',
      responseType: 'code',
      //this.oauthService.showDebugInformation = true;
      //this.oauthService.oidc = true;
      requireHttps: false,
      dummyClientSecret: environment.clientSecret,
      //this.oauthService.customTokenParameters = ['title'];
      silentRefreshRedirectUri:environment.baseUrl,
      tokenEndpoint: '/auth/realms/An-Viet-Phat/protocol/openid-connect/token'
    })


  }

  ngOnInit(): void {


  }

  prepare(redirectUrl?: string) {
    // let params = window.location.href.replace(environment.baseUrl, '');
    // if (params == '/' || params.includes('state=')) {
    //   this.oauthService.redirectUri = environment.baseUrl;
    // } else {
    let baseUrl = window.location.href;
    if (baseUrl.indexOf('?') > 0) {
      baseUrl = baseUrl.substr(0, baseUrl.indexOf('?'));
    }
      this.oauthService.redirectUri = baseUrl;
    // }

    this.oauthService.setupAutomaticSilentRefresh();
    return this.oauthService.loadDiscoveryDocument().then((r) => {
      this.oauthService.tokenEndpoint = '/auth/realms/An-Viet-Phat/protocol/openid-connect/token';
      return this.oauthService.tryLogin().then(r => {
        if (r == false) {
          this.oauthService.initLoginFlow();
        }
      }).catch((err: any) => {
        console.log(err);
      });
    });
    // return this.oauthService.loadDiscoveryDocumentAndTryLogin().then((r) => {

    //   //this.oauthService.initCodeFlow();
    //   //   this.oauthService.tryLogin();
    // });
  }

  getIssuer() {
    return this.oauthService.issuer;
  }

  isLogedIn(): boolean {
    return this.oauthService.hasValidAccessToken();
  }

  login() {
    // this.oauthService.loadDiscoveryDocumentAndTryLogin().then((r) => {
    //   this.oauthService.initCodeFlow();
    // });
    this.oauthService.initLoginFlow();
  }

  logout() {
    return this.oauthService.logOut();
  }

  getUserInfo(): any {
    let claims = this.oauthService.getIdentityClaims();
    if (claims == null) {
      return null;
    }
    return claims;
  }

  getAccessToken() {
    return this.oauthService.getAccessToken();
  }



}
