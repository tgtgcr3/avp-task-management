import {EventEmitter, Injectable} from '@angular/core';
import { RequestGroupService } from '../../../generated/web-api/api/requestGroup.service';
import { RequestGroup } from '../../../generated/web-api/model/requestGroup';
import { IRequestService } from './base/request.service';
import { IEvent } from './base/event.service';
import { Attribute, RequestGroupGetResult } from 'src/generated/web-api';
import { environment } from 'src/environments/environment';
import { OneAlertService } from './one-alert.service';

@Injectable({
    providedIn: 'root'
})
export class RequestGroupWrapperService implements IRequestService<RequestGroup>, IEvent {
    
    refresh: EventEmitter<any> = new EventEmitter<any>();
    
    constructor(
        private oneAlert: OneAlertService,
        private requestGroupService: RequestGroupService) { }

    create(payload: RequestGroup): Promise<RequestGroup> {
        return new Promise<RequestGroup>((resolve, reject) => {
            this.clearAttributeIds(payload);
            this.requestGroupService.createRequestGroup(environment.tenantId, payload)
                .toPromise()
                .then((requestGroup: RequestGroup) => {
                    this.oneAlert.onSuccess();
                    resolve(requestGroup);
                }, reject);
        }); 
    }

    import(file: Blob) : Promise<any> {
        return new Promise<Request>((resolve, reject) => {
            this.requestGroupService.importRequestGroup(environment.tenantId, file).toPromise()
                .then((response: any) => {
                    if(response.result == true) {
                        this.oneAlert.onSuccess();
                        resolve(response);
                    }
                    else {
                        this.oneAlert.onError({message: response.description})
                    }
                }, reject);
        })
    }

    update(id: number, payload: RequestGroup): Promise<RequestGroup> {
        return new Promise<RequestGroup>((resolve, reject) => {
            this.clearAttributeIds(payload);
            this.requestGroupService.updateRequestGroup(id, environment.tenantId, payload)
                .toPromise()
                .then((requestGroup: RequestGroup) => {
                    this.oneAlert.onSuccess();
                    resolve(requestGroup);
                }, reject);
        }); 
    }
    
    delete(id: number): Promise<RequestGroup> {
        throw new Error('Method not implemented.');
    }

    get(id: number): Promise<RequestGroup> {
        return this.requestGroupService.getLatestRequestGroup(Number(id), environment.tenantId).toPromise()
    }
    
    getAll(): Promise<RequestGroup[]> {
        return new Promise<RequestGroup[]>((resolve, reject) => {
            this.requestGroupService
                .getAllRequestGroups(environment.tenantId).toPromise()
                .then((requestGroupsResult: RequestGroupGetResult) => {
                    resolve(requestGroupsResult.items.filter((group: RequestGroup) => {
                        return group.status != RequestGroup.StatusEnum.DELETED;
                    }))
                }, reject);
        });
    }

    private clearAttributeIds(group: RequestGroup) {
        if(group.attrs) {
            group.attrs.forEach((att: Attribute) => {
                att.id = null;
            });
        }
    }
}