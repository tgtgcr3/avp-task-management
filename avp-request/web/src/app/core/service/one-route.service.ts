import { EventEmitter, Injectable } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd, ActivationEnd, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class OneRouteService {

    private _params: any = {};
    private _queryParams: any = {};
    private _currentRoute: string = '';

    public refreshComponentEvt: EventEmitter<any> = new EventEmitter();

    constructor(public route: ActivatedRoute,
        public router: Router) {

        router.events.subscribe((route) => {
            if(route instanceof NavigationEnd) {
                let params = this.collectRouteParams(router);
                this._params = params[0];
                this._queryParams = params[1];
                this._currentRoute = route.urlAfterRedirects;
            }
        });
    }

    collectRouteParams(router: Router) {
        let params = {};
        let queryParams = {};
        let stack: ActivatedRouteSnapshot[] = [router.routerState.snapshot.root];
        while (stack.length > 0) {
            const route = stack.pop()!;
            params = { ...params, ...route.params };
            queryParams = { ...queryParams, ...route.queryParams };
            stack.push(...route.children);
        }
        return [params, queryParams];
    }

    navigateTo(commands: any[], optionalParams: any = {}) {
        return this.router.navigate(commands, { relativeTo: this.route, queryParams: optionalParams });
    }

    reload() {
      let queryParams = this.queryParams || {};
      let one = queryParams.one;
      if (one != null) {
        delete queryParams.one;
      } else {
        queryParams.one = 1;
      }
      return this.navigateTo([
        this.pathWithoutOptionalParams
      ], queryParams);
    }

    refreshComponent() {
        this.refreshComponentEvt.emit();
    }

    get pathWithoutOptionalParams() {
        let path = this.router.url.split('?');
        return path[0];
    }

    get params() {
        return this._params;
    }

    get queryParams() {
        return this._queryParams;
    }

    get current() {
        return this._currentRoute;
    }
}
