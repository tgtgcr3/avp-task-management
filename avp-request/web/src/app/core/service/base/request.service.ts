export interface IRequestService<T> {
    create(payload: T) : Promise<T>;
    update(id: number, payload: T) : Promise<T>;
    delete(id: number) : Promise<T>;
    
    get(id: number) : Promise<T>;
    getAll() : Promise<T[]>;
}