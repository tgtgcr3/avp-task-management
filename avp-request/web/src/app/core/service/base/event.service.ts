import { EventEmitter } from "events";

export interface IEvent {
    refresh: EventEmitter;
}