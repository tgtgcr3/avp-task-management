import { Injectable } from '@angular/core';
import { RequestApprovalType } from '../../../generated/web-api/model/requestApprovalType';
import { RequestApprovalTypeGetAllResult } from '../../../generated/web-api/model/requestApprovalTypeGetAllResult';
import { RequestApprovalTypeService } from '../../../generated/web-api/api/requestApprovalType.service';
import { IRequestService } from './base/request.service';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class RequestApprovalTypeWrapperService implements IRequestService<RequestApprovalType> {
    
    private approvalTypes: RequestApprovalType[] | null = null;
    
    constructor(private requestApprovalTypeService: RequestApprovalTypeService) { }

    create(payload: RequestApprovalType): Promise<RequestApprovalType> {
        throw new Error('Method not implemented.');
    }

    update(id: number, payload: RequestApprovalType): Promise<RequestApprovalType> {
        throw new Error('Method not implemented.');
    }
    
    delete(id: number): Promise<RequestApprovalType> {
        throw new Error('Method not implemented.');
    }

    get(id: number, forceUpdate: boolean = false): Promise<RequestApprovalType> {
        throw new Error('Method not implemented.');
    }
    
    getAll(): Promise<RequestApprovalType[]> {
        return new Promise<RequestApprovalType[]>((resolve, reject) => {
            if(this.approvalTypes == null) {
                this.requestApprovalTypeService
                .getAllRequestApprovalTypes(environment.tenantId).toPromise()
                .then((response: RequestApprovalTypeGetAllResult) => {
                    this.approvalTypes = response.items;
                    resolve(response.items);
                }, reject);
            }
            else {
                resolve(this.approvalTypes);
            }
        });
    }
}