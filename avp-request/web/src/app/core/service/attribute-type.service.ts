import { Injectable } from '@angular/core';
import { AttributeType } from '../../../generated/web-api/model/attributeType';
import { AttributeTypeGetAllResult } from '../../../generated/web-api/model/attributeTypeGetAllResult';
import { AttributeTypeService } from '../../../generated/web-api/api/attributeType.service';
import { IRequestService } from './base/request.service';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AttributeTypeWrapperService implements IRequestService<AttributeType> {
    
    private attributeTypes: AttributeType[] | null = null;
    
    constructor(private AttributeTypeService: AttributeTypeService) { }

    create(payload: AttributeType): Promise<AttributeType> {
        throw new Error('Method not implemented.');
    }

    update(id: number, payload: AttributeType): Promise<AttributeType> {
        throw new Error('Method not implemented.');
    }
    
    delete(id: number): Promise<AttributeType> {
        throw new Error('Method not implemented.');
    }

    get(id: number, forceUpdate: boolean = false): Promise<AttributeType> {
        throw new Error('Method not implemented.');
    }
    
    getAll(): Promise<AttributeType[]> {
        return new Promise<AttributeType[]>((resolve, reject) => {
            if(this.attributeTypes == null) {
                this.AttributeTypeService
                .getAllAttributeTypes(environment.tenantId).toPromise()
                .then((response: AttributeTypeGetAllResult) => {
                    this.attributeTypes = response.items;
                    resolve(response.items);
                }, reject);
            }
            else {
                resolve(this.attributeTypes);
            }
        });
    }
}