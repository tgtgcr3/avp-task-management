import { Observable, Observer } from 'rxjs';
import { Injectable, NgZone, OnInit } from '@angular/core';

import {SSE} from 'sse.js';
import {SecurityService} from "./security.service";

@Injectable(
  {
    providedIn: 'root'
  }
)

export class SseService implements OnInit{

  eventSource: SSE;

  private listeners: Map<string, Observable<any>> = new Map<string, Observable<any>>();

  private observers: Map<string, Observer<any>> = new Map<string, Observer<any>>();

  private sseObservable: Observable<any>;

  public static readonly Event_Logout = "logout";

  public static readonly Event_Notification = "notification";

  constructor(private _zone: NgZone,
              private securityService: SecurityService
              ) {

  }
  ngOnInit(): void {
    //throw new Error('Method not implemented.');
  }

  addEventListener(event: string): Observable<any> {
    let listener = this.listeners[event];
    if (listener != null) {
      return listener;
    }
    listener = new Observable((observer: Observer<any>) => {
      this.observers[event] = observer;
    })
    this.listeners[event] = listener;
    return listener;
  }

  initServerSentEvent(url: string): Observable<any> {
    return this.sseObservable = new Observable((observer: Observer<any>) => {
      const eventSource = this.getEventSource(url);
      eventSource.addEventListener(SseService.Event_Notification, (e: any) => {
        this._zone.run(() => {
          let listener = this.observers[SseService.Event_Notification];
          if (listener) {
            listener.next(e);
          }
        });
      });
      eventSource.addEventListener(SseService.Event_Logout, (e: any) => {
        this._zone.run(() => {
          let listener = this.observers[SseService.Event_Logout];
          if (listener) {
            listener.next(e);
          }
        });
      });
      eventSource.stream();
      eventSource.onmessage = (event:any) => {
        console.log("onmessage");
        console.log(event);
      }

      eventSource.onerror = (error: any) => {
        this._zone.run(() => {
          observer.error(error);
        });
      }
      eventSource.onopen = (ev: any) => {
        console.log(ev);
      }
    });
  }

  private getEventSource(url: string): SSE {
    return new SSE(url, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${this.securityService.getAccessToken()}`
      }
    });
  }

}
