import { OneRequest } from "src/generated/web-api";

export const ALERT_DURATION = 3;
export type AlertType = 'success' | 'warn' | 'error';
export enum SAVE_STATUS { OK, Failed };
export interface DialogResult {
    status: SAVE_STATUS;
    body: any;
};
export const StatusFilters = [
    {
        value: '',
        name: 'Tất cả'
    },
    {
        value: OneRequest.StatusEnum.TIMEOUT,
        name: 'Quá hạn'
    },
    {
        value: OneRequest.StatusEnum.PENDING,
        name: 'Đang chờ'
    },
    {
        value: OneRequest.StatusEnum.APPROVED,
        name: 'Đã chấp thuận'
    },
    {
        value: OneRequest.StatusEnum.REJECTED,
        name: 'Đã từ chối'
    },
    {
        value: OneRequest.StatusEnum.CANCELLED,
        name: 'Đã hủy'
    },
    {
        value: OneRequest.StatusEnum.CLOSED,
        name: 'Đã đóng'
    }
];
export const StatusText = {
    PENDING: 'Chờ duyệt',
    APPROVED: 'Chấp thuận',
    TIMEOUT: 'Quá hạn',
    REJECTED: 'Từ chối',
    CANCELLED: 'Đã hủy',
    REQUEST_UPDATE: 'Cập nhật',
    CLOSED: 'Đã đóng'
}