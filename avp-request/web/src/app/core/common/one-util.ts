export function groupBy(list: any[], key: string) {
    return list.reduce((pv: any, cv: any[]) => {
        (pv[cv[key]] = pv[cv[key]] || []).push(cv);
        return pv;
    }, {});
}

export function capitalizeFirstLetter(string?: string) {
    if(string) {
        string = string.toLowerCase();
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    return null;
}

export function reorder(orderedList: any[], orderedKey: string, moveFromIndex?: number, moveToIndex?: number) {
    if(orderedList.length < 2) {
        return orderedList;
    }

    if(moveFromIndex == null) {
        for(let i = moveToIndex + 1; i < orderedList.length; i++) {
            orderedList[i][orderedKey] += 1;
        }
        return orderedList;
    }

    if(moveFromIndex < moveToIndex) {
        let movedItem = orderedList[moveFromIndex];
        let i = moveFromIndex;
        while(i < moveToIndex) {
            orderedList[i] = orderedList[i + 1];
            i++;
        }
        orderedList[moveToIndex] = movedItem;
    }
    else {
        let movedItem = orderedList[moveFromIndex];
        let i = moveFromIndex;
        while(i > moveToIndex + 1) {
            orderedList[i] = orderedList[i - 1];
            i--;
        }
        orderedList[moveToIndex + 1] = movedItem;
    }

    orderedList.forEach((item, index) => {
        item[orderedKey] = index;
    });

    return orderedList;
}