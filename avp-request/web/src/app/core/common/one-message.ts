export const TITLE : string = 'AVP Group';
export const REQUEST_SEARCH_TEXT : string = 'Tìm nhanh yêu cầu - đề xuất';
export const GROUP_SEARCH_TEXT: string = 'Tìm nhanh nhóm đề xuất';
export const HOME: string = 'Trang chủ';
export const CREATE_NEW_REQUEST_TEXT : string = 'Tạo đề xuất';
export const IMPORTANT_REQUEST_TITLE : string = 'Quan trọng';
export const DIRECT_REQUEST_TEXT : string = 'Đề xuất trực tiếp';
export const SEND_TO_ME : string = 'Gửi đến tôi';
export const MY_REQUESTS : string = 'Tôi gửi đi';
export const MY_FOLLOWING : string = 'Đang theo dõi';
export const SEARCH_TEXT : string = 'Tìm kiếm';
export const CREATE_NEW_GROUP: string = 'Thêm nhóm đề xuất mới';
export const SEARCH_RESULT_TEXT : string = 'Kết quả tìm kiếm';
export const EXPORT : string = 'Xuất file';
export const FILTER: any = {
    ALL: 'Tất cả',
    IN_TURN: 'Đến lượt',
    OVERDUE: 'Quá hạn',
    WAITING: 'Đang chờ',
    APPROVED: 'Đã duyệt',
    REJECTED: 'Đã từ chối',
    CANCELLED: 'Đã hủy'
}
export const GROUP_FILTER: any = {
    ALL: 'Danh sách',
    ACTIVE: 'Khả dụng',
    INACTIVE: 'Tạm đóng'
}
export const STATUS_FILTER: any = {
    TEXT: 'Trạng thái',
    ALL: 'Tất cả',
    DONE: 'Đã xử lý',
    IN_PROGRESS: 'Chưa xử lý',
    APPROVED: 'Đã duyệt',
    REJECTED: 'Từ chối',
    MARKED: 'Đánh dấu'
}
export const CREATED_TIME: string = 'Thời gian tạo';
export const REPORT: any = {
    OVERVIEW: 'Tổng quan',
    REQUEST: 'Thống kê request',
    GROUP: 'Thống kê theo nhóm',
    USER: 'Thống kê theo người dùng'
}
export const PAGE_NOT_FOUND: string = '404 Not Found: Không tìm thấy trang';
export const BACK_TO_HOME: string = 'Nhấn vào <a href="/">đây</a> để quay về trang chủ';
export const SAVE_SUCCESSED: string = 'Lưu thành công';
export const CANCEL: string = 'Hủy bỏ';
export const PROGRESSING: string = 'Đang xử lý...';
export const NO_DESCRIPTION: string = 'Chưa có mô tả';

/** Group **/
export const CREATE_GROUP_TITLE: string = 'THÊM NHÓM ĐỀ XUẤT MỚI';
export const EDIT_GROUP_TITLE: string = 'CHỈNH SỬA NHÓM ĐỀ XUẤT';
export const GROUP_NAME: string = 'Tên loại đề xuất';
export const DIRECT_MANAGER_APPROVAL: string = 'Yêu cầu thông báo tới người quản lý trực tiếp?';
export const WORKFLOW_LABEL: string = 'Quy trình xử lí';
export const GROUP_CATEGORY_LABEL: string = 'Phân loại';
export const SLA: string = 'Thời gian SLA';
export const SLA_UNIT: string = 'giờ';
export const DEPARTMENT: string = 'Bộ phận';
export const FOLLOWER: string = 'Người theo dõi';
export const DESCRIPTION: string = 'Mô tả';
export const CUSTOM_FORM: string = 'Mẫu form đề xuất?';
export const CREATE_GROUP: string = 'Tạo nhóm đề xuất';
export const EDIT_GROUP: string = 'Chỉnh sửa nhóm';
export const APPROVER: string = 'Người xét duyệt';

export const ERR_INCORRECT_FORMAT: string = 'Không đúng định dạng.';
export const ERR_OUT_OF_RANGE: string = 'Giá trị không nằm trong khoảng cho phép.';
export const ERR_NOT_NULL_VALUE: string = ' không thể bỏ trống.';

/** Request **/
export const CREATE_REQUEST_TITLE: string = 'Tạo đề xuất mới';
export const REQUEST_TITLE: string = 'Tên đề xuất';
export const GROUP: string = 'Nhóm';
export const ATTACHMENTS: string = 'Tệp đính kèm';
export const CREATE_REQUEST: string = 'Tạo đề xuất mới';