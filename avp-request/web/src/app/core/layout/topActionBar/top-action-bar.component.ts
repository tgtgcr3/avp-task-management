import { KeyCloakService } from 'src/app/core/service/keycloak.service';
import { Component, OnInit, SimpleChanges } from '@angular/core';
import * as message from '../../common/one-message';
import { OneRouteService } from '../../service/one-route.service';
import { OneRequest, RequestGetAllResult, RequestService } from 'src/generated/web-api';
import { environment } from 'src/environments/environment';
import { forkJoin } from 'rxjs';

@Component({
  templateUrl: 'top-action-bar.component.html',
  styleUrls: ['top-action-bar.component.scss'],
  selector: 'top-action-bar'
})
export class TopActionBarComponent implements OnInit {

  userInfo: any;
  requests: OneRequest[] = [];

  constructor(
    private keyCloakService: KeyCloakService,
    private oneRoute: OneRouteService,
    private requestService: RequestService
  ) {

  }

  ngOnInit() {
    setTimeout(() => {
      this.keyCloakService.getUserInfo().then((userInfo: any) => {
        this.userInfo = userInfo;
      });
    }, 500);
  }

  onSearch($event: any) {
    let searchText = $event.target.value;
    if(searchText == '') {
      this.requests = [];
    }
    else {
      this.requestService.getAllRequests(environment.tenantId, searchText, 0, 10, true).toPromise()
      .then((response: RequestGetAllResult) => {
        let searchId = Number.parseInt(searchText);
        if(isNaN(searchId)) {
          this.requests = response.items;
        }
        else {
          searchId = Math.abs(searchId);
          this.requestService.getRequest(environment.tenantId, searchId, false).toPromise()
          .then((results: OneRequest) => {
            this.requests = [results].concat(response.items);
          })
          .catch((error: any) => {
            this.requests = response.items;
          })
        }
      });
    }
  }

  navToRequest(request: OneRequest) {
    this.oneRoute.navigateTo(['/requests', request.id]);
  }

  public get text() {
    return message;
  }
}
