import { MatDialog } from '@angular/material/dialog';
import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { KeyCloakService } from "../../service/keycloak.service";
import { UserData } from 'src/generated/keycloak-api';

@Component({
  selector: 'one-right-panel',
  templateUrl: './rightPanel.component.html',
  styleUrls: [
    './rightPanel.component.scss'
  ]
})

export class RightPanelComponent implements OnInit {
  @Input()
  userInfo: any = {};

  isAdmin: boolean = false;

  isExternal: boolean = true;

  fullName: string = '';

  constructor(private keycloak: KeyCloakService,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    // this.userInfo = this.keycloak.getUserInfo() || {};
    this.isAdmin = this.keycloak.isAdmin();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.isAdmin = this.keycloak.isAdmin();
    if (this.userInfo != null) {
      this.fullName = this.keycloak.getFullName(this.userInfo);
      let username = this.keycloak.getUsername(this.userInfo);
      if (username != null && username.length > 0) {
        this.keycloak.getAllUsers(username, true).toPromise().then((users: UserData[]) => {
          if (users.length > 0) {
            this.isExternal = this.keycloak.isFromExternal(users[0]);
          }
        });

      }
    }
  }

  changeUserPassword() {
    this.keycloak.getAllUsers(this.keycloak.getUsername(this.userInfo), false).toPromise().then((users) => {
      if (users != null && users.length > 0) {
        if (this.keycloak.isFromExternal(users[0])) {
          alert('We do not support reset password for your account');
        } else {
          //const dialogRef = this.dialog.open(PasswordModalComponent);

          // dialogRef.afterClosed().subscribe(result => {
          //   if (result == true) {
          //     alert('Đổi mật khẩu thành công');
          //   }
          //   console.log(`Dialog result: ${result}`);
          // });
        }
      }
    })
  }

  getUsername(userInfo) {
    return this.keycloak.getUsername(userInfo);
  }

}
