import {Component, Input, OnInit} from "@angular/core";
import {SecurityService} from "../../service/security.service";
import {Router} from "@angular/router";
import {KeyCloakService} from "../../service/keycloak.service";
import {MatDialog} from "@angular/material/dialog";
import {AppListComponent} from "./comp/appList/appList.component";
import { KeyCloakUserService } from "src/generated/account-api/api/keyCloakUser.service";

@Component(
  {
    selector: 'one-left-panel',
    templateUrl: './leftPanel.component.html',
    styleUrls: [
      './leftPanel.component.scss'
    ]
  }
)

export class LeftPanelComponent implements OnInit {

  @Input()
  userInfo: any = {}

  currentRoute = '';

  constructor(
    private securityService: SecurityService,
    private keycloakService: KeyCloakService,
    private userKCService: KeyCloakUserService,
    private router: Router,
    private dialog: MatDialog
  ) {
  }

  ngOnInit() {
   // this.userInfo = this.keycloakService.getUserInfo() || {};
    // console.log('userinfo');
    // console.log(this.userInfo);
  }

  login() {
    this.securityService.login();
  }

  logout() {
    this.userKCService.logout({
      token: this.keycloakService.getUsername(this.keycloakService.getUserInfo())
    }).toPromise().then((data) => {
      this.securityService.logout();
      this.router.navigate(['/']);
    });

  }

  navigateToMemberList() {
    this.router.navigate(['/members']);
    this.currentRoute = 'members';
  }

  navigateToPersonalInfo() {
    this.router.navigate(['/userDetails', 'me']);
    this.currentRoute = 'personal';
  }

  showAppList() {
    const dialogRef = this.dialog.open(AppListComponent, {panelClass: 'one-app-dialog'});
    dialogRef.updatePosition({top: '10px', left: '106px'});
    dialogRef.updateSize('770px', 'calc(100% - 20px)');
  }

  navigateToGroups() {
    this.router.navigate(['/management/userGroups']);
    this.currentRoute = 'groups';
  }

  navigateToGuests() {
    this.router.navigate(['/guests']);
    this.currentRoute = 'guests';
  }

  getUsername() {
    return this.keycloakService.getUsername(this.userInfo);
  }
}
