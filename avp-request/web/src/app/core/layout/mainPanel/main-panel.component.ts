import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { KeyCloakService } from '../../service/keycloak.service';
import { OneRouteService } from '../../service/one-route.service';

@Component({
    templateUrl: './main-panel.component.html',
    styleUrls: ['./main-panel.component.scss'],
    selector: 'main-panel'
})
export class MainPanelComponent implements OnInit {

    private user: any;

    constructor(private oneRoute: OneRouteService,
        private keyCloakService: KeyCloakService) { }

    get opened() {
        return !!this.oneRoute.params['id'] == false || !(this.isInRequest || this.isInSystemRequest);
    }

    get isInRequest() {
        let regex = /^\/requests/gi;
        return regex.test(this.oneRoute.current);
    }

    get isInSystemRequest() {
        let regex = /^\/systemRequest/gi;
        return regex.test(this.oneRoute.current);
    }

    get isInGroup() {
        let regex = /^\/(groups|systemRequest)/gi;
        return regex.test(this.oneRoute.current);
    }

    get isAdmin() {
        return this.keyCloakService.isAdmin() || this.keyCloakService.isOwner();
    }

    ngOnInit() {
        this.keyCloakService.getUserInfo().then((user: any) => {
            this.user = user;
            console.log(this.user);
        });
    }
}
