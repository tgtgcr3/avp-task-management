import { KeyCloakService } from './core/service/keycloak.service';
import { MiniChatAdapter } from './module/messaging/services/mini-chat.adapter';
import { Component, OnInit } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import { SecurityService } from './core/service/security.service';
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {SseService} from "./core/service/sse-service";
import {environment} from "../environments/environment";
import { HttpClient } from '@angular/common/http';
import {ActivityLogService} from './core/service/activity-log.service';


declare var RocketChat: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'web';

  isLogedIn: boolean = false;
  userInfo: any = {};

  userId = '';


  //chatAdapter: ChatAdapter;

  constructor(
    private translate: TranslateService,
    private securityService: SecurityService,
    public chatAdapter: MiniChatAdapter,
    // public fileAdapter: MiniChatFileUploadAdapter,
    private keycloakService: KeyCloakService,
    private router: Router,
    private sseService: SseService,
    private http: HttpClient,
    private activityLogService: ActivityLogService
  ) {
    translate.setDefaultLang('vi-VN');
  }
  ngOnInit(): void {
    this.securityService.prepare().then(() => {
      this.isLogedIn = this.securityService.isLogedIn();
      if (this.isLogedIn == true) {
        let username = this.keycloakService.getUsername(this.securityService.getUserInfo());
        this.keycloakService.findUserByUsername(username).toPromise().then(users => {
          this.userInfo = users.find(u => this.keycloakService.getUsername(u) == username);

          let url = this.router.url;
          this.handleNavigate(url);

        });  //this.securityService.getUserInfo();
        //console.log(this.userInfo);
        this.userId = username;
        if (username != null) {
          this.listenForLogout(username);
        }
        this.chatAdapter.init();

        let userInfo = this.securityService.getUserInfo();
        let lastAuthTime = localStorage.getItem('lastAuthTime');
        let currentAuthTime = userInfo.auth_time;
        if (lastAuthTime == null || lastAuthTime != currentAuthTime) {
          this.http.get('https://api.ipify.org?format=json').toPromise().then((data: any) => {
            this.sendActivityLogs(username, data.ip, currentAuthTime);
          }).catch((err:any) => {
            this.sendActivityLogs(username, null, currentAuthTime);
          });
        }
      } else {
        this.securityService.login();
      }
    });

  }

  sendActivityLogs(username, publicIp, currentAuthTime) {
    this.activityLogService.createActivity({
      user: username,
      action: 'Login',
      source: 'Request',
      localId: null,
      publicIp: publicIp
    }).toPromise().then(() => {
      localStorage.setItem('lastAuthTime', currentAuthTime);
    });
  }


  sseObservable: Subscription;

  private listenForLogout(username: string): void {
    this.sseObservable = this.sseService.initServerSentEvent("/sse/api/pri/logout-sse?username=" + username).subscribe(event => {
    }, (error: any) => {
      try {
        this.sseObservable.unsubscribe();
      } catch(ex) {
      }
      setTimeout(() => {
        this.listenForLogout(username);
      }, 1000);
    });

    this.sseService.addEventListener(SseService.Event_Logout).subscribe((event) => {
      console.log(event);
      if (event && event.data == username) {
        try {
          this.sseObservable.unsubscribe();
        } catch(ex) {
        }
        alert('Tài khoản bạn đã được thoát. Hệ thống sẽ chuyển đến trang đăng nhập để đăng nhập lại');
        setTimeout(() => {
          this.securityService.logout();
        });
      }
    });

    // this.sseService.addEventListener(SseService.Event_Notification).subscribe((event:any) => {
    //   // console.log("receive notification");
    //   // console.log(event);
    // })
  }

  private handleNavigate(url: string) {
    if (this.securityService.getAccessToken() != null) {
      if(url.includes('/?state=') || this.router.url == '/') {
        this.router.navigate(['/requests']);
      }
      else {
        let tempUrl = url;
        if (url.includes('?')) {
          url = url.substr(0, url.indexOf('?'));
        }

        if (tempUrl.includes('one=')) {
          this.router.navigate([url]);
        } else {
          this.router.navigate([url], {queryParams: {one: 1}});
        }
      }
    } else {
      setTimeout(() => {
        this.handleNavigate(url);
      }, 100);
    }

  }
}
