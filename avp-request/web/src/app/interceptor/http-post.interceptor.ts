import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent
} from '@angular/common/http';
import {OneLoaderService} from '../core/service/one-loader.service';

import { Observable, Subscription, throwError } from 'rxjs';
import { map, catchError, finalize } from 'rxjs/operators';

@Injectable()
export class HttpPostInterceptor implements HttpInterceptor {

    constructor(private oneLoaderService: OneLoaderService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
        if(request.method !== "GET") {
            const spinnerSubscription: Subscription = this.oneLoaderService.spinner$.subscribe();

            return next.handle(request).pipe(
                finalize(() => spinnerSubscription.unsubscribe())
            )
        }
        
        return next.handle(request)
    }
}