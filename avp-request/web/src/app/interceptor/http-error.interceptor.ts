import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent
} from '@angular/common/http';
import {OneAlertService} from '../core/service/one-alert.service';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private oneAlert: OneAlertService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler) : Observable<HttpEvent<any>> {
        let observable = next.handle(request)
        .pipe(
            catchError((error) => {
                if(request.method !== "GET") {
                    console.error(error);
                    this.oneAlert.onError(error.error);
                }
                return throwError(error.error);
            })
        );

        return observable;
    }
}