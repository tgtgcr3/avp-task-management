// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  idUrl: 'https://sso.ntoannhan.xyz',
  baseUrl: 'https://request.ntoannhan.xyz',
  //baseUrl: 'http://192.168.12.2:4200',
  oneMessage: 'http://192.168.12.51:3000',
  oneWework: 'http://192.168.11.51:4300/wework',
  oneTaskAPI: 'http://192.168.11.51:4300/one_api/v1',
  oneTask: 'http://192.168.11.51:4300',
  discussionTarget: 1,
  clientId: 'avp-request',
  clientSecret: 'f98f249b-8ca6-4608-9639-192d860fd7bb',
  oneMessageWs: 'ws://192.168.11.51:3000',
  defaultAvatar: 'https://account.ntoannhan.xyz/api/pri/file/download/default_avatar.png',
  tenantId: 'an-viet-phat',
  appId: 'request'
  // defaultAvatar: 'http://192.168.11.51:4200/api/pri/file/download/default_avatar.png'
};

// export const environment = {
//   production: false,
//   idUrl: 'http://ntoannhan.duckdns.org:9999',
//   baseUrl: 'http://ntoannhan.duckdns.org:4300',
//   oneMessage: 'http://192.168.12.51:3000',
//   oneWework: 'http://192.168.11.51:4300/wework',
//   clientId: 'avp-request',
//   clientSecret: 'f98f249b-8ca6-4608-9639-192d860fd7bb',
//   oneMessageWs: 'ws://192.168.11.51:3000',
//   defaultAvatar: 'http://task.anvietenergy.com/api/pri/file/download/default_avatar.png',
//   tenantId: 'an-viet-phat',
//   appId: 'request'
//   //defaultAvatar: 'http://192.168.11.51:4200/api/pri/file/download/default_avatar.png'
// };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
