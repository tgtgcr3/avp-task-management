export const environment = {
  production: true,
  idUrl: 'https://sso.ntoannhan.xyz',
  baseUrl: 'https://request.ntoannhan.xyz',
  //baseUrl: 'http://192.168.12.2:4200',
  oneMessage: 'http://192.168.12.51:3000',
  oneWework: 'http://192.168.11.51:4300/wework',
  oneTaskAPI: 'http://192.168.11.51:4300/one_api/v1',
  oneTask: 'http://192.168.11.51:4300',
  discussionTarget: 1,
  clientId: 'avp-request',
  clientSecret: 'f98f249b-8ca6-4608-9639-192d860fd7bb',
  oneMessageWs: 'ws://192.168.11.51:3000',
  defaultAvatar: 'https://account.ntoannhan.xyz/api/pri/file/download/default_avatar.png',
  tenantId: 'an-viet-phat',
  appId: 'request'
};
