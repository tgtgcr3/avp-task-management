#!/bin/bash


helpFunction()
{
   echo ""
   echo "Usage: $0 -v Version -p Prefix"
   echo -e "\t-v Version of this release"   
   echo -e "\t-p Prefix of this release"   
   exit 1 # Exit script after printing help
}

compileResource()
{
   cd api && mvn clean install && cd ..
#   cd web && ng build --output-hashing=all && cd ..
#   rm -rf app/src/main/resources/static && cp -r web/dist/web app/src/main/resources/static
   cd app && mvn clean install && cd ..

#   cd ..
}

buildImage()
{
    imageTag=$1
    dockerFile=$2

    echo "buildImage with Dockerfile '$dockerFile' and tag '$imageTag'"
    docker build --tag "$imageTag" -f "$dockerFile" .
}

buildServerImage()
{
    version=$1
    prefix=$2
    
    imageName="$prefix-request-server:$version"

    cd avp-docker-server && cp -rf ../app/target/*.jar ./ && cp -rf ../app/*.jar

    echo "build core image with name '$imageName'"
    buildImage "$imageName" 'Dockerfile'
    cd ..
}

buildClientImage()
{
    version=$1
    prefix=$2
    
    imageName="$prefix-request-client:$version"

#    cd avp-docker-client && rm -rf web && cp -rf ../new-gen/web/dist/web ./web
    cd avp-docker-client && rm -rf web && cp -rf ../web ./web

    echo "build core image with name '$imageName'"
    buildImage "$imageName" 'Dockerfile'

    cd ..
}

while getopts "v:p:" opt
do
   case "$opt" in
      v ) releaseVersion="$OPTARG" ;;      
      p ) prefix="$OPTARG" ;;      
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$releaseVersion" ] || [ -z "$prefix" ]
then
   echo "Release version is empty or Prefix is empty";
   helpFunction
fi

# Begin script in case all parameters are correct
echo "Build release using version '$releaseVersion' and prefix '$prefix'"

echo "Build Core Image"
compileResource

buildClientImage "$releaseVersion" "$prefix"
buildServerImage "$releaseVersion" "$prefix"
