export const environment = {
  production: true,
  baseUrl: '',
  avpAccountUrl: 'http://account.anvietenergy.com:4200',
  avpMessageUrl: 'http://message.anvietenergy.com:3000',
  avpRequestUrl: 'http://request.anvietenergy.com:4300'
};
