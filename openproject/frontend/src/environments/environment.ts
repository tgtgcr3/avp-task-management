// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  baseUrl: '',
  avpAccountUrl: 'https://192.168.11.2:4200',
  avpMessageUrl: 'https://192.168.11.160:3000',
  avpRequestUrl: 'https://192.168.11.2:4300'
};
