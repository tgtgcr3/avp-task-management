//-- copyright
// OpenProject is a project management system.
// Copyright (C) 2012-2015 the OpenProject Foundation (OPF)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License version 3.
//
// OpenProject is a fork of ChiliProject, which is a fork of Redmine. The copyright follows:
// Copyright (C) 2006-2013 Jean-Philippe Lang
// Copyright (C) 2010-2013 the ChiliProject Team
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// See doc/COPYRIGHT.rdoc for more details.
//++

import { HalResource } from 'core-app/modules/hal/resources/hal-resource';
import { Attachable } from 'core-app/modules/hal/resources/mixins/attachable-mixin';
import {AttachmentCollectionResource} from 'core-app/modules/hal/resources/attachment-collection-resource';
import {ProjectCacheService} from 'core-components/projects/project-cache.service';

export interface ProjectResourceLinks {
  addAttachment(attachment: HalResource): Promise<any>;
}

export class ProjectBaseResource extends HalResource {
  public $links:ProjectResourceLinks;
  public attachments:AttachmentCollectionResource;

  readonly projectCacheService:ProjectCacheService = this.injector.get(ProjectCacheService);
  private attachmentsBackend = true;

  public updateAttachments():Promise<HalResource> {
    return this
      .updateLinkedResources('attachments')
      .then((resources:any) => resources.attachments);
  }

  public updateLinkedResources(...resourceNames:string[]):Promise<any> {
    const resources:{ [id:string]:Promise<HalResource> } = {};

    resourceNames.forEach(name => {
      const linked = this[name];
      resources[name] = linked ? linked.$update() : Promise.reject(undefined);
    });

    const promise = Promise.all(_.values(resources));
    promise.then(() => {
      this.projectCacheService.touch(this.id!);
    });

    return promise;
  }

  public $initialize(source:any) {
    super.$initialize(source);

    let attachments:any = this.attachments || { $source: {}, elements: [] };
    this.attachments = new AttachmentCollectionResource(
      this.injector,
      // Attachments MAY be an array if we're building from a form
      _.get(attachments, '$source', attachments),
      false,
      this.halInitializer,
      'HalResource'
    );
  }
}

export const ProjectResource = Attachable(ProjectBaseResource);

export interface ProjectResource extends ProjectBaseResource, ProjectResourceLinks {
}