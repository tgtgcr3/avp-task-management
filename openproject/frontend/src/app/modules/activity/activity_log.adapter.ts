import { Injectable } from '@angular/core';
import { ActivityService } from 'generated/web-api/api/activity.service';
import { ActionType } from 'generated/web-api/model/actionType';
import { ActivityLog } from 'generated/web-api/model/activityLog';
import { ActivitySource } from 'generated/web-api/model/activitySource';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LogAdapter {

    constructor(private activityService: ActivityService,
        private currentUserService: CurrentUserService,
        private httpClient: HttpClient) {
        this.activityService.configuration.basePath = "/api/pri";
    }

    logging(action: ActionType, projectId?: any, description: string = "") {

        let payload : ActivityLog = {
            action: action,
            user: this.currentUserService.email,
            source: ActivitySource.Task,
            description: description,
            projectId: Number.parseInt(projectId)
        };

        this.httpClient.get("/local_ip")
        .subscribe((res:any)=>{
            payload.localId = res.ip;
            this.createLog(payload);
        },
        (error: any) => {
            this.createLog(payload);
        });
        
    }

    private createLog(activityLog: ActivityLog) {
        this.activityService.createActivity(activityLog).toPromise();
    }
}

export const ActivityActionType = {
    Login: 'Login' as ActionType,
    Logout: 'Logout' as ActionType,
    UpdateProfile: 'Update_Profile' as ActionType,
    CreateTask: 'Create_Task' as ActionType,
    UpdateTask: 'Update_Task' as ActionType,
    DeleteTask: 'Delete_Task' as ActionType,
    AssignTask: 'Assign_Task' as ActionType,
    CreateProject: 'Create_Project' as ActionType,
    UpdateProject: 'Update_Project' as ActionType,
    DeleteProject: 'Delete_Project' as ActionType
}