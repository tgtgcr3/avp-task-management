var PROXY_CONFIG = [
  {
    "context": ['/api/pri/**'],
    "target": "https://account.ntoannhan.xyz",
    "secure": false,
    "changeOrigin": true
    // "bypass": function (req, res, proxyOptions) {
    // }
  },
  {
    "context": ['/local_ip'],
    "target": "http://api.ipify.org/?format=json",
    "secure": false,
    "changeOrigin": true,
    "pathRewrite": {
      "^/local_ip": ""
    }
  },
  {
    "context": ['/**'],
    "target": "http://localhost:3000",
    "secure": false
    // "bypass": function (req, res, proxyOptions) {
    // }
  }
];

module.exports = PROXY_CONFIG;
