#-- encoding: UTF-8

#-- copyright
# OpenProject is a project management system.
# Copyright (C) 2012-2018 the OpenProject Foundation (OPF)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License version 3.
#
# OpenProject is a fork of ChiliProject, which is a fork of Redmine. The copyright follows:
# Copyright (C) 2006-2017 Jean-Philippe Lang
# Copyright (C) 2010-2013 the ChiliProject Team
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# See docs/COPYRIGHT.rdoc for more details.
#++

class Queries::WorkPackages::Filter::DepartmentFilter <
  Queries::WorkPackages::Filter::WorkPackageFilter
  def allowed_values
    @allowed_values ||= begin
      visible_departments_array.map { |id, name| [name, id.to_s] }
    end
  end

  def available_operators
    [::Queries::Operators::All,
     ::Queries::Operators::None,
     ::Queries::Operators::Equals]
  end

  def available?
    true
  end

  def type
    :list
  end

  def human_name
    I18n.t('query_fields.department')
  end

  def self.key
    :department
  end

  def ar_object_filter?
    true
  end

  def value_objects
    value_ints = values.map(&:to_i)
  end

  def where
    ids = []
    value_objects.each do |value|
      ids += Project.find(value).descendants.map(&:id)
    end

    "#{Project.table_name}.id IN (%s)" % ids.uniq.join(',')
  end

  private

  def visible_departments_array
    visible_departments.pluck(:id, :name)
  end

  def visible_departments
    # This can be accessed even when `available?` is false
    @visible_departments ||= begin
      Project.joins(" INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                        AND custom_values.customized_id = projects.id AND custom_values.value = '1'
                      INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'")
            .where("parent_id = #{Setting.avp_project_id}")
    end
  end

  def operator_strategy
    case operator
    when '*'
      ::Queries::Operators::All
    when '!*'
      ::Queries::Operators::None
    when '='
      ::Queries::Operators::Equals
    end
  end
end
