
(function ($) {
  jQuery(function () {

    function qd(e) {
      return _.get(e.config, "_config.openProject.pluginContext")
    }

    function zd(e, t) {
      return qd(e).services[t]
    }

    function Hd(e, t) {
      return zd(e, "pathHelperService")
    }

    function jd(e) {
      return _.get(e.config, "_config.openProject.context.resource")
    }

    window.OPClassicEditor.defaultConfig.mention.feeds[0].feed = function (e) {
      let t = jd(this.editor);
      if (!t || "WorkPackage" !== t._type) return [];
      const n = t.project.idFromLink, o = Hd(this.editor).api.v3.avpUsersForTagging(e);
      let i = window.OpenProject.urlRoot + "/userDetails/";
      return new Promise((e, t) => {
        jQuery.getJSON(o, t => {
          e(t._embedded.elements.map(e => {
            const t = `tag#${e.mail}`;
            return {
              type: "user", id: `@${e.id}`, text: t, name: e.name, link: i + e.mail, avt: "/api/pri/avatar?email=" + e.mail
            }
          }))
        })
      })
    }

    window.OPClassicEditor.defaultConfig.mention.feeds[0].itemRenderer = function(item) {
      const imgTag = document.createElement('img');
      imgTag.src = item.avt;
      imgTag.onerror = function($event) {
        $event.target.src = '/assets/avp-icons/default_user.png';
      }
      
      const imgWrapper = document.createElement('div');
      imgWrapper.classList.add('image');

      imgWrapper.appendChild(imgTag);

      const avatarElement = document.createElement('div');
      avatarElement.classList.add('avatar', 'avatar-24', '-circled');
      avatarElement.style.float = 'left';

      avatarElement.appendChild(imgWrapper);

      const usernameElement = document.createElement( 'div' );
      usernameElement.style.paddingLeft = '30px';
      usernameElement.textContent = item.name;

      const itemElement = document.createElement( 'div' );
      itemElement.classList.add( 'mention-list-item' );

      itemElement.appendChild( avatarElement );
      itemElement.appendChild( usernameElement );

      return itemElement;
    }
  })
}(jQuery));