class RepeatedWorkPackageJob
  def self.perform
    Rails.logger.info { " Start generating frequence work packages " }

    work_packages = generate_work_packages

    work_packages.each do |work_package|
      copy_work_package(work_package)
    end

    Rails.logger.info { work_packages.map(&:subject).join(', ') }

    Rails.logger.info { " Finish generating frequence work packages " }
  end
  

  def self.generate_work_packages
    week_day = Date.today.strftime("%a").downcase
    month_date = Date.today.strftime("%-d")

    join = "
    inner join custom_fields cf_status 
      on cf_status.name = 'frequence_status'
      and cf_status.type = 'WorkPackageCustomField'
    inner join custom_values cv_status
      on cv_status.custom_field_id = cf_status.id
      and cv_status.customized_id = work_packages.id 
      and cv_status.value = 't'
    "

    join += "
    inner join custom_fields cf_value
      on cf_value.name = 'frequence_values'
      and cf_value.type = 'WorkPackageCustomField'
    inner join custom_values cv_value
      on cv_value.custom_field_id = cf_value.id
      and cv_value.customized_id = work_packages.id 
      and (cv_value.value like '%#{week_day}%' or (cv_value.value like '#{month_date},%' or 
                                                   cv_value.value like '%,#{month_date}' or 
                                                   cv_value.value like '%,#{month_date},%'))
    "

    type = Type.find_by(name: "Frequency")

    where = "
    work_packages.type_id = #{type.id} and
    (work_packages.start_date IS NULL or work_packages.start_date <= '#{Date.today}') and
    (work_packages.due_date IS NULL or work_packages.due_date >= '#{Date.today}') and
    work_packages.id not in (select relations.to_id 
                              from relations 
                              where relations.to_id = work_packages.id and relations.from_id != work_packages.id)"

    
    WorkPackage.joins(join).where(where)
  end

  def self.copy_work_package(work_package, attributes = {})
    override_attributes = { 
                    status_id: 1,
                    start_date: Date.today,
                    due_date: Date.today,
                    type_id: 1,
                    custom_field_values: nil
                  }.merge(attributes)


    service_call = WorkPackages::CopyService
                        .new(user: work_package.author,
                            work_package: work_package,
                            contract_class: WorkPackages::CopyProjectContract)
                        .call(override_attributes)
    
    if service_call.success?
      new_work_package = service_call.result

      notify_to_assignee(new_work_package) unless new_work_package.assigned_to.nil?

      child_work_packages = work_package.children
      child_work_packages.each do |child_wp|

        copy_work_package(child_wp, { parent_id: new_work_package.id })
      end

      Rails.logger.info { " Copied work package id: #{new_work_package.id}" }
    else
      Rails.logger.error "Cannot copied #{work_package.subject}"
    end
  end

  def self.notify_to_assignee(work_package)
    description = {
      stateName: "tasks.list",
      params: { task: work_package.id },
      content: ''
    }

    payload = {
      ownerEmail: work_package.author.mail,
      targetEmail: work_package.assigned_to.mail,
      title: "<b>[#{work_package.parent.nil? ? '' : work_package.parent.name}] #{work_package.author.name}</b> tạo mới một công việc <b>#{work_package.subject}</b> cho bạn",
      description: JSON.unparse(description),
      datetime: Time.now.to_i,
      status: 0
    }

    uri = URI.parse(Setting.avp_sso_url + "/api/pri/notifications?email=#{work_package.author.mail}")
    http = Net::HTTP.new(uri.host, uri.port)
    res = http.post(uri, payload.to_json, {'Content-Type': 'application/json'})

    case res
    when Net::HTTPSuccess, Net::HTTPRedirection
      # OK
      puts 'OK'
    else
      puts 'Fail'
    end
  end
end