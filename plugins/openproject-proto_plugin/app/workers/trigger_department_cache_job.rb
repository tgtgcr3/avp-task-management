class TriggerDepartmentCacheJob < ApplicationJob
  def perform
    Rails.logger.info { 'Perform trigger department cache' }
    uri = URI.parse(Setting.department_cache)
    req = Net::HTTP::Post.new(uri)
    req['X-TENANT-ID'] = Setting.tenant_id
    req['X-APP-ID'] = 'request'
    req['X-ONE-HEADER'] = Setting.x_one_header

    res = Net::HTTP.start(uri.hostname, uri.port) {|http|
      http.request(req)
    }

    case res
    when Net::HTTPSuccess
      puts 'Department cache triggered'
      true
    else
      puts 'Cannot trigger department cache'
      Rails.logger.info { 'Cannot trigger department cache' }
      false
    end
  end
end