// Plain Javascript goes here. This is outside the angular application context.
// The file has to be included into the page first via
//
//   <%= javascript_include_tag 'proto_plugin/main', plugin: :openproject_proto_plugin %>
//
// For this example plugin the file is included into every page through the
// :header_tags view hook (see `lib/open_project/proto_plugin/hooks.rb`).

(function ($) {
  $(document).ready(function () {

    // OpenProject logo gets thick red border on mouse hover.


    // Widget box emphasized by giving it a nice red border.


    console.log(I18n.t('js.proto_plugin_name') + ' OK');

    $('#context-menu').hide();

    $(document).on('click', '.user-mention', function () {
      var classes = $(this).attr('class').split(/\s+/);
      var api = `/api/pri/internal/userinfo?email=${classes[1]}`;
      $.ajax({
        async: false,
        url: api,
        success: function (response) {
          console.log(response);
          $('#error').hide();
          $('#user-popup-name').empty();
          $('#user-popup-name').append(response.firstName + ' ' + response.lastName);

          $('#user-popup-title').empty();
          $('#user-popup-title').append(response.title);

          $('#user-popup-phone').empty();
          $('#user-popup-phone').append(response.phoneNumber);

          $('#user-popup-avatar').attr("src", response.avatar);
          $('#user-popup-link').attr('data-url', `${window.environment.avpAccountUrl}/userDetails/${response.username}`);

          $('#user-popup').show();

        },
        error: function (error) {
          console.log('On Error');
          $('#user-popup').hide();

          $('#error').empty();
          $('#error').append('Không thể tải người dùng')
          $('#error').show();
        }
      });
      var position = $(this).offset();
      $('#context-menu-main').css('top', position.top + 25);
      $('#context-menu-main').css('left', position.left + 10);

      $('#context-menu').show();
    });

    $(document).on('click', '#user-popup-link', function () {
      var url = $(this).attr('data-url');
      window.open(url, '_blank');
    });

    $('#context-menu-close').click(function () {
      $('#context-menu').hide();
    })
  });
})(jQuery);
