require 'json'
require 'net/http'

class AvpForumController < ApplicationController
  before_action :require_login

  def create_topic
    project_id = params[:project_id]
    forum = Forum.find_by(project_id: project_id, position: 1)

    message = Message.new.tap do |m|
      m.subject = params[:subject]
      m.content = params[:content]
      m.author = User.current
      m.forum = forum
    end

    if message.save
      watchers = params[:watchers]

      watchers.each do |w|
        message.set_watcher(User.find(w[:id]))
      end

      render json: ::API::V3::Posts::PostRepresenter.new(message,
                                                          current_user: current_user,
                                                          embed_links: true)
    else
      return render_403
    end
  end

  def all
    projects = User.current.projects
    forum_ids = projects.map(&:forums).map(&:ids).uniq.flatten(1)
    messages = Message.where(parent_id: nil, forum_id: forum_ids)

    render json: messages.to_json(only: %i[id subject content replies_count created_on updated_on],
                                  include: [
                                    forum: {
                                      include: [project: {only: %i[id name]}]
                                    },
                                    author: {
                                      only: %i[id firstname lastname mail login], methods: :avatar
                                    },
                                    watchers: {
                                      include: [user: { only: %i[id firstname lastname mail login], methods: :avatar}]
                                    }])
  end

  def by_project
    project_id = params[:project_id]
    forum = Forum.find_by(project_id: project_id)
    enable_module
    forum = create_new_forum if forum.nil?

    messages = Message.where(parent_id: nil, forum_id: forum.id)

    render json: messages.to_json(only: %i[id subject content replies_count created_on updated_on],
                                  include: [
                                    forum: {
                                      include: [project: {only: %i[id name]}]
                                    },
                                    author: {
                                      only: %i[id firstname lastname mail login], methods: :avatar
                                    },
                                    watchers: {
                                      include: [user: { only: %i[id firstname lastname mail login], methods: :avatar}]
                                    }])
  end

  def message_presenter
    message = Message.new.tap do |m|
      m.author = User.current
      m.forum = Forum.find(1)
    end
    representer = ::API::V3::Posts::PostRepresenter.new(message,
                                                        current_user: current_user,
                                                        embed_links: true)

    render json: representer
  end

  def get_topic
    id = params[:id]
    index = params[:page].nil? ? 1 : params[:page]
    total = Message.where("parent_id = #{id} or id = #{id}").count
    root = ::API::V3::Posts::PostRepresenter.new(Message.find(id), 
                                                current_user: User.current,
                                                embed_links: true)

    messages = Message.where("parent_id = #{id} or id = #{id}").order(:created_on, :id).limit(10).offset((index.to_i - 1) * 10)
    elements = []
    messages.each do |message|
      elements << ::API::V3::Posts::PostRepresenter.new(message, 
                                                        current_user: User.current,
                                                        embed_links: true)
    end

    render json: {
      root: root,
      elements: elements,
      total: total
    }
  end

  def edit_topic
    id = params[:id]
    message = Message.find(id)
    if message.nil?
      Rails.logger.error "Message #{id} cannot found"
    else
      message.subject = params[:subject]
      message.content = params[:content]
      if message.save
        render json: ::API::V3::Posts::PostRepresenter.new(message,
                                                          current_user: current_user,
                                                          embed_links: true)
      else
        return render_500
      end
    end
  end

  def delete_topic
    id = params[:id]
    message = Message.find(id)
    if message.nil?
      render json: { status: 200 }
    else
      # return render_403 unless message.destroyable_by?(User.current) # depend on Forum module is enabled or not.
      message.destroy
      render json: { status: 200 }
    end
  end

  def reply_or_quote
    id = params[:id]

    message = Message.new.tap do |m|
      m.subject = params[:subject]
      m.content = params[:content]
      m.author = User.current
      m.forum = Forum.find(params[:forum_id])
      m.parent_id = id
    end

    if message.save
      render json: ::API::V3::Posts::PostRepresenter.new(message,
                                                          current_user: current_user,
                                                          embed_links: true)
    else
      return render_500
    end

  end

  private 

  def enable_module
    project_id = params[:project_id]
    forumModule = EnabledModule.find_by(project_id: project_id, name: 'forums')

    if forumModule.nil?
      EnabledModule.new(name: 'forums', project_id: project_id).save
    end

  end

  def create_new_forum
    project_id = params[:project_id]
    project = Project.find(project_id)

    forum = Forum.new(name: project.name, description: project.name, position: 1)
    forum.project = project

    forum.save
    forum
  end
end