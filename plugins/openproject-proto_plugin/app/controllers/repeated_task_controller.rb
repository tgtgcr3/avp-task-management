require 'json'

class RepeatedTaskController < ApplicationController
	before_action :require_login

	def show
		results = [{id: 1, name: 'Name'}]
		render json: results
	end

  def destroy_work_packages
    ids = params[:ids]
    work_packages = WorkPackage.where(id: ids)

    work_packages.each do |work_package|
      begin
        WorkPackages::DeleteService
          .new(user: current_user,
               model: work_package.reload)
          .call
      rescue ::ActiveRecord::RecordNotFound
        # raised by #reload if work package no longer exists
        # nothing to do, work package was already deleted (eg. by a parent)
      end
    end

    render json: {status: 'success'}
  end

end