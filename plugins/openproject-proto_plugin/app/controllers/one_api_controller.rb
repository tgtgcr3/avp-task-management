require 'json'
require 'net/http'

class OneApiController < ApplicationController

  def create_work_package
    creator_email = params[:creator_email]
    name = params[:name]
    project_id = params[:project_id]
    category_id = params[:category_id]
    dueDate = params[:dueDate]
    startDate = params[:startDate]
    followers = params[:followers]

    attributes = {
      type_id: 1,
      status_id: 1,
      project_id: project_id,
      subject: name,
      start_date: startDate,
      due_date: dueDate
    }

    attributes[:category_id] = category_id if params[:category_id]
    unless params[:assignee_email].nil?
      assignee = User.find_by(mail: params[:assignee_email])
      if assignee.nil?
        render json: {status: 'Failed', message: "Assignee doesn't exist"}
        return
      end
      attributes[:assigned_to_id] = assignee.id
    end

    creator = User.find_by(mail: creator_email)
    if creator.nil?
      render json: {status: 'Failed', message: "Author doesn't exist"}
      return
    end

    create_call = create_wp(creator, attributes, followers)
    if create_call.success?
      render json: {
        status: 'Success',
        workPackage: create_call.result
      }
    else
      render json: {status: 'Failed', message: "Cannot create task"}
    end
  end

  def work_packages
    ids = params[:ids]
    work_packages = WorkPackage.where(id: ids)

    render json: work_packages.to_json(only: %i[id subject start_date due_date],
                                      include: [status: {only: %i[id name]},
                                      assigned_to: {only: %i[id mail lastname firstname]},
                                      project: {only: %i[id name]},
                                      category: {only: %i[id name]}
                                    ])
  end

  def projects
    user_email = params[:user_email]

    user = User.find_by(mail: user_email)
    
    if user.nil?
      render json: {status: 'Failed', message: "User doesn't exist"}
      return
    end

    projects = user.projects.where.not(status: 9).select { |item| item.is_project }

    render json: 
    { 
      status: 'Success',
      items: projects.as_json(only: %i[id name description],
                                        include: [categories: {
                                          only: %i[id name]
                                        }])
    }
  end

  private

  def create_wp(creator, attributes, followers)
    attributes[:custom_field_values] = custom_field_values

    create_call = WorkPackages::CreateService
                    .new(user: creator)
                    .call(attributes)

    if create_call.success?
      work_package = create_call.result

      unless followers.nil?
        followers.each do |follower_email|
          user = User.find_by(mail: follower_email)
          work_package.add_watcher(user) if user.active?
        end
      end

      notify_work_package_created(create_call.result) unless create_call.result.assigned_to.nil?
    end

    create_call
  end

  def custom_field_values
    cf_result = CustomField.find_by(type: 'WorkPackageCustomField', name: 'Result')
    cf_important = CustomField.find_by(type: 'WorkPackageCustomField', name: 'Important')
    cf_urgent = CustomField.find_by(type: 'WorkPackageCustomField', name: 'Urgent')
    cf_favourite = CustomField.find_by(type: 'WorkPackageCustomField', name: 'Favourite')

    [
      [cf_result.id, ""],
      [cf_important.id, "f"],
      [cf_urgent.id, "f"],
      [cf_favourite.id, "f"]
    ].to_h
  end

  def notify_work_package_created(workPackage)
    description = {
      stateName: "tasks.list",
      params: {task: workPackage.id},
      content: ""
    }

    body = {
      ownerEmail: workPackage.author.mail,
      targetEmail: workPackage.assigned_to.mail,
      title: "<b>#{workPackage.parent ? `[#{workPackage.parent.name}]` : ''} #{workPackage.author.name}</b> tạo mới một công việc <b>#{workPackage.subject}</b> cho bạn",
      description: JSON.unparse(description),
      datetime: Time.now.to_i,
      status: 0
    }
    puts body
    uri = URI.parse(Setting.avp_sso_url + "/api/pri/notifications?email=#{workPackage.author.mail}")
    http = Net::HTTP.new(uri.host, uri.port)
    res = http.post(uri, body.to_json, {'Content-Type': 'application/json'})

    case res
    when Net::HTTPSuccess, Net::HTTPRedirection
      # OK
      puts 'OK'
    else
      puts 'Fail'
    end
  end
end