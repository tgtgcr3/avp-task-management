require 'json'

class ReportApiController < ApplicationController
  def report_all
    
    department_clause = ""
    department_id = params[:departmentId]
    unless department_id.nil?
      project_ids = Project.find(department_id).descendants.map(&:id)
      department_clause = "AND work_packages.project_id IN (%s)" % project_ids.uniq.join(',')
    end
    
    
    by = params[:by] # 'created_at' | 'state_date' | 'due_date'
    from = params[:from]
    to = params[:to]
    by_clause = if by == 'created_at'
                  "AND (#{by} BETWEEN '#{from}' AND '#{to}')"
                else
                  from = Date.parse(from)
                  to = Date.parse(to)
                  "AND (#{by} BETWEEN '#{from}' AND '#{to}')"
                end
    

    status = params[:status] # 'inprogess' | 'done'
    status_clause = "AND status_id IN (#{status == 'inprogress' ? '1, 7' : '16, 17'})" unless status.nil?


    where_clause = "type_id = 1 #{by_clause} #{status_clause} #{department_clause}"

    count_sub_task = params[:countSubTask]
    join_clause = ""
    if count_sub_task == '0'
      join_clause += "LEFT JOIN relations rl ON rl.to_id = work_packages.id and rl.hierarchy = 1 "
      where_clause += " AND rl.id IS NULL "
    end

    wps = WorkPackage.joins(join_clause).where(where_clause)
    render json: wps.to_json(only: %i[id subject created_at state_date due_date],
                            methods: [:isImportant, :isUrgent],
                            include: [
                              status: {
                                only: %i[id name]
                              },
                              project: {
                                only: %i[id name description],
                                methods: :status_value,
                                include: [
                                  parent: {
                                    only: %i[id name description]
                                  }
                                ]
                              },
                              assigned_to: {
                                only: %i[id firstname lastname mail]
                              }
                            ])
  end

  def report_by_project
    project_id = params[:projectId]
    where_clause = "type_id = 1 AND project_id = #{project_id} "

    status = params[:status] # 'inprogess' | 'done'
    where_clause += " AND status_id IN (#{status == 'inprogress' ? '1, 7' : '16, 17'})" unless status.nil?

    by_clause = ""
    unless params[:by].nil?
      by = params[:by] # 'created_at' | 'state_date' | 'due_date'
      from = params[:from]
      to = params[:to]
      where_clause += if by == 'created_at'
                        "AND (#{by} BETWEEN '#{from}' AND '#{to}')"
                      else
                        from = Date.parse(from)
                        to = Date.parse(to)
                        "AND (#{by} BETWEEN '#{from}' AND '#{to}')"
                      end
    end
    
    wps = WorkPackage.where(where_clause)
    render json: wps.to_json(only: %i[id subject created_at state_date due_date],
                            methods: [:isImportant, :isUrgent],
                            include: [
                              status: {
                                only: %i[id name]
                              },
                              project: {
                                only: %i[id name description],
                                methods: :status_value,
                                include: [
                                  parent: {
                                    only: %i[id name description]
                                  }
                                ]
                              },
                              assigned_to: {
                                only: %i[id firstname lastname mail]
                              }
                            ])

  end

  def report_by_employee
    employee_ids = params[:employeeIds]
    employee_clause = "AND assigned_to_id IN (%s)" % employee_ids

    project_clause = ""
    project_id = params[:projectId]
    unless project_id.nil?
      project_clause = "AND project_id = #{project_id}"
    end
    
    by = params[:by] # 'created_at' | 'state_date' | 'due_date'
    from = params[:from]
    to = params[:to]
    by_clause = if by == 'created_at'
                  "AND (#{by} BETWEEN '#{from}' AND '#{to}')"
                else
                  from = Date.parse(from)
                  to = Date.parse(to)
                  "AND (#{by} BETWEEN '#{from}' AND '#{to}')"
                end

    where_clause = "type_id = 1 #{employee_clause} #{by_clause} #{project_clause}"

    wps = WorkPackage.where(where_clause)
    render json: wps.to_json(only: %i[id subject created_at state_date due_date],
                            include: [
                              status: {
                                only: %i[id name]
                              },
                              project: {
                                only: %i[id name description]
                              },
                              assigned_to: {
                                only: %i[id firstname lastname mail]
                              }
                            ])
  end
end