require 'json'

class TasksController < ApplicationController
  before_action :require_login
  
  def index
    respond_to do |format|
      format.html do
        render :index, layout: 'angular'
      end
    end
  end

  def getTaskCustomFieldIDs
    @result = CustomField.where("type = 'WorkPackageCustomField' and is_for_all = true")
                  .select('id')
    render json: @result.as_json
  end
end