require 'json'
require 'net/http'

class AvpController < ApplicationController

  include AvatarHelper

  before_action :require_login, except: [:avp_logout, :get_departments]

  def index
    load_current_user_info

    respond_to do |format|
      format.html do
        render :index, layout: 'angular'
      end
    end
  end

  def getTaskCustomFieldIDs
    result = CustomField.where("type = 'WorkPackageCustomField' and is_for_all = true")
                         .select('id')
    render json: result.as_json
  end

  def copySubTaskOf
    source_id = params[:sourceId]
    target_id = params[:targetId]
    project_id = params[:projectId]
    children = WorkPackage.find(source_id).children
    @result = []
    children.each do |wp|
      overrides = { parent_id: target_id,
                    project: Project.find(project_id),
                    subject: "Bản sao của #{wp.subject}",
                    status: Status.find(1),
                    due_date: nil,
                    fixed_version: wp.fixed_version && versions.detect { |v| v.name == wp.fixed_version.name } }

      service_call = WorkPackages::CopyService
                         .new(user: User.current,
                              work_package: wp,
                              contract_class: WorkPackages::CopyProjectContract)
                         .call(overrides)

      if service_call.success?
        new_work_package = service_call.result
        @result << new_work_package
      else
        Rails.logger.error "Cannot copied #{wp.subject}"
      end
    end

    render json: @result
  end

  def taskSummary
    tasks = WorkPackage.where(assigned_to_id: User.current.id, type_id: 1, project_id: Project.active.map(&:id))
    total = tasks.count
    done = tasks.where(status_id: [16, 17]).count

    render json: {
      total: total,
      done: done
    }
  end

  def users
    filters = []

    filters = params[:filters] unless params[:filters].blank? 
    filters = JSON.parse(filters)
    
    condition = "id != 1"
    filters.each do |filter|
      filter.each do |key, value|
        if key == 'name'
          condition += " and (firstname #{value['operator']} '%#{value['values']}%' 
                            or lastname #{value['operator']} '%#{value['values']}%')"
        else
          condition += " and #{key} #{value['operator']} #{value['values']}"
        end
      end
    end

    users = User.active.where(condition)
    users = users.limit(params[:pageSize]) unless params[:pageSize].blank?
    users = users.offset(params[:offset]) unless params[:offset].blank?
    fetched_users = self.class.account_users()
    elements = self.class.combine_with_account_users(fetched_users, users)

    result = {
      "_embedded": {
        "elements": elements
      }
    }
    render json: result
  end

  def usersAll

    query = params[:query]
    offset = params[:offset].to_i
    page_size = params[:pageSize].to_i

    queryString = "id != 1 and (firstname like '%#{query}%' or lastname like '%#{query}%')"

    users = User.active.where(queryString)
    total = users.length()

    users = users.offset((offset - 1) * page_size).limit(page_size) unless offset == 0 || page_size == 0

    fetched_users = self.class.account_users()
    elements = self.class.combine_with_account_users(fetched_users, users)
    
    sorted_list = elements.sort_by { |user| I18n.transliterate(user[:firstName]) }

    result = {
      "total": total,
      "_embedded": {
          "elements": sorted_list
      }
    }
    render json: result
  end

  def my_employees
    uri = URI.parse(Setting.avp_sso_url + "/api/pri/users")
    req = Net::HTTP::Get.new(uri)
    req['X-ONE-Header'] = Setting.x_one_header
    req['content-type'] = 'application/json; charset=utf-8'

    res = Net::HTTP.start(uri.hostname, uri.port) {|http|
      http.request(req)
    }

    responses = []

    case res
    when Net::HTTPSuccess
      result = JSON.parse(res.body)
      fetched_users = result['items']

      me = fetched_users.detect { |user| user['email'] == User.current.mail }

      unless me.nil?
        directMembers = me['directMembers'].split(',').map { |u| fetched_users.detect { |user| user['username'] == u } }

        if directMembers.blank?
          render json: responses
        end 
        
        directMemberEmails = directMembers.map { |m| m['email'] }

        users = User.active.where(mail: directMemberEmails.compact)
        responses = self.class.combine_with_account_users(result, users)
      end
    end

    render json: responses
  end

  def update_user
    ActiveRecord::Base.transaction do
      begin
        email = params[:omail]
        new_mail = params[:nmail]
        user = User.find_by(mail: email)
        user.login = new_mail
        user.mail = new_mail

        render json: user.to_json(only: %i[login firstname lastname mail]) if user.save
      rescue Exception => e
        render_error status: 422, message: "Update user failed due to " + e.inspect
        raise ActiveRecord::Rollback
      end
    end
  end

  def departments
    join_member = ""
    join_member = "INNER JOIN members m ON projects.id = m.project_id AND m.user_id = #{User.current.id}" unless (User.current.admin || User.current.avp_admin)

    departments = Project.joins("#{join_member} INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                                                AND custom_values.customized_id = projects.id AND custom_values.value = '1'
                                                INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'")
                         .where(parent_id: Setting.avp_project_id, status: 1)
    render json: departments.to_json #(methods: :audit)
  end

  def all_departments
    departments = Project.joins(" INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                                    AND custom_values.customized_id = projects.id AND custom_values.value = '1'
                                  INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'")
                         .where(parent_id: Setting.avp_project_id, status: 1)
    render json: departments.to_json
  end

  def departments_with_hierarchy
    departments = Project.joins("INNER JOIN members m ON projects.id = m.project_id AND m.user_id = #{User.current.id}
                                  INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                                                          AND custom_values.customized_id = projects.id AND custom_values.value = '1'
                                  INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'")
                         .where(parent_id: Setting.avp_project_id, status: 1)
    results = []

    departments.each do |dep|
      department = {}
      department['id'] = dep[:id]
      department['name'] = dep[:name]
      department['children'] = Project.joins("INNER JOIN members m ON projects.id = m.project_id AND m.user_id = #{User.current.id}")
                                      .where(parent_id: dep.id).as_json(only: %i[id name])

      results << department unless department['children'].blank?
    end
    render json: results
  end

  def none_department_projects
    projects = Project.joins("INNER JOIN members m ON projects.id = m.project_id AND m.user_id = #{User.current.id}
                               INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                                                          AND custom_values.customized_id = projects.id AND custom_values.value > 1
                               INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'")
                      .where(parent_id: Setting.avp_project_id, status: [0,1])
    render json: projects.to_json(only: %i[id name status parent_id])
  end

  def create_category
    ActiveRecord::Base.transaction do
      payload = params[:avp]
      begin
        c = Category.new do |category|
          category.project_id = payload[:projectId]
          category.name = payload[:name]
        end

        if c.save
          project = Project.find(c.project_id)
          receivers = project.possible_assignees.as_json(only: %i[mail])

          render json: { category: c, project: project, receivers: receivers }
        end
      rescue => e
        puts e
        render_error status: 422
        raise ActiveRecord::Rollback
      end
    end
  end

  def edit_category
    id = params[:id]

    ActiveRecord::Base.transaction do
      begin
        category = Category.find(id)
        category.name = params[:avp][:name]

        category.save

        render json: category.to_json
      rescue => e
        render json: {
          status: 'failure',
          message: e.inspect
        }
      end
    end
  end

  def delete_category
    id = params[:id]

    ActiveRecord::Base.transaction do
      begin
        category = Category.find(id)
        WorkPackage.where(category_id: category.id).destroy_all
        category.destroy

        render json: {
          status: 'success'
        }

      rescue => e
        render json: {
          status: 'failure',
          message: e.inspect
        }
      end
    end
  end

  def updateMemberships
    payload = params[:avp]
    project = Project.find(payload[:project_id])

    update_membership(project, payload[:member])

    render json: {status: 'Success'}
  end

  def deleteMemberships
    result = {}
    ActiveRecord::Base.transaction do
      member_id = params[:id]
      member = Member.find(member_id)

      result = member.destroy
      notify_member_deleted(member.project, member)
    end

    render json: result
  end

  def posible_projects
    join_string = ""
    
    unless (User.current.admin || User.current.avp_admin)
      join_string = "INNER JOIN members m ON projects.id = m.project_id AND m.user_id = #{User.current.id} "
    end

    join_string += "INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                      AND custom_values.customized_id = projects.id AND custom_values.value > 1
                    INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'"

    results = Project.joins(join_string).where(status: [1, 0])
    render json: results.to_json(only: %i[id name])
  end

  def projects
    filters = params[:filters] 
    isAll = params[:m] == "true" && (User.current.admin || User.current.avp_admin)
    join_string = (isAll == false ? 
                  " INNER JOIN members m ON projects.id = m.project_id AND m.user_id = #{User.current.id} " : 
                  "")

    join_string += " INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                                                AND custom_values.customized_id = projects.id AND custom_values.value > 1
                      INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'"

    where_string = "status != 9"
    where_string += " and (select count(*) 
                            from custom_values cv 
                            inner join custom_fields cf on cf.id = cv.custom_field_id and cf.name = 'Project of' 
                            where cv.customized_type = 'Project' and cv.customized_id = projects.id and cv.value IS NOT NULL) = 0" if isAll == true
    if filters.blank?
      where_string += ""
    else
      filters = JSON.parse(filters)
      where_string += filters.key?('parent_id') ? " and parent_id = #{filters['parent_id']}" : ""
      where_string += filters.key?('status') ? " and status = #{filters['status']}" : ""
      filters.each do |key, value|
        next if value.blank?
        next unless key != 'parent_id' and key != 'status'

        join_string += "INNER JOIN custom_values cv#{key} ON cv#{key}.customized_type = 'Project'
                                              AND cv#{key}.customized_id = projects.id AND cv#{key}.value = '#{value}'
                  INNER JOIN custom_fields cf#{key} ON cf#{key}.id = cv#{key}.custom_field_id AND cf#{key}.name = '#{key}' "
      end
    end

    index = params[:page]
    result = if index.blank?
                Project.joins(join_string).where(where_string)
    else
      Project.joins(join_string).where(where_string).limit(20).offset((index.to_i - 1) * 20)
    end

    render json: result.to_json(only: %i[id name description status updated_on parent_id],
                                methods: :permission, 
                                include: [:parent,
                                          custom_values: {
                                              only: %i[id value],
                                              include: [custom_field: {
                                                  only: %i[id name]
                                              }]
                                          },
                                          members: {
                                              include: [:roles,
                                                user: {
                                                  only: %i[id firstname lastname login], methods: :avatar
                                                }]
                                          },
                                          work_packages: {
                                              only: %i[subject status_id due_date]
                                          }])
  end

  def project_only
    join_string = (User.current.admin == false ? 
                  " INNER JOIN members m ON projects.id = m.project_id AND m.user_id = #{User.current.id} " : 
                  "")
    join_string += " INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                                          AND custom_values.customized_id = projects.id AND custom_values.value > 1
                    INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'"
    results = Project.joins(join_string).where.not(status: 9)

    render json: results.to_json(only: %i[id name description],
                                 methods: :permission)
  end

  def related_projects
    join_string = " INNER JOIN members m ON projects.id = m.project_id AND m.user_id = #{User.current.id} "
    join_string += " INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                                          AND custom_values.customized_id = projects.id AND custom_values.value > 1
                    INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'"
    results = Project.joins(join_string).where.not(status: 9)

    render json: results.to_json(only: %i[id name description],
                                 methods: :permission)
  end

  def getProject
    id = params[:id]
    project = Project.find(id)
    render json: project.to_json(only: %i[id name identifier status description updated_on],
                                 include: [:parent,
                                           custom_values: {
                                               only: %i[id value],
                                               include: [custom_field: {
                                                   only: %i[id name]
                                               }]
                                           },
                                           members: {
                                               include: [:roles,
                                                         user: {
                                                           only: %i[id firstname lastname login mail], methods: :avatar
                                                         }]
                                           },
                                           work_packages: {
                                               only: %i[id subject status_id due_date assigned_to_id author_id],
                                               include: [status: {
                                                   only: %i[id name]
                                               }]
                                           }])
  end

  def project_status
    project_id = params[:id]
    result = Project.find(project_id)
    render json: result.to_json(only: %i[status])
  end

  def project_roles
    id = params[:id]
    project = Project.find(id)
    roles = User.current.roles_for_project(project).map(&:name)

    render json: roles
  end

  def project_possible_assignees
    id = params[:id]
    possible_assignees = Project.find(id).possible_assignees
    fetched_users = self.class.account_users()
    elements = self.class.combine_with_account_users(fetched_users, possible_assignees)
    render json: {
      "_embedded": {
        "elements": elements
      }
    }
  end

  def updateProject
    project_id = params[:id]
    payload = params[:avp]
    ActiveRecord::Base.transaction do
      begin
        project = Project.find(project_id)

        send_notification, change_set = project_edited_notification(project, payload)
        
        # Update Parent Project
        project.parent_id = payload[:parent_id] if payload.key?('parent_id')
        project.description = payload[:description] if payload.key?('description')
        project.name = payload[:name] if payload.key?('name')
        project.identifier = payload[:identifier] if payload.key?('identifier')
        project.update_attribute :status, payload[:is_active] if payload.key?('is_active')
        project.updated_on = DateTime.now
        project.save

        # Update memberships
        if payload.key?('member')
          members = payload[:member]
          update_membership(project, members)
        end

        CustomValue.where(id: payload[:status][:custom_value_id])
                   .update_all(value: payload[:status][:custom_value_value]) if payload.key?('status')

        # Update DueDate
        CustomValue.where(id: payload[:dueDate][:custom_value_id])
                   .update_all(value: payload[:dueDate][:custom_value_value]) if payload.key?('dueDate')

        # Update StartDate
        CustomValue.where(id: payload[:startDate][:custom_value_id])
                   .update_all(value: payload[:startDate][:custom_value_value]) if payload.key?('startDate')

        update_reviewer(project, payload)

        handle_project_updated(project, change_set) if send_notification
        update_department_cache if project.is_department
      rescue => e
        ActiveRecord::Rollback
        puts e
        render_error status: 500
        return false
      end
    end

    # Fetch Project
    result = Project.find(project_id)

    render json: result.to_json(only: %i[id name identifier status description updated_on],
                                include: [:parent,
                                          custom_values: {
                                              only: %i[id value],
                                              include: [custom_field: {
                                                  only: %i[id name]
                                              }]
                                          },
                                          members: {
                                              include: [:roles,
                                                        user: {
                                                          only: %i[id firstname lastname login mail], methods: :avatar
                                                        }]
                                          },
                                          work_packages: {
                                              only: %i[id subject status_id due_date assigned_to_id author_id],
                                              include: [status: {
                                                  only: %i[id name]
                                              }]
                                          }])
  end

  def update_project_status
    project_id = params[:id]
    payload = params[:avp]

    ActiveRecord::Base.transaction do
      begin
        project = Project.find(project_id)
        
        send_notification, change_set = project_edited_notification(project, payload)

        project.update_attribute :status, payload[:is_active]
        project.updated_on = DateTime.now
        project.save

        CustomValue.where(id: payload[:status][:custom_value_id])
                   .update_all(value: payload[:status][:custom_value_value]) if payload.key?('status')

        handle_project_updated(project, change_set) if send_notification
        update_department_cache if project.is_department
      rescue => e
        ActiveRecord::Rollback
        puts e
        render_error status: 500
        return false
      end
    end

    # Fetch Project
    result = Project.find(project_id)

    render json: result.to_json(only: %i[id name status description updated_on],
                                include: [:parent,
                                          custom_values: {
                                              only: %i[id value],
                                              include: [custom_field: {
                                                  only: %i[id name]
                                              }]
                                          },
                                          members: {
                                              include: [:roles,
                                                        user: {
                                                          only: %i[id firstname lastname avatar login]
                                                        }]
                                          },
                                          work_packages: {
                                              only: %i[subject status_id due_date]
                                          }])
  end

  def createProject
    ActiveRecord::Base.transaction do
      payload = params[:avp]
      begin
        project = Project.new(name: payload[:name],
                              identifier: payload[:identifier],
                              description: payload[:description])
        project.parent_id = payload[:parent_id] if payload.key?('parent_id')
        if project.save
          add_memberships_to_project(project)
          add_custom_fields_to_project(project)
          create_project_for_department(project) if params[:avp][:level] == '1'
          generate_default_forum(project)

          update_department_cache if project.is_department
        end
      rescue => e
        puts e
        render_error status: 500
        raise ActiveRecord::Rollback
      end

      unless project.blank?
        render json: project.to_json(only: %i[id name status description updated_on],
                                     include: [:parent,
                                               custom_values: {
                                                   only: %i[id value],
                                                   include: [custom_field: {
                                                       only: %i[id name]
                                                   }]
                                               },
                                               members: {
                                                   include: [:roles, 
                                                             user: {
                                                               only: %i[id firstname lastname login mail], methods: :avatar
                                                             }]
                                               },
                                               work_packages: {
                                                   only: %i[subject status_id due_date]
                                               }])
      end
    end
  end

  def deleteProject
    id = params[:id]
    project = Project.find(id)
    is_department = project.is_department

    service = ::Projects::DeleteProjectService.new(user: User.current, project: project)
    call = service.call(delayed: false)

    if call.success?
      Rails.logger.info 'Delete project successfully.'
      update_department_cache if is_department
    else
      Rails.logger.error I18n.t('projects.delete.schedule_failed', errors: call.errors.full_messages.join("\n"))
      render_error status: 500
    end
  end

  def project_permissions
    id = params[:id]
    results = CustomValue.joins(" INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id 
                                    AND custom_fields.type = 'ProjectCustomField'
                                    AND custom_fields.name IN ('project_permissions', 'workpackage_permissions')")
                          .where(customized_type: 'Project', customized_id: params[:id])
    render json: results.to_json( only: %i[id value],
                                  include: [ custom_field: {
                                              only: %i[id name]
                                            }])
  end

  def update_project_permissions
    field_id = params[:fieldId]
    if field_id != '0'
      permission = CustomValue.find(field_id)
      permission.value = params[:avp][:value]
      permission.save
    else
      permission = CustomValue.new
      permission.custom_field_id = CustomField.find_by(name: params[:avp][:permission_name]).id
      permission.customized_type = 'Project'
      permission.customized_id = params[:id]
      permission.value = params[:avp][:value]
      
      permission.save
    end

    project_permissions
  end

  def project_email_configs
    id = params[:id]
    result = CustomValue.joins(" INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id 
                                    AND custom_fields.type = 'ProjectCustomField'
                                    AND custom_fields.name IN ('email_configs')")
                          .find_by(customized_type: 'Project', customized_id: params[:id])

    render json: result.to_json( only: %i[id value],
                                  include: [ custom_field: {
                                              only: %i[id name]
                                            }])
  end

  def update_project_email_configs
    field_id = params[:fieldId]
    if field_id != '0'
      permission = CustomValue.find(field_id)
      permission.value = params[:avp][:value]
      permission.save
    else
      permission = CustomValue.new
      permission.custom_field_id = CustomField.find_by(name: 'email_configs').id
      permission.customized_type = 'Project'
      permission.customized_id = params[:id]
      permission.value = params[:avp][:value]
      
      permission.save
    end

    project_email_configs
  end

  def project_permissions_by_current_user
    project_id = params[:id]
    project = Project.find(project_id)
    permission = project.permission

    render json: permission
  end

  def avp_logout
    mail = params[:email]
    user = User.find_by(mail: mail)

    body = {
      token: cookies[:_open_project_session_access_token]
    }
    # uri = URI.parse(Setting.avp_account_url + "/api/pri/keycloak/user/logout")
    # http = Net::HTTP.new(uri.host, uri.port)
    # res = http.post(uri, body.to_json, {'Content-Type': 'application/json'})
    
    cookies.delete OpenProject::Configuration['autologin_cookie_name']
    Token::AutoLogin.where(user_id: user.id).delete_all
    self.logged_user = nil

    render json: {
      message: 'Logout successfully'
    }
    #redirect_to home_url
  end

  def get_departments
    departments = Project.joins(" INNER JOIN custom_values ON custom_values.customized_type = 'Project'
                            AND custom_values.customized_id = projects.id AND custom_values.value = '1'
                            INNER JOIN custom_fields ON custom_fields.id = custom_values.custom_field_id AND custom_fields.name = 'Level'")
                          .where(parent_id: Setting.avp_project_id, status: 1)

    results = []

    departments.each do |dep|
      department = {}
      department['id'] = dep[:id]
      department['name'] = dep[:name]
      users = []
      projects = Project.where(parent_id: dep.id)
      projects.each do |project|
        users << project.possible_assignees
      end

      department['members'] = (dep.possible_assignees + users).flatten(1).map(&:mail).uniq

      results << department
    end
    render json: results
  end

  def filter_properties
    id = params[:id]
    filters = Query.find(id).filters

    render json: filters.to_json
  end

  def last_work_package_created_date
    project_id = params[:id]
    last_wp = WorkPackage.where(project_id: project_id).last
    result = if last_wp.nil?
              nil
             else
              ::API::V3::Utilities::DateTimeFormatter.format_datetime(last_wp.created_at)
             end
    render json: { result: result }
  end

  private

  def add_memberships_to_project(project)
    members = params[:avp][:members]
    
    members.each do |user|
      if user[:id] != User.current.id
        r = Role.where(name: user[:roles])
        m = Member.new do |member|
          member.principal = User.find(user[:id])
          member.role_ids = r.map(&:id)
        end
        project.members << m
      end
    end
    
    m = Member.new do |member|
      member.principal = User.current
      member.role_ids = [3, 4]
    end
    project.members << m
  end

  def add_custom_fields_to_project(project)
    if params[:avp].key?('status')
      status = params[:avp][:status]
      cf = CustomField.find_by(type: 'ProjectCustomField', name: 'Status')
      cv_status = CustomValue.find_by(custom_field_id: cf.id, customized_id: project.id)
      cv_status.value = status
      cv_status.save
    end

    if params[:avp].key?('dueDate')
      due_date = params[:avp][:dueDate]
      cf = CustomField.find_by(type: 'ProjectCustomField', name: 'DueDate')
      cv_due_date = CustomValue.find_by(custom_field_id: cf.id, customized_id: project.id)
      cv_due_date.value = due_date
      cv_due_date.save
    end

    if params[:avp].key?('startDate')
      start_date = params[:avp][:startDate]
      cf = CustomField.find_by(type: 'ProjectCustomField', name: 'StartDate')
      cv_start_date = CustomValue.find_by(custom_field_id: cf.id, customized_id: project.id)
      cv_start_date.value = start_date
      cv_start_date.save
    end

    if params[:avp].key?('isInternal')
      is_internal = params[:avp][:isInternal]
      cf = CustomField.find_by(type: 'ProjectCustomField', name: 'IsInternal')
      cv_is_internal = CustomValue.find_by(custom_field_id: cf.id, customized_id: project.id)
      cv_is_internal.value = is_internal
      cv_is_internal.save
    end

    if params[:avp].key?('level')
      level = params[:avp][:level]
      cf = CustomField.find_by(type: 'ProjectCustomField', name: 'Level')
      cv_level = CustomValue.find_by(custom_field_id: cf.id, customized_id: project.id)
      cv_level.value = level
      cv_level.save
    end

    if params[:avp].key?('review_required')
      review_required = params[:avp][:review_required]
      cf = CustomField.find_by(type: 'ProjectCustomField', name: 'review_required')
      cv_review_required = CustomValue.find_by(custom_field_id: cf.id, customized_id: project.id)
      cv_review_required.value = review_required
      cv_review_required.save
    end

    if params[:avp].key?('reviewer_as_assigner')
      reviewer_as_assigner = params[:avp][:reviewer_as_assigner]
      cf = CustomField.find_by(type: 'ProjectCustomField', name: 'reviewer_as_assigner')
      cv_reviewer_as_assigner = CustomValue.find_by(custom_field_id: cf.id, customized_id: project.id)
      cv_reviewer_as_assigner.value = reviewer_as_assigner
      cv_reviewer_as_assigner.save
    end

    if params[:avp].key?('reviewer')
      reviewer = params[:avp][:reviewer]
      cf = CustomField.find_by(type: 'ProjectCustomField', name: 'reviewer')
      cv_reviewer = CustomValue.find_by(custom_field_id: cf.id, customized_id: project.id)
      cv_reviewer.value = reviewer
      cv_reviewer.save
    end

    # audit = {
    #   creator: User.current.id,
    #   time: DateTime.now()
    # }

    # audit_cf = CustomField.find_by(type: 'ProjectCustomField', name: 'audit')
    # audit_value = CustomValue.find_by(custom_field_id: audit_cf.id, customized_id: project.id)    
    # audit_value.value = JSON.unparse(audit)
    # audit_value.save
  end

  def create_project_for_department(department)
    project = Project.new(name: 'Công việc của ' + department.name,
      identifier: 'cong-viec-cua-' + department.identifier,
      parent_id: department.id)

    if project.save
      add_memberships_to_project(project)

      cf_level = CustomField.find_by(type: 'ProjectCustomField', name: 'Level')
      cv_level = CustomValue.find_by(custom_field_id: cf_level.id, customized_id: project.id)
      cv_level.value = 2
      cv_level.save

      cf_status = CustomField.find_by(type: 'ProjectCustomField', name: 'Status')
      cv_status = CustomValue.find_by(custom_field_id: cf_status.id, customized_id: project.id)
      cv_status.value = 'on-track'
      cv_status.save
    end
  end

  def update_membership(project, members)
    ActiveRecord::Base.transaction do
      members[:tobeDeleted].each do |p|
        deleted_roles = []
        p[:roles].each do |role|
          deleted_roles << (role == 'admin' ? 3 : 4)
        end
        m = Member.find_by(user_id: p[:user_id], project_id: project.id)
        new_roles = m.role_ids.uniq - deleted_roles
        m.role_ids = new_roles
        puts new_roles.blank?
        notify_member_deleted(project, m) if new_roles.blank?
        m.save
      end

      members[:tobeAdded].each do |p|
        added_roles = []
        p[:roles].each do |role|
          added_roles << (role == 'admin' ? 3 : 4)
        end
        m = Member.find_by(user_id: p[:user_id], project_id: project.id)
        m = Member.new do |member|
          member.principal = User.find(p[:user_id])
          member.role_ids = []
        end if m.nil?

        m.role_ids = (added_roles + m.role_ids).uniq
        if m.new_record?
          project.members << m
          notify_member_added(project, m)
        else
          m.save
        end
      end

    end
  end

  def notify_member_added(project, new_member)
    description = {
      stateName: "project",
      params: {projectId: project.id},
      content: ""
    }

    body = {
      ownerEmail: User.current.mail,
      targetEmail: new_member.principal.mail,
      title: "<b>[#{User.current.name}]</b> đã thêm bạn vào dự án <b>[#{project.name}]</b>",
      description: JSON.unparse(description),
      datetime: Time.now.to_i,
      status: 0
    }
    uri = URI.parse(Setting.avp_sso_url + "/api/pri/notifications?email=#{User.current.mail}")
    http = Net::HTTP.new(uri.host, uri.port)
    res = http.post(uri, body.to_json, {'Content-Type': 'application/json'})

    case res
    when Net::HTTPSuccess, Net::HTTPRedirection
      # OK
      puts 'OK'
    else
      puts 'Fail'
    end
  end

  def notify_member_deleted(project, deleted_member)
    description = {
      stateName: "projects",
      params: {},
      content: ""
    }

    body = {
      ownerEmail: User.current.mail,
      targetEmail: deleted_member.principal.mail,
      title: "<b>[#{User.current.name}]</b> đã xóa bạn khỏi dự án <b>[#{project.name}]</b>",
      description: JSON.unparse(description),
      datetime: Time.now.to_i,
      status: 0
    }
    uri = URI.parse(Setting.avp_sso_url + "/api/pri/notifications?email=#{User.current.mail}")
    http = Net::HTTP.new(uri.host, uri.port)
    res = http.post(uri, body.to_json, {'Content-Type': 'application/json'})

    case res
    when Net::HTTPSuccess, Net::HTTPRedirection
      # OK
      puts 'OK'
    else
      puts 'Fail'
    end
  end

  def project_edited_notification(project, payload)
    change_set = {}
    send_notification = false
    email_configs = project.email_configs
    return false, nil if email_configs[:PROJECT][:EDITED] == false

    if payload.key?('name') && project.name != payload[:name]
      change_set[:name] = {
        old: project.name,
        new: payload[:name]
      }
      send_notification = true
    end

    if payload.key?('description') && project.description != payload[:description]
      change_set[:description] = {
        old: project.description,
        new: payload[:description]
      }
      send_notification = true
    end

    change_set[:is_active] = {
      old: project.status == 1 ? 'Đang hoạt động' : 'Đóng',
      new: project.status == 1 ? 'Đang hoạt động' : 'Đóng',
    }
    if payload.key?('is_active')
      change_set[:is_active][:new] = payload[:is_active] == 1 ? 'Đang hoạt động' : 'Đóng'
      send_notification = true if project.status != payload[:is_active]
    end

    custom_status = project.custom_status
    status = custom_status.value if custom_status
    change_set[:status] = {
      old: status == 'on-track' ? 'Đúng tiến độ' : status == 'off-track' ? 'Chậm tiến độ' : 'Rủi ro cao',
      new: status == 'on-track' ? 'Đúng tiến độ' : status == 'off-track' ? 'Chậm tiến độ' : 'Rủi ro cao',
    }
    if payload.key?('status')
      change_set[:status][:new] = payload[:status][:custom_value_value] == 'on-track' ? 'Đúng tiến độ' : status == 'off-track' ? 'Chậm tiến độ' : 'Rủi ro cao'
      send_notification = true if status != payload[:status][:custom_value_value]
    end

    custom_due_date = project.custom_due_date
    due_date = custom_due_date.value if custom_due_date
    change_set[:due_date] = {
      old: due_date,
      new: due_date
    }
    if payload.key?('dueDate')
      change_set[:due_date][:new] = payload[:dueDate][:custom_value_value]
      send_notification = true if due_date != payload[:dueDate][:custom_value_value]
    end

    custom_start_date = project.custom_start_date
    start_date = custom_start_date.value if custom_start_date
    change_set[:start_date] = {
      old: start_date,
      new: start_date
    }
    if payload.key?('startDate')
      change_set[:start_date][:new] = payload[:startDate][:custom_value_value]
      send_notification = true if start_date != payload[:startDate][:custom_value_value]
    end

    return send_notification, change_set
  end

  def handle_project_updated(project, change_set)
    members = project.users
    members.each do |member|
      ProjectMailer.project_updated(member, project, change_set, User.current).deliver_now
    end
  end

  def update_reviewer(project, payload)
    if payload.key?('review_required')
      review_required = payload[:review_required]
      review_required_cf = CustomField.find_by(type: 'ProjectCustomField', name: 'review_required')
      review_required_cv = CustomValue.find_by(custom_field_id: review_required_cf.id, customized_id: project.id, customized_type: 'Project')
      review_required_cv.value = review_required[:custom_value_value]
      review_required_cv.save
    end

    if payload.key?('reviewer_as_assigner')
      reviewer_as_assigner = payload[:reviewer_as_assigner]
      reviewer_as_assigner_cf = CustomField.find_by(type: 'ProjectCustomField', name: 'reviewer_as_assigner')
      reviewer_as_assigner_cv = CustomValue.find_by(custom_field_id: reviewer_as_assigner_cf.id, customized_id: project.id, customized_type: 'Project')
      reviewer_as_assigner_cv.value = reviewer_as_assigner[:custom_value_value]
      reviewer_as_assigner_cv.save
    end

    if payload.key?('reviewer')
      reviewer = payload[:reviewer]
      reviewer_cf = CustomField.find_by(type: 'ProjectCustomField', name: 'reviewer')
      reviewer_cv = CustomValue.find_by(custom_field_id: reviewer_cf.id, customized_id: project.id, customized_type: 'Project')
      reviewer_cv.value = reviewer[:custom_value_value]
      reviewer_cv.save
    end
  end
  
  def generate_default_forum(project)
    forum = Forum.new(name: project.name, description: project.name, position: 1)
    forum.project = project

    forum.save
  end

  def load_current_user_info
    uri = URI.parse(Setting.avp_sso_url + "/api/pri/internal/userinfo?email=#{User.current.mail}")
    http = Net::HTTP.new(uri.host, uri.port)
    res = http.get(uri)

    case res
    when Net::HTTPSuccess
      info = JSON.parse(res.body)
      User.current.title = info['title']
    else
      nil
    end
  end

  def update_department_cache
    job = TriggerDepartmentCacheJob.new
    #job.perform
    Delayed::Job.enqueue job
  end

  def self.combine_with_account_users(fetched_users, users)
    elements = []

    users.each do |user|  
      fetched = fetched_users['items'].detect { |fu| fu['email'] == user['mail'] }
      
      unless fetched.nil?
        user['lastname'].strip!
        user['firstname'].strip!

        elements << {
            id: user['id'],
            firstName: user['firstname'],
            lastName: user['lastname'],
            login: user['login'],
            mail: user['mail'],
            name: "#{user['lastname']} #{user['firstname']}",
            avatar: fetched['attributes']['avp_avatar'][0],
            title: fetched['attributes']['avp_title'][0],
            username: fetched['username']
        } if fetched['enabled'] == true

        if fetched['enabled'] == true && !user.active?
          user.activate
        elsif fetched['enabled'] == false && user.active?
          user.lock
        end
      end
    end

    elements
  end

  def self.account_users
    uri = URI.parse(Setting.avp_sso_url + "/api/pri/keycloak/users?offset=0&size=1024")
    http = Net::HTTP.new(uri.host, uri.port)
    res = http.get(uri)

    case res
    when Net::HTTPSuccess
      JSON.parse(res.body)
    else
      nil
    end
  end
end