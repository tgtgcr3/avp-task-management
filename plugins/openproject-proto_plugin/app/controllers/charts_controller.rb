class ChartsController < ApplicationController
  before_action :find_optional_project

  def index
    render layout: true
  end

end
