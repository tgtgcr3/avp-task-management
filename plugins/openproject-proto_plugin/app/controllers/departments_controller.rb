class DepartmentsController < ApplicationController
  before_action :find_optional_project

  def index
    project_name = params['project_id']
    parent_project = Project.find_by "identifier = ?", project_name
    @child_projects = []
    if parent_project != nil
      @child_projects = Project.where("parent_id = " + parent_project.id.to_s)
    end
    render layout: true
  end

end
