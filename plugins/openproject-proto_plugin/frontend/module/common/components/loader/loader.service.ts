import {Injectable, EventEmitter} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  public onLoadingEvent: EventEmitter<any> = new EventEmitter<any>();
  private stacked: string[] = [];
  public loading(loading: boolean, message: string = "Đang xử lý") {
    if(loading) {
      this.stacked.push(message);
    }
    else {
      this.stacked.pop();
    }

    if((this.stacked.length == 1 && loading)
    || ((this.stacked.length == 0 && !loading))) {
      this.onLoadingEvent.emit({loading: loading, message: message});
    }
  }
}