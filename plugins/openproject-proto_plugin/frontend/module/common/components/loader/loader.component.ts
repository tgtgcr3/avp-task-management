import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { NgbModalConfig, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService } from "./loader.service";
import { DynamicBootstrapper } from "core-app/globals/dynamic-bootstrapper";

@Component({
  template: `
    <ng-template #loader let-modal>
      <div class="__dialogcontent">
        <i class="fa fa-spinner fa-spin" style="font-size: 32px;"></i>
        <div class="text">{{message}}</div>
      </div>
    </ng-template>`,
  selector: 'loader'
})
export class LoaderComponent implements OnInit, OnDestroy {
  @ViewChild('loader') loaderModal: ElementRef;

  public message: string = "";
  private subscription: any;
  private isLoading: boolean = false;
  private modalRef: any;
  constructor(private config: NgbModalConfig,
    private modalService: NgbModal,
    private loaderService: LoaderService) { }

  ngOnInit() {
    this.loaderService.onLoadingEvent.subscribe(this.onLoadingChanged.bind(this));
  }

  onLoadingChanged(changes: any) {
    setTimeout(this.updateLoader.bind(this, changes));
  }

  private updateLoader(changes: any) {
    if (changes.loading == true && this.isLoading == false) {
      this.isLoading = true;
      this.config.backdrop = 'static';
      this.config.keyboard = false;
      this.message = changes.message;
      if (!this.modalRef)
        this.modalRef = this.modalService.open(this.loaderModal, { windowClass: 'avp-loader', centered: true });
    }
    else {
      this.isLoading = false;
      this.config.backdrop = true;
      this.config.keyboard = true;
      this.modalRef.close();
      this.modalRef = null;
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

DynamicBootstrapper.register({ selector: 'loader', cls: LoaderComponent });