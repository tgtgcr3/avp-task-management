import { Injectable, EventEmitter } from '@angular/core';
import { NotificationsService } from "core-app/modules/common/notifications/notifications.service";

@Injectable({
  providedIn: 'root'
})
export class ErrorDialogService {
  constructor(
    private notificationsService:NotificationsService) {
  }
  
  public show(message: any) {
    this.notificationsService.addError(message);
  }

  public showSuccess(message: any = '') {
    this.notificationsService.addSuccess(message);
  }
}