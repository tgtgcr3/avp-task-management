import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MembersService } from "../../../members/services/members.service";
import { AVP, noAccents } from '../../consts/avp';
import { AvpUtils } from '../../services/avp-utils.service';

@Component({
  templateUrl: './users-selector.component.html',
  selector: 'users-selector'
})
export class UsersSeletorComponent implements OnInit {
  @Input() title: string = 'Giao cho';
  @Input() removable: boolean = true;
  @Input() projectId: number;
  @Input() externableUser: boolean = true;
  @Output() onUserSelected: EventEmitter<number> = new EventEmitter();
  @Output() onOpenExternalUserSelector: EventEmitter<any> = new EventEmitter();

  public users: any = {
    list: [],
    searching: []
  };

  constructor(private membersService: MembersService,
    private avpUtils: AvpUtils) { }

  async ngOnInit() {
    if (this.projectId) {
      let response = await this.membersService.byProject(this.projectId);
      this.users.list = response._embedded.elements;
      this.users.searching = response._embedded.elements.map((u: any) => {
        return { id: u.id, name: u.name, avatar: u.avatar, title: u.title };
      });
    }
    else {
      await this.membersService.all().subscribe(
        (response: any) => {
          this.users.list = response._embedded.elements;
          this.users.searching = response._embedded.elements.map((u: any) => {
            return { id: u.id, name: u.name, avatar: u.avatar, title: u.title };
          });
        },
        (error: any) => {
          console.error(error);
        });
    }
  }

  public openExternalSelector() {
    this.onOpenExternalUserSelector.emit();
  }

  public searchUsers(event: any) {
    let searchString = noAccents(event.target.value);
    let searchingResult: any[] = this.users.list.filter((p: any) => noAccents(p.name).includes(searchString));
    this.users.searching = searchingResult.map((u: any) => {
      return { id: u.id, name: u.name, avatar: u.avatar, title: u.title };
    });
  }

  public onSelect(user: any) {
    this.onUserSelected.emit(user.id);
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }
}