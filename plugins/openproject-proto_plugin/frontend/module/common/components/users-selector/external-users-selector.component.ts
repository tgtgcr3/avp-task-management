import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MembersService } from "../../../members/services/members.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AvpUtils } from '../../services/avp-utils.service';
import { AVP, noAccents } from '../../consts/avp';

@Component({
  template: `
  <div class="modal-header">
    <h6 class="modal-title">Chọn một người để giao việc</h6>
    <button type="button" class="close" aria-label="Đóng" (click)="close()">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="custom-selection">
    <div class="isearch">
      <input type="text" placeholder="Tìm kiếm nhanh" (input)="searchUsers($event)">
    </div>
    <div class="rh" style="max-height: 550px; overflow-y: auto; min-height: 100px">
      <div class="list list-actions with-image -border">
        <div class="js-li li item unselectable" *ngFor="let user of showingUsers" (click)="select(user)">
          <div class="image">
            <div class="avatar">
              <img src="{{user.avatar}}" (error)="onAvatarFailed($event)">
            </div>
          </div>
          <b>{{user.name}}</b>
          <div class="sub">{{user.title}}</div>
        </div>
      </div>
    </div>
  </div>
  `,
  selector: 'external-users-selector'
})
export class ExternalUsersSelectorComponent implements OnInit {

  @Output() onSelect: EventEmitter<any> = new EventEmitter<any>();
  @Output() onClose: EventEmitter<any> = new EventEmitter<any>();

  public users: any[] = [];
  public showingUsers: any[] = [];

  constructor(private membersService: MembersService,
    private modalService: NgbModal,
    private avpUtils: AvpUtils) { }

  ngOnInit() {
    this.loadUsers();
  }

  public select(user: any) {
    this.onSelect.emit(user.id);
    this.modalService.dismissAll();
  }

  public close() {
    this.onClose.emit();
  }

  public searchUsers(event: any) {
    let searchString = noAccents(event.target.value);
    this.showingUsers = this.users.filter((user: any) => {
      return noAccents(user.name).includes(searchString)
        || noAccents(user.login).includes(searchString);
    });
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private loadUsers() {
    this.membersService.all().subscribe(
      (response: any) => {
        this.users = response._embedded.elements;
        this.showingUsers = response._embedded.elements;
      },
      (error: any) => {
        console.log(error);
      });
  }
}