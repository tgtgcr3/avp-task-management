import { Component, Input, Output, OnInit, OnChanges, EventEmitter, SimpleChanges } from '@angular/core';
import { MembersService } from "../../../members/services/members.service";
import { AvpUtils } from '../../services/avp-utils.service';
import { AVP, noAccents } from '../../consts/avp';

@Component({
  templateUrl: './user-search-box.component.html',
  selector: 'user-search-box'
})
export class UserSearchBoxComponent implements OnInit, OnChanges {
  @Input() selectedUserIds: number[] = [];
  @Input() projectId: number;
  @Input() multiple: boolean = false;
  @Input() placeHolder: string;
  @Input() clearable: boolean = true;
  @Input() appendTo: string = '';
  @Input() disabled: boolean = false;
  @Output() selectItem: EventEmitter<any> = new EventEmitter<any>();

  public selectedUsers: any;
  public users: any[] = [];
  private initialized: boolean = false;

  constructor(private memberService: MembersService) { }

  async ngOnInit() {
    this.initialized = true;
    await this.loadMembers(this.projectId);
  }

  async ngOnChanges(changes: SimpleChanges) {
    if(this.initialized) {
      if(changes.projectId) {
        if(changes.projectId.previousValue != changes.projectId.currentValue) {
          let newProjectId = changes.projectId.currentValue;
          await this.loadMembers(newProjectId);
        }
      }
    }
  }

  async loadMembers(projectId: number | null) {
    if(projectId) {
      const response = await this.memberService.byProject(projectId);
      this.onLoadUserSuccess(response);
    }
    else {
      this.memberService.all().subscribe(
        this.onLoadUserSuccess.bind(this),
        this.errorHandle.bind(this))
    }
  }

  onSelectItem(item: any) {
    this.selectItem.emit(this.selectedUsers);
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public customSearchFn(term: string, item: any) {
    term = noAccents(term);
    return item.name.toLowerCase().indexOf(term) > -1
      || noAccents(item.name).indexOf(term) > -1
      || (item.mail && item.mail.toLowerCase().indexOf(term) > -1);
  }

  private onLoadUserSuccess(response: any) {
    this.users = response._embedded.elements;
    if (this.selectedUserIds && this.selectedUserIds.length > 0) {
      let initUsers = this.users.filter((user: any) => {
        return this.selectedUserIds.some((id: number) => id == user.id);
      });

      if (this.multiple == true) {
        this.selectedUsers = initUsers;
      }
      else {
        this.selectedUsers = initUsers[0];
      }
      this.onSelectItem(null);
    }
  }

  private errorHandle(error: any) {
    console.error(error.message);
  }
}