import { Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

@Component({
  template: `
    <div class="cform">
      <div class="c-title">{{title}}</div>
      <div class="side" >
        <span class="action url" [title]="removeButtonText" (click)="onRemove()">{{removeButtonText}}</span>
      </div>
      <div class="extra" *ngIf="type == 'start_date'">
        <div class="row" style="padding-right: 15px">
          <div class="input" style="width: 100%">
            <input type="text" placeholder="Chọn ngày" 
            [(ngModel)]="dateModel" 
            ngbDatepicker #sd="ngbDatepicker" 
            (focus)="sd.open()" (click)="sd.open()"
            navigation="arrows" (dateSelect)="onSaveChange($event)">
          </div>
        </div>
<!--        <div class="sep-10"></div>-->
<!--        <div class="row">-->
<!--          <div class="input" style="margin-right: 10px;">-->
<!--            <ngb-timepicker [(ngModel)]="timeModel"></ngb-timepicker>-->
<!--          </div>-->
<!--          <div class="save" style="margin-top: 37px" (click)="onSaveChange($event)">Lưu</div>-->
<!--        </div>-->
      </div>
      <div class="extra" *ngIf="type == 'due_date'">
        <div class="row" style="padding-right: 15px">
          <div class="input" style="width: 100%">
            <input type="text" placeholder="Chọn ngày" 
              [(ngModel)]="dateModel" 
              ngbDatepicker #sd="ngbDatepicker" 
              (focus)="sd.open()" (click)="sd.open()"
              navigation="arrows" (dateSelect)="onSaveChange($event)">
          </div>
        </div>
      </div>
    </div>
  `,
  selector: 'datetime-popover'
})
export class DatetimeComponent implements OnInit {
  @Input() type: string;
  @Input() date: string;

  @Output() onSave: EventEmitter<any> = new EventEmitter();

  public title: string;
  public removeButtonText: string;
  public dateModel: NgbDateStruct;
  public timeModel: {hour: number, minute: number} = {hour: 0, minute: 0};

  ngOnInit() {
    if(this.type == 'start_date') {
      this.title = 'Chọn tg bắt đầu';
      this.removeButtonText = 'Xóa tg bắt đầu';
    }
    else if (this.type == 'due_date') {
      this.title = 'Chọn tg kết thúc';
      this.removeButtonText = 'Xóa tg kết thúc';
    }
    let dateMoment = this.date != undefined ? moment(this.date) : moment();

    this.timeModel.hour = dateMoment.hour();
    this.timeModel.minute = dateMoment.minute();

    this.dateModel = {
      "day": dateMoment.date(),
      "month": dateMoment.month() + 1,
      "year": dateMoment.year()
    };

  }

  onSaveChange(evt: any) {
    let dateString = moment(`${evt.year}/${evt.month}/${evt.day}`).format('YYYY-MM-DD');
    this.onSave.emit(dateString);
  }

  onRemove() {
    this.onSave.emit(null);
  }
}