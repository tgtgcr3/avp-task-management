export const HREFS: any = {
  SCHEMA: '/api/v3/queries/filter_instance_schemas',
  FILTER: '/api/v3/queries/filters',
  OPERATOR: '/api/v3/queries/operators'
};

export const FILTERS: any = [
  // {
  //   id: 'custom',
  //   text: 'Loại công việc'
  // },
  {
    id: 'subject',
    text: 'Tên công việc'
  },
  {
    id: 'assignee',
    text: 'Giao cho'
  },
  {
    id: 'author',
    text: 'Được tạo bởi'
  },
  {
    id: 'status',
    text: 'Trạng thái'
  },
  {
    id: 'createdAt',
    text: 'Thời gian tạo'
  },
  {
    id: 'dueDate',
    text: 'Hoàn thành'
  },
  {
    id: 'startDate',
    text: 'Bắt đầu'
  },
  {
    id: 'customField14',
    text: 'Khẩn cấp'
  }
];

export const OPERATORS: any = {
  is: {
    id: '=',
    title: 'is',
    href: HREFS.OPERATOR + '/=',
    text: 'Là',
  },
  contains: {
    id: '~',
    title: 'contains',
    href: HREFS.OPERATOR + '/~',
    text: 'Chứa'
  },
  on: {
    id: '=d',
    title: 'on',
    href: HREFS.OPERATOR + '/=d',
    text: 'Vào ngày'
  },
  since: {
    id: '>t-',
    title: 'since',
    href: HREFS.OPERATOR + '/>t-',
    text: 'Trong khoảng',
    prefix: 'ngày trước'
  },
  open: {
    id: 'o',
    title: 'open',
    href: HREFS.OPERATOR + '/o',
  }
};

export const FILTER_OPERATORS: any = {
  custom: [OPERATORS.is],
  subject: [OPERATORS.contains],
  assignee: [OPERATORS.is],
  author: [OPERATORS.is],
  status: [OPERATORS.is],
  createdAt: [OPERATORS.on, OPERATORS.since],
  dueDate: [OPERATORS.on],
  startDate: [OPERATORS.on],
  customField13: [OPERATORS.is]
};

export const FILTER_OPERATOR_VALUES: any = {
  custom: {
    is: {
      values: [{
        value: 'Author',
        name: 'me',
        text: 'Công việc tôi tạo'
      },
      {
        name: 'me',
        value: 'Assignee',
        text: 'Công việc tôi được giao'
      }],
      type: 'single',
      isNonPrimitive: true
    }
  },
  subject: {
    contains: {
      values: [{
        value: ''
      }],
      type: 'text',
      isNonPrimitive: false
    }
  },
  assignee: {
    is: {
      values: [],
      type: 'Users',
      isNonPrimitive: true
    }
  },
  author: {
    is: {
      values: [],
      type: 'Users',
      isNonPrimitive: true
    }
  },
  status: {
    is: {
      values: [{
          name: 'New',
          value: '1',
          text: 'Phải làm'
        },
        {
          name: 'In progress',
          value: '7',
          text: 'Đang làm'
        },
        {
          name: 'Done',
          value: '16',
          text: 'Hoàn thành đúng hẹn'
        },
        {
          name: 'Done Late',
          value: '17',
          text: 'Hoàn thành muộn'
      }],
      type: 'multiple',
      isNonPrimitive: true
    }
  },
  createdAt: {
    on: {
      values: [],
      type: 'date',
      isNonPrimitive: false
    },
    since: {
      values: [],
      type: 'number',
      isNonPrimitive: false
    }
  },
  dueDate: {
    on: {
      values: [],
      type: 'date',
      isNonPrimitive: false
    }
  },
  startDate: {
    on: {
      values: [],
      type: 'date',
      isNonPrimitive: false
    }
  },
  customField14: {
    is: {
      values: [{
        name: 'Is Urgent',
        value: 't',
        text: 'Có'
      },
      {
        name: 'Not Urgent',
        value: 'f',
        text: 'Không'
      }],
      type: 'single',
      isNonPrimitive: false
    }
  }
};