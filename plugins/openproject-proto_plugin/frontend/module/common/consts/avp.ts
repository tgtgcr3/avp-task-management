export const AVP: any = {
  id: 7,
  user_default_img_url: '/assets/avp-icons/default_user.png'
}

export enum NotificationType {
  WP_START_DATE,
  WP_DUE_DATE,
  WP_ASSIGNED,
  WP_CREATE,
  WP_DELETE,
  WP_DESCRIPTION,
  WP_ATTACHMENT,
  WP_WATCHER,
  WP_COMMENT_TAGGED,
  WP_COMMENT,
  WP_STATUS,
  WP_URGENT,
  WP_RESULT,
  CATEGORY_CREATE,
  ADD_USER_TO_PROJECT,
  TOPIC_WATCHER_ADDED,
  POST_ADDED
}

const cleanAccents = (str: string): string => {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  // Combining Diacritical Marks
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, "");
  str = str.replace(/\u02C6|\u0306|\u031B/g, "");

  return str;
};

export function generateIdentifier(text: string) {
  let cleanName = cleanAccents(text.toLowerCase());
  return cleanName.replace(/[^a-zA-Z0-9]/g, '_');
}


export function noAccents(text: string) {
  let cleanName = cleanAccents(text.toLowerCase());
  return cleanName;
}

export function formatted_date(date: any) {
  return date ? date.replace(/\-/gi, '/') : date;
}

export const DEFAULT_PROJECT_PERMISSIONS : any = {
  CREATE_WP: {
    ADMIN: true,
    MEMBER: true
  },
  CREATE_CATEROGY: {
    ADMIN: true,
    MEMBER: false
  },
  VIEW_WP: {
    ADMIN: true,
    MEMBER: true
  },
  ASSIGN_WP: {
    ADMIN: true,
    MEMBER: false
  },
  VIEW_REPORT: {
    ADMIN: true,
    MEMBER: false
  }
}

export const DEFAULT_WP_PERMISSIONS : any = {
  EDIT_NAME: {
    AUTHOR: true,
    ASSIGNEE: true,
    WATCHER: false,
    OTHER: false
  },
  EDIT_STATUS: {
    AUTHOR: true,
    ASSIGNEE: true,
    WATCHER: false,
    OTHER: false
  },
  EDIT_DESCRIPTION: {
    AUTHOR: true,
    ASSIGNEE: true,
    WATCHER: false,
    OTHER: false
  },
  EDIT_TIME: {
    AUTHOR: true,
    ASSIGNEE: true,
    WATCHER: false,
    OTHER: false
  },
  EDIT_RESULT: {
    AUTHOR: true,
    ASSIGNEE: true,
    WATCHER: false,
    OTHER: false
  },
  EDIT_ASSIGNEE: {
    AUTHOR: true,
    ASSIGNEE: true,
    WATCHER: false,
    OTHER: false
  },
  DELETE_WP: {
    AUTHOR: true,
    ASSIGNEE: false,
    WATCHER: false,
    OTHER: false
  }
}

export const DEFAULT_EMAIL_NOTIFY : any = {
  WP: {
    ASSIGNED: true,
    EDITED: true,
    STATUS_CHANGED: true,
    DELETED: false
  },
  PROJECT: {
    EDITED: false,
    DELETED: false
  }
}

export const FILTER_STATUS_PROPERIES : any[] = [
  {
    value: "all",
    label: "Tất cả trạng thái",
    isDefault: true
  },
  {
    value: "not-done",
    label: "Chưa hoàn thành"
  },
  {
    value: "done",
    label: "Đã hoàn thành"
  },
  {
    value: "in-review",
    label: "Chờ review"
  },
  {
    value: "done-late",
    label: "HT muộn"
  },
  {
    value: "overdue",
    label: "Quá hạn"
  },
  {
    value: "urgent",
    label: "Khẩn cấp"
  },
  {
    value: "important",
    label: "Quan trọng"
  },
];

export const FILTER_TYPE_PROPERTIES : any[] = [
  {
    value: "assigneeOrAuthor",
    label: "CV tạo & được giao",
    isDefault: true
  },
  {
    value: "assignee",
    label: "CV được giao"
  },
  {
    value: "author",
    label: "CV đã tạo"
  },
];

export const DISPLAY_OPTIONS : any[] = [
  {
    value: "sub-tasks",
    label: "Một cấp công việc con",
    isDefault: true
  },
  {
    value: "all",
    label: "Công việc & công việc con"
  },
  {
    value: "no-sub-tasks",
    label: "Không có công việc con"
  }
];

export const ORDER_OPTIONS : any[] = [
  {
    value: "updatedAt",
    label: "Thời gian cập nhật",
    isDefault: true
  },
  {
    value: "createdAt",
    label: "Thời gian tạo",
  },
  {
    value: "dueDate",
    label: "Thời hạn hoàn thành",
  },
];

export const FILTER_PROJECT_DEFAULT : any = {
  value: "all",
  label: "Tất cả dự án",
  isDefault: true
};

export const FILTER_EMPLOYEE_DEFAULT : any = {
  value: "all",
  label: "Tất cả",
  isDefault: true
}

export const PROJECT_STATUSES : any[] = [
  {
    value: 'on-track',
    label: 'Đúng tiến độ'
  },
  {
    value: 'off-track',
    label: 'Chậm tiến độ'
  },
  {
    value: 'high-risk',
    label: 'Rủi ro cao'
  }
];

export const PROJECT_ACTIVATIONS : any[] = [
  {
    value: 0,
    label: 'Đóng'
  },
  {
    value: 1,
    label: 'Đang thực hiện'
  }
];

export const PROJECT_REVIEWING_OPTIONS : any[] = [
  {
    value: 'f',
    label: 'Không yêu cầu review'
  },
  {
    value: 't',
    label: 'Yêu cầu review'
  }
];

export const PROJECT_REVIEWER_OPTIONS : any[] = [
  {
    value: 't',
    label: 'Người review là người giao việc'
  },
  {
    value: 'f',
    label: 'Khác'
  }
]