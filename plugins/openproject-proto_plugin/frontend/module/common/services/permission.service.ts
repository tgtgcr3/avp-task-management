import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from "./base.service";
import { DEFAULT_WP_PERMISSIONS } from '../consts/avp';

@Injectable({
  providedIn: 'root'
})
export class PermissionService extends BaseService {
  private _currentUserPermissionPool: any = {};

  public async getCurrentUserPermission(projectId: string): Promise<any> {
    if (!this._currentUserPermissionPool || !this._currentUserPermissionPool[projectId]) {
      let requestUrl = this.restUrl + `/avp/api/v3/projects/${projectId}/user_permissions`;
      const permission = await this.httpClient.get<any>(requestUrl).toPromise();
      if (permission.wpPermission == null) {
        permission.wpPermission = DEFAULT_WP_PERMISSIONS;
      }
      this._currentUserPermissionPool[projectId] = permission;
    }

    return this._currentUserPermissionPool[projectId];
  }

  public async canAction(projectId: string, role: string, action: string) : Promise<boolean> {
    const permission = await this.getCurrentUserPermission(projectId);
    if(action == 'EDIT_ASSIGNEE') {
      if(permission.projectPermission.canAssignWP) {
        return await true;
      }
    }

    if(action == 'CREATE_WP') {
      return await permission.projectPermission.canCreateWP;
    }

    if(action == 'DELETE_WP' || action == 'EDIT_TIME') {
      if(this.currentUserService.role == 'owner') {
        return true;
      } 
    }

    return await permission.wpPermission[action][role];
  }

  public async canView(projectId: string, isResponsive: boolean) : Promise<boolean> {
    const permission = await this.getCurrentUserPermission(projectId);
    return await isResponsive || permission.projectPermission.canViewWP;
  }
}