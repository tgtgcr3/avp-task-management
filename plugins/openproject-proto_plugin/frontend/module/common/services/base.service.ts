import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "environments/environment";
import { CurrentUserService } from "core-components/user/current-user.service";
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { UserCacheService } from "core-components/user/user-cache.service";
import { ProjectCacheService } from 'core-components/projects/project-cache.service';
import { AvpUtils } from './avp-utils.service';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  protected restUrl: string;
  protected accountUrl: string;
  protected pathParams: string[];

  constructor(protected httpClient: HttpClient,
    readonly userCacheService: UserCacheService,
    readonly projectCacheService: ProjectCacheService,
    readonly currentUserService: CurrentUserService,
    protected avpUtils: AvpUtils,
    protected halResourceService: HalResourceService) {
    this.restUrl = environment.baseUrl;
    this.accountUrl = environment.avpAccountUrl;
  }

  protected getQueryParams(params: any): HttpParams {
    let httpParams: HttpParams = new HttpParams();
    var pathParams = this.pathParams || [];
    Object.keys(params).forEach((k) => {
      if (pathParams.indexOf(':' + k) < 0) {
        httpParams = httpParams.set(k, JSON.stringify(params[k]))
      }
    });

    return httpParams;
  }
}