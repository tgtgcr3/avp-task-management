import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "environments/environment";
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";

@Injectable({
  providedIn: 'root'
})
export class AvpUtils {

  constructor() {

  }

  getAvatarUrl(user: any): string {
    let userObj = user || {};
    return `/api/pri/avatar?email=${userObj.login}`;
  }

  getAvatarUrlByEmail(email: string): string {
    return `/api/pri/avatar?email=${email}`;
  }
}