import { Component, OnDestroy, OnInit } from '@angular/core';
import { ErrorDialogService, LoaderService } from "../../../common/components";
import { ProjectsService } from "../../../projects/services/projects.service";
import { AVP_VIEWS } from "../../../common/consts/view-name";
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './department-list.component.html',
  selector: 'departments'
})
export class DepartmentListComponent implements OnInit, OnDestroy {

  public views = AVP_VIEWS;
  public departments: any[] = [];
  public showingDeps: any[] = [];

  private onRefreshSubscription: Subscription;
  private onSearchSubscription: Subscription;

  constructor(private loaderService: LoaderService,
    private projectService: ProjectsService,
    private errorDialogService: ErrorDialogService) {
    this.onSearchSubscription = this.projectService.filterByNameEvent.subscribe(this.filter.bind(this));
    this.onRefreshSubscription = this.projectService.refreshEvent.subscribe(this.loadProjects.bind(this));
  }

  ngOnInit() {
    this.loadProjects();
  }

  ngOnDestroy() {
    this.onSearchSubscription && this.onSearchSubscription.unsubscribe();
    this.onRefreshSubscription && this.onRefreshSubscription.unsubscribe();
  }

  private filter(searchingText: string) {
    this.showingDeps = this.departments.filter((dep: any) => {
      return dep.name.toLowerCase().includes(searchingText.toLowerCase());
    });
  }

  private loadProjects() {
    this.loaderService.loading(true, "Loading");
    this.projectService.getDepartments().subscribe(
      (responses: any[]) => {
        this.departments = responses;
        this.showingDeps = responses;
        this.loaderService.loading(false);
      },
      this.errorHandle.bind(this)
    );
  }

  private errorHandle(error: any) {
    this.loaderService.loading(false,);
    this.errorDialogService.show("Lỗi dữ liệu");
  }
}