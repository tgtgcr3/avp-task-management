import { Component, OnInit, Input } from '@angular/core'
import { AVP } from '../../../../common/consts/avp';
import { AvpUtils } from "../../../../common/services/avp-utils.service";
import { ProjectsService } from '../../../../projects/services/projects.service';

@Component({
  templateUrl: './department-sidebar.component.html',
  selector: 'department-sidebar'
})
export class DepartmentSidebarComponent implements OnInit {

  @Input() department: any;

  public showingOwners: boolean = false;
  public showingGroupUsers: boolean = false;

  constructor(private avpUtils: AvpUtils,
    private projectService: ProjectsService) { }

  ngOnInit() {
    this.projectService.onUpdatedEvent.subscribe(this.update.bind(this));
  }

  public get admin() {
    let admin: any[] = [];
    if (this.department != undefined) {
      this.department.members.forEach((member: any) => {
        let isAdmin = member.roles.some((role: any) => {
          return role.name == 'Project admin';
        });
        if (isAdmin) {
          admin.push(member);
        }
      })
    }
    return admin;
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private update(department: any) {
    if (this.department.id == department.id) {
      this.department = department;
    }
  }
}