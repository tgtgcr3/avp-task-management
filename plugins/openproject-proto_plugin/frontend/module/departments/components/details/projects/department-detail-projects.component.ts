import {Component, OnInit} from '@angular/core';
import {StateService} from '@uirouter/core';

@Component({
  templateUrl: './department-detail-projects.component.html',
  selector: 'department-detail-projects'
})
export class DepartmentDetailProjectsComponent implements OnInit {

  public departmentId: number;
  constructor(readonly $state:StateService) { }

  ngOnInit() {
    this.departmentId = this.$state.params['departmentId'];
  }
}