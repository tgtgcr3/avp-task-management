import { Component, OnInit } from '@angular/core';
import { StateService } from '@uirouter/core';
import { ProjectsService } from "../../../projects/services/projects.service";
import { ErrorDialogService } from "../../../common/components";

@Component({
  templateUrl: './department-detail-master.component.html',
  selector: 'department-detail-master'
})
export class DepartmentDetailMasterComponent implements OnInit {

  private departmentId: number;
  private _department: any = undefined;
  public sideShowing: boolean = false;
  constructor(readonly $state: StateService,
    private projectService: ProjectsService,
    private errorDialogService: ErrorDialogService,) { }

  ngOnInit() {
    this.departmentId = this.$state.params['departmentId'];
    this.projectService.onUpdatedEvent.subscribe(this.update.bind(this));
    this.loadDepartment();
  }

  public get department() {
    return this._department;
  }

  private update(department: any) {
    if (this._department.id == department.id) {
      this._department = department;
    }
  }

  private loadDepartment() {
    this.projectService.get(this.departmentId).subscribe(
      (response: any) => {
        this._department = response;
      },
      this.errorHandle.bind(this)
    );
  }

  private errorHandle(error: any) {
    this.errorDialogService.show("Lỗi dữ liệu");
  }
}