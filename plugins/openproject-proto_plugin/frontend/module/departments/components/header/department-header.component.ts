import { Component, OnInit, Input } from '@angular/core';
import { MainMenuToggleService } from "core-components/main-menu/main-menu-toggle.service";
import { ProjectsService } from "../../../projects/services/projects.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { UserCacheService } from "core-components/user/user-cache.service";
import { ErrorDialogService, LoaderService } from "../../../common/components";
import { StateService } from '@uirouter/core';
import { generateIdentifier } from '../../../common/consts/avp';

@Component({
  templateUrl: './department-header.component.html',
  selector: 'department-header'
})
export class DepartmentHeaderComponent implements OnInit {
  @Input() department: any;

  private currentUser: any = {};

  public editting: any = {
    name: '',
    admins: [],
    members: []
  };

  constructor(public readonly toggleService: MainMenuToggleService,
    readonly userCacheService: UserCacheService,
    private userService: CurrentUserService,
    private loaderService: LoaderService,
    private projectService: ProjectsService,
    private modalService: NgbModal,
    readonly $state: StateService,
    private errorDialogService: ErrorDialogService) { }

  ngOnInit() {
    this.userCacheService
      .require(this.userService.userId)
      .then((response: any) => this.currentUser = response)
      .catch((error: any) => console.error(error));
  }

  public onSearchProject($event: any) {
    let searchString = $event.target.value.toLowerCase();
    this.projectService.onFilterByName(searchString);
  }

  public open(modal: any) {
    this.editting.name = this.department ? this.department.name : '';
    this.modalService.open(modal, { backdrop: 'static' });
  }

  public openCreateDepModal(modal: any) {
    this.modalService.open(modal, { backdrop: 'static' });
  }

  public onAdminChanged(event: any) {
    this.editting.admins = event;
  }

  public onMemberChanged(event: any) {
    this.editting.members = event;
  }

  public onSave() {
    if (!this.validateForm()) {
      return false;
    }

    let payload = this.payload;
    this.projectService.updateProject(this.department.id, payload).subscribe(
      (response: any) => {
        this.department = response;
        this.errorDialogService.showSuccess();
        this.projectService.onUpdate(this.department);
      },
      this.errorHandle.bind(this)
    )

    return true;
  }

  public onDelete(modal: any) {
    modal.dismiss();
    this.loaderService.loading(true, 'Đang xóa phòng ban, vui lòng chờ!');
    this.projectService.delete(this.department.id).subscribe(
      (response: any) => {
        this.loaderService.loading(false);
        this.errorDialogService.showSuccess("Xóa phòng ban thành công!");
        this.$state.go("departments");
      },
      (error: any) => {
        this.loaderService.loading(false);
        this.errorDialogService.show('Tác vụ không thành công');
      }
    )
  }

  public get isOwner() {
    return this.currentUser.owner;
  }

  public get isDepartmentAdmin() {
    return this.adminIds.some((id: any) => id == this.userService.userId);
  }

  public get adminIds() {
    return this.department ? this.department.members
      .filter((member: any) => {
        return member.roles.some((role: any) => {
          return role.name.toLowerCase().includes('admin');
        });
      })
      .map((member: any) => {
        return member.user_id;
      }) : [];
  }

  public get memberIds() {
    return this.department ? this.department.members
      .filter((member: any) => {
        return member.roles.some((role: any) => {
          return !role.name.toLowerCase().includes('admin');
        });
      })
      .map((member: any) => {
        return member.user_id;
      }) : [];
  }

  private errorHandle(error: any) {
    this.errorDialogService.show(error.message);
  }

  private validateForm(): boolean {
    if (this.editting.name == "" || this.editting.name == undefined) {
      this.errorHandle({ message: "Tên dự án không thể  bỏ trống!" });
      return false;
    }

    if (this.editting.admins.length == 0) {
      this.errorHandle({ message: "Quản trị dự án không thể bỏ trống!" });
      return false;
    }

    return true;
  }


  private get payload() {
    let payload: any = {};
    payload["name"] = this.editting.name;
    payload["identifier"] = generateIdentifier(this.editting.name);
    payload["member"] = {
      "tobeDeleted": [],
      "tobeAdded": []
    };
    this.editting.admins.forEach((user: any) => {
      payload["member"]["tobeAdded"].push({
        user_id: user.id,
        roles: ['admin']
      });
    });

    this.editting.members.forEach((user: any) => {
      let a = payload["member"]["tobeAdded"].filter((u: any) => {
        return u.user_id == user.id;
      });

      if(a.length > 0) {
        a[0].roles.push('member');
      }
      else {
        payload["member"]["tobeAdded"].push({
          user_id: user.id,
          roles: ['member']
        });
      }
    });

    this.department.members.forEach((member: any) => {
      let byAddedAdmins = this.editting.admins.filter((user: any) => {
        return user.id == member.user_id;
      });

      if (byAddedAdmins.length == 0) {
        payload["member"]["tobeDeleted"].push({
          user_id: member.user_id,
          roles: ['admin']
        });
      }

      let byAddedMembers = this.editting.members.filter((user: any) => {
        return user.id == member.user_id;
      });

      if(byAddedMembers.length == 0) {
        let d = payload["member"]["tobeDeleted"].filter((u: any) => {
          return u.user_id == member.user_id;
        });
  
        if(d.length > 0) {
          d[0].roles.push('member');
        }
        else {
          payload["member"]["tobeDeleted"].push({
            user_id: member.user_id,
            roles: ['member']
          });
        }
      }
    });

    return payload;
  }
}