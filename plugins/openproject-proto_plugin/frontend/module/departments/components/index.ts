export * from './header/department-header.component';
export * from './list/department-list.component';
export * from './details/department-detail-master.component';
export * from './details/projects/department-detail-projects.component';
export * from './details/side-bar/department-sidebar.component';