import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseService } from "../../common/services/base.service";

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends BaseService {
  public createCategory(payload: any) {
    let requestUrl = this.restUrl + '/avp/api/v3/categories';
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers });
  }

  public editCategory(id: string, payload: any) {
    let requestUrl = this.restUrl + `/avp/api/v3/category/${id}`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers });
  }

  public deleteCategory(id: string) {
    let requestUrl = this.restUrl + `/avp/api/v3/category/${id}`;
    return this.httpClient.delete(requestUrl);
  }
}
