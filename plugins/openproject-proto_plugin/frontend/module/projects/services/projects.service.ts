import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BaseService } from "../../common/services/base.service";
import { AVP } from '../../common/consts/avp';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService extends BaseService {

  public refreshRequestEvent: EventEmitter<any> = new EventEmitter();
  public refreshEvent: EventEmitter<any> = new EventEmitter();
  public filterByNameEvent: EventEmitter<string> = new EventEmitter();
  public viewChangedEvent: EventEmitter<string> = new EventEmitter();
  public onUpdatedEvent: EventEmitter<any> = new EventEmitter();
  public onExportEvent: EventEmitter<any> = new EventEmitter();

  public async getCategories(projectId: number) : Promise<any> {
    let requestUrl = this.restUrl + `/api/v3/projects/${projectId}/categories`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  public getAll() {
    let requestUrl = this.restUrl + '/avp/api/v3/projects';
    return this.httpClient.get(requestUrl);
  }

  public getRelatedProjectsOnly() {
    let requestUrl = this.restUrl + 'avp/api/v3/related_projects';
    return this.httpClient.get(requestUrl);
  }

  private _projects: any;
  public async getProjectsAsync(force: boolean = false) : Promise<any> {
    if(this._projects == null || force) {
      let requestUrl = this.restUrl + 'avp/api/v3/project_only';
      const response = await this.httpClient.get<any>(requestUrl).toPromise();
      this._projects = response;
    }
    return this._projects;
  }

  public getAllDepartments() {
    let requestUrl = this.restUrl + '/avp/api/v3/all_departments';
    return this.httpClient.get(requestUrl);
  }

  public getDepartments() {
    let requestUrl = this.restUrl + '/avp/api/v3/depts';
    return this.httpClient.get(requestUrl);
  }
  
  public getReportAll(by: string, from: string, to: string, countSubTask: boolean, departmentId: number | null, status: 'inprogress' | 'done' | null) {
    let requestUrl = this.restUrl + `/avp/api/v3/report_all?by=${by}&from=${from}&to=${to}&countSubTask=${countSubTask}`;
    if(departmentId) {
      requestUrl += `&departmentId=${departmentId}`;
    }
    if(status) {
      requestUrl += `&status=${status}`;
    }

    return this.httpClient.get(requestUrl);
  }

  public getProjectReport(projectId: number, status: 'inprogress' | 'done' | null, by: string, from: string, to: string, ) {
    let requestUrl = this.restUrl + `/avp/api/v3/report_project?projectId=${projectId}`;
    
    if(by) {
      requestUrl += `&by=${by}&from=${from}&to=${to}`;
    }

    if(status) {
      requestUrl += `&status=${status}`;
    }

    return this.httpClient.get(requestUrl);
  }

  public getDepartmentsWithChildren() {
    let requestUrl = this.restUrl + '/avp/api/v3/deptsWithHierarchy';
    return this.httpClient.get(requestUrl);
  }

  public getNoneDepartmentProjects() {
    let requestUrl = this.restUrl + '/avp/api/v3/noneDepProjects';
    return this.httpClient.get(requestUrl);
  }

  public getByFilters(filters: any, query: any = undefined, isAll: boolean = false) {
    let requestUrl = this.restUrl + (query ? `/avp/api/v3/projects?page=${query.pageIndex}&m=${isAll}` : '/avp/api/v3/projects');
    let httpParams = this.getQueryParams({ ['filters']: filters });
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

  public getByDepartment(departmentId: number) {
    let requestUrl = this.restUrl + '/avp/api/v3/projects?m=true';
    let httpParams = this.getQueryParams({ ['filters']: { parent_id: departmentId } });
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

  public getPosibleProjects() {
    let requestUrl = this.restUrl + '/avp/api/v3/posible_projects';
    return this.httpClient.get(requestUrl);
  }

  public get(id: number) {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${id}`;
    return this.httpClient.get(requestUrl);
  }

  public async getByOldAPI(id: number) : Promise<any> {
    let requestUrl = this.restUrl + `/api/v3/projects/${id}`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  public getStatus(id: number) {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${id}/status`;
    return this.httpClient.get(requestUrl);
  }

  public async getLastWpCreatedDate(id: number) : Promise<any> {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${id}/last_wp_created_date`;
    return this.httpClient.get<any>(requestUrl).toPromise();
  }

  public async getRoles(id: number) : Promise<any> {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${id}/roles`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  public updateProject(projectId: number, payload: any) {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${projectId}`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers });
  }

  public updateProjectStatus(projectId: number, payload: any) {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${projectId}/status`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers });
  }

  public onUpdate(project: any) {
    this.onUpdatedEvent.emit(project);
  }

  public create(payload: any) {
    let requestUrl = this.restUrl + '/avp/api/v3/projects';
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers });
  }

  public delete(id: any) {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${id}`;
    return this.httpClient.delete(requestUrl);
  }

  public onRefreshWithParams(filters: any) {
    this.refreshRequestEvent.emit(filters);
  }

  public onRefresh() {
    this.refreshEvent.emit();
  }

  public onViewChanged(targetView: string) {
    this.viewChangedEvent.emit(targetView);
  }

  public onFilterByName(text: string) {
    this.filterByNameEvent.emit(text);
  }

  public onExport(filters: any[], orders: any[]) {
    this.onExportEvent.emit({
      filters: filters,
      orders: orders
    });
  }

  public getUsersOf(workPackages: any[]) {
    let users: any[] = [];
    workPackages.forEach((wp: any) => {
      if (wp.assigned_to) {
        let userId = wp.assigned_to.id;

        if (!users.some((user: any) => user.id == userId)) {
          wp.assigned_to.avatar = this.avpUtils.getAvatarUrlByEmail(wp.assigned_to.mail);
          users.push(wp.assigned_to);
        }
      }
    });
    return users;
  }

  public getProjectsOf(workPackages: any[]) {
    let projects: any[] = [];
    workPackages.forEach((wp: any) => {
      let projectId = wp.project.id;

      if (!projects.some((project: any) => project.id == projectId)) {
        projects.push(wp.project);
      }
    });
    return projects;
  }


  public getProjectPermissions(projectId: string) {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${projectId}/permissions`;
    return this.httpClient.get(requestUrl);
  }

  public updateProjectPermission(projectId: string, fieldId: number, payload: any) {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${projectId}/permissions/${fieldId}`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers });
  }

  public getProjectEmailConfig(projectId: string) {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${projectId}/email_configs`;
    return this.httpClient.get(requestUrl);
  }

  public updateProjectEmailConfig(projectId: string, fieldId: number, payload: any) {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${projectId}/email_configs/${fieldId}`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers });
  }

  public getDepartmentsOf(info: any) {
    let departments: any[] = [];
    info.projects.forEach((project: any) => {
      let dep = project["parent"];
      if(dep && dep.id && dep.id != AVP.id) {
        let workPackages = info["workPackages"].filter((wp: any) => wp.project.id === project.id);
        let addedD  = departments.find((d: any) => d.id == dep.id);
        if(addedD) {
          addedD.workPackages = addedD.workPackages.concat(workPackages);
        } else {
          dep.workPackages = workPackages;
          departments.push(dep);
        }
      }
    });
    return departments;
  }
}