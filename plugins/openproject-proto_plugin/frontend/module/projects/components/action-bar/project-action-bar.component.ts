import { Component, OnInit } from '@angular/core'
import { ProjectsService } from "../../services/projects.service";

@Component({
  templateUrl: 'project-action-bar.component.html',
  selector: 'project-action-bar'
})
export class ProjectActionBarComponent implements OnInit {

  public activations: any[] = [
    {
      value: "1",
      label: "Active",
      isDefault: true
    },
    {
      value: "0",
      label: "Đã đóng"
    }
  ];

  public statuses: any[] = [
    {
      value: "all",
      label: "Tất cả trạng thái",
      isDefault: true
    },
    {
      value: "on-track",
      label: "Đúng tiến độ"
    },
    {
      value: "off-track",
      label: "Chậm tiến độ"
    },
    {
      value: "high-risk",
      label: "Có rủi ro cao"
    },
  ];

  public selected: any = {
    activate: this.activations[0],
    status: this.statuses[0],
    view: 'table'
  };

  private currentFilters: any = {};

  constructor(private projectService: ProjectsService) { }

  ngOnInit() {

  }

  public onChangeActive(activate: any) {
    this.selected.activate = activate;
    this.currentFilters["status"] = this.selected.activate.value;

    this.onRefreshProjects();
  }

  public onChangeStatus(status: any) {
    this.selected.status = status;
    this.currentFilters["Status"] = "";
    if (status.value != "all") {
      this.currentFilters["Status"] = this.selected.status.value;
    }

    this.onRefreshProjects();
  }

  public onChangeView(view: string) {
    this.selected.view = view;
    this.projectService.onViewChanged(this.selected.view);
  }

  public onSearchProject($event: any) {
    let searchString = $event.target.value.toLowerCase();
    this.projectService.onFilterByName(searchString);
  }

  private onRefreshProjects() {
    this.projectService.onRefreshWithParams(this.currentFilters);
  }
}