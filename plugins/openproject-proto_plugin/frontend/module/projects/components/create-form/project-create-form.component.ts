import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { UserCacheService } from "core-components/user/user-cache.service";
import { ProjectsService } from "../../services/projects.service";
import { ErrorDialogService } from "../../../common/components";
import { AVP, generateIdentifier, NotificationType } from '../../../common/consts/avp';
import { AvpNotificationService } from '../../../avp-base/notifications/avp-notifications.service';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './project-create-form.component.html',
  selector: 'project-create-form'
})
export class ProjectCreateFormComponent implements OnInit {
  @Input() type: string; // project | team
  @Output() onDismiss: EventEmitter<any> = new EventEmitter();

  public type_title: string;
  public statuses: any[] = [
    {
      value: 'on-track',
      label: 'Đúng tiến độ'
    },
    {
      value: 'off-track',
      label: 'Chậm tiến độ'
    },
    {
      value: 'high-risk',
      label: 'Rủi ro cao'
    },
  ];
  public showAdvanced: boolean = false;
  public usingAddGroup: boolean = false;

  public departments: any[] = [];

  public createForm: any = {
    admins: [],
    members: [],
    parent_id: undefined,
    startDate: undefined,
    dueDate: undefined,
    status: 'on-track',
    description: '',
    name: '',
    identifier: '',
    level: '2',
    type: 'internal'
  };

  public defaultAdmin: any[] = [];
  public onSaving: boolean = false;

  private currentUser: any;

  constructor(readonly userCacheService: UserCacheService,
    private projectService: ProjectsService,
    private errorDialogService: ErrorDialogService,
    private currentUserService: CurrentUserService,
    private avpNotificationService: AvpNotificationService,
    private logAdapter: LogAdapter) { }

  ngOnInit() {
    this.type_title = this.type == 'project' ? 'dự án' : 'phòng ban';
    this.createForm.level = this.type == 'project' ? '2' : '1';
    this.loadDepartments();
    this.defaultAdmin.push(this.currentUserService.userId);
    this.userCacheService
      .require(this.currentUserService.userId)
      .then((user: any) => {
        this.currentUser = user;
      });
  }

  public onMemberChanged(event: any) {
    this.createForm.members = event;
  }

  public onAdminChanged(event: any) {
    this.createForm.admins = event;
  }

  public save() {
    this.onSaving = true;
    let errorMessage = this.validate();
    if (errorMessage != '') {
      this.errorHandle({ message: errorMessage });
      this.onSaving = false;
      return;
    }

    let payload = {
      identifier: this.type == 'project' ? generateIdentifier(this.createForm.name) : 'avpdept_' + generateIdentifier(this.createForm.name),
      name: this.createForm.name,
      description: this.createForm.description,
      parent_id: this.createForm.parent_id ? this.createForm.parent_id : AVP.id,
      members: this.refineMembers(),
      status: this.createForm.status,
      dueDate: this.getDateString(this.createForm.dueDate),
      startDate: this.getDateString(this.createForm.startDate),
      isInternal: this.createForm.type == 'internal',
      level: this.createForm.level
    };

    this.projectService.create(payload).subscribe(
      (response: any) => {
        this.sendNotify(response);
        this.projectService.onRefresh();
        this.dismiss();
        this.logAdapter.logging(ActivityActionType.CreateProject, response.id, response.name);
        this.onSaving = false;
      },
      this.errorHandle.bind(this)
    );
  }

  public dismiss() {
    this.onDismiss.emit();
  }

  public get isProject() {
    return this.type == 'project';
  }

  private refineMembers() {
    let members: any[] = [];
    this.createForm.admins.forEach((user: any) => {
      members.push({
        id: user.id,
        roles: ['Project admin']
      })
    });

    this.createForm.members.forEach((user: any) => {
      let member = members.filter((m: any) => {
        return m.id == user.id;
      });

      if (member.length > 0) {
        member[0].roles.push('Member');
      }
      else {
        members.push({
          id: user.id,
          roles: ['Member']
        });
      }
    });

    return members;
  }

  private validate() {
    if (this.createForm.name == '') {
      return "Vui lòng nhập tên dự án/phòng ban";
    }

    if (!this.validateStartDateEndDate()) {
      return "Ngày bắt đầu và ngày kết thúc không hợp lệ!";
    }

    return '';
  }

  private validateStartDateEndDate() {
    if (this.createForm.dueDate != null) {
      let today = new Date();
      let dueDate = new Date(this.createForm.dueDate.year, this.createForm.dueDate.month - 1, this.createForm.dueDate.day);
      if (dueDate.getTime() < today.getTime()) {
        return false;
      }
    }
    if (this.createForm.startDate != null && this.createForm.dueDate != null) {
      if (this.createForm.startDate.year > this.createForm.dueDate.year) return false;
      else if (this.createForm.startDate.year == this.createForm.dueDate.year) {
        if (this.createForm.startDate.month > this.createForm.dueDate.month) return false;
        else if (this.createForm.startDate.month == this.createForm.dueDate.month) {
          if (this.createForm.startDate.day > this.createForm.dueDate.day) return false;
        }
      }
    }

    return true;
  }

  private loadDepartments() {
    this.projectService.getDepartments().subscribe(
      (responses: any[]) => {
        this.departments = responses;
      },
      this.errorHandle.bind(this)
    )
  }

  private errorHandle(error: any) {
    this.onSaving = false;
    this.errorDialogService.show(error.message);
  }

  private getDateString(date: any) {
    if (date == undefined) return "";

    return new Date(date.year, date.month - 1, date.day).toISOString().split('T')[0];
  }

  private sendNotify(response: any) {
    let notifyData: any = {
      id: response.id,
      receivers: this.recievers(response),
      author: this.currentUser,
      project: { id: response.id, name: response.name }
    }
    this.avpNotificationService.notify(NotificationType.ADD_USER_TO_PROJECT, notifyData);
  }

  private recievers(project: any) {
    let receiverEmails = project.members ? project.members.map((member: any) => member.user.mail) : [];

    let authorEmail = this.currentUser.email;

    return receiverEmails.filter((email: string) => {
      return email != authorEmail;
    });
  }
}