import { Component, OnInit } from '@angular/core';
import { MainMenuToggleService } from "core-components/main-menu/main-menu-toggle.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { UserCacheService } from "core-components/user/user-cache.service";

@Component({
  templateUrl: './projects-header.component.html',
  selector: 'projects-header'
})
export class ProjectsHeaderComponent implements OnInit {

  private currentUser: any = {};
  public isAdmin: boolean = false;
  public isOwner: boolean = false;

  constructor(public readonly toggleService: MainMenuToggleService,
    readonly userCacheService: UserCacheService,
    private userService: CurrentUserService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.userCacheService
      .require(this.userService.userId)
      .then((response: any) => {
        this.currentUser = response
        this.isAdmin = this.currentUser.admin || this.currentUser.owner;
        this.isOwner = this.currentUser.owner;
      })
      .catch((error: any) => console.error(error));
  }

  public open(modal: any) {
    this.modalService.open(modal, { backdrop: 'static' });
  }
}