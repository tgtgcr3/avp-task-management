import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ProjectsService } from "../../services/projects.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { ErrorDialogService, LoaderService } from "../../../common/components";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as AVPConst from '../../../common/consts/avp';
import { Subscription } from 'rxjs';
import { StateService, TransitionService, Transition } from '@uirouter/core';
import { AVP_VIEWS } from '../../../common/consts/view-name';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './project-list.component.html',
  selector: 'project-list'
})
export class ProjectListComponent implements OnInit, OnDestroy {

  @Input() showActionBar: boolean = true;
  @Input() departmentId: number;

  public projects: any[] = [];
  public departments: any[] = [];
  public statuses: any[] = AVPConst.PROJECT_STATUSES;
  public activations: any[] = AVPConst.PROJECT_ACTIVATIONS;
  public editingProject: any;
  public editingProjectParentId: any;
  public currentView: string = 'table';
  public searchText: string = '';

  private onFiltersSubscription: Subscription;
  private onSearchSubscription: Subscription;
  private onRefreshSubscription: Subscription;
  private onViewChangedSubscription: Subscription;

  private currentFilters: any = {};
  public query: any = {
    pageSize: 15,
    pageIndex: 1
  };

  constructor(private projectService: ProjectsService,
    private loaderService: LoaderService,
    readonly $state: StateService,
    private logAdapter: LogAdapter,
    private errorDialogService: ErrorDialogService,
    private currentUserService: CurrentUserService,
    private modalService: NgbModal,
    readonly trans: TransitionService) { }

  ngOnInit() {
    this.onSearchSubscription = this.projectService.filterByNameEvent.subscribe(this.searchByName.bind(this));
    this.onFiltersSubscription = this.projectService.refreshRequestEvent.subscribe(this.refreshProjectWithParams.bind(this));
    this.onViewChangedSubscription = this.projectService.viewChangedEvent.subscribe(this.viewChanged.bind(this));
    this.onRefreshSubscription = this.projectService.refreshEvent.subscribe(this.refresh.bind(this));
    this.currentFilters["status"] = 1;

    if (this.$state.params['page']) {
      this.query.pageIndex = this.$state.params['page'];
    }

    this.trans.onSuccess({}, (transition: Transition) => {
      let param = transition.params('to');
      if (param.page && param.page != this.query.pageIndex) {
        if (this.$state.params['page']) {
          this.query.pageIndex = this.$state.params['page'];
        }
        this.refresh();
      }
    });

    this.refresh();
  }

  ngOnDestroy() {
    if (this.onSearchSubscription) {
      this.onSearchSubscription.unsubscribe();
    }

    if (this.onFiltersSubscription) {
      this.onFiltersSubscription.unsubscribe();
    }

    this.onViewChangedSubscription && this.onViewChangedSubscription.unsubscribe();
    this.onRefreshSubscription && this.onRefreshSubscription.unsubscribe();
  }

  public openModal(modal: any, targetProject: any) {
    this.editingProject = JSON.parse(JSON.stringify(targetProject));

    if (this.editingProject.DueDate == undefined) {
      this.editingProject.DueDate = {
        id: 0,
        value: undefined
      }
    }

    if (this.editingProject.StartDate == undefined) {
      this.editingProject.StartDate = {
        id: 0,
        value: undefined
      };
    }

    this.editingProjectParentId = targetProject.parent != undefined ? targetProject.parent.id : undefined;

    this.modalService.open(modal, { backdrop: 'static' });
  }

  public onChangeAdminTo(event: any) {
    this.editingProject.admins = event;
  }

  public onChangeStartDate(event: any) {
    let dateString = "";
    if (event != undefined) {
      dateString = `${event.year}-${event.month}-${event.day}`
    }

    this.editingProject.StartDate.value = dateString;
  }

  public onChangeDueDate(event: any) {
    let dateString = "";
    if (event != undefined) {
      dateString = `${event.year}-${event.month}-${event.day}`
    }

    this.editingProject.DueDate.value = dateString;
  }

  public onSave(modal: any) {

    if (!this.validateForm()) {
      return;
    }

    let payload = this.payload;
    this.projectService.updateProject(this.editingProject.id, payload).subscribe(
      (response: any) => {
        this.errorDialogService.showSuccess();
        this.logAdapter.logging(ActivityActionType.UpdateProject, response.id, response.name);
        this.loadProjects();
      },
      (error: any) => {
        this.errorHandle(error);
      }
    )

    modal.dismiss();
  }

  public onSaveStatus(modal: any) {
    let payload = this.statusPayload;
    this.projectService.updateProjectStatus(this.editingProject.id, payload).subscribe(
      (response: any) => {
        this.errorDialogService.showSuccess();
        this.logAdapter.logging(ActivityActionType.UpdateProject, response.id, response.name);
        this.loadProjects();
      },
      (error: any) => {
        this.errorHandle(error);
      }
    )
    modal.dismiss();
  }

  public goto(url: string) {
    window.open(url, '_blank');
  }

  public hasDepartment(project: any) {
    return project.parent != undefined && project.parent.id != AVPConst.AVP.id;
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVPConst.AVP.user_default_img_url;
  }

  public isAdmin(project: any) {
    return project.members.some((member: any) => {
      return member["roles"].some((role: any) => {
        return role.name.toLowerCase() == "project admin";
      }) && member.user_id == this.currentUserId;
    });
  }

  public get currentState() {
    return this.$state.current.name;
  }

  public get showingProjects() {
    return this.projects.filter((project: any) => {
      return project.name.toLowerCase().includes(this.searchText.toLowerCase());
    });
  }

  public get currentUserId() {
    return this.currentUserService.userId;
  }

  public get admins() {
    return this.editingProject.members.filter((member: any) => {
      return member["roles"].some((role: any) => {
        return role.name.toLowerCase() == "project admin";
      });
    });
  }

  public get adminIds() {
    return this.admins.map((member: any) => member.user.id);
  }

  private refresh() {
    this.loadProjects();
    this.loadDepartments();
  }

  private refreshProjectWithParams(params: any) {
    this.currentFilters = params;
    this.loadProjects();
  }

  private loadProjects() {
    this.loaderService.loading(true, "Loading");
    let subscription: any;
    if (this.departmentId != undefined) {
      subscription = this.projectService.getByDepartment(this.departmentId);
    }
    else {
      subscription = this.projectService.getByFilters(this.currentFilters, this.query, this.$state.current.name == AVP_VIEWS.PROJECTS_ALL);
    }
    subscription.subscribe(
      this.refineData.bind(this),
      this.errorHandle.bind(this)
    );
  }

  private loadDepartments() {
    this.projectService.getDepartments()
      .subscribe(
        (response: any) => {
          this.departments = response;
        }
      )
  }

  private refineData(response: any) {
    this.projects = response;
    this.projects.forEach((project: any) => {
      this.initCustomFields(project);
    });
    this.loaderService.loading(false);
  }

  private searchByName(text: string) {
    this.searchText = text;
  }

  private viewChanged(view: string) {
    this.currentView = view;
  }

  private initCustomFields(project: any) {
    project["infos"] = this.getInfos(project.work_packages);
    project["admins"] = project.members.filter((member: any) => {
      return member["roles"].some((role: any) => {
        return role.name.toLowerCase() == "project admin";
      });
    }).map((member: any) => member.user);
    
    project.custom_values.forEach((customValue: any) => {
      project[customValue.custom_field.name] = {
        id: customValue.id,
        value: customValue.value
      };
    });
  }

  private getInfos(workPackages: any[]) {
    let total = workPackages.length;
    let done = 0;
    let overdue = 0;
    workPackages.forEach((wp: any) => {
      if (wp.status_id == 16 || wp.status_id == 17) {
        done++;
      }
      else if (wp.due_date != null) {
        let dueDate = new Date(wp.due_date);
        if (dueDate.getTime() < new Date().getTime()) {
          overdue++;
        }
      }
    });
    return [total, done, overdue];
  }

  private errorHandle(error: any) {
    this.errorDialogService.show(error.message);
  }

  private validateForm(): boolean {
    if (this.editingProject.name == "" || this.editingProject.name == undefined) {
      this.errorHandle({ message: "Tên dự án không thể  bỏ trống!" });
      return false;
    }

    if (this.editingProject.admins == null || this.editingProject.admins.length == 0) {
      this.errorHandle({ message: "Quản trị dự án không thể bỏ trống!" });
      return false;
    }

    if (this.editingProjectParentId == this.editingProject.id) {
      this.errorHandle({ message: "Phòng ban không hợp lệ" });
      return false;
    }

    return true;
  }

  private get payload() {
    let payload: any = {};
    payload["id"] = this.editingProject.id;
    payload["name"] = this.editingProject.name;
    payload["identifier"] = AVPConst.generateIdentifier(this.editingProject.name);
    payload["description"] = this.editingProject.description;
    payload["parentId"] = this.editingProjectParentId;
    payload["dueDate"] = {
      custom_value_id: this.editingProject.DueDate.id,
      custom_value_value: this.editingProject.DueDate.value
    };
    payload["startDate"] = {
      custom_value_id: this.editingProject.StartDate.id,
      custom_value_value: this.editingProject.StartDate.value
    };
    payload["member"] = {
      "tobeDeleted": [],
      "tobeAdded": []
    };

    this.editingProject.admins.forEach((user: any) => {
      payload["member"]["tobeAdded"].push({
        user_id: user.id,
        roles: ['admin']
      });
    });

    this.admins.forEach((member: any) => {
      let byAddedList = this.editingProject.admins.filter((admin: any) => {
        return admin.id == member.user_id;
      });

      if (byAddedList.length == 0) {
        payload["member"]["tobeDeleted"].push({
          user_id: member.user_id,
          roles: ['admin']
        });
      }
    });

    return payload;
  }

  private get statusPayload() {
    let payload: any = {};
    payload["is_active"] = this.editingProject.status;
    payload["status"] = {
      custom_value_id: this.editingProject.Status.id,
      custom_value_value: this.editingProject.Status.value
    };

    return payload;
  }
}
