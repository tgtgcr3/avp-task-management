import { Component, OnInit } from '@angular/core';
import { ErrorDialogService } from '../../../../common/components';
import { ProjectsService } from '../../../services/projects.service';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { StateService } from "@uirouter/core";
import { AVP_VIEWS } from '../../../../common/consts/view-name';

@Component({
  templateUrl: './project-setting-layout.component.html',
  selector: '<project-setting>'
})
export class ProjectSettingLayoutComponent implements OnInit {

  constructor(readonly $state: StateService,
    private projectService: ProjectsService,
    private currentUserService: CurrentUserService,
    private errorDialogService: ErrorDialogService
    ) { }

  async ngOnInit() {
    await this.authenticate();
  }

  async authenticate() {
    if(this.$state["params"].projectId) {
      let roles = await this.projectService.getRoles(this.$state["params"].projectId);
      if(this.currentUserService.role == 'owner' || roles.includes('Project admin')) {
        return true;
      }
    }
    this.errorDialogService.show('Bạn không có quyền truy cập trang này');
    if(this.$state["params"].projectId) {
      this.$state.go(AVP_VIEWS.PROJECT, {projectId: this.$state["params"].projectId});
    }
    else {
      this.$state.go(AVP_VIEWS.PROJECTS);
    }
    return false;
  }
}