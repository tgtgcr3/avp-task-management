import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../../../../services/projects.service';
import { StateService } from "@uirouter/core";
import * as AvpConst from '../../../../../common/consts/avp';
import { ErrorDialogService, LoaderService } from '../../../../../common/components';

@Component({
  templateUrl: './project-setting-email.component.html',
  selector: 'project-setting-email'
})
export class ProjectSettingEmailComponent implements OnInit {

  private _emailConfig: any = {
    id: 0,
    value: ""
  };

  constructor(readonly $state: StateService,
    private errorHandleService: ErrorDialogService,
    private loaderService: LoaderService,
    private projectService: ProjectsService) {
  }

  ngOnInit() {
    this.loadConfig();
  }

  public onChange(type: string, feature: string, value: boolean) {
    let emailConfig = this.emailConfig;
    emailConfig[type][feature] = value;
    this.save({
      value: JSON.stringify(emailConfig)
    });
  }

  private loadConfig() {
    this.projectService.getProjectEmailConfig(this.projectId).subscribe(
      (response: any) => {
        if(response) {
          this._emailConfig = response;
          this._emailConfig.value = JSON.parse(this._emailConfig.value);
        }
      },
      this.errorHandler.bind(this)
    )
  }
  
  private save(payload: any) {
    this.loaderService.loading(true, 'Đang lưu');
    this.projectService.updateProjectEmailConfig(this.projectId, this._emailConfig.id, payload)
    .subscribe(
      (response: any) => {
        this.loaderService.loading(false);
        this._emailConfig = response;
        this._emailConfig.value = JSON.parse(this._emailConfig.value);
      },
      this.errorHandler.bind(this)
    );
  }

  private errorHandler(error: any) {
    console.error(error.message);
    this.loaderService.loading(false);
    this.errorHandleService.show(error.message);
  }

  public get emailConfig() {
    return this._emailConfig.value ? this._emailConfig.value : AvpConst.DEFAULT_EMAIL_NOTIFY;
  }

  private get projectId() {
    return this.$state["params"].projectId;
  }
}