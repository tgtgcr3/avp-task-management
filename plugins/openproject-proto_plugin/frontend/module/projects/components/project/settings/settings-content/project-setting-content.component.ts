import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { ErrorDialogService, LoaderService } from '../../../../../common/components';
import { ProjectsService } from '../../../../services/projects.service';
import { StateService } from "@uirouter/core";
import * as AVPConst from '../../../../../common/consts/avp';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './project-setting-content.component.html',
  selector: '<project-setting-content>'
})
export class ProjectSettingContentComponent implements OnInit {

  public activations: any[] = AVPConst.PROJECT_ACTIVATIONS;

  public statuses: any[] = AVPConst.PROJECT_STATUSES;

  public reviewOptions: any[] = AVPConst.PROJECT_REVIEWING_OPTIONS;

  public reviewerOptions: any[] = AVPConst.PROJECT_REVIEWER_OPTIONS;

  public project: any = {
    Status: {},
    StartDate: {},
    DueDate: {},
    parent: {},
    review_required: {},
    reviewer_as_assigner: {},
    reviewer: {}
  };
  public departments: any[] = [];

  constructor(private projectService: ProjectsService,
    readonly $state: StateService,
    private loaderService: LoaderService,
    private currentUserService: CurrentUserService,
    private logAdapter: LogAdapter,
    private errorDialogService: ErrorDialogService) {}

  ngOnInit() {
    this.loadProject();
    this.loadDepartments();
  }

  public onChangeStartDate(event: any) {
    let dateString = "";
    if (event != undefined) {
      dateString = `${event.year}-${event.month}-${event.day}`
    }
    this.project.StartDate.value = dateString;
  }

  public onChangeDueDate(event: any) {
    let dateString = "";
    if (event != undefined) {
      dateString = `${event.year}-${event.month}-${event.day}`
    }
    this.project.DueDate.value = dateString;
  }

  public onReviewerChanged(user: any) {
    this.project.reviewer.value = user.id;
  }

  public onSave() {
    if (!this.validateForm()) {
      return;
    }

    let payload = this.payload;
    this.projectService.updateProject(this.project.id, payload).subscribe(
      (response: any) => {
        this.analyzeResponse(response);
        this.errorDialogService.showSuccess();
        this.logAdapter.logging(ActivityActionType.UpdateProject, this.projectId, response.name);
      },
      this.errorHandler.bind(this)
    )
  }

  private validateForm(): boolean {
    if (this.project.name == "" || this.project.name == undefined) {
      this.errorHandler({ message: "Tên dự án không thể  bỏ trống!" });
      return false;
    }

    if(this.project.reviewer_as_assigner.value == 'f') {
      if(this.project.reviewer.value == undefined) {
        this.errorHandler({ message: "Người review không thể bỏ trống!" });
        return false;
      }
    }

    return true;
  }

  private loadDepartments() {
    this.projectService.getAllDepartments()
      .subscribe(
        (response: any) => {
          this.departments = response;
        }
      )
  }

  private loadProject() {
    this.loaderService.loading(true, "Loading");
    this.projectService.get(this.projectId).subscribe(
      (response: any) => {
        this.analyzeResponse(response);
        this.loaderService.loading(false);
      },
      this.errorHandler.bind(this)
    )
  }

  private analyzeResponse(response: any){ 
    this.project = response;
    this.project.parent.id == AVPConst.AVP.id ? this.project.parent.id = null : undefined;
    this.project.custom_values.forEach((customValue: any) => {
      this.project[customValue.custom_field.name] = {
        id: customValue.id,
        value: customValue.value
      };
    });

    this.ensureCustomFieldInitailized(this.project);
  }

  private ensureCustomFieldInitailized(project: any) {
    project['review_required'] = project['review_required'] ? project['review_required'] : {value: 'f'};
    project['reviewer_as_assigner'] = project['reviewer_as_assigner'] ? project['reviewer_as_assigner'] : {value: 't'};
    project['reviewer'] = project['reviewer'] ? project['reviewer'] : {value: undefined};
  }

  private errorHandler(error: any) {
    this.loaderService.loading(false);
    this.errorDialogService.show(error.message);
  }

  public get projectId() {
    return this.$state["params"].projectId;
  }

  public get isReviewRequired() {
    return this.project.review_required.value == 't';
  }

  public get isReviewerAsAssigner() {
    return this.project.reviewer_as_assigner.value == 't';
  }

  private get payload() {
    let payload: any = {};
    payload["id"] = this.project.id;
    payload["name"] = this.project.name;
    payload["identifier"] = AVPConst.generateIdentifier(this.project.name);
    payload["description"] = this.project.description;
    payload["parentId"] = this.project.parent.id == null ? AVPConst.AVP.id : this.project.parent.id;
    payload["dueDate"] = {
      custom_value_id: this.project.DueDate.id,
      custom_value_value: this.project.DueDate.value
    };
    payload["startDate"] = {
      custom_value_id: this.project.StartDate.id,
      custom_value_value: this.project.StartDate.value
    };
    payload["is_active"] = this.project.status;
    payload["status"] = {
      custom_value_id: this.project.Status.id,
      custom_value_value: this.project.Status.value
    };
    payload["review_required"] = {
      custom_value_id: this.project.review_required.id,
      custom_value_value: this.project.review_required.value
    };
    payload["reviewer_as_assigner"] = {
      custom_value_id: this.project.reviewer_as_assigner.id,
      custom_value_value: this.project.review_required.value == 'f' ? 't' : this.project.reviewer_as_assigner.value
    };
    payload["reviewer"] = {
      custom_value_id: this.project.reviewer.id,
      custom_value_value: (this.project.review_required.value == 'f' || this.project.reviewer_as_assigner.value == 't') ? undefined : this.project.reviewer.value
    };

    return payload;
  }
}