import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { ErrorDialogService, LoaderService } from '../../../../../common/components';
import { MembersService } from '../../../../../members/services/members.service';
import { ProjectsService } from '../../../../services/projects.service';
import { StateService } from "@uirouter/core";
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './project-setting-member.component.html',
  selector: 'project-setting-member'
})
export class ProjectSettingMemberComponent implements OnInit {

  public project: any = {};
  private tobeAddedMembers: any[] = [];
  private tobeAddedAdmins: any[] = [];

  constructor(private projectService: ProjectsService,
    readonly $state: StateService,
    private loaderService: LoaderService,
    private membershipService: MembersService,
    private errorDialogService: ErrorDialogService,
    private currentUserService: CurrentUserService,
    private logAdapter: LogAdapter,
    private modalService: NgbModal) {}

  ngOnInit() {
    this.loadProject();
  }

  public get members() {
    return this.project && this.project.members ? this.project.members : [];
  }

  public get adminIds() {
    return this.admins.map((member: any) => member.user.id);
  }

  public get memberIds() {
    return this.notAdmins.map((member: any) => member.user.id);
  }

  public get notAdmins() {
    return this.members.filter((member: any) => {
      return member["roles"].every((role: any) => {
        return role.name.toLowerCase() == "member";
      });
    });
  }

  public get admins() {
    return this.members.filter((member: any) => {
      return member["roles"].some((role: any) => {
        return role.name.toLowerCase() == "project admin";
      });
    });
  }

  public get isAdmin() {
    return this.admins.some((member: any) => member.user.id == this.currentUserService.userId);
  }

  public get canSaveAdmin() {
    return this.tobeAddedAdmins.length != 0;
  }

  public get canSaveMember() {
    return this.tobeAddedMembers.length != 0;
  }

  public onAdminChanged(selectedMembers: any[]) {
    this.tobeAddedAdmins = selectedMembers;
  }

  public onMemberChanged(selectedMembers: any[]) {
    this.tobeAddedMembers = selectedMembers;
  }

  public onAddAdmins(modal: any) {
    this.onSaveMemberships(this.tobeAddedAdmins, true, modal);
  }

  public onAddMembers(modal: any) {
    this.onSaveMemberships(this.tobeAddedMembers, false, modal);
  }

  public openModal(modal: any) {
    this.modalService.open(modal, { backdrop: 'static' });
  }

  private onSaveMemberships(users: any[], isAdmin = false, modal: any) {
    let payload: any = {
      project_id: this.projectId,
      member: {
        "tobeDeleted": [],
        "tobeAdded": []
      }
    };

    users.forEach((user: any) => {
      payload["member"]["tobeAdded"].push({
        user_id: user.id,
        roles: isAdmin ? ['admin'] : ['member']
      });
    });

    let members = isAdmin ? this.admins : this.notAdmins;

    members.forEach((member: any) => {
      let byAddedList = users.filter((user: any) => {
        return user.id == member.user_id;
      });

      if (byAddedList.length == 0) {
        payload["member"]["tobeDeleted"].push({
          user_id: member.user_id,
          roles: isAdmin ? ['admin'] : ['member']
        });
      }
    });

    this.membershipService.updateAll(payload).subscribe(
      (response: any) => {
        modal.dismiss();
        this.errorDialogService.showSuccess();
        this.logAdapter.logging(ActivityActionType.UpdateProject, this.projectId, response.name);
        this.loadProject();
      }
    )
  }

  private loadProject() {
    this.loaderService.loading(true, "Loading");
    this.projectService.get(this.projectId).subscribe(
      (response: any) => {
        this.project = response;
        this.loaderService.loading(false);
      },
      this.errorHandler.bind(this)
    )
  }

  private errorHandler(error: any) {
    this.loaderService.loading(false);
    this.errorDialogService.show(error.message);
  }

  private get projectId() {
    return this.$state["params"].projectId;
  }
}