import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { environment } from "environments/environment";
import { StateService } from "@uirouter/core";
import { AVP } from '../../../../../../common/consts/avp';
import { AvpUtils } from '../../../../../../common/services/avp-utils.service';
import { MembersService } from '../../../../../../members/services/members.service';
import { ErrorDialogService, LoaderService } from '../../../../../../common/components';

@Component({
  templateUrl: './project-setting-member-item.component.html',
  selector: 'member-setting-item'
})
export class ProjectSettingMemberItemComponent implements OnInit {
  @Input() member: any;
  @Input() adminMember: boolean = false;
  @Input() isAdmin: boolean = false;

  @Output() onRefresh: EventEmitter<any> = new EventEmitter();

  constructor(readonly $state: StateService,
              private membersService: MembersService,
              private loaderService: LoaderService,
              private errorHandleService: ErrorDialogService,
              private avpUtils: AvpUtils) { }

  ngOnInit() {
    this.loadUserInfo();
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public navToUser() {
    if (this.member.user.username != "")
      window.open(`${environment.avpAccountUrl}/userDetails/${this.member.user.username}`, '_blank');
  }

  public onRemoveAdmin() {
    let payload: any = {
      project_id: this.projectId,
      member: {
        "tobeDeleted": [],
        "tobeAdded": []
      }
    };

    payload["member"]["tobeDeleted"].push({
      user_id: this.member.user_id,
      roles: ['admin']
    });

    this.membersService.updateAll(payload).subscribe(
      (response: any) => {
        this.onRefresh.emit();
      }
    )
  }

  public onSetAdmin() {
    let payload: any = {
      project_id: this.projectId,
      member: {
        "tobeDeleted": [],
        "tobeAdded": []
      }
    };

    payload["member"]["tobeAdded"].push({
      user_id: this.member.user_id,
      roles: ['admin']
    });

    this.membersService.updateAll(payload).subscribe(
      (response: any) => {
        this.onRefresh.emit();
      }
    )
  }

  public onDeleteMember() {
    return this.membersService.delete(this.member.id).subscribe(
      (response: any) => {
        this.onRefresh.emit();
      },
      this.errorHandler.bind(this)
    );
  }

  private errorHandler(error: any) {
    console.error(error.message);
    this.loaderService.loading(false);
    this.errorHandleService.show(error.message);
  }

  private loadUserInfo() {
    this.membersService.getUserInfo(this.member.user.mail).subscribe(
      (response: any) => {
        this.member.user.title = response.title;
        this.member.user.username = response.username;
      },
      (error: any) => {
        this.member.user.title = "";
        this.member.user.username = "";
      }
    )
  }

  private get projectId() {
    return this.$state["params"].projectId;
  }
}