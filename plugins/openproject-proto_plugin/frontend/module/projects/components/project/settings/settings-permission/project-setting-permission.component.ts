import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../../../../services/projects.service';
import { StateService } from "@uirouter/core";
import * as AvpConst from '../../../../../common/consts/avp';
import { ErrorDialogService, LoaderService } from '../../../../../common/components';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';
import { CurrentUserService } from 'core-components/user/current-user.service';

@Component({
  templateUrl: './project-setting-permission.component.html',
  selector: 'project-setting-permission'
})
export class ProjectSettingPermissionComponent implements OnInit {
  
  private permissionHolder: any[] = [];

  private projectPermissionFieldId: number = 0;
  private wpPermissionFieldId: number = 0;
  private assigneePermissionFieldId: number = 0;

  constructor(readonly $state: StateService,
              private currentUserService: CurrentUserService,
              private logAdapter: LogAdapter,
              private errorHandleService: ErrorDialogService,
              private loaderService: LoaderService,
              private projectService: ProjectsService) { }

  ngOnInit() {
    this.loadPermission();
  }

  public get projectPermission() {
    let filterResult = this.permissionHolder.filter((permission: any) => {
      return permission.custom_field.name == 'project_permissions';
    });

    if(filterResult.length > 0) {
      this.projectPermissionFieldId = filterResult[0].id;
      if(filterResult[0].value) {
        return JSON.parse(filterResult[0].value);
      } 
    }

    return AvpConst.DEFAULT_PROJECT_PERMISSIONS;
  }

  public get wpPermission() {
    let filterResult = this.permissionHolder.filter((permission: any) => {
      return permission.custom_field.name == 'workpackage_permissions';
    });

    if(filterResult.length > 0) {
      this.wpPermissionFieldId = filterResult[0].id;
      if(filterResult[0].value) {
        return JSON.parse(filterResult[0].value);
      } 
    }

    return AvpConst.DEFAULT_WP_PERMISSIONS;
  }

  public onChangePP(fnc: string, role: string, value: boolean) {
    let permission = this.projectPermission;
    permission[fnc][role] = value;
    this.save(this.projectPermissionFieldId, {
      permission_name: 'project_permissions',
      value: JSON.stringify(permission)
    });
  }

  public onChangeWPP(fnc: string, role: string, value: boolean) {
    let permission = this.wpPermission;
    permission[fnc][role] = value;
    this.save(this.wpPermissionFieldId, {
      permission_name: 'workpackage_permissions',
      value: JSON.stringify(permission)
    });
  }

  private loadPermission() {
    this.projectService.getProjectPermissions(this.projectId).subscribe(
      (responses: any[]) => {
        this.permissionHolder = responses;
      },
      this.errorHandler.bind(this)
    )
  }

  private save(permissionFieldId: number, payload: any) {
    this.loaderService.loading(true, 'Đang lưu');
    this.projectService.updateProjectPermission(this.projectId, permissionFieldId, payload)
    .subscribe(
      (responses: any[]) => {
        this.loaderService.loading(false);
        this.permissionHolder = responses;
      },
      this.errorHandler.bind(this)
    );
  }

  private errorHandler(error: any) {
    console.error(error.message);
    this.loaderService.loading(false);
    this.errorHandleService.show(error.message);
  }

  private get projectId() {
    return this.$state["params"].projectId;
  }
}