import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { StateService } from '@uirouter/core';
import { WorkPackageResource } from "core-app/modules/hal/resources/work-package-resource";
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { CategoryService } from '../../../../services/category.service';
import { ErrorDialogService } from '../../../../../common/components';
import { TasksService } from '../../../../../tasks/services/tasks.service';

@Component({
  templateUrl: './category.component.html',
  selector: 'category',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CategoryComponent implements OnInit, OnDestroy {

  @Input() category: any;
  @Input() permission: any;
  @Input() editable: boolean = false;
  @Input() filters: any[] = [];
  @Input() order: any[] = [];
  @Output() deleted: EventEmitter<any> = new EventEmitter<any>();

  private displayType: string = 'sub-tasks';
  private searchFilter: any[] = [];
  private onFiltersSubscription: Subscription;
  private onSearchSubscription: Subscription;
  private onViewChangedSubscription: Subscription;

  loading: boolean = true;
  isOpen: boolean = false;
  canCreateWP: boolean = false;
  canCreateCategory: boolean = false;
  pageInfo: any = {
    total: 0,
    page: 1,
    maxSize: 5,
    size: 15
  }

  originWorkPackages: any[] = [];
  filteredWorkPackages: any[] = [];
  copiedCategory: any;
  
  private get isShowSubtask() {
    return this.displayType == 'sub-tasks';
  }

  private get noSubTasks() {
    return this.displayType == 'no-sub-tasks';
  }

  public get projectId() {
    return this.$state["params"].projectId;
  }

  constructor(readonly $state: StateService,
    private halResourceService: HalResourceService,
    private wpCacheService: WorkPackageCacheService,
    private modalService: NgbModal,
    private categoryService: CategoryService,
    private errorService: ErrorDialogService,
    private taskService: TasksService,
    private ref: ChangeDetectorRef) { }

  ngOnInit() {
    this.onFiltersSubscription = this.taskService.onTaskRefreshEvent.subscribe(this.refreshWithParams.bind(this));
    this.onSearchSubscription = this.taskService.onSearchTaskEvent.subscribe(this.onSearch.bind(this));
    this.onViewChangedSubscription = this.taskService.onViewChangedEvent.subscribe(this.changeDisplay.bind(this));

    this.canCreateWP = this.permission.projectPermission.canCreateWP;
    this.canCreateCategory = this.permission.projectPermission.canCreateCategory;
    this.isOpen = this.category.id == 0 ? true : false;
    this.loadWorkPackages();
  }

  ngOnDestroy() {
    this.onFiltersSubscription.unsubscribe();
    this.onSearchSubscription.unsubscribe();
    this.onViewChangedSubscription.unsubscribe();
  }

  openOrCollapse() {
    this.isOpen = !this.isOpen;
  }

  onPageChange(event: any) {
    this.loadWorkPackages();
  }

  refresh() {
    this.loadWorkPackages();
  }

  open(modal: any) {
    this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
  }

  openEditCategory(modal: any) {
    this.copiedCategory = {...this.category};
    this.modalService.open(modal).result.then((result: any) => {
      if(result == "Save") {
        this.categoryService.editCategory(this.copiedCategory.id, {
          name: this.copiedCategory.name
        }).subscribe((response: any) => {
          this.category = response;
          this.ref.markForCheck();
        },
        this.errorHandle.bind(this))
      }
    }, (reason: any) => {
    });
  }

  openDeleteCategory(modal: any) {
    this.copiedCategory = {...this.category};
    this.modalService.open(modal).result.then((result: any) => {
      if(result == "Delete") {
        this.categoryService.deleteCategory(this.copiedCategory.id).subscribe((response: any) => {
          if(response.status == 'success') {
            this.deleted.emit(this.copiedCategory.id);
          }
          else {
            this.errorHandle(response);
          }
        },
        this.errorHandle.bind(this))
      }
    }, (reason: any) => {
    });
  }

  onNewTaskCreated(event: any) {
    this.loadWorkPackages();
  }

  private loadWorkPackages() {
    this.loading = true;
    let size = this.pageInfo.size;
    let offset = this.pageInfo.page;
    let filters = this.filters.concat(this.searchFilter);
    this.ref.markForCheck();
    this.taskService.getByCategory(this.projectId, this.category.id, filters, size, offset).toPromise()
    .then(
      (response: any) => {
        this.updateCache(response._embedded.elements);
        this.originWorkPackages = response._embedded.elements;
        this.pageInfo.total = response.total;
        this.updateShowingWorkPackages();
        this.loading = false;
        this.ref.markForCheck();
      }
    )
  }

  private updateCache(workPackages: any[]) {
    workPackages.forEach((item: any) => {
      let wp = this.halResourceService.createHalResource<WorkPackageResource>(item);
      this.wpCacheService.updateWorkPackage(wp, true);
    });
  }

  private updateShowingWorkPackages() {
    this.filteredWorkPackages = this.originWorkPackages.filter(
      (wp: any) => {
        if (this.isShowSubtask || this.noSubTasks) {
          return wp._links.parent.href == null && 
                (this.category.id == 0 ? wp._links.category.href == null : wp._links.category.href === `/api/v3/categories/${this.category.id}`);
        }
  
        return this.category.id == 0 ? wp._links.category.href == null : wp._links.category.href === `/api/v3/categories/${this.category.id}`;
      }
    )
    this.ref.markForCheck();
  }

  private changeDisplay(params: any) {
    if (params.displayType != "") {
      this.displayType = params.displayType;
      this.updateShowingWorkPackages();
    }
  }

  private refreshWithParams(params: any) {
    this.filters = params.filters;
    this.order = params.orderBy;
    this.loadWorkPackages();
  }

  private onSearch(params: any) {
    this.searchFilter = params.filters;
    if (params.filters[0]['subjectOrId']['values'] == '') {
      this.searchFilter = [];
    }
    this.loadWorkPackages();
  }

  private errorHandle(error: any) {
    this.errorService.show(error.message);
  }
}