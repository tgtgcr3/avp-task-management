import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { StateService } from "@uirouter/core";
import { ProjectsService } from "../../../../services/projects.service";
import { ErrorDialogService, LoaderService } from "../../../../../common/components";

@Component({
  templateUrl: './project-tasks-board.component.html',
  selector: 'project-tasks-board'
})
export class ProjectTasksBoardComponent implements OnInit {

  public categories: any[] = [];
  private projectStatus: any = 0;

  constructor(readonly $state: StateService,
    private projectService: ProjectsService,
    private loader: LoaderService,
    private errorService: ErrorDialogService) { }

  async ngOnInit() {
    await this.loadCategories();
    this.loadProjectStatus();
  }

  public get editable() {
    return this.projectStatus == 1;
  }

  private get projectId() {
    return this.$state["params"].projectId;
  }

  private loadProjectStatus() {
    this.projectService.getStatus(this.projectId).subscribe(
      (response: any) => {
        this.projectStatus = response.status;
      }
    )
  }

  private async loadCategories() {
    const response = await this.projectService.getCategories(this.projectId);
    this.categories = response._embedded.elements;
  }

  private errorHandle(error: any) {
    this.errorService.show(error.message);
    this.loader.loading(false);
  }
}