import { Component, Input, OnDestroy, OnInit, ViewChild, ChangeDetectorRef, ElementRef, OnChanges, EventEmitter, Output } from '@angular/core';
import { StateService, TransitionService, Transition } from '@uirouter/core';
import { States } from "core-components/states.service";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { Subscription } from 'rxjs';
import { DragAndDropService } from "core-app/modules/boards/drag-and-drop/drag-and-drop.service";
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { TasksService } from "../../../../../../tasks/services/tasks.service";
import { WorkPackageResource } from "core-app/modules/hal/resources/work-package-resource";
import { PermissionService } from '../../../../../../common/services/permission.service';

@Component({
  templateUrl: './tasks-board-list.component.html',
  selector: 'tasks-board-list'
})
export class TasksBoardListComponent implements OnInit, OnDestroy {

  @ViewChild("container") container: ElementRef;
  @ViewChild("wpModal") wpModal: ElementRef;

  @Input() category: any;
  @Input() editable: boolean = true;

  private filters: any[] = [];
  private order: any[] = [];
  private searchFilter: any[] = [];
  private _workPackages: any[] = [];
  private _counting: number = 0;

  private onFiltersSubscription: Subscription;
  private onSearchSubscription: Subscription;

  private unregisterListener: Function;
  private isInActive: boolean = false;
  private permission: any;
  private selectedWP: any;
  private wpPopup: NgbModalRef;

  constructor(readonly $state: StateService,
    readonly states: States,
    readonly trans: TransitionService,
    readonly dragService: DragAndDropService,
    private modalService: NgbModal,
    private taskService: TasksService,
    private permissionService: PermissionService,
    private wpCacheService: WorkPackageCacheService) { }

  async ngOnInit() {
    this.registerDragAndDrop();

    this.onFiltersSubscription = this.taskService.onTaskRefreshEvent.subscribe(this.refreshWithParams.bind(this));
    this.onSearchSubscription = this.taskService.onSearchTaskEvent.subscribe(this.onSearch.bind(this));
    this.permission = await this.permissionService.getCurrentUserPermission(`${this.projectId}`);

    this.loadTasks();
    this.unregisterListener = this.trans.onSuccess({}, (transition: Transition) => {
      if (this.isInActive) return undefined;

      this.selectedWP = undefined;
      let param = transition.params('to');
      if (this.wpPopup) {
        this.wpPopup.dismiss();
      }

      if (param.task) {
        this.toWPPopup();
        return true;
      }

      return false;
    });

    if (this.taskId) {
      this.toWPPopup();
    }
  }

  ngOnDestroy() {
    this.dragService.remove(this.container.nativeElement);
    this.onFiltersSubscription.unsubscribe();
    this.onSearchSubscription.unsubscribe();
    this.isInActive = true;
    this.unregisterListener();
  }

  public onNewTaskCreated(wps: any[]) {
    this.loadTasks();
  }

  public toWPPopup() {
    this.wpCacheService.loadWorkPackage(this.taskId).values$()
      .subscribe((wp: WorkPackageResource) => {
        this.selectedWP = wp;
      });

    this.wpPopup = this.modalService.open(this.wpModal, { size: 'lg', windowClass: '-task-display', backdrop: 'static' });
  }

  public open(modal: any) {
    this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
  }

  public get stateName() {
    return this.$state.current.name;
  }

  public get workPackages() {
    return this._workPackages;
  }

  public get name() {
    return this.category ? this.category.name : "Chưa phân loại";
  }

  public get categoryId() {
    return this.category ? this.category.id : 0;
  }

  public get counting() {
    return `${this._counting}`;
  }

  public get projectId() {
    return this.$state["params"].projectId;
  }

  public get taskId() {
    return this.$state.params['task'];
  }

  public get canCreateWP() {
    return this.permission && this.permission.projectPermission.canCreateWP;
  }

  private get done() {
    return this.workPackages.filter((task: any) => {
      return (this.categoryId == 0 ? task._links.category.href == null : task._links.category.href === `/api/v3/categories/${this.categoryId}`)
        && task._links.status.title.toLowerCase().includes('done');
    });
  }

  private loadTasks() {
    let filters = this.filters.concat(this.searchFilter);
    this.taskService.getByCategory(this.projectId, this.categoryId, filters, undefined, undefined, this.order)
      .subscribe(
        (response: any) => {
          this._workPackages = response._embedded.elements;
          this._counting = this._workPackages.length;
        }
      )
  }

  private refreshWithParams(params: any) {
    this.filters = params.filters;
    this.order = params.orderBy;
    this.loadTasks();
  }

  private onSearch(params: any) {
    this.searchFilter = params.filters;
    if (params.filters[0]['subjectOrId']['values'] == '') {
      this.searchFilter = [];
    }
    this.loadTasks();
  }

  private registerDragAndDrop() {
    this.dragService.register({
      dragContainer: this.container.nativeElement,
      scrollContainers: [this.container.nativeElement],
      moves: (card: HTMLElement) => true,
      accepts: () => {
        console.log(this.categoryId);
        return true;
      },
      onMoved: (card: HTMLElement) => {

      },
      onRemoved: (card: HTMLElement) => {
        this._counting -= 1;
      },
      onAdded: async (card: HTMLElement) => {
        const wpId: string = card.dataset.workPackageId!;
        const result = await this.onChangeCategory(wpId);
        return result;
      }
    });
  }

  async onChangeCategory(wpId: string) {
    try {
      const workPackage = this.states.workPackages.get(wpId).value!;
      let payload = {
        _links: {
          category: {
            href: this.categoryId == 0 ? null : `/api/v3/categories/${this.categoryId}`
          }
        },
        lockVersion: workPackage.lockVersion
      };
      await workPackage.updateImmediately(payload);
      await this.wpCacheService.require(wpId, true);
      this._counting += 1;
      return true;
    }
    catch (e) {
      console.log(e);
    }
    return false;
  }
}