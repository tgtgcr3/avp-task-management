import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { StateService } from "@uirouter/core";
import { Subscription } from 'rxjs';
import { ProjectsService } from "../../../../services/projects.service";
import { TasksService } from "../../../../../tasks/services/tasks.service";
import { WorkPackageEmbeddedTableComponent } from "core-components/wp-table/embedded/wp-embedded-table.component";

@Component({
  template:
    `<wp-embedded-table #embeddedTable *ngIf="loaded"
                        [queryProps]="queryProps" 
                        [configuration]="configuration"
                        [externalHeight]="true">
    </wp-embedded-table>`,
  selector: 'project-gantt-view'
})
export class ProjectGanttViewComponent implements OnInit, OnDestroy {

  @ViewChild('embeddedTable') protected embeddedTable: WorkPackageEmbeddedTableComponent;

  constructor(readonly $state: StateService,
    private projectService: ProjectsService,
    private taskService: TasksService) { }

  private _queryProps: any = {
    "columns[]": ["subject", "startDate", "dueDate", "status", "assignee"],
    "filters": []
  };

  public configuration: Partial<any> = {
    actionsColumnEnabled: false,
    columnMenuEnabled: false,
    hierarchyToggleEnabled: false,
    contextMenuEnabled: false,
    forcePerPageOption: false,
    projectContext: true
  };

  public loaded: boolean = false;

  private onSearchSubscription: Subscription;
  private filters: any = [{ "type": { "operator": "=", "values": ["1"] } }, { "project": { "operator": "=", "values": [`${this.projectId}`] } }];

  ngOnInit(): void {
    this.onSearchSubscription = this.taskService.onSearchTaskEvent.subscribe(this.onSearch.bind(this));
    this._queryProps.filters = JSON.stringify(this.filters);
    this.loaded = true;
  }

  ngOnDestroy() {
    this.onSearchSubscription.unsubscribe();
  }

  public get projectId() {
    return this.$state["params"].projectId;
  }

  public get queryProps() {
    return this._queryProps;
  }

  private onSearch(params: any) {
    this.loaded = false;
    let queryProps = this._queryProps;
    let filters = this.filters.filter((f: any) => {
      return Object.keys(f).indexOf('subjectOrId') > -1;
    });
    if (filters.length > 0) {
      let index = this.filters.indexOf(filters[0]);
      this.filters.splice(index, 1);
    }

    if (params.filters[0]['subjectOrId']['values'] != '') {
      this.filters.push(params.filters[0]);
    }
    queryProps.filters = [];
    queryProps.filters = JSON.stringify(this.filters);

    this._queryProps = queryProps;
    this.embeddedTable.isInitialized && this.embeddedTable.loadQuery(true, false);
    this.loaded = true;
  }
}