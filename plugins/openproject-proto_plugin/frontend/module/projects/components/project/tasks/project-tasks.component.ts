import { Component, OnInit, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { StateService } from '@uirouter/core';
import { Subscription } from 'rxjs';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { UserCacheService } from "core-components/user/user-cache.service";
import { ProjectCacheService } from "core-components/projects/project-cache.service";
import { forkJoin } from 'rxjs';
import { ProjectsService } from "../../../services/projects.service";
import { ErrorDialogService, LoaderService } from "../../../../common/components";
import { TasksService } from "../../../../tasks/services/tasks.service";
import { PermissionService } from '../../../../common/services/permission.service';

@Component({
  templateUrl: './project-tasks.component.html',
  selector: 'project-tasks',
})
export class ProjectTasksComponent implements OnInit, OnDestroy {

  public projectStatus: any = 0;
  public categories: any[] = [];
  public loaded: boolean = false;
  
  private onRefreshSubscription: Subscription;
  private onExportSubscription: Subscription;

  public filters: any[] = [];
  public order: any[] = [];
  public permission: any;

  constructor(readonly $state: StateService,
    readonly userCacheService: UserCacheService,
    private currentUserService: CurrentUserService,
    private projectService: ProjectsService,
    private taskService: TasksService,
    private projectCacheService:ProjectCacheService,
    private permissionService: PermissionService,
    private errorService: ErrorDialogService,
    private loaderService: LoaderService,
    private ref: ChangeDetectorRef) {
     }

  ngOnInit() {
    this.onRefreshSubscription = this.taskService.refreshRequest.subscribe(this.refresh.bind(this));
    this.onExportSubscription = this.projectService.onExportEvent.subscribe(this.export.bind(this));

    this.loaderService.loading(true);

    forkJoin([
      this.permissionService.getCurrentUserPermission(`${this.projectId}`),
      this.projectService.getStatus(this.projectId)
    ]).toPromise().then(
      (results: any[]) => {
        this.permission = results[0];
        this.projectStatus = results[1].status;
        this.loaded = true;
        this.loaderService.loading(false);
      }
    )
    .catch(
      (error: any) => {
        this.errorHandle(error);
        this.loaded = true;
      }
    );
  }

  ngOnDestroy() {
    this.onRefreshSubscription.unsubscribe();
    this.onExportSubscription.unsubscribe();
  }
  
  public refresh() {
    this.loadCategories();
  }

  public get projectId() {
    return this.$state["params"].projectId;
  }

  public get editable() {
    return this.projectStatus == 1;
  }

  public onActionBarInitialized(initParams: any) {
    this.filters = initParams.filters;
    this.order = initParams.orderBy;
    this.loadCategories();
  }

  public onCategoryDeleted(categoryId: number) {
    let index = this.categories.findIndex((category: any) => {
      return categoryId == category.id;
    });
    if(index > -1) {
      this.categories.splice(index, 1);
    }
  }

  private loadCategories() {
    this.projectService.getCategories(this.projectId).then(
      (response: any) => {
        let noneCategory = [
          {
            id: 0, 
            name: 'Chưa phân loại'
          }
        ];
        
        this.categories = noneCategory.concat(response._embedded.elements);
      }
    );
  }

  private export(params: any) {
    let filters = params.filters;
    let order = params.orders;
    forkJoin([
      this.projectCacheService.require(this.projectId),
      this.userCacheService.require(this.currentUserService.userId),
      this.taskService.getByProject(this.projectId, filters, order)
    ]).toPromise().then(
      (results: any[]) => {
        let project = results[0];
        let currentUser = results[1];
        let tasks = results[2]._embedded.elements;
        this.taskService.exportByProject(currentUser.name, this.currentUserService.title, project.name, this.categories, tasks);
      }
    )
    .catch(
      (error: any) => {
        this.errorHandle(error);
      }
    )
  }

  private errorHandle(error: any) {
    this.errorService.show(error.message);
    this.loaderService.loading(false);
  }
}