import { Component, OnInit } from '@angular/core';
import { StateService, UIRouter } from '@uirouter/core';
import { UserCacheService } from "core-components/user/user-cache.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { ProjectCacheService } from 'core-components/projects/project-cache.service';
import { AVP_VIEWS } from "../../../../common/consts/view-name";
import { TasksService } from "../../../../tasks/services/tasks.service";
import { ErrorDialogService } from "../../../../common/components";

@Component({
  templateUrl: 'project-layout.component.html',
  selector: 'project-layout'
})
export class ProjectLayoutComponent implements OnInit {

  public showingTask: any;
  public currentUser: any = {};
  public project: any;
  public sideShowing: boolean = false;
  private currentUrl: string = "";

  constructor(private taskService: TasksService,
    private uiRouter: UIRouter,
    private errorDialogService: ErrorDialogService,
    private currentUserService: CurrentUserService,
    readonly $state: StateService,
    readonly userCacheService: UserCacheService,
    readonly projectCacheService: ProjectCacheService) { }

  ngOnInit() {
    this.uiRouter.urlService.onChange(this.routerChanged.bind(this));

    this.loadCurrentUser();
    this.loadProject();
  }

  public get boardView() {
    return this.$state.current.name == AVP_VIEWS.PROJECT_TASKS_BOARD;
  }

  public get isTaskShowing() {
    return this.$state.params['task'] != null && !this.boardView;
  }

  public get noSidebar() {
    return this.$state.current.name == AVP_VIEWS.PROJECT_REPORTS
    || this.$state.current.name == AVP_VIEWS.PROJECT_TASKS_GANTT
    || this.$state.current.name == AVP_VIEWS.PROJECT_TASKS_BOARD
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_INFO
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_MEMBERS
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_PERMISSIONS
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_MAIL_NOTIFY
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_REPEATED
    || this.$state.current.name == AVP_VIEWS.PROJECT_TOPICS
    || this.$state.current.name == AVP_VIEWS.PROJECT_TOPIC
    || this.$state.current.name == AVP_VIEWS.PROJECT_DOCUMENTS
    || this.currentUrl.includes(`/avp/projects/${this.projectId}/report`) 
    || this.currentUrl.includes('/gantt') 
    || this.currentUrl.includes('/board');
  }

  private routerChanged(params: any) {
    this.currentUrl = params.url;
  }

  private loadCurrentUser() {
    this.userCacheService
      .require(this.currentUserService.userId)
      .then((user: any) => {
        this.currentUser = user;
      })
      .catch(this.errorHandle.bind(this));
  }

  private loadProject() {
    this.projectCacheService
      .require(this.projectId)
      .then((project: any) => {
        this.project = project;
      })
      .catch(this.errorHandle.bind(this));
  }

  private errorHandle(error: any) {
    console.error(error);
    this.errorDialogService.show(error.message);
  }

  private get projectId() {
    return this.$state["params"].projectId;
  }
}