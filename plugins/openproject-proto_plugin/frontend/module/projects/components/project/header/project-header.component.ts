import {Component, OnInit, Input} from '@angular/core';
import {MainMenuToggleService} from "core-components/main-menu/main-menu-toggle.service";
import {ProjectsService} from "../../../services/projects.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { UserCacheService } from "core-components/user/user-cache.service";
import { StateService } from "@uirouter/core";
import { PermissionService } from '../../../../common/services/permission.service';

@Component({
  templateUrl: './project-header.component.html',
  selector: 'project-header'
})
export class ProjectHeaderComponent implements OnInit {

  @Input() project: any;

  private currentUser: any = {};
  private permission: any;
  private roles: string[] = [];

  constructor(public readonly toggleService: MainMenuToggleService,
              readonly $state: StateService,
              readonly userCacheService: UserCacheService,
              private userService: CurrentUserService,
              private permissionService: PermissionService,
              private projectService: ProjectsService) { }

  async ngOnInit() { 
    this.userCacheService
      .require(this.userService.userId)
      .then((response: any) => this.currentUser = response)
      .catch((error: any) => console.error(error));

    this.permission = await this.permissionService.getCurrentUserPermission(this.projectId);
    this.roles = await this.projectService.getRoles(this.projectId);
  }

  public get avatar() {
    return this.project ? this.project.name.substring(0, 1) : '';
  }

  public get name() {
    return this.project ? this.project.name : '';
  }

  public get projectId() {
    return this.$state["params"].projectId;
  }

  public get currentView() {
    return this.$state.current.name;
  }

  public get canViewReport() {
    return this.permission && this.permission.projectPermission.canViewReport;
  }

  public onChangeView(view: string) {
    this.projectService.onViewChanged(view);
  }

  public get isAdmin() {
    return this.roles.includes('Project admin') || this.currentUser.owner;
  }
}