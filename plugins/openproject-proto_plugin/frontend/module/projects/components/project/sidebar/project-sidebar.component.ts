import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { ProjectsService } from "../../../services/projects.service";
import { ErrorDialogService } from "../../../../common/components";
import { MembersService } from "../../../../members/services/members.service";
import { StateService } from "@uirouter/core";
import { AvpUtils } from "../../../../common/services/avp-utils.service";
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AVP, NotificationType, formatted_date } from '../../../../common/consts/avp';
import { environment } from "environments/environment";
import { UserCacheService } from "core-components/user/user-cache.service";
import { AvpNotificationService } from '../../../../avp-base/notifications/avp-notifications.service';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './project-sidebar.component.html',
  selector: 'project-sidebar'
})
export class ProjectSidebarComponent implements OnInit {

  public showMembers: boolean = false;
  public showStatus: boolean = false;
  public showWarnings: boolean = false;
  public showNews: boolean = false;
  public showAuthored: boolean = false;
  public showOverdue: boolean = false;
  public showNotStarted: boolean = false;
  public statisticByMembers: boolean = false;
  public reportByStatus: any;
  public reportByMembers: any[] = [];
  public reportLoaded: boolean = false;
  public selectingOption: any = {
    member: {},
    isAdmin: false
  };
  public reportData: any = {
    done: [],
    doneLate: [],
    overdue: [],
    inProgress: [],
    new: [],
    percentage: 0,
    doneRatio: '0/0',
    myAssigned: [],
    myDone: [],
    myInProgress: [],
    myNew: [],
    myOverdue: [],
    authoredAndOverdue: []
  };

  private project: any;

  private tobeAddedMembers: any[] = [];
  private tobeAddedAdmins: any[] = [];

  private customValueDueDateId = '';
  private customValueStartDateId = '';
  private currentUser: any;

  constructor(readonly $state: StateService,
    private projectService: ProjectsService,
    private membersService: MembersService,
    private userService: CurrentUserService,
    private userCacheService: UserCacheService,
    private notificationService: AvpNotificationService,
    private errorHandleService: ErrorDialogService,
    private modalService: NgbModal,
    private logAdapter: LogAdapter) { }

  ngOnInit(): void {
    this.loadProject();
    this.loadUser();
  }

  public get members() {
    return this.project && this.project.members ? this.project.members : [];
  }

  public get memberCount() {
    return this.members.length;
  }

  public get admins() {
    return this.members.filter((member: any) => {
      return member["roles"].some((role: any) => {
        return role.name.toLowerCase() == "project admin";
      });
    });
  }

  public get editable() {
    return this.project ? this.project.status == 1 && this.isAdmin : 0;
  }

  public get adminIds() {
    return this.admins.map((member: any) => member.user.id);
  }

  public get memberIds() {
    return this.notAdmins.map((member: any) => member.user.id);
  }

  public get notAdmins() {
    return this.members.filter((member: any) => {
      return member["roles"].every((role: any) => {
        return role.name.toLowerCase() == "member";
      });
    });
  }

  public get stateName() {
    return this.$state.current.name;
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public get status() {
    let sttVal = '';
    if (this.project) {
      let filtered = this.project["custom_values"].filter((cv: any) => {
        return cv.custom_field.name.toLowerCase() == "status";
      });

      if (filtered.length != 0) {
        sttVal = filtered[0].value;
      }
    }

    switch (sttVal) {
      case 'off-track':
        return { name: 'off-track', label: 'Chậm tiến độ' };
      case 'high-risk':
        return { name: 'high-risk', label: 'Nguy cơ cao' };
      default:
        return { name: 'on-track', label: 'Đúng tiến độ' };
    }
  }

  public get name() {
    return this.project ? this.project.name : '';
  }

  public get description() {
    return this.project && this.project.description ? this.project.description : 'Chưa có mô tả';
  }

  public get startDate() {
    let startDate: any;
    if (this.project) {
      let filtered = this.project["custom_values"].filter((cv: any) => {
        return cv.custom_field.name.toLowerCase() == "startdate";
      });

      if (filtered.length != 0) {
        startDate = filtered[0].value;
      }
    }

    return startDate ? moment(formatted_date(startDate)).format('MM/DD/YYYY') : undefined;
  }

  public get dueDate() {
    let dueDate: any;
    if (this.project) {
      let filtered = this.project["custom_values"].filter((cv: any) => {
        return cv.custom_field.name.toLowerCase() == "duedate";
      });

      if (filtered.length != 0) {
        dueDate = filtered[0].value;
      }
    }

    return dueDate ? moment(formatted_date(dueDate)).format('MM/DD/YYYY') : undefined;
  }

  public get department() {
    return this.project && this.project.parent.id != AVP.id ? this.project.parent.name : '--';
  }

  public get memberReport() {
    return this.reportByMembers;
  }

  public get total() {
    return this.project ? this.project.work_packages.length : 0;
  }

  public get isAdmin() {
    return this.admins.some((member: any) => member.user.id == this.userService.userId);
  }

  public get canSaveAdmin() {
    return this.tobeAddedAdmins.length != 0;
  }

  public get canSaveMember() {
    return this.tobeAddedMembers.length != 0;
  }

  public openModal(modal: any) {
    this.modalService.open(modal, { backdrop: 'static' });
  }

  public onAdminChanged(selectedMembers: any[]) {
    this.tobeAddedAdmins = selectedMembers;
  }

  public onMemberChanged(selectedMembers: any[]) {
    this.tobeAddedMembers = selectedMembers;
  }

  public onAddAdmins(modal: any) {
    this.onSaveMemberships(this.tobeAddedAdmins, true, modal);
  }

  public onAddMembers(modal: any) {
    this.onSaveMemberships(this.tobeAddedMembers, false, modal);
  }

  public onUpdateDueDate(evt: any) {
    if (!this.isValidDate(this.startDate, evt)) {
      this.errorHandleService.show('Ngày Bắt Đầu phải bé hơn Ngày Kết Thúc!');
      return false;
    }

    let payload: any = {};
    payload["dueDate"] = {
      custom_value_id: this.customValueDueDateId,
      custom_value_value: evt ? evt : undefined
    };
    this.onSave(payload);
    return true;
  }

  public onUpdateStartDate(evt: any) {
    if (!this.isValidDate(evt, this.dueDate)) {
      this.errorHandleService.show('Ngày Bắt Đầu phải bé hơn Ngày Kết Thúc!');
      return false;
    }

    let payload: any = {};
    payload["startDate"] = {
      custom_value_id: this.customValueStartDateId,
      custom_value_value: evt ? evt : undefined
    };
    this.onSave(payload);
    return true;
  }

  public onMemberOptionSelect(member: any, isAdmin: boolean = false) {
    this.selectingOption.member = member;
    this.selectingOption.isAdmin = isAdmin;
  }

  public onRemoveAdmin(member: any) {
    let payload: any = {
      project_id: this.projectId,
      member: {
        "tobeDeleted": [],
        "tobeAdded": []
      }
    };

    payload["member"]["tobeDeleted"].push({
      user_id: member.user_id,
      roles: ['admin']
    });

    this.membersService.updateAll(payload).subscribe(
      (response: any) => {
        this.loadProject();
        this.logAdapter.logging(ActivityActionType.UpdateProject, this.projectId, response.name);
      }
    )
  }

  public onSetAdmin(member: any) {
    let payload: any = {
      project_id: this.projectId,
      member: {
        "tobeDeleted": [],
        "tobeAdded": []
      }
    };

    payload["member"]["tobeAdded"].push({
      user_id: member.user_id,
      roles: ['admin']
    });

    this.membersService.updateAll(payload).subscribe(
      (response: any) => {
        this.loadProject();
        this.logAdapter.logging(ActivityActionType.UpdateProject, this.projectId, response.name);
      }
    )
  }

  public onDeleteMember(member: any) {
    return this.membersService.delete(member.id).subscribe(
      (response: any) => {
        this.loadProject();
        this.logAdapter.logging(ActivityActionType.UpdateProject, this.projectId, response.name);
      },
      this.errorHandler.bind(this)
    );
  }

  public navToUser(member: any) {
    if (member.user.username != "")
      window.open(`${environment.avpAccountUrl}/userDetails/${member.user.username}`, '_blank');
  }

  private isValidDate(startDate: any, dueDate: any) {
    if (!startDate || !dueDate) {
      return true;
    }
    return moment(formatted_date(startDate)).isSameOrBefore(moment(formatted_date(dueDate)));
  }

  private onSaveMemberships(users: any[], isAdmin = false, modal: any) {
    let payload: any = {
      project_id: this.projectId,
      member: {
        "tobeDeleted": [],
        "tobeAdded": []
      }
    };

    users.forEach((user: any) => {
      payload["member"]["tobeAdded"].push({
        user_id: user.id,
        roles: isAdmin ? ['admin'] : ['member']
      });
    });

    let members = isAdmin ? this.admins : this.notAdmins;

    members.forEach((member: any) => {
      let byAddedList = users.filter((user: any) => {
        return user.id == member.user_id;
      });

      if (byAddedList.length == 0) {
        payload["member"]["tobeDeleted"].push({
          user_id: member.user_id,
          roles: isAdmin ? ['admin'] : ['member']
        });
      }
    });

    this.membersService.updateAll(payload).subscribe(
      (response: any) => {
        modal.dismiss();
        this.loadProject();
        this.logAdapter.logging(ActivityActionType.UpdateProject, this.projectId, response.name);
      }
    )
  }

  private sendNotify() {
    let notifyData: any = {
      project: this.project,
      author: this.currentUser,
      receivers: []
    }

    this.notificationService.notify(NotificationType.ADD_USER_TO_PROJECT, notifyData);
  }

  private initReports() {

    this.reportData = {
      done: this.filterStatus('done'),
      doneLate: this.filterStatus('done late'),
      overdue: this.filterStatus('overdue'),
      inProgress: this.filterStatus('in progress'),
      new: this.filterStatus('new'),
      percentage: 0,
      doneRatio: '0/0',
      myAssigned: [],
      myDone: [],
      myInProgress: [],
      myNew: [],
      myOverdue: [],
      authoredAndOverdue: []
    };

    this.reportData['percentate'] = ((this.reportData.done.length + this.reportData.doneLate.length) * 100 / this.total).toFixed(2);
    this.reportData['doneRatio'] = `${this.reportData.done.length +  this.reportData.doneLate.length}/${this.total}`;

    this.reportData['myNew'] = this.project.work_packages.filter((wp: any) => {
      return wp.assigned_to_id == this.userService.userId && wp.status_id == 1;
    });

    this.reportData['myInProgress'] = this.project.work_packages.filter((wp: any) => {
      return wp.assigned_to_id == this.userService.userId && wp.status_id == 7;
    });

    this.reportData['myDone'] = this.project.work_packages.filter((wp: any) => {
      return wp.assigned_to_id == this.userService.userId && (wp.status_id == 16 || wp.status_id == 17);
    });

    this.reportData['myAssigned'] = this.filterStatus('in progress', true).concat(this.filterStatus('new', true));

    this.reportData['myOverdue'] = this.filterStatus('overdue', true);

    this.reportData['authoredAndOverdue'] = this.project.work_packages.filter((wp: any) => {
      return wp.author_id == this.userService.userId && this.checkOverdue(wp);
    });

    this.reportByStatus = {
      labels: [
        `${this.reportData.done.length} HT đúng hạn`,
        `${this.reportData.doneLate.length} HT muộn`,
        `${this.reportData.overdue.length} Quá hạn`,
        `${this.reportData.inProgress.length} Đang thực hiện`,
        `${this.reportData.new.length} Phải thực hiện`],
      data: [
        this.reportData.done.length,
        this.reportData.doneLate.length,
        this.reportData.overdue.length,
        this.reportData.inProgress.length,
        this.reportData.new.length,
      ]
    };

    this.reportByMembers = [];
    this.members.forEach((member: any) => {
      let report = member.user;
      report['done'] = this.filterByMember('done', member.id);
      report['total'] = this.filterByMember('total', member.id);
      this.reportByMembers.push(report);
    });

    this.reportLoaded = true;
  }

  private loadProject() {
    this.projectService.get(this.projectId)
      .subscribe(
        (response: any) => {
          this.project = response;
          this.loadUsersInfo();
          this.initCustomValueIds();
          this.initReports();
        },
        this.errorHandler.bind(this)
      );
  }

  private loadUser() {
    this.userCacheService
      .require(this.userService.userId)
      .then((user: any) => {
        this.currentUser = user;
      });
  }

  private loadUsersInfo() {
    this.members.forEach((member: any) => {

      this.membersService.getUserInfo(member.user.mail).subscribe(
        (response: any) => {
          member.user.title = response.title;
          member.user.username = response.username;
        },
        (error: any) => {
          member.user.title = "";
          member.user.username = "";
        }
      )
    })
  }

  private filterStatus(status: string, isMyWP: boolean = false) {
    return this.project ? this.project.work_packages.filter((wp: any) => {
      let isOverdue = this.checkOverdue(wp);
      if (status == 'overdue')
        return isOverdue && (isMyWP == true ? wp.assigned_to_id == this.userService.userId : true);
      else if (status == 'in progress' || status == 'new')
        return !isOverdue && wp.status.name.toLowerCase().includes(status) && (isMyWP == true ? wp.assigned_to_id == this.userService.userId : true);
      return wp.status.name.toLowerCase() === status && (isMyWP == true ? wp.assigned_to_id == this.userService.userId : true);
    }) : [];
  }

  private filterByMember(status: string, assignee: number) {
    return this.project ? this.project.work_packages.filter((wp: any) => {
      if (status == 'done')
        return wp.status.name.toLowerCase().includes('done') && wp.assigned_to_id == assignee;
      else
        return wp.assigned_to_id == assignee;
    }) : [];
  }

  private checkOverdue(wp: any) {
    if (wp.due_date == null) {
      return false;
    }

    let dueDate = new Date(wp.due_date);
    return dueDate.getTime() < new Date().getTime() &&
      wp.status.name != 'Done'
      && wp.status.name != 'Done Late';
  }

  private initCustomValueIds() {
    let filtered = this.project["custom_values"].filter((cv: any) => {
      return cv.custom_field.name.toLowerCase() == "startdate";
    });

    if (filtered.length != 0) {
      this.customValueStartDateId = filtered[0].id;
    }

    filtered = this.project["custom_values"].filter((cv: any) => {
      return cv.custom_field.name.toLowerCase() == "duedate";
    });

    if (filtered.length != 0) {
      this.customValueDueDateId = filtered[0].id;
    }
  }

  private errorHandler(error: any) {
    console.error(error.message);
    this.errorHandleService.show(error.message);
  }

  private onSave(payload: any) {
    this.projectService.updateProject(this.projectId, payload)
      .subscribe((response: any) => {
        this.project = response;
        this.logAdapter.logging(ActivityActionType.UpdateProject, this.projectId, response.name);
      }, this.errorHandler.bind(this))
  }

  private get projectId() {
    return this.$state["params"].projectId;
  }
}