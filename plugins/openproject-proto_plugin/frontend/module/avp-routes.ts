import { Ng2StateDeclaration } from "@uirouter/angular";
import { TaskComponent, TasksComponent } from "./tasks/components/my-tasks";
import { AvpBaseComponent } from "./avp-base/avp-base.component";
import { TaskMasterComponent } from "./tasks/components/master/task-master.component";
import { AVPCalendarComponent } from "./tasks/components/calendar/calendar.component";
import { AVP_VIEWS } from "./common/consts/view-name";
import {
  ProjectListComponent,
  ProjectMasterComponent,
  ProjectLayoutComponent,
  ProjectTasksComponent,
  ProjectGanttViewComponent,
  ProjectTasksBoardComponent
} from "./projects/components";
import {
  DepartmentDetailMasterComponent,
  DepartmentDetailProjectsComponent,
  DepartmentListComponent
} from "./departments/components";
import { ProjectPageComponent, ReportsPageComponent } from "./projects-page/components"
import { MemberComponent, MemberDetailMasterComponent, MemberTasksComponent } from "./members/components"
import { ReportLayoutComponent } from "./reports";
import { ProjectSettingLayoutComponent, 
  ProjectSettingContentComponent, 
  ProjectSettingMemberComponent, 
  ProjectSettingPermissionComponent,
  ProjectSettingEmailComponent, 
  ProjectSettingRepeatedWPComponent} from "./projects/components/project/settings";
import { RepeatedTasksComponent } from "./tasks/components/repeated-task";
import { ProjectTopicListComponent, TopicComponent } from "./topics/components";
import { DocumentComponent } from "./documents/components";
import { MyEmployeeComponent } from "./tasks/components/my-employee/my-employee.component";

export const AVP_ROUTES: Ng2StateDeclaration[] = [
  {
    name: AVP_VIEWS.ROOT,
    url: '/avp',
    component: AvpBaseComponent,
    redirectTo: AVP_VIEWS.TASKS
  },
  {
    name: AVP_VIEWS.TASKS,
    parent: 'avp-root',
    url: '/tasks',
    redirectTo: AVP_VIEWS.TASKS_LIST,
    component: TaskMasterComponent
  },
  {
    name: AVP_VIEWS.TASKS_LIST,
    url: '?page&task',
    params: {
      page: {
        type: 'int',
        dynamic: true
      },
      task: {
        type: 'int',
        dynamic: true
      }
    },
    component: TasksComponent,
    data: {
      parent: AVP_VIEWS.TASKS,
      bodyClasses: 'project-canvas'
    },
  },
  {
    name: AVP_VIEWS.TASKS_TEAM,
    url: '/team?page&task',
    params: {
      page: {
        type: 'int',
        dynamic: true
      },
      task: {
        type: 'int',
        dynamic: true
      }
    },
    component: TasksComponent,
    data: {
      parent: AVP_VIEWS.TASKS,
      bodyClasses: 'project-canvas'
    },
  },
  {
    name: AVP_VIEWS.TASKS_TEAM_REPORT,
    url: '/team-report',
    component: MyEmployeeComponent,
    data: {
      parent: AVP_VIEWS.TASKS,
      bodyClasses: 'project-canvas'
    },
  },
  {
    name: AVP_VIEWS.TASKS_FILTERS,
    url: '/filters?queryId&page&task',
    params: {
      page: {
        type: 'int',
        dynamic: true
      },
      task: {
        type: 'int',
        dynamic: true
      }
    },
    component: TasksComponent,
    data: {
      parent: AVP_VIEWS.TASKS,
      bodyClasses: 'project-canvas'
    },
  },
  {
    name: AVP_VIEWS.TASKS_FOLLOWING,
    url: '/following?page&task',
    params: {
      page: {
        type: 'int',
        dynamic: true
      },
      task: {
        type: 'int',
        dynamic: true
      }
    },
    component: TasksComponent,
    data: {
      parent: AVP_VIEWS.TASKS,
      bodyClasses: 'project-canvas'
    },
  },
  {
    name: AVP_VIEWS.TASKS_CALENDAR,
    url: '/calendar?task',
    params: {
      task: {
        type: 'int',
        dynamic: true
      }
    },
    component: AVPCalendarComponent,
    data: {
      parent: AVP_VIEWS.TASKS,
      bodyClasses: 'project-canvas'
    },
  },
  {
    name: AVP_VIEWS.TASKS_REPEATED,
    url: '/repeated',
    component: RepeatedTasksComponent,
    data: {
      parent: AVP_VIEWS.TASKS,
      bodyClasses: 'project-canvas'
    },
  },
  {
    name: AVP_VIEWS.PROJECTS,
    parent: AVP_VIEWS.ROOT,
    url: '/projects',
    component: ProjectMasterComponent,
    redirectTo: AVP_VIEWS.PROJECTS_LIST
  },
  {
    name: AVP_VIEWS.PROJECTS_LIST,
    url: '?page',
    component: ProjectListComponent,
    params: {
      page: {
        type: 'int',
        dynamic: true
      }
    },
    data: {
      parent: AVP_VIEWS.ROOT,
      bodyClasses: 'project-master'
    }
  },
  {
    name: AVP_VIEWS.PROJECTS_ALL,
    url: '/all?page',
    component: ProjectListComponent,
    params: {
      page: {
        type: 'int',
        dynamic: true
      }
    },
    data: {
      parent: AVP_VIEWS.ROOT,
      bodyClasses: 'project-master'
    }
  },
  {
    name: AVP_VIEWS.PROJECTS_TOPICS,
    url: '/topics',
    component: ProjectTopicListComponent,
    data: {
      parent: AVP_VIEWS.ROOT,
      bodyClasses: 'project-master'
    }
  },
  {
    name: AVP_VIEWS.DEPARTMENTS,
    parent: AVP_VIEWS.ROOT,
    url: '/departments',
    component: DepartmentListComponent
  },
  {
    name: AVP_VIEWS.DEPARTMENT,
    parent: AVP_VIEWS.ROOT,
    url: '/departments/{departmentId: [0-9]+}',
    component: DepartmentDetailMasterComponent,
    redirectTo: AVP_VIEWS.DEPARTMENT_PROJECTS
  },
  {
    name: AVP_VIEWS.DEPARTMENT_PROJECTS,
    url: '',
    component: DepartmentDetailProjectsComponent,
    data: {
      parent: AVP_VIEWS.DEPARTMENT,
      bodyClasses: 'project-canvas'
    },
  },
  {
    name: AVP_VIEWS.PROJECT,
    parent: AVP_VIEWS.ROOT,
    url: '/projects/{projectId: [0-9]+}',
    component: ProjectLayoutComponent,
    redirectTo: AVP_VIEWS.PROJECT_TASKS
  },
  {
    name: AVP_VIEWS.PROJECT_TASKS,
    url: '?task&page',
    params: {
      page: {
        type: 'int',
        dynamic: true
      },
      task: {
        type: 'int',
        dynamic: true
      }
    },
    component: ProjectTasksComponent,
    data: {
      parent: AVP_VIEWS.PROJECT,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_TASKS_GANTT,
    url: '/gantt',
    component: ProjectGanttViewComponent,
    data: {
      parent: AVP_VIEWS.PROJECT,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_TASKS_BOARD,
    url: '/board?page&task',
    component: ProjectTasksBoardComponent,
    params: {
      page: {
        type: 'int',
        dynamic: true
      },
      task: {
        type: 'int',
        dynamic: true
      }
    },
    data: {
      parent: AVP_VIEWS.PROJECT,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_REPORTS,
    url: '/report?by&status&from&to',
    component: ReportsPageComponent,
    data: {
      parent: AVP_VIEWS.PROJECT,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_TOPICS,
    url: '/topics',
    component: ProjectTopicListComponent,
    data: {
      parent: AVP_VIEWS.PROJECT,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_TOPIC,
    url: '/topics/{topic: [0-9]+}?page',
    component: TopicComponent,
    params: {
      page: {
        type: 'int',
        dynamic: true
      }
    },
    data: {
      parent: AVP_VIEWS.PROJECT_TOPICS,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_DOCUMENTS,
    url: '/documents',
    component: DocumentComponent,
    data: {
      parent: AVP_VIEWS.PROJECT,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_SETTINGS,
    url: '/settings',
    component: ProjectSettingLayoutComponent,
    redirectTo: AVP_VIEWS.PROJECT_SETTINGS_INFO,
    data: {
      parent: AVP_VIEWS.PROJECT,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_SETTINGS_INFO,
    url: '',
    component: ProjectSettingContentComponent,
    data: {
      parent: AVP_VIEWS.PROJECT_SETTINGS,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_SETTINGS_MEMBERS,
    url: '/members',
    component: ProjectSettingMemberComponent,
    data: {
      parent: AVP_VIEWS.PROJECT_SETTINGS,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_SETTINGS_PERMISSIONS,
    url: '/permissions',
    component: ProjectSettingPermissionComponent,
    data: {
      parent: AVP_VIEWS.PROJECT_SETTINGS,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_SETTINGS_MAIL_NOTIFY,
    url: '/email',
    component: ProjectSettingEmailComponent,
    data: {
      parent: AVP_VIEWS.PROJECT_SETTINGS,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_SETTINGS_REPEATED,
    url: '/repeated',
    component: ProjectSettingRepeatedWPComponent,
    data: {
      parent: AVP_VIEWS.PROJECT_SETTINGS,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.PROJECT_PAGE,
    parent: AVP_VIEWS.ROOT,
    url: '/project-page',
    component: ProjectPageComponent,
  },
  {
    name: AVP_VIEWS.MEMBERS,
    parent: AVP_VIEWS.ROOT,
    url: '/members?page&query',
    component: MemberComponent,
    params: {
      page: {
        type: 'int',
        dynamic: true
      },
      query: {
        type: 'string',
        dynamic: true
      }
    }
  },
  {
    name: AVP_VIEWS.MEMBER,
    parent: AVP_VIEWS.ROOT,
    url: '/members/{memberId: [0-9]+}',
    component: MemberDetailMasterComponent,
    redirectTo: AVP_VIEWS.MEMBER_TASKS
  },
  {
    name: AVP_VIEWS.MEMBER_TASKS,
    url: '?page&task',
    params: {
      page: {
        type: 'int',
        dynamic: true
      },
      task: {
        type: 'int',
        dynamic: true
      }
    },
    component: MemberTasksComponent,
    data: {
      parent: AVP_VIEWS.MEMBER,
      bodyClasses: 'project-canvas'
    }
  },
  {
    name: AVP_VIEWS.REPORTS,
    parent: AVP_VIEWS.ROOT,
    url: '/reports?by&status&from&to&dep&subtaskopt',
    component: ReportLayoutComponent
  }
];