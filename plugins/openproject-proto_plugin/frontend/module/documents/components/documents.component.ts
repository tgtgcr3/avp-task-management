import {Component, OnInit} from '@angular/core';
import { StateService } from '@uirouter/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { ProjectCacheService } from 'core-components/projects/project-cache.service';
import { ErrorDialogService, LoaderService } from '../../common/components';
import { ProjectsService } from '../../projects/services/projects.service';
import * as moment from 'moment';

@Component({
  templateUrl: './documents.component.html',
  selector: 'documents'
})
export class DocumentComponent implements OnInit {

  private newAttachments: any[] = [];
  private projectResource: any;
  private attachmentSearchString: string = "";

  constructor(readonly $state: StateService,
    private modalService: NgbModal,
    private loaderService: LoaderService,
    private projectService: ProjectsService,
    private halResourceService: HalResourceService,
    private errorDialogService: ErrorDialogService,
    readonly projectCacheService: ProjectCacheService) { }

  ngOnInit() {
    this.projectCacheService
      .require(this.projectId)
      .then((project: any) => {
        this.projectResource = project;
      })
  }

  public open(modal: any) {
    this.modalService.open(modal, { backdrop: 'static' }).result.then(
      (result) => {
        this.projectResource.uploadAttachments(this.newAttachments);
      },
      (message) => {
        
      });
  }

  public openDelete(modal: any, tobeDeleteAttachment: any) {
    this.modalService.open(modal, { backdrop: 'static' }).result.then(
      (result) => {
        this.projectResource.removeAttachment(tobeDeleteAttachment);
      },
      (message) => {
        
      });
  }

  public onAttachmentsChanged(event: any) {
    console.log(event);
    let attachments = Array.from(event.target.files);
    attachments.forEach((attachment: any) => {
      attachment.description = {
        format: "plain",
        html: "",
        raw: ""
      }
    });

    this.newAttachments = attachments;
  }

  public formattedDate(date: any) {
    return moment(date).format('HH:mm DD-MM-YYYY');
  }

  public onSearch(event: any) {
    this.attachmentSearchString = event.target.value.toLowerCase();
  }

  public formatBytes(bytes: number, decimals: number = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  public get attachments() {
    return this.projectResource ? 
      this.projectResource['attachments']['elements'].filter((attachment: any) => {
        return attachment.fileName.toLowerCase().includes(this.attachmentSearchString);
      })
      : [];
  }

  private get projectId() {
    return this.$state.params['projectId'];
  }
}