import { Injectable, EventEmitter } from '@angular/core';
import { HttpHeaders } from "@angular/common/http";
import { BaseService } from '../../common/services/base.service';

@Injectable({
  providedIn: 'root'
})
export class TopicService extends BaseService {

  async getAll() : Promise<any> {
    let requestUrl = this.restUrl + '/avp_forum/api/v3/all';
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  async getByProject(projectId: string) : Promise<any> {
    let requestUrl = this.restUrl + `/avp_forum/api/v3/project/${projectId}/topics`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  async get(id: string) : Promise<any> {
    let requestUrl = this.restUrl + `/avp_forum/api/v3/topic/${id}`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  async getTopicRepresenter() : Promise<any> {
    let requestUrl = this.restUrl + `/avp_forum/api/v3/message_representer`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  async getPosts(topicId: string, page: number) : Promise<any> {
    let requestUrl = this.restUrl + `/avp_forum/api/v3/topics/${topicId}?page=${page}`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  addNewTopic(projectId: string, payload: any) {
    let requestUrl = this.restUrl + `/avp_forum/api/v3/project/${projectId}/topics`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post<any>(requestUrl, payload, { headers: headers });
  }

  replyOrQuote(payload: any, topicId: string) {
    let requestUrl = this.restUrl + `/avp_forum/api/v3/topics/${topicId}/reply_or_quote`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post<any>(requestUrl, payload, { headers: headers });
  }

  edit(payload: any, postId: string) {
    let requestUrl = this.restUrl + `/avp_forum/api/v3/topics/${postId}`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.put<any>(requestUrl, payload, { headers: headers });
  }

  delete(id: string) {
    let requestUrl = this.restUrl + `/avp_forum/api/v3/topics/${id}`;
    return this.httpClient.delete<any>(requestUrl);
  }
}