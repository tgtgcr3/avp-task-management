import {Component, OnInit} from '@angular/core';
import { CkeditorAugmentedTextareaComponent } from "core-app/ckeditor/ckeditor-augmented-textarea.component";
import { StateService } from '@uirouter/core';
import { HalResourceService } from 'core-app/modules/hal/services/hal-resource.service';
import { AvpNotificationService } from '../../../avp-base/notifications/avp-notifications.service';
import { ErrorDialogService, LoaderService } from '../../../common/components';
import { TopicService } from '../../services/topic.service';
import { NotificationType } from '../../../common/consts/avp';


@Component({
  templateUrl: './topic.component.html',
  selector: 'topic'
})
export class TopicComponent implements OnInit {
  
  private _topic: any = {};

  public page: number = 1;
  public isFormOpen: boolean = false;
  public replyData: any = {};
  public loaded: boolean = false;

  private editor: CkeditorAugmentedTextareaComponent;

  constructor(readonly $state: StateService,
    private loaderService: LoaderService,
    private errorDialogService: ErrorDialogService,
    private avpNotificationService: AvpNotificationService,
    private halResourceService: HalResourceService,
    private topicService: TopicService) { }

  async ngOnInit() {
    this.loaded = false;
    this.page = this.$state.params['page'] ? this.$state.params['page'] : 1;
    await this.loadPosts();
    this.loaded = true;
  }

  public onEditorInitialized(event: any) {
    this.editor = event;
  }

  public async onPageChange(event: any) {
    this.$state.go('.', { page: event });
    await this.loadPosts();
  }

  public async onReply(el: any) {
    const representer = await this.topicService.getTopicRepresenter();

    this.replyData = {
      isReply: true,
      subject: 'RE: ' + this.root.subject,
      source: representer,
      link: `/api/v3/posts/${this.topicId}`
    }
    this.isFormOpen = true;
    setTimeout(() => {
      el.scrollIntoView({behavior: 'smooth', block: 'end'});
    }, 500);
  }

  public onQuote(event: any, el: any) {
    let newSubject = event.subject.replace(/"/, "\"");
    newSubject = newSubject.startsWith('RE:') ? newSubject : 'RE: ' + newSubject;
    let newContent = `${event._embedded.author.name} bình luận: \n>`;
    newContent += event.description.raw.trim().replace(/(\r?\n|\r\n?)/, "\n> ") + "\n\n";
    event.description.raw = newContent;
    this.replyData = {
      isQuote: true,
      subject: newSubject,
      source: event,
      link: event._links.self.href
    };

    this.isFormOpen = true;
    setTimeout(() => {
      el.scrollIntoView({behavior: 'smooth', block: 'end'});
    }, 500);
  }

  public onEdit(event: any, el: any) {
    this.replyData = {
      isEdit: true,
      subject: event.subject,
      source: event,
      link: `/api/v3/posts/${event.id}`
    }

    this.isFormOpen = true;
    setTimeout(() => {
      el.scrollIntoView({behavior: 'smooth', block: 'end'});
    }, 500);
  }

  public onItemDeleted() {
    this.loadPosts();
  }

  public onPost() {
    let payload = {
      subject: this.replyData.subject,
      forum_id: this.forumId,
      content: this.editor.getContent()
    }

    if(!this.validatePost(payload)) {
      return;
    }

    let subscription: any = {};
    if(this.replyData.isQuote || this.replyData.isReply) {
      subscription = this.topicService.replyOrQuote(payload, this.topicId);
    }
    else {
      subscription = this.topicService.edit(payload, this.replyData.source.id);
    }

    subscription.subscribe(
      (response: any) => {
        this.loadPosts();
        this.errorDialogService.showSuccess();
        if(this.replyData.isQuote || this.replyData.isReply) {
          let halResource = this.halResourceService.createHalResource(response, true);
          this.sendNotify(halResource);
        }
      },
      (error: any) => {
        this.errorDialogService.show(error.message);
      }
    );
    this.isFormOpen = false;
  }

  public get posts() {
    return this._topic.elements;
  }

  public get root() {
    return this._topic.root;
  }

  public get total() {
    return this._topic.total;
  }

  public get forumId() {
    return this.root.forumId;
  }

  private async loadPosts() {
    this.loaderService.loading(true);
    this._topic = await this.topicService.getPosts(this.topicId, this.page);
    this.loaderService.loading(false);
  }

  private validatePost(payload: any) {
    if(!payload.subject) {
      this.errorDialogService.show('Xin nhập tiêu đề bình luận');
      return false;
    }
    if(!payload.content) {
      this.errorDialogService.show('Xin nhập nội dung bình luận');
      return false;
    }

    return true;
  }

  private sendNotify(response: any) {
    let notifyData: any = {
      id: response.id,
      receivers: this.recievers(response),
      author: response.author,
      project: response.project
    }
    this.avpNotificationService.notify(NotificationType.POST_ADDED, notifyData);
  }

  private recievers(message: any) {
    let receiverEmails = message.watchers ? message.watchers.elements.map((user: any) => user.email) : [];

    let authorEmail = message.author.email;

    if (!receiverEmails.includes(authorEmail) && authorEmail) {
      receiverEmails.push(authorEmail);
    }

    return receiverEmails.filter((email: string) => {
      return email != message.author.email;
    });
  }

  public get topicId() {
    return this.$state.params['topic'];
  }
}