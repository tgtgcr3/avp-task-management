import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StateService } from '@uirouter/core';
import { AVP } from '../../../../common/consts/avp';
import { AvpUtils } from '../../../../common/services/avp-utils.service';
import { TopicService } from '../../../services/topic.service';
import { ErrorDialogService } from '../../../../common/components';
import * as moment from 'moment';

@Component({
  templateUrl: './post.component.html',
  selector: 'post'
})
export class PostComponent implements OnInit {
  @Input() source: any = {};
  @Input() isFormOpened: boolean = false;
  @Input() isRoot: boolean = false;
  @Output() onQuote: EventEmitter<any> = new EventEmitter<any>();
  @Output() onEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() onDeleted: EventEmitter<any> = new EventEmitter<any>();

  constructor(private avpUtils: AvpUtils,
    readonly $state: StateService,
    private topicService: TopicService,
    private modalService: NgbModal,
    private errorDialogService: ErrorDialogService,
    private currentUser: CurrentUserService) { }

  ngOnInit() {

  }

  public quote() {
    let payload = {
      _type: 'Post',
      subject: this.subject,
      description: this.source.description,
      _embedded: this.source._embedded,
      _links: this.source._links
    };
    this.onQuote.emit(payload);
  }

  public edit() {
    this.onEdit.emit();
  }

  public delete(modal: any) {
    this.modalService.open(modal).result.then((result) => {
      if (result == 'OK') {
        this.topicService.delete(this.source.id).subscribe(
          (response: any) => {
            if(this.isRoot) {
              this.$state.go('project.topics', {projectId: this.projectId});
            }
            else {
              this.onDeleted.emit();
            }
          },
          (error: any) => {
            this.errorDialogService.show('Không thể xóa thảo luận');
          }
        )
      }
    });
  }

  public onAvatarLoadFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public get subject() {
    return this.source.subject;
  }

  public get content() {
    return this.source.description.html;
  }

  public get createdDate() {
    return moment(this.source.createdOn).format('HH:mm DD-MM-YYYY');
  }

  public get isAuthor() {
    return this.currentUser.userId == this.source._embedded.author.id;
  }

  public get author() {
    return this.source._embedded.author.name;
  }

  public get projectId() {
    return this.$state.params['projectId'];
  }
}