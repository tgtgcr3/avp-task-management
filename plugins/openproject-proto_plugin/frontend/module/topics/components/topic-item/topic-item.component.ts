import {Component, Input, OnInit} from '@angular/core';
import { AVP } from '../../../common/consts/avp';
import { AvpUtils } from '../../../common/services/avp-utils.service';

@Component({
  templateUrl: './topic-item.component.html',
  selector: 'topic-item'
})
export class TopicItemComponent implements OnInit {

  @Input() source: any = {};
  public watchers: any[] = [];

  constructor(private avpUtils: AvpUtils) { }

  ngOnInit() {
    this.watchers = this.source.watchers.slice(0, 4);
  }

  public get id() {
    return this.source.id;
  }

  public get name() {
    return this.source.subject
  }

  public get description() {
    return this.source.content;
  }

  public get authorName() {
    return this.source.author.firstname + ' ' + this.source.author.lastname;
  }

  public get replies_count() {
    return this.source.replies_count;
  }

  public get createdAt() {
    return this.source.created_on;
  }

  public get updatedAt() {
    return this.source.updated_on;
  }

  public get projectName() {
    return this.source.forum.project.name;
  }

  public get projectId() {
    return this.source.forum.project.id;
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public get watcherCount() {
    return this.source.watchers.length - 4;
  }
}