import {Component, OnInit, ViewChild} from '@angular/core';
import { StateService } from '@uirouter/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TopicService } from '../services/topic.service';
import { ProjectsService } from '../../projects/services/projects.service';
import { LoaderService } from '../../common/components';

@Component({
  templateUrl: './project-topic-list.component.html',
  selector: 'project-topic-list'
})
export class ProjectTopicListComponent implements OnInit {

  @ViewChild('createNewTopic') createNewTopicModal: any;
  @ViewChild('projectSelectionModal') projectSelectionModal: any;
  

  private _topics: any[] = [];
  private _searchString: string = "";
  private _projectSearchString: string = "";
  private _projects: any[] = [];
  public newTopic: any = {
    projectId: ""
  }

  constructor(readonly $state: StateService,
    private modalService: NgbModal,
    private loaderService: LoaderService,
    private projectService: ProjectsService,
    private topicService: TopicService) { }

  async ngOnInit() {
    await this.loadTopics();
  }

  public onSearch(event: any) {
    this._searchString = event.target.value.toLowerCase();
  }

  public onSearchProject(event: any) {
    this._projectSearchString = event.target.value.toLowerCase();
  }

  public onNewTopic() {
    if(this.projectId) {
      this.newTopic.projectId = this.projectId;
      this.openNewTopicModal();
    }
    else {
      this.openProjectSelectModal();
    }
  }

  private async loadTopics() {
    this.loaderService.loading(true);
    if(this.projectId) {
      const response = await this.topicService.getByProject(this.projectId);
      this._topics = response;
    }
    else {
      const response = await this.topicService.getAll();
      this._topics = response;
      const projects = await this.projectService.getProjectsAsync(true);
      this._projects = projects;
    }
    this.loaderService.loading(false);
  }

  private async openNewTopicModal() {
    this.modalService.open(this.createNewTopicModal, { size: 'lg', backdrop: 'static' }).result.then((result: string) => {
      if (result != 'cancel') {
        this.loadTopics();
      }
    });
  }

  private openProjectSelectModal() {
    this.modalService.open(this.projectSelectionModal, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.newTopic.projectId = result;
      this.openNewTopicModal();
    });
  }

  public get topics() {
    return this._topics.filter((item: any) => {
      return item.subject.toLowerCase().includes(this._searchString);
    });
  }

  public get projects() {
    return this._projects.filter((item: any) => {
      return item.name.toLowerCase().includes(this._projectSearchString);
    });
  }

  public get projectId() {
    return this.$state["params"].projectId;
  }
}