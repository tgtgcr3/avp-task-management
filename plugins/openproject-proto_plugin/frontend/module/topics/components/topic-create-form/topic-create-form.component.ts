import {Component, OnInit, ViewChild, Input} from '@angular/core';
import { StateService } from '@uirouter/core';
import { HalResourceService } from 'core-app/modules/hal/services/hal-resource.service';
import { CkeditorAugmentedTextareaComponent } from "core-app/ckeditor/ckeditor-augmented-textarea.component";
import { TopicService } from '../../services/topic.service';
import { ErrorDialogService } from '../../../common/components/error-dialog/error-dialog.service';
import { AvpNotificationService } from '../../../avp-base/notifications/avp-notifications.service';
import { NotificationType } from '../../../common/consts/avp';

@Component({
  templateUrl: './topic-create-form.component.html',
  selector: 'topic-create-form'
})
export class TopicCreateForm implements OnInit {

  @Input() projectId: string = "";
  @Input() modal: any;

  private ckeditor: CkeditorAugmentedTextareaComponent;

  public newTopic: any = {
    title: "",
    content: "",
    watchers: [],
    projectId: null,
    project_api: "",
    representer: null
  };

  constructor(readonly $state: StateService,
    private errorDialogService: ErrorDialogService,
    private halResourceService: HalResourceService,
    private avpNotificationService: AvpNotificationService,
    private topicService: TopicService) { }

  async ngOnInit() {
    this.newTopic.projectId = this.projectId;
    this.newTopic.project_api = `/api/v3/projects/${this.newTopic.projectId}`;
    const representer = await this.topicService.getTopicRepresenter();
    this.newTopic.representer = representer;
  }

  public onWatching(event: any) {
    this.newTopic.watchers = event;
  }

  public onAddNewTopic() {
    let payload: any = {
      subject: this.newTopic.title,
      content: this.ckeditor.getContent(),
      watchers: this.newTopic.watchers
    };

    if(!this.validateNewTopic(payload)) {
      return;
    }

    this.topicService.addNewTopic(this.newTopic.projectId, payload).subscribe(
      (response: any) => {
        let halResource = this.halResourceService.createHalResource(response, true);
        this.errorDialogService.showSuccess();
        this.sendNotify(halResource);
        this.modal.close('added');
      },
      (error: any) => {
        this.errorDialogService.show(error.message);
      }
    )
  }

  onEditorInitialized(editor: CkeditorAugmentedTextareaComponent) {
    this.ckeditor = editor
  }

  private validateNewTopic(payload: any) {
    if(!payload.subject) {
      this.errorDialogService.show('Please enter title');
      return false;
    }

    if(!payload.content) {
      this.errorDialogService.show('Please enter content of discussion');
      return false;
    }

    return true;
  }

  private sendNotify(response: any) {
    let notifyData: any = {
      id: response.id,
      receivers: this.recievers(response),
      author: response.author,
      project: response.project
    }
    this.avpNotificationService.notify(NotificationType.TOPIC_WATCHER_ADDED, notifyData);
  }

  private recievers(message: any) {
    let receiverEmails = message.watchers ? message.watchers.elements.map((user: any) => user.email) : [];

    let authorEmail = message.author.email;

    if (!receiverEmails.includes(authorEmail) && authorEmail) {
      receiverEmails.push(authorEmail);
    }

    return receiverEmails.filter((email: string) => {
      return email != message.author.email;
    });
  }
}