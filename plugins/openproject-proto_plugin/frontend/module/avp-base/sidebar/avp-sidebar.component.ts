import { Component, OnDestroy, OnInit } from '@angular/core';
import { DynamicBootstrapper } from "core-app/globals/dynamic-bootstrapper";
import { UserCacheService } from "core-components/user/user-cache.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { StateService } from "@uirouter/core";
import { AVP_VIEWS } from '../../common/consts/view-name';
import { ProjectsService } from "../../projects/services/projects.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { AvpUtils } from "../../common/services/avp-utils.service";
import { AVP } from '../../common/consts/avp';
import { Subscription } from 'rxjs';
import { AvpNotificationService } from '../notifications/avp-notifications.service';

@Component({
  templateUrl: './avp-sidebar.component.html',
  selector: 'avp-sidebar'
})
export class AvpSidebarComponent implements OnInit, OnDestroy {

  private static NOTIFICATION_INTERVAL = 20000;

  public user: any = {};
  public departments: any[] = [];
  public projects: any[] = [];
  public section: any = {};
  public searchModel: any;

  private allProjects: any[] = [];
  private isManagementOpening: boolean = false;
  private isNotificationOpening: boolean = false;
  private onRefreshSubscription: Subscription;
  private notificationData: any = undefined;
  private timeoutId: number;

  constructor(readonly userCacheService: UserCacheService,
    private currentUserService: CurrentUserService,
    private modalService: NgbModal,
    private projectService: ProjectsService,
    private avpUtils: AvpUtils,
    private notificationService: AvpNotificationService,
    private $state: StateService) { }

  ngOnInit() {
    this.loadUser();
    this.loadData();
    this.onRefreshSubscription = this.projectService.refreshEvent.subscribe(this.loadData.bind(this));
    this.section['group-menu-name-sum'] = true;
    this.section['group-menu-name-none'] = true;
    this.section['group-menu-name-option'] = true;
  }

  ngOnDestroy() {
    this.onRefreshSubscription && this.onRefreshSubscription.unsubscribe();
    clearTimeout(this.timeoutId);
  }

  public openOrColappse(suffix: string) {
    this.section[`group-menu-name-${suffix}`] = !this.section[`group-menu-name-${suffix}`];
  }

  public openModal(modal: any) {
    this.modalService.open(modal, { backdrop: 'static' });
  }

  public openAVPManagement(modal: any) {
    this.modalService.dismissAll();
    if (!this.isManagementOpening) {
      this.isManagementOpening = true;
      this.modalService.open(modal, { windowClass: 'base-apps', backdropClass: 'content-backdrop' })
        .result.then((result) => {
          this.isManagementOpening = false;
        }, (reason) => {
          this.isManagementOpening = false;
        });;
    }
  }

  public openAVPNotification(modal: any) {
    this.modalService.dismissAll();
    if (!this.isNotificationOpening) {
      this.isNotificationOpening = true;
      this.updateStatusAsRead();
      this.modalService.open(modal, { windowClass: 'base-apps', backdropClass: 'content-backdrop' })
        .result.then((result) => {
          this.isNotificationOpening = false;
        }, (reason) => {
          this.isNotificationOpening = false;
        });;
    }
  }

  public search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map((term: any) => {
        if (term === '') {
          return [];
        }

        return this.allProjects.filter((project: any) => project.name.toLowerCase().indexOf(term.toLowerCase()) > -1)
          .slice(0, 10);
      })
    );

  public formatter = (x: any) => x.name;

  public onProjectSelect(event: any) {
    this.$state.go(AVP_VIEWS.PROJECT, { projectId: event.item.id });
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public updateStatusAsRead() {
    if (this.notificationData) {
      this.notificationData.items.forEach((notify: any) => {
        if (notify.status != 1) {
          notify.status = 1;
          this.notificationService.updateStatusAsRead(notify.id, notify.targetEmail, notify.status)
            .subscribe((response: any) => { });
        }
      });
    }
  }

  public get unread() {
    return this.notificationData ? this.notificationData.items.filter((item: any) => item.status == 0).length : 0;
  }

  public get view() {
    return AVP_VIEWS;
  }

  public get currentView() {
    return this.$state.current.name;
  }

  public get projectId() {
    return this.$state['params'].projectId;
  }

  public get standaloneProjects() {
    return this.projects.filter((project: any) => {
      return project.parent_id == AVP.id;
    })
  }

  public get isAdmin() {
    return this.user.admin || this.user.owner;
  }

  public get isOwner() {
    return this.user.owner;
  }

  public get notifications() {
    return this.notificationData;
  }

  private loadData() {
    this.loadDepartments();
    this.loadStandaloneProjects();
  }

  private loadUser() {
    this.userCacheService
      .require(this.currentUserService.userId)
      .then((user: any) => {
        this.user = user;
        this.loadNotifications();
      });
  }

  private loadNotifications() {
    this.notificationService.getNotifications(this.user.email).subscribe(
      (response: any) => {
        this.notificationData = response;
        this.timeoutId = setTimeout(() => this.loadNotifications(), AvpSidebarComponent.NOTIFICATION_INTERVAL);
      },
      (error: any) => {
        this.timeoutId = setTimeout(() => this.loadNotifications(), AvpSidebarComponent.NOTIFICATION_INTERVAL);
      }
    );
  }

  private loadDepartments() {
    this.projectService.getDepartmentsWithChildren()
      .subscribe(
        (responses: any[]) => {
          responses.forEach((item: any) => {
            this.section[`group-menu-name-${item.id}`] = true;
            if (item.children == null) {
              item.children = [];
            }
            item.children.forEach((pro: any) => {
              pro.avt = pro.name.substring(0, 1);
            });

            this.allProjects = this.allProjects.concat(item.children);
          });
          this.departments = responses;
        },
        (error: any) => {
          console.error(error);
        }
      )
  }

  private loadStandaloneProjects() {
    this.projectService.getNoneDepartmentProjects()
      .subscribe(
        (responses: any[]) => {
          responses.forEach((pro: any) => {
            pro.avt = pro.name.substring(0, 1);
          });
          this.projects = responses;
          this.allProjects = this.allProjects.concat(this.projects);
        },
        (error: any) => {
          console.error(error);
        })
  }
}

DynamicBootstrapper.register({ selector: 'avp-sidebar', cls: AvpSidebarComponent });