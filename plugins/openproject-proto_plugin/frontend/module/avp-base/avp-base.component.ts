import { Component, OnInit } from '@angular/core';
import jwt_decode from "jwt-decode";
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  template: `
    <user-info></user-info>
    <div class="avp-base--ui-view">
      <ui-view></ui-view>
    </div>
`,
  selector: 'avp-base'
})
export class AvpBaseComponent implements OnInit {

  constructor(private logAdapter: LogAdapter) { }

  ngOnInit() {
    const myChat = document.getElementById('myChat');
    if (myChat != null) {
      const token = myChat.dataset.myChatId || '';

      const userInfo = jwt_decode(token);
      this.checkAuthTime(userInfo);
    }   
  }

  checkAuthTime(userInfo: any) {
    let lastAuthTime = localStorage.getItem("lastAuthTime");
    let currentAuthTime = userInfo.auth_time;
    if(lastAuthTime == null || lastAuthTime != currentAuthTime) {
      localStorage.setItem("lastAuthTime", currentAuthTime);
      this.logAdapter.logging(ActivityActionType.Login);
    }
  }
}