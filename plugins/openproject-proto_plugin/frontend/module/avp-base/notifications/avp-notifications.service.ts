import { Injectable } from '@angular/core';
import { NotificationType } from '../../common/consts/avp';
import { HttpHeaders } from "@angular/common/http";
import { BaseService } from '../../common/services/base.service';
import * as moment from 'moment';
import { AVP_VIEWS } from '../../common/consts/view-name';

@Injectable({
  providedIn: 'root'
})
export class AvpNotificationService extends BaseService {

  public notify(type: NotificationType, notifyData: any) {

    if (!notifyData.receivers || notifyData.receivers.length == 0) return;

    let payload: any = {
      ownerEmail: notifyData.author.email,
      targetEmail: "",
      title: "",
      description: {
        stateName: "",
        params: {},
        content: ''
      },
      datetime: moment().valueOf(),
      status: 0
    }

    if (notifyData.wp) {
      payload.description.stateName = AVP_VIEWS.TASKS_LIST;
      payload.description.params = { task: notifyData.wp.id };
    }


    switch (type) {
      case NotificationType.WP_START_DATE:
        this.notifyWPStartDate(notifyData, payload);
        break;
      case NotificationType.WP_DUE_DATE:
        this.notifyWPDueDate(notifyData, payload);
        break;
      case NotificationType.WP_ASSIGNED:
        this.notifyWPAssignee(notifyData, payload);
        break;
      case NotificationType.WP_CREATE:
        this.notifyWPCreate(notifyData, payload);
        break;
      case NotificationType.WP_DELETE:
        this.notifyWPDelete(notifyData, payload);
        break;
      case NotificationType.WP_DESCRIPTION:
        this.notifyWPDescription(notifyData, payload);
        break;
      case NotificationType.WP_ATTACHMENT:
        this.notifyWPAttachment(notifyData, payload);
        break;
      case NotificationType.WP_WATCHER:
        this.notifyWPWatcher(notifyData, payload);
        break;
      case NotificationType.WP_COMMENT_TAGGED:
        this.notifyWPMentioned(notifyData, payload);
        break;
      case NotificationType.WP_COMMENT:
        this.notifyWPComment(notifyData, payload);
        break;
      case NotificationType.WP_STATUS:
        this.notifyWPStatus(notifyData, payload);
        break;
      case NotificationType.WP_URGENT:
        this.notifyWPUrgent(notifyData, payload);
        break;
      case NotificationType.WP_RESULT:
        this.notifyWPResult(notifyData, payload);
        break;
      case NotificationType.CATEGORY_CREATE:
        this.notifyCategoryCreated(notifyData, payload);
        break;
      case NotificationType.ADD_USER_TO_PROJECT:
        this.notifyMemberAdded(notifyData, payload);
        break;
      case NotificationType.TOPIC_WATCHER_ADDED:
        this.notifyTopicWatcherAdded(notifyData, payload);
        break;
      case NotificationType.POST_ADDED:
        this.notifyPostAdded(notifyData, payload);
        break;
    }

    let receivers = notifyData.receivers;
    this.sendNotify(payload.ownerEmail, receivers, payload);
  }

  private notifyWPStartDate(notifyData: any, payload: any) {
    if (notifyData.wp.startDate) {
      payload.title = `<b>[${notifyData.wp.project.name}] ${notifyData.author.name}</b> chỉnh sửa thời điểm bắt đầu công việc <b>${notifyData.wp.subject}</b> thành ${notifyData.wp.startDate}`;
    }
    else {
      payload.title = `<b>[${notifyData.wp.project.name}] ${notifyData.author.name}</b> đã xóa thời điểm bắt đầu của công việc <b>${notifyData.wp.subject}</b>`;
    }
  }

  private notifyWPDueDate(notifyData: any, payload: any) {
    if (notifyData.wp.dueDate) {
      payload.title = `<b>[${notifyData.wp.project.name}] ${notifyData.author.name}</b> chỉnh sửa thời điểm deadline công việc <b>${notifyData.wp.subject}</b> thành ${notifyData.wp.dueDate}`;
    }
    else {
      payload.title = `<b>[${notifyData.wp.project.name}] ${notifyData.author.name}</b> đã xóa thời điểm deadline của công việc <b>${notifyData.wp.subject}</b>`;
    }
  }

  private notifyWPAssignee(notifyData: any, payload: any) {
    if (notifyData.wp.assignee) {
      payload.title = `<b>[${notifyData.wp.project.name}] ${notifyData.author.name}</b> giao công việc <b>${notifyData.wp.subject}</b> cho ${notifyData.wp.assignee.name}`;
    }
    else {
      payload.title = `<b>[${notifyData.wp.project.name}] ${notifyData.author.name}</b> đã xóa người thực hiện của công việc <b>${notifyData.wp.subject}</b>`;
    }
  }

  private notifyWPCreate(notifyData: any, payload: any) {
    payload.title = `<b>[${notifyData.wp.parent ? `${notifyData.wp.parent.name}` : ''}] ${notifyData.author.name}</b> tạo mới một công việc <b>${notifyData.wp.subject}</b> cho bạn`;
  }

  private notifyWPDelete(notifyData: any, payload: any) {
    payload.title = `<b>[${notifyData.wp.project.name}] ${notifyData.author.name}</b> xóa công việc <b>${notifyData.wp.subject}</b>`;
  }

  private notifyWPDescription(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> chỉnh sửa nội dung miêu tả của công việc <b>${notifyData.wp.subject}</b>`;
  }

  private notifyWPAttachment(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> thêm file(s) cho công việc <b>${notifyData.wp.subject}</b>`;
    let content = "";
    notifyData.attachments.forEach((name: any) => {
      content += `<b>[${name}]</b> <br>`
    })
    payload.description.content = content;
  }

  private notifyWPWatcher(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> đã thêm người theo dõi vào công việc ${notifyData.wp.subject}`;
    let content = "";
    notifyData.watchers.forEach((user: any) => {
      content += `<b>[${user}]</b> <br>`
    })
    payload.description.content = content;
  }

  private notifyWPMentioned(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> tag bạn vào một bình luận trong công việc <b>${notifyData.wp.subject}</b>`;
  }

  private notifyWPComment(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> đã bình luận trong công việc <b>${notifyData.wp.subject}</b>`;
  }

  private notifyWPStatus(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> đã thay đổi trạng thái của <b>${notifyData.wp.subject}</b> thành <b>${notifyData.wp.status.name}</b>`;
  }

  private notifyWPUrgent(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> gắn nhãn Khẩn cấp cho công việc <b>${notifyData.wp.subject}</b>`;
  }

  private notifyWPResult(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> cập nhật nội dung kết quả công việc <b>${notifyData.wp.subject}</b>`;
  }

  private notifyCategoryCreated(notifyData: any, payload: any) {
    payload.title = `<b>[${notifyData.project.name}] ${notifyData.author.name}</b> tạo nhóm công việc <b>${notifyData.category.name}</b>`;
    payload.description.stateName = AVP_VIEWS.PROJECT;
    payload.description.params = {
      projectId: notifyData.project.id
    };
  }

  private notifyMemberAdded(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> đã thêm bạn vào dự án <b>${notifyData.project.name}</b>`;
    payload.description.stateName = AVP_VIEWS.PROJECT;
    payload.description.params = {
      projectId: notifyData.project.id
    };
  }

  private notifyTopicWatcherAdded(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> đã gán bạn theo dõi một thảo luận tại dự án <b>${notifyData.project.name}</b>`;
    payload.description.stateName = AVP_VIEWS.PROJECT_TOPIC;
    payload.description.params = {
      projectId: notifyData.project.id,
      topic: notifyData.id
    };
  }

  private notifyPostAdded(notifyData: any, payload: any) {
    payload.title = `<b>${notifyData.author.name}</b> đã thêm một bình luận vào thảo luận <b>${notifyData.project.name}</b>`;
    payload.description.stateName = AVP_VIEWS.PROJECT_TOPIC;
    payload.description.params = {
      projectId: notifyData.project.id,
      topic: notifyData.id
    };
  }

  private sendNotify(authorMail: string, receivers: string[], payload: any) {
    let requestUrl = `/api/pri/notifications?email=${authorMail}`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');

    payload.description = JSON.stringify(payload.description);

    receivers.forEach((mail: string) => {
      payload.targetEmail = mail;
      this.httpClient.post(requestUrl, payload, { headers: headers }).subscribe(
        (response: any) => {

        },
        (error: any) => {
          console.log(error);
        }
      );
    })
  }

  getNotifications(mail: string) {
    let requestUrl = `/api/pri/notifications?email=${mail}`;
    return this.httpClient.get(requestUrl);
  }

  updateStatusAsRead(id: number, targetEmail: string, status: number) {
    let requestUrl = `/api/pri/notification/${id}?email=${targetEmail}&status=${status}`;
    return this.httpClient.post(requestUrl, {});
  }
}