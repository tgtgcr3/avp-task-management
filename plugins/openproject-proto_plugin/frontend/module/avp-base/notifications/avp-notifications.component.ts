import { Component, Input, OnInit } from '@angular/core';
import { AVP } from '../../common/consts/avp';
import { AvpUtils } from '../../common/services/avp-utils.service';
import * as moment from 'moment';
import { StateService, StateOrName } from '@uirouter/core';

@Component({
  templateUrl: './avp-notifications.component.html',
  selector: 'avp-notifications'
})
export class AvpNotificationsComponent implements OnInit {

  @Input() data: any;
  private numberOfShowing: number = 10;
  public notifications: any[] = [];

  constructor(private avpUtils: AvpUtils,
    readonly $state: StateService) { }

  ngOnInit() {
    this.refreshNotifications();
  }

  public goTo(notification: any) {
    let parsed = JSON.parse(notification.description);
    if (parsed.stateName) {
      this.$state.go(parsed.stateName as StateOrName, parsed.params);
    }
  }

  public loadMore() {
    this.numberOfShowing += 10;
    this.refreshNotifications();
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private refreshNotifications() {
    this.notifications = this.data ? this.data.items.slice(0, this.numberOfShowing).map((item: any) => {
      return {
        description: item.description,
        status: item.status,
        ownerEmail: item.ownerEmail,
        avatar: this.avpUtils.getAvatarUrlByEmail(item.ownerEmail),
        title: item.title,
        content: JSON.parse(item.description).content,
        datetime: moment(item.datetime).format('HH:mm DD/MM/YYYY')
      }
    }) : [];
  }
}