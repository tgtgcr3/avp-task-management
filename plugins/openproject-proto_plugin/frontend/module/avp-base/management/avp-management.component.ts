import { Component, Input, EventEmitter, Output } from '@angular/core'
import { AVP } from '../../common/consts/avp';
import { environment } from "environments/environment";
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './avp-management.component.html',
  selector: 'avp-management'
})
export class AvpManagementComponent {
  @Input() user: any;
  @Output() close: EventEmitter<any> = new EventEmitter<any>();

  constructor(private logAdapter: LogAdapter) { }

  public get name() {
    return this.user ? this.user.name : '';
  }

  public get email() {
    return this.user ? this.user.login : '';
  }

  public get accountUrl() {
    return environment.avpAccountUrl;
  }

  public get messageUrl() {
    return environment.avpMessageUrl;
  }

  public get baseUrl() {
    return environment.baseUrl;
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  onNavToAccount() {
    window.open(this.accountUrl, '_blank');
    this.closeDialog();
  }

  onNavToTask() {
    window.open('/avp', '_blank');
    this.closeDialog();
  }

  onNavToMessage() {
    window.open(this.messageUrl, '_blank');
    this.closeDialog();
  }

  onNavToRequest() {
    window.open(environment.avpRequestUrl, '_blank');
    this.closeDialog();
  }

  onLogout() {
    this.logAdapter.logging(ActivityActionType.Logout);
    window.location.href = '/logout';
  }

  private closeDialog() {
    this.close.emit();
  }
}