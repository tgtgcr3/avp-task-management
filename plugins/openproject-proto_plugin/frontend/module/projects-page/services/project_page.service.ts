import {Injectable, EventEmitter} from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import {Observable} from 'rxjs';
import {BaseService} from "../../common/services/base.service";
import * as moment from 'moment';
// import * as abc from 'moment-weekday-calc';

@Injectable({
  providedIn: 'root'
})
export class ProjectPageService extends BaseService{

  private dayDuration: number = 0;

  getAllInfoReportPage(projectId: number) : Observable<any>{
    let requestUrl = this.restUrl + `/api/v3/projects/${projectId}/work_packages`;
      let httpParams = this.getQueryParams({
          ['pageSize']: 100,
      });
    return this.httpClient.get(requestUrl,{ params: httpParams });
  }

  getByFilters(projectId: number, statusId: number) : Observable<any> {
    let requestUrl = this.restUrl + `/api/v3/projects/${projectId}/work_packages`;

    let httpParams = this.getQueryParams({ ['pageSize']: 100,
      ['offset']: 1,
      ['filters']: [{"status":{"operator":"=","values":[`${statusId}`]}}]});
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

}