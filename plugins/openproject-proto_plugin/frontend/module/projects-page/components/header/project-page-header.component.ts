import {Component, OnInit} from '@angular/core';
import {MembersService} from "../../../members/services/members.service";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProjectPageService} from "../../services/project_page.service"

@Component({
  templateUrl: './project-page-header.component.html',
  selector: 'project-page-header'
})
export class ProjectPageHeaderComponent implements OnInit {

  public user: any = {};
  public filters: any[] = [];

  constructor(
              private projectPageService: ProjectPageService,
) {

  }

  ngOnInit() {
  }



}