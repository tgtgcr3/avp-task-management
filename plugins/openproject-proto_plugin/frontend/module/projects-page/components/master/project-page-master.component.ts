import {Component, OnInit} from '@angular/core';
import {StateService, UIRouter} from '@uirouter/core';
import {AVP_VIEWS} from "../../../common/consts/view-name";

@Component({
  templateUrl: './project-page-master.component.html',
  selector: 'project-page-master'
})
export class ProjectPageComponent implements OnInit{

  public isTaskShowing = false;
  public showingTask: any;
  public noSidebar: boolean = false;

  constructor(
              private uiRouter: UIRouter,
              readonly $state:StateService) { }

  ngOnInit() {

  }

}