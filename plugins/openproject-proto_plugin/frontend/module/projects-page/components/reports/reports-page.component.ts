import { Component, OnInit } from '@angular/core';
import { StateService } from '@uirouter/core';
import * as moment from 'moment';
import { ProjectsService } from "../../../projects/services/projects.service";
import { LoaderService } from "../../../common/components";
@Component({
  templateUrl: './reports-page.component.html',
  selector: 'reports-page'
})
export class ReportsPageComponent implements OnInit {
  public reportInfo: any = {
    workPackages: [],
    members: [],
    departments: [],
    projects: []
  };
  public loaded: boolean = false;

  constructor(private projectService: ProjectsService,
    private loaderService: LoaderService,
    readonly $state: StateService) {
  }

  ngOnInit() {
    this.loadWorkPackages();
  }

  private loadWorkPackages() {
    let filter = this.buildFilter();
    this.loaderService.loading(true);
    this.projectService.getProjectReport(this.projectId, filter.status, filter.by, filter.from, filter.to)
      .subscribe(
        (responses: any[]) => {
          this.reportInfo.workPackages = responses;
          this.reportInfo.members = this.projectService.getUsersOf(this.reportInfo.workPackages);
          this.loaded = true;
          this.loaderService.loading(false);
        }
      )
  }

  private buildFilter() {
    let filter: any = {
      status: this.status
    };

    let from = this.$state["params"].from;
    let to = this.$state["params"].to;

    if (from && to) {
      let froms = from.split('-');
      let tos = to.split('-');
      let formattedFrom = moment(`${froms[2]}/${froms[1]}/${froms[0]}`).startOf('date');
      let formattedTo = moment(`${tos[2]}/${tos[1]}/${tos[0]}`).endOf('date');
      
      filter['by'] = this.by == null ? "created_at" : this.by == 'duedate' ? "due_date" : "start_date";
      filter['from'] = formattedFrom.toISOString();
      filter['to'] = formattedTo.toISOString();
    }

    return filter;
  }

  private get projectId() {
    return this.$state["params"].projectId;
  }

  private get by() {
    return this.$state["params"].by;
  }

  private get status() {
    return this.$state["params"].status;
  }
}