import { Component, Input, OnInit } from '@angular/core';
import { StateService, UIRouter } from '@uirouter/core';
import { AvpUtils } from "../../../../common/services/avp-utils.service";
import { UserCacheService } from "core-components/user/user-cache.service";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { AVP } from '../../../../common/consts/avp';

@Component({
  templateUrl: './report-by-member.component.html',
  selector: 'report-by-member'
})
export class ReportByMemberComponent implements OnInit {

  @Input() reportInfo: any;
  EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  EXCEL_EXTENSION = '.xlsx';
  REPORT_EXPORT_TITLE = "thanhvien.report"
  dataExportFile: any[] = [];

  public reportMember: any[] = [];
  public mostWork: any[] = [];
  public mostOverDue: any[] = [];


  constructor(readonly userCacheService: UserCacheService,
    private avpUtils: AvpUtils) { }

  ngOnInit() {
    this.loadReportMember();
  }

  public onLoadAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private loadReportMember() {
    this.reportInfo["members"].forEach((user: any) => {

      let doneTask = this.countStatusTasks(user.id, 'done');
      let doneLateTask = this.countStatusTasks(user.id, 'done late');
      let newTask = this.countStatusTasks(user.id, 'new');
      let inProgressTask = this.countStatusTasks(user.id, 'in progress');
      let overdueTask = this.countStatusTasks(user.id, 'overdue');
      let count = this.countByUsers(user.id);

      let member: any = {
        name: `${user.lastname} ${user.firstname}`,
        total: count,
        doneTask: doneTask + doneLateTask,
        doneLateTask: doneLateTask,
        newTask: newTask,
        inProgressTask: inProgressTask,
        overdueTask: overdueTask,
        mostWork: count - doneTask,
        mostOverDue: doneLateTask + overdueTask,
        avatar: user.avatar
      };

      this.reportMember.push(member);
    });

    this.mostWork = this.reportMember.sort(
      (a: any, b: any) => b.mostWork - a.mostWork
    );

    this.mostOverDue = this.reportMember.filter((user: any) => {
      return user.mostOverDue > 0
    }).sort(
      (a: any, b: any) => b.mostOverDue - a.mostOverDue
    );
  }

  private countStatusTasks(userId: string, status: string) {
    let count = 0;
    return this.reportInfo["workPackages"].filter((wp: any) => {
      if (status != 'overdue' && wp.assigned_to)
        return wp.assigned_to.id == userId &&
                wp.status.name.toLowerCase().includes(status);
          

      return this.checkOverdue(userId, wp);
    }).length;
  }

  private countByUsers(userId: string) {
    return this.reportInfo["workPackages"].filter((wp: any) => {
      return wp.assigned_to != null &&
              wp.assigned_to.id == userId;
    }).length;
  }

  private checkOverdue(userId: string, wp: any) {
    if (wp.due_date == null) {
      return false;
    }

    let dueDate = new Date(wp.due_date);
    if (dueDate.getTime() < new Date().getTime()) {
    }
    return dueDate.getTime() < new Date().getTime() &&
      wp.assigned_to != null && !wp.status.name.toLowerCase().includes('done')
      && wp.assigned_to.id == userId;
  }

  exportAsXLSX() {

    this.reportMember.forEach((value) => {
      let u: any = {
        "Thành viên": value.name,
        "Phải Làm": value.newTask,
        "Đang Làm": value.inProgressTask,
        "Hoàn Thành": value.doneTask,
        "Hoàn Thành Muộn": value.doneLateTask,
        "Quá Hạn": value.overdueTask,
      };

      this.dataExportFile.push(u);

    });

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.dataExportFile);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, this.REPORT_EXPORT_TITLE);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: this.EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + "." + moment().format('DD-MM-YYYY') + this.EXCEL_EXTENSION);
  }
}