import { Component, Input, OnInit } from '@angular/core';
import { StateOrName, StateService, UIRouter } from '@uirouter/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ProjectsService } from '../../../../projects/services/projects.service';

@Component({
  templateUrl: './report-filter.component.html',
  selector: 'report-filter'
})
export class ReportFilterComponent implements OnInit {

  @Input() comprehensiveReport: boolean = false;

  public hoveredDate: any = null;

  public fromDate: any;
  public toDate: any;

  public departments: any[] = [];

  constructor(private calendar: NgbCalendar,
    private projectService: ProjectsService,
    public formatter: NgbDateParserFormatter,
    readonly $state: StateService) { }

  ngOnInit() {
    this.initFromTo();
    if (this.comprehensiveReport == true) {
      this.loadDepartments();
    }
  }

  public onDateSelection(date: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }

    if (this.fromDate && this.toDate) {
      this.$state.go(this.$state.current.name as StateOrName, {
        "from": `${this.fromDate.day}-${this.fromDate.month}-${this.fromDate.year}`,
        "to": `${this.toDate.day}-${this.toDate.month}-${this.toDate.year}`
      });
    }
  }

  public removeFromTo() {
    this.$state.go(this.$state.current.name as StateOrName, {
      "from": null,
      "to": null
    });
  }

  public isHovered(date: any) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  public isInside(date: any) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  public isRange(date: any) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  public get dep() {
    if (!this.$state["params"].dep)
      return 'Tất cả phòng ban';

    let selectedDep = this.departments.find((dep: any) => {
      return dep.id == this.$state["params"].dep;
    });

    if(selectedDep) {
      return selectedDep.name;
    }
    return '';
  }

  public get by() {
    if (this.$state["params"].by === 'duedate')
      return 'Theo thời hạn';

    if (this.$state["params"].by === 'started')
      return 'Theo ngày bắt đầu';

    return 'Theo ngày tạo';
  }

  public get status() {
    if (this.$state["params"].status === 'inprogress')
      return 'Công việc đang thực hiện';

    if (this.$state["params"].status === 'done')
      return 'Công việc đã hoàn thành';

    return 'Tất cả trạng thái';
  }

  public get subtaskopt() {
    if (this.$state["params"].subtaskopt === 'excluded')
      return 'Không tính công việc con';

    return 'Công việc & công việc con';
  }

  public get filteredDate() {
    let from = this.$state["params"].from;
    let to = this.$state["params"].to;

    if (from && to) {
      return `${from} đến ${to}`;
    }

    return "Tất cả";
  }

  public get stateName() {
    return this.$state.current.name;
  }

  private initFromTo() {
    let from = this.$state["params"].from;
    let to = this.$state["params"].to;

    if (from && to) {
      let froms = from.split('-');
      let tos = to.split('-');

      this.fromDate = {
        "day": parseInt(froms[0]),
        "month": parseInt(froms[1]),
        "year": parseInt(froms[2])
      };

      this.toDate = {
        "day": parseInt(tos[0]),
        "month": parseInt(tos[1]),
        "year": parseInt(tos[2])
      };
    }
    else {
      if (this.comprehensiveReport) {
        let date = new Date();
        this.toDate = {
          "day": date.getDate(),
          "month": date.getMonth() + 1,
          "year": date.getFullYear()
        };

        date.setMonth(date.getMonth() - 1);
        this.fromDate = {
          "day": date.getDate(),
          "month": date.getMonth() + 1,
          "year": date.getFullYear()
        };
      }
    }
  }

  private loadDepartments() {
    this.projectService.getAllDepartments().subscribe(
      (responses: any) => {
        this.departments = responses;
      }
    )
  }
}