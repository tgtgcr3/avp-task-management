import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
  templateUrl: './report-by-day.component.html',
  selector: 'report-by-day'
})
export class ReportByDayComponent implements OnInit {

  @Input() reportInfo: any;

  public reportByDate: any = {
    labels: [],
    datasets: [
      { data: [], label: 'Tất cả công việc', type: 'line', stack: "line-y-axis", fill: false },
      { data: [], label: 'Tổng thực hiện', stack: 'bar-y-axis' },
      { data: [], label: 'Tổng hoàn thành', stack: 'bar-y-axis' },
      { data: [], label: 'Tổng quá hạn', stack: 'bar-y-axis' }
    ]
  };

  public reportByMonth: any = {
    labels: [],
    datasets: [
      { data: [], label: 'Công việc' },
      { data: [], label: 'Thực hiện' },
      { data: [], label: 'Hoàn thành' },
      { data: [], label: 'Quá hạn' }
    ]
  };

  public displayed: boolean = false;

  constructor() { }

  ngOnInit() {
    this.groupTasksByDate();
    this.groupTasksByMonth();
    this.displayed = true;
  }

  private groupTasksByDate() {
    let groupedTask = this.reportInfo["workPackages"].reduce((r: any, a: any) => {
      let date = moment(a.created_at).startOf('day').format();
      r[date] = [...r[date] || [], a];
      return r;
    }, {});

    let total = 0;
    let previousInProgress = 0;
    let previousDone = 0;
    let previousOverdue = 0;

    Object.keys(groupedTask)
      .sort((a: string, b: string) => {
        return a.localeCompare(b);
      })
      .forEach((key: string) => {
        previousInProgress += this.filterStatus(groupedTask[key], 'in progress');
        previousDone += this.filterStatus(groupedTask[key], 'done');
        previousOverdue += this.filterStatus(groupedTask[key], 'overdue');
        total += groupedTask[key].length;

        let label = moment(key).format('DD/MM');
        this.reportByDate.labels.push(label);
        this.reportByDate.datasets[0].data.push(total);
        this.reportByDate.datasets[1].data.push(previousInProgress); // In-Progress
        this.reportByDate.datasets[2].data.push(previousDone); // Done
        this.reportByDate.datasets[3].data.push(previousOverdue); // Overdue
      });
  }

  private groupTasksByMonth() {
    let groupedTask = this.reportInfo["workPackages"].reduce((r: any, a: any) => {
      let date = moment(a.created_at).format('MM/YY');
      r[date] = [...r[date] || [], a];
      return r;
    }, {});

    Object.keys(groupedTask)
      .sort()
      .forEach((key: string) => {
        let inProgress = this.filterStatus(groupedTask[key], 'in progress');
        let done = this.filterStatus(groupedTask[key], 'done');
        let overdue = this.filterStatus(groupedTask[key], 'overdue');

        this.reportByMonth.labels.push(key);
        this.reportByMonth.datasets[0].data.push(groupedTask[key].length);
        this.reportByMonth.datasets[1].data.push(inProgress); // In-Progress
        this.reportByMonth.datasets[2].data.push(done); // Done
        this.reportByMonth.datasets[3].data.push(overdue); // Overdue
      });
  }

  private filterStatus(list: any[], status: string) {
    return list.filter((wp: any) => {
      if (status == 'overdue')
        return this.checkOverdue(wp);
      else if (status == 'in progress' || status == 'new')
        return wp.status.name.toLowerCase().includes(status) && !this.checkOverdue(wp);

      return wp.status.name.toLowerCase().includes(status);
    }).length;
  }

  private checkOverdue(wp: any) {
    if (wp.due_date == null) {
      return false;
    }

    let dueDate = new Date(wp.due_date);
    return dueDate.getTime() < new Date().getTime() &&
      !wp.status.name.toLowerCase().includes('done');
  }
}