import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
  templateUrl: './report-total.component.html',
  selector: 'report-total'
})
export class ReportTotalComponent implements OnInit {

  @Input() comprehensiveReport: boolean = false;
  @Input() reportInfo: any;

  private _workingDays: number = 0;
  private _weekendDays: number = 0;

  constructor() { }

  ngOnInit() {

  }

  public get total() {
    return this.reportInfo["workPackages"].length;
  }

  public get done() {
    return this.filterStatus('done');
  }

  public get new() {
    return this.filterStatus('new');
  }

  public get inProgress() {
    return this.filterStatus('in progress');
  }

  public get workingDays() {
    if (this._workingDays == 0)
      this._workingDays = this.getDurations(true);
    return this._workingDays;
  }

  public get member() {
    return this.reportInfo["members"].length;
  }

  public get projects() {
    return this.reportInfo["projects"].length;
  }


  public get weekendDays() {
    if (this._weekendDays == 0)
      this._weekendDays = this.getWeekend();
    return this._weekendDays;
  }

  private filterStatus(status: string) {
    return this.reportInfo["workPackages"].filter((wp: any) => {
      return wp.status.name.toLowerCase().includes(status);
    }).length;
  }

  private getDurations(workingDays: boolean): number {
    let totalDay = 0;
    this.reportInfo["workPackages"].forEach((value: any) => {
      if (!(value["start_date"] == null) && !(value["due_date"] == null)) {
        let countDayDuration = this.countDayDuration(value["start_date"], value["due_date"], workingDays);
        totalDay += countDayDuration;
      }
    });

    return totalDay;
  }

  private countDayDuration(startDate: any, endDate: any, workingDays: boolean): number {
    if (workingDays) {
      let now = moment().format('YYYY-MM-DD');
      if (now > moment(endDate).format('YYYY-MM-DD')) {
        return moment(endDate).diff(moment(startDate), "days") + 1;
      } else {
        return moment(now).diff(moment(startDate), "days") + 1;
      }
    }
    else {
      return moment(endDate).diff(moment(startDate), "days") + 1;
    }
  }

  private getWeekend(): number {
    let totalSat = 0;
    let totalSun = 0;
    this.reportInfo["workPackages"].forEach((value: any) => {
      if (!(value["start_date"] == null) && !(value["dueDate"] == null)) {
        let startDay = value["start_date"];
        let enday = value["due_date"];
        let now = moment().format('YYYY-MM-DD');
        if (now > moment(enday).format('YYYY-MM-DD')) {
          totalSat += this.weekdaysBetween(startDay, enday, 6);
          totalSun += this.weekdaysBetween(startDay, enday, 7);
        } else {
          totalSat += this.weekdaysBetween(startDay, now, 6);
          totalSun += this.weekdaysBetween(startDay, now, 7)
        }
      }
    });

    return totalSat + totalSun;
  }

  private weekdaysBetween(d1: any, d2: any, isoWeekday: any) {
    d1 = moment(d1);
    d2 = moment(d2);
    let daysToAdd = ((7 + isoWeekday) - d1.isoWeekday()) % 7;
    let nextTuesday = d1.clone().add(daysToAdd, 'days');
    if (nextTuesday.isAfter(d2)) {
      return 0;
    }
    let weeksBetween = d2.diff(nextTuesday, 'weeks');
    return weeksBetween + 1;
  }
}