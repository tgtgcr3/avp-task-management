import { Component, OnInit, Input } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as moment from 'moment';

@Component({
  templateUrl: './report-by-department.component.html',
  selector: 'report-by-dep'
})
export class ReportByDepartmentComponent implements OnInit {

  @Input() reportInfo: any;
  EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  EXCEL_EXTENSION = '.xlsx';
  REPORT_EXPORT_TITLE = "phongban.report"
  dataExportFile: any[] = [];

  public reportProject: any[] = [];
  public reportByDep: any = {
    labels: [],
    datasets: [
      { data: [], label: 'Công việc' },
      { data: [], label: 'Đang thực hiện' },
      { data: [], label: 'Hoàn thành' },
      { data: [], label: 'Quá hạn' },
      { data: [], label: 'Đang đánh giá'}
    ]
  };

  ngOnInit() {
    this.loadReportProject();
  }

  private loadReportProject() {
    this.reportInfo["departments"].forEach((dep: any) => {

      let doneTask = this.countStatusTasks(dep, 'done');
      let inProgressTask = this.countStatusTasks(dep, 'progress');
      let overdueTask = this.countStatusTasks(dep, 'overdue');
      let inReviewTask = this.countStatusTasks(dep, 'review');
      let count = this.countByProject(dep);

      let p: any = {
        name: dep.name,
        total: count,
        doneTask: doneTask,
        inProgressTask: inProgressTask,
        overdueTask: overdueTask,
        inReview: inReviewTask,
        avatar: dep.name.substring(0, 1).toUpperCase()
      };

      this.reportProject.push(p);

      this.reportByDep.labels.push(p.name);
      this.reportByDep.datasets[0].data.push(count);
      this.reportByDep.datasets[1].data.push(inProgressTask); // In-Progress
      this.reportByDep.datasets[2].data.push(doneTask); // Done
      this.reportByDep.datasets[3].data.push(overdueTask); // Overdue
      this.reportByDep.datasets[4].data.push(inReviewTask); // Overdue
    });
  }

  private countStatusTasks(dep: any, status: string) {
    return dep["workPackages"].filter((wp: any) => {
      if (status != 'overdue')
        return wp.status.name.toLowerCase().includes(status);

      return this.checkOverdue(wp);
    }).length;
  }

  private countByProject(dep: any) {
    return dep["workPackages"].length;
  }

  private checkOverdue(wp: any) {
    if (wp.due_date == null) {
      return false;
    }

    let dueDate = new Date(wp.due_date);
    if (dueDate.getTime() < new Date().getTime()) {
    }
    return dueDate.getTime() < new Date().getTime() &&
      !wp.status.name.toLowerCase().includes('done');
  }

  exportAsXLSX() {

    this.reportProject.forEach((value) => {
      let u: any = {
        "Phòng ban": value.name,
        "Công việc": value.total,
        "Hoàn Thành": value.doneTask,
        "Quá Hạn": value.overdueTask,
        "Đang Làm": value.inProgressTask,
        "Đang Đánh Giá": value.inReview
      };

      this.dataExportFile.push(u);

    });

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.dataExportFile);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, this.REPORT_EXPORT_TITLE);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: this.EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + "." + moment().format('DD-MM-YYYY') + this.EXCEL_EXTENSION);
  }
}