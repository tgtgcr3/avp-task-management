import { Component, OnInit, Input } from '@angular/core';
import { UserCacheService } from "core-components/user/user-cache.service";
import { AVP } from '../../../../common/consts/avp';
import { AvpUtils } from "../../../../common/services/avp-utils.service";

@Component({
  templateUrl: './report-task.component.html',
  selector: 'report-task'
})
export class ReportTaskComponent implements OnInit {

  @Input() reportInfo: any;

  public reportByStatus: any = {
    labels: ['Hoàn thành đúng hạn', 'Hoàn thành muộn', 'Đang thực hiện', 'Phải thực hiện'],
    data: [0, 0, 0, 0]
  };

  public displayTaskReport: boolean = false;
  public notImportantAndUrgent: number = 0;
  public onlyImportant: number = 0;
  public onlyUrgent: number = 0;
  public bothImportantAndUrgent: number = 0;

  public new: number = 0;
  public inProgress: number = 0;
  public overdue: number = 0;
  public done: number = 0;
  public doneLate: number = 0;
  public starPerformers: any[] = [];
  public noDueDateTask: number = 0;

  constructor(readonly userCacheService: UserCacheService,
    private avpUtils: AvpUtils) { }

  ngOnInit() {
    this.initReports();
    this.loadStarPerformers();
  }

  public onLoadAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private initReports() {
    this.new = this.filterStatus('new');
    this.inProgress = this.filterStatus('in progress');
    this.overdue = this.filterStatus('overdue');
    this.done = this.filterStatus('done');
    this.doneLate = this.filterStatus('done late');

    this.reportByStatus = {
      labels: ['Hoàn thành đúng hạn', 'Hoàn thành muộn', 'Đang thực hiện', 'Phải thực hiện'],
      data: [
        this.done,
        this.doneLate,
        this.inProgress,
        this.new,
      ]
    };

    this.displayTaskReport = true;
    this.onlyImportant = this.reportInfo["workPackages"].filter((wp: any) => {
      return wp.isImportant == true && wp.isUrgent != true;
    }).length;
    
    this.onlyUrgent = this.reportInfo["workPackages"].filter((wp: any) => {
      return wp.isUrgent == true && wp.isImportant != true;
    }).length;

    this.bothImportantAndUrgent = this.reportInfo["workPackages"].filter((wp: any) => {
      return wp.isUrgent == true && wp.isImportant == true;
    }).length;

    this.notImportantAndUrgent = this.reportInfo["workPackages"].length - this.onlyImportant - this.onlyUrgent - this.bothImportantAndUrgent;

    this.noDueDateTask = this.reportInfo["workPackages"].filter((wp: any) => {
      return wp.due_date == null;
    }).length;
  }

  private filterStatus(status: string) {
    return this.reportInfo["workPackages"].filter((wp: any) => {
      if (status != 'overdue')
        return wp.status.name.toLowerCase() === status;

      return this.checkOverdue(wp);
    }).length;
  }

  private checkOverdue(wp: any) {
    if (wp.due_date == null) {
      return false;
    }

    let dueDate = new Date(wp.due_date);
    return dueDate.getTime() < new Date().getTime() &&
      !wp.status.name.toLowerCase().includes('done');
  }

  private loadStarPerformers() {
    this.reportInfo["members"].forEach((user: any) => {

      let doneTask = this.countDoneTasks(user.id);
      let count = this.countByUsers(user.id);
      let u: any = {
        name: `${user.lastname} ${user.firstname}`,
        count: count,
        done: doneTask,
        percentage: count == 0 ? 0 : (doneTask * 100 / count).toFixed(2),
        href: `api/v3/users/${user.id}`,
        avatar: user.avatar
      };

      this.starPerformers.push(u);
    });

    this.starPerformers = this.starPerformers.sort(
      (a: any, b: any) => b.percentage - a.percentage
    );
  }

  private countDoneTasks(userId: string) {
    return this.reportInfo["workPackages"].filter((wp: any) => {
      if(wp.assigned_to) {
        return wp.assigned_to.id == userId &&
                wp.status.name.toLowerCase().includes('done');
      }
      return false;
    }).length;
  }

  private countByUsers(userId: string) {
    return this.reportInfo["workPackages"].filter((wp: any) => {
      if(wp.assigned_to) {
        return wp.assigned_to.id == userId;
      }
      return false;
    }).length;
  }
}