import { Component, OnInit, Input } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as moment from 'moment';

@Component({
  templateUrl: './report-by-project.component.html',
  selector: 'report-by-project'
})
export class ReportByProjectComponent implements OnInit {

  @Input() reportInfo: any;
  EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  EXCEL_EXTENSION = '.xlsx';
  REPORT_EXPORT_TITLE = "duan.report"
  dataExportFile: any[] = [];

  public reportProject: any[] = [];

  public reportByStatus: any = {
    labels: ['Đúng tiến độ', 'Chậm tiến độ', 'Có rủi ro cao'],
    data: [0, 0, 0]
  };

  constructor() { }

  ngOnInit() {
    this.loadReportProject();
  }

  private loadReportProject() {
    this.reportInfo["projects"].forEach((project: any) => {

      let doneTask = this.countStatusTasks(project, 'done');
      let inProgressTask = this.countStatusTasks(project, 'progress');
      let overdueTask = this.countStatusTasks(project, 'overdue');
      let inReviewTask = this.countStatusTasks(project, 'review');
      let count = this.countByProject(project);

      let p: any = {
        name: project.name,
        description: project.description ? project.description : 'Chưa có mô tả',
        total: count,
        doneTask: doneTask,
        inProgressTask: inProgressTask,
        overdueTask: overdueTask,
        inReview: inReviewTask,
        avatar: project.name.substring(0, 1).toUpperCase()
      };

      this.reportProject.push(p);
    });

    this.reportByStatus = {
      labels: ['Đúng tiến độ', 'Chậm tiến độ', 'Có rủi ro cao'],
      data: [this.onTrack, this.offTrack, this.highRisk]
    };
  }

  private get onTrack() {
    return this.reportInfo["projects"].filter((p: any) => p.status == 'on-track').length;
  }

  private get offTrack() {
    return this.reportInfo["projects"].filter((p: any) => p.status == 'off-track').length;
  }

  private get highRisk() {
    return this.reportInfo["projects"].filter((p: any) => p.status == 'high-risk').length;
  }

  private countStatusTasks(project: any, status: string) {
    return this.reportInfo["workPackages"].filter((wp: any) => {
      if (status != 'overdue')
        return wp.project.id == project.id &&
          wp.status.name.toLowerCase().includes(status);

      return this.checkOverdue(project, wp);
    }).length;
  }

  private countByProject(project: any) {
    return this.reportInfo["workPackages"].filter((wp: any) => {
      return wp.project.id == project.id;
    }).length;
  }

  private checkOverdue(project: any, wp: any) {
    if (wp.due_date == null) {
      if (wp != null)
        return false;
    }
    let dueDate = new Date(wp.due_date);
    if (dueDate.getTime() < new Date().getTime()) {
    }
    return dueDate.getTime() < new Date().getTime() &&
      !wp.status.name.toLowerCase().includes('done')
      && wp.project.id == project.id;
  }

  exportAsXLSX() {

    this.reportProject.forEach((value) => {
      let u: any = {
        "Dự án": value.name,
        "Công việc": value.total,
        "Hoàn Thành": value.doneTask,
        "Quá Hạn": value.overdueTask,
        "Đang Làm": value.inProgressTask,
        "Đang Đánh Giá": value.inReview
      };

      this.dataExportFile.push(u);

    });

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.dataExportFile);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, this.REPORT_EXPORT_TITLE);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: this.EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + "." + moment().format('DD-MM-YYYY') + this.EXCEL_EXTENSION);
  }
}