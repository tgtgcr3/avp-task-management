import {Component, OnInit, Input} from '@angular/core';
import { ChartOptions,ChartType,ChartDataSets } from 'chart.js';

@Component({
  templateUrl: './bar-chart.component.html',
  selector: 'bar-chart'
})
export class BarChartComponent implements OnInit {
  @Input() stacked: boolean = false;
  @Input() dataInput: any;

  public barChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'bottom'
    }
  };
  public barChartLabels: any[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = "bottom";
  public barChartPlugins = [];
  public barChartColors: any[] = [
    { backgroundColor: '#48856c' },
    { backgroundColor: '#389dd9' },
    { backgroundColor: '#14cc3f' },
    { backgroundColor: '#f54e3b' },
  ];

  public barChartData: ChartDataSets[] = [];

  constructor() { }

  ngOnInit() {

    if(this.stacked == true) {
      this.barChartOptions.scales = {
        yAxes: [{
          stacked: true
        }],
        xAxes: [{
          stacked: true
        }]
      }
    }

    this.barChartLabels = this.dataInput.labels;
    this.barChartData = this.dataInput.datasets;
  }
}