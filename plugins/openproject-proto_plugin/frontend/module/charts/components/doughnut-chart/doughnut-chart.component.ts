import {Component, OnInit, Input, OnChanges} from '@angular/core';
import { ChartType } from 'chart.js';

@Component({
  templateUrl: './doughnut-chart.component.html',
  selector: 'doughnut-chart'
})
export class DoughnutChartComponent implements OnInit, OnChanges {
  @Input() dataInput: any;

  public display: boolean = false;
  doughnutChartLabels: any[] = [];
  doughnutChartData: any[] = [];
  options =  {
    legend: {
      position: 'bottom',
      align: 'start'
    }
  };
  doughnutChartType: ChartType = 'doughnut';

  constructor() { }

  ngOnInit() {
    this.doughnutChartLabels = this.dataInput.labels;
    this.doughnutChartData.length = 0;
    this.dataInput.data.forEach((value: number) => {
      this.doughnutChartData.push(value);
    });
    this.display = true;
  }

  ngOnChanges(changes: any): void {
    this.doughnutChartData.length = 0;
    changes.dataInput.currentValue.data.forEach((value: number) => {
      this.doughnutChartData.push(value);
    });
  }

  public get data() {
    this.doughnutChartData.length = 0;
    for (let i = this.dataInput.data.length - 1; i >= 0; i--) {
      this.doughnutChartData.push(this.dataInput.data[i]);
    }
    return this.doughnutChartData;
  }

}