import {Injectable, EventEmitter} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FilterSetupService {
  public onDisplay: EventEmitter<any> = new EventEmitter<any>();

  private successCallBackFunc: any;
  private dismissCallbackFunc: any;
  public open(input: any, fnSuccessCallback: any, fnErrorCallback: any) {
    this.dismissCallbackFunc = fnErrorCallback;
    this.successCallBackFunc = fnSuccessCallback;
    this.onDisplay.emit(input);
  }

  public onSuccess(params: any = undefined) {
    this.successCallBackFunc(params);
  }

  public onDismiss() {
    this.dismissCallbackFunc();
  }
}