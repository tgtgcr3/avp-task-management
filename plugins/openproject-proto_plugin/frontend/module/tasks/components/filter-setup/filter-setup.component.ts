import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FilterSetupService } from "./filter-setup.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as FilterConsts from "../../../common/consts/filters";
import { TasksService } from "../../services/tasks.service";
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { HalResource } from "core-app/modules/hal/resources/hal-resource";
import { ErrorDialogService } from "../../../common/components";
import { MembersService } from '../../../members/services/members.service';
import { AVP } from '../../../common/consts/avp';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './filter-setup.component.html',
  selector: 'filter-setup'
})
export class FilterSetupComponent implements OnInit, OnDestroy {
  public static defaultTitle = 'Custom Filters';
  @ViewChild('filterSetup') filterSetupModal: ElementRef;

  public isEditMode: boolean = false;
  public filters: any[] = FilterConsts.FILTERS;
  public operators: any = FilterConsts.FILTER_OPERATORS;
  public defaultValues = FilterConsts.FILTER_OPERATOR_VALUES;
  public queryTitle: string = FilterSetupComponent.defaultTitle;
  public filterList: any[];
  public users: any[] = [];
  public usersLoading = false;

  private halResource: HalResource;
  private subscription: Subscription;

  constructor(private filterSetupService: FilterSetupService,
    private tasksService: TasksService,
    private memberService: MembersService,
    private halResourceService: HalResourceService,
    private errorDialogService: ErrorDialogService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.subscription = this.filterSetupService.onDisplay.subscribe(this.open.bind(this));
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }

  public onPropertyChanged(item: any) {

    if (item == undefined) return;

    let filters = this.filterList.filter((value: any, index: number) => {
      return value.id == item.id;
    });

    if (filters.length != 0) {
      return;
    }

    let tobeAdded: any = {
      name: item.text,
      id: item.id,
      operator: this.operators[item.id][0].title,
      values: []
    };

    this.filterList.push(tobeAdded);
  }

  public onRemove(item: any) {
    let index = this.filterList.indexOf(item);
    if (index > -1) {
      this.filterList.splice(index, 1);
    }
  }

  public onSave() {
    let payload = {
      name: this.queryTitle,
      public: false,
      filters: this.getHalFilters()
    };

    if (this.isEditMode) {
      this.halResource.updateImmediately(payload)
        .then((response: any) => {
          this.errorDialogService.showSuccess();
          this.onClose();
        })
        .catch((error: any) => {
          this.errorDialogService.show('Xảy ra lỗi khi lưu');
        });
    }
    else {
      this.tasksService.createQuery(payload).subscribe(
        (response: any) => {
          this.onClose();
          this.errorDialogService.showSuccess();
          this.filterSetupService.onSuccess({ action: 'create', item: { queryId: response.id, name: response.name } });
        },
        (error: any) => {
          this.errorDialogService.show('Xảy ra lỗi khi lưu');
        }
      )
    }
  }

  public onDelete() {
    // delete this filter
    this.halResource.delete()
      .then((res: any) => {
        this.onClose();
        this.filterSetupService.onSuccess({ action: 'delete', item: { id: this.halResource.id } });
      })
      .catch((error: any) => {
        this.errorDialogService.show('Xảy ra lỗi khi xóa');
      });
  }

  public onClose(params: any = {}) {
    this.modalService.dismissAll();
  }

  public inputType(type: string) {
    if (type == 'multiple' || type == 'single') {
      return 'list';
    }
    return type;
  }

  private async open(input: any) {
    await this.loadUsers();
    this.filterList = [];
    this.halResource = this.halResourceService.createHalResource({ _type: 'Query', filters: [] });
    this.queryTitle = FilterSetupComponent.defaultTitle;
    this.isEditMode = input.isEditMode;

    if (this.isEditMode) {
      let editId = input.id;
      this.tasksService.getDataByUrl(`/api/v3/queries/${editId}`).subscribe(
        this.onRefineQuery.bind(this),
        (error: any) => {
        }
      );
    }

    this.modalService.open(this.filterSetupModal, { size: 'lg' }).result.then(
      (result) => {
        this.filterSetupService.onSuccess();
      }, (reason) => {
        this.filterSetupService.onDismiss();
      });
  }

  private onRefineQuery(resource: any) {
    this.halResource = this.halResourceService.createHalResource(resource);
    this.queryTitle = resource.name;
    resource.filters.forEach((value: any, index: number) => {
      let filter = this.getFilterValue(value);
      if (filter != undefined) this.filterList.push(filter);
    });
  }

  private getFilterValue(value: any) {
    let strArr = value.source._links.filter.href.split('/');
    let id = strArr[strArr.length - 1];

    if (this.operators[id] != undefined) {
      return {
        name: this.getFilterText(id),
        id: id,
        operator: this.operators[id][0].title,
        values: this.getValuesFromResource(value.values)
      };
    }
    console.error("Filter item is invalid");
    return undefined;
  }

  private getFilterText(id: string) {
    let target = FilterConsts.FILTERS.filter((value: any, index: number) => {
      return value.id == id;
    });

    if (target.length > 0) {
      return target[0].text;
    }
    return "";
  }

  private getHalFilters() {
    let filters: any[] = [];
    this.filterList.forEach((filter: any) => {
      let refinedFilter: any = {
        _type: `${filter.id}QueryFilter`,
        name: filter.id,
        _links: {
          schema: {
            href: FilterConsts.HREFS.SCHEMA + `/${filter.id}`
          },
          filter: {
            href: FilterConsts.HREFS.FILTER + `/${filter.id}`
          },
          operator: {
            href: FilterConsts.OPERATORS[filter.operator].href
          }
        }
      };

      let values = Array.isArray(filter.values) == true ? filter.values : [filter.values];

      if (FilterConsts.FILTER_OPERATOR_VALUES[filter.id][filter.operator].isNonPrimitive) {
        let valueTemplate: any[] = [];

        if (filter.id == 'custom') {
          if (values[0] == 'author') {
            refinedFilter.name = 'author';
            refinedFilter._links.schema.href = FilterConsts.HREFS.SCHEMA + `/author`;
            refinedFilter._links.filter.href = FilterConsts.HREFS.FILTER + `/author`;

            valueTemplate.push({ href: '/api/v3/users/me' });
          } else if (values[0] == 'assignee') {
            refinedFilter.name = 'assignee';
            refinedFilter._links.schema.href = FilterConsts.HREFS.SCHEMA + `/assignee`;
            refinedFilter._links.filter.href = FilterConsts.HREFS.FILTER + `/assignee`;

            valueTemplate.push({ href: '/api/v3/users/me' });
          }
        } else if (filter.id == 'author' || filter.id == 'assignee') {
          values.forEach((value: any) => {
            valueTemplate.push({ href: `/api/v3/users/${value}` })
          });
        } else if (filter.id == 'status') {
          values.forEach((value: any) => {
            valueTemplate.push({ href: `/api/v3/statuses/${value}` })
          });
        }

        refinedFilter._links["values"] = valueTemplate;
      } else {
        refinedFilter["values"] = values;
      }

      filters.push(refinedFilter);
    });

    return filters;
  }

  private getValuesFromResource(resources: any[]) {
    let values: any[] = [];
    resources.forEach((value: any) => {
      if (value.source != undefined) {
        let strArr = value.source._links.self.href.split('/');

        values.push(strArr[strArr.length - 1]);
      }
      else {
        values.push(value);
      }
    });
    return values;
  }

  private async loadUsers() {
    if(this.users.length != 0) {
      return;
    }

    this.usersLoading = true;
    let response = await this.memberService.byProject(AVP.id);

    this.users = response._embedded.elements.map((value: any) => {
      return { id: `${value.id}`, name: value.name };
    });
    this.usersLoading = false;
  }
}