import { Component, OnInit } from '@angular/core';
import { StateService } from '@uirouter/core';
import { UserCacheService } from "core-components/user/user-cache.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TasksService } from "../../../services/tasks.service";
import { ErrorDialogService, LoaderService } from "../../../../common/components";
import { ProjectsService } from '../../../../projects/services/projects.service';
import { AVP_VIEWS } from '../../../../common/consts/view-name';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  selector: 'search-control',
  templateUrl: './search-control.component.html',
})
export class SearchControlComponent implements OnInit {

  public title: string = "Tìm nhanh công việc";
  private currentFilters: any[] = [];
  private currentUser: any;

  private projectRoles: any[] = [];

  constructor(
    readonly $state: StateService,
    readonly userCacheService: UserCacheService,
    private taskService: TasksService,
    private userService: CurrentUserService,
    private modalService: NgbModal,
    private projectService: ProjectsService,
    private errorDialogService: ErrorDialogService,
    private logAdapter: LogAdapter,
    private loaderService: LoaderService) {
  }

  async ngOnInit() {
    if(this.projectId) {
      this.userCacheService
      .require(this.userService.userId)
      .then((user: any) => {
        this.currentUser = user;
      });

      this.projectRoles = await this.projectService.getRoles(this.projectId);
    }
  }

  onEnter(event: any) {
    if (this.currentFilters) {
      this.currentFilters = [];
    }
    this.currentFilters.push(
      {
        "subjectOrId": {
          "operator": "**",
          "values": event.target.value,
        }
      }
    );
    this.onRefreshTasks(this.currentFilters);
  }

  public open(modal: any) {
    this.modalService.open(modal).result.then((result: any) => {
      if(result == "Delete") {
        this.projectService.delete(this.projectId).subscribe((response: any) => {
          this.logAdapter.logging(ActivityActionType.DeleteProject, this.projectId, response.name);
          this.projectService.onRefresh();
          this.$state.go(AVP_VIEWS.PROJECTS);
        }, this.errorHandle.bind(this));
      }
    }, (reason: any) => {
    });
  }

  public get projectId() {
    return this.$state["params"].projectId;
  }

  public get showProjectOption() {
    return this.currentUser && 
    (this.projectRoles.includes('Project admin') || this.currentUser.owner == true) &&
    this.projectId;
  }

  public get hideSearchField() {
    return this.$state.current.name == AVP_VIEWS.PROJECT_REPORTS
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_INFO
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_MEMBERS
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_PERMISSIONS
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_MAIL_NOTIFY
    || this.$state.current.name == AVP_VIEWS.PROJECT_SETTINGS_REPEATED
    || this.$state.current.name == AVP_VIEWS.PROJECT_TOPICS
    || this.$state.current.name == AVP_VIEWS.PROJECT_TOPIC;
  }

  private onRefreshTasks(nameWork: any) {
    this.taskService.onSearch(this.currentFilters);
  }

  private errorHandle(error: any) {
    this.errorDialogService.show(error.message);
    this.loaderService.loading(false);
  }

}
