import { AvpUtils } from './../../../common/services/avp-utils.service';
import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { MainMenuToggleService } from "core-components/main-menu/main-menu-toggle.service";
import { AVP } from '../../../common/consts/avp';

@Component({
  templateUrl: './task-header.component.html',
  selector: 'task-header'
})
export class TaskHeaderComponent implements OnInit {

  @Input() user: any = {};
  @Input() filters: any[] = [];

  @Output() filterEvent: EventEmitter<any> = new EventEmitter();

  constructor(public readonly toggleService: MainMenuToggleService, private avpUtils: AvpUtils) {
  }

  ngOnInit() {
  }

  public openFilter() {
    this.filterEvent.emit({ isEditMode: false });
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }
}