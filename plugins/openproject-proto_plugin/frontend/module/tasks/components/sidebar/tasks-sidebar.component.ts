import { StateService } from "@uirouter/core";
import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core'
import { TasksService } from "../../services/tasks.service";
import { AVP } from '../../../common/consts/avp';
import { AVP_VIEWS } from "../../../common/consts/view-name";

@Component({
  templateUrl: './tasks-sidebar.component.html',
  selector: 'tasks-sidebar'
})
export class TasksSidebarComponent implements OnInit {

  @Input() user: any = {};
  @Input() filters: any[] = [];

  @Output() filterEvent: EventEmitter<any> = new EventEmitter<any>();

  private summary: any = {};
  public myEmployees: any[] = [];
  public loaded: boolean = false;

  constructor(private taskService: TasksService,
    readonly $state: StateService) { }

  async ngOnInit() {
    this.loaded = false;
    await this.loadSummary();
    await this.loadMyEmployees();
    this.loaded = true;
  }

  public onEditFilter(filter: any) {
    this.filterEvent.emit({ isEditMode: true, id: filter.queryId });
  }

  public onAddFilter() {
    this.filterEvent.emit({ isEditMode: false });
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public get totalTask() {
    return this.summary.total;
  }

  public get doneCount() {
    return this.summary.done;
  }

  public get percentage() {
    return this.totalTask != 0 ? (this.doneCount * 100 / this.totalTask).toFixed(2) : 0;
  }

  public get byEmployee() {
    return this.$state.current.name == AVP_VIEWS.TASKS_TEAM;
  }

  private async loadSummary() {
    const response = await this.taskService.getTaskSummary();
    this.summary = response;
  }

  private async loadMyEmployees() {
    const responses = await this.taskService.getMyEmployees();
    this.myEmployees = responses;
  }
}