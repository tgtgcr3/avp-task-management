import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ProjectsService } from "../../../projects/services/projects.service";
import { StateService } from '@uirouter/core';
import { AVP_VIEWS } from "../../../common/consts/view-name";
import { TasksService } from "../../services/tasks.service";
import { UserCacheService } from "core-components/user/user-cache.service";
import { ProjectCacheService } from 'core-components/projects/project-cache.service';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { AvpNotificationService } from '../../../avp-base/notifications/avp-notifications.service';
import { NotificationType } from '../../../common/consts/avp';
import { MembersService } from '../../../members/services/members.service';
import { PermissionService } from '../../../common/services/permission.service';
import { CategoryService } from '../../../projects/services/category.service';

@Component({
  templateUrl: 'action-bar.component.html',
  selector: 'action-bar'
})
export class ActionBarComponent implements OnInit {

  @Input() filterByAssignment: boolean = false;
  @Input() filterByProject: boolean = false;
  @Input() ordering: boolean = false;
  @Input() queryName: string;
  @Input() projectId: number;
  @Input() projectStatus: any = 0;
  @Output() initialized: EventEmitter<any> = new EventEmitter();

  @ViewChild('createTaskModal') createTaskModal: any;

  public project: any = {
    all: [],
    searching: [],
    selectedId: 0
  };
  public views = AVP_VIEWS;
  public tabName: string = AVP_VIEWS.TASKS;
  public filterParams: any = {};

  public category: any = {
    name: ''
  };

  private currentUser: any;
  private permission: any;

  constructor(private modalService: NgbModal,
    private notificationService: AvpNotificationService,
    private projectService: ProjectsService,
    private categoryService: CategoryService,
    private memberService: MembersService,
    private taskService: TasksService,
    private userCacheService: UserCacheService,
    private projectCacheService: ProjectCacheService,
    private permissionService: PermissionService,
    private currentUserService: CurrentUserService,
    readonly $state: StateService) { }


  async ngOnInit() {
    this.tabName = this.$state.current.name as string;

    if (this.tabName === this.views.TASKS_FILTERS) {
      this.filterParams = this.$state.params;
    }
    if (this.projectId) {
      this.loadUser();
      this.permission = await this.permissionService.getCurrentUserPermission(`${this.projectId}`);
    }
  }

  public get editable() {
    return !this.projectId || (this.projectId && this.projectStatus == 1);
  }

  public get canCreateWP() {
    if (!this.projectId) return true;
    return this.permission && this.permission.projectPermission.canCreateWP;
  }

  public get canCreateCategory() {
    return this.permission && this.permission.projectPermission.canCreateCategory;
  }

  public onFilterInitialized(initParams: any) {
    this.initialized.emit(initParams);
  }

  onCreateTask(modal: any) {
    if (this.projectId != undefined) {
      this.project.selectedId = this.projectId;
      this.onOpenCreateTaskModal();
      return;
    }

    this.projectService.getByFilters({ "status": 1 })
      .subscribe((responses: any[]) => {
        let projects = responses.filter((item: any) => {
          return item.permission.projectPermission.canCreateWP;
        });

        this.project.all = projects;
        this.project.searching = projects.map((p: any) => {
          return { id: p.id, name: p.name };
        });
        this.onOpenProjectSelectionModal(modal);
      });
  }

  onOpenProjectSelectionModal(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.project.selectedId = result;
      this.onOpenCreateTaskModal();
    }, (reason) => {
    });
  }

  onOpenCreateTaskModal() {
    this.modalService.open(this.createTaskModal, { size: 'lg', backdrop: 'static' });
  }

  onSearchProject(event: any) {
    let searchString = event.target.value.toLowerCase();
    let searchingResult: any[] = this.project.all.filter((p: any) => p.name.toLowerCase().includes(searchString));
    this.project.searching = searchingResult.map((p: any) => {
      return { id: p.id, name: p.name };
    });
  }

  open(modal: any) {
    this.modalService.open(modal, { backdrop: 'static' });
  }

  public onCreateCategory(modal: any) {
    let payload = {
      projectId: this.projectId,
      name: this.category.name
    };
    this.categoryService.createCategory(payload)
      .subscribe(
        (response: any) => {
          this.onRefreshTasks(modal);
          this.onCategoryCreatedNotify(response)
        },
        (error: any) => {
          console.error(error);
          modal.close();
        }
      );
  }

  onRefreshTasks(modal: any) {
    modal.close();
    this.taskService.onRefresh();
  }

  private onCategoryCreatedNotify(response: any) {
    let receivers = response.receivers.map((user: any) => user.mail)
      .filter((email: string) => email != this.currentUser.email);

    let notifyData: any = {
      author: this.currentUser,
      project: response.project,
      category: response.category,
      receivers: receivers
    };

    console.log(notifyData);

    this.notificationService.notify(NotificationType.CATEGORY_CREATE, notifyData);
  }


  private loadUser() {
    this.userCacheService
      .require(this.currentUserService.userId)
      .then((user: any) => {
        this.currentUser = user;
      });
  }
}
