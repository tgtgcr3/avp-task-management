import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TasksService } from "../../../services/tasks.service";
import * as AVP from "../../../../common/consts/avp";
import { AVP_VIEWS } from "../../../../common/consts/view-name";
import { ProjectsService } from "../../../../projects/services/projects.service";
import { ErrorDialogService } from "../../../../common/components";
import { StateService } from "@uirouter/core";
import * as moment from 'moment';

@Component({
  templateUrl: './filter-control.component.html',
  selector: 'filter-control'
})
export class FilterControlComponent implements OnInit {

  @Input() currentTab: string;
  @Input() projectId: number;
  @Output() initialized: EventEmitter<any> = new EventEmitter();

  public hoveredDate: any = null;
  public fromDate: any;
  public toDate: any;

  public types: any[] = AVP.FILTER_TYPE_PROPERTIES;

  public displays: any[] = AVP.DISPLAY_OPTIONS;

  public statuses: any[] = AVP.FILTER_STATUS_PROPERIES;

  public orders: any[] = AVP.ORDER_OPTIONS;

  public projects: any[] = [];

  public employees: any[] = [];

  public queries: any[] = [];

  public selected: any = {
    type: this.types[0],
    displaying: this.displays[0],
    status: this.statuses[1],
    employee: {},
    project: {},
    ordering: this.orders[0]
  };

  private currentFilters: any[] = [];
  private currentOrders: any[] = [];

  constructor(private taskService: TasksService,
    private projectService: ProjectsService,
    private errorDialogService: ErrorDialogService,
    readonly $state: StateService) { }

  async ngOnInit() {
    if(this.byType == true) {
      this.currentFilters.push({
        'assigneeOrAuthor': {
          'operator': '=',
          'values': ['me']
        }
      });
    }

    if (this.byProject == true) {
      this.loadProjects();
    }

    if (this.byEmployee == true) {
      await this.loadEmployees();
    }

    if (this.otherFilter == true) {
      this.loadFilters();
    }
    else {
      this.currentFilters.push({
        "status": {
          "operator": "=",
          "values": ["1", "7"]
        }
      });
    }

    this.initialized.emit({filters: this.currentFilters, orderBy: this.currentOrders});
  }

  public onChangeDisplaying(displaying: any) {
    this.selected.displaying = displaying;
    this.onDisplayChange();
  }

  public onChangeType(type: any) {
    this.selected.type = type;
    this.checkAndRemoveIfExisted("assigneeOrAuthor");
    this.checkAndRemoveIfExisted("assignee");
    this.checkAndRemoveIfExisted("author");
    let filter: any = {};
    filter[type.value] = {
      "operator": "=",
      "values": ["me"]
    };

    this.currentFilters.push(filter);
    this.onRefreshTasks();
  }

  public onChangeStatus(status: any) {
    this.selected.status = status;
    this.checkAndRemoveIfExisted("status");
    this.checkAndRemoveIfExisted("dueDate");
    this.checkAndRemoveIfExisted("customField13");
    this.checkAndRemoveIfExisted("customField14");


    switch (status.value) {
      case "not-done":
        this.currentFilters.push({
          "status": {
            "operator": "=",
            "values": ["1", "7"]
          }
        });
        break;
      case "done":
        this.currentFilters.push({
          "status": {
            "operator": "=",
            "values": ["16", "17"]
          }
        });
        break;
      case "in-review":
        this.currentFilters.push({
          "status": {
            "operator": "=",
            "values": ["10"]
          }
        });
        break;
      case "done-late":
        this.currentFilters.push({
          "status": {
            "operator": "=",
            "values": ["17"]
          }
        });
        break;
      case "overdue":
        this.currentFilters.push({
          "status": {
            "operator": "!",
            "values": ["16", "17"]
          }
        });
        this.currentFilters.push({
          "dueDate": {
            "operator": "<t-",
            "values": ["1"]
          }
        });
        break;
      case "urgent":
        this.currentFilters.push({
          "customField14": {
            "operator": "=",
            "values": ["t"]
          }
        });
        break;
      case "important":
        this.currentFilters.push({
          "customField13": {
            "operator": "=",
            "values": ["t"]
          }
        });
        break;
      default:
        break;
    }

    this.onRefreshTasks();
  }

  public onChangeProject(project: any) {
    this.selected.project = project;
    this.checkAndRemoveIfExisted("project");
    if (project.value != "all") {
      this.currentFilters.push({
        "project": {
          "operator": "=",
          "values": [project.value]
        }
      });
    }

    this.onRefreshTasks();
  }

  public onChangeEmployee(employee: any) {
    this.selected.employee = employee;
    this.checkAndRemoveIfExisted("assignee");
    if (employee.value != "all") {
      this.currentFilters.push({
        "assignee": {
          "operator": "=",
          "values": [employee.value]
        }
      });
    }
    else {
      this.currentFilters.push({
        "assignee": {
          "operator": "=",
          "values": this.employees.slice(1, this.employees.length).map((user: any) => user.value)
        }
      });
    }

    this.onRefreshTasks();
  }

  public onChangeOrder(ordering: any) {
    this.selected.ordering = ordering;

    this.currentOrders = [
      [ordering.value, "desc"]
    ];

    this.onRefreshTasks();
  }

  public export() {
    this.projectService.onExport(this.currentFilters, this.currentOrders);
  }

  public removeFromTo() {
    this.fromDate = null;
    this.toDate = null;
    this.checkAndRemoveIfExisted('createdAt');
    this.onRefreshTasks();
  }

  public get byType() {
    return this.currentTab == AVP_VIEWS.TASKS ||
      this.currentTab == AVP_VIEWS.TASKS_LIST;
  }

  public get byStatus() {
    return this.currentTab != AVP_VIEWS.TASKS_FILTERS;
  }

  public get byProject() {
    return this.currentTab == AVP_VIEWS.TASKS ||
      this.currentTab == AVP_VIEWS.TASKS_LIST;
  }

  public get byDisplay() {
    return this.currentTab == AVP_VIEWS.TASKS ||
      this.currentTab == AVP_VIEWS.TASKS_LIST ||
      this.currentTab == AVP_VIEWS.PROJECT_TASKS;
  }

  public get ordering() {
    return this.currentTab != AVP_VIEWS.TASKS_FILTERS;
  }

  public get byEmployee() {
    return this.currentTab == AVP_VIEWS.TASKS_TEAM;
  }

  public get otherFilter() {
    return this.currentTab == AVP_VIEWS.TASKS_FILTERS;
  }

  public get isProjectPage() {
    return this.currentTab == AVP_VIEWS.PROJECT_TASKS;
  }

  public get filterUrl() {
    return AVP_VIEWS.TASKS_FILTERS;
  }

  private loadProjects() {
    this.projectService.getRelatedProjectsOnly()
      .subscribe(
        (responses: any[]) => {
          this.projects = [AVP.FILTER_PROJECT_DEFAULT];

          this.projects = this.projects.concat(responses.map((project: any) => {
            return { value: project.id, label: project.name };
          }));

          this.selected.project = this.projects[0];
        },
        (error: any) => {
          this.errorDialogService.show("Lỗi khi tải tài nguyên");
        }
      )
  }

  private async loadEmployees() {
    const employees = await this.taskService.getMyEmployees();
    this.employees = [AVP.FILTER_EMPLOYEE_DEFAULT];
    this.employees = this.employees.concat(employees.map((employee: any) => {
      return { value: employee.id, label: employee.name };
    }));

    this.selected.employee = this.employees[0];
    this.currentFilters.push({
      "assignee": {
        "operator": "=",
        "values": this.employees.slice(1, this.employees.length).map((user: any) => user.value)
      }
    });
  }

  private loadFilters() {
    this.taskService.getDataByUrl('/api/v3/queries').subscribe(
      (response: any) => {
        this.queries = response._embedded.elements.map((query: any) => {
          return { value: query.id, label: query.name };
        })
      },
      (error: any) => {
        this.errorDialogService.show(`Load Filter: ${error.message}`);
      }
    )
  }

  private onRefreshTasks() {
    this.taskService.onRefreshWithParams(this.currentFilters, this.currentOrders);
  }

  private onDisplayChange() {
    this.taskService.onChangeView(this.selected.displaying.value);
  }

  private checkAndRemoveIfExisted(key: string) {
    let tobeRemovedFilters = this.currentFilters.filter((filter: any) => {
      return filter[key] != undefined;
    });

    tobeRemovedFilters.forEach((filter: any) => {
      let index = this.currentFilters.indexOf(filter);
      this.currentFilters.splice(index, 1);
    })
  }

  public isHovered(date: any) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  public isInside(date: any) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  public isRange(date: any) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  public onDateSelection(date: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }

    if (this.fromDate && this.toDate) {
      this.currentFilters.push(this.buildDateFilter(this.fromDate, this.toDate));
      this.onRefreshTasks();
    }
  }

  private buildDateFilter(from: any, to: any) {
    this.checkAndRemoveIfExisted('createdAt');
    let formattedFrom = moment(`${from.year}/${from.month}/${from.day}`).startOf('date');
    let formattedTo = moment(`${to.year}/${to.month}/${to.day}`).endOf('date');
    let filter: any = {};
    filter['createdAt'] = {
      "operator": "<>d",
      "values": [formattedFrom.toISOString(), formattedTo.toISOString()]
    };

    return filter;
  }

}
