import { Component, OnInit, Input, OnDestroy, ViewChild, EventEmitter, Output } from '@angular/core';
import { NgbModal, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { WorkPackageResource } from 'core-app/modules/hal/resources/work-package-resource';
import { componentDestroyed } from 'ng2-rx-componentdestroyed';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { StateService } from '@uirouter/core';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { ErrorDialogService } from '../../../../common/components';
import { AVP } from '../../../../common/consts/avp';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';
import { PermissionService } from '../../../../common/services/permission.service';

@Component({
  templateUrl: './repeated-task-item.component.html',
  selector: 'repeated-task-item'
})
export class RepeatedTaskItemComponent implements OnInit, OnDestroy {

  @Input() wp: any;
  @Input() schema: any;
  @Input() projects: any[];
  @Output() onDeleted: EventEmitter<any> = new EventEmitter();

  @ViewChild('userSelector') userSelector: NgbPopover;

  public activations: any[] = [
    { value: true, label: 'Kích hoạt' },
    { value: false, label: 'Vô hiệu hóa' }
  ];

  public isActivate: boolean = true;
  
  public wpProjectId: number; 

  public isLoading: boolean = true;
  public posibleProjects: any[] = [];
  public assignee: any = {};
  
  private _workPackage: any = {};
  private freqType: string;
  private freqValues: string;
  private _permission: any;

  constructor(private modalService: NgbModal,
    private wpCacheService: WorkPackageCacheService,
    private userService: CurrentUserService,
    private permissionService: PermissionService,
    private errorDialogService: ErrorDialogService,
    private logAdapter: LogAdapter,
    readonly $state: StateService) { }

  async ngOnInit() {
    this.isLoading = true;

    this.wpCacheService.loadWorkPackage(this.wp.id).values$()
      .pipe(
        takeUntil(componentDestroyed(this))
      )
      .subscribe(async (wp: WorkPackageResource) => {
        this._workPackage = wp;
        if(this._workPackage.assignee == null) {
          this.assignee = {
            name: "Chưa được giao",
            title: "Bấm để giao việc",
            avatar: null
          };
        }
        else {
          this.assignee = {
            name: this._workPackage.assignee.name,
            title: this._workPackage.assignee.login,
            avatar: this._workPackage.assignee.avatar
          }
        }
    
        this.wpProjectId = Number.parseInt(wp.project.id);
        this.isActivate = this._workPackage[`customField${this.schema['frequenceStatus'].id}`];
        this.freqType = this._workPackage[`customField${this.schema['frequenceType'].id}`];
        this.freqValues = this.freqType == 'monthly' ? this._workPackage[`customField${this.schema['frequenceValues'].id}`] 
                                                        : this.getValueString(this._workPackage[`customField${this.schema['frequenceValues'].id}`]);
        this._permission = await this.permissionService.getCurrentUserPermission(wp.project.id);
        
        this.posibleProjects = this.disableProjectSelection ? this.projects : this.projects.filter((item: any) => {
          return item.permission.projectPermission.canCreateWP;
        });
        this.isLoading = false;
      });
  }

  ngOnDestroy() { }

  public open(modal: any) {
    this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
  }

  public async openDelete(modal: any) {
    const allowed = await this.validateAction('DELETE_WP');
    if(!allowed) {
      return;
    }

    this.modalService.open(modal, { backdrop: 'static' }).result.then((result: any) => {
      if(result == 'deleted') {
        this._workPackage.delete()
          .then((res: any) => {
            this.onDeleted.emit();
          })
          .catch((error: any) => {
            this.errorDialogService.show('Xảy ra lỗi khi xóa');
          });
      }
    });
  }

  public assigneeChanged(assigneeId: number) {
    this.userSelector.close();

    let payload = {
      _links: {
        assignee: {
          href: assigneeId == 0 ? null : `/api/v3/users/${assigneeId}`
        }
      },
      lockVersion: this._workPackage.lockVersion
    };

    this.onSave(payload, null);
  }

  public projectChanged(project: any) {
    let payload = {
      _links: {
        project: {
          href: project.id == 0 ? null : `/api/v3/projects/${project.id}`
        },
        category: {
          href: null
        }
      },
      lockVersion: this._workPackage.lockVersion
    };

    this.onSave(payload, null);
  }

  public statusChanged(status: any) {
    let payload: any = {
      lockVersion: this._workPackage.lockVersion
    };
    payload[`customField${this.schema['frequenceStatus'].id}`] = status.value;

    this.onSave(payload, null);
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private onSave(payload: any, callback: any) {
    this._workPackage.updateImmediately(payload)
      .then((wp: WorkPackageResource) => {
        this.wpCacheService.updateWorkPackage(wp, true);
        this._workPackage = wp;
        this.wp = wp.$source;
        this.errorDialogService.showSuccess();
        this.logAdapter.logging(ActivityActionType.UpdateTask, wp.project.id, wp.subject);
        if (callback) callback(wp);
      })
      .catch((error: any) => {
        this.errorDialogService.show('Xảy ra lỗi khi lưu');
      });
  }

  private getValueString(values: string) {
    let dayPairs: any = {'mon': 2, 'tue': 3, 'wed': '4', 'thu': '5', 'fri': '6', 'sat': '7', 'sun': 'CN'};
    let days: any[] = JSON.parse(values);
    let valueString = "T";
    valueString += days.map((day: string) =>  dayPairs[day]).join(', ');

    return valueString;
  }

  private async validateAction(action: string) {
    const allowed = await this.permissionService.canAction(this.projectId, 
      this.isAuthor ? 'AUTHOR' : this.isAssignee ? 'ASSIGNEE' : this.isWatcher ? 'WATCHER' : 'OTHER',
      action);

    if(!allowed) {
      this.errorDialogService.show('Người dùng không thể chỉnh sửa công việc!');
      return false;
    }

    return true;
  }

  public get subject() {
    return this._workPackage.subject;
  }

  public get description() {
    return this._workPackage.description.raw;
  }

  public get subTaskCount() {
    return this._workPackage.children ? this._workPackage.children.length : 0;
  }

  public get frequencies() {
    return this.freqValues;
  }

  public get duration() {
    if (this._workPackage.dueDate == null) return `${moment(this._workPackage.startDate).format('DD/MM')} - Chưa đặt`;

    let startDate = moment(this._workPackage.startDate).format('DD/MM');
    let dueDate = moment(this._workPackage.dueDate).format('DD/MM');

    return `${startDate} - ${dueDate}`;
  }

  public get updatedAt() {
    return moment(this._workPackage.updatedAt).format('DD-MM-YYYY');
  }

  public get disableProjectSelection() {
    return this.$state["params"].projectId || !this.isAuthor;
  }

  public get type() {
    return this.wp[`customField${this.schema['frequenceType'].id}`]
  }

  public get wpId() {
    return this.wp.id;
  }

  public get isAuthor() {
    return this._workPackage && this.userService.userId == this._workPackage.author.id;
  }

  public get isAssignee() {
    return this._workPackage && this.userService.userId == this._workPackage.assignee.id;
  }

  public get isWatcher() {
    return this._workPackage && this._workPackage.watchers && this._workPackage.watchers.elements.some((user: any) => user.id == this.userService.userId);
  }

  public get projectId() {
    return this._workPackage ? this._workPackage.project.id : 0;
  }

  public get canEditAssignee() {
    return this._permission ? this._permission.wpPermission['EDIT_ASSIGNEE'][this.role] : true;
  }

  private get role() {
    return this.isAuthor ? 'AUTHOR' : this.isAssignee ? 'ASSIGNEE' : this.isWatcher ? 'WATCHER' : 'OTHER';
  }
}