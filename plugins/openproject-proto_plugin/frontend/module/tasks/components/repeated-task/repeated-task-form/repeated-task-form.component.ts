import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnDestroy } from '@angular/core';
import {CkeditorAugmentedTextareaComponent} from 'core-app/ckeditor/ckeditor-augmented-textarea.component';
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { WorkPackageResource } from 'core-app/modules/hal/resources/work-package-resource';
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { StateService } from '@uirouter/core';
import { take } from 'rxjs/operators';
import * as moment from 'moment';
import { ProjectsService } from '../../../../projects/services/projects.service';
import { ErrorDialogService } from '../../../../common/components';
import { TasksService } from '../../../services/tasks.service';
import { PermissionService } from '../../../../common/services/permission.service';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './repeated-task-form.component.html',
  selector: 'repeated-task-form'
})
export class RepeatedTaskFormComponent implements OnInit, OnDestroy {

  @Input() isEdit: boolean = false;
  @Input() workPackageId: any;
  @Input() type: string = '';
  @Input() modal: any;
  @Output() onDismiss: EventEmitter<any> = new EventEmitter();

  public advancedOptionTogged: boolean = false;

  public freqTypes: any[] = [
    { value: 'weekly', label: 'Hàng tuần' },
    { value: 'monthly', label: 'Hàng tháng' }
  ];

  public projects: any[] = [];
  public posibleProjects: any[] = [];
  public categories: any[] = [];

  public repeatDays: any[] = [
    { value: 'mon', label: 'Thứ 2' },
    { value: 'tue', label: 'Thứ 3' },
    { value: 'wed', label: 'Thứ 4' },
    { value: 'thu', label: 'Thứ 5' },
    { value: 'fri', label: 'Thứ 6' },
    { value: 'sat', label: 'Thứ 7' },
    { value: 'sun', label: 'Chủ nhật' }
  ];

  public activations: any[] = [
    { value: true, label: 'Kích hoạt' },
    { value: false, label: 'Vô hiệu hóa' }
  ];

  public _workPackage: any;
  public subject: string = '';
  public description: string = '';
  public freqType: string;
  public freqValues: any;
  public freqStatus: boolean = true;
  public projectId: number | undefined;
  public categoryId: number | undefined;
  public assigneeId: number | undefined;
  public followerIds: number[] = [];
  public startDate: any = {};
  public dueDate: any = undefined;

  public loading: boolean = true;

  private ckeditor: CkeditorAugmentedTextareaComponent;
  private _subTasks: any[] = [];
  private _deletedSubTasks: any[] = [];
  private _schema: any;
  private _permission: any;

  constructor(private projectService: ProjectsService,
    private taskService: TasksService,
    private wpCacheService: WorkPackageCacheService,
    private errorDialogService: ErrorDialogService,
    private halResourceService: HalResourceService,
    private userService: CurrentUserService,
    private permissionService: PermissionService,
    private logAdapter: LogAdapter,
    readonly $state: StateService) { }

  async ngOnInit() {
    this.loading = true;
    await this.initData();
    this._schema = await this.taskService.getSchema();
    if(this.isEdit) {
      this.wpCacheService.loadWorkPackage(this.workPackageId, true).values$()
      .pipe(
        take(1)
      )
      .subscribe(async (wp: WorkPackageResource) => {
        this._workPackage = wp;
        await this.generateWP(wp);
        this.loading = false;
      });
    }
    else {
      this.loading = false;
    }    
  }

  ngOnDestroy() { }

  public onAssignTo($event: any) {
    this.assigneeId = $event ? $event.id : undefined;
  }

  public onAddFollowers($event: any[]) {
    this.followerIds = $event.map((user: any) => Number.parseInt(user.id));
  }

  public async save(modal: any) {
    let wpTobeSave: any = {
      subject: this.subject,
      startDate: this.startDate ? moment(`${this.startDate.year}/${this.startDate.month}/${this.startDate.day}`).format('YYYY-MM-DD') : null,
      dueDate: this.dueDate ? moment(`${this.dueDate.year}/${this.dueDate.month}/${this.dueDate.day}`).format('YYYY-MM-DD') : null,
      _links: {
        assignee: {
          href: this.assigneeId ? `/api/v3/users/${this.assigneeId}` : null
        },
        status: {
          href: "/api/v3/statuses/1"
        },
        type: {
          href: "/api/v3/types/10"
        },
        project: {
          href: `/api/v3/projects/${this.projectId}`
        },
        category: {
          href: this.categoryId ? `/api/v3/categories/${this.categoryId}` : null
        }
      },
    };

    if(this.canEditDescription) {
      wpTobeSave['description'] = {
        format: 'markdown',
        raw: this.ckeditor.getContent()
      };
    }
    
    this.injectCustomFields(wpTobeSave);
    if(this.validate(wpTobeSave)) {
      if(this.isEdit){
        wpTobeSave['lockVersion'] = this._workPackage.lockVersion;
        let wp = await this._workPackage.updateImmediately(wpTobeSave)
        .catch((error: any) => {
          this.errorDialogService.show('Xảy ra lỗi khi lưu');
        });

        this.logAdapter.logging(ActivityActionType.UpdateTask, wp.project.id, wp.subject);
        await this.addRemoveWatchers(wp);
        await this.saveSubTasks();
        this.wpCacheService.loadWorkPackage(wp.id!, true);
      }
      else {
        const response = await this.taskService.createAsync(wpTobeSave, this.projectId)
        .catch((error: any) => {
          this.errorDialogService.show('Xảy ra lỗi khi lưu');
        });

        this._workPackage = this.halResourceService.createHalResourceOfType('WorkPackage', response);
        this.logAdapter.logging(ActivityActionType.UpdateTask, this._workPackage.project.id, this._workPackage.subject);
        await this.addRemoveWatchers(this._workPackage);
        await this.saveSubTasks();
      }

      modal.close('created');
    }
  }

  public addSubtask() {
    this._subTasks.push({
      id: 0,
      subject: "",
      assigned_to_id: undefined
    });
  }

  public removeSubTask(index: number) {
    this._deletedSubTasks = this._deletedSubTasks.concat(this._subTasks.splice(index, 1));
  }

  public onChangeSTAssignee($event: any, subTask: any) {
    subTask.assigned_to_id = $event ? $event.id : null;
  }

  public async onProjectSelect($event: any) {
    const response = await this.projectService.getCategories($event.id);
    this.categories = response._embedded.elements;
    this.categoryId = undefined;
    this._permission = await this.permissionService.getCurrentUserPermission(`${this.projectId}`);
  }

  public onFreqTypeSelect($event: any) {
    this.freqValues = undefined;
  }

  onEditorInitialized(editor: CkeditorAugmentedTextareaComponent) {
    this.ckeditor = editor;
  }

  private async saveSubTasks() {
    let deletedIds = this._deletedSubTasks.filter((wp: any) => wp.id != 0).map((wp: any) => wp.id);
    if (deletedIds.length != 0) await this.taskService.deleteMulti(deletedIds);

    this._subTasks.forEach( async (wp: any) => {
      await this.createOrEditWP(wp);
    });

    return true;
  }

  private async createOrEditWP(wp: any) {
    let wpTobeSave: any = {
      subject: wp.subject,
      _links: {
        assignee: {
          href: wp.assigned_to_id ? `/api/v3/users/${wp.assigned_to_id}` : null
        },
        parent: {
          href: `/api/v3/work_packages/${this._workPackage.id}`
        },
        status: {
          href: "/api/v3/statuses/1"
        },
        type: {
          href: "/api/v3/types/10"
        },
        project: {
          href: `/api/v3/projects/${this.projectId}`
        },
        category: {
          href: this.categoryId ? `/api/v3/categories/${this.categoryId}` : null
        }
      },
    };
    
    this.injectCustomFields(wpTobeSave);

    if(wp.id){
      wpTobeSave['lockVersion'] = wp.lockVersion;
      await this.taskService.updateAsync(wp.id, wpTobeSave);
      this.logAdapter.logging(ActivityActionType.UpdateTask, this.projectId, wp.subject);
    }
    else {
      await this.taskService.createAsync(wpTobeSave, this.projectId);
      this.logAdapter.logging(ActivityActionType.CreateTask, this.projectId, wp.subject);
    }
  }

  private async initData() {
    this.freqType = this.type ? this.type : 'weekly';

    if(this.freqType == 'weekly') {
      this.freqValues = [];
    }

    this.startDate = {
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate()
    }

    this.projects = await this.projectService.getProjectsAsync(true);
    this.posibleProjects = this.disableProjectSelection ? this.projects : this.projects.filter((item: any) => {
      return item.permission.projectPermission.canCreateWP;
    });
    
    this.projectId = this.projectIdFromUrl ? Number.parseInt(this.projectIdFromUrl) : undefined;
    if (this.projectId && !this.isEdit) {
      await this.onProjectSelect({id: this.projectId});
    }
  }

  private async generateWP(wp: any) {
    this.projectId = Number.parseInt(wp.project.id);
    await this.onProjectSelect({id: this.projectId});

    this.subject = wp.subject;
    this.description = wp.description.raw;
    this.freqType = wp[`customField${this._schema['frequenceType'].id}`];
    this.freqStatus = wp[`customField${this._schema['frequenceStatus'].id}`];
    this.freqValues = this.isWeekly ? JSON.parse(wp[`customField${this._schema['frequenceValues'].id}`]) : wp[`customField${this._schema['frequenceValues'].id}`];
    
    this.categoryId = wp.category ? Number.parseInt(wp.category.id) : undefined;
    this.assigneeId = wp.assignee ? Number.parseInt(wp.assignee.id) : undefined;
    this.followerIds = wp.watchers.elements.map((user: any) => Number.parseInt(user.id));
    this._subTasks = wp.source._links.children ? wp.source._links.children.map((st: any) => {
      return { id: st.id, subject: st.title, assigned_to_id: st.assigned_to, lockVersion: st.lock_version }
    }) : [];

    this.startDate = {
      year: wp.startDate ? moment(wp.startDate).year() : new Date().getFullYear(),
      month: wp.startDate ? moment(wp.startDate).month() + 1 : new Date().getMonth() + 1,
      day: wp.startDate ? moment(wp.startDate).date() : new Date().getDate()
    }

    this.dueDate = wp.dueDate ? {
      year: moment(wp.dueDate).year(),
      month: moment(wp.dueDate).month() + 1,
      day: moment(wp.dueDate).date()
    } : undefined;
  }

  private injectCustomFields(wp: any) {
    let freqStatus = this._schema['frequenceStatus'];
    let freqValues = this._schema['frequenceValues'];
    let freqType = this._schema['frequenceType'];

    wp['customField' + freqStatus.id] = this.freqStatus;
    wp['customField' + freqValues.id] = this.isWeekly ? JSON.stringify(this.freqValues) : this.freqValues;
    wp['customField' + freqType.id] = this.freqType;
  }

  private async addRemoveWatchers(wp: any) {
    let watcher = this.separateAddOrRemoveWatchers(wp);
    
    let promies: Promise<any>[] = [];
    watcher['added'].forEach((userId: number) => {
      promies.push(this.addRemoveWatcher(userId, wp, true));
    });

    watcher['deleted'].forEach((userId: number) => {
      promies.push(this.addRemoveWatcher(userId, wp, false));
    });

    await Promise.all(promies)
    .then((responses: any[]) => {
    })
    .catch(() => {
      this.errorDialogService.show('Xảy ra lỗi khi lưu');
    });
  }

  private addRemoveWatcher(watcherId: number, wp: any, isAdd: boolean) : Promise<any> {
    let promise:Promise<any>;
    let watcher = {
      href: `/api/v3/users/${watcherId}`,
      id: watcherId
    };

    if (isAdd) { 
      promise = wp.addWatcher.$link.$fetch({ user: { href: watcher.href } });
    }
    else { 
      promise = wp.removeWatcher.$link.$prepare({ user_id: watcher.id })();
    }

    return promise;
  }

  private separateAddOrRemoveWatchers(wp: any) : any {
    let watchers: any = {};
    watchers['added'] = this.followerIds.filter((userId: number) => {
      return wp.watchers.elements.every((watcher: any) => watcher.id != userId);
    }); 

    watchers['deleted'] = wp.watchers.elements.filter((watcher: any) => {
      return this.followerIds.every((userId: number) => watcher.id != userId);
    }).map((watcher: any) => Number.parseInt(watcher.id));

    return watchers;
  }

  private validate(wp: any) {
    // check subject empty for task and its sub tasks
    if(this.subject == "") {
      this.errorDialogService.show('Tên công việc không được bỏ trống.');
      return false;
    }

    if(this._subTasks.some((wp: any) => wp.subject == "")) {
      this.errorDialogService.show('Tên công việc con không được bỏ trống.');
      return false;
    }

    if(this.projectId == 0) {
      this.errorDialogService.show('Chưa lựa chọn dự án');
      return false;
    }

    if(!this.freqValues) {
      this.errorDialogService.show('Tần suất lặp lại không thể để trống');
      return false;
    }

    if(this.freqType == 'monthly') {
      let regx = /^[\d,]*$/;
      if(!regx.test(this.freqValues)) {
        this.errorDialogService.show('Danh sách ngày lặp lại sai định dạng');
        return false;
      }
    }

    if (wp.startDate && wp.dueDate) {
      if(!moment(wp.startDate).isSameOrBefore(moment(wp.dueDate))) {
        this.errorDialogService.show('Ngày bắt đầu phải trước ngày kết thúc');
        return false;
      }
    }
    
    return true;
  }

  public get canEditTitle() {
    return this.isEdit && this._permission ? this._permission.wpPermission['EDIT_NAME'][this.role] : true;
  }

  public get canEditDescription() {
    return this.isEdit && this._permission ? this._permission.wpPermission['EDIT_DESCRIPTION'][this.role] : true;
  }

  public get canEditTime() {
    return this.isEdit && this._permission ? this._permission.wpPermission['EDIT_TIME'][this.role] : true;
  }

  public get canEditAssignee() {
    return this.isEdit && this._permission ? this._permission.wpPermission['EDIT_ASSIGNEE'][this.role] : true;
  }

  public get canCreateWP() {
    return this._permission ? this._permission.projectPermission.canCreateWP : true;
  }

  public get canDeleteWP() {
    return this._permission ? this._permission.wpPermission['DELETE_WP'][this.role] : true;
  }

  public get disableCategory() {
    return this.projectId == 0;
  }

  public get subTasks() {
    return this._subTasks;
  }

  public get title() {
    return this.isEdit ? 'Chỉnh sửa công việc lặp lại' : 'Thêm công việc lặp lại';
  }

  public get saveText() {
    return this.isEdit ? 'Chỉnh sửa' : 'Thêm';
  }

  public get isWeekly() {
    return this.freqType == 'weekly';
  }

  public get disableFreqTypeSelection() {
    return this.type != '' || this.isEdit;
  }

  public get disableProjectSelection() {
    return this.projectIdFromUrl || (!this.isAuthor && this.isEdit);
  }

  public get wpSource() {
    return this.isEdit ? this._workPackage.source : null;
  }

  public get descriptionHtml() {
    return this._workPackage ? this._workPackage.description.html : "";
  }

  public get isAuthor() {
    return this._workPackage && this.userService.userId == this._workPackage.author.id;
  }

  public get isAssignee() {
    return this._workPackage && this.userService.userId == this._workPackage.assignee.id;
  }

  public get isWatcher() {
    return this._workPackage && this._workPackage.watchers && this._workPackage.watchers.elements.some((user: any) => user.id == this.userService.userId);
  }

  private get role() {
    return this.isAuthor ? 'AUTHOR' : this.isAssignee ? 'ASSIGNEE' : this.isWatcher ? 'WATCHER' : 'OTHER';
  }

  private get projectIdFromUrl() {
    return this.$state["params"].projectId;
  }
}