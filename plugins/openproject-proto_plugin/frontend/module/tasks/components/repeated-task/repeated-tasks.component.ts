import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TasksService } from '../../services/tasks.service';
import { WorkPackageResource } from "core-app/modules/hal/resources/work-package-resource";
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { StateService } from '@uirouter/core';
import { LoaderService } from "../../../common/components";
import { ProjectsService } from '../../../projects/services/projects.service';
import { PermissionService } from '../../../common/services/permission.service';

@Component({
  templateUrl: './repeated-tasks.component.html',
  selector: 'repeated-tasks'
})
export class RepeatedTasksComponent implements OnInit {

  private defaultProjectFilter: any = {
    value: "all",
    label: "Tất cả dự án",
    isDefault: true
  };

  public projects: any[] = [];
  public filterProjects: any[] = [this.defaultProjectFilter];
  public selectedProjectForFilter: any = this.defaultProjectFilter;

  private _schema: any;
  private typeName: string = "";
  private _workPackages: any[] = [];
  
  private _searchText: string = "";
  private permission: any | undefined;

  constructor(private modalService: NgbModal,
    private loaderService: LoaderService,
    private projectService: ProjectsService,
    private halResourceService: HalResourceService,
    private wpCacheService: WorkPackageCacheService,
    private permissionService: PermissionService,
    private taskService: TasksService,
    readonly $state: StateService) { }

  async ngOnInit() {
    this.loaderService.loading(true);
    this.taskService.onSearchTaskEvent.subscribe(this.onSearch.bind(this));
    this._schema = await this.taskService.getSchema();
    this.typeName = `customField${this._schema['frequenceType'].id}`;
    this.projects = await this.projectService.getProjectsAsync();
    this.loadProjectForFilter(this.projects);
    await this.loadWPs();
    this.permission = this.projectId ? await this.permissionService.getCurrentUserPermission(`${this.projectId}`) : undefined;
  }

  public open(modal: any) {
    this.modalService.open(modal, { size: 'lg', backdrop: 'static' }).result.then((result: string) => {
      if (result != 'cancel') {
        this.loaderService.loading(true);
        this.loadWPs();
      }
    });;
  }

  public onWPDeleted(wp: any) {
    let index = this._workPackages.indexOf(wp);
    this._workPackages.splice(index, 1);
  }

  public filterByProject(project: any) {
    this.selectedProjectForFilter = project;
  }

  public get byWeekly() {
    return this._workPackages.filter((wp: any) => 
      wp[this.typeName] == 'weekly' &&
      wp.subject.toLowerCase().includes(this._searchText.toLowerCase()) &&
      (this.selectedProjectForFilter.value == 'all' ? true : wp._links.project.id == this.selectedProjectForFilter.value)
      );
  }

  public get byMonthly() {
    return this._workPackages.filter((wp: any) => 
      wp[this.typeName] == 'monthly' && 
      wp.subject.toLowerCase().includes(this._searchText.toLowerCase()) &&
      (this.selectedProjectForFilter.value == 'all' ? true : wp._links.project.id == this.selectedProjectForFilter.value)
      );
  }

  public get schema() {
    return this._schema;
  }

  public get projectId() {
    return this.$state["params"].projectId;
  }

  public get allowedToCreateWP() {
    return (this.permission && this.permission.projectPermission.canCreateWP) || !this.projectId;
  }

  private loadProjectForFilter(projects: any) {
    this.filterProjects = this.filterProjects.concat(projects.map((project: any) => {
      return { value: project.id, label: project.name };
    }));

    this.selectedProjectForFilter = this.filterProjects[0];
  }

  private onSearch(event: any) {
    this._searchText = event.filters[0]['subjectOrId']['values'] as string;
  }

  private async loadWPs() {
    const response = await this.taskService.getRepeatedWPs(this.projectId);
    this._workPackages = response._embedded.elements;
    this.updateCache(this._workPackages);
    this.loaderService.loading(false);
  }

  private updateCache(workPackages: any[]) {
    workPackages.forEach((item: any) => {
      let wp = this.halResourceService.createHalResource<WorkPackageResource>(item);
      this.wpCacheService.updateWorkPackage(wp, true);
    });
  }
}