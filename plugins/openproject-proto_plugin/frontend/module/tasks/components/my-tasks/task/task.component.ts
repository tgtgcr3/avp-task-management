import { Component, Input, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { TasksService } from "../../../services/tasks.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { NgbModal, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { ErrorDialogService } from "../../../../common/components";
import { CkeditorAugmentedTextareaComponent } from "core-app/ckeditor/ckeditor-augmented-textarea.component";
import { UserCacheService } from "core-components/user/user-cache.service";
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { WorkPackageResource } from "core-app/modules/hal/resources/work-package-resource";
import {Subscription} from 'rxjs';
import * as moment from 'moment';
import { AVP, NotificationType } from '../../../../common/consts/avp';
import { StateService, TransitionService, Transition, StateOrName } from '@uirouter/core';
import { AvpNotificationService } from '../../../../avp-base/notifications/avp-notifications.service';
import { PermissionService } from '../../../../common/services/permission.service';
import { ProjectsService } from '../../../../projects/services/projects.service';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './task.component.html',
  selector: 'task-detail'
})
export class TaskComponent implements OnInit, OnDestroy {
  @Input() item: any;

  @ViewChild('userSelector') internalUserSelector: NgbPopover;

  public workPackage: WorkPackageResource;
  public actions: any = {
    isSubjectEditing: false,
    isDescriptionEditing: false,
    isResultEditing: false,
    category: {
      showOrCollapse: false
    }
  };
  public subTaskIds: string[] = [];
  public loading: boolean = false;
  public watchers: any[];
  public attachments: any[] | undefined;
  public activities: any[] | undefined;
  public categories: any[] = [];

  private unregisterListener: Function;
  private isInActive: boolean = false;
  private currentUser: any;
  private permission: any;
  private observerWPSubscription: Subscription;
  private ckeditor: CkeditorAugmentedTextareaComponent;

  constructor(readonly userCacheService: UserCacheService,
    private wpCacheService: WorkPackageCacheService,
    private taskService: TasksService,
    private userService: CurrentUserService,
    private modalService: NgbModal,
    private errorDialogService: ErrorDialogService,
    private notificationService: AvpNotificationService,
    private permissionService: PermissionService,
    private projectService: ProjectsService,
    private logAdapter: LogAdapter,
    readonly $state: StateService,
    readonly trans: TransitionService) {
  }

  ngOnInit() {
    this.onLoadWorkPackage();
    this.loadCurrentUser();
    this.unregisterListener = this.trans.onSuccess({}, (transition: Transition) => {
      if (this.isInActive) return undefined;
      this.item = undefined;
      this.activities = undefined;
      this.attachments = undefined;
      this.onLoadWorkPackage();
      return true;
    });
  }

  ngOnDestroy() {
    this.isInActive = true;
    this.unregisterListener();
  }

  public get isAuthor() {
    return `/api/v3/users/${this.userService.userId}` == this.item._links.author.href;
  }

  public get isAssignee() {
    return `/api/v3/users/${this.userService.userId}` == this.item._links.assignee.href;
  }

  public get isResponsible() {
    return `/api/v3/users/${this.userService.userId}` == this.item._links.responsible.href;
  }

  public get canTakeAction() {
    return this.userService.role == 'owner' || this.isAuthor;
  }

  public get projectId() {
    return this.item._links.project.href.split('/').pop()!;
  }

  public get categoryId() {
    return this.item._links.category.href ? this.item._links.category.href.split('/').pop()! : 0;
  }

  public get isOverdue() {

    if (this.workPackage.dueDate == null) {
      return false;
    }

    let dueDate = new Date(this.workPackage.dueDate);
    dueDate.setDate(dueDate.getDate() + 1);
    let date = new Date();
    return dueDate.getTime() < date.getTime() &&
      !this.workPackage.status.name.toLowerCase().includes('done');
  }

  public get parent() {
    return this.workPackage && this.workPackage.parent ? this.workPackage.parent.name : '';
  }

  public get statusString() {
    if (this.status.includes('done')) {
      return 'Hoàn thành';
    }
    else if (this.status.includes('progress')) {
      return 'Đang làm';
    }
    else if (this.status.includes('review')) {
      return 'Chờ review';
    }
    else {
      return 'Phải làm';
    }
  }

  public get status() {
    return this.item._links.status.title.toLowerCase();
  }

  onShowOrCollapse(item: any) {
    item.showOrCollapse = !item.showOrCollapse;
  }

  enableTitleEditing() {
    this.actions.isSubjectEditing = true;
  }

  onEditorInitialized(editor: CkeditorAugmentedTextareaComponent) {
    this.ckeditor = editor;
  }

  async onSaveTitle() {
    const valid = await this.validateAction('EDIT_NAME');
    if(!valid) {
      return;
    }

    let payload = {
      subject: this.item.subject,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, null);
    this.onDisableTitleEditing();
  }

  onDisableTitleEditing() {
    this.actions.isSubjectEditing = false;
  }

  enableResultEditing() {
    this.actions.isResultEditing = true;
  }

  enableDescriptionEditor() {
    this.actions.isDescriptionEditing = true;
  }

  async onSaveResult() {
    const valid = await this.validateAction('EDIT_RESULT');
    if(!valid) {
      return;
    }

    let payload = {
      _links: {},
      customField2: {
        format: "markdown",
        raw: this.item.customField2.raw
      },
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_RESULT, notifyData);
    });
    this.onDisableResultEditing();
  }

  async onSaveDescription() {
    const valid = await this.validateAction('EDIT_DESCRIPTION');
    if(!valid) {
      return;
    }

    let payload = {
      _links: {},
      description: {
        format: "markdown",
        raw: this.ckeditor.getContent()
      },
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_DESCRIPTION, notifyData);
    });
    this.disableDescriptionEditor();
  }

  onDisableResultEditing() {
    this.actions.isResultEditing = false;
  }

  disableDescriptionEditor() {
    this.actions.isDescriptionEditing = false;
  }

  private loadCurrentUser() {
    this.userCacheService
      .require(this.userService.userId)
      .then((user: any) => {
        this.currentUser = user;
      });
  }

  public async onStatusChanged() {
    if (!this.status.includes('new')) {
      return;
    }

    await this.statusChangesDirectly();
  }

  public async statusChangesDirectly() {
    const valid = await this.validateAction('EDIT_STATUS');
    if(!valid) {
      return;
    }


    let payload = {
      _links: {
        status: {
          href: '/api/v3/statuses/7'
        }
      },
      lockVersion: this.item.lockVersion
    };

    if (this.status.includes('progress') || this.status.includes('review')) {
      // update status as done
      payload._links.status.href = '/api/v3/statuses/16';

      if (this.isOverdue) {
        payload._links.status.href = '/api/v3/statuses/17';
      }
    }

    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_STATUS, notifyData);
    });
  }

  public async statusChangesTo(targetStatus: string) {
    const valid = await this.validateAction('EDIT_STATUS');
    if(!valid) {
      return;
    }


    if (this.status.includes(targetStatus)) {
      return;
    }

    let payload = {
      _links: {
        status: {
          href: '/api/v3/statuses/7'
        }
      },
      lockVersion: this.item.lockVersion
    };

    if (targetStatus == 'review') {
      payload._links.status.href = '/api/v3/statuses/10';
    }
    else if (targetStatus == 'done') {
      payload._links.status.href = '/api/v3/statuses/16';

      if (this.isOverdue) {
        payload._links.status.href = '/api/v3/statuses/17';
      }
    }

    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_STATUS, notifyData);
    });
  }

  public onFavouriteChanged() {
    let payload = {
      _links: {},
      customField12: !this.item.customField12,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, null);
  }

  public onImportantChanged() {
    let payload = {
      _links: {},
      customField13: !this.item.customField13,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, null);
  }

  public onUrgentChanged() {
    let payload = {
      _links: {},
      customField14: !this.item.customField14,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_URGENT, notifyData);
    });
  }

  public async onUpdateStartDate(evt: any) {
    const valid = await this.validateAction('EDIT_TIME');
    if(!valid) {
      return;
    }


    if (!this.isValidDate(evt, this.item.dueDate)) {
      this.errorDialogService.show('Ngày Bắt Đầu phải bé hơn Ngày Kết Thúc!');
      return false;
    }

    let payload = {
      _links: {},
      startDate: evt == null ? null : evt,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_START_DATE, notifyData);
    });
    return true;
  }

  public async onUpdateDueDate(evt: any) {
    const valid = await this.validateAction('EDIT_TIME');
    if(!valid) {
      return;
    }

    if (!this.isValidDate(this.item.startDate, evt)) {
      this.errorDialogService.show('Ngày Bắt Đầu phải bé hơn Ngày Kết Thúc!');
      return false;
    }

    let payload = {
      _links: {},
      dueDate: evt == null ? null : evt,
      lockVersion: this.item.lockVersion
    };

    if (payload.dueDate) {
      let dueDate = new Date(payload.dueDate);
      dueDate.setDate(dueDate.getDate() + 1);
      let date = new Date();

      if (this.item._links.status.title.toLowerCase() == 'done' && dueDate.getTime() < date.getTime()) {
        payload._links = {
          status: {
            href: `/api/v3/statuses/17`
          }
        }
      }
      else if (this.item._links.status.title.toLowerCase() == 'done late' && dueDate.getTime() >= date.getTime()) {
        payload._links = {
          status: {
            href: `/api/v3/statuses/16`
          }
        }
      }
    }

    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_DUE_DATE, notifyData);
    });
    return true;
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private isValidDate(startDate: any, dueDate: any) {
    if (!startDate || !dueDate) {
      return true;
    }
    return moment(startDate).isSameOrBefore(moment(dueDate));
  }

  public async onAssigneeChanged(assigneeId: number) {
    const valid = await this.validateAction('EDIT_ASSIGNEE');
    if(!valid) {
      return;
    }

    this.internalUserSelector.isOpen() ? this.internalUserSelector.close() : '';
    let payload = {
      _links: {
        assignee: {
          href: assigneeId == 0 ? null : `/api/v3/users/${assigneeId}`
        }
      },
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_ASSIGNED, notifyData);
    });
  }

  public onAttachmentsChanged(evt: any) {
    let attachments = Array.from(evt.target.files);
    attachments.forEach((attachment: any) => {
      attachment.description = {
        format: "plain",
        html: "",
        raw: "AttachedFile"
      }
    });
    this.uploadAttachments(attachments, false);
  }

  public onResultAttachmentsChanged(evt: any) {
    let attachments = Array.from(evt.target.files);
    attachments.forEach((attachment: any) => {
      attachment.description = {
        format: "plain",
        html: "",
        raw: "Result"
      }
    });
    this.uploadAttachments(attachments, true);
  }

  private uploadAttachments(attachments: any[], isResult: boolean) {
    this.workPackage.uploadAttachments(attachments)
      .then((response: any) => {
        this.onLoadAttachments(true);
        let type = isResult == true ? NotificationType.WP_RESULT : NotificationType.WP_ATTACHMENT;
        let notifyData: any = {
          receivers: this.notifyReceivers(this.workPackage),
          author: this.currentUser,
          wp: this.workPackage,
          attachments: attachments.map((attachment: any) => attachment.name)
        }

        this.notificationService.notify(type, notifyData);
      })
      .catch((error: any) => {
        this.errorDialogService.show("Xảy ra lỗi khi tải tệp");
      });
  }

  public onSubTasksCreated(wps: any[]) {
    if (wps.length > 0) {
      this.subTaskIds = this.subTaskIds.concat(wps.map((wp: any) => wp.id));
      this.taskService.onTaskCreated(wps);
      this.wpCacheService.loadWorkPackage(this.taskId, true)
    }
  }

  public onOpenExternalUserSelector(modal: any) {
    this.internalUserSelector.close();
    this.open(modal);
  }

  public open(content: any) {
    this.modalService.open(content);
  }

  public openTaskModal(modal: any) {
    this.modalService.open(modal, { size: 'lg', backdrop: 'static' });
  }

  public openDeletePopup(dialog: any) {
    this.modalService.open(dialog).result.then((result) => {
      if (result == 'OK') {
        this.onDelete();
        this.modalService.dismissAll();
      }
    });
  }

  public onWatchersChanged(event: any) {
    this.watchers = event;
  }

  public onAddWatchers(modal: any) {
    let addWatcherRequests: any[] = this.watchers.map((user: any) => {
      return this.workPackage.addWatcher.$link.$fetch({ user: { href: `/api/v3/users/${user.id}` } })
    });

    Promise.all(addWatcherRequests)
      .then((response: any) => {
        let notifyData: any = {
          receivers: this.notifyReceivers(this.workPackage),
          author: this.currentUser,
          wp: this.workPackage,
          watchers: response.map((watcher: any) => watcher.name)
        }

        this.notificationService.notify(NotificationType.WP_WATCHER, notifyData);
        this.taskService.onRefreshWatchers();
      })
      .catch((error: any) => {
        this.errorDialogService.show('Xảy ra lỗi thêm người theo dõi');
      });
    modal.dismiss();
  }

  public onChangeCategory(categoryId: string | null) {
    let payload = {
      _links: {
        category: {
          href: categoryId ? `/api/v3/categories/${categoryId}` : categoryId
        }
      },
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, null);
  }

  private onLoadWorkPackage() {
    if(!this.taskId) {
      return;
    }

    this.loading = true;
    this.wpCacheService.require(this.taskId, true)
      .then(async (wp: WorkPackageResource) => {
        this.onUpdateWP(wp);
        this.registerObserverWP();
        await this.onLoadAttachments();
        await this.onLoadActivities();
        await this.onLoadCategories();
        this.permission = await this.permissionService.getCurrentUserPermission(this.projectId);
        this.loading = false;
      });
  }

  private registerObserverWP() {
    if(this.observerWPSubscription) {
      this.observerWPSubscription.unsubscribe();
    }

    this.observerWPSubscription = this.wpCacheService.loadWorkPackage(this.item.id)
                                      .values$()
                                      .subscribe(this.onUpdateWP.bind(this));
  }

  private onUpdateWP(wp: WorkPackageResource) {
    this.workPackage = wp;
    this.item = wp.$source;
    this.subTaskIds = this.item._links.children ? this.item._links.children.map((item: any) => {
      return item.href.split('/').pop()!;
    }) : [];
  }

  private async onLoadAttachments(force: boolean = false) {
    if(force || !this.attachments) {
      let response = await this.taskService.getAsyncDataUrl(this.item._links.attachments.href);
      this.attachments = response._embedded.elements;
    }
  }

  private async onLoadActivities(force: boolean = false) {
    if(force || !this.activities) {
      let response = await this.taskService.getActivities(this.item.id);
      this.activities = response._embedded.elements;
    }
  }

  private async onLoadCategories() {
    const response = await this.projectService.getCategories(this.projectId);
    this.categories = response._embedded.elements;
  }

  private async onDelete() {
    const valid = await this.validateAction('DELETE_WP');
    if(!valid) {
      return;
    }

    this.workPackage.delete()
      .then((res: any) => {
        this.taskService.onRefresh();
        let notifyData: any = {
          receivers: this.notifyReceivers(this.workPackage),
          author: this.currentUser,
          wp: this.workPackage
        }
        this.logAdapter.logging(ActivityActionType.DeleteTask, this.projectId, this.workPackage.subject);
        this.notificationService.notify(NotificationType.WP_DELETE, notifyData);
        this.$state.go(this.$state.current.name as StateOrName, { task: null });
      })
      .catch((error: any) => {
        this.errorDialogService.show('Xảy ra lỗi khi xóa');
      });
  }

  private onSave(payload: any, callback: any) {
    this.workPackage.updateImmediately(payload)
      .then((response: any) => {
        this.wpCacheService.updateWorkPackage(response, true);
        this.errorDialogService.showSuccess();
        this.logAdapter.logging(ActivityActionType.UpdateTask, this.projectId, response.subject);
        if (callback) callback(response);
      })
      .catch((error: any) => {
        this.errorDialogService.show('Xảy ra lỗi khi lưu');
      });
  }

  private notifyReceivers(workPackage: any) {
    let assigneeEmail = workPackage.assignee ? workPackage.assignee.email : "";
    let receiverEmails = workPackage.watchers ? workPackage.watchers.elements.map((user: any) => user.email) : [];
    let authorEmail = workPackage.author.email;

    if (!receiverEmails.includes(assigneeEmail) && assigneeEmail) {
      receiverEmails.push(assigneeEmail);
    }

    if (!receiverEmails.includes(authorEmail) && authorEmail) {
      receiverEmails.push(authorEmail);
    }

    return receiverEmails.filter((email: string) => {
      return email != this.currentUser.email;
    });
  }

  public get validUser() {
    return this.isAuthor || this.isAssignee;
  }

  public get canCreateWP() {
    return this.permission && this.permission.projectPermission.canCreateWP;
  }

  public get taskId() {
    return this.item ? this.item.id : this.$state.params['task'];
  }

  public get stateName() {
    return this.$state.current.name;
  }

  public get isWatcher() {
    return this.workPackage && this.workPackage.watchers && this.workPackage.watchers.elements.some((user: any) => user.id == this.userService.userId);
  }

  private async validateAction(action: string) {
    const canEditWP = await this.permissionService.canAction(this.projectId, 
      this.isAuthor ? 'AUTHOR' : this.isAssignee ? 'ASSIGNEE' : this.isWatcher ? 'WATCHER' : 'OTHER',
      action);

    if(!canEditWP) {
      if(action == 'EDIT_STATUS' && this.isResponsible){
        return true;
      }
      this.errorDialogService.show('Người dùng không thể chỉnh sửa công việc!');
      return false;
    }

    return true;
  }
}