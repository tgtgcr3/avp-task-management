import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { ErrorDialogService } from "../../../../../common/components";
import { CurrentUserService } from 'core-components/user/current-user.service';

@Component({
  templateUrl: './attachments.component.html',
  selector: 'attachments-preview'
})
export class AttachmentsComponent implements OnInit, OnChanges {
  @Input() item: any;
  @Input() attachments: any[] = [];
  @Input() description: string = 'AttachedFile';

  public files: any[] = [];
  public images: any[] = [];

  constructor(private logAdapter: LogAdapter,
    private currentUserService: CurrentUserService,
    private halResourceService: HalResourceService,
    private errorDialogService: ErrorDialogService) {
  }

  ngOnInit() {
    this.separateType(this.attachments);
  }

  ngOnChanges(changes: any) {
    if(changes.attachments)
      this.separateType(changes.attachments.currentValue);
  }

  public onDelete(file: any) {
    let resource = this.halResourceService.createHalResource(file, true);
    resource.delete()
      .then((response: any) => {
        if (file.contentType.includes('image/')) {
          let index = this.images.indexOf(file);
          if (index > -1) {
            this.images.splice(index, 1);
          }
        }
        else {
          let index = this.files.indexOf(file);
          if (index > -1) {
            this.files.splice(index, 1);
          }
        }
        this.logAdapter.logging(ActivityActionType.UpdateTask, this.item._links.project.href.split('/').pop(), this.item.subject);
      })
      .catch((error: any) => {
        this.errorDialogService.show(error.message);
      });
  }

  private separateType(attachments: any[]) {
    this.images = attachments.filter((value: any) => {
      return value.contentType.includes('image/') && value.description.raw == this.description;
    });

    this.files = attachments.filter((value: any) => {
      return !value.contentType.includes('image/') && value.description.raw == this.description;
    });
  }

  private get isAuthor() {
    return this.currentUserService.userId == this.item._links.author.href.split('/').pop();
  }

  private get isAssignee() {
    return `/api/v3/users/${this.currentUserService.userId}` == this.item._links.assignee.href;
  }

  public get validUser() {
    return this.isAuthor || this.isAssignee;
  }
}