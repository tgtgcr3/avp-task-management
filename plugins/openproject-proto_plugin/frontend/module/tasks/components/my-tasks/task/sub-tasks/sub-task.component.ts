import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ErrorDialogService } from "../../../../../common/components";
import { UserCacheService } from "core-components/user/user-cache.service";
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { WorkPackageResource } from "core-app/modules/hal/resources/work-package-resource";
import { componentDestroyed } from 'ng2-rx-componentdestroyed';
import { takeUntil } from 'rxjs/operators';
import { AVP } from '../../../../../common/consts/avp';
import { StateService } from '@uirouter/core';
import { PermissionService } from '../../../../../common/services/permission.service';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './sub-task.component.html',
  selector: 'sub-task'
})
export class SubTaskComponent implements OnInit, OnDestroy {
  @Input() id: string;

  public workPackage: WorkPackageResource;
  public loading: boolean = true;

  constructor(readonly userCacheService: UserCacheService,
    private wpCacheService: WorkPackageCacheService,
    private errorDialogService: ErrorDialogService,
    readonly $state: StateService,
    private permissionService: PermissionService,
    private userService: CurrentUserService,
    private logAdapter: LogAdapter) {
  }

  ngOnInit() {
    this.loadWP();
  }

  ngOnDestroy() { }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public get isWatcher() {
    return this.workPackage && this.workPackage.watchers && this.workPackage.watchers.elements.some((user: any) => user.id == this.userService.userId);
  }

  public get isAuthor() {
    return this.userService.userId == this.workPackage.author.id;
  }

  public get isAssignee() {
    return this.workPackage.assignee && this.userService.userId == this.workPackage.assignee.id;
  }

  public get projectId() {
    return this.workPackage.project.id;
  }

  public get stateName() {
    return this.$state.current.name;
  }

  public onSubTaskStatusChange(wp: any) {
    if(!this.validateAction('EDIT_STATUS')) {
      return;
    }

    let payload = {
      _links: {
        status: {
          href: '/api/v3/statuses/7'
        }
      },
      lockVersion: wp.lockVersion
    };

    if (wp.status.name == 'Done'
      || wp.status.name == 'Done Late') {
      // update status as not done
    }
    else {
      // update status as done
      payload._links.status.href = '/api/v3/statuses/16';

      if (wp.dueDate) {
        let dueDate = new Date(wp.dueDate);
        if (dueDate.getTime() < new Date().getTime() &&
          wp.status.name != 'Done'
          && wp.status.name != 'Done Late') {
          payload._links.status.href = '/api/v3/statuses/17';
        }
      }
    }

    wp.updateImmediately(payload)
      .then((wp: WorkPackageResource) => {
        this.wpCacheService.updateWorkPackage(wp, true);
        this.logAdapter.logging(ActivityActionType.UpdateTask, wp.project.id, wp.subject);
        this.errorDialogService.showSuccess();
      })
      .catch((error: any) => {
        this.errorDialogService.show('Xảy ra lỗi khi lưu');
      });
  }

  private loadWP() {
    this.loading = true;
    this.wpCacheService.loadWorkPackage(this.id).values$()
      .pipe(
        takeUntil(componentDestroyed(this))
      )
      .subscribe((wp: WorkPackageResource) => {
        this.workPackage = wp;
        this.loading = false;
      });
  }

  private validateAction(action: string) {
    if(!this.permissionService.canAction(this.projectId, 
      this.isAuthor ? 'AUTHOR' : this.isAssignee ? 'ASSIGNEE' : this.isWatcher ? 'WATCHER' : 'OTHER',
      action)) {

      this.errorDialogService.show('Người dùng không thể chỉnh sửa công việc!');
      return false;
    }

    return true;
  }
}