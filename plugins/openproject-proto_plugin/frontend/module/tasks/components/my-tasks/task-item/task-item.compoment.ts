import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { UserCacheService } from "core-components/user/user-cache.service";
import { NgbModal, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { AvpUtils } from '../../../../common/services/avp-utils.service';
import { componentDestroyed } from 'ng2-rx-componentdestroyed';
import { takeUntil } from 'rxjs/operators';
import { ErrorDialogService } from "../../../../common/components";
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { WorkPackageResource } from 'core-app/modules/hal/resources/work-package-resource';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { AVP, NotificationType } from '../../../../common/consts/avp';
import * as moment from 'moment';
import { StateService } from '@uirouter/core';
import { AvpNotificationService } from '../../../../avp-base/notifications/avp-notifications.service';
import { PermissionService } from '../../../../common/services/permission.service';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './task-item.component.html',
  selector: 'task-item'
})
export class TaskItemComponent implements OnInit, OnDestroy {

  @Input() item: any = undefined;
  @Input() isShowSubtasks: boolean = false;
  @Input() isSubtask: boolean = false;
  @Input() view: String = "";
  @Output() onDeleted: EventEmitter<any> = new EventEmitter();

  @ViewChild('userSelector') internalUserSelector: NgbPopover;
  public isStartDatePopupShowing: boolean = false;
  public isEndDatePopupShowing: boolean = false;
  public isTitleEditing: boolean = false;
  public titleEditText: string = "";

  private workPackage: WorkPackageResource;
  private currentUser: any;

  constructor(readonly userCacheService: UserCacheService,
    private modalService: NgbModal,
    private userService: CurrentUserService,
    private errorDialogService: ErrorDialogService,
    private wpCacheService: WorkPackageCacheService,
    private logAdapter: LogAdapter,
    private permissionService: PermissionService,
    private notificationService: AvpNotificationService,
    private ref: ChangeDetectorRef,
    readonly $state: StateService) { }

  ngOnInit() {
    this.loadWorkPackage();
    this.loadCurrentUser();
  }

  ngOnDestroy() {

  }

  public get projectName() {
    return this.workPackage ? this.workPackage.project.name : '';
  }
  public get projectId() {
    return this.workPackage ? this.workPackage.project.id : 0;
  }

  public get description() {
    return this.item.description.raw;
  }

  public get author() {
    return this.item && this.item._links ? this.item._links.author.title : '';
  }

  public get isAuthor() {
    return this.userService.userId == this.item._links.author.href.split('/').pop();
  }

  public get canDelete() {
    return this.userService.role == 'owner' || this.isAuthor;
  }

  public get isResponsible() {
    return this.item._links.responsible.href ? false : this.userService.userId == this.item._links.responsible.href.split('/').pop();
  }

  public get assignee() {
    return this.item && this.item._links ? this.item._links.assignee.href ? this.item._links.assignee.title : undefined : undefined;
  }

  public get isAssignee() {
    return `/api/v3/users/${this.userService.userId}` == this.item._links.assignee.href;
  }

  public get isWatcher() {
    return this.workPackage && this.workPackage.watchers && this.workPackage.watchers.elements.some((user: any) => user.id == this.userService.userId);
  }

  public get parent() {
    return this.workPackage && this.workPackage.parent ? this.workPackage.parent.name : '';
  }

  public get subTasks() {
    return this.workPackage && this.workPackage.children ? this.workPackage.children : [];
  }

  public get isDone() {
    return this.item && this.item._links ? this.item._links.status.title.toLowerCase().includes('done') : false;
  }

  public get isDoneLate() {
    return this.item && this.item._links ? this.item._links.status.title.toLowerCase().includes('done late') : false;
  }

  public get isSelected() {
    return this.$state.params['task'] == this.item.id;
  }

  public get stateName() {
    return this.$state.current.name;
  }

  public isExpired(dueDateString: string) {
    if (!dueDateString) {
      return false;
    }

    let dueDate = new Date(dueDateString);
    dueDate.setDate(dueDate.getDate() + 1);
    let date = new Date();
    return dueDate.getTime() < date.getTime() &&
      !this.item._links.status.title.toLowerCase().includes('done');
  }

  public openTask() {
    this.$state.go('tasks.list', { task: this.item.id }, { notify: false })
  }

  public async onStatusChanged() {
    const valid = await this.validateAction('EDIT_STATUS');
    if(!valid) {
      return;
    }

    this.onEditTitleCancel();
    let payload = {
      _links: {
        status: {
          href: '/api/v3/statuses/7'
        }
      },
      lockVersion: this.item.lockVersion
    };

    if (this.item._links.status.title.toLowerCase().includes('done')) {
      // update status as not done
    }
    else {
      // update status as done
      payload._links.status.href = '/api/v3/statuses/16';

      if (this.isExpired(this.item.dueDate)) {
        payload._links.status.href = '/api/v3/statuses/17';
      }
    }

    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_STATUS, notifyData);
    });
  }

  public onFavouriteChanged() {
    this.onEditTitleCancel();
    let payload = {
      _links: {},
      customField12: !this.item.customField12,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, null);
  }

  public onImportantChanged() {
    let payload = {
      _links: {},
      customField13: !this.item.customField13,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, null);
  }

  public onUrgentChanged() {
    let payload = {
      _links: {},
      customField14: !this.item.customField14,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_URGENT, notifyData);
    });
  }

  public async onUpdateStartDate(evt: any, popup: any) {
    const valid = await this.validateAction('EDIT_TIME');
    if(!valid) {
      return;
    }

    if (!this.isValidDate(evt, this.item.dueDate)) {
      this.errorDialogService.show('Ngày Bắt Đầu phải bé hơn Ngày Kết Thúc!');
      return false;
    }

    let payload = {
      _links: {},
      startDate: evt == null ? null : evt,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_START_DATE, notifyData);
    });
    popup.close();
    return true;
  }

  public async onUpdateDueDate(evt: any, popup: any) {
    const valid = await this.validateAction('EDIT_TIME');
    if(!valid) {
      return;
    }

    if (!this.isValidDate(this.item.startDate, evt)) {
      this.errorDialogService.show('Ngày Bắt Đầu phải bé hơn Ngày Kết Thúc!');
      return false;
    }

    let payload = {
      _links: {},
      dueDate: evt == null ? null : evt,
      lockVersion: this.item.lockVersion
    };

    if (payload.dueDate) {
      let dueDate = new Date(payload.dueDate);
      dueDate.setDate(dueDate.getDate() + 1);
      let date = new Date();

      if (this.item._links.status.title.toLowerCase() == 'done' && dueDate.getTime() < date.getTime()) {
        payload._links = {
          status: {
            href: `/api/v3/statuses/17`
          }
        }
      }
      else if (this.item._links.status.title.toLowerCase() == 'done late' && dueDate.getTime() >= date.getTime()) {
        payload._links = {
          status: {
            href: `/api/v3/statuses/16`
          }
        }
      }
    }

    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_DUE_DATE, notifyData);
    });
    popup.close();
    return true;
  }

  public async onAssigneeChanged(assigneeId: number) {
    const valid = await this.validateAction('EDIT_ASSIGNEE');
    if(!valid) {
      return;
    }

    this.internalUserSelector.isOpen() ? this.internalUserSelector.close() : '';
    let payload = {
      _links: {
        assignee: {
          href: assigneeId == 0 ? null : `/api/v3/users/${assigneeId}`
        }
      },
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_ASSIGNED, notifyData);
    });
  }

  public openDeletePopup(dialog: any) {
    this.modalService.open(dialog).result.then((result) => {
      if (result == 'OK') {
        this.onDelete();
      }
    });
  }

  public onEditTitle() {
    this.isTitleEditing = true;
    this.titleEditText = this.item.subject;
  }

  public onEditTitleCancel() {
    this.isTitleEditing = false;
  }

  public async onSaveTitle() {
    const valid = await this.validateAction('EDIT_NAME');
    if(!valid) {
      return;
    }

    this.isTitleEditing = false;
    let payload = {
      _links: {},
      subject: this.titleEditText,
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, null);
  }

  public onOpenExternalUserSelector(modal: any) {
    this.internalUserSelector.close();
    this.modalService.open(modal);
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private isValidDate(startDate: any, dueDate: any) {
    if (!startDate || !dueDate) {
      return true;
    }
    return moment(startDate).isSameOrBefore(moment(dueDate));
  }

  private loadWorkPackage() {
    this.wpCacheService.loadWorkPackage(this.item.id).values$()
      .pipe(
        takeUntil(componentDestroyed(this))
      )
      .subscribe((wp: WorkPackageResource) => {
        this.workPackage = wp;
        this.item = wp.$source;
        this.ref.detectChanges();
      });
  }

  private loadCurrentUser() {
    this.userCacheService
      .require(this.userService.userId)
      .then((user: any) => {
        this.currentUser = user;
      });
  }

  private async onDelete() {
    const valid = await this.validateAction('DELETE_WP');
    if(!valid) {
      return;
    }

    this.workPackage.delete()
      .then((res: any) => {
        let notifyData: any = {
          receivers: this.notifyReceivers(this.workPackage),
          author: this.currentUser,
          wp: this.workPackage
        }
        this.logAdapter.logging(ActivityActionType.DeleteTask, this.workPackage.project.id, this.workPackage.subject);
        this.notificationService.notify(NotificationType.WP_DELETE, notifyData);
        this.onDeleted.emit();
      })
      .catch((error: any) => {
        this.errorDialogService.show('Xảy ra lỗi khi xóa');
      });
  }

  private onSave(payload: any, callback: any) {
    this.workPackage.updateImmediately(payload)
      .then((wp: WorkPackageResource) => {
        this.wpCacheService.updateWorkPackage(wp, true);
        this.workPackage = wp;
        this.item = wp.$source;
        this.errorDialogService.showSuccess();
        this.logAdapter.logging(ActivityActionType.UpdateTask, wp.project.id, wp.subject);
        if (callback) callback(wp);
      })
      .catch((error: any) => {
        this.errorDialogService.show('Xảy ra lỗi khi lưu');
      });
  }

  private notifyReceivers(workPackage: any) {
    let assigneeEmail = workPackage.assignee ? workPackage.assignee.email : "";
    let receiverEmails = workPackage.watchers ? workPackage.watchers.elements.map((user: any) => user.email) : [];
    let authorEmail = workPackage.author.email;

    if (!receiverEmails.includes(assigneeEmail) && assigneeEmail) {
      receiverEmails.push(assigneeEmail);
    }

    if (!receiverEmails.includes(authorEmail) && authorEmail) {
      receiverEmails.push(authorEmail);
    }

    return receiverEmails.filter((email: string) => {
      return email != this.currentUser.email;
    });
  }

  private get validUser() {
    return this.isAuthor || this.isAssignee;
  }

  private async validateAction(action: string) {
    const canEditWP = await this.permissionService.canAction(this.projectId, 
      this.isAuthor ? 'AUTHOR' : this.isAssignee ? 'ASSIGNEE' : this.isWatcher ? 'WATCHER' : 'OTHER',
      action);

    if(!canEditWP) {
      if(action == 'EDIT_STATUS' && this.isResponsible) {
        return true;
      }
      this.errorDialogService.show('Người dùng không thể chỉnh sửa công việc!');
      return false;
    }

    return true;
  }
}