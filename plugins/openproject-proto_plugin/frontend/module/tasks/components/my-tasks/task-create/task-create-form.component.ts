import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { TasksService } from "../../../services/tasks.service";
import { MembersService } from "../../../../members/services/members.service";
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { HalResource } from "core-app/modules/hal/resources/hal-resource";
import { ErrorDialogService } from "../../../../common/components/error-dialog/error-dialog.service";
import { CkeditorAugmentedTextareaComponent } from "core-app/ckeditor/ckeditor-augmented-textarea.component";
import { ProjectsService } from "../../../../projects/services/projects.service";
import { Observable, forkJoin } from 'rxjs';
import * as moment from 'moment';
import { AvpNotificationService } from "../../../../avp-base/notifications/avp-notifications.service";
import { NotificationType } from "../../../../common/consts/avp";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { UserCacheService } from "core-components/user/user-cache.service";
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';

@Component({
  templateUrl: './task-create-form.component.html',
  selector: 'task-create-form'
})
export class TaskCreateFormComponent implements OnInit {
  @Input() projectId: number;
  @Input() categoryId: number = 0;
  @Input() parentId: number;
  @Input() isCopy: boolean = false;
  @Input() isCopyToOtherProject: boolean = false;
  @Input() itemToCopied: any;
  @Output() onDialogClose: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSaveSuccess: EventEmitter<any[]> = new EventEmitter<any[]>();
  @ViewChild('ckeditor') ckeditor: CkeditorAugmentedTextareaComponent;

  public newTask: any = {
    title: '',
    description: '',
    assignees: [],
    project_id: 0,
    category_id: 0,
    deadline: null,
    followers: {
      isShowOrCollapse: false,
      list: []
    },
    tags: {
      isShowOrCollapse: false,
      list: []
    },
    attachments: [],
    attachmentsInDescription: []
  };
  public categories: any[] = [];
  public isCopySubTasks: boolean = true;
  public projects: any[] = [];

  public assigneeIds: any[] = [];
  public followerIds: any[] = [];

  public onSaving: boolean = false;

  private currentUser: any;
  private itemHalResource: any;
  private createdWpResources: any[] = [];

  constructor(private taskService: TasksService,
    private projectService: ProjectsService,
    private notifyService: AvpNotificationService,
    private userService: CurrentUserService,
    private userCacheService: UserCacheService,
    private halResourceService: HalResourceService,
    private logAdapter: LogAdapter,
    private errorDialogService: ErrorDialogService) { }

  async ngOnInit() {
    if (this.isCopy) {
      this.loadCopiedItem();
    }

    if (this.categoryId != 0) {
      this.newTask.category_id = this.categoryId;
    }
    else {
      await this.loadCategories();
    }

    this.loadCurrentUser();
  }

  public loadCopiedItem() {
    this.itemHalResource = this.halResourceService.createHalResource(this.itemToCopied, true);
    this.newTask.title = `Bản sao của ${this.itemHalResource.subject}`.trim();
    this.newTask.description = this.itemHalResource.description.raw;
    this.newTask.assignees = this.itemHalResource.assignee == undefined ? [] : [this.itemHalResource.assignee];
    this.newTask.project_id = this.itemHalResource.project.id;
    this.projectId = parseInt(this.itemHalResource.project.id);
    this.newTask.category_id = this.itemHalResource.category == undefined ? 0 : this.itemHalResource.category.id;
    this.newTask.followers.list = this.itemHalResource.watchers.elements;
    this.newTask.followers.isShowOrCollapse = this.newTask.followers.list.length > 0;
    this.newTask.deadline = this.convertToDateModel(this.itemHalResource.dueDate);

    if (this.isCopyToOtherProject) {
      this.projectService.getAll().subscribe(
        (responses: any[]) => {
          this.projects = responses;
        }
      )
    }

    this.assigneeIds = this.newTask.assignees.map((user: any) => user.id);
    this.followerIds = this.newTask.followers.list.map((user: any) => user.id);
  }

  onAssignTo(e: any) {
    this.newTask.assignees = e;
  }

  onAddFollowers(e: any) {
    this.newTask.followers.list = e;
  }

  onAttachmentsChanged(e: any) {
    let attachments = Array.from(e.target.files);
    attachments.forEach((attachment: any) => {
      attachment.description = {
        format: "plain",
        html: "",
        raw: "AttachedFile"
      }
    });
    this.newTask.attachments = this.newTask.attachments.concat(attachments);
  }

  onRemoveAttachment(e: any) {
    let removedIndex = this.newTask.attachments.indexOf(e);
    this.newTask.attachments.splice(removedIndex, 1);
  }

  onSaveTask() {
    this.onSaving = true;
    if (this.newTask.title == '') {
      this.errorDialogService.show('Tên công việc còn trống');
      this.onSaving = false;
      return;
    }

    let isMultiple = this.newTask.assignees.length > 1;

    let wps: any[] = [];
    if(this.newTask.assignees.length == 0) {
      let payload = this.getTaskToBeSaved(undefined);
      wps = [payload];
    }
    else {
      wps = this.newTask.assignees.map((assignee: any) => {
        return this.getTaskToBeSaved(assignee, isMultiple);
      });
    }
    
    this.createdWpResources = [];
    this.onSaveSingleTask(0, wps);
  }

  onSaveSingleTask(index: number, listWps: any[]) {
    let payload = listWps[index];

    this.taskService.create(payload, this.projectId).toPromise()
    .then((response: any) => {
      let halResource = this.halResourceService.createHalResource(response, true);
      this.onCreateTaskSuccess(halResource);
      this.createdWpResources.push(response);

      if(index < listWps.length - 1) {
        setTimeout(() => {
          index += 1;
          this.onSaveSingleTask(index, listWps);
        }, 500);
      }
      else if (index == listWps.length - 1) {
        this.onSaved();
      }
    })
    .catch((error: any) => {
      console.log(error);
      this.errorDialogService.show("Lỗi khi tạo công việc: " + payload.subject);
    })
  }

  onShowOrCollapseTags() {
    this.newTask.tags.isShowOrCollapse = !this.newTask.tags.isShowOrCollapse;
  }

  onShowOrCollapseFollowers() {
    this.newTask.followers.isShowOrCollapse = !this.newTask.followers.isShowOrCollapse;
  }

  onClose() {
    window.location.reload();
    this.onDialogClose.emit();
  }

  public get title() {
    return this.isCopy === true ? "Nhân bản công việc" : "Tạo công việc mới";
  }

  private async loadCategories() {
    const response = await this.projectService.getCategories(this.projectId);
    this.categories = response._embedded.elements;
  }

  private loadCurrentUser() {
    this.userCacheService
      .require(this.userService.userId)
      .then((user: any) => {
        this.currentUser = user;
      });
  }

  private onCreateTaskSuccess(halResource: any) {
    this.notifyWPCreated(halResource);

    let uploadObservable = this.onUploadAttachments(halResource);
    let saveWatchersObservables = this.isCopy && this.isCopyToOtherProject ? [] : this.onSaveWatchers(halResource);
    let copySubTasksObservable = this.isCopy && this.isCopySubTasks ? this.onCopySubTasks(halResource) : null;
    let observables: any[] = [];
    if (copySubTasksObservable != null) {
      observables.push(copySubTasksObservable);
    }
    if (uploadObservable != null) {
      observables.push(uploadObservable);
    }
    if (saveWatchersObservables.length > 0) {
      observables = observables.concat(saveWatchersObservables);
    }

    return observables;
  }

  private onSaveWatchers(halResource: any): Observable<any>[] {
    let observables: Observable<any>[] = [];
    this.newTask.followers.list.forEach((user: any) => {
      let payload = {
        user: {
          href: `/api/v3/users/${user.id}`
        }
      };

      observables.push(halResource.addWatcher(payload));
    });
    return observables;
  }

  private onUploadAttachments(halResource: any) {
    if (this.newTask.attachments.length > 0) {
      return halResource.uploadAttachments(this.newTask.attachments);
    }
    return null;
  }

  private onCopySubTasks(halResource: any) {
    if (this.itemHalResource.children &&
      this.itemHalResource.children.length > 0) {
      return this.taskService.copySubTask({
        projectId: this.projectId,
        sourceId: this.itemHalResource.id,
        targetId: halResource.id
      }).then((response: any) => {
        // DO NOTHING
      });
    }
    return null;
  }

  private getTaskToBeSaved(assignee: any, manyAssignee: boolean = false): any {
    let newModel: WorkPackagePayload = {
      _links: {
        assignee: {
          href: assignee ? `/api/v3/users/${assignee.id}` : undefined
        },
        category: {
          href: this.newTask.category_id == 0 ? undefined : `/api/v3/categories/${this.newTask.category_id}`
        },
        project: {
          href: `/api/v3/projects/${this.projectId}`
        },
        status: {
          href: "/api/v3/statuses/1"
        },
        type: {
          href: "/api/v3/types/1"
        },
        parent: {
          href:  this.parentId ? `/api/v3/work_packages/${this.parentId}`: undefined
        }
      },
      description: {
        format: "markdown",
        raw: this.ckeditor.getContent()
      },
      subject: manyAssignee == false ? this.newTask.title : `[${this.newTask.title}]--${assignee.name}`,
      dueDate: this.newTask.deadline == null ? undefined : moment(`${this.newTask.deadline.year}/${this.newTask.deadline.month}/${this.newTask.deadline.day}`).format('YYYY-MM-DD')
    };
    return newModel;
  }

  private convertToDateModel(dateString: string) {
    if (dateString == null) return null;

    let date = new Date(dateString);
    return {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    }
  }

  private onSaved() {
    this.onSaving = false;
    this.errorDialogService.showSuccess();
    this.onSaveSuccess.emit(this.createdWpResources);
    this.createdWpResources.forEach((wp: any) => {
      this.logAdapter.logging(ActivityActionType.CreateTask, this.projectId, wp.subject);
    });
    this.onClose();
  }

  private notifyWPCreated(workPackage: any) {
    if (workPackage.assignee) {
      if(workPackage.assignee.id != this.currentUser.id) {
        let notifyData: any = {
          author: this.currentUser,
          receivers: [workPackage.assignee.email],
          wp: workPackage
        };
        this.notifyService.notify(NotificationType.WP_CREATE, notifyData);
      }
    }
  }
}

interface WorkPackagePayload {
  _links?: {
    self?: {
      href?: string
    },
    attachments?: any[],
    assignee?: {
      href?: string
    },
    category?: {
      href?: string
    },
    parent?: {
      href?: string,
      title?: string,
      uncacheable?: boolean
    },
    responsible?: {
      href?: string
    },
    project?: {
      href?: string
    },
    status?: {
      href?: string
    },
    type?: {
      href?: string
    },
    version?: {
      href?: string
    }
  };
  subject?: string;
  description?: {
    format: "markdown",
    raw?: string,
    html?: string
  };
  startDate?: any;
  dueDate?: any;
  estimatedTime?: any;
  percentageDone?: number;
  customField2?: {
    format: "markdown",
    raw?: string, 
    html?: string
  };
  customField12?: boolean;
  customField13?: boolean;
  customField14?: boolean;
}