import { Component, OnInit, Input, ElementRef, Injector } from '@angular/core';
import { WorkPackageCommentFieldHandler } from "core-components/work-packages/work-package-comment/work-package-comment-field-handler";
import { TasksService } from "../../../../services/tasks.service";
import { AvpUtils } from "../../../../../common/services/avp-utils.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { UserCacheService } from "core-components/user/user-cache.service";
import * as moment from 'moment';
import { AVP, NotificationType } from '../../../../../common/consts/avp';
import { AvpNotificationService } from '../../../../../avp-base/notifications/avp-notifications.service';

@Component({
  templateUrl: './comment.component.html',
  selector: 'comment'
})
export class CommentComponent extends WorkPackageCommentFieldHandler implements OnInit {
  @Input() workPackage: any;
  @Input() activity: any = {};

  public fieldLabel: string;
  public user: any;

  constructor(readonly elementRef: ElementRef,
    readonly injector: Injector,
    private notificationService: AvpNotificationService,
    readonly userCacheService: UserCacheService,
    private taskService: TasksService,
    private avpUtils: AvpUtils,
    private currentUser: CurrentUserService) {
    super(elementRef, injector);
  }

  public ngOnInit() {
    super.ngOnInit();

    this.userCacheService
      .require(this.activity._links.user.href.split('/').pop()!)
      .then((user: any) => {
        this.user = user;
      });
  }

  public activate() {
    super.activate(this.activity.comment.raw);
  }

  public handleUserSubmit() {
    let payload = {
      "comment": this.rawComment
    };
    return this.taskService.updateComment(this.activity.id, payload)
      .then((response: any) => {
        this.activity = response;
        this.doNotifyForTagged(response);
        this.deactivate(true);
      });
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private doNotifyForTagged(resource: any) {
    var input = resource.comment.raw;
    var regex = /(?:tag#([a-zA-Z0-9_.\-@]+))/g;

    var matches: any;
    var tagged: string[] = [];
    while (matches = regex.exec(input)) {
      if (!tagged.includes(matches[1]) && matches[1] != this.user.email) tagged.push(matches[1]);
    }

    let notifyData: any = {
      author: this.user,
      wp: this.workPackage,
      receivers: tagged
    }

    this.notificationService.notify(NotificationType.WP_COMMENT_TAGGED, notifyData);
  }

  public get currentUserId() {
    return this.currentUser.userId;
  }

  public get htmlId() {
    return `comment_${this.activity.id}`;
  }

  public get createdAt() {
    return moment(this.activity.createdAt).format('HH:m DD/MM/YYYY');
  }
}