import { Component, OnInit, Input, ElementRef, Injector, OnChanges, Output, EventEmitter } from '@angular/core';
import { TasksService } from "../../../services/tasks.service";
import { WorkPackageCommentFieldHandler } from "core-components/work-packages/work-package-comment/work-package-comment-field-handler";
import { UserCacheService } from "core-components/user/user-cache.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { AvpNotificationService } from '../../../../avp-base/notifications/avp-notifications.service';
import { NotificationType } from '../../../../common/consts/avp';

@Component({
  templateUrl: './comments.component.html',
  selector: 'comments'
})
export class CommentsComponent extends WorkPackageCommentFieldHandler implements OnInit {

  @Input() workPackage: any;
  @Input() activities: any[] = [];
  @Output() loadSuccess: EventEmitter<any> = new EventEmitter();

  public comments: any[] = [];
  public fieldLabel: string;
  public loading: boolean = true;

  private currentUser: any;

  constructor(readonly userCacheService: UserCacheService,
    private userService: CurrentUserService,
    private notificationService: AvpNotificationService,
    readonly elementRef: ElementRef,
    readonly injector: Injector,
    private taskService: TasksService) {
    super(elementRef, injector);
  }

  public ngOnInit() {
    super.ngOnInit();
    this.filterComments(this.activities);
    this.loadCurrentUser();
  }

  public handleUserSubmit() {
    return this.workPackage.addComment(
      { comment: this.commentValue },
      { 'Content-Type': 'application/json; charset=UTF-8' }
    )
      .then(async (response: any) => {
        console.log(response);
        this.doNotifyForComment(response);
        this.deactivate(true);
        await this.loadComments();
      })
      .catch(this.errorHandle.bind(this));
  }

  public get htmlId() {
    return `user_activity_edit_field${this.workPackage.id}`;
  }

  public activate() {
    super.activate();
  }

  public get orderedComments() {
    return this.comments.sort((cmtA: any, cmtB: any) => {
      if (cmtA.id > cmtB.id) return 1;
      else if (cmtA.id < cmtB.id) return -1;
      return 0;
    })
  }

  private async loadComments() {
    this.loading = true;
    let response = await this.taskService.getActivities(this.workPackage.id);
    this.filterComments(response._embedded.elements);
  }

  private filterComments(activities: any[]) {
    this.comments = activities.filter((item: any) => {
      return item._type === 'Activity::Comment';
    });
    this.loading = false;
  }

  private errorHandle(error: any) {
    console.error(error);
    this.loading = false;
  }

  private doNotifyForComment(resource: any) {
    var input = this.commentValue.raw;
    var regex = /(?:tag#([a-zA-Z0-9_.\-@]+))/g;

    var matches: any;
    var tagged: string[] = [];
    while (matches = regex.exec(input)) {
      if (!tagged.includes(matches[1]) && matches[1] != this.currentUser.email) tagged.push(matches[1]);
    }

    this.doNotifyForTagged(tagged, resource);

    let receivers = this.notifyReceivers(this.workPackage, tagged);

    let notifyData: any = {
      author: this.currentUser,
      wp: this.workPackage,
      receivers: receivers
    }

    this.notificationService.notify(NotificationType.WP_COMMENT, notifyData);
  }

  private doNotifyForTagged(receivers: string[], resource: any) {
    let notifyData: any = {
      author: this.currentUser,
      wp: this.workPackage,
      receivers: receivers
    }

    this.notificationService.notify(NotificationType.WP_COMMENT_TAGGED, notifyData);
  }

  private notifyReceivers(workPackage: any, ignores: string[]) {
    let assigneeEmail = workPackage.assignee ? workPackage.assignee.email : "";
    let receiverEmails = workPackage.watchers ? workPackage.watchers.elements.map((user: any) => user.email) : [];

    let authorEmail = workPackage.author.email;

    if (!receiverEmails.includes(assigneeEmail) && assigneeEmail) {
      receiverEmails.push(assigneeEmail);
    }

    if (!receiverEmails.includes(authorEmail) && authorEmail) {
      receiverEmails.push(authorEmail);
    }

    return receiverEmails.filter((email: string) => {
      return email != this.currentUser.email && !ignores.includes(email);
    });
  }

  private loadCurrentUser() {
    this.userCacheService
      .require(this.userService.userId)
      .then((user: any) => {
        this.currentUser = user;
      });
  }
}