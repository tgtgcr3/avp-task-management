import { Component, Input, OnInit, OnChanges, ViewChild, OnDestroy } from '@angular/core'
import { TasksService } from '../../../services/tasks.service';
import * as moment from 'moment';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { LogAdapter, ActivityActionType } from 'core-app/modules/activity/activity_log.adapter';
import { ErrorDialogService } from "../../../../common/components";
import { NgbModal, NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { WorkPackageResource } from "core-app/modules/hal/resources/work-package-resource";
import { componentDestroyed } from 'ng2-rx-componentdestroyed';
import { takeUntil } from 'rxjs/operators';
import { AVP, NotificationType } from '../../../../common/consts/avp';
import { AvpNotificationService } from '../../../../avp-base/notifications/avp-notifications.service';
import { UserCacheService } from "core-components/user/user-cache.service";
import { PermissionService } from '../../../../common/services/permission.service';

@Component({
  templateUrl: './task-side-bar.component.html',
  selector: 'task-side-bar'
})
export class TaskSideBarComponent implements OnInit, OnDestroy {
  @Input() item: any;
  @Input() activities: any[];

  @ViewChild('watchersSelector') watchersSelector: NgbPopover;
  @ViewChild('userSelector') internalUserSelector: NgbPopover;

  public side_menu_action: any = {
    author_section: {
      isCollapsed: false,
    },
    assignee_section: {
      isShowOrCollapseAssign: false
    },
    milestone_section: {
      isShowOrCollapseMilestone: false
    },
    starttime_section: {
      isShowOrCollapseStartTime: false
    },
    follower_section: {
      isCollapsed: false,
      isShowOrCollapseFollowers: false,
      isShowOrCollapseExternalFollowers: false
    },
    log_section: {
      isCollapsed: true
    }
  };

  public journals: any[] = [];
  public watchers: any[] = [];
  public comments: any[] = [];

  public loaded: boolean = false;

  public startedSince: number = 0;
  public createdAt: string = '';
  public updatedAt: string = '';
  public startDate: string = '';
  public dueDate: string = '';
  public isOverdue: boolean = false;
  public isAssignedToMe: boolean = false;
  public isAuthor: boolean = false;
  public isWatcher: boolean = false;

  private workPackage: WorkPackageResource;
  private currentUser: any;

  constructor(private taskService: TasksService,
    private wpCacheService: WorkPackageCacheService,
    private currentUserService: CurrentUserService,
    private permissionService: PermissionService,
    readonly userCacheService: UserCacheService,
    private notificationService: AvpNotificationService,
    private logAdapter: LogAdapter,
    private modalService: NgbModal,
    private errorDialogService: ErrorDialogService) { }

  ngOnInit() {
    this.taskService.onWatcherUpdatedEvent.subscribe(this.loadWatchers.bind(this));
    this.loadCurrentUser();
    this.loadRelatedData();
  }

  ngOnDestroy(): void {
  }

  loadRelatedData() {
    this.loaded = false;
    this.wpCacheService.loadWorkPackage(this.item.id).values$()
      .pipe(
        takeUntil(componentDestroyed(this))
      )
      .subscribe((wp: WorkPackageResource) => {
        this.workPackage = wp;
        this.startedSince = this.workPackage.startDate != '' ? moment().diff(moment(this.workPackage.startDate).format('MM/DD/YYYY'), 'days') : 0;
        this.createdAt = moment(this.workPackage.createdAt).format('HH:mm DD/MM/YYYY');
        this.updatedAt = moment(this.workPackage.updatedAt).format('HH:mm DD/MM/YYYY');
        this.startDate = moment(this.workPackage.startDate).format('DD/MM/YYYY');
        this.dueDate = moment(this.workPackage.dueDate).format('DD/MM/YYYY');

        if (this.workPackage.dueDate) {
          let dueDate = new Date(this.workPackage.dueDate);
          dueDate.setDate(dueDate.getDate() + 1);
          let date = new Date();
          this.isOverdue =  dueDate.getTime() < date.getTime() &&
            !this.workPackage.status.name.toLowerCase().includes('done');
        }

        this.isAssignedToMe = this.workPackage.assignee ? this.currentUserService.userId == this.workPackage.assignee.id : false;
        this.isAuthor = this.workPackage.author.id == this.currentUserService.userId;

        this.item = wp.$source;
        this.loaded = true;
      });
    this.loadActivities();
    this.loadWatchers();
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public get projectId() {
    return this.workPackage.project.id;
  }

  public onUpdateStartDate(evt: any, popup: any, today: boolean = false) {
    if(!this.validateAction('EDIT_TIME')) {
      return false;
    }

    evt = today == true ? moment().format('YYYY-MM-DD') : evt;

    if (!this.isValidDate(evt, this.workPackage.dueDate)) {
      this.errorDialogService.show('Ngày Bắt Đầu phải bé hơn Ngày Kết Thúc!');
      return false;
    }

    let payload = {
      _links: {},
      startDate: evt ? evt : null,
      lockVersion: this.workPackage.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_START_DATE, notifyData);
    });

    if (popup) {
      popup.close();
    }
    return true;
  }

  private isValidDate(startDate: any, dueDate: any) {
    if (!startDate || !dueDate) {
      return true;
    }
    return moment(startDate).isSameOrBefore(moment(dueDate));
  }

  public onAddOrRemoveWatcher(watcherId: any) {
    this.watchersSelector.isOpen() ? this.watchersSelector.close() : '';

    if (!this.validUser) {
      this.errorDialogService.show('Người dùng không thể chỉnh sửa công việc');
      return;
    }

    let existingWatcher = this.watchers.filter((value: any) => {
      return value.id == watcherId;
    });

    let promise = null;
    let watcher = {
      href: `/api/v3/users/${watcherId}`,
      id: watcherId
    };

    if (existingWatcher.length == 0) { // Add watcher
      promise = this.workPackage.addWatcher.$link.$fetch({ user: { href: watcher.href } })
        .then((response: any) => {
          this.watchers.push(response.$source);
          let notifyData: any = {
            receivers: this.notifyReceivers(this.workPackage),
            author: this.currentUser,
            wp: this.workPackage,
            watchers: [response.name]
          }
          this.errorDialogService.showSuccess();
          this.logAdapter.logging(ActivityActionType.UpdateTask, this.workPackage.project.id, this.workPackage.subject);
          this.notificationService.notify(NotificationType.WP_WATCHER, notifyData);
        })
        .catch((error: any) => {
          this.errorDialogService.show('Xảy ra lỗi khi lưu');
        });
    }
    else { // Remove watcher
      promise = this.workPackage.removeWatcher.$link.$prepare({ user_id: watcher.id })()
        .then((response: any) => {
          let index = this.watchers.indexOf(existingWatcher[0]);
          this.errorDialogService.showSuccess();
          this.watchers.splice(index, 1);
        })
        .catch((error: any) => {
          this.errorDialogService.show('Xảy ra lỗi khi lưu');
        });
    }
  }

  public onOpenExternalUserSelector(modal: any) {
    this.internalUserSelector.close();
    this.modalService.open(modal);
  }

  public onAssigneeChanged(assigneeId: number) {
    if(!this.validateAction('EDIT_ASSIGNEE')) {
      return false;
    }

    this.internalUserSelector.isOpen() ? this.internalUserSelector.close() : '';
    let payload = {
      _links: {
        assignee: {
          href: assigneeId == 0 ? null : `/api/v3/users/${assigneeId}`
        }
      },
      lockVersion: this.item.lockVersion
    };
    this.onSave(payload, (workPackage: any) => {
      let notifyData: any = {
        receivers: this.notifyReceivers(workPackage),
        author: this.currentUser,
        wp: workPackage
      }

      this.notificationService.notify(NotificationType.WP_ASSIGNED, notifyData);
    });
    return true;
  }

  onSectionChanged(section: string) {
    let isCollapsed = this.side_menu_action[section].isCollapsed;
    this.side_menu_action[section].isCollapsed = !isCollapsed;
  }

  private onSave(payload: any, callback: any) {
    this.workPackage.updateImmediately(payload)
      .then((response: any) => {
        this.wpCacheService.updateWorkPackage(response, true);
        this.errorDialogService.showSuccess();
        this.logAdapter.logging(ActivityActionType.UpdateTask, response.project.id, response.subject);
        if (callback) callback(response);
      })
      .catch((error: any) => {
        this.errorDialogService.show('Xảy ra lỗi khi lưu');
      });
  }
  
  private get isOwner() {
    return this.currentUser.owner;
  }

  private get validUser() {
    return this.isAuthor || this.isAssignedToMe || this.isOwner;
  }

  private loadWatchers() {
    this.taskService.getDataByUrl(this.item._links.watchers.href).subscribe(
      (response: any) => {
        this.watchers = response._embedded.elements;
        this.isWatcher = this.watchers.some((user: any) => user.id == this.currentUserService.userId);
      },
      (error: any) => {
      });
  }

  private loadCurrentUser() {
    this.userCacheService
      .require(this.currentUserService.userId)
      .then((user: any) => {
        this.currentUser = user;
      });
  }

  private notifyReceivers(workPackage: any) {
    let assigneeEmail = workPackage.assignee ? workPackage.assignee.email : "";
    let receiverEmails = workPackage.watchers ? workPackage.watchers.elements.map((user: any) => user.email) : [];
    let authorEmail = workPackage.author.email;

    if (!receiverEmails.includes(assigneeEmail) && assigneeEmail) {
      receiverEmails.push(assigneeEmail);
    }

    if (!receiverEmails.includes(authorEmail) && authorEmail) {
      receiverEmails.push(authorEmail);
    }

    return receiverEmails.filter((email: string) => {
      return email != this.currentUser.email;
    });
  }

  private loadActivities() {
    this.journals = this.activities.filter((value: any, index: number) => {
      if(index == 0) {
        value.details = [];
        value.details.push({
          html: "Công việc được tạo"
        });
      }
      return value._type == 'Activity' && value.details.length != 0;
    }).sort((a: any, b: any) => { return a.createdAt > b.createdAt ? -1 : 1; });

    this.journals.forEach((journal: any) => {
      journal.createdAt = moment(journal.createdAt).format('HH:mm DD/MM/YYYY');
    })

    this.comments = this.activities.filter((value: any) => {
      return value._type == 'Activity::Comment';
    })
  }

  private validateAction(action: string) {
    if(!this.permissionService.canAction(this.projectId, 
      this.isAuthor ? 'AUTHOR' : this.isAssignedToMe ? 'ASSIGNEE' : this.isWatcher ? 'WATCHER' : 'OTHER',
      action)) {

      this.errorDialogService.show('Người dùng không thể chỉnh sửa công việc!');
      return false;
    }

    return true;
  }
}