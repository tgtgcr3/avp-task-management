import { Component, OnInit, OnDestroy } from '@angular/core'
import { TasksService } from '../../services/tasks.service';
import { Subscription, Observable } from 'rxjs';
import { LoaderService } from "../../../common/components";
import { StateService, TransitionService, Transition } from '@uirouter/core';
import { AVP_VIEWS } from "../../../common/consts/view-name";
import { WorkPackageResource } from "core-app/modules/hal/resources/work-package-resource";
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";


@Component({
  templateUrl: './tasks.component.html',
  selector: 'task-list'
})
export class TasksComponent implements OnInit, OnDestroy {

  public query: any = {
    pageSize: 15,
    pageIndex: 1
  };
  public queryName: string;
  public displayType: string = 'sub-tasks';
  public subjectOrId: string = "subjectOrId";
  public values: string = "values";
  public tasks: any[] = [];

  private onFiltersSubscription: Subscription;
  private onSearchSubscription: Subscription;
  private onRefreshSubscription: Subscription;
  private loadTasksSubscription: Subscription;
  private onViewChangedSubscription: Subscription;

  private searchFilters: any[] = [];
  private currentFilters: any[] = [{ 'assigneeOrAuthor': { 'operator': '=', 'values': ['me'] } }];
  private orderBy: any[] = [["updatedAt", "desc"]];
  private _tasks: any[] = [];

  constructor(private tasksService: TasksService,
    private loaderService: LoaderService,
    readonly $state: StateService,
    private halResourceService: HalResourceService,
    private wpCacheService: WorkPackageCacheService,
    readonly trans: TransitionService) { }

  ngOnInit() {
    this.onFiltersSubscription = this.tasksService.onTaskRefreshEvent.subscribe(this.refreshWithParams.bind(this));
    this.onSearchSubscription = this.tasksService.onSearchTaskEvent.subscribe(this.onSearch.bind(this));
    this.onRefreshSubscription = this.tasksService.refreshRequest.subscribe(this.refresh.bind(this));
    this.onViewChangedSubscription = this.tasksService.onViewChangedEvent.subscribe(this.changeDisplay.bind(this));
    this.tasksService.onTaskCreatedEvent.subscribe(this.addTask.bind(this));

    if (this.$state.params['page']) {
      this.query.pageIndex = this.$state.params['page'];
    }

    this.trans.onSuccess({}, (transition: Transition) => {
      let param = transition.params('to');
      if (param.page && param.page != this.query.pageIndex) {
        if (this.$state.params['page']) {
          this.query.pageIndex = this.$state.params['page'];
        }
        this.refresh();
      }
    });

    //this.refresh();
  }

  ngOnDestroy() {
    if (this.loadTasksSubscription) {
      this.loadTasksSubscription.unsubscribe();
    }

    this.onFiltersSubscription.unsubscribe();
    this.onSearchSubscription.unsubscribe();
    this.onRefreshSubscription.unsubscribe();
    this.onViewChangedSubscription.unsubscribe();
  }

  public async refresh() {
    if (this.loadTasksSubscription) {
      this.loadTasksSubscription.unsubscribe();
    }

    let observable = new Observable<any>();

    this.loaderService.loading(true, 'Đang tải');

    if (this.$state.current.name == AVP_VIEWS.TASKS_FOLLOWING) {
      observable = this.tasksService.getWatching(this.query.pageSize, this.query.pageIndex);
    }
    else if (this.$state.current.name == AVP_VIEWS.TASKS_FILTERS) {
      let queryId = this.$state.params['queryId'];
      let filters = await this.tasksService.getFilterProps(queryId);

      observable = this.tasksService.getByFilters(this.query.pageSize, this.query.pageIndex, filters);
    }
    else if (this.$state.current.name == AVP_VIEWS.TASKS_TEAM) {
      this.displayType = 'all';
      let filters = this.currentFilters.concat(this.searchFilters);
      let assigneeFilter = filters.find((filter: any) => filter['assignee'] != null);
      if(assigneeFilter && assigneeFilter['assignee']['values'].length > 0) {
        observable = this.tasksService.getByFilters(this.query.pageSize, this.query.pageIndex, filters, this.orderBy);
      }
      else {
        this.refineData({'_embedded': {'elements':[]}});
        return;
      }
    }
    else {
      let filters = this.currentFilters.concat(this.searchFilters);
      observable = this.tasksService.getByFilters(this.query.pageSize, this.query.pageIndex, filters, this.orderBy);
    }

    this.loadTasksSubscription = observable.subscribe(
      this.refineData.bind(this),
      (error: any) => {
        this.loaderService.loading(false);
      });
  }

  public get taskId() {
    return this.$state.params['task'];
  }

  public get currentState() {
    return this.$state.current.name;
  }

  onActionBarInitialized(event: any) {
    this.refreshWithParams(event);
  }

  public refreshWithParams(params: any) {
    this.currentFilters = params.filters;
    this.orderBy = params.orderBy;

    this.refresh();
  }

  private changeDisplay(params: any) {
    if (params.displayType != "") {
      this.displayType = params.displayType;
      this.updateTaskList();
    }
  }

  private updateTaskList() {
    if (this.isShowSubtask || this.noSubTasks) {
      this.tasks = this._tasks.filter((task: any) => {
        return task._links.parent.href == null;
      });
      return;
    }

    this.tasks = this._tasks;
  }

  private refineData(response: any) {
    this.updateCache(response["_embedded"]["elements"]);
    this._tasks = response["_embedded"]["elements"];
    this.updateTaskList();
    this.loaderService.loading(false);
  }

  private updateCache(workPackages: any[]) {
    workPackages.forEach((item: any) => {
      let wp = this.halResourceService.createHalResource<WorkPackageResource>(item);
      this.wpCacheService.updateWorkPackage(wp, true);
    });
  }

  private addTask(newWPs: any[]) {
    this._tasks.unshift(newWPs.map((wp: any) => wp.$source));
    this.updateTaskList();
  }

  private onSearch(params: any) {
    this.searchFilters = params.filters;
    if (params.filters[0]['subjectOrId']['values'] == '') {
      this.searchFilters = [];
    }
    this.refresh();
  }

  private get isShowSubtask() {
    return this.displayType == 'sub-tasks';
  }

  private get noSubTasks() {
    return this.displayType == 'no-sub-tasks';
  }
}
