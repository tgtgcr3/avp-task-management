import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as AVP from '../../../../common/consts/avp';
import { ProjectsService } from '../../../../projects/services/projects.service';

@Component({
  templateUrl: './my-employee-action-bar.component.html',
  selector: 'my-employee-action-bar'
})
export class MyEmployeeActionBarComponent implements OnInit {

  @Output() refresh: EventEmitter<any> = new EventEmitter();
  @Output() orderChange: EventEmitter<any> = new EventEmitter();

  public loaded: boolean = false;

  public hoveredDate: any = null;

  public orders: any[] = [
    {
      value: "work_package",
      label: "Số lượng công việc",
      isDefault: true
    },
    {
      value: "rate",
      label: "Tỉ lệ hoàn thành"
    }
  ];

  public by: any[] = [
    {
      value: "created_at",
      label: "Theo ngày tạo",
      isDefault: true
    },
    {
      value: "start_date",
      label: "Theo ngày bắt đầu"
    },
    {
      value: "due_date",
      label: "Theo thời hạn"
    }
  ]

  public projects: any[] = [];

  public selected: any = {
    fromDate: {},
    toDate: {},
    by: this.by[0],
    project: AVP.FILTER_PROJECT_DEFAULT,
    ordering: this.orders[0]
  };

  public get filteredDate() {
    let from = this.selected.fromDate;
    let to = this.selected.toDate;

    if (from && to) {
      return `${from.day}/${from.month}/${from.year} đến ${to.day}/${to.month}/${to.year} `;
    }

    return "";
  }

  constructor(private projectService: ProjectsService) { }

  ngOnInit() {
    this.initFromTo();
    this.loadProjects();
    this.refresh.emit(this.selected);
  }

  public onDateSelection(date: any) {
    if (!this.selected.fromDate && !this.selected.toDate) {
      this.selected.fromDate = date;
    } else if (this.selected.fromDate && !this.selected.toDate && date && date.after(this.selected.fromDate)) {
      this.selected.toDate = date;
    } else {
      this.selected.toDate = null;
      this.selected.fromDate = date;
    }

    if (this.selected.fromDate && this.selected.toDate) {
      this.refresh.emit(this.selected);
    }
  }

  public isHovered(date: any) {
    return this.selected.fromDate && !this.selected.toDate && this.hoveredDate && date.after(this.selected.fromDate) && date.before(this.hoveredDate);
  }

  public isInside(date: any) {
    return this.selected.toDate && date.after(this.selected.fromDate) && date.before(this.selected.toDate);
  }

  public isRange(date: any) {
    return date.equals(this.selected.fromDate) || (this.selected.toDate && date.equals(this.selected.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onChangeBy(by: any) {
    this.selected.by = by;
    this.refresh.emit(this.selected);
  }

  onChangeProject(project: any) {
    this.selected.project = project;
    this.refresh.emit(this.selected);
  }

  onChangeOrder(order: any) {
    this.selected.ordering = order;
    this.orderChange.emit(this.selected);
  }

  private initFromTo() {
    let date = new Date();
    this.selected.toDate = {
      "day": date.getDate(),
      "month": date.getMonth() + 1,
      "year": date.getFullYear()
    };

    date.setMonth(date.getMonth() - 1);
    this.selected.fromDate = {
      "day": date.getDate(),
      "month": date.getMonth() + 1,
      "year": date.getFullYear()
    };
  }

  private loadProjects() {
    this.loaded = false;
    this.projectService.getPosibleProjects()
    .subscribe((responses: any[]) => {
      let projects = [AVP.FILTER_PROJECT_DEFAULT]
      projects = projects.concat(responses.map((project: any) => { 
        return { value: project.id, label: project.name };
      }));
      this.projects = projects;
      this.selected.project = this.projects[0];
      this.loaded = true;
    });
  }
}