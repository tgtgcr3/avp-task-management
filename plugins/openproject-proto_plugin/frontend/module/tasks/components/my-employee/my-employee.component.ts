import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { LoaderService } from '../../../common/components';
import { AVP } from '../../../common/consts/avp';
import { TasksService } from '../../services/tasks.service';


@Component({
  templateUrl: './my-employee.component.html',
  styleUrls: ['./my-employee.component.scss'],
  selector: 'my-employee'
})
export class MyEmployeeComponent implements OnInit {

  public employees: any[] = [];
  public loaded: boolean = false;
  public reports: any[] = [];

  private filter: any = {};

  constructor(private taskService: TasksService, private loaderService: LoaderService) { }

  async ngOnInit() {
    await this.loadEmployees();
  }

  onRefresh(event: any) {
    let formattedFrom = moment(`${event.fromDate.month}/${event.fromDate.day}/${event.fromDate.year}`).startOf('date').toISOString();
    let formattedTo =  moment(`${event.toDate.month}/${event.toDate.day}/${event.toDate.year}`).endOf('date').toISOString();
    this.filter = {
      fromDate: formattedFrom,
      toDate: formattedTo,
      by: event.by.value,
      projectId: event.project.value == 'all' ? null : event.project.value,
      order: event.ordering.value
    }
    this.loadReport();
  }

  onOrderChanged(event: any) {
    this.filter.order = event.ordering.value;
    this.orderReport();
  }

  onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private loadReport() {
    if(this.employees.length > 0) {
      this.loaderService.loading(true);
      this.taskService.reportByEmployee(this.employees.map((user: any) => user.id),
                                         this.filter.by, this.filter.fromDate, this.filter.toDate,
                                         this.filter.projectId)
                      .subscribe((responses: any[]) => {
                        this.generateReport(responses);
                        this.loaderService.loading(false);
                      });
    }
    else {
      this.reports = [];
    }
  }

  private async loadEmployees() {
    this.loaded = false;
    const responses = await this.taskService.getMyEmployees();
    this.employees = responses;
    this.loaded = true;
  }

  private generateReport(workPackages: any[]) {
    this.reports = [];

    this.employees.forEach((employee: any) => {
      let eWorkPackages = workPackages.filter((wp: any) => {
        return wp.assigned_to.id == employee.id;
      });

      let projects = eWorkPackages.map((wp: any) => wp.project.id).filter((value, index, self) => self.indexOf(value) === index);
      let done = eWorkPackages.filter((wp: any) => {
        return wp.status.name.toLowerCase() == 'done';
      });

      let inProgress = eWorkPackages.filter((wp: any) => {
        return wp.status.name.toLowerCase().includes('progress');
      });

      let inReview = eWorkPackages.filter((wp: any) => {
        return wp.status.name.toLowerCase().includes('review');
      });

      let doneLate = eWorkPackages.filter((wp: any) => {
        return wp.status.name.toLowerCase().includes('done late');
      });

      let overdue = eWorkPackages.filter((wp: any) => {
        return wp.due_date && new Date(wp.due_date).getTime() < new Date().getTime() && !wp.status.name.toLowerCase().includes('done');
      });

      let rate = eWorkPackages.length == 0 ? 0 : ((done.length + doneLate.length) * 100 / eWorkPackages.length).toFixed(1);
      
      let eReport = {
        employee: employee,
        workPackage: eWorkPackages.length,
        project: projects.length,
        done: done.length,
        inProgress: inProgress.length,
        inReview: inReview.length,
        doneLate: doneLate.length,
        overdue: overdue.length,
        rate: rate
      }

      this.reports.push(eReport);
    });

    this.orderReport();
  }

  private orderReport() {
    if(this.filter.order == 'work_package') {
      this.reports = this.reports.sort((a, b) => b.workPackage - a.workPackage);
    }
    else if(this.filter.order == 'rate') {
      this.reports = this.reports.sort((a, b) => b.rate - a.rate);
    }
  }
}