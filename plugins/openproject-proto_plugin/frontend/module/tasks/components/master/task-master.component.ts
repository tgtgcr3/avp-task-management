import { Component, OnInit } from '@angular/core';
import { TasksService } from '../../services/tasks.service';
import { StateService } from '@uirouter/core';
import { AVP_VIEWS } from "../../../common/consts/view-name";
import { ErrorDialogService } from "../../../common/components";
import { FilterSetupService } from "../filter-setup/filter-setup.service";
import { UserCacheService } from "core-components/user/user-cache.service";
import { CurrentUserService } from 'core-components/user/current-user.service';

@Component({
  templateUrl: './task-master.component.html',
  selector: 'task-master'
})
export class TaskMasterComponent implements OnInit {

  public currentUser: any = {};
  public filters: any[] = [];
  public sideShowing: boolean = false;

  constructor(private taskService: TasksService,
    private errorDialogService: ErrorDialogService,
    private filterSetupService: FilterSetupService,
    private currentUserService: CurrentUserService,
    readonly userCacheService: UserCacheService,
    readonly $state: StateService) { }

  ngOnInit() {
    this.loadCurrentUser();
    this.loadFilters();
  }

  public get isTaskShowing() {
    return this.$state.params['task'] != null && this.$state.current.name != AVP_VIEWS.TASKS_CALENDAR;
  }

  public get noSidebar() {
    return this.$state.current.name == AVP_VIEWS.TASKS_CALENDAR
      || this.$state.current.name == AVP_VIEWS.TASKS_REPEATED
      || this.$state.current.name == AVP_VIEWS.TASKS_TEAM_REPORT;
  }

  public onFilterEvent(evt: any) {
    this.filterSetupService.open(evt,
      (response: any) => {
        // update filters list
        if (response.action == 'create') {
          this.filters.push(response.item);
        }
        else if (response.action == 'delete') {
          let removedItem = this.filters.filter((item: any) => {
            return item.queryId == response.item.id;
          })[0];

          let index = this.filters.indexOf(removedItem);
          this.filters.splice(index, 1);
        }
      },
      (message: any) => {
      });
  }

  private loadCurrentUser() {
    this.userCacheService
      .require(this.currentUserService.userId)
      .then((user: any) => {
        this.currentUser = user;
      })
      .catch(() => this.errorHandle.bind(this));
  }

  private loadFilters() {
    this.taskService.getDataByUrl('/api/v3/queries').subscribe(
      (response: any) => {
        this.filters = response['_embedded']['elements'].map((value: any) => {
          return { queryId: value.id, name: value.name };
        });
      },
      this.errorHandle.bind(this)
    );
  }

  private errorHandle(error: any) {
    console.error(error);
    this.errorDialogService.show(error.message);
  }


}