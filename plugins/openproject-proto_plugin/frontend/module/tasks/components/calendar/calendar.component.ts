import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import { TasksService } from "../../services/tasks.service";
import { Moment } from "moment";
import { ErrorDialogService, LoaderService } from "../../../common/components";
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { StateService, TransitionService, Transition } from '@uirouter/core';
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { WorkPackageResource } from "core-app/modules/hal/resources/work-package-resource";
import { AVP_VIEWS } from '../../../common/consts/view-name';

@Component({
  templateUrl: './calendar.component.html',
  selector: 'calendar'
})
export class AVPCalendarComponent implements OnInit, OnDestroy {

  static MAX_DISPLAYED = 100;
  @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;
  @ViewChild("wpModal") wpModal: ElementRef;

  public calendarOptions: Options;
  public selectedWP: any;
  private unregisterListener: Function;
  private isInActive: boolean = false;
  private wpPopup: NgbModalRef;

  constructor(private element: ElementRef,
    private userService: CurrentUserService,
    private taskService: TasksService,
    private loaderService: LoaderService,
    private modalService: NgbModal,
    private errorDialogService: ErrorDialogService,
    private halResourceService: HalResourceService,
    private wpCacheService: WorkPackageCacheService,
    readonly trans: TransitionService,
    readonly $state: StateService) { }

  ngOnInit() {
    this.setCalendarOptions();
    this.unregisterListener = this.trans.onSuccess({}, (transition: Transition) => {
      if (this.isInActive) return undefined;

      this.selectedWP = undefined;
      let param = transition.params('to');
      if (this.wpPopup) {
        this.wpPopup.dismiss();
      }

      if (param.task) {
        this.toWPPopup();
        return true;
      }

      return false;
    });

    if (this.taskId) {
      this.toWPPopup();
    }
  }

  ngOnDestroy() {
    this.isInActive = true;
    this.unregisterListener();
  }

  public updateTimeframe($event: any) {
    let calendarView = this.calendarElement.fullCalendar('getView')!;
    let startDate = (calendarView.start as Moment).format('YYYY-MM-DD');
    let endDate = (calendarView.end as Moment).format('YYYY-MM-DD');
    let filters = this.defaultFilterProps(startDate, endDate);

    this.loaderService.loading(true, "Đang tải");
    this.taskService.getByFilters(AVPCalendarComponent.MAX_DISPLAYED, 1, filters).subscribe(
      (response: any) => {
        let halResource = this.halResourceService.createHalResource(response, true);
        this.warnOnTooManyResults(halResource);
        this.mapToCalendarEvents(halResource.elements);
        this.loaderService.loading(false);
      },
      (error: any) => {
        console.error(error.message);
        this.errorDialogService.show("Lỗi khi tải lịch");
        this.loaderService.loading(false);
      }
    )
  }

  public toWP($event: any) {
    this.$state.go(AVP_VIEWS.TASKS_CALENDAR, { task: $event.detail.event.workPackage.id });
  }

  public toWPPopup() {
    this.wpCacheService.loadWorkPackage(this.taskId).values$()
      .subscribe((wp: WorkPackageResource) => {
        this.selectedWP = wp;
      });

    this.wpPopup = this.modalService.open(this.wpModal, { size: 'lg', windowClass: '-task-display' });
  }

  public get stateName() {
    return this.$state.current.name;
  }

  public get taskId() {
    return this.$state.params['task'];
  }

  private mapToCalendarEvents(workPackages: any[]) {
    let events = workPackages.map((workPackage: any) => {
      let startDate = this.eventDate(workPackage, 'start');
      let endDate = this.eventDate(workPackage, 'due');

      let exclusiveEnd = moment(endDate).add(1, 'days');

      let status = this.getStatus(workPackage);

      return {
        title: workPackage.subject,
        start: startDate,
        end: exclusiveEnd,
        allDay: true,
        className: `__hl_background_type_${status}`,
        workPackage: workPackage.source
      };
    });

    this.ucCalendar.renderEvents(events);
  }

  private warnOnTooManyResults(collection: any) {
    if (collection.count < collection.total) {
      this.errorDialogService.show(`Số lượng công việc (${collection.total}) nhiều hơn số lượng có thể hiển thị (${AVPCalendarComponent.MAX_DISPLAYED})`);
    }
  }

  private getStatus(workPackage: any) {
    if (workPackage.status.name == 'New') {
      return 'new';
    }
    else if (workPackage.status.name == 'Done' || workPackage.status.name == 'Done Late') {
      return 'done';
    }
    else {
      if (workPackage.dueDate == null) {
        return 'in_progress';
      }

      let dueDate = new Date(workPackage.dueDate);
      if (dueDate.getTime() < new Date().getTime()) {
        return 'overdue';
      }
      return 'in_progress';
    }
  }

  private eventDate(workPackage: any, type: 'start' | 'due') {
    return workPackage[`${type}Date`];
  }

  private defaultFilterProps(startDate: string, endDate: string) {
    let filters = [
      {
        "status": { "operator": "o", "values": [] }
      },
      {
        "datesInterval": { "operator": "<>d", "values": [startDate, endDate] }
      },
      {
        "assigneeOrAuthor": { "operator": "=", "values": ['me'] }
      }
    ];

    return filters;
  }

  private setCalendarOptions() {
    this.calendarOptions = this.options;
  }

  private get options() {
    return {
      editable: false,
      eventLimit: false,
      height: () => {
        // -12 for the bottom padding
        return jQuery(window).height()! - this.calendarElement.offset()!.top - 12;
      },
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'today'
      },
      views: {
        month: {
          fixedWeekCount: false
        }
      }
    };
  }

  private get calendarElement() {
    return jQuery(this.element.nativeElement).find('ng-fullcalendar');
  }
}