import { Injectable, EventEmitter } from '@angular/core';
import { HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { BaseService } from "../../common/services/base.service";
import { HttpClient, HttpParams } from "@angular/common/http";
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { UserCacheService } from "core-components/user/user-cache.service";
import { ProjectCacheService } from 'core-components/projects/project-cache.service';
import { CurrentUserService } from "core-components/user/current-user.service";
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { I18nService } from "core-app/modules/common/i18n/i18n.service";
import { AvpUtils } from '../../common/services/avp-utils.service';
import { ErrorDialogService } from '../../common/components';

@Injectable({
  providedIn: 'root'
})
export class TasksService extends BaseService {
  onTaskRefreshEvent: EventEmitter<any> = new EventEmitter();
  refreshRequest: EventEmitter<any> = new EventEmitter();
  onWatcherUpdatedEvent: EventEmitter<any> = new EventEmitter();
  onSearchTaskEvent: EventEmitter<any> = new EventEmitter<any>();
  onTaskCreatedEvent: EventEmitter<any[]> = new EventEmitter<any[]>();
  onViewChangedEvent: EventEmitter<any> = new EventEmitter<any>();
  
  private EXCEL_EXTENSION = '.xlsx';

  private _schema: any;

  constructor(protected httpClient: HttpClient,
    readonly userCacheService: UserCacheService,
    readonly projectCacheService: ProjectCacheService,
    readonly I18n:I18nService,
    readonly currentUserService: CurrentUserService,
    private errorService: ErrorDialogService,
    protected avpUtils: AvpUtils,
    protected halResourceService: HalResourceService) { 
      super(httpClient, userCacheService, projectCacheService, currentUserService, avpUtils, halResourceService);
      this._schema = this.getSchema();
    }

  getAll(pageSize: number, pageIndex: number): Observable<any> {
    let requestUrl = this.restUrl + '/api/v3/work_packages';
    let httpParams = this.getQueryParams({
      ['pageSize']: pageSize,
      ['offset']: pageIndex,
      ['sortBy']: [["updatedAt", "desc"], ["id", "desc"]],
      ['filters']: [{ "type": { "operator": "!", "values": ["10"] } }]
    });
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

  getWatching(pageSize: number, pageIndex: number): Observable<any> {
    let requestUrl = this.restUrl + '/api/v3/work_packages';
    let httpParams = this.getQueryParams({
      ['pageSize']: pageSize,
      ['offset']: pageIndex,
      ['filters']: [
        { "status": { "operator": "o", "values": [] } }, 
        { "watcher": { "operator": "=", "values": ["me"] } }, 
        { "type": { "operator": "!", "values": ["10"] } }
      ],
      ['sortBy']: [["updatedAt", "desc"], ["id", "desc"]]
    });
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

  getByFilters(pageSize: number, pageIndex: number, filters: any[], sortBy: any[] = [["updatedAt", "desc"]]): Observable<any> {
    let requestUrl = this.restUrl + '/api/v3/work_packages';

    if (sortBy.length == 0) {
      sortBy = [["updatedAt", "desc"], ["id", "desc"]];
    }
    else {
      sortBy.push(["id", "desc"]);
    }

    filters.push({ "type": { "operator": "!", "values": ["10"] } })

    let httpParams = this.getQueryParams({
      ['pageSize']: pageSize,
      ['offset']: pageIndex,
      ['filters']: filters,
      ['sortBy']: sortBy
    });
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

  async getFilterProps(queryId: number) : Promise<any> {
    let requestUrl = this.restUrl + `/avp/api/v3/query/${queryId}/filters`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  getByQuery(pageSize: number, pageIndex: number, queryId: number): Observable<any> {
    let requestUrl = this.restUrl + `/api/v3/queries/${queryId}`;
    let httpParams = this.getQueryParams({
      ['pageSize']: pageSize,
      ['offset']: pageIndex,
      ['filters']: [{ "assigneeOrAuthor": { "operator": "=", "values": ["me"] } }, { "type": { "operator": "!", "values": ["10"] } }],
      ['sortBy']: [["updatedAt", "desc"], ["id", "desc"]]
    });
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

  async getByProject(projectId: number, filters: any[], sortBy: any[] = [["updatedAt", "desc"]]): Promise<any> {
    let requestUrl = this.restUrl + `/api/v3/projects/${projectId}/work_packages?pageSize=10000`;

    if (sortBy.length == 0) {
      sortBy = [["updatedAt", "desc"], ["id", "desc"]];
    }
    else {
      sortBy.push(["id", "desc"]);
    }

    filters.push({ "type": { "operator": "!", "values": ["10"] } });

    let httpParams = this.getQueryParams({
      ['filters']: filters,
      ['sortBy']: sortBy
    });
    return await this.httpClient.get<any>(requestUrl, { params: httpParams }).toPromise();
  }

  getByCategory(projectId: number, categoryId: number, filters: any[], pageSize?: number, offset?: number, sortBy: any[] = [["updatedAt", "desc"]]): Observable<any> {
    let requestUrl = this.restUrl + `/api/v3/projects/${projectId}/work_packages`;
    filters.push({
      "category": {
        "operator": categoryId != 0 ? "=" : "!*",
        "values": categoryId != 0 ? [`${categoryId}`] : []
      }
    });
    filters.push({ "type": { "operator": "!", "values": ["10"] } })
    if (sortBy.length == 0) {
      sortBy = [["updatedAt", "desc"], ["id", "desc"]];
    }
    else {
      sortBy.push(["id", "desc"]);
    }

    let httpParams = this.getQueryParams({
      ['pageSize']: pageSize ? pageSize : 100,
      ['offset']: offset ? offset : 1,
      ['filters']: filters,
      ['sortBy']: sortBy
    });
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

  async getRepeatedWPs(projectId: number | undefined = undefined) : Promise<any> {
    
    let requestUrl = projectId ? this.restUrl + `/api/v3/projects/${projectId}/work_packages` : this.restUrl + '/api/v3/work_packages';
    let httpParams = this.getQueryParams({
      ['pageSize']: 50,
      ['filters']: [
        { "type": { "operator": "=", "values": ["10"] } }, 
        { "parent": { "operator": "!*", "values": [""] } }, 
        { "assigneeOrAuthor": { "operator": "=", "values": ["me"] } }]
    });
    return await this.httpClient.get<any>(requestUrl, { params: httpParams }).toPromise();
  }

  async getTaskSummary() {
    let requestUrl = this.restUrl + `/avp/api/v3/summary`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  get(id: number): Observable<any> {
    let requestUrl = this.restUrl + `/api/v3/work_packages/${id}`;
    return this.httpClient.get(requestUrl);
  }

  create(task: any, projectId: number | undefined): Observable<any> {
    let requestUrl = this.restUrl + `/api/v3/projects/${projectId}/work_packages`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, task, { headers: headers });
  }

  async createAsync(task: any, projectId: number | undefined): Promise<any> {
    let requestUrl = this.restUrl + `/api/v3/projects/${projectId}/work_packages`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return await this.httpClient.post<any>(requestUrl, task, { headers: headers }).toPromise();
  }

  async updateAsync(id: number, payload: any): Promise<any> {
    let requestUrl = this.restUrl + `/api/v3/work_packages/${id}`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return await this.httpClient.patch<any>(requestUrl, payload, { headers: headers }).toPromise();
  }

  createQuery(query: any): Observable<any> {
    let requestUrl = this.restUrl + `/api/v3/queries`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, query, { headers: headers });
  }

  async getActivities(id: number): Promise<any> {
    let requestUrl = this.restUrl + `/api/v3/work_packages/${id}/activities`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  updateComment(id: number, payload: any): any {
    let requestUrl = this.restUrl + `/api/v3/activities/${id}`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    return this.httpClient.patch(requestUrl, payload, { headers: headers }).toPromise();
  }

  copySubTask(payload: any): any {
    let requestUrl = this.restUrl + `/avp/api/v3/copy_children`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers }).toPromise();
  }

  getDataByUrl(url: string) {
    return this.httpClient.get(url);
  }

  reportByEmployee(employeeIds: string[], by: string, from: string, to: string, projectId: number | null) {
    let requestUrl = this.restUrl + `/avp/api/v3/report_by_employee?by=${by}&from=${from}&to=${to}&employeeIds=${employeeIds}`;
    if(projectId) {
      requestUrl += `&projectId=${projectId}`;
    }

    return this.httpClient.get(requestUrl);
  }

  async getAsyncDataUrl(url: string) : Promise<any> {
    return await this.httpClient.get<any>(url).toPromise();
  }

  async getSchema() : Promise<any> {
    if(this._schema == null) {
      let requestUrl = this.restUrl + '/api/v3/work_packages/schemas/7-1';
      const schema = await this.httpClient.get<any>(requestUrl).toPromise();
      this._schema = schema;
    }
    return this._schema;
  }

  async deleteMulti(workPackageIds: string[]) : Promise<any> {
    let requestUrl = this.restUrl + '/repeated_task/api/v3/delete_work_packages?ids=' + workPackageIds.join('&ids=');
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    return await this.httpClient.post<any>(requestUrl, {}, { headers: headers }).toPromise();
  }

  async getMyEmployees() {
    let myEmployeesUrl = this.restUrl + '/avp/api/v3/my_employees';
    return await this.httpClient.get<any[]>(myEmployeesUrl).toPromise();
  }

  export(payload: any) {
    let requestUrl = '/api/pri/internal/export/wework/report';
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    return this.httpClient.post<any>(requestUrl, payload, { headers: headers, responseType: 'blob' as 'json' });
  }

  exportByMember(reporter: string, title: string, username: string, projects: any[], wps: any[]) {
    let payload: any = {
      'department': '',
      'reporter': reporter,
      'title': title,
      'reportTitle': 'Báo cáo công việc cá nhân của ' + username,
      'data': []
    };

    projects.forEach((project: any) => {
      let data: any = {
        'project_name': project.value,
        'work_packages': []
      };
      let _wps = wps[project.value];

      let reorderWPs = this.reorderByHierachy(_wps, null);

      data['work_packages'] = this.getFormattedWP(reorderWPs);

      payload['data'].push(data);
    });

    this.export(payload).subscribe(
      (response: any) => {
        FileSaver.saveAs(response, username + ".report." + moment().format('DD-MM-YYYY') + this.EXCEL_EXTENSION);
      },
      (error: any) => {
        this.errorService.show(error.message);
      }
    );
  }

  exportByProject(reporter: string, title: string, projectName: string, categories: any[], wps: any[]) {
    let payload: any = {
      'department': '',
      'reporter': reporter,
      'title': title,
      'reportTitle': 'Báo cáo công việc của dự án ' + projectName,
      'data': []
    };

    categories.forEach((cat: any) => {
      let data: any = {
        'project_name': cat.name,
        'work_packages': []
      };
      let _wps = wps.filter((task: any) => {
        return cat.id != 0 ? task._links.category.href == `/api/v3/categories/${cat.id}` : task._links.category.href == null;
      });

      let reorderWPs = this.reorderByHierachy(_wps, null);

      data['work_packages'] = this.getFormattedWP(reorderWPs);

      payload['data'].push(data);
    });

    this.export(payload).subscribe(
      (response: any) => {
        FileSaver.saveAs(response, projectName + ".report." + moment().format('DD-MM-YYYY') + this.EXCEL_EXTENSION);
      },
      (error: any) => {
        this.errorService.show(error.message);
      }
    );
  }

  private getFormattedWP(wps: any) {
    return wps.map((wp: any) => {
      return {
        'subject': `${wp['_links']['ancestors'] ? '#'.repeat(wp['_links']['ancestors'].length) + ' ' : ''}` + wp.subject.trim(),
        'author': wp['_embedded']['author']['name'],
        'assignee': wp['_embedded']['assignee'] ? wp['_embedded']['assignee']['name'] : '',
        'watchers': wp['_embedded']['watchers']['_embedded']['elements'].map((user: any) => user.name).join(', '),
        'is_urgent': wp.customField13,
        'start_date': wp.startDate ? wp.startDate : '',
        'due_date': wp.dueDate ? wp.dueDate : '',
        'finish_date': wp.finishedDate ? wp.finishedDate : '',
        'description': wp.description.raw ? wp.description.raw : '',
        'status':  this.I18n.t(`js.work_packages.statuses.${wp['_embedded']['status']['name'].toLowerCase().replace(/[^a-zA-Z0-9]/g, '_')}`),
        'result': wp.customField2.raw ? wp.customField2.raw : ''
      }
    })
  }

  private reorderByHierachy(wps: any, parentHref: string | null) {
    let orderedWPs:any[] = [];
    let rootWPs = wps.filter((wp: any) => {
      return wp._links.parent.href == parentHref;
    });

    rootWPs.forEach((wp: any) => {
      orderedWPs.push(wp);
      let descendants = this.reorderByHierachy(wps, wp._links.self.href);
      orderedWPs = orderedWPs.concat(descendants);
    });

    return orderedWPs;
  }

  onTaskCreated(wps: any[]) {
    this.onTaskCreatedEvent.emit(wps);
  }

  onRefreshWithParams(filters: any[] = [], orders: any[] = []) {
    this.onTaskRefreshEvent.emit({
      filters: filters,
      orderBy: orders
    });
  }

  onChangeView(displayType: string) {
    this.onViewChangedEvent.emit({
      displayType: displayType
    });
  }

  onSearch(filters: any[]) {
    this.onSearchTaskEvent.emit({
      filters: filters
    });
  }

  onRefresh() {
    this.refreshRequest.emit();
  }

  onRefreshWatchers() {
    this.onWatcherUpdatedEvent.emit();
  }
}