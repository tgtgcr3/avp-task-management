import { Component, OnInit } from '@angular/core';
import { StateService } from '@uirouter/core';
import * as moment from 'moment';
import { ProjectsService } from "../../projects/services/projects.service";
import { LoaderService } from "../../common/components";

@Component({
  templateUrl: './report-main.component.html',
  selector: 'report-main'
})
export class ReportMainComponent implements OnInit {
  public reportInfo: any = {
    workPackages: [],
    members: [],
    departments: [],
    projects: []
  };
  public loaded: boolean = false;

  constructor(private projectService: ProjectsService,
    private loaderService: LoaderService,
    readonly $state: StateService) {
  }

  ngOnInit() {
    this.loadReport();
  }

  private loadReport() {
    this.loadWorkPackages();
  }

  private loadWorkPackages() {
    let filter = this.buildFilter();
    this.loaderService.loading(true);

    this.projectService.getReportAll(filter.by, filter.from, filter.to, filter.countSubTask, filter.departmentId, filter.status).
      subscribe(
        (responses: any[]) => {
          this.reportInfo.workPackages = responses;
          this.reportInfo.members = this.projectService.getUsersOf(this.reportInfo.workPackages);
          this.reportInfo.projects = this.projectService.getProjectsOf(this.reportInfo.workPackages);
          this.reportInfo.departments = this.projectService.getDepartmentsOf(this.reportInfo);
          this.loaded = true;
          this.loaderService.loading(false);
        }
      )
  }

  private buildFilter() {
    let from = this.$state["params"].from;
    let to = this.$state["params"].to;
    let formattedTo = moment();
    let formattedFrom = moment().subtract(1, 'months').startOf('date');
    if (from && to) {
      let froms = from.split('-');
      let tos = to.split('-');
      formattedFrom = moment(`${froms[2]}-${froms[1]}-${froms[0]}`).startOf('date');
      formattedTo = moment(`${tos[2]}-${tos[1]}-${tos[0]}`).endOf('date');
    }

    let filter: any = {
      by: this.by == null ? "created_at" : this.by == 'duedate' ? "due_date" : "start_date",
      from: formattedFrom.toISOString(),
      to: formattedTo.toISOString(),
      countSubTask: this.includeSubtask,
      departmentId: this.depId,
      status: this.status
    }
    return filter;
  }

  private get by() {
    return this.$state["params"].by;
  }

  private get status() {
    return this.$state["params"].status;
  }

  private get includeSubtask() {
    return this.$state["params"].subtaskopt != 'excluded';
  }

  private get depId() {
    return this.$state["params"].dep;
  }
}