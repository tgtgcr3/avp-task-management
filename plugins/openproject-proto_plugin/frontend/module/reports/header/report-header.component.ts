import { Component, OnInit } from '@angular/core';
import { MainMenuToggleService } from "core-components/main-menu/main-menu-toggle.service";

@Component({
  templateUrl: './report-header.component.html',
  selector: 'report-header'
})
export class ReportHeaderComponent {

  constructor(public readonly toggleService: MainMenuToggleService) { }
}