// -- copyright
// OpenProject is a project management system.
// Copyright (C) 2012-2018 the OpenProject Foundation (OPF)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License version 3.
//
// OpenProject is a fork of ChiliProject, which is a fork of Redmine. The copyright follows:
// Copyright (C) 2006-2013 Jean-Philippe Lang
// Copyright (C) 2010-2013 the ChiliProject Team
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// See doc/COPYRIGHT.rdoc for more details.

import { APP_INITIALIZER, Injector, NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { HookService } from "core-app/modules/plugins/hook-service";
import { NotificationsService } from "core-app/modules/common/notifications/notifications.service";
import { ChartsModule } from 'ng2-charts';
import {
  TasksComponent,
  TaskItemComponent,
  TaskComponent,
  TaskSideBarComponent,
  TaskCreateFormComponent,
  AttachmentsComponent,
  CommentsComponent,
  CommentComponent,
  SubTaskComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/my-tasks";
import { TaskMasterComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/master/task-master.component";
import { ActionBarComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/action-bar/action-bar.component";
import { TasksSidebarComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/sidebar/tasks-sidebar.component";
import { FilterControlComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/action-bar/filtercontrol/filter-control.component";
import { SearchControlComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/header/searchcontrol/search-control.component";
import { TaskHeaderComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/header/task-header.component";
import { AVPCalendarComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/calendar/calendar.component";
import { FilterSetupComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/filter-setup/filter-setup.component";
import { TasksService } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/services/tasks.service";
import { ProjectPageService } from "core-app/modules/plugins/linked/openproject-proto_plugin/projects-page/services/project_page.service";
import { FilterSetupService } from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/filter-setup/filter-setup.service";
import { ClickOutsideDirective } from "core-app/modules/plugins/linked/openproject-proto_plugin/common/directives/click-outside.directive";
import { ProjectsService } from "./projects/services/projects.service";
import { CategoryService } from "./projects/services/category.service";
import { MembersService } from "./members/services/members.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NgbModalModule,
  NgbDatepickerModule,
  NgbTypeaheadModule,
  NgbPopoverModule,
  NgbTimepickerModule,
  NgbPaginationModule
} from '@ng-bootstrap/ng-bootstrap';
import {
  UserSearchBoxComponent,
  ErrorDialogService,
  LoaderService,
  LoaderComponent,
  UserInfoComponent,
  DatetimeComponent,
  UsersSeletorComponent,
  ExternalUsersSelectorComponent
} from "./common/components";
import { OpenprojectHalModule } from "core-app/modules/hal/openproject-hal.module";
import { OpenprojectEditorModule } from "core-app/modules/editor/openproject-editor.module";
import { OpenprojectFieldsModule } from "core-app/modules/fields/openproject-fields.module";
import { MainMenuToggleService } from "core-components/main-menu/main-menu-toggle.service";
import { FullCalendarModule } from "ng-fullcalendar";
import { UIRouterModule } from "@uirouter/angular";
import { AVP_ROUTES } from "./avp-routes";
import { AvpBaseComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/avp-base/avp-base.component";
import { AvpSidebarComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/avp-base/sidebar/avp-sidebar.component";
import { AvpManagementComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/avp-base/management/avp-management.component";
import { AvpNotificationsComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/avp-base/notifications/avp-notifications.component";
import { NgSelectModule } from "@ng-select/ng-select";
import {
  ProjectMasterComponent,
  ProjectsHeaderComponent,
  ProjectListComponent,
  ProjectActionBarComponent,
  ProjectCreateFormComponent,
  ProjectLayoutComponent,
  ProjectHeaderComponent,
  ProjectTasksComponent,
  ProjectSidebarComponent,
  ProjectGanttViewComponent,
  ProjectTasksBoardComponent,
  TasksBoardListComponent,
  CategoryComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/projects/components";
import {
  DepartmentListComponent,
  DepartmentHeaderComponent,
  DepartmentDetailMasterComponent,
  DepartmentDetailProjectsComponent,
  DepartmentSidebarComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/departments/components";
import {
  ProjectPageComponent,
  ProjectPageHeaderComponent,
  ReportsPageComponent,
  ReportFilterComponent,
  ReportTotalComponent,
  ReportTaskComponent,
  ReportByDayComponent,
  ReportByMemberComponent,
  ReportByProjectComponent,
  ReportByDepartmentComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/projects-page/components";
import {
  DoughnutChartComponent,
  BarChartComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/charts/components";
import { DragAndDropService } from "core-app/modules/boards/drag-and-drop/drag-and-drop.service";
import { IWorkPackageCreateServiceToken } from "core-components/wp-new/wp-create.service.interface";
import { IWorkPackageEditingServiceToken } from "core-components/wp-edit-form/work-package-editing.service.interface";
import { WorkPackageEditingService } from "core-components/wp-edit-form/work-package-editing-service";
import { UserCacheService } from 'core-components/user/user-cache.service';
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { ProjectCacheService } from 'core-components/projects/project-cache.service';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { OpenprojectWorkPackagesModule } from 'core-app/modules/work_packages/openproject-work-packages.module';
import {
  MemberComponent,
  MemberHeaderComponent,
  MemberTasksComponent,
  MemberTasksActionBarComponent,
  ListMemberComponent,
  MemberDetailMasterComponent,
  MemberDetailSidebarComponent,
  MemberDetailHeaderComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/members/components";
import {
  ReportHeaderComponent,
  ReportLayoutComponent,
  ReportMainComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/reports";
import {
  ProjectSettingLayoutComponent,
  ProjectSettingContentComponent,
  ProjectSettingRepeatedWPComponent,
  ProjectSettingMemberComponent,
  ProjectSettingMemberItemComponent,
  ProjectSettingPermissionComponent,
  ProjectSettingEmailComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/projects/components/project/settings";
import {
  RepeatedTaskItemComponent,
  RepeatedTasksComponent,
  RepeatedTaskFormComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/repeated-task";
import {
  ProjectTopicListComponent,
  TopicItemComponent,
  TopicCreateForm,
  TopicComponent,
  PostComponent
} from "core-app/modules/plugins/linked/openproject-proto_plugin/topics/components";
import { TopicService } from "core-app/modules/plugins/linked/openproject-proto_plugin/topics/services/topic.service";
import { DocumentComponent } from "core-app/modules/plugins/linked/openproject-proto_plugin/documents/components";
import { MyEmployeeComponent } from 'core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/my-employee/my-employee.component';
import { MyEmployeeActionBarComponent } from 'core-app/modules/plugins/linked/openproject-proto_plugin/tasks/components/my-employee/my-employee-action-bar/my-employee-action-bar.component';
import { LogAdapter } from 'core-app/modules/activity/activity_log.adapter';

export function initializeProtoPlugin(injector: Injector) {
  return () => {
    const hookService = injector.get(HookService);

    // Explicitly bootstrap the kitten component in the DOM if it is found
    // Angular would otherwise only bootstrap the global entry point bootstrap from the core
    // preventing us from using components like this kitten component
    hookService.register('openProjectAngularBootstrap', () => {
      return [
        { selector: 'bar-chart', cls: BarChartComponent },
        { selector: 'doughnut-chart', cls: DoughnutChartComponent },
        { selector: 'task-list', cls: TasksComponent },
        { selector: 'task-item', cls: TaskItemComponent },
        { selector: 'task-master', cls: TaskMasterComponent },
        { selector: 'task-side-bar', cls: TaskSideBarComponent },
        { selector: 'avp-base', cls: AvpBaseComponent },
        { selector: 'loader', cls: LoaderComponent },
        { selector: 'external-users-selector', cls: ExternalUsersSelectorComponent }
      ];
    });
  };
}

@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    NgbDatepickerModule,
    NgbTypeaheadModule,
    OpenprojectHalModule,
    OpenprojectEditorModule,
    OpenprojectFieldsModule,
    NgbPopoverModule,
    NgbTimepickerModule,
    NgSelectModule,
    FullCalendarModule,
    NgbPaginationModule,
    UIRouterModule.forChild({ states: AVP_ROUTES }),
    NgCircleProgressModule.forRoot({
      "backgroundColor": "#F1F1F1",
      "backgroundPadding": -18,
      "radius": 60,
      "toFixed": 2,
      "outerStrokeWidth": 10,
      "outerStrokeColor": "#FF6347",
      "innerStrokeColor": "#32CD32",
      "innerStrokeWidth": 1,
      "startFromZero": false
    }),
    OpenprojectWorkPackagesModule
  ],
  providers: [
    // This initializer gets called when the Angular frontend is being loaded by the core
    // use it to hook up global listeners or bootstrap components
    { provide: APP_INITIALIZER, useFactory: initializeProtoPlugin, deps: [Injector], multi: true },
    { provide: IWorkPackageCreateServiceToken, useValue: {} },
    { provide: IWorkPackageEditingServiceToken, useClass: WorkPackageEditingService },
    TasksService,
    ProjectsService,
    CurrentUserService,
    MembersService,
    ErrorDialogService,
    LoaderService,
    MainMenuToggleService,
    FilterSetupService,
    UserCacheService,
    ProjectPageService,
    ProjectCacheService,
    WorkPackageCacheService,
    DragAndDropService,
    CategoryService,
    TopicService,
    NotificationsService,
    LogAdapter
  ],
  declarations: [
    // Declare the component for angular to use
    TasksComponent,
    TaskItemComponent,
    TaskMasterComponent,
    TaskComponent,
    ActionBarComponent,
    ClickOutsideDirective,
    TaskSideBarComponent,
    UserSearchBoxComponent,
    TaskCreateFormComponent,
    LoaderComponent,
    DatetimeComponent,
    UsersSeletorComponent,
    ExternalUsersSelectorComponent,
    AttachmentsComponent,
    TaskHeaderComponent,
    AvpBaseComponent,
    FilterSetupComponent,
    FilterControlComponent,
    AVPCalendarComponent,
    TasksSidebarComponent,
    SearchControlComponent,
    ProjectMasterComponent,
    ProjectsHeaderComponent,
    ProjectListComponent,
    ProjectActionBarComponent,
    ProjectCreateFormComponent,
    DepartmentListComponent,
    DepartmentHeaderComponent,
    DepartmentDetailMasterComponent,
    DepartmentDetailProjectsComponent,
    DepartmentSidebarComponent,
    ProjectPageComponent,
    ProjectPageHeaderComponent,
    ReportsPageComponent,
    ReportHeaderComponent,
    ReportTotalComponent,
    ReportTaskComponent,
    ReportByDayComponent,
    ReportByMemberComponent,
    DoughnutChartComponent,
    BarChartComponent,
    CommentsComponent,
    CommentComponent,
    AvpSidebarComponent,
    ProjectLayoutComponent,
    ProjectHeaderComponent,
    ProjectTasksComponent,
    ProjectSidebarComponent,
    ProjectGanttViewComponent,
    ProjectTasksBoardComponent,
    AvpManagementComponent,
    MemberComponent,
    MemberHeaderComponent,
    ListMemberComponent,
    SubTaskComponent,
    MemberDetailMasterComponent,
    MemberDetailSidebarComponent,
    MemberDetailHeaderComponent,
    MemberTasksComponent,
    MemberTasksActionBarComponent,
    UserInfoComponent,
    ReportFilterComponent,
    ReportLayoutComponent,
    ReportMainComponent,
    ReportByProjectComponent,
    TasksBoardListComponent,
    AvpNotificationsComponent,
    ProjectSettingLayoutComponent,
    ProjectSettingContentComponent,
    ProjectSettingRepeatedWPComponent,
    ProjectSettingMemberComponent,
    ProjectSettingMemberItemComponent,
    ProjectSettingPermissionComponent,
    ProjectSettingEmailComponent,
    ReportByDepartmentComponent,
    RepeatedTaskItemComponent,
    RepeatedTasksComponent,
    RepeatedTaskFormComponent,
    ProjectTopicListComponent,
    TopicItemComponent,
    TopicCreateForm,
    TopicComponent,
    PostComponent,
    DocumentComponent,
    MyEmployeeComponent,
    MyEmployeeActionBarComponent,
    CategoryComponent
  ],
  entryComponents: [
    // Special case: Declare the component also as a bootstrap component
    // as it is being rendered from Rails.
    TasksComponent,
    TaskItemComponent,
    TaskMasterComponent,
    TaskComponent,
    ActionBarComponent,
    TaskSideBarComponent,
    UserSearchBoxComponent,
    TaskCreateFormComponent,
    LoaderComponent,
    DatetimeComponent,
    UsersSeletorComponent,
    ExternalUsersSelectorComponent,
    AttachmentsComponent,
    TaskHeaderComponent,
    AvpBaseComponent,
    FilterSetupComponent,
    FilterControlComponent,
    AVPCalendarComponent,
    TasksSidebarComponent,
    SearchControlComponent,
    ProjectMasterComponent,
    ProjectsHeaderComponent,
    ProjectListComponent,
    ProjectActionBarComponent,
    ProjectCreateFormComponent,
    DepartmentListComponent,
    DepartmentHeaderComponent,
    DepartmentDetailMasterComponent,
    DepartmentDetailProjectsComponent,
    DepartmentSidebarComponent,
    ProjectPageComponent,
    ProjectPageHeaderComponent,
    ReportsPageComponent,
    ReportTotalComponent,
    ReportTaskComponent,
    ReportByDayComponent,
    ReportByMemberComponent,
    DoughnutChartComponent,
    BarChartComponent,
    CommentsComponent,
    CommentComponent,
    AvpSidebarComponent,
    ProjectLayoutComponent,
    ProjectHeaderComponent,
    ProjectTasksComponent,
    ProjectSidebarComponent,
    ProjectGanttViewComponent,
    ProjectTasksBoardComponent,
    AvpManagementComponent,
    MemberComponent,
    MemberHeaderComponent,
    ListMemberComponent,
    SubTaskComponent,
    MemberDetailMasterComponent,
    MemberDetailHeaderComponent,
    MemberTasksComponent,
    MemberTasksActionBarComponent,
    UserInfoComponent,
    ReportFilterComponent,
    ReportLayoutComponent,
    ReportMainComponent,
    ReportByProjectComponent,
    TasksBoardListComponent,
    MemberDetailSidebarComponent,
    AvpNotificationsComponent,
    ProjectSettingLayoutComponent,
    ProjectSettingContentComponent,
    ProjectSettingRepeatedWPComponent,
    ProjectSettingMemberComponent,
    ProjectSettingMemberItemComponent,
    ProjectSettingPermissionComponent,
    ProjectSettingEmailComponent,
    ReportByDepartmentComponent,
    RepeatedTaskItemComponent,
    RepeatedTasksComponent,
    RepeatedTaskFormComponent,
    ProjectTopicListComponent,
    TopicItemComponent,
    TopicCreateForm,
    TopicComponent,
    PostComponent,
    DocumentComponent,
    MyEmployeeComponent,
    MyEmployeeActionBarComponent,
    CategoryComponent
  ]
})
export class PluginModule {
}



