import { EventEmitter, Injectable } from '@angular/core';
import { BaseService } from "../../common/services/base.service";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { UserResource } from 'core-app/modules/hal/resources/user-resource';
import { Observable, Subject } from 'rxjs';
import { environment } from "environments/environment";

@Injectable({
  providedIn: 'root'
})
export class MembersService extends BaseService {

  onTaskSideBarEvent: EventEmitter<any> = new EventEmitter();
  onTaskSideBarFilterEvent: EventEmitter<any> = new EventEmitter();
  onUserInfoEvent: EventEmitter<any> = new EventEmitter();
  onTasksGroupByProjectEvent: EventEmitter<any> = new EventEmitter();
  onSearchUserEvent: EventEmitter<string> = new EventEmitter();
  onExportTasksEvent: EventEmitter<string> = new EventEmitter();

  async byProject(projectId: number) : Promise<any> {
    let requestUrl = this.restUrl + `/avp/api/v3/projects/${projectId}/available_assignees`;
    return await this.httpClient.get<any>(requestUrl).toPromise();
  }

  allMember(query?: string, offset?: number, pageSize?: number): Promise<any> {
    let requestUrl = this.restUrl + `/avp/api/v3/usersAll?offset=${offset}&pageSize=${pageSize}`;
    if(query) {
      requestUrl += `&query=${query}`;
    }
    return this.httpClient.get<any>(requestUrl).toPromise();
  }

  all(queryString: string = '') {
    let requestUrl = this.restUrl + `/avp/api/v3/users`;
    let filters = [];
    if (queryString != '') {
      filters.push({ 'name': { 'operator': 'like', 'values': queryString } });
    }
    let httpParams = this.getQueryParams({ ['filters']: filters });
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

  get(id: number) {
    let requestUrl = this.restUrl + `/api/v3/users/${id}`;
    return this.halResourceService
      .get<UserResource>(requestUrl)
      .toPromise();
  }

  getByUrl(path: string) {
    let requestUrl = this.restUrl + path;
    return this.halResourceService
      .get<UserResource>(requestUrl)
      .toPromise();
  }

  create(payload: any) {
    let requestUrl = this.restUrl + `/api/v3/memberships`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers });
  }

  updateAll(payload: any) {
    let requestUrl = this.restUrl + `/avp/api/v3/memberships`;
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; utf-8');
    headers.set('Accept', 'application/json');
    return this.httpClient.post(requestUrl, payload, { headers: headers });
  }

  delete(id: number) {
    let requestUrl = this.restUrl + `/avp/api/v3/memberships/${id}`;
    return this.httpClient.delete(requestUrl);
  }

  getTasksGroupByProject(userId: number, filters: any[], sortBy: any[] = [["updatedAt", "desc"]]): Observable<any> {
    let requestUrl = this.restUrl + `/api/v3/work_packages?groupBy=project&pageSize=100`;
    if (sortBy.length == 0) {
      sortBy = [["updatedAt", "desc"], ["id", "desc"]];
    }
    else {
      sortBy.push(["id", "desc"]);
    }
    filters.push({ "assignee": { "operator": "=", "values": [`${userId}`] } });
    filters.push({ "type": { "operator": "!", "values": ["10"] } });

    let httpParams = this.getQueryParams({
      ['filters']: filters,
      ['sortBy']: sortBy
    });
    return this.httpClient.get(requestUrl, { params: httpParams });
  }

  onUpdateTaskSideBar(data: any) {
    this.onTaskSideBarEvent.emit(data);
  }

  onSideBarFiltered(project: any) {
    this.onTaskSideBarFilterEvent.emit(project)
  }

  onFilterByName(searchText: any) {
    this.onSearchUserEvent.emit(searchText);
  }

  onExportTasks(username: string) {
    this.onExportTasksEvent.emit(username);
  }

  getUserInfo(mail: string) {
    let api = `/api/pri/internal/userinfo?email=${mail}`;
    return this.httpClient.get(api);
  }

  async getUsersFromAccount() : Promise<any> {
    let api = '/api/pri/keycloak/users?offset=0&size=1024';
    return await this.httpClient.get<any>(api).toPromise();
  }
}