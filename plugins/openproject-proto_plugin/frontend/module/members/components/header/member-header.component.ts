import { Component } from '@angular/core';
import { MainMenuToggleService } from "core-components/main-menu/main-menu-toggle.service";
import { MembersService } from '../../services/members.service';
import { CurrentUserService } from 'core-components/user/current-user.service';
import { StateService } from '@uirouter/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as moment from 'moment';

@Component({
  templateUrl: './member-header.component.html',
  selector: 'member-header'
})
export class MemberHeaderComponent {

  private EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  private EXCEL_EXTENSION = '.xlsx';
  private REPORT_EXPORT_TITLE = "anvietphatcompany.members.report";

  constructor(public readonly toggleService: MainMenuToggleService,
    private currentUserService: CurrentUserService,
    private memberService: MembersService,
    private $state: StateService) { }

  public onSearchProject($event: any) {
    let searchString = $event.target.value.toLowerCase();
    this.$state.go('.', {query: searchString});
  }

  public get isAdminOrOwner() {
    return this.currentUserService.role == 'owner' || this.currentUserService.role == 'admin';
  }

  public onExport() {
    let exportData: any[] = [];
    this.memberService.allMember().then(
      (response: any) => {
        let reportInfo = response['_embedded']['elements'];
        reportInfo.forEach((value: any) => {
          let user: any = {
            'Last name': value.lastName,
            'First name': value.firstName,
            'Email': value.mail,
            'Username': value.username,
            'Job title': value.title
          };
          exportData.push(user);
        });
    
        this.saveAsExcelFile(exportData);
      }
    );
  }

  private saveAsExcelFile(dataToExport: any) {

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataToExport);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });

    const data: Blob = new Blob([excelBuffer], {
      type: this.EXCEL_TYPE
    });
    FileSaver.saveAs(data, this.REPORT_EXPORT_TITLE + "." + moment().format('DD-MM-YYYY') + this.EXCEL_EXTENSION);
  }
}