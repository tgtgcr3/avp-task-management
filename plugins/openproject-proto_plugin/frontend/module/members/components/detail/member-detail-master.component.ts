import { Component, OnInit } from '@angular/core';
import { StateService } from '@uirouter/core';

@Component({
  templateUrl: './member-detail-master.component.html',
  selector: 'member-detail-master'
})
export class MemberDetailMasterComponent implements OnInit {

  public showingTask: any;

  constructor(readonly $state: StateService) { }

  ngOnInit() {
  }

  public get isTaskShowing() {
    return this.$state.params['task'] != null;
  }
}