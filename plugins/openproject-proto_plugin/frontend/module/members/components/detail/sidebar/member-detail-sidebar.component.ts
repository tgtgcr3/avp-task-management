import { Component, OnInit, ViewChild } from '@angular/core';
import { UserCacheService } from "core-components/user/user-cache.service";
import { MembersService } from "../../../services/members.service";
import { StateService } from "@uirouter/core";
import { AvpUtils } from "../../../../common/services/avp-utils.service";
import * as moment from 'moment';
import { AVP } from '../../../../common/consts/avp';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  templateUrl: './member-detail-sidebar.component.html',
  selector: 'member-detail-sidebar'
})
export class MemberDetailSidebarComponent implements OnInit {

  public chartData: any = {
    barChartType: "bar",
    barChartLegend: "bottom",
    barChartLabels: [],
    barChartOptions: {
      responsive: true,
      maintainAspectRatio: false,
      legend: {
        position: 'bottom'
      },
      scales: {
        yAxes: [{
          stacked: true,
          ticks: {
            precision: 0
          }
        }],
        xAxes: [{
          stacked: true
        }]
      }
    },
    barChartColors: [
      { backgroundColor: '#48856c' },
      { backgroundColor: '#14cc3f' },
      { backgroundColor: '#f54e3b' },
    ],
    barChartData: [
      { data: [], label: 'Tất cả', type: 'line', stack: "line-y-axis", fill: false },
      { data: [], label: 'Hoàn thành', stack: 'bar-y-axis' },
      { data: [], label: 'Quá hạn', stack: 'bar-y-axis' }
    ]
  }

  public isFiltered: boolean = false;
  public filteredProject: any;
  public user: any = {
    name: "",
    login: "",
    avatar: ""
  };
  public tasksGroupByProject: any = {
    groups: []
  };

  public reportData: any = {
    inProgress: 0,
    new: 0,
    done: 0,
    overdue: 0,
    urgent: 0
  };
  
  private workPackages: any[] = [];
  private filteredWorkPackages: any[] = [];

  constructor(readonly $state: StateService,
    private membersService: MembersService,
    readonly userCacheService: UserCacheService,
    private avpUtils: AvpUtils) { }

  ngOnInit(): void {
    this.membersService.onTaskSideBarEvent.subscribe(this.onUpdateData.bind(this));
    this.loadUser();
  }

  public onFilter(project: any) {
    this.isFiltered = true;
    this.filteredProject = project;
    this.filteredWorkPackages = this.workPackages.filter((wp: any) => {
      return wp._links.project.href === project._links.valueLink[0].href;
    });
    this.membersService.onSideBarFiltered(project);
    this.initDataChart();
  }

  public onRemoveFilter() {
    this.isFiltered = false;
    this.filteredProject = null;
    this.membersService.onSideBarFiltered(null);
    this.filteredWorkPackages = this.workPackages;
    this.initDataChart();
  }
  
  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  public get projects() {
    return this.tasksGroupByProject ? this.tasksGroupByProject.groups : [];
  }

  public get memberId() {
    return this.$state["params"].memberId;
  }

  public get total() {
    return this.filteredWorkPackages.length;
  }

  private loadUser() {
    this.userCacheService
      .require(this.memberId)
      .then((user: any) => {
        this.user = user;
      });
  }

  private initDataChart() {
    this.reportData = {
      inProgress: this.filterStatus(this.filteredWorkPackages, 'progress'),
      new: this.filterStatus(this.filteredWorkPackages, 'new'),
      done: this.filterStatus(this.filteredWorkPackages, 'done'),
      overdue: this.filterStatus(this.filteredWorkPackages, 'overdue'),
      urgent: 0
    };
    this.reportData['urgent'] = this.filteredWorkPackages.filter((wp: any) => {
      return wp.customField13 == true;
    }).length

    let groupedTask = this.filteredWorkPackages.reduce((r: any, a: any) => {
      let date = moment(a.createdAt).startOf('day').format();
      r[date] = [...r[date] || [], a];
      return r;
    }, {});

    let total = 0;
    let previousDone = 0;
    let previousOverdue = 0;

    this.chartData.barChartLabels = [];
    this.chartData.barChartData[0].data = [];
    this.chartData.barChartData[1].data = []; // Done
    this.chartData.barChartData[2].data = []; // Overdue

    Object.keys(groupedTask)
      .sort((a: string, b: string) => {
        return a.localeCompare(b);
      })
      .forEach((key: string) => {
        previousDone += this.filterStatus(groupedTask[key], 'done');
        previousOverdue += this.filterStatus(groupedTask[key], 'overdue');
        total += groupedTask[key].length;

        let label = moment(key).format('DD/MM');
        this.chartData.barChartLabels.push(label);
        this.chartData.barChartData[0].data.push(total);
        this.chartData.barChartData[1].data.push(previousDone); // Done
        this.chartData.barChartData[2].data.push(previousOverdue); // Overdue
      });
  }

  private onUpdateData(response: any) {
    this.tasksGroupByProject = response;
    this.workPackages = response._embedded.elements;
    this.filteredWorkPackages = response._embedded.elements;
    this.initDataChart();
  }

  private filterStatus(list: any[], status: string) {
    return list ? list.filter((wp: any) => {
      if (status == "overdue") {
        return this.checkOverdue(wp);
      }
      return wp._embedded.status.name.toLowerCase().includes(status);
    }).length : 0;
  }

  private checkOverdue(wp: any) {
    if (wp.dueDate == null || wp.dueDate == 0 || wp.dueDate == undefined) {
      return false;
    }
    let dueDate = new Date(wp.dueDate);
    return dueDate.getTime() < new Date().getTime() &&
      !wp._embedded.status.name.toLowerCase().includes('done');
  }
}