import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { StateService } from '@uirouter/core';
import { NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ErrorDialogService } from '../../../../common/components';
import { ProjectsService } from '../../../../projects/services/projects.service';
import { TasksService } from '../../../../tasks/services/tasks.service';
import * as moment from 'moment';
import * as AVP from '../../../../common/consts/avp';

@Component({
  templateUrl: './member-tasks-filter.component.html',
  selector: 'member-tasks-actionbar'
})
export class MemberTasksActionBarComponent implements OnInit {

  @Output() initialized: EventEmitter<any> = new EventEmitter();

  public hoveredDate: any = null;

  public fromDate: any;
  public toDate: any;

  public orders: any[] = [
    {
      value: "updatedAt",
      label: "Thời gian cập nhật",
      isDefault: true
    },
    {
      value: "createdAt",
      label: "Thời gian tạo"
    },
    {
      value: "dueDate",
      label: "Thời hạn hoàn thành",
    },
  ];

  public statuses: any[] = AVP.FILTER_STATUS_PROPERIES;;

  public projects: any[] = [];

  public selected: any = {
    employee: {},
    project: {},
    status: this.statuses[1],
    ordering: this.orders[0]
  };

  private currentFilters: any[] = [];
  private currentOrders: any[] = [];

  constructor(private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    private taskService: TasksService,
    private projectService: ProjectsService,
    private errorDialogService: ErrorDialogService,
    readonly $state: StateService) {
    this.toDate = calendar.getToday();
    this.fromDate = calendar.getPrev(this.toDate, 'd', 30);
  }

  ngOnInit() {
    this.loadPosibleProjects();
    
  }

  public onDateSelection(date: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }

    if (this.fromDate && this.toDate) {
      this.onFilterByDate();
    }
  }

  public isHovered(date: any) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  public isInside(date: any) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  public isRange(date: any) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  public get filteredDate() {
    if (!this.fromDate && !this.toDate) {
      return 'Tất cả';
    }

    let from = this.fromDate ? `${this.fromDate["day"]}-${this.fromDate["month"]}-${this.fromDate["year"]}` : '--';
    let to = this.toDate ? `${this.toDate["day"]}-${this.toDate["month"]}-${this.toDate["year"]}` : '--';

    return `${from} đến ${to}`;
  }

  public onChangeOrder(ordering: any) {
    this.selected.ordering = ordering;

    this.currentOrders = [
      [ordering.value, "desc"]
    ];

    this.onRefreshTasks();
  }

  public onChangeProject(project: any) {
    this.selected.project = project;
    this.checkAndRemoveIfExisted("project");
    if (project.value != "all") {
      this.currentFilters.push({
        "project": {
          "operator": "=",
          "values": [project.value]
        }
      });
    }
    else {
      let projectIds = this.projects.slice(1, this.projects.length).map((response: any) => response.value);
      this.currentFilters.push({
        "project": {
          "operator": "=",
          "values": projectIds
        }
      });
    }

    this.onRefreshTasks();
  }

  public onChangeStatus(status: any) {
    this.selected.status = status;
    this.checkAndRemoveIfExisted("status");
    this.checkAndRemoveIfExisted("dueDate");
    this.checkAndRemoveIfExisted("customField13");
    this.checkAndRemoveIfExisted("customField14");


    switch (status.value) {
      case "not-done":
        this.currentFilters.push({
          "status": {
            "operator": "=",
            "values": ["1", "7"]
          }
        });
        break;
      case "done":
        this.currentFilters.push({
          "status": {
            "operator": "=",
            "values": ["16", "17"]
          }
        });
        break;
      case "in-review":
        this.currentFilters.push({
          "status": {
            "operator": "=",
            "values": ["10"]
          }
        });
        break;
      case "done-late":
        this.currentFilters.push({
          "status": {
            "operator": "=",
            "values": ["17"]
          }
        });
        break;
      case "overdue":
        this.currentFilters.push({
          "status": {
            "operator": "!",
            "values": ["16", "17"]
          }
        });
        this.currentFilters.push({
          "dueDate": {
            "operator": "<t-",
            "values": ["1"]
          }
        });
        break;
      case "urgent":
        this.currentFilters.push({
          "customField14": {
            "operator": "=",
            "values": ["t"]
          }
        });
        break;
      case "important":
        this.currentFilters.push({
          "customField13": {
            "operator": "=",
            "values": ["t"]
          }
        });
        break;
      default:
        break;
    }

    this.onRefreshTasks();
  }

  private onFilterByDate() {
    this.checkAndRemoveIfExisted('createdAt');

    let formattedFrom = `${this.fromDate.month}/${this.fromDate.day}/${this.fromDate.year}`;
    let formattedTo = `${this.toDate.month}/${this.toDate.day}/${this.toDate.year}`;
    let filter: any = {};
    filter['createdAt'] = {
      "operator": "<>d",
      "values": [moment(formattedFrom).toISOString(), moment(formattedTo).toISOString()]
    };
    this.currentFilters.push(filter);
    this.onRefreshTasks();
  }

  private checkAndRemoveIfExisted(key: string) {
    let tobeRemovedFilters = this.currentFilters.filter((filter: any) => {
      return filter[key] != undefined;
    });

    tobeRemovedFilters.forEach((filter: any) => {
      let index = this.currentFilters.indexOf(filter);
      this.currentFilters.splice(index, 1);
    })
  }

  private onRefreshTasks() {
    this.taskService.onRefreshWithParams(this.currentFilters, this.currentOrders);
  }

  private loadProjects() {
    this.projectService.getAll()
      .subscribe(
        (responses: any[]) => {
          this.projects = [{
            value: "all",
            label: "Tất cả dự án",
            isDefault: true
          }];

          this.projects = this.projects.concat(responses.map((project: any) => {
            return { value: project.id, label: project.name };
          }));

          this.selected.project = this.projects[0];
        },
        (error: any) => {
          this.errorDialogService.show("Lỗi khi tải tài nguyên");
        }
      )
  }

  private loadPosibleProjects() {
    this.projectService.getPosibleProjects()
      .subscribe(
        (responses: any[]) => {
          this.projects = [{
            value: "all",
            label: "Tất cả dự án",
            isDefault: true
          }];

          this.projects = this.projects.concat(responses.map((project: any) => {
            return { value: project.id, label: project.name };
          }));

          this.selected.project = this.projects[0];

          this.currentFilters.push({
            "project": {
              "operator": "=",
              "values": responses.map((project: any) => project.id)
            }
          });

          this.currentFilters.push({
            "status": {
              "operator": "=",
              "values": ["1", "7"]
            }
          });

          this.initialized.emit({filters: this.currentFilters, orderBy: this.currentOrders});
        },
        (error: any) => {
          this.errorDialogService.show("Lỗi khi tải tài nguyên");
        }
      )
  }
}