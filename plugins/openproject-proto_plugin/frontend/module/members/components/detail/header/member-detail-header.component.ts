import {Component, OnInit, Input} from '@angular/core';
import {MainMenuToggleService} from "core-components/main-menu/main-menu-toggle.service";
import {CurrentUserService} from 'core-components/user/current-user.service';
import {UserCacheService} from "core-components/user/user-cache.service";
import { StateService } from '@uirouter/core';
import {ErrorDialogService} from "../../../../common/components";
import { AvpUtils } from '../../../../common/services/avp-utils.service';
import { MembersService } from '../../../services/members.service';
import { AVP } from '../../../../common/consts/avp';

@Component({
  templateUrl: './member-detail-header.component.html',
  selector: 'member-detail-header'
})
export class MemberDetailHeaderComponent implements OnInit {

  private EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  private EXCEL_EXTENSION = '.xlsx';

  public user: any = {};
  constructor(readonly $state: StateService,
              public readonly toggleService: MainMenuToggleService,
              readonly userCacheService: UserCacheService,
              private errorDialogService: ErrorDialogService,
              private currentUserService: CurrentUserService,
              private memberService: MembersService,
              private avpUtils: AvpUtils) { }

  ngOnInit() {
    this.loadUser();
  }

  public onExport() {
    this.memberService.onExportTasks(this.name.replace(/\s/gi, '-'));
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  private loadUser() {
    this.userCacheService
        .require(this.memberId)
        .then((user: any) => {
          this.user = user;
        })
        .catch(() => this.errorHandle.bind(this));
  }

  private errorHandle(error: any) {
    console.error(error);
    this.errorDialogService.show(error.message);
  }

  public get name() {
    return this.user ? this.user.name : '';
  }

  public get isAdminOrOwner() {
    return this.currentUserService.role == 'owner' || this.currentUserService.role == 'admin';
  }

  public get memberId() {
    return this.$state["params"].memberId;
  }
}