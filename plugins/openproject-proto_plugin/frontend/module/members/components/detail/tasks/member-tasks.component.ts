import { Component, OnDestroy, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { UserCacheService } from "core-components/user/user-cache.service";
import { CurrentUserService } from 'core-components/user/current-user.service';
import { WorkPackageResource } from "core-app/modules/hal/resources/work-package-resource";
import { WorkPackageCacheService } from 'core-components/work-packages/work-package-cache.service';
import { HalResourceService } from "core-app/modules/hal/services/hal-resource.service";
import { ErrorDialogService, LoaderService } from '../../../../common/components';
import { TasksService } from '../../../../tasks/services/tasks.service';
import { MembersService } from '../../../services/members.service';
import { StateService } from '@uirouter/core';
import { Subscription } from 'rxjs';
import { ProjectsService } from '../../../../projects/services/projects.service';
import { I18nService } from "core-app/modules/common/i18n/i18n.service";

@Component({
  templateUrl: './member-tasks.component.html',
  selector: 'member-tasks',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MemberTasksComponent implements OnInit, OnDestroy {

  public groups: any = {};
  public filteredProjects: any[] = [];

  private onFiltersSubscription: Subscription;
  private onSearchSubscription: Subscription;
  private onRefreshSubscription: Subscription;
  private onTaskSideBarSubscription: Subscription;
  private onExportWPSupscription: Subscription;

  private filters: any[] = [];
  private order: any[] = [];
  private searchFilter: any[] = [];
  private currentUser: any;

  private tasksGroupByProject: any;
  private posibleProjectFilter: any = {};

  constructor(readonly $state: StateService,
    protected I18n:I18nService,
    private memberService: MembersService,
    private projectService: ProjectsService,
    private errorService: ErrorDialogService,
    private taskService: TasksService,
    private currentUserService: CurrentUserService,
    private halResourceService: HalResourceService,
    private wpCacheService: WorkPackageCacheService,
    readonly userCacheService: UserCacheService,
    private loaderService: LoaderService,
    private ref: ChangeDetectorRef) { }

  ngOnInit() {
    this.onFiltersSubscription = this.taskService.onTaskRefreshEvent.subscribe(this.refreshWithParams.bind(this));
    this.onSearchSubscription = this.taskService.onSearchTaskEvent.subscribe(this.onSearch.bind(this));
    this.onRefreshSubscription = this.taskService.refreshRequest.subscribe(this.refresh.bind(this));
    this.onTaskSideBarSubscription = this.memberService.onTaskSideBarFilterEvent.subscribe(this.filterByTaskSideBar.bind(this));
    this.onExportWPSupscription = this.memberService.onExportTasksEvent.subscribe(this.onExportWPs.bind(this));
    //this.loadPosibleProjects();
    this.loadCurrentUser();
  }

  ngOnDestroy() {
    this.onFiltersSubscription.unsubscribe();
    this.onSearchSubscription.unsubscribe();
    this.onRefreshSubscription.unsubscribe();
    this.onTaskSideBarSubscription.unsubscribe();
    this.onExportWPSupscription.unsubscribe();
  }

  public get memberId() {
    return this.$state["params"].memberId;
  }

  public openOrCollapse(group: string) {
    this.groups[`${group}`] = !this.groups[`${group}`];
  }

  public onFilterInitialized(params: any) {
    this.filters = params.filters;
    this.order = params.orderBy;
    this.loadTasks();
  }

  private refresh() {
    this.loadTasks();
  }

  private filterByTaskSideBar(project: any) {
    if (project) {
      this.filteredProjects = [project];
    }
    else {
      this.filteredProjects = this.tasksGroupByProject.groups;
    }
  }

  private loadPosibleProjects() {
    this.loaderService.loading(true);
    this.projectService.getPosibleProjects()
      .subscribe(
        (responses: any[]) => {
          let filter: any = {
            "project": {
              "operator": "=",
              "values": responses.map((item: any) => { return item.id })
            }
          };
          this.posibleProjectFilter = filter;
          this.filters.push(this.posibleProjectFilter);
          this.loadTasks();
          this.ref.markForCheck();
          this.loaderService.loading(false);
        }
      )
  }

  private loadCurrentUser() {
    this.userCacheService
      .require(this.currentUserService.userId)
      .then((user: any) => {
        this.currentUser = user;
        this.ref.markForCheck();
      })
      .catch(() => this.errorHandle.bind(this));
  }

  private loadTasks() {
    this.loaderService.loading(true);
    let filters = this.filters.concat(this.searchFilter);
    this.memberService.getTasksGroupByProject(this.memberId, filters, this.order)
      .subscribe(
        (response: any) => {
          this.updateCache(response['_embedded']['elements']);
          this.tasksGroupByProject = {
            groups: response.groups,
            work_packages: {}
          };

          response.groups.forEach((p: any) => {
            this.groups[p.value] = true;
            this.tasksGroupByProject.work_packages[p.value] = response['_embedded']['elements'].filter((task: any) => {
              return task._links.project.title.toLowerCase() === p.value.toLowerCase();
            })
          });
          
          this.filteredProjects = response.groups;
          this.memberService.onUpdateTaskSideBar(response);
          this.loaderService.loading(false);
          this.ref.markForCheck();
        },
        this.errorHandle.bind(this)
      );
  }

  private updateCache(workPackages: any[]) {
    workPackages.forEach((item: any) => {
      let wp = this.halResourceService.createHalResource<WorkPackageResource>(item);
      this.wpCacheService.updateWorkPackage(wp, true);
    });
  }

  private refreshWithParams(params: any) {
    let filterByProject = params.filters.some((filter: any) => {
      return Object.keys(filter).some((key: any) => key === 'project');
    });
    this.filters = params.filters;
    if (!filterByProject) {
      this.filters.push(this.posibleProjectFilter);
    }
    this.order = params.orderBy;
    this.loadTasks();
  }

  private onSearch(params: any) {
    this.searchFilter = params.filters;
    if (params.filters[0]['subjectOrId']['values'] == '') {
      this.searchFilter = [];
    }
    this.loadTasks();
  }

  private errorHandle(error: any) {
    this.errorService.show(error.message);
    this.loaderService.loading(false);
  }

  public onExportWPs(username: string) {
    this.taskService.exportByMember(this.currentUser.name, this.currentUserService.title, username, this.filteredProjects, this.tasksGroupByProject.work_packages);
  }
}