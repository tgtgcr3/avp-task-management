import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { StateService, TransitionService, Transition } from '@uirouter/core';
import { AVP_VIEWS } from '../../../common/consts/view-name';
import { AVP, noAccents } from '../../../common/consts/avp';
import { MembersService } from '../../services/members.service';
import { Subscription } from 'rxjs';
import { LoaderService } from '../../../common/components';

@Component({
  templateUrl: './list-member.component.html',
  selector: 'list-member'
})
export class ListMemberComponent implements OnInit, OnDestroy {
  
  private unregisterListener: Function;
  private searchText?: string;

  users: any[] = [];
  pageInfo: any = {
    size: 20,
    page: 1,
    maxSize: 5,
    total: 0
  };
  

  constructor(private loaderService: LoaderService,
    private memberService: MembersService,
    readonly $state: StateService,
    readonly trans: TransitionService) {
      this.unregisterListener = this.trans.onSuccess({}, (transition: Transition) => {
        this.searchText = this.$state.params["query"];
        this.onLoadUsers();
      });
     }

  ngOnInit() {
    this.onLoadUsers();
  }

  onLoadUsers() {
    this.loaderService.loading(true);
    this.memberService.allMember(this.searchText, this.pageInfo.page, this.pageInfo.size).then(
      (response: any) => {
        this.pageInfo.total = response["total"];
        this.users = response['_embedded']['elements'];
        this.loaderService.loading(false);
      }
    )
    .catch((error: any) => {
      console.log(error);
      this.loaderService.loading(false);
    });
  }

  onPageChange(event: any) {

    let params = this.$state.params;
    params["page"] = event;
    this.$state.go('.', params);
  }

  public get view() {
    return AVP_VIEWS;
  }

  public onAvatarFailed(event: any) {
    event.target.src = AVP.user_default_img_url;
  }

  ngOnDestroy() {
    this.unregisterListener();
  }
}