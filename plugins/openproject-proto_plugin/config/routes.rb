Rails.application.routes.draw do
  resources :avp, only: %i[index] do
    collection do
      get '/customfieldids' => 'avp#getTaskCustomFieldIDs', as: 'task_custom_field_ids'
      get '/api/v3/summary' => 'avp#taskSummary', as: 'task_summary'
      post '/api/v3/copy_children' => 'avp#copySubTaskOf', as: 'copy_sub_tasks'

      get '/api/v3/users' => 'avp#users', as: 'users'
      get '/api/v3/usersAll' => 'avp#usersAll', as: 'usersAll'
      get '/api/v3/my_employees' => 'avp#my_employees', as: 'my_employees'
      post '/api/v3/users' => 'avp#update_user', as: 'update_user'

      get '/api/v3/projects' => 'avp#projects', as: 'projects'
      get '/api/v3/project_only' => 'avp#project_only', as: 'project_only'
      get '/api/v3/related_projects' => 'avp#related_projects', as: 'related_projects'
      get '/api/v3/projects/:id' => 'avp#getProject', as: 'get_project'
      get '/api/v3/posible_projects' => 'avp#posible_projects', as: 'posible_projects'
      post '/api/v3/projects/:id' => 'avp#updateProject', as: 'update_project'
      post '/api/v3/projects' => 'avp#createProject', as: 'create_project'
      delete '/api/v3/projects/:id' => 'avp#deleteProject', as: 'delete_project'

      get '/api/v3/projects/:id/status' => 'avp#project_status', as: 'project_status'
      post '/api/v3/projects/:id/status' => 'avp#update_project_status', as: 'update_project_status'

      get '/api/v3/projects/:id/permissions' => 'avp#project_permissions', as: 'project_permissions'
      post '/api/v3/projects/:id/permissions/:fieldId' => 'avp#update_project_permissions', as: 'update_project_permissions'
      get '/api/v3/projects/:id/user_permissions' => 'avp#project_permissions_by_current_user', as: 'current_user_perrmissions'
      
      get '/api/v3/projects/:id/email_configs' => 'avp#project_email_configs', as: 'project_email_configs'
      post '/api/v3/projects/:id/email_configs/:fieldId' => 'avp#update_project_email_configs', as: 'update_project_email_configs'

      get '/api/v3/projects/:id/roles' => 'avp#project_roles', as: 'project_roles'

      get '/api/v3/projects/:id/available_assignees' => 'avp#project_possible_assignees', as: 'project_possible_assignees'

      get '/api/v3/projects/:id/last_wp_created_date' => 'avp#last_work_package_created_date', as: 'last_work_package_created_date'

      get '/api/v3/depts' => 'avp#departments', as: 'departments'
      get '/api/v3/all_departments' => 'avp#all_departments', as: 'all_departments'
      get '/api/v3/deptsWithHierarchy' => 'avp#departments_with_hierarchy', as: 'departments_with_hierachy'
      get '/api/v3/noneDepProjects' => 'avp#none_department_projects', as: 'none_department_projects'
      get '/api/v3/departments' => 'avp#get_departments', as: 'all_departments_api'

      post '/api/v3/categories' => 'avp#create_category', as: 'create_category'
      post '/api/v3/category/:id' => 'avp#edit_category', as: 'edit_category'
      delete '/api/v3/category/:id' => 'avp#delete_category', as: 'delete_category'

      post '/api/v3/memberships' => 'avp#updateMemberships', as: 'update_membership'
      delete '/api/v3/memberships/:id' => 'avp#deleteMemberships', as: 'delete_membership'

      get 'avp_logout' => 'avp#avp_logout', as: 'avp_logout'

      get '/api/v3/query/:id/filters' => 'avp#filter_properties', as: 'filter_properties'

      get 'api/v3/report_all' => 'report_api#report_all', as: 'report_all'
      get 'api/v3/report_project' => 'report_api#report_by_project', as: 'report_by_project'
      get 'api/v3/report_by_employee' => 'report_api#report_by_employee', as: 'report_by_employee'
    end

    get '(/*state)' => 'avp#index', on: :collection, as: ''

  end

  resources :repeated_task do
    collection do 
      get '/list' => 'repeated_task#show', as: 'show_list'
      post '/api/v3/delete_work_packages' => 'repeated_task#destroy_work_packages', as: 'destroy_work_packages'
    end
  end

  resources :avp_forum do
    collection do
      get '/api/v3/all' => 'avp_forum#all', as: 'list_topic'
      get '/api/v3/message_representer' => 'avp_forum#message_presenter', as: 'message_presenter'
      get '/api/v3/project/:project_id/topics' => 'avp_forum#by_project', as: 'list_topic_by_project'
      post '/api/v3/project/:project_id/topics' => 'avp_forum#create_topic', as: 'create_topic'

      get '/api/v3/topics/:id' => 'avp_forum#get_topic', as: 'get_topic'
      put '/api/v3/topics/:id' => 'avp_forum#edit_topic', as: 'edit_topic'
      delete '/api/v3/topics/:id' => 'avp_forum#delete_topic', as: 'delete_topic'
      post '/api/v3/topics/:id/reply_or_quote' => 'avp_forum#reply_or_quote', as: 'reply_or_quote'
    end
  end

  resources :one_api do
    collection do
      get '/v1/create_work_packages' => 'one_api#create_work_package', as: 'create_work_package'
      get '/v1/work_packages' => 'one_api#work_packages', as: 'work_packages'
      get '/v1/projects' => 'one_api#projects', as: 'projects'
    end
  end

  match '/api/pri/*path', to: redirect { |params, _req| "http://192.168.0.105:8080#{_req.fullpath}" }, via: :all
  match '/auth/realms/*path', to: redirect { |params, _req| "http://sso.anvietenergy.com:9999#{_req.fullpath}" }, via: :all
  match '/api/v1/*path', to: redirect { |params, _req| "http://192.168.0.105:4400#{_req.fullpath}" }, via: :all

end
